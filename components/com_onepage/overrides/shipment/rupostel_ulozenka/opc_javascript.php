<?php
/* 
*
* @copyright Copyright (C) 2007 - 2012 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
*/
defined('_JEXEC') or die('Restricted access');



JHTMLOPC::_('behavior.modal', 'a.ulozenkamodal'); 
if (file_exists(JPATH_SITE.DIRECTORY_SEPARATOR.'plugins'.DIRECTORY_SEPARATOR.'vmshipment'.DIRECTORY_SEPARATOR.'rupostel_ulozenka'.DIRECTORY_SEPARATOR.'helper.php'))
{
	/*
	if (empty($method))
	{
       $db = JFactory::getDBO(); 
		$q = 'select `shipment_params` from `#__virtuemart_shipmentmethods` where shipment_element = \'rupostel_ulozenka\' '; 
		$db->setQuery($q); 
		$params = $db->loadResult(); 
		$err = true; 
		if (empty($params)) 
		{
			$err = true; 
		}
		else
		{
		$a = explode('|', $params); 
		$obj = new stdClass(); 
		foreach ($a as $p)
		 {
		    $a2 = explode('=', $p); 
			if (!empty($a2) && (count($a2)==2))
			 {
			   $obj->$a2[0] = json_decode($a2[1]); 
			 }
		 }
		 
		 $method = $obj; 
		}
	}
	*/
	
require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'plugins'.DIRECTORY_SEPARATOR.'vmshipment'.DIRECTORY_SEPARATOR.'rupostel_ulozenka'.DIRECTORY_SEPARATOR.'helper.php'); 
$xml = UlozenkaHelper::getPobocky($method); 

$js_adresa="\n\nvar adresa=new Array();";
				$js_oteviraci_doba="\n\nvar oteviraci_doba=new Array();";
				$js_cena="\n\nvar cena=new Array();";
				$js_values="\n\nvar values=new Array();";
//				$js_mapa="\n\nvar mapa=new Array();";

//if (!defined('ulozenka_javascript'))
{
$document = JFactory::getDocument(); 
$script = "\n".'//<![CDATA['."\n" 
				."\n\nvar detail_url='".JURI::root()."plugins/vmshipment/ulozenka/detail_pobocky.php?id=';\n"	
				."\n\n
				function changeUlozenka(id) {
				    if (typeof jQuery != 'undefined')
					 jQuery('.zasielka_div1').not('#ulozenka_branch_' + id).hide();
					var ppp = document.getElementById('ulozenka_pobocka'); 
					if (ppp != null)
					ppp.value=id;
					
					
					var b = document.getElementById('ulozenka_pobocky'); 
					if (b != null)
					{
						b.className = b.className.split('invalid').join(''); 
					}
					
					var d = document.getElementById('ulozenka_saved'); 
					if (d != null)
					d.value = id; 
					
					var br = document.getElementById('ulozenka_branch_'+id); 
					if (br != null) br.style.display='block';
					if (typeof jQuery != 'undefined')
					{
					  jQuery('#shipment_id_".$method->virtuemart_shipmentmethod_id."').click(); 
					}
					else
					document.getElementById('shipment_id_".$method->virtuemart_shipmentmethod_id."').onclick();
					
					if (typeof Onepage != 'undefined')
					Onepage.changeTextOnePage3();
					
					if (typeof Onepage.getTotalsObj != 'undefined')
   {
	   var costs = Onepage.getTotalsObj(); 
	   var sh = costs.order_shipping.valuetxt; 
	   var d = document.getElementById('ulozenka_cena'); 
	   if (d != null)
	   {
		   d.innerHTML = sh; 
	   }
	   
   }
					
					
				};\n".'

 
 var ulozenkaId = null;
 var ulozenkaIndex = null;  
 var ulozenkaVmId = '.$method->virtuemart_shipmentmethod_id.'; 
 var lastVmOpt = \'\'; 
 function saveUlozenka()
 {
   var dx = document.getElementsByName(\'virtuemart_shipmentmethod_id\'); 
   if (dx != null)
   for (var i = 0; i<dx.length; i++)
    {
	  if (dx[i].checked)
	  lastVmOpt = dx[i].id; 
	}
   var b = document.getElementById(\'ulozenka_pobocky\'); 
   if (b != null)
     {
	   if (b.options != null)
	   if (b.selectedIndex != null)
	   if (b.selectedIndex > 0)
	   {
	    ulozenkaId = b.options[b.selectedIndex].value;
	    ulozenkaIndex =  b.selectedIndex; 
		//Onepage.op_log(ulozenkaId); 
	    return; 
	   }
	 }
 }
 function restoreUlozenka()
 {
   if (lastVmOpt != \'shipment_id_\'+ulozenkaVmId) return; 
   
   
   
   //Onepage.op_log(ulozenkaId); 
   
   if (typeof ulozenkaId != \'undefined\')
   if (ulozenkaId != null)
   {
    var d = document.getElementById(\'ulozenka_pobocky\'); 
	if (d!= null) 
	  {
	    if (d.options[ulozenkaIndex] != null)
		if (d.options[ulozenkaIndex].value != null)
		if (d.options[ulozenkaIndex].value == ulozenkaId)
		{
	     d.selectedIndex = ulozenkaIndex; 
		 var vmid = d.getAttribute(\'vmid\'); 
		 changeUlozenka(ulozenkaId); 
		} 
	  }
    
   }
   
   if (typeof Onepage.getTotalsObj != \'undefined\')
   {
	   var costs = Onepage.getTotalsObj(); 
	   var sh = costs.order_shipping.valuetxt; 
	   var d = document.getElementById(\'ulozenka_cena\'); 
	   if (d != null)
	   {
		   d.innerHTML = sh; 
	   }
	   
   }

 }
 function updatePriceUlozenka()
 {
	 var shipping_id = Onepage.getVShippingRate();
	 if (shipping_id != ulozenkaVmId) return;
	 if (typeof Onepage.getTotalsObj != \'undefined\')
   {
	   var costs = Onepage.getTotalsObj(); 
	   var sh = costs.order_shipping.valuetxt; 
	   var d = document.getElementById(\'ulozenka_cena\'); 
	   if (d != null)
	   {
		   d.innerHTML = sh; 
	   }
	   
   }
 }
 
 function validateUlozenka()
	{
	   var ppp = document.getElementById(\'shipment_id_'.$method->virtuemart_shipmentmethod_id.'\');
 var ppp2 = document.getElementById("ulozenka_pobocky");
 if ((ppp != null) && (ppp2 != null))
 if (ppp.checked == true && (ppp2.value == 0 || ppp2.value == null)) {
	 
	ppp2.className += \' invalid\'; 
	 
    alert(\'';
	
	
	if (!empty($method->chyba))
	{
	 $script .= addslashes(JText::_($method->chyba));
	}
	else 
	{
	 $script .= 'Vyberte pobočku Uloženky'; 
	}
	
	$script .= '\'); 
	
    return false; 
    }
	}
	
	if (typeof addOpcTriggerer != \'undefined\')
    addOpcTriggerer(\'callSubmitFunct\', \'validateUlozenka\');
 
 
 
 addOpcTriggerer(\'callBeforeAjax\', \'saveUlozenka()\'); 
 addOpcTriggerer(\'callAfterRender\', \'restoreUlozenka()\'); 
 addOpcTriggerer(\'callAfterPaymentSelect\', \'updatePriceUlozenka()\'); 

				
				
//]]>'."\n"; 

$document->addScriptDeclaration($script);  
 //define('ulozenka_javascript'); 

 }				

}	

$style = '
select#ulozenka_pobocky.invalid, #vmMainPageOPC select#ulozenka_pobocky.invalid { 
 border: 1px solid red; 
}

'; 
$document->addStyleDeclaration( $style );