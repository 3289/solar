<?php
/**
 * Overrided Controller class for the OPC ajax and checkout
 *
 * @package One Page Checkout for VirtueMart 2
 * @subpackage opc
 * @author stAn
 * @author RuposTel s.r.o.
 * @copyright Copyright (C) 2007 - 2012 RuposTel - All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * One Page checkout is free software released under GNU/GPL and uses some code from VirtueMart
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * 
 *
 */

 // Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');



if (!class_exists('VirtueMartControllerCart'))
	  require_once(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.'cart.php'); 
	  
class VirtueMartControllerCartOpc extends VirtueMartControllerCart {

    /**
     * To set a payment method
     *
     * @author Max Milbers
     * @author Oscar van Eijk
     * @author Valerie Isaksen
     */
    function setpayment(&$cart) {

	/* Get the payment id of the cart */
	//Now set the payment rate into the cart
	if (defined('VM_VERSION') && (VM_VERSION >= 3))
	if (!empty($cart->cartPrices))
	{
		$cart->pricesUnformatted = $cart->cartPrices; 
	}
	
	if ($cart) {
		if(isset($cart->pricesUnformatted['billTotal']) && empty($cart->pricesUnformatted['billTotal'])) return true; # seyi_code

		if(!class_exists('vmPSPlugin')) require(JPATH_VM_PLUGINS.DIRECTORY_SEPARATOR.'vmpsplugin.php');
	    JPluginHelper::importPlugin('vmpayment');
	    //Some Paymentmethods needs extra Information like
	    $virtuemart_paymentmethod_id = JRequest::getInt('virtuemart_paymentmethod_id', '0');
		$cart->virtuemart_paymentmethod_id = $virtuemart_paymentmethod_id; 
	    //OLD: $cart->setPaymentMethod($virtuemart_paymentmethod_id);

	    //Add a hook here for other payment methods, checking the data of the choosed plugin
	    $_dispatcher = JDispatcher::getInstance();
		$msg = ''; 
		
		$cart->virtuemart_paymentmethod_id = $virtuemart_paymentmethod_id;

		/*
				$retValues = $_dispatcher->trigger('plgVmOnCheckoutCheckDataPayment', array( $cart));

				foreach ($retValues as $retVal) {
					if ($retVal === true) {
						$cart->setCartIntoSession();
						return true;  
						// Plugin completed succesful; nothing else to do
					} elseif ($retVal === false) {
						// Missing data, ask for it (again)
						$redirectMsg = OPCLang::_('COM_VIRTUEMART_CART_NO_PAYMENT_SELECTED'); 
						 $mainframe = JFactory::getApplication();
						 $mainframe->redirect(JRoute::_('index.php?option=com_virtuemart&view=cart&task=editpayment',false,$this->useSSL), 'Error 68: '.$redirectMsg);
						// 	NOTE: inactive plugins will always return null, so that value cannot be used for anything else!
					}
				}
		*/
		
	    $_retValues = $_dispatcher->trigger('plgVmOnSelectCheckPaymentOPC', array( &$cart, &$msg));
	    $dataValid = true;
		
		
		
		
	    foreach ($_retValues as $_retVal) {
		if ($_retVal === true ) {
			
		
		    $cart->setCartIntoSession();
			// opc mod:
			return true; 
		   
		} else if ($_retVal === false ) {
		
		   $redirectMsg = $msg; 
		   if (empty($redirectMsg))
		   {
			   $redirectMsg = OPCLang::_('COM_VIRTUEMART_CART_NO_PAYMENT_SELECTED'); 
		   }
		   /*
		  
		   $redirectMsg = ''; 
		   if (empty($msg))
		   $msg = JFactory::getApplication()->getMessageQueue(); 
				if (!empty($msg) && (is_array($msg)))
				{
				  
				  foreach ($msg as $line)
				  {
				  if (is_array($line))
				  {
				   if (!empty($line['message']))
				    $redirectMsg .= $line['message'].'<br />'; 
				   }
				   else
				   {
				    $redirectMsg .= $line.'<br />'; 
				   }
				  }
				}
				else $redirectMsg = $msg; 
			*/	
			 $mainframe = JFactory::getApplication();
			 require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'loader.php'); 
			 $url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart&task=editpayment'; 
			$lang = OPCloader::getLangCode(); 
			if (!empty($lang))
		    $url .= '&lang='.$lang; 
		    $error = 'Error 99: '.$redirectMsg;
			
			OPCloader::storeError($error); 
			
			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
			
			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 
			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			
			if (empty($debug_plugins)) $error = $redirectMsg;
			
			
			
				$mainframe->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $error);
			 
		     
		    break;
		}
	    }
//			$cart->setDataValidation();	//Not needed already done in the getCart function

	    if ($cart->getInCheckOut()) {
		return true; 
	    }
	}
	

	return true; 
	parent::display();
    }
	
	    /**
     * Sets a selected shipment to the cart
     *
     * @author Max Milbers
     */
    public function setshipment(&$cart, $virtuemart_shipmentmethod_id_here=null, $redirect=true, $incheckout=true) {
		
		
		
	include(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'onepage.cfg.php'); 
	if (!empty($op_disable_shipping)) return true; 
	/* Get the shipment ID from the cart */
	if (empty($virtuemart_shipmentmethod_id_here))
	$virtuemart_shipmentmethod_id = JRequest::getInt('virtuemart_shipmentmethod_id', '0');
	else $virtuemart_shipmentmethod_id = $virtuemart_shipmentmethod_id_here; 
	
	
	
	$mainframe = JFactory::getApplication();
	if (!empty($virtuemart_shipmentmethod_id)) {
	    //Now set the shipment ID into the cart
	    
		// general test: 
		$n_id = (int)$virtuemart_shipmentmethod_id; 
		if (!empty($n_id))
		{
			// check for non existent shipping method: 
			$db = JFactory::getDBO(); 
			$q = 'select virtuemart_shipmentmethod_id from #__virtuemart_shipmentmethods where virtuemart_shipmentmethod_id = '.$n_id.' limit 1'; 
			$db->setQuery($q); 
			$n_id2 = $db->loadResult(); 
			
			
			
			$e = $db->getErrorMsg(); if (!empty($e)) {
			 JFactory::getApplication()->enqueueMessage($e); 
		     return; 
			}
			if (empty($n_id2))
			{
				$redirectMsg = 'Error 155: '.OPCLang::_('COM_VIRTUEMART_CART_NO_SHIPPINGRATE'); 
				if ($redirect)
				{
					
					$url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart&task=edit_shipment'; 
					$lang = OPCloader::getLangCode(); 
					if (!empty($lang))
					$url .= '&lang='.$lang; 
					$error = 'Error 120: '.OPCLang::_('COM_VIRTUEMART_CART_NO_PRODUCT'); 
					OPCloader::storeError($redirectMsg); 
					
					
					require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
			
			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 
			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			
			if (empty($debug_plugins)) $redirectMsg = OPCLang::_('COM_VIRTUEMART_CART_NO_SHIPPINGRATE');
					
					
					
			
			
				   $mainframe->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $redirectMsg);
				   $mainframe->close(); 
				}
			}
		}
		
	    if (!empty($cart)) {
		if (!class_exists('vmPSPlugin')) require(JPATH_VM_PLUGINS .DIRECTORY_SEPARATOR. 'vmpsplugin.php');
		JPluginHelper::importPlugin('vmshipment');
		
		if (!empty($virtuemart_shipmentmethod_id))
		{
		$cart->virtuemart_shipmentmethod_id = $virtuemart_shipmentmethod_id; 
		}
		
		// let's do the opc validation first: 
		$_dispatcher = JDispatcher::getInstance();
		$msg = ''; 
		$_retValues = $_dispatcher->trigger('plgVmOnSelectCheckShipmentOPC', array( &$cart, &$msg));
		foreach ($_retValues as $r)
		{
			if ($r === false)
			{
				$redirectMsg = 'Error 180: '.$msg.OPCLang::_('COM_VIRTUEMART_CART_NO_SHIPPINGRATE'); 
				if ($redirect)
				{
					
				$url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart&task=edit_shipment'; 
				$lang = OPCloader::getLangCode(); 
				if (!empty($lang))
				$url .= '&lang='.$lang; 
				$error = $redirectMsg;
			     OPCloader::storeError($error); 
					
					
					require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			
			if (empty($debug_plugins)) $redirectMsg = OPCLang::_('COM_VIRTUEMART_CART_NO_SHIPPINGRATE');
			
			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 
			
				$mainframe->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $redirectMsg);
				JFactory::getApplication()->close(); 
				}
				return false; 
			}
		}
		
		
		
		
		if (method_exists($cart, 'setShipment'))
		{
		  $cart->setShipment($virtuemart_shipmentmethod_id);
		}
		else
		{
		  $cart->virtuemart_shipmentmethod_id = $virtuemart_shipmentmethod_id; 
		}
		//Add a hook here for other payment methods, checking the data of the choosed plugin
		/*
		$_retValues = $_dispatcher->trigger('plgVmOnSelectCheckShipment', array( &$cart));
		$dataValid = true;
		foreach ($_retValues as $_retVal) {
		    if ($_retVal === true ) {// Plugin completed succesfull; nothing else to do
			$cart->setCartIntoSession();
			// opc mod
			return true; 
			break;
		    } else if ($_retVal === false ) {
		       
			   $msg = JFactory::getSession()->get('application.queue');; 
				if (!empty($msg) && (is_array($msg)))
				$redirectMsg = implode('<br />', $msg); 
			
				if (empty($redirectMsg))
				{
					$redirectMsg = 'Error 219: '.OPCLang::_('COM_VIRTUEMART_CART_NO_SHIPPINGRATE'); 
				}
			   if ($redirect)
			   {
		       $mainframe->redirect(JRoute::_('index.php?option=com_virtuemart&view=cart&task=checkout',false,$this->useSSL), 'Error 218: '.$redirectMsg);
			   JFactory::getApplication()->close(); 
			   }
			   else return;
			break;
		    }
		}
		
		
		*/
		
		
		if ($incheckout)
		if (method_exists($cart, 'getInCheckOut'))
		if ($cart->getInCheckOut()) {
			//opc mod
			return true; 
		}
	    }
	}

	return true; 
	
    }



}