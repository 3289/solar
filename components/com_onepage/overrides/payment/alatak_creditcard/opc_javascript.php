<?php
/**
 *
 * a Stripe payment Charge method:
 *
 * @version 1.01
 * @version Stripe PHP bindings from the Stripe API Libraries v1.6.2
 * @author Herv� Boinnard
 * @copyright Copyright (C) 2013 Herv� Boinnard - (C) 2012 Stephen.V. - All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
 *
 * http://www.puma-it.ie
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

$paymentForm = 'adminForm'; 
if (empty($viewData))
{
	$viewData = array(); 
	$viewData['virtuemart_paymentmethod_id'] = $method->virtuemart_paymentmethod_id; 
	
			$creditCards = $method->creditcards;
		if (empty($creditCards)) {
			$creditCards = array('visa', 'visa_electron', 'mastercard','amex','discover',
				'diners_club_international','diners_club_carte_blanche','jcb','laser','maestro');
		} elseif (!is_array($creditCards)) {
			$creditCards = (array)$creditCards;
		}
		foreach ($creditCards as $key => $creditCard) {
			$creditCards[$key] = '"' . $creditCard . '"';
		}
		$creditCardsList = implode(',', $creditCards);

		$viewData['creditcards'] = $creditCardsList; 
	
}
	JFactory::getDocument()->addStyleSheet(JURI::root(true) . '/components/com_onepage/overrides/payment/alatak_creditcard/alatak_creditcard.css');




	$js='
jQuery(document).ready( function($) {
	 
	
	attachCCE(); 
	
	
});

function attachCCE(e)
{
	console.log("attach ce"); 
	if (typeof e == \'undefined\') e = "#card_number_' . $viewData['virtuemart_paymentmethod_id'] . '"; 
	jQuery(e).validateCreditCard(function (e) {
            return jQuery("#card_number_' . $viewData['virtuemart_paymentmethod_id'] . '").removeAttr("class").attr("class","").addClass("card_number"), null == e.card_type ? void jQuery(".vertical.maestro").slideUp({duration: 200}).animate({opacity: 0}, {
                queue: !1,
                duration: 200
            }) : (jQuery("#card_number_' . $viewData['virtuemart_paymentmethod_id'] . '").addClass(e.card_type.name), "maestro" === e.card_type.name ? jQuery(".vertical.maestro").slideDown({duration: 200}).animate({opacity: 1}, {queue: !1}) : jQuery(".vertical.maestro").slideUp({duration: 200}).animate({opacity: 0}, {
	queue: !1,
                duration: 200
            }), luhn_valid(e,"#card_number_' . $viewData['virtuemart_paymentmethod_id'] . '")
            )
        }, {accept: [' . $viewData['creditcards'] . ']});
        
}

function luhn_valid(e, card_number) {
	        if (e.length_valid && e.luhn_valid) {
	        	 jQuery("#card_number_' . $viewData['virtuemart_paymentmethod_id'] . '").removeAttr("class").attr("class","");
	             jQuery("#card_number_' . $viewData['virtuemart_paymentmethod_id'] . '").addClass("card_number valid " + e.card_type.name);
	             jQuery("#card_type_' . $viewData['virtuemart_paymentmethod_id'] . '").val(e.card_type.name);
				jQuery("#payment_id_' . $viewData['virtuemart_paymentmethod_id'] . '").attr("checked", true);

	        } else {
	            jQuery(card_number).removeClass("valid").addClass("card_number "  + e.card_type.name);
				jQuery("#payment_id_' . $viewData['virtuemart_paymentmethod_id'] . '").attr("checked", false);

	        }
			if (e.card_type.name==="amex") {
					jQuery("#cvv_' . $viewData['virtuemart_paymentmethod_id'] . '").attr("placeholder", "1234");
					jQuery("#cvv_' . $viewData['virtuemart_paymentmethod_id'] . '").attr("maxlength", "4");
			} else {
					jQuery("#cvv_' . $viewData['virtuemart_paymentmethod_id'] . '").attr("placeholder", "123");
					jQuery("#cvv_' . $viewData['virtuemart_paymentmethod_id'] . '").attr("maxlength", "3");
			}
    }


'; // addScriptDeclaration
static $jsLoaded = false;

if (VM_VERSION < 3) {

	if (!$jsLoaded) {
		JFactory::getDocument()->addScript(JURI::root(true) . '/plugins/vmpayment/alatak_creditcard/alatak_creditcard/assets/js/jquery.creditCardValidator.js');
	}
		$js = "	//<![CDATA[" . $js . "//]]>";
		JFactory::getDocument()->addScriptDeclaration($js);
		$jsLoaded=true;

} else {
if (!$jsLoaded) {
	vmJsApi::addJScript('/plugins/vmpayment/alatak_creditcard/alatak_creditcard/assets/js/jquery.creditCardValidator.js');
}
	vmJsApi::addJScript('creditCardForm', $js);
}

