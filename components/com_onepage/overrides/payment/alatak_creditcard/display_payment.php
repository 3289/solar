<?php  defined ('_JEXEC') or die();
/**
 * @version 2.5.0
 * @package VirtueMart
 * @subpackage Plugins - vmpayment
 * @author 		    Valérie Isaksen (www.alatak.net)
 * @copyright       Copyright (C) 2012-2015 Alatak.net. All rights reserved
 * @license		    gpl-2.0.txt
 *
 */



?>
<div id="ccoffline_form">

	<ul >

		<li>
			<label for="card_number_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>"><?php echo vmText::_('VMPAYMENT_ALATAK_CREDITCARD_CCNUM') ?></label>
			<input type="text" name="card_number_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" value="<?php echo $viewData['card_number']; ?>" id="card_number_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" placeholder="1234 5678 9012 3456" class="card_number" onchange="attachCCE(this);">
			<input type="hidden" name="card_type_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" id="card_type_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" >
		</li>
		<li class="vertical">
			<ul>
				<li>
					<label for="expiry_date_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>"><?php echo vmText::_('VMPAYMENT_ALATAK_CREDITCARD_EXDATE') ?></label>
					<input type="text" name="expiry_date_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" id="expiry_date_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" value="<?php echo $viewData['expiry_date']; ?>" maxlength="5" placeholder="mm/yy" class="expiry_date">
				</li>

				<li>
					<label for="cvv_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>"><?php echo vmText::_('VMPAYMENT_ALATAK_CREDITCARD_CVV') ?></label>
					<input type="text" name="cvv_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" id="cvv_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" value="<?php echo $viewData['cvv']; ?>" maxlength="3" placeholder="123" class="cvv">
				</li>
			</ul>
		</li>

		<li class="vertical maestro" style="display: none; opacity: 0;">
			<ul>
				<li>
					<label for="issue_date_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>"><?php echo vmText::_('VMPAYMENT_ALATAK_CREDITCARD_ISSUE_DATE') ?></label>
					<input type="text" name="issue_date_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" id="issue_date_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>"  placeholder="mm/yy" value="<?php echo $viewData['issue_date']; ?>" maxlength="5" class="issue_date">
				</li>

				<li>
					<span class="or">or</span>
					<label for="issue_number_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>"><?php echo vmText::_('VMPAYMENT_ALATAK_CREDITCARD_ISSUE_NUMBER') ?></label>
					<input type="text" name="issue_number_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" id="issue_number_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" placeholder="12" maxlength="2" value="<?php echo $viewData['issue_number']; ?>" class="issue_number">
				</li>
			</ul>
		</li>

		<li>
			<label for="name_on_card_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>"><?php echo vmText::_('VMPAYMENT_ALATAK_CREDITCARD_CCNAME') ?></label>
			<input type="text" name="name_on_card_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" id="name_on_card_<?php echo $viewData['virtuemart_paymentmethod_id']; ?>" value="<?php echo $viewData['name_on_card']; ?>" placeholder="<?php echo vmText::_('VMPAYMENT_ALATAK_CREDITCARD_CCNAME_PLACEHOLDER') ?>">
		</li>
	</ul>

</div>