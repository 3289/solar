<?php
/**
 * Overrided portion of cart class for OPC2 on Virtuemart 2
 *
 * @package One Page Checkout for VirtueMart 2
 * @subpackage opc
 * @author stAn
 * @author RuposTel s.r.o.
 * @copyright Copyright (C) 2007 - 2012 RuposTel - All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * One Page checkout is free software released under GNU/GPL and uses some code from VirtueMart
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * 
 *
 */

 // Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'loader.php'); 

class OPCcheckout extends VirtueMartCart
{
     //public static $_triesValidateCoupon;
    var $cartProductsData = array();
	//public static $_triesValidateCoupon = 0;
	
	var $useSSL = 1;
	
	 var $prices = null;
	 var $pricesUnformatted = null;
	 var $pricesCurrency = null;
	 public static $opc_cart = null; 
     function __construct(&$cart) {
	 
		//self::$_triesValidateCoupon=0;
		self::$opc_cart =& $cart;
		foreach ($cart as $key => &$val)
		 {
		   $this->{$key} = $val; 
		 }
		 
		return; 
	    
		
		
	}
	
	static $current_cart; 
	static $opc_controller; 
	static $new_order; 
	
	function checkoutData($redirect=true) {
	    
		$cart =& self::$current_cart; 


		
		$this->_redirect = false;
		$this->_inCheckOut = true;
		include(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'onepage.cfg.php'); 
		$cart->_inCheckOut = true;
		
		
		
		
		if (!isset($cart->tosAccepted)) $cart->tosAccepted = 1; 
		$cart->tosAccepted = JRequest::getInt('tosAccepted', $cart->tosAccepted);


		if (!isset($cart->tos)) $cart->tos = 1; 
		$cart->tos = JRequest::getInt('tos', $cart->tosAccepted);

		
		if (!isset($cart->customer_comment)) $cart->customer_comment = ''; 
		
		$cart->customer_comment = JRequest::getVar('customer_comment', $cart->customer_comment);
		if (empty($cart->customer_comment))
		{
		  $cart->customer_comment = JRequest::getVar('customer_note', $cart->customer_comment);
		}
		
		$op_disable_shipto = OPCloader::getShiptoEnabled($cart); 
		if (empty($op_disable_shipto))
		{
			$shipto = JRequest::getVar('shipto', null); 
			
		if ($shipto != 'new')
		if (($cart->selected_shipto = $shipto) !== null) {
			//JModel::addIncludePath(JPATH_VM_ADMINISTRATOR .DIRECTORY_SEPARATOR. 'models');
			require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'mini.php'); 	
			$userModel = OPCmini::getModel('user'); //JModel::getInstance('user', 'VirtueMartModel');
			$stData = $userModel->getUserAddressList(0, 'ST', $cart->selected_shipto);
			if (isset($stData[0]))
			$this->validateUserData('ST', $stData[0], $cart);
		}
		}
		else
		{
		    $cart->STsameAsBT = 1;
			$cart->ST = $cart->BT;
		}
		
		
		
		
		$cart->setCartIntoSession();

		$mainframe = JFactory::getApplication();
		
		if (isset($cart->cartProductsData))
		$count = count($cart->cartProductsData); 
		else $count = count($cart->products); 
		
		$qerrors = array(); 
		$redirectMsg = false; 
		
		if ($count == 0) {
			
			$url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart'; 
			$lang = OPCloader::getLangCode(); 
			if (!empty($lang))
		    $url .= '&lang='.$lang; 
			
			
			
		    $error = 'Error 120: '.OPCLang::_('COM_VIRTUEMART_CART_NO_PRODUCT'); 
			OPCloader::storeError($error); 
			
			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
			
			
			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 
			
			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			if (empty($debug_plugins)) $error = OPCLang::_('COM_VIRTUEMART_CART_NO_PRODUCT'); 
			
			$this->redirect(JRoute::_($url, false,  VmConfig::get('useSSL', false)), $error);
		} else {
			foreach ($cart->products as $product) {
				$redirectMsg = $this->checkForQuantities($product, $product->quantity);
				if ($redirectMsg !== true)
				{
					$qerrors[] = $redirectMsg; 
				}
				
			}
		}

		if (!empty($qerrors)) {
					$redirectMsg = implode("<br />\n", $qerrors); 
					
					//$this->redirect(JRoute::_('index.php?option=com_virtuemart&view=cart&task=checkout', false, VmConfig::get('useSSL', false)), 'Error 135: '.$redirectMsg);
					//return false; 
				}

		
		include(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'onepage.cfg.php'); 
		
		
		
		//But we check the data again to be sure
		if (empty($cart->BT)) {
			
			$url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart&task=checkout'; 
			$lang = OPCloader::getLangCode(); 
			if (!empty($lang))
		    $url .= '&lang='.$lang; 
		    $error = 'Error 147: Cart Bill To is empty' ; 
			OPCloader::storeError($error); 
			
						require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
			
						$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 

			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			if (empty($debug_plugins)) $error = 'Cart Bill To is empty'; 
			
			
			$this->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $error);
			
		} else {
			
			$redirectMsg = $this->validateUserData('BT', null, $cart);
			
		if (defined('VM_VERSION') && (VM_VERSION >= 3))
		{
			$redirectMsg2 = $this->validateUserData('cart', null, $cart);
			if (!empty($redirectMsg2))
			{
				if (!empty($redirectMsg)) $redirectMsg .= '<br />'; 
				$redirectMsg .= $redirectMsg2; 
			}
		}
			
			
		}
	
		if($cart->STsameAsBT!==0){
			$cart->ST = $cart->BT;
		} else {
			//Only when there is an ST data, test if all necessary fields are filled
			if (!empty($cart->ST)) {
				$redirectMsg3 = $this->validateUserData('ST', null, $cart);
			
				if ($redirectMsg3) {
					
					if (!empty($redirectMsg)) $redirectMsg .= '<br />'; 
					$redirectMsg .= $redirectMsg3; 
					
					//				$cart->setCartIntoSession();
					//$this->redirect(JRoute::_('index.php?option=com_virtuemart&view=cart&task=checkout', false, VmConfig::get('useSSL', false)), 'Error 169: '.$redirectMsg);
				}
			}
		}
		
		
		$default = new stdClass(); 
	 $default->enabled = false; 
     $config = OPCconfig::getValue('opc_delivery_date', '', 0, $default, true); 
	 if (!empty($config->enabled))
	  {
		   if (!empty($config->required))
		   {
			   
			   $dt = JRequest::getVar('opc_date_picker_store'); 
			   if (isset($_POST['opc_date_picker_store']))
			   if (empty($dt))
			   {
				   $redirectMsg4 = OPCLang::_('COM_ONEPAGE_CHOOSE_DESIRED_DELIVERY_DATE_ERROR');
				   if (!empty($redirectMsg)) $redirectMsg .= '<br />'; 
				   $redirectMsg .= $redirectMsg4; 
			   }
		   }
	  }
	  
	  
	  if ($redirectMsg) {
		    $url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart'; 
			$lang = OPCloader::getLangCode(); 
			if (!empty($lang))
		    $url .= '&lang='.$lang; 
		    $error = 'Error 156: '.$redirectMsg;
			OPCloader::storeError($error); 
			
			
			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 

			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 

			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			if (empty($debug_plugins)) $error = $redirectMsg; 

			
				$this->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $error);
			}


		// Test Coupon
		$shipment = $cart->virtuemart_shipmentmethod_id; 
		$payment = $cart->virtuemart_paymentmethod_id; 
		
		

		//2.0.144: $prices = $cartClass->getCartPrices();
		
		
		$cart->virtuemart_shipmentmethod_id = $shipment; 
		$cart->virtuemart_paymentmethod_id = $payment; 
		
		//2.0.144 added
		if(!class_exists('calculationHelper')) require(JPATH_VM_ADMINISTRATOR.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'calculationh.php');
		$calc = calculationHelper::getInstance();
		
		  
		  if (method_exists($calc, 'setCartPrices')) $vm2015 = true; 
		  else $vm2015 = false; 
			if ($vm2015)
			{
			 $calc->setCartPrices(array()); 
			}
			
		//  $cart->pricesUnformatted = $prices = $calc->getCheckoutPrices(  $cart, false, 'opc');
		$cart->cartPrices = $cart->pricesUnformatted = $prices = OPCloader::getCheckoutPrices(  $cart, false, $vm2015, 'opc' ); //$calc->getCheckoutPrices(  $cart, false, 'opc');
		
		
		// Check if a minimun purchase value is set
		if (($msg = $this->checkPurchaseValue($prices)) != null) {
			
			$url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart&task=checkout'; 
			$lang = OPCloader::getLangCode(); 
			if (!empty($lang))
		    $url .= '&lang='.$lang; 
		    $error = 'Error 205: '.$msg;
			OPCloader::storeError($error); 
			
			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 

			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 

			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			if (empty($debug_plugins)) $error = $msg; 
			
			$this->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $error);
		}
		
		
		if (!empty($prices['billTotal']))
		{
		 // special case for zero value orders, do not charge payment fee: 
		 if ($prices['billTotal'] == $prices['paymentValue'])
		  {
		   $savedp = $cart->virtuemart_paymentmethod_id; 
		   $cart->virtuemart_paymentmethod_id = 0; 
		   if (method_exists($calc, 'getCheckoutPricesOPC'))
	       {
	         $cart->cartPrices = $prices = $calc->getCheckoutPricesOPC(  $cart, false );
	   
	       }
	       else
	       $cart->cartPrices = $prices = OPCloader::getCheckoutPrices(  $cart, false, $vm2015, 'opc' );
		 
		 
		   $cart->virtuemart_paymentmethod_id = 0;  
		 }
		 
		}
		
		
		//2.0.144:end
		
		

		if (!empty($cart->couponCode)) {
			
			if (!class_exists('CouponHelper')) {
				require(JPATH_VM_SITE .DIRECTORY_SEPARATOR. 'helpers' .DIRECTORY_SEPARATOR. 'coupon.php');
			}
			
			$redirectMsg2 = CouponHelper::ValidateCouponCode($cart->couponCode, $prices['salesPrice']);
			/*
			stAn: OPC will not redirect the customer due to incorrect coupons here
			if (!empty($redirectMsg)) {
				$cart->couponCode = '';
				//				$this->setCartIntoSession();
				$this->redirect(JRoute::_('index.php?option=com_virtuemart&view=cart',$cart->useXHTML,$cart->useSSL), $redirectMsg);
			}
			*/
		}
	  
	  
		//Test Shipment and show shipment plugin
	    $op_disable_shipping = OPCloader::getShippingEnabled($cart); 
		if (empty($op_disable_shipping))
		{
		if (empty($cart->virtuemart_shipmentmethod_id)) {
			$redirectMsg = OPCLang::_('COM_VIRTUEMART_CART_NO_SHIPPINGRATE'); 
			
			$url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart'; 
			$lang = OPCloader::getLangCode(); 
			if (!empty($lang))
		    $url .= '&lang='.$lang; 
		    $error = 'Error 258: '.$redirectMsg; 
			OPCloader::storeError($error); 

			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 

			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 

			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			if (empty($debug_plugins)) $error = $redirectMsg; 

			
			$this->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $error);
		} else {
			if (!class_exists('vmPSPlugin')) require(JPATH_VM_PLUGINS .DIRECTORY_SEPARATOR. 'vmpsplugin.php');
			JPluginHelper::importPlugin('vmshipment');
			//Add a hook here for other shipment methods, checking the data of the choosed plugin
			$dispatcher = JDispatcher::getInstance();
			$msg = ''; 
			$retValues = $dispatcher->trigger('plgVmOnCheckoutCheckDataShipmentOPC', array(  &$cart, &$msg));

			foreach ($retValues as $retVal) {
				if ($retVal === true) {
					break; // Plugin completed succesful; nothing else to do
				} elseif ($retVal === false) {
					// Missing data, ask for it (again)
					$redirectMsg = $msg.OPCLang::_('COM_VIRTUEMART_CART_NO_SHIPPINGRATE'); 
					
					$url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart'; 
					$lang = OPCloader::getLangCode(); 
					if (!empty($lang))
					$url .= '&lang='.$lang; 
					$error = 'Error 272: '.$redirectMsg; 
					OPCloader::storeError($error); 

			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 

			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 

			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			if (empty($debug_plugins)) $error = OPCLang::_('COM_VIRTUEMART_CART_NO_SHIPPINGRATE');

					
					$this->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $error);
					// 	NOTE: inactive plugins will always return null, so that value cannot be used for anything else!
				}
			}
		}
		}
		

		
		// some shipping uset payment: 
		$cart->virtuemart_paymentmethod_id = $payment; 
	
		//echo 'hier ';
		//Test Payment and show payment plugin

		$total = (float)$prices['billTotal']; 
		
		
		
		if ($total > 0)
		{
		if (empty($cart->virtuemart_paymentmethod_id)) {
			$redirectMsg = OPCLang::_('COM_VIRTUEMART_CART_NO_PAYMENT_SELECTED'); 
			
			$url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart'; 
			$lang = OPCloader::getLangCode(); 
			if (!empty($lang))
		    $url .= '&lang='.$lang; 
		    $error = 'Error 290: '.$redirectMsg;
			OPCloader::storeError($error); 
			
			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 

			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 

			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			if (empty($debug_plugins)) $error = $redirectMsg;
			
			$this->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $error);
			
			
		} else {
			if(!class_exists('vmPSPlugin')) require(JPATH_VM_PLUGINS.DIRECTORY_SEPARATOR.'vmpsplugin.php');
			JPluginHelper::importPlugin('vmpayment');
			//Add a hook here for other payment methods, checking the data of the choosed plugin
			$dispatcher = JDispatcher::getInstance();
			
			
			
			$retValues = $dispatcher->trigger('plgVmOnCheckoutCheckDataPayment', array( $cart));

		$session = JFactory::getSession ();
		$sessionKlarna = $session->get ('Klarna', 0, 'vm');
		
			
			foreach ($retValues as $retVal) {
				if ($retVal === true) {
					break; // Plugin completed succesful; nothing else to do
				} elseif ($retVal === false) {
				
				$msg = JFactory::getSession()->get('application.queue');; 
				$msgq1 = JFactory::getApplication()->get('messageQueue', array()); 
		        $msgq2 = JFactory::getApplication()->get('_messageQueue', array()); 
				
				$res = array_merge($msg, $msgg1, $msgg2);
				$msg = $res; 
				
				if (!empty($msg) && (is_array($msg)))
				$redirectMsg = implode('<br />', $msg); 
					
					
					$url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart'; 
			$lang = OPCloader::getLangCode(); 
			if (!empty($lang))
		    $url .= '&lang='.$lang; 
		    $error = 'Error 323: '.$redirectMsg;
			OPCloader::storeError($error); 
			
			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 

			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 

			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			if (empty($debug_plugins)) $error = '';
			
			//note, in core VM there is no message shown from here, the plugins should handle their own error messages
			
		     $this->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $error);
					// Missing data, ask for it (again)
					//$this->redirect(JRoute::_('index.php?option=com_virtuemart&view=cart&task=checkout', false, VmConfig::get('useSSL', false)), 'Error 323: '.$redirectMsg);
					// 	NOTE: inactive plugins will always return null, so that value cannot be used for anything else!
				}
			}
		}
		}
		else
		$cart->virtuemart_paymentmethod_id = 0; 

	
	// some payments unset shipping: 
	$shipment = $cart->virtuemart_shipmentmethod_id; 
		
		
			
	
		if (VmConfig::get('agree_to_tos_onorder', 1))
		{
		if (empty($cart->tosAccepted)) {
			require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'mini.php'); 				
			$userFieldsModel = OPCmini::getModel('Userfields'); 
			$required = $userFieldsModel->getIfRequired('agreed');
			if(!empty($required)){
				
				$url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart'; 
			$lang = OPCloader::getLangCode(); 
			if (!empty($lang))
		    $url .= '&lang='.$lang; 
		    $error = 'Error 339: '.OPCLang::_('COM_VIRTUEMART_CART_PLEASE_ACCEPT_TOS');
			OPCloader::storeError($error); 
			
			
			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 

			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 

			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			if (empty($debug_plugins)) $error = OPCLang::_('COM_VIRTUEMART_CART_PLEASE_ACCEPT_TOS');
			
			$this->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $error);
				
				
			}
		}
		}
		/* stAn: 2.0.231: registered does not mean logged in, therefore we are going to disable this option with opc, so normal registration would still work when activation is enabled 
		if (empty($GLOBALS['is_dup']))
		if(VmConfig::get('oncheckout_only_registered',0)) {
			$currentUser = JFactory::getUser();
			if(empty($currentUser->id)){
				$this->redirect(JRoute::_('index.php?option=com_virtuemart&view=cart', false, VmConfig::get('useSSL', false)), OPCLang::_('COM_VIRTUEMART_CART_ONLY_REGISTERED') );
			}
		 }
		 */


		//Show cart and checkout data overview
		
		$cart->_inCheckOut = false;
		$cart->_dataValidated = true;

		$cart->setCartIntoSession();

		return true;
	}
	
	
	function doCurl($order)
	{
		
	 if (!function_exists('curl_multi_exec')) return; 
	 $ch = array(); 
	  include(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'onepage.cfg.php'); 
	  if (!empty($curl_url) && (is_array($curl_url)))
	   {
	     $i = 0; 
	     foreach ($curl_url as $blink)
		  {
		   $i++; 
		   
		    $link = @base64_decode($blink); 
			if (strpos($link, 'http')===0)
			 {
			   	if (!function_exists('curl_init'))
				 return; 
		
			if (isset($order->email))
			$link = str_replace('{email}', $order->email, $link); 
			
			if (isset($order->first_name))
			$link = str_replace('{first_name}', $order->first_name, $link); 
			
			if (isset($order->last_name))
			$link = str_replace('{last_name}', $order->last_name, $link); 
			
			if (isset($order->virtuemart_order_id))
			$link = str_replace('{order_id}', $order->virtuemart_order_id, $link); 
			
			foreach ($order as $key=>$search)
			{
			  if (is_string($search))
			  $link = str_replace('{'.$key.'}', $search, $link); 
			}
			
			//$link = str_replace('{amount}', $order['details']->
			
		
			// http://arguments.callee.info/2010/02/21/multiple-curl-requests-with-php/
		
		    $ch[$i] = null; 
			$ch[$i] = curl_init($link); 
			$url = $link;
			curl_setopt ($ch[$i], CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch[$i], CURLOPT_SSL_VERIFYPEER, 0); 
			curl_setopt($ch[$i], CURLOPT_URL,$url); // set url to post to
			curl_setopt($ch[$i], CURLOPT_RETURNTRANSFER,1); // return into a variable
			curl_setopt($ch[$i], CURLOPT_TIMEOUT, 4000); // times out after 4s
			curl_setopt($ch[$i], CURLOPT_POST, 0); 
			curl_setopt($ch[$i], CURLOPT_ENCODING , "gzip");
			curl_setopt($ch[$i], CURLOPT_CUSTOMREQUEST, 'GET');
		  }
		  }
		
		  $mh = curl_multi_init();
		  if (!empty($ch))
		  foreach ($ch as $key=>$v)
		   {
		     // build the multi-curl handle, adding both $ch
			curl_multi_add_handle($mh, $ch[$key]);
		   }
		   
		   // execute all queries simultaneously, and continue when all are complete
			$running = null;
			$start = microtime(true); 
			
			do {
			
				curl_multi_exec($mh, $running);
				$now = microtime(true); 
				if (($now-$start) > ($adwords_timeout / 1000)) 
				 {
				 $running = false;
				 break 1; 
				 }
				
			} while ($running);
		  
		 

	

			 }
		  
	   
	}
	
    function getEscaped(&$dbc, $string)
	{
		return $dbc->escape($string); 
	}
	
	private function getModifiedData(&$cart, $restore=false)
	{
		static $saved_cart_data; 
		
		include(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'onepage.cfg.php'); 	
		if (!$restore)
		if (!empty($custom_rendering_fields))
		{
			if ($opc_cr_type == 'save_none')
			{
				foreach ($custom_rendering_fields as $fname)
				{
					if (isset($cart->BT[$fname])) {
					
					if (!isset($saved_cart_data['BT'])) $saved_cart_data['BT'] = array(); 
					$saved_cart_data['BT'][$fname] = $cart->BT[$fname]; 
					
					
					$cart->BT[$fname] = ''; 
					}
					if (!empty($cart->ST))
					{
					 if (isset($cart->ST['shipto_'.$fname])) 
					 {
					  if (!isset($saved_cart_data['ST'])) $saved_cart_data['ST'] = array(); 
					  $saved_cart_data['ST']['shipto_'.$fname] = $cart->ST['shipto_'.$fname]; 
					  $cart->ST['shipto_'.$fname] = ''; 
					 }
					 if (isset($cart->ST[$fname])) 
					  {
					  if (!isset($saved_cart_data['ST'])) $saved_cart_data['ST'] = array(); 
					  $saved_cart_data['ST'][$fname] = $cart->ST[$fname]; 
					  $cart->ST[$fname] = ''; 
					  }
					}
					
				}
			}
			else return;
		}
		else return; 
		if ($restore)
		{
			
			if (!empty($custom_rendering_fields))
			{
			if ($opc_cr_type == 'save_none')
			{
				if (empty($saved_cart_data)) return;
				if (!empty($saved_cart_data['ST']))
				foreach ($saved_cart_data['ST'] as $fname=>$val)
				{
					$cart->ST[$fname] = $val; 
				}
				if (!empty($saved_cart_data['BT']))
				foreach ($saved_cart_data['BT'] as $fname=>$val)
				{
					$cart->BT[$fname] = $val; 
					
					
				}

			}
			}
		}
	}
	function getModifiedOrder(&$order, &$cart)
	{
		include(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'onepage.cfg.php'); 	
		if (!empty($custom_rendering_fields))
		{
			if ($opc_cr_type == 'save_none')
			{
				
				foreach ($custom_rendering_fields as $fname)
				{
					$order['details']['BT']->$fname = $cart->BT[$fname]; 
					if (!empty($order['details']['ST']))
					if (!empty($cart->ST))
					{
					 if (isset($cart->ST['shipto_'.$fname]))
					 if (isset($order['details']['ST']))
					 if (isset($order['details']['ST']->${'shipto_'.$fname})) $order['details']['ST']->${'shipto_'.$fname} = $cart->ST['shipto_'.$fname];
				     if (isset($cart->ST[$fname]))
					 if (isset($order['details']['ST']))
					 if (isset($order['details']['ST']->$fname)) 
					 $order['details']['ST']->$fname = $cart->ST[$fname]; 
					}
					
				}
			}
			else return;
		}
		else return; 
		
	}
	/**
	 * This function is called, when the order is confirmed by the shopper.
	 *
	 * Here are the last checks done by payment plugins.
	 * The mails are created and send to vendor and shopper
	 * will show the orderdone page (thank you page)
	 *
	 */
	//function confirmedOrder(&$cart, $ref, &$order) {
	function confirmedOrder() {
	
	    $cart =& self::$current_cart; 
		 
		 $payment_id = $cart->virtuemart_paymentmethod_id; 
		 

		
		 
		include(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'onepage.cfg.php'); 
		
		//Just to prevent direct call
		if ($cart->_dataValidated && $cart->_confirmDone) {
			require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'mini.php'); 	
			$orderModel = OPCmini::getModel('Orders'); 
			

			$this->getModifiedData($cart); 
			if (!isset($delivery_date))
			 {
			    $dt = JRequest::getVar('delivery_date', ''); 
				if (!empty($dt)) $delivery_date = $dt; 
				
				$dt = JRequest::getVar('opc_date_picker_store', ''); 
				if (!empty($dt)) 
				{
				$delivery_date = $dt; 
				JRequest::setVar('delivery_date', $dt); 
				}
			 }
			 
			 if ((!empty($delivery_selector) && (!empty($delivery_date))))
			 {
				 JRequest::setVar($delivery_selector, $delivery_date); 
				 JRequest::setVar('shipto_'.$delivery_selector, $delivery_date); 
				 
				 $cart->BT[$delivery_selector] = $delivery_date; 
				 if (!empty($cart->ST)) $cart->ST[$delivery_selector] = $delivery_date; 
			 }
			 
			
			 
			if (!empty($delivery_date))
			 {
			   $cart->delivery_date = $delivery_date; 
			 }
	
	
			require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'userfields.php');  
        	OPCUserFields::checkCart($cart); 

	
			$orderID = $orderModel->createOrderFromCart($cart);
			
	
	
			
			// only on recent vm : 
			if (defined('VM_VERSION'))
			if (!empty($orderID))
			if (!empty($delivery_date))
			 {
			    $db = JFactory::getDBO(); 
			    $q = 'update `#__virtuemart_orders` set `delivery_date` = "'.$db->escape($delivery_date).'" where virtuemart_order_id = '.(int)$orderID.' limit 1'; 
				$db->setQuery($q); 
				$db->query(); 
				
				
			 }
			 
			 									 
			
			
			$this->getModifiedData($cart, true); 
			
			$msgq1 = JFactory::getApplication()->get('messageQueue', array()); 
		    $msgq2 = JFactory::getApplication()->get('_messageQueue', array()); 

			
			$op_disable_shipping = OPCloader::getShippingEnabled($cart); 
			if ($op_disable_shipping)
			{
			  //$q = 'update #__virtuemart_orders set 
			}
			
			
			if (empty($orderID)) {
				$mainframe = JFactory::getApplication();
				
				$url = 'index.php?option=com_virtuemart&nosef=1&error_redirect=1&view=cart'; 
			$lang = OPCloader::getLangCode(); 
			if (!empty($lang))
		    $url .= '&lang='.$lang; 
		    $error = 'Error 637: Order creation failed !';
			OPCloader::storeError($error); 
			

			
			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 

			$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 

			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			if (empty($debug_plugins)) $error = 'Order creation failed';
			
				$this->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $error);
				
				//$this->redirect(JRoute::_('index.php?option=com_virtuemart&view=cart&task=checkout', false, VmConfig::get('useSSL', false)) , 'Error 637: Order creation failed !');
			}
			
			$cart->virtuemart_order_id = $orderID;
			
			/*
			if (defined('VM_VERSION') && (VM_VERSION >= 3))
			{
				$order = $orderModel->getMyOrderDetails($orderID);
				
			}
			else
		    */
			$order= $orderModel->getOrder($orderID);
			
			

			
			$this->getModifiedOrder($order, $cart); 
			
			
			if (!empty($order['details']['BT']->opc_vat))
			{
				  require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'mini.php'); 
				  if (OPCmini::tableExists('onepage_moss'))
				  {
					  $f = JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'vat.php'; 
					  require_once($f); 
					  
					  OPCvat::addOrderId($order['details']['BT']->opc_vat, $orderID); 
				  }
			}
			
			// $GLOBALS['is_dup']
			if (!empty($orderID))
			{
			
			if (!empty($GLOBALS['is_dup']) && (is_numeric($GLOBALS['is_dup'])))
			{
			  $dbj = JFactory::getDBO(); 
			  $q = "update `#__virtuemart_orders` SET `virtuemart_user_id` = '".$this->getEscaped($dbj, $GLOBALS['is_dup'])."' where virtuemart_order_id = '".$this->getEscaped($dbj, $orderID)."' limit 1"; 
			  $dbj->setQuery($q); 
			  $dbj->query(); 
			  
			  $dbj = JFactory::getDBO(); 
			  $q = "update #__virtuemart_order_userinfos SET virtuemart_user_id = '".$this->getEscaped($dbj, $GLOBALS['is_dup'])."' where virtuemart_order_id = '".$this->getEscaped($dbj, $orderID)."' limit 2"; 
			  $dbj->setQuery($q); 
			  $dbj->query(); 

			  
			  $e = $dbj->getErrorMsg(); 
			 
			}
			else
			if (!empty($GLOBALS['opc_new_user']) && (is_numeric($GLOBALS['opc_new_user'])))
			{
			  $dbj = JFactory::getDBO(); 
			  $q = "update #__virtuemart_orders SET virtuemart_user_id = '".$this->getEscaped($dbj, $GLOBALS['opc_new_user'])."' where virtuemart_order_id = '".$this->getEscaped($dbj, $orderID)."' limit 1"; 
			  $dbj->setQuery($q); 
			  $dbj->query(); 
			  
			  $dbj = JFactory::getDBO(); 
			  $q = "update #__virtuemart_order_userinfos SET virtuemart_user_id = '".$this->getEscaped($dbj, $GLOBALS['opc_new_user'])."' where virtuemart_order_id = '".$this->getEscaped($dbj, $orderID)."' limit 2"; 
			  $dbj->setQuery($q); 
			  $dbj->query(); 

			  
			  $e = $dbj->getErrorMsg(); 
			 
			}

			
			
			}
			//opc_new_user
			if (isset($order['details']['ST']))
			{
			if (empty($order['details']['ST']->email) && (!empty($order['details']['BT']->email))) $order['details']['ST']->email = $order['details']['BT']->email;
			}
// 			$cart = $this->getCart();
			
			if (isset($order['details']['BT']))
			$this->doCurl($order['details']['BT']); 
			
			
// 			$html="";
			
			if ( $order['details']['BT']->order_total <= 0) $no_payment = true; 
			else $no_payment = false; 
			

			
			
			$dispatcher = JDispatcher::getInstance();
			if (empty($op_disable_shipping))	
			JPluginHelper::importPlugin('vmshipment');
			JPluginHelper::importPlugin('vmcustom');
			if (empty($no_payment))
			JPluginHelper::importPlugin('vmpayment');

		
			JPluginHelper::importPlugin('vmcustom');
			JPluginHelper::importPlugin('vmcalculation');
		
			$session = JFactory::getSession();
			$return_context = $session->getId();
			
				
			
		
			
			
			ob_start(); 
			
			
		   //$order= $orderModel->getOrder($orderID);
			
			if ($order['details']['BT']->order_status != $zero_total_status)
			if ( $order['details']['BT']->order_total <= 0) { 
				require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'mini.php'); 	
				$modelOrder = OPCmini::getModel('orders');
				$order['order_status'] = $zero_total_status;
				$order['customer_notified'] = 1;
				$order['comments'] = '';
				$modelOrder->updateStatusForOneOrder($orderID, $order, true);
                //$order['paymentName']= $dbValues['payment_name'];
                //We delete the old stuff
				$cart->emptyCart();
			}
			$output = ob_get_clean(); 
			
			
			
			//$returnValues = $dispatcher->trigger('plgVmConfirmedOrder', array($cart, $order));
			
		    $defhtml = '<div id="thank_you_page_comes_here"></div>'; 
			JRequest::setVar ('html', $defhtml);
			

			
			// pairs the cookie with the database 
			$returnValues = $dispatcher->trigger('plgOpcOrderCreated', array($cart, $order ));
								 		
			if (class_exists('OPCPluginLoaded'))
			{
			// runs shipping confirm as first and payment as last
			$returnValues = $dispatcher->trigger('plgVmConfirmedOrderOPC', array('shipment', $cart, $order));
			
			$returnValues = $dispatcher->trigger('plgVmConfirmedOrderOPC', array('calculation', $cart, $order));
			
			$returnValues = $dispatcher->trigger('plgVmConfirmedOrderOPC', array('custom', $cart, $order));
			
			// reload the order because it does not include the above trigger info
			//$order = $orderModel->getOrder($orderID);
			
			// we used detach event above, so we can now freely call the function again: 
			//$returnValues = $dispatcher->trigger('plgVmConfirmedOrder', array($cart, $order));
			
			
			
			unset($order['customer_notified']); 
			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
			$email_fix1 = OPCconfig::get('email_fix1', false); 
			
			if (!empty($email_fix1))
			{
				$mt = array($orderModel, 'notifyCustomer'); 
			if (is_callable($mt))
			if (method_exists($orderModel, 'notifyCustomer'))
			{
				$saved_status = $order['details']['BT']->order_status; 
				$orderstatusForShopperEmail = VmConfig::get('email_os_s',array('U','C','S','R','X'));
			    $orderstatusForVendorEmail = VmConfig::get('email_os_v',array('U','C','R','X'));
			    //((in_array($saved_status,$orderstatusForVendorEmail) && (!in_array( $new_status, $orderstatusForVendorEmail)))))
			    //  && (!in_array( $new_status, $orderstatusForShopperEmail)))
			    
				
				
				if (in_array($saved_status,$orderstatusForShopperEmail)) 
			    {
					
				  $orderModel->notifyCustomer($orderID, $order);		
			      //$order['customer_notified'] = 1; 
			    }
			
			}
			}
			$except = array('shipment', 'custom', 'calculation'); 
			$returnValues = $dispatcher->trigger('plgVmConfirmedOrderOPCExcept', array($except, &$cart, &$order));			
			
			
			$dispatcher = JDispatcher::getInstance();
			//$dispatcher = JEventDispatcher::getInstance(); 
			
			if (!empty(vmPlugin::$detached))
			foreach (vmPlugin::$detached as &$instance)
			{
		      $dispatcher->detach($instance); 
			}
			// re-attach events: 
			// stAn - we only dettached plgVmConfirmedOrder function
			// we detached all VMplugins, now we can run system plugins
			// important: they cannot further run shipment/payment plugins (display) because they are detached
			JPluginHelper::importPlugin('system'); 
			$returnValues = $dispatcher->trigger('plgVmConfirmedOrder', array($cart, $order));
			
			
			
			
			if (!empty(vmPlugin::$detached))
			foreach (vmPlugin::$detached as &$instance)
			{
			 $dispatcher->attach($instance); 
			}
			
			
			
			
			
			}
			else
			{
				$mt = array($orderModel, 'notifyCustomer'); 
			
			   if (is_callable($mt))
			   if (method_exists($orderModel, 'notifyCustomer'))
			   $orderModel->notifyCustomer($orderID, $order);
				
				
				$returnValues = $dispatcher->trigger('plgVmConfirmedOrder', array($cart, $order));
			}
			 
			//$order = $orderModel->getOrder($orderID);
			
			$new_status = $order['details']['BT']->order_status;
			
			
			/*
			$orderstatusForShopperEmail = VmConfig::get('email_os_s',array('U','C','S','R','X'));
			$orderstatusForVendorEmail = VmConfig::get('email_os_v',array('U','C','R','X'));
			if ((in_array($saved_status,$orderstatusForShopperEmail) && (!in_array( $new_status, $orderstatusForShopperEmail))) && 
			 ((in_array($saved_status,$orderstatusForVendorEmail) && (!in_array( $new_status, $orderstatusForVendorEmail)))))
			
			if ($new_status == $saved_status)
			{
				
			}
			*/
			
			self::$new_order =& $order; 
			
			
			
			/*  OLD CODE: 
			$except = array('shipment', 'custom', 'calculation'); 
			$returnValues = $dispatcher->trigger('plgVmConfirmedOrderOPCExcept', array($except, $cart, $order));			
			*/
			
			//OPC: maybe we want to send emil before a redirect: 
			if (!empty($send_pending_mail))
			{
			
			if(!class_exists('shopFunctionsF')) require(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'shopfunctionsf.php');

		//Important, the data of the order update mails, payments and invoice should
		//always be in the database, so using getOrder is the right method
		require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'mini.php'); 	
		$orderModel=OPCmini::getModel('orders');
		//$order = $orderModel->getOrder($orderID);

		$payment_name = $shipment_name='';
		$op_disable_shipping = OPCloader::getShippingEnabled($cart); 
		
		$msgqx1 = JFactory::getApplication()->get('messageQueue', array()); 
		$msgqx2 = JFactory::getApplication()->get('_messageQueue', array()); 
		$msgqx3 = JFactory::getApplication()->getMessageQueue(); 
		
		if (!class_exists('vmPSPlugin')) require(JPATH_VM_PLUGINS .DIRECTORY_SEPARATOR. 'vmpsplugin.php');
		if (empty($op_disable_shipping))
		JPluginHelper::importPlugin('vmshipment');
		if (empty($no_payment))
		JPluginHelper::importPlugin('vmpayment');
		$dispatcher = JDispatcher::getInstance();
		if (empty($op_disable_shipping))
		$returnValues = $dispatcher->trigger('plgVmonShowOrderPrintShipment',array(  $order['details']['BT']->virtuemart_order_id, $order['details']['BT']->virtuemart_shipmentmethod_id, &$shipment_name));
		if (empty($no_payment))
		$returnValues = $dispatcher->trigger('plgVmonShowOrderPrintPayment',array(  $order['details']['BT']->virtuemart_order_id, $order['details']['BT']->virtuemart_paymentmethod_id, &$payment_name));
		
		
		
		
		$order['shipmentName']=$shipment_name;
		if (empty($no_payment))
		$order['paymentName']=$payment_name; 
		else $order['paymentName']='';

		
		if (!isset($vars)) $vars = array(); 
		
		$vars['orderDetails']=$order;
		if (!isset($vars['newOrderData'])) $vars['newOrderData'] = array(); 
	    $vars['newOrderData']['customer_notified']=1;

	
		$vars['url'] = 'url';
		
		$vars['doVendor'] = false;
		
		if (!empty($order['details']['BT']->virtuemart_vendor_id))
		$virtuemart_vendor_id = $order['details']['BT']->virtuemart_vendor_id; 
		else
		$virtuemart_vendor_id=1;

		require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'mini.php'); 	
		$vendorModel = OPCmini::getModel('vendor');
		$vendor = $vendorModel->getVendor($virtuemart_vendor_id);
		$vars['vendor'] = $vendor;
		$vendorEmail = $vendorModel->getVendorEmail($virtuemart_vendor_id);
		
		if (empty($vendorEmail)) 
		 {
		     $db = JFactory::getDBO ();
			
			  $query = 'SELECT * FROM `#__virtuemart_vmusers`'; 
			  $db->setQuery($query); 
			  $res = $db->loadAssocList(); 
					
						
			$query = 'SELECT ju.email FROM `#__virtuemart_vmusers` as vmu, `#__users` as ju WHERE `virtuemart_vendor_id`=' . (int)$virtuemart_vendor_id.' and ju.id = vmu.virtuemart_user_id and vmu.user_is_vendor = 1 limit 0,1';
			$db->setQuery ($query);
			$vendorEmail = $db->loadResult ();

		   
		   
		 }
		
		
		$vars['vendorEmail'] = $vendorEmail;

		

		// Send the email
		if (file_exists(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'invoice'.DIRECTORY_SEPARATOR.'view.html.php'))
		{
			
		  if (OPCcheckout::renderMail('invoice', $vendorEmail, $vars, null, true, false)) {
		   //ok
		  }
		
		$x = JFactory::getApplication()->set('messageQueue', $msgqx1); 
	    $x = JFactory::getApplication()->set('_messageQueue', $msgqx2); 

		if (OPCJ3)
		if (class_exists('ReflectionClass'))
		{
		   $a = JFactory::getApplication(); 
		   $reflectionClass = new ReflectionClass($a);
		   $property = $reflectionClass->getProperty('_messageQueue'); 
		   $property->setAccessible(true);
		 
		   $property->setValue($a, $msgqx3);
		   $x = JFactory::getApplication()->getMessageQueue(); 
		   
		}
		
		
		
		
		}
			
			}
			
			
			/*
			jimport( 'joomla.plugin.helper' );
			$plugin = JPluginHelper::getPlugin('vmpayment', 'opctracking');
			if (!empty($plugin))
			{
			   $opctracking = true; 
			}
			else 
			{
			   $opctracking = false; 
			}
			*/
			
			
			//$html = JRequest::getVar('html', OPCLang::_('COM_VIRTUEMART_ORDER_PROCESSED'), null, 'string', JREQUEST_ALLOWRAW); 
			$html = JRequest::getVar('html', OPCLang::_('COM_VIRTUEMART_ORDER_PROCESSED'), 'default', 'STRING', JREQUEST_ALLOWRAW);
			$app = JFactory::getApplication(); 
			
			require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php');
			
			$utm_p_load = false; 
			$default = array(); 
			$utm_p = OPCConfig::getValue('opc_config', 'utm_payments', 0, $default, false, false);
			if (!empty($payment_id))
			{
			    if (in_array($payment_id, $utm_p))
				$utm_p_load = true; 
			}
			
			if (method_exists($app, 'input'))
			{
			$html2 = $app->input->get('html', OPCLang::_('COM_VIRTUEMART_ORDER_PROCESSED'), 'RAW' ); 

			
			
			
			
			if ($utm_p_load)
			{
			 $html2 = str_replace('&amp;view=', '&amp;utm_nooverride=1&view=', $html2); 
			 $html2 = str_replace('&view=', '&utm_nooverride=1&view=', $html2); 
			 $app->input->set('html', $html2); 
			}

			
			}
			else 
			{
			$html2 = JRequest::getVar('html', OPCLang::_('COM_VIRTUEMART_ORDER_PROCESSED'), 'default', 'STRING', JREQUEST_ALLOWRAW);
			if ($utm_p_load)
			{
			
			$html2 = str_replace('&view=pluginresponse', '&utm_nooverride=1&view=pluginresponse', $html2); 
			$html2 = str_replace('&amp;view=pluginresponse', '&amp;utm_nooverride=1&amp;view=pluginresponse', $html2); 
			JRequest::setVar('html', $html2); 
			}
			}
			
			
			
			
			if ($html != $html2) $output .= $html2; 
			$output .= $html; 	


			ob_start(); 
			if ( $order['details']['BT']->order_total <= 0) { 
				$cart->emptyCart();
			}
			$output .= ob_get_clean(); 
			
			$x = JFactory::getApplication()->set('messageQueue', $msgq1); 
			$x = JFactory::getApplication()->set('_messageQueue', $msgq2); 

			
			
			
			
			if (!empty($output))
			return $output; 
			
			
			// may be redirect is done by the payment plugin (eg: paypal)
			// if payment plugin echos a form, false = nothing happen, true= echo form ,
			// 1 = cart should be emptied, 0 cart should not be emptied

		
	 }

	}
	
	
	function secondMail($order, $vendorEmail)
	{
	   //OPC: maybe we want to send emil before a redirect: 
			//if (!empty($send_pending_mail))
			
			
			if(!class_exists('shopFunctionsF')) require(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'shopfunctionsf.php');

		//Important, the data of the order update mails, payments and invoice should
		//always be in the database, so using getOrder is the right method
		require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'mini.php'); 	
		$orderModel=OPCmini::getModel('orders');
		//$order = $orderModel->getOrder($orderID);

		$payment_name = $shipment_name='';
		$op_disable_shipping = OPCloader::getShippingEnabled($cart); 
		
		$msgqx1 = JFactory::getApplication()->get('messageQueue', array()); 
		$msgqx2 = JFactory::getApplication()->get('_messageQueue', array()); 
		$msgqx3 = JFactory::getApplication()->getMessageQueue(); 
		
		if (!class_exists('vmPSPlugin')) require(JPATH_VM_PLUGINS .DIRECTORY_SEPARATOR. 'vmpsplugin.php');
		if (empty($op_disable_shipping))
		JPluginHelper::importPlugin('vmshipment');
		if (empty($no_payment))
		JPluginHelper::importPlugin('vmpayment');
		$dispatcher = JDispatcher::getInstance();
		if (empty($op_disable_shipping))
		$returnValues = $dispatcher->trigger('plgVmonShowOrderPrintShipment',array(  $order['details']['BT']->virtuemart_order_id, $order['details']['BT']->virtuemart_shipmentmethod_id, &$shipment_name));
		if (empty($no_payment))
		$returnValues = $dispatcher->trigger('plgVmonShowOrderPrintPayment',array(  $order['details']['BT']->virtuemart_order_id, $order['details']['BT']->virtuemart_paymentmethod_id, &$payment_name));
		
		
		
		
		$order['shipmentName']=$shipment_name;
		if (empty($no_payment))
		$order['paymentName']=$payment_name; 
		else $order['paymentName']='';

		if (!isset($vars)) $vars = array(); 
		
		$vars['orderDetails']=$order;
		if (!isset($vars['newOrderData'])) $vars['newOrderData'] = array(); 
	    $vars['newOrderData']['customer_notified']=1;

	
		$vars['url'] = 'url';
		
		$vars['doVendor'] = false;
		
		if (!empty($order['details']['BT']->virtuemart_vendor_id))
		$virtuemart_vendor_id = $order['details']['BT']->virtuemart_vendor_id; 
		else
		$virtuemart_vendor_id=1;

		require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'mini.php'); 	
		$vendorModel = OPCmini::getModel('vendor');
		$vendor = $vendorModel->getVendor($virtuemart_vendor_id);
		$vars['vendor'] = $vendor;
		
		
		
		$vars['vendorEmail'] = $vendorEmail;

		

		// Send the email
		if (file_exists(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'invoice'.DIRECTORY_SEPARATOR.'view.html.php'))
		{
			
		  if (OPCcheckout::renderMail('invoice', $vendorEmail, $vars, null, true, false)) {
		   //ok
		  }
		
		}
	
	    $x = JFactory::getApplication()->set('messageQueue', $msgqx1); 
	    $x = JFactory::getApplication()->set('_messageQueue', $msgqx2); 

		if (OPCJ3)
		if (class_exists('ReflectionClass'))
		{
		   $a = JFactory::getApplication(); 
		   $reflectionClass = new ReflectionClass($a);
		   $property = $reflectionClass->getProperty('_messageQueue'); 
		   $property->setAccessible(true);
		 
		   $property->setValue($a, $msgqx3);
		   $x = JFactory::getApplication()->getMessageQueue(); 
		   
		}
	
	
	}
	
	
	/** Checks if the quantity is correct
	 *
	 * @author Max Milbers
	 */
	 function checkForQuantities($product, &$quantity=0,&$errorMsg ='') {

		$stockhandle = VmConfig::get('stockhandle','none');
		$mainframe = JFactory::getApplication();
		// Check for a valid quantity
		if (!is_numeric( $quantity)) {
			$errorMsg = OPCLang::_('COM_VIRTUEMART_CART_ERROR_NO_VALID_QUANTITY', false);
			//			$this->_error[] = 'Quantity was not a number';
			return $errorMsg; 
			
		}
		// Check for negative quantity
		if ($quantity < 1) {
			
			$errorMsg = $product->product_name.': '.OPCLang::_('COM_VIRTUEMART_CART_ERROR_NO_VALID_QUANTITY', false);
			return $errorMsg;
		}
		include(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'onepage.cfg.php'); 
		
		if (empty($opc_stock_handling))
		{
		// Check to see if checking stock quantity
		if ($stockhandle!='none' && $stockhandle!='risetime') {

			$productsleft = $product->product_in_stock - $product->product_ordered;
			// TODO $productsleft = $product->product_in_stock - $product->product_ordered - $quantityincart ;
			if ($quantity > $productsleft ){
				if($productsleft>0 and $stockhandle='disableadd'){
					$quantity = $productsleft;
					$errorMsg = $product->product_name.': '.OPCLang::sprintf('COM_VIRTUEMART_CART_PRODUCT_OUT_OF_QUANTITY',$quantity);
					return $errorMsg; 
				} else {
					
					$errorMsg = $product->product_name.': '.OPCLang::_('COM_VIRTUEMART_CART_PRODUCT_OUT_OF_STOCK');
					//$this->setError($errorMsg); // Private error retrieved with getError is used only by addJS, so only the latest is fine
					//vmInfo($errorMsg,$product->product_name,$productsleft);
					
					// $mainframe->enqueueMessage($errorMsg);
					return $errorMsg;
				}
			}
		}
		}
		else
		{
			
			if ($opc_stock_handling === 3) return true; 
			
			if (!empty($opc_stock_zero_weight))
			if (empty($product->product_weight)) return true; 
			 
			 require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'stock.php'); 
			
			$status = OPCstock::getStatus($product); 
			if ($status === 0) return true; 
			
			if ($opc_stock_handling === 1)
			{
				 
				 
				  if ($status === 1)
				  {
					  $errorMsg = 'OPC Error Code 1160, '.$product->product_name.': '.OPCLang::_('COM_VIRTUEMART_CART_PRODUCT_OUT_OF_STOCK');
					 
					 OPCloader::storeError($errorMsg); 
					 
require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			
			if (empty($debug_plugins)) $errorMsg = $product->product_name.': '.OPCLang::_('COM_VIRTUEMART_CART_PRODUCT_OUT_OF_STOCK');
					 //JFactory::getApplication()->enqueueMessage($product->product_name.': '.$errorMsg, 'error'); 
					  return $errorMsg; 
				  }
				  
				  if ($status === 3)
				  {
					  $errorMsg = 'OPC Error Code 1167, '.$product->product_name.': '.OPCLang::sprintf('COM_VIRTUEMART_CART_PRODUCT_OUT_OF_QUANTITY');
					  OPCloader::storeError($errorMsg); 
					  			 
require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			
			if (empty($debug_plugins)) $errorMsg = $product->product_name.': '.OPCLang::_('COM_VIRTUEMART_CART_PRODUCT_OUT_OF_QUANTITY');
					  
					  
					  return $errorMsg; 
					  
					 
					  
				  }
				  
				  
			}
			else
			{
				// opc_stock_handling type 2, ignoring reserved products:
				  if ($status === 1)
				  {
					  $errorMsg = 'OPC Error Code 1181, '.$product->product_name.': '.OPCLang::_('COM_VIRTUEMART_CART_PRODUCT_OUT_OF_STOCK');
					 
					 OPCloader::storeError($errorMsg); 
				  			 
require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			
			if (empty($debug_plugins)) $errorMsg = $product->product_name.': '.OPCLang::_('COM_VIRTUEMART_CART_PRODUCT_OUT_OF_STOCK');

					 //JFactory::getApplication()->enqueueMessage($product->product_name.': '.$errorMsg, 'error'); 
					  return $errorMsg; 
				  }
				  if ($status === 3)
				  {
					  $errorMsg = 'OPC Error Code 1187, '.$product->product_name.': '.OPCLang::_('COM_VIRTUEMART_CART_PRODUCT_OUT_OF_STOCK');
					  
					  OPCloader::storeError($errorMsg); 
					  
					  require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
			
			$debug_plugins = OPCConfig::get('opc_debug_plugins', false); 
			
			if (empty($debug_plugins)) $errorMsg = $product->product_name.': '.OPCLang::_('COM_VIRTUEMART_CART_PRODUCT_OUT_OF_STOCK');
					  
					  
					  
					  return $errorMsg; 
					  
					  JFactory::getApplication()->enqueueMessage($product->product_name.': '.$errorMsg, 'error'); 
					  
				  }
			}
			
			
		}

		// Check for the minimum and maximum quantities
		$min = $product->min_order_level;
		$max = $product->max_order_level;
		if ($min != 0 && $quantity < $min) {
			//			$this->_error[] = 'Quantity reached not minimum';
			$errorMsg = $product->product_name.': '.OPCLang::sprintf('COM_VIRTUEMART_CART_MIN_ORDER', $min);
			//$this->setError($errorMsg);
			//vmInfo($errorMsg,$product->product_name);
			return $errorMsg;
		}
		if ($max != 0 && $quantity > $max) {
			//			$this->_error[] = 'Quantity reached over maximum';
			$errorMsg = OPCLang::sprintf('COM_VIRTUEMART_CART_MAX_ORDER', $max);
			//$this->setError($errorMsg);
			//vmInfo($errorMsg,$product->product_name);
			return $errorMsg;
		}

		return true;
	}
	
	private static function renderMail ($viewName, $recipient, $vars = array(), $controllerName = NULL, $noVendorMail = FALSE,$useDefault=true) {
	   	if(!class_exists( 'VirtueMartControllerVirtuemart' )) require(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.'virtuemart.php');

		$controller = new VirtueMartControllerVirtuemart();
		
		$controller->addViewPath( JPATH_VM_SITE.DIRECTORY_SEPARATOR.'views' );

		$view = $controller->getView( $viewName, 'html' );
		if(!$controllerName) $controllerName = $viewName;
		$controllerClassName = 'VirtueMartController'.ucfirst( $controllerName );
		if(!class_exists( $controllerClassName )) require(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$controllerName.'.php');

	
		$view->addTemplatePath( JPATH_VM_SITE.'/views/'.$viewName.'/tmpl' );

		$vmtemplate = VmConfig::get( 'vmtemplate', 'default' );
		if($vmtemplate == 'default') {
			if(JVM_VERSION >= 2) {
				$q = 'SELECT `template` FROM `#__template_styles` WHERE `client_id`="0" AND `home`="1"';
			} else {
				$q = 'SELECT `template` FROM `#__templates_menu` WHERE `client_id`="0" AND `menuid`="0"';
			}
			$db = JFactory::getDbo();
			$db->setQuery( $q );
			$template = $db->loadResult();
		} else {
			$template = $vmtemplate;
		}

		if($template) {
			$view->addTemplatePath( JPATH_ROOT.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$template.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.$viewName );
		} 

		foreach( $vars as $key => $val ) {
			$view->$key = $val;
		}

		$user = FALSE;
		
		$user = OPCcheckout::sendVmMail( $view, $recipient, $noVendorMail );
	}
	
	
	
	/**
	 * With this function you can use a view to sent it by email.
	 * Just use a task in a controller
	 *
	 * @param string $view for example user, cart
	 * @param string $recipient shopper@whatever.com
	 * @param bool $vendor true for notifying vendor of user action (e.g. registration)
	 */

	private static function sendVmMail (&$view, $recipient, $noVendorMail = FALSE) {

		$jlang = JFactory::getLanguage();
		if(VmConfig::get( 'enableEnglish', 1 )) {
			$jlang->load( 'com_virtuemart', JPATH_SITE, 'en-GB', TRUE );
		}
		$jlang->load( 'com_virtuemart', JPATH_SITE, $jlang->getDefault(), TRUE );
		$jlang->load( 'com_virtuemart', JPATH_SITE, NULL, TRUE );

		if(!empty($view->orderDetails['details']['BT']->order_language)) {
			$jlang->load( 'com_virtuemart', JPATH_SITE, $view->orderDetails['details']['BT']->order_language, true );
			$jlang->load( 'com_virtuemart_shoppers', JPATH_SITE, $view->orderDetails['details']['BT']->order_language, true );
			$jlang->load( 'com_virtuemart_orders', JPATH_SITE, $view->orderDetails['details']['BT']->order_language, true );
		} else {
			if (method_exists('VmConfig', 'loadJLang'))
			{
			 VmConfig::loadJLang('com_virtuemart_shoppers',TRUE);
			 VmConfig::loadJLang('com_virtuemart_orders',TRUE);
			}
		}

		ob_start();

		$view->renderMailLayout( $noVendorMail, $recipient );
		$body = ob_get_contents();
		ob_end_clean();

		$subject = (isset($view->subject)) ? $view->subject : OPCLang::_( 'COM_VIRTUEMART_DEFAULT_MESSAGE_SUBJECT' );
		$mailer = JFactory::getMailer();
		$mailer->addRecipient( $recipient );
		$mailer->setSubject(  html_entity_decode( $subject) );
		$mailer->isHTML( VmConfig::get( 'order_mail_html', TRUE ) );
		$mailer->setBody( $body );

		if(!$noVendorMail) {
			$replyto[0] = $view->vendorEmail;
			$replyto[1] = $view->vendor->vendor_name;
			$mailer->addReplyTo( $replyto );
		}
		/*	if (isset($view->replyTo)) {
				 $mailer->addReplyTo($view->replyTo);
			 }*/

		if(isset($view->mediaToSend)) {
			foreach( (array)$view->mediaToSend as $media ) {
				$mailer->addAttachment( $media );
			}
		}

		// set proper sender
		$sender = array();
		if(!empty($view->vendorEmail) and VmConfig::get( 'useVendorEmail', 0 )) {
			$sender[0] = $view->vendorEmail;
			$sender[1] = $view->vendor->vendor_name;
		} else {
			// use default joomla's mail sender
			$app = JFactory::getApplication();
			$sender[0] = $app->getCfg( 'mailfrom' );
			$sender[1] = $app->getCfg( 'fromname' );
			if(empty($sender[0])){
				$config = JFactory::getConfig();
				if (method_exists($config, 'getValue'))
				$sender = array( $config->getValue( 'config.mailfrom' ), $config->getValue( 'config.fromname' ) );
				else
				$sender = array( $config->get( 'mailfrom' ), $config->get( 'fromname' ) );
			}
		}
		$mailer->setSender( $sender );
		
		// stAn, return the language to original: 
		$jlang = JFactory::getLanguage();
		if(VmConfig::get( 'enableEnglish', 1 )) {
			$jlang->load( 'com_virtuemart', JPATH_SITE, 'en-GB', TRUE );
		}
		
		$lang     = JFactory::getLanguage();
        $tag = $lang->getTag(); 
        $filename = 'com_virtuemart';
        $lang->load($filename, JPATH_ADMINISTRATOR, $tag, true);
        $lang->load($filename, JPATH_SITE, $tag, true);
		
		$jlang->load( 'com_virtuemart_shoppers', JPATH_SITE, $tag, true );
		$jlang->load( 'com_virtuemart_orders', JPATH_SITE, $tag, true );
		
		return $mailer->Send();
	}

	
	
	
	
		/**
	 * Check if a minimum purchase value for this order has been set, and if so, if the current
	 * value is equal or hight than that value.
	 * @author Oscar van Eijk
	 * @return An error message when a minimum value was set that was not eached, null otherwise
	 */
	 function checkPurchaseValue($prices) {
		require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'mini.php'); 	
		$vendor = OPCmini::getModel('vendor'); 
		

		$vendor->setId(self::$opc_cart->vendorId);
		$store = $vendor->getVendor();
		if ($store->vendor_min_pov > 0) {
			
			if ($prices['salesPrice'] < $store->vendor_min_pov) {
				if (!class_exists('CurrencyDisplay'))
				require(JPATH_VM_ADMINISTRATOR .DIRECTORY_SEPARATOR. 'helpers' .DIRECTORY_SEPARATOR. 'currencydisplay.php');
				$currency = CurrencyDisplay::getInstance();
				$minValue = $currency->priceDisplay($min);
				return OPCLang::sprintf('COM_VIRTUEMART_CART_MIN_PURCHASE', $currency->priceDisplay($store->vendor_min_pov));
			}
		}
		return null;
	}
	
	function redirect($x, $y="")
	{
	
	require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 

	$url = 'index.php?option=com_virtuemart&view=cart&task=checkout'; 
				$newitemid = OPCconfig::getValue('opc_config', 'newitemid', 0, 0, true); 
			if (!empty($newitemid)) $url .= '&Itemid='.$newitemid; 

			$lang = OPCloader::getLangCode(); 
			if (!empty($lang))
		    $url .= '&lang='.$lang; 
			
	  $mainframe = JFactory::getApplication();
	  $mainframe->redirect(JRoute::_($url, false, VmConfig::get('useSSL', false)), $y);
	  $mainframe->close(); 
	  return; 
	}
		/**
	 * Test userdata if valid
	 *
	 * @author Max Milbers
	 * @param String if BT or ST
	 * @param Object If given, an object with data address data that must be formatted to an array
	 * @return redirectMsg, if there is a redirectMsg, the redirect should be executed after
	 */
	 function validateUserData($type='BT', $obj = null, $cart=null) {

	 include(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'onepage.cfg.php'); 
	 // we disable validation for ST address, because it is still missing at the front-end and shall be added as an optional feature
		if ($type == 'ST') return false; 
		require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'loader.php'); 
	 
		require_once(JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'mini.php'); 	
		$userFieldsModel = OPCmini::getModel('userfields'); 
		
		
		
		if ($type == 'BT')
		$fieldtype = 'account'; else
		$fieldtype = 'shipment';

		if ($type == 'BT')
		if (empty($bt_fields_from))
		$fieldtype = 'registration'; 
	    else
		{
                   if ($bt_fields_from  === 1) $fieldtype = 'account'; 
                   else $fieldtype = 'cart'; 
		}
		
		if ($fieldtype === 'cart')
		if (!defined('VM_VERSION') || (VM_VERSION < 3))
		{
			 $fieldtype = 'registration'; 
		}
		else
		$fieldtype = 'registration'; 
	
		if ($type == 'cart')
		{
			$fieldtype = 'cart';
		}
	
		
		$params =array('required' => true, 'delimiters' => true, 'captcha' => true, 'system' => false);  
		$ignore = array('delimiter_userinfo', 'name','username', 'address_type_name', 'address_type', 'user_is_vendor'); 
		
		$pwd_i = true; 
		$id = JFactory::getUser()->get('id'); 
		if (defined('VM_REGISTRATION_TYPE'))
		{
		if (VM_REGISTRATION_TYPE == 'NORMAL_REGISTRATION')
	    if (empty($id))
		{
			$pwd_i = false; 
		}
		if (VM_REGISTRATION_TYPE == 'OPTIONAL_REGISTRATION')
		{
			$register = JRequest::getVar('register_account', false); 
			if (!empty($register))
			{
				$pwd_i = false; 
			}
		}
		}
		
		if ($pwd_i)
		{
			$ignore[] = 'password'; 
			$ignore[] = 'password2'; 
			
		}
		
		$neededFields = $userFieldsModel->getUserFields(
		$fieldtype
		, $params
		, $ignore);

		
		
		$redirectMsg = false;
		$missinga = array(); 
		
		$i = 0 ;
        $missing = ''; 
		foreach ($neededFields as $field) {
			 $is_business = JRequest::getVar('opc_is_business', 0); 
	   
	    // we need to alter shopper group for business when set to: 
	     $is_business = JRequest::getVar('opc_is_business', 0); 
		 if (!empty($business_fields))
	     if (!$is_business)
		 {
		   // do not check if filled
		   if (in_array($field->name, $business_fields)) continue; 
		 }
		 if ($type=='ST')
		 if (!empty($shipping_obligatory_fields))
		 {
		   if (!in_array($field->name, $shipping_obligatory_fields)) continue; 
		 }
		   // manage required business fields when not business selected: 
		   //foreach ($business_fields as $fn)
	
	     if ($type == 'cart') $type = 'BT';
	
			if($field->required && empty($cart->{$type}[$field->name]) && $field->name != 'virtuemart_state_id'){
				$redirectMsg = OPCLang::sprintf('COM_VIRTUEMART_MISSING_VALUE_FOR_FIELD',OPCLang::_($field->title) );
				$i++;
				
				//more than four fields missing, this is not a normal error (should be catche by js anyway, so show the address again.
				if($type=='BT'){
				    $missinga[] = OPCLang::_($field->title); 
					$redirectMsg = OPCLang::_('COM_VIRTUEMART_CHECKOUT_PLEASE_ENTER_ADDRESS');
				}
			
			
			
			
			}

			if ($obj !== null && is_array($cart->{$type})) {
				$cart->{$type}[$field->name] = $obj->{$field->name};
			}

			//This is a special test for the virtuemart_state_id. There is the speciality that the virtuemart_state_id could be 0 but is valid.
			if ($field->name == 'virtuemart_state_id') {
				if (!class_exists('VirtueMartModelState')) require(JPATH_VM_ADMINISTRATOR .DIRECTORY_SEPARATOR. 'models' .DIRECTORY_SEPARATOR. 'state.php');
				if(!empty($cart->{$type}['virtuemart_country_id']) && !empty($cart->{$type}['virtuemart_state_id']) ){
					if (!$msg = VirtueMartModelState::testStateCountry($cart->{$type}['virtuemart_country_id'], $cart->{$type}['virtuemart_state_id'])) {
						
						$redirectMsg = $msg;
					}
				}

			}
		}
		
		if (empty($redirectMsg)) return false; 
		
		$missing = implode(', ', $missinga); 
		
		$redirectMsg .= ' '.$missing; 
		return $redirectMsg;
	}
	/**
	 * Set the last error that occured.
	 * This is used on error to pass back to the cart when addJS() is invoked.
	 * @param string $txt Error message
	 * @author Oscar van Eijk
	 */
	public function setError($txt) {
		$this->_lastError = $txt;
	}
	
	// generic method to get the new data from original cart
	public function __get($name)
	{
	  if (isset(self::$opc_cart->{$name}))
	  return self::$opc_cart->{$name}; 
	  
	  return null; 
	}
	
} 


