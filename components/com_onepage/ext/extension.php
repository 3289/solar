<?php
/**
* @package com_onepage
* @version 2
* @copyright Copyright (C) 2010 RuposTel s.r.o.. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page Checkout, Virtuemart and Joomla! is free software and parts of it may contain or be derived from the
* GNU General Public License or other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if (!defined('OPEXT'))
define('OPEXT', JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'ext');


//if (!class_exists('opExtension'))
{
class opExtension {
   var $exts; 
   var $opClass;
   var $opFile;
   var $opFunction;
   
   function __construct() {
     $this->exts = $this->getExt();
     $this->getParams($this->opFunction, $this->opFile, $this->opClass);
     
   }
   
   function parseParams($ext)
   {
     $path = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'ext'.DIRECTORY_SEPARATOR.$ext;
     $params = null;
     if (file_exists($path.DIRECTORY_SEPARATOR.'extension.config.php'))
     {
     $txt = file_get_contents($path.DIRECTORY_SEPARATOR.'extension.config.php');
     $params = vmParameters::parse($txt);
     }
     return $params;
     
   }
   
   function getParams(&$function, &$file, &$class)
   {
    
    /*
    if (!empty($function)) $this->opFunction = $function;
    if (!empty($file)) $this->opFile = $file;
    if (!empty($class)) $this->opClass = $class;
    */
    
    if (empty($function))
    if (!empty($this->opFunction)) $function = $this->opFunction;
    
    if (empty($class))
    if (!empty($this->opClass)) $class = $this->opClass;
    
    if (empty($file))
    if (!empty($this->opFile)) $file = $this->opFile;
    
    
    if ((!empty($this->opFunction) && (!empty($this->opClass)) && (!empty($this->opFile)))) return true;
    if ((!empty($function)) && (!empty($class)) && (!empty($file))) return true;
    if (!function_exists('debug_backtrace')) return false;
    
    $callstack = debug_backtrace();
    
    
    
    

     for ($i=0; $i<count($callstack); $i++)
     {
      if (!empty($callstack[$i]['function']))
      if (($callstack[$i]['function'] == 'include') || ($callstack[$i]['function'] == 'include_once'))
      if (!empty($callstack[$i]['args']))
      {
      
       if (strpos($callstack[$i]['args'][0], 'ext'.DIRECTORY_SEPARATOR.'extension.php')!==false)
       {
         $filep = pathinfo($callstack[$i]['file']);
         if (empty($file))
         $file = $filep['basename'];
       }
      }
      if (!empty($callstack[$i]['function']))
      if ($callstack[$i]['function'] == '__construct')
      if (!empty($callstack[$i]['class']) && ($callstack[$i]['class']=='opExtension'))
      if ($i<(count($callstack)-1)) 
      if (!empty($callstack[$i+1]['function']))
      {
       if (empty($function))
       $function = $callstack[$i+1]['function'];
       if (!empty($callstack[$i+1]['class']))
       if (empty($class))
        $class = $callstack[$i+1]['class'];
       if (empty($file))
         {
         $filep = pathinfo($callstack[$i]['file']);
         if ($filep['basename'] !== 'extension.php')
         $file = $filep['basename'];
         }
       
      }
      
      if (!empty($file) && (!empty($function)) && (!empty($class))) 
       {
        if (!empty($function)) $this->opFunction = $function;
        if (!empty($file)) $this->opFile = $file;
        if (!empty($class)) $this->opClass = $class;

        return true;
       }
     }
   
   	 if (empty($file)) 
   	  { 
   	  return false; 
   	  }
   	 if (!empty($function)) $this->opFunction = $function;
     if (!empty($file)) $this->opFile = $file;
     if (!empty($class)) $this->opClass = $class;

     return true;
   }

   function getDirectIncludes($function='', $file='', $class='')
   {
    
     if (!$this->getParams($function, $file, $class)) return false;
     $arr = array();
     foreach ($this->exts as $ext)
     {
      if (file_exists(OPEXT.DIRECTORY_SEPARATOR.$ext.DIRECTORY_SEPARATOR.$file.DIRECTORY_SEPARATOR.'include.php'))
      $arr[] = OPEXT.DIRECTORY_SEPARATOR.$ext.DIRECTORY_SEPARATOR.$file.DIRECTORY_SEPARATOR.'include.php';
     }
     return $arr;
   }

   
   function getIncludes($function='', $file='', $class='')
   {
     if (!$this->getParams($function, $file, $class)) return false;
     require(JPATH_SITE.DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'onepage'.DIRECTORY_SEPARATOR.'onepage.cfg.php');
     $arr = array();
     foreach ($this->exts as $k=>$ext)
     {
      if (file_exists(OPEXT.DIRECTORY_SEPARATOR.$ext.DIRECTORY_SEPARATOR.$file.DIRECTORY_SEPARATOR.'extension.php'))
      {
        $load = false;
        if (file_exists(OPEXT.DIRECTORY_SEPARATOR.$ext.DIRECTORY_SEPARATOR.'validate.php'))
        {
          require(OPEXT.DIRECTORY_SEPARATOR.$ext.DIRECTORY_SEPARATOR.'validate.php');
        }
      	if ($load)
        $arr[$k] = OPEXT.DIRECTORY_SEPARATOR.$ext.DIRECTORY_SEPARATOR.$file.DIRECTORY_SEPARATOR.'extension.php';
      }
     }
     return $arr;
   }
   
   function functionExists($function='', $file='', $class='')
   {
     if (empty($this->exts)) return false;

     
     
     if (!$this->getParams($function, $file, $class)) return false;
     
     $includes = $this->getIncludes($function, $file, $class); 
     


     if (!empty($includes))
     foreach ($includes as $k=>$incl)
     {
        
      	if (!empty($file))
     	{
      	 $a1 = explode('.', $file);
      	 
      	 $fClass = ucfirst($a1[0]);
      	}
      	else $fClass = '';

      	// example of class opExtFreeshippingBasket
     	 $eClass = 'opExt'.ucfirst($this->exts[$k]).$fClass;
     	 $params = $this->parseParams($this->exts[$k]); 

     	 if (!class_exists($eClass))
         include_once($incl);
         if (class_exists($eClass))
          { 
            $rc = new ReflectionClass($eClass);
            if ($rc->hasMethod($function)) {
				return true;
            }
 
          }
      }
      return false;
   
   }
   
   // return false on a problem
   // class is a class suffix
   // normally each file has a class name such as opExtPaypal (where paypal is the name of the extension)
   // if the class has to be loaded more then once, you can add suffix to the calling function
   function runExt($function='', $file='', $class='', &$param1 = null, &$param2 = null,&$param3= null,&$param4= null,&$param5= null,&$param6= null,&$param7= null, &$param8=null )
   {
		
     // suffix
	 $classOrig = $class;   
     if (empty($this->exts)) return false;
    
	    
     if (!$this->getParams($function, $file, $class)) return false;
 
     $includes = $this->getIncludes($function, $file, $class); 
     
     $ret = '';


		


     if (!empty($includes))
     foreach ($includes as $k=>$incl)
     {
//     	 if (empty($classOrig))
     	 $eClass = 'opExt'.ucfirst($this->exts[$k]);
//     	 else $eClass = 'opExt'.ucfirst($this->exts[$k]).'_'.$classOrig;
        if (!empty($file))
     	{
      	 $a1 = explode('.', $file);
      	 
      	 $fClass = ucfirst($a1[0]);
      	}
      	else $fClass = '';

     	 $eClass = $eClass.$fClass;
     	 
     	 $params = $this->parseParams($this->exts[$k]); 
     	 
      //echo $eClass.'<br />';
     	 
     	 if (!class_exists($eClass))
         include_once($incl);
         if (class_exists($eClass))
          { 
            $rc = new ReflectionClass($eClass);
            if ($rc->hasMethod($function)) {
              $x = new $eClass;
              $retVal = $x->{$function}($params, $param1,$param2,$param3,$param4,$param5,$param6,$param7, $param8 );
              if (isset($retVal))
              if (is_bool($retVal)) 
              {
                if ($ret === '') $ret = $retVal;
                elseif (is_bool($ret)) 
                 {
                  if ($ret && $retVal) $ret = true;
                  else $ret = false;
                 }
              }
              else
              if (is_string($retVal))
              {
                $ret .= $retVal;
              }
              if (is_array($retVal))
              {
                if ($ret === '') $ret = $retVal;
                elseif (is_array($ret)) $ret = array_merge($ret, $retVal); 
              }
            }
            else
            {
              
              /*
				echo $eClass; 
				die();
			  */
            }
 
          }
      }
      
      if ($ret !== '')
      return $ret;
      
      return false;
    }
       
     
  
  
  	function getExt()
		{
		 $dir = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'ext';
		 $arr = @scandir($dir);
		 $ret = array();
		 if (!empty($arr))
		 {
		  foreach ($arr as $file)
		  {
		   if (is_dir($dir.DIRECTORY_SEPARATOR.$file) && ($file != '.') && ($file != '..') && (file_exists($dir.DIRECTORY_SEPARATOR.$file.DIRECTORY_SEPARATOR.'enabled.html'))) $ret[] = $file;
		  }
		 }
		 return $ret;
		}
}
}

//if (!empty($_GLOBALS['opExt']))
{
  $op_x = new opExtension();
  $op_a = $op_x->getDirectIncludes($op_x->opFunction, $op_x->opFile, $op_x->opClass);
 
  
  if (!empty($op_a))
  foreach ($op_a as $x)
  {
    
    include ($x);
  }
}



