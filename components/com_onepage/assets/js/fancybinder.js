function bindFancyBox(el)
{
	// jquery must be loaded: 
	if (typeof el.data == 'undefined') return; 
	
	if (!el.data("fancybox")) {
	el.data("fancybox", true);
	
	// fancybox must be loaded: 
	if (typeof el.fancybox == 'undefined') return; 
	
	var params = {
	 href: el.attr('href'),
	 type: 'iframe',
	 autoDimensions: true,
	 overlayShow: true
	};
	el.data('rel', el.rel); 
	var rr = {}; 
	
	var relData = el.attr('rel'); 
	if (relData != null)
	{
	
	  try {
		var str = 'var rr = '+relData+';';
		eval(str); 
		
		if (rr != null)
		{
			if (typeof rr.size != 'undefined')
			if (typeof rr.size.x != 'undefined')
			{
			  params.width = rr.size.x; 	
			}
			if (typeof rr.size != 'undefined')
			if (typeof rr.size.y != 'undefined')
			{
			  params.height = rr.size.y; 	
			}
		}
		
	  }
	  catch (e) { 
	 
	  }
	
	}	
	
	
	
	el.attr('rel', ''); 
	el.fancybox(params); 
	}
	return false; 
}