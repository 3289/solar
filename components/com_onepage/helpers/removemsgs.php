<?php
/*
*
* @copyright Copyright (C) 2007 - 2012 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
*/

if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 

class OPCremoveMsgs {

 public static function removeMsgs($cart)
  {
      $x = JFactory::getApplication()->getMessageQueue(); 
	  
	  $msg = JFactory::getSession()->get('application.queue', array());; 
	  $msgq1 = JFactory::getApplication()->get('messageQueue', array()); 
	  $msgq2 = JFactory::getApplication()->get('_messageQueue', array()); 
	  
	  $x = array_merge($x, $msg); 
	  $x = array_merge($x, $msgq1); 
	  $x = array_merge($x, $msgq2); 
	  
	  
	  
	   $arr = array(); 
	   $disablarray = array( 'Unrecognised mathop', JText::_('COM_VIRTUEMART_CART_PLEASE_ACCEPT_TOS')); 
	   
	   include(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'third_party'.DIRECTORY_SEPARATOR.'third_party_disable_msgs.php'); 
	   
	      $euvat_text = array('VMUSERFIELD_ISTRAXX_EUVATCHECKER_INVALID', 'VMUSERFIELD_ISTRAXX_EUVATCHECKER_VALID', 'VMUSERFIELD_ISTRAXX_EUVATCHECKER_INVALID_COUNTRYCODE', 'VMUSERFIELD_ISTRAXX_EUVATCHECKER_INVALID_FORMAT_REASON', 'VMUSERFIELD_ISTRAXX_EUVATCHECKER_INVALID_FORMAT', 'VMUSERFIELD_ISTRAXX_EUVATCHECKER_SERVICE_UNAVAILABLE', 
		  'VMUSERFIELD_ISTRAXX_EUVATCHECKER_COMPANYNAME_REQUIRED'); 
	   
	   foreach ($euvat_text as $k=>$t)
	   {
	   $tt = JText::_($t); 
	   $euvat_text[$k] = substr($tt, 0, 20); 
	   
	   }
	   $euvatinfo = ''; 
	   
	   			 
		//missing fields: 
		$t1 = JText::_('COM_VIRTUEMART_MISSING_VALUE_FOR_FIELD'); 
		$a = explode('%s', $t1); 
		
		
	   
	   $remove = array(); 
	   foreach ($x as $key=>$val)
	    {
		
if (!is_array($val)) continue; 
if (!isset($val['message'])) continue; 

// removes missing value for...
$f1 = false; 
if (!empty($a[0]))
{
if (stripos($val['message'], $a[0]) !== false)
$f1 = true; 
}
else $f1 = true; 

if (!empty($a[1]))
{
if (stripos($val['message'], $a[1]) !== false)
$f2 = true; 
}
else $f2 = true; 

if ($f1 && $f2) 
{
$remove[] = $key; 

}
// end,... removes missing value for...



		
		  
		     foreach ($euvat_text as $kx => $eutext)
			 {
			 // echo 'comparing '.$eutext.' with '.$val['message']."<br />\n"; 
			 if (stripos($val['message'], $eutext)!==false)
			   {

			     $euvatinfo .= $val['message']; 
			     $remove[] = $key; 
				 break;
			   }
			 }
			 
		  
		  foreach ($disablarray as $msg)
		  {
		  
		     
			  if (stripos($val['message'], $msg)!==false)
			  {
				  $remove[] = $key; 
			  }
			  if (stripos($val['message'], JText::_('COM_VIRTUEMART_COUPON_CODE_INVALID'))!==false)
			  {
				  $cart->couponCode = ''; 
				  $cart->setCartIntoSession();

			  }
		  }
		  
		}
		
		
		foreach ($x as $key=>$val)
		{
			if (!in_array($key, $remove))
			{
			  $arr[] = $val; 		
			}
		}
		
	    
		if (!empty($remove))
		if (OPCJ3)
		if (class_exists('ReflectionClass'))
		{
		   $a = JFactory::getApplication(); 
		   $reflectionClass = new ReflectionClass($a);
		   $property = $reflectionClass->getProperty('_messageQueue'); 
		   $property->setAccessible(true);
		 
		   $property->setValue($a, $arr);
		   $x = JFactory::getApplication()->getMessageQueue(); 
		   
		}
	   
	   
	  // this works under j25 only: 
	  if (!empty($remove))
	  {
	   JFactory::getApplication()->set('messageQueue', $arr); 
	   JFactory::getApplication()->set('_messageQueue', $arr); 
	   JFactory::getSession()->set('application.queue', $arr);   
	  }
	  
  }
}
