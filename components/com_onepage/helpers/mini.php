<?php

/*
*
* @copyright Copyright (C) 2007 - 2013 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
*/

if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
class OPCmini
{
 function loadJSfile($file)
 {
   jimport('joomla.filesystem.file');
   $file = JFile::makeSafe($file); 
   $pa = pathinfo($file); 
   $fullpath = JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.$file; 
   if (!empty($pa['extension']))
   if ($pa['extension']=='js')
    {
	 //http://php.net/manual/en/function.header.php 
	if(strstr($_SERVER["HTTP_USER_AGENT"],"MSIE")==false) {
		@header("Content-type: text/javascript");
		@header("Content-Disposition: inline; filename=\"".$file."\"");
		//@header("Content-Length: ".filesize($fullpath));
	} else {
		@header("Content-type: application/force-download");
		@header("Content-Disposition: attachment; filename=\"".$file."\"");
		//@header("Content-Length: ".filesize($fullpath));
	}
	@header("Expires: Fri, 01 Jan 2010 05:00:00 GMT");
	if(strstr($_SERVER["HTTP_USER_AGENT"],"MSIE")==false) {
	@header("Cache-Control: no-cache");
	@header("Pragma: no-cache");
    }
	//include(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'js'.DIRECTORY_SEPARATOR.$file);
	echo file_get_contents($fullpath); 
	$doc = JFactory::getApplication(); 
	$doc->close(); 
    die(); 

	}
	
	
 }
    public static $selected_template; 
	public static function getSelectedTemplate()
	{
	 
	 if (!empty(self::$selected_template)) 
	 {
		 
		 return self::$selected_template; 
	 }
	 require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'loader.php');  
	 $selected = JRequest::getVar('opc_theme', null); 
	  {
	    if (!empty($selected))
		 {
			 
		    jimport( 'joomla.filesystem.file' );
		    $selected = JFile::makeSafe($selected); 
			$dir = JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'themes'.DIRECTORY_SEPARATOR.$selected;
			
			if (file_exists($dir) && ($selected != 'extra'))
			{
			
		     self::$selected_template = $selected; 
			 if (!defined('OPC_DETECTED_DEVICE'))
			 define('OPC_DETECTED_DEVICE', 'DESKTOP'); 
			 return self::$selected_template; 
			}
		 }
		 else
		 {
			
		 }
	  }
	 
	 if (!defined('OPC_DETECTED_DEVICE'))
	 {
	  require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'plugin.php'); 
	  OPCplugin::detectMobile(); 
	 }
	 
	 require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'loader.php'); 
	 
	 include(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'onepage.cfg.php');  
	 
	 if (!empty($selected_template) && ($selected_template != 'extra'))
     self::$selected_template = $selected_template; 
	 
	 if (defined('OPC_DETECTED_DEVICE') && (OPC_DETECTED_DEVICE != 'DESKTOP'))
     if (!empty($mobile_template)) self::$selected_template = $mobile_template; 	 
 
 
	 if (OPCloader::checkOPCSecret())
     {
	  if (!empty($selected_template) && ($selected_template != 'extra'))
	  self::$selected_template .= '_preview'; 
     }
	 
	 

	 
	 return self::$selected_template; 
	}
 public static function isSuperVendor(){

	if ((!defined('VM_VERSION')) || (VM_VERSION < 3))
			{
			if (!class_exists('Permissions'))
			require(JPATH_VM_ADMINISTRATOR .DIRECTORY_SEPARATOR. 'helpers' .DIRECTORY_SEPARATOR. 'permissions.php');
			if (Permissions::getInstance()->check("admin,storeadmin")) {
				return true; 
				
			}
			}
			else
			{
			 $text = '';
			$user = JFactory::getUser();
			if($user->authorise('core.admin','com_virtuemart') or $user->authorise('core.manage','com_virtuemart') or VmConfig::isSuperVendor()) {
			  return true; 
			}
			}
			
			return false; 
	}
 
   public static $cache; 
   static function clearTableExistsCache()
   {
    self::$cache = array(); 
   }
   
   // -1 for a DB error, true for has index and false for does not have index
   public static function hasIndex($table, $column)
   {
	   
	   
	    $db = JFactory::getDBO(); 
		$prefix = $db->getPrefix();
	    $table = str_replace('#__', '', $table); 
		$table = str_replace($prefix, '', $table); 
		$table = $db->getPrefix().$table; 
	    if (!self::tableExists($table)) return -1; 
	    $q = "SHOW INDEX FROM `".$table."`"; 
		try
		{
		 $db->setQuery($q); 
		 $r = $db->loadAssocList(); 
		}
		catch (Exception $e)
		{
			return -1; 
		}
		
		if (empty($r)) return false; 
		
		foreach ($r as $k=>$row)
		{
		foreach ($row as $kn=>$data)
		{
			$kk = strtolower($kn); 
			if (($kk === 'key_name') || ($kk === 'column_name'))
			{
				$dt = strtolower($data); 
				$c = strtolower($column); 
				if ($dt === $c) return true; 
				if ($dt === $c.'_index') return true; 
			}
		}
		}
		
		return false; 
	   
   }
   
   static function addIndex($table, $cols=array())
   {
	   if (empty($cols)) return; 
	   $db = JFactory::getDBO(); 
		$prefix = $db->getPrefix();
	    $table = str_replace('#__', '', $table); 
		$table = str_replace($prefix, '', $table); 
		$table = $db->getPrefix().$table; 
		if (!self::tableExists($table)) return; 
		
		$name = reset($cols); 
		$name .= '_index'; 
		foreach ($cols as $k=>$v)
		{
			if (!is_numeric($k)) { $name = $k; }
			$cols[$k] = '`'.$db->escape($v).'`'; 
		}
		$cols = implode(', ', $cols); 
		
		$q = "ALTER TABLE  `".$table."` ADD INDEX  `".$db->escape($name)."` (  ".$cols." ) "; 
		try {
		 $db->setQuery($q); 
		 $db->query(); 
		}
		catch (Exception $e)
		{
		
		}
   }
   
   static function tableExists($table)
  {
   
   
   $db = JFactory::getDBO();
   $prefix = $db->getPrefix();
   $table = str_replace('#__', '', $table); 
   $table = str_replace($prefix, '', $table); 
   $table = $db->getPrefix().$table; 
   
   
   
   if (isset(self::$cache[$table])) return self::$cache[$table]; 
   
   $q = 'select * from '.$table.' where 1 limit 0,1';
   // stAn, it's much faster to do a positive select then to do a show tables like...
    /*
	if(version_compare(JVERSION,'3.0.0','ge')) 
	{
	try
    {
		$db->setQuery($q); 
		$res = $db->loadResult(); 
		
		if (!empty($res))
		{
			self::$cache[$table] = true; 
			return true;
		}
		$er = $db->getErrorMsg(); 
		if (empty($er))
		{
			self::$cache[$table] = true; 
			return true;
		}
		
		
    } catch (Exception $e)
	{
		  $e = (string)$e; 
	}
	}
    */	
   
   $q = "SHOW TABLES LIKE '".$table."'";
	   $db->setQuery($q);
	   $r = $db->loadResult();
	   
	   if (empty(self::$cache)) self::$cache = array(); 
	   
	   if (!empty($r)) 
	    {
		self::$cache[$table] = true; 
		return true;
		}
		self::$cache[$table] = false; 
   return false;
  }

     // moved from opc loaders so we do not load loader when not needed
	static $modelCache; 
   	public static function getModel($model)
	 {
	 
	 // make sure VM is loaded:
	 if (!class_exists('VmConfig'))	  
	 {
	  require(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
	  VmConfig::loadConfig(); 
	 }
		if (empty(OPCmini::$modelCache)) OPCmini::$modelCache = array(); 
	    if (!empty(OPCmini::$modelCache[$model])) return OPCmini::$modelCache[$model]; 
		
		
	    if (!class_exists('VirtueMartModel'.ucfirst($model)))
		require(JPATH_VM_ADMINISTRATOR .DIRECTORY_SEPARATOR. 'models' .DIRECTORY_SEPARATOR. strtolower($model).'.php');
		if ((method_exists('VmModel', 'getModel')))
		{
		
		$view = JRequest::getWord('view','virtuemart');
		
		$resetview = false; 
		if (empty($view))
		{
			$view = JRequest::setVar('view','virtuemart');
			$resetview = true; 
		}
		
		$Omodel = VmModel::getModel($model); 
		
		if ($resetview)
		{
			$view = JRequest::setVar('view','');
		}
		
		OPCmini::$modelCache[$model] = $Omodel; 
		return $Omodel; 
		}
		else
		{
			// this section loads models for VM2.0.0 to VM2.0.4
		   $class = 'VirtueMartModel'.ucfirst($model); 
		   if (class_exists($class))
		    {
				
				if ($class == 'VirtueMartModelUser')
				{
				
				//require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'overrides'.DIRECTORY_SEPARATOR.'user.php'); 
				//$class .= 'Override'; 
				
				 $Omodel = new VirtueMartModelUser; 
				 
				 return $Omodel; 
				 $Omodel->setMainTable('virtuemart_vmusers');
				 
				}
				
				
			    $Omodel = new $class(); 
				
			  OPCmini::$modelCache[$model] = $Omodel; 
			  return $Omodel; 
			}
			else
			{  
			  echo 'Class not found: '.$class; 
			  $app = JFactory::getApplication()->close(); 
			}
			
		}
		echo 'Model not found: '.$model; 
		$app = JFactory::getApplication()->close(); 
		
		//return new ${'VirtueMartModel'.ucfirst($model)}(); 
	 
	 }	
	 
	 public static function slash($string, $insingle = true)
	 {
	    $string = str_replace("\r\r\n", " ", $string); 
   $string = str_replace("\r\n", " ", $string); 
   $string = str_replace("\n", " ", $string); 
   $string = (string)$string; 
   if ($insingle)
    {
	 $string = addslashes($string); 
     $string = str_replace('/"', '"', $string); 
	 return $string; 
	}
	else
	{
	  $string = addslashes($string); 
	  $string = str_replace("/'", "'", $string); 
	  return $string; 
	}
	 
	 }


 
}