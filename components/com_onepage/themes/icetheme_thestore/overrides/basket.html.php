<?php
if( !defined( '_VALID_MOS' ) && !defined( '_JEXEC' ) ) die( 'Direct Access to '.basename(__FILE__).' is not allowed.' ); 
/**
* This is the default Basket Template. Modify as you like.
*
* @version $Id: basket_b2c.html.php 1377 2008-04-19 17:54:45Z gregdev $
* @package VirtueMart
* @subpackage templates
* @copyright Copyright (C) 2004-2005 Soeren Eberhardt. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
*
* http://virtuemart.net
*/
?>
<div id="basket_container">
<div class="inside">
<div class="black-basket">
		
         
            <div class="basket-header">                                      
           
                                                                    
                            <h3>
                             <span><?php echo OPCLang::_('COM_VIRTUEMART_CART_TITLE'); ?></span>
                               
                        	</h3>
                                                 
        	             
                         <div>
<div class="onetable-cart">                         

  
<?php 
foreach( $product_rows as $product ) { 

/*
DEVELOPER INFORMATION
If you need any other specific information about the product being showed in the basket you can use the following variables in the theme: 
$product['info'] is an instance of VirtueMartModelProduct->getProduct($product['product_id'], $front=true, $calc=false, $onlypublished=false);

To get instance of the single product information associated with the cart without any extra info, you can use: 
$product['product']

All of the variables used in this file are defined in: 
\components\com_onepage\helpers\loader.php
Please don't modify loader.php if you plan to update OPC on bug fix releases. 

Tested Example to show manufacturer info: 


if (!empty($product['info']->virtuemart_manufacturer_id))
{
echo $product['info']->mf_name; 
}
*/

?>

<div class="basket-row">
    <div class="basket-rowin">
    <div class="basket-image">
        <?php 
		if($product['product']->file_url){
			echo '<img src="'.$product['product']->file_url.'" alt="'.$product['product']->product_name.'" />';		
		}else{
			echo '<img src="/images/stories/virtuemart/product/200x200/noimage.png" alt="'.$product['product']->product_name.'" />';	
		} ?>
    </div>
    
    <div class="basket-info">
             <div class="basket-name"><?php echo $product['product_name'] . $product['product_attributes'] ?></div>
             
             <?php if($product['product_sku']){?>
                  <div class="basket-sku"><?php echo OPCLang::_('COM_VIRTUEMART_PRODUCT_SKU'); ?>: <?php echo $product['product_sku'] ?></div>
             <?php }?>
             
             <div class="basket-price"><?php echo $product['product_price'] ?></div>
    </div>
     <?php echo $product['update_form'] ?>
            <?php //echo $product['delete_form']; 
    ?>
    <div class="basket-total">
         <div class="subtotal-label"><?php echo OPCLang::_('COM_VIRTUEMART_CART_SUBTOTAL') ?></div>
         <div class="subtotal-price"><?php echo $product['subtotal'] ?></div>
    </div>
    <div class="clr"></div>
    </div>
</div>
 
<?php } ?>
</div>

 
  

  <?php 
  
  if (!empty($continue_link)) { ?>
  <div class="op_basket_row">
    <div style="width: 100%; clear: both;">
  		 <a href="<?php echo $continue_link ?>" class="continue_link_ice" ><span>
		 	<?php echo OPCLang::_('COM_VIRTUEMART_CONTINUE_SHOPPING'); ?></span>
		 </a>
	&nbsp;</div>
  </div>
  <?php } ?>


                         </div>
           </div>
           
</div>
</div>
</div>
<script type="text/javascript">
 jQuery('.basket-rowin').each(function(index, element) {
    jQuery(this).find('.b-row').height(jQuery(this).height());
});
</script>