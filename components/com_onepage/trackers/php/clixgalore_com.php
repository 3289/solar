<?php
/*
*
* @copyright Copyright (C) 2007 - 2013 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
* stAn note: Always use default headers for your php files, so they cannot be executed outside joomla security 
*
*/

defined( '_JEXEC' ) or die( 'Restricted access' );
$order_total = $this->order['details']['BT']->order_total;
$order_total_txt = number_format($order_total, 2, '.', ''); 
$this->params->idformat = (int)$this->params->idformat; 

$idformat = $this->order['details']['BT']->virtuemart_order_id; 
 

if ($this->params->idformat===1)
{
  $idformat = $this->order['details']['BT']->virtuemart_order_id.'_'.$this->order['details']['BT']->order_number;
}
else
if ($this->params->idformat===2)
 {
   $idformat = $this->order['details']['BT']->order_number; 
 }

?><img src="https://www.clixGalore.com/AdvTransaction.aspx?SV=<?php echo $order_total_txt; ?>&OID=<?php echo $idformat; ?>&AdID=<?php echo $this->params->addId; ?>" width="1" height="1" />