<?php
/*
*
* @copyright Copyright (C) 2007 - 2013 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
* stAn note: Always use default headers for your php files, so they cannot be executed outside joomla security 
*
*/

defined( '_JEXEC' ) or die( 'Restricted access' );
// to be used as described at https://support.google.com/tagmanager/answer/3002580?hl=en
if (!empty($this->params->adwords_remarketing))
{
	
$products_tags = array(); 
$this->params->pidformat = (int)$this->params->pidformat; 
$total = 0; 
foreach ($this->products as $key=>$product) { 

/*

switch ($this->params->pidformat)
{
  case 0: 
   $pid = $product->virtuemart_product_id; 
   break;
  case 1: 
    $pid = $product->product_sku; 
	if (empty($pid))
    $pid = $product->virtuemart_product_id; 
	break; 
  case 2: 
    $pid = $product->product_sku; 
	if (empty($pid))
    $pid = $product->virtuemart_product_id; 
    $lang = $this->order['details']['BT']->order_language; 
	if (!empty($lang) && (stripos($lang, '-')!==false))
	 {
	   $a = explode('-', $lang); 
	   $pid = $pid.'-'.$a[1]; 
	 }
	 else
	 {
	   $tag = JFactory::getLanguage()->getTag(); 
	   $a = explode('-', $tag); 
	   if (!empty($a))
	   $pid = $pid.'-'.$a[1]; 
	 }
	 break; 
	 
}
*/

$pid = $this->getPID($product->virtuemart_product_id, $product->product_sku); 

$products_tags[] = "'".$this->escapeSingle($pid)."'"; 
$total += ($product->product_final_price * $product->product_quantity); 


}




?><script>
//<![CDATA[ 

if (typeof dataLayer != 'undefined')
dataLayer.push({
'event': '<?php echo $this->escapeSingle($this->params->tag_event); ?>',
    'google_tag_params': {
      'ecomm_prodid': <?php 
	  $pr = implode(',', $products_tags); 
	  echo '['.$pr.']'; 
	  // will show products like ['1', '2'] .... 
	  ?>,
      'ecomm_pagetype': 'cart',
      'ecomm_totalvalue': "<?php echo number_format($total, 2, '.', ''); ?>"
	}
}); 



    if ((typeof console != 'undefined')  && (typeof console.log != 'undefined')  &&  (console.log != null))
	  {
	     console.log('OPC Tracking: google_tag_manager remarketing cart event, triggered for <?php echo $this->escapeSingle($this->params->tag_event); ?>'); 
	  }
	
//]]>	
</script>


<?php 
}


