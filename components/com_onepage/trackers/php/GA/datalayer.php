<?php
/*
*
* @copyright Copyright (C) 2007 - 2013 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
* stAn note: Always use default headers for your php files, so they cannot be executed outside joomla security 
*
*/

defined( '_JEXEC' ) or die( 'Restricted access' );
?>


<?php 
if (!empty($this->params->adwords_remarketing))
{
$products_tags = array(); 
foreach ($this->order['items'] as $key=>$order_item) { 
/*
$this->params->pidformat = (int)$this->params->pidformat; 

switch ($this->params->pidformat)
{
  case 0: 
   $pid = $order_item->virtuemart_product_id; 
   break;
  case 2: 
    $pid = $order_item->product_sku; 
	if (empty($pid))
    $pid = $order_item->virtuemart_product_id; 
	break; 
  case 3: 
    $pid = $order_item->product_sku; 
	if (empty($pid))
    $pid = $order_item->virtuemart_product_id; 
    $lang = $this->order['details']['BT']->order_language; 
	if (!empty($lang) && (stripos($lang, '-')!==false))
	 {
	   $a = explode('-', $lang); 
	   $pid = $pid.'-'.$a[1]; 
	 }
	 else
	 {
	   $tag = JFactory::getLanguage()->getTag(); 
	   $a = explode('-', $tag); 
	   if (!empty($a))
	   {
			$pid = $pid.'-'.$a[1]; 
	   }
	 }
	 break; 
	 
}
*/

$pid = $this->getPID($order_item->virtuemart_product_id, $order_item->product_sku); 

$products_tags[] = "'".$this->escapeSingle($pid)."'"; 
?>


		

<?php } ?>


if (typeof dataLayer != 'undefined')
dataLayer.push({
    'event': '<?php echo $this->escapeSingle($this->params->tag_event); ?>',
    'google_tag_params': {
      'ecomm_prodid': <?php 
	  $pr = implode(',', $products_tags); 
	  echo '['.$pr.']'; 
	  // will show products like ['1', '2'] .... 
	  ?>,
      'ecomm_pagetype': 'purchase',
      'ecomm_totalvalue': "<?php echo number_format($order_total, 2, '.', ''); ?>"
    }
   });

    if ((typeof console != 'undefined')  && (typeof console.log != 'undefined')  &&  (console.log != null))
	  {
	     console.log('OPC Tracking: Adwords ecommerce datalayer added and triggered for <?php echo $this->escapeSingle($this->params->tag_event); ?>'); 
	  }
   
<?php
// GA remarketing tag end
}


$this->params->idformat = (int)$this->params->idformat; 
$idformat = $this->order['details']['BT']->virtuemart_order_id; 
 

if ($this->params->idformat===1)
{
  $idformat = $this->order['details']['BT']->virtuemart_order_id.'_'.$this->order['details']['BT']->order_number;
}
else
if ($this->params->idformat===2)
 {
   $idformat = $this->order['details']['BT']->order_number; 
 }


?>
// Send transaction data with a pageview if available
// when the page loads. Otherwise, use an event when the transaction
// data becomes available.
if (typeof dataLayer != 'undefined')
dataLayer.push({
  'ecommerce': {
    'purchase': {
      'actionField': {
        'id': '<?php echo $this->escapeSingle($idformat); ?>',                         // Transaction ID. Required for purchases and refunds.
        'affiliation':  "<?php echo $this->escapeDouble($this->vendor['company']); ?>",
        'revenue': <?php echo number_format($order_total, 2, '.', ''); ?>,                     // Total transaction value (incl. tax and shipping)
        'tax': <?php echo number_format($this->order['details']['BT']->order_tax, 2, '.', ''); ?>,
        'shipping': <?php echo number_format($this->order['details']['BT']->order_shipment, 2, '.', ''); ?>,
        'coupon': '<?php if (!empty($this->order['details']['BT']->coupon_code)) echo $this->escapeSingle($this->order['details']['BT']->coupon_code); ?>'
      },
      'products': [<?php 
	  
	  $max = count($this->order['items']); 
	  $i = 0; 
	  foreach ($this->order['items'] as $key=>$order_item) 
	  { 
	  
	  //pidformat
	  
$this->params->pidformat = (int)$this->params->pidformat; 

switch ($this->params->pidformat)
{
  case 0: 
   $product_id = $order_item->virtuemart_product_id; 
   break;
  case 2: 
    $product_id = $order_item->product_sku; 
	if (empty($product_id))
    $product_id = $order_item->virtuemart_product_id; 
	break; 
  case 3: 
    $product_id = $order_item->product_sku; 
	if (empty($product_id))
    $product_id = $order_item->virtuemart_product_id; 
    $lang = $this->order['details']['BT']->order_language; 
	if (!empty($lang) && (stripos($lang, '-')!==false))
	 {
	   $a = explode('-', $lang); 
	   $product_id = $product_id.'-'.$a[1]; 
	 }
	 else
	 {
	   $tag = JFactory::getLanguage()->getTag(); 
	   $a = explode('-', $tag); 
	   if (!empty($a))
	   $product_id = $product_id.'-'.$a[1]; 
	 }
	 break; 
	 
}
	  
	  
	  $i++; 
	  
	  if (empty($order_item->category_name)) $order_item->category_name = ''; 
	  if (!empty($order_item->virtuemart_category_name)) $order_item->category_name = $order_item->virtuemart_category_name; 
	  
	  ?>{                            // List of productFieldObjects.
        'name': "<?php echo $this->escapeDouble($order_item->order_item_name); ?>",     // Name or ID is required.
        'id': "<?php echo $this->escapeDouble($product_id); ?>",
        'price': "<?php echo number_format($order_item->product_final_price, 2, '.', ''); ?>",
        'brand': '',
        'category': "<?php echo $this->escapeDouble($order_item->category_name ); ?>",
        'variant': "<?php echo $this->escapeDouble($order_item->order_item_sku); ?>",
        'quantity': <?php echo number_format($order_item->product_quantity , 0, '.', ''); ?>,
        'coupon':  '<?php if (!empty($this->order['details']['BT']->coupon_code)) echo $this->escapeSingle($this->order['details']['BT']->coupon_code); ?>'                            // Optional fields may be omitted or set to empty string.
       }<?php if ($i !== $max) echo ','; ?>
   <?php } ?>]
    }
  }
});

    if ((typeof console != 'undefined')  && (typeof console.log != 'undefined')  &&  (console.log != null))
	  {
	     console.log('OPC Tracking google_tag_manager: Purchase datalayer event added.'); 
	  }

