<?php
/*
*
* @copyright Copyright (C) 2007 - 2013 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
* stAn note: Always use default headers for your php files, so they cannot be executed outside joomla security 
*
*/
// per specs from https://developers.google.com/tag-manager/enhanced-ecommerce#details
defined( '_JEXEC' ) or die( 'Restricted access' );


/*
$this->params->pidformat = (int)$this->params->pidformat; 



switch ($this->params->pidformat)
{
  case 0: 
   $pid = $product->virtuemart_product_id; 
   break;
  case 1: 
    $pid = $product->product_sku; 
	if (empty($pid))
    $pid = $product->virtuemart_product_id; 
	break; 
  case 2: 
    $pid = $product->product_sku; 
	if (empty($pid))
    $pid = $product->virtuemart_product_id; 
    $lang = $this->order['details']['BT']->order_language; 
	if (!empty($lang) && (stripos($lang, '-')!==false))
	 {
	   $a = explode('-', $lang); 
	   $pid = $pid.'-'.$a[1]; 
	 }
	 else
	 {
	   $tag = JFactory::getLanguage()->getTag(); 
	   $a = explode('-', $tag); 
	   if (!empty($a))
	   $pid = $pid.'-'.$a[1]; 
	 }
	 break; 
	 
}
*/

$pid = $this->getPID($product->virtuemart_product_id, $product->product_sku); 


?>
<script>
// Measure a view of product details. This example assumes the detail view occurs on pageload,
// and also tracks a standard pageview of the details page.
if (typeof dataLayer != 'undefined')
dataLayer.push({
  'ecommerce': {
    'detail': {
      'actionField': {'list': 'OPC: Product Details Impression'},    // 'detail' actions have an optional list property.
      'products': [{
        'name': '<?php echo $this->escapeSingle($product->product_name); ?>',         // Name or ID is required.
        'id': '<?php echo $this->escapeSingle($pid); ?>',
        'price': '<?php echo number_format($product->prices['salesPrice'], 2, '.', ''); ?>',
        'brand': '<?php if (!empty($product->mf_name )) echo $this->escapeSingle($product->mf_name ); ?>',
        'category': '<?php echo $this->escapeSingle($product->category_name); ?>',
        'variant': '<?php echo $this->escapeSingle($product->product_sku); ?>'
       }]
     }
   }
});

    if ((typeof console != 'undefined')  && (typeof console.log != 'undefined')  &&  (console.log != null))
	  {
	     console.log('OPC Tracking google_tag_manager: Datalayer product page view added.'); 
	  }

<?php
if (!empty($this->params->adwords_remarketing)) {
?>
 
 var google_tag_params = {
  'ecomm_pagetype': 'product',       //set the pagetype value
  'ecomm_pcat': ['<?php echo $this->escapeSingle($product->category_name); ?>'],   // product category
  'ecomm_prodid': ['<?php echo $this->escapeSingle($pid); ?>'],      // sku
  'ecomm_pname': ['<?php echo $this->escapeSingle($product->product_name); ?>'],    // product name 
  'ecomm_pvalue': [<?php echo number_format($product->prices['salesPrice'], 2, '.', ''); ?>],      // product value
  'ecomm_totalvalue': '<?php echo number_format($product->prices['salesPrice'], 2, '.', ''); ?>'      // total value
};

if (typeof dataLayer != 'undefined')
dataLayer.push({
    'event': '<?php echo $this->escapeSingle($this->params->tag_event); ?>',
    'google_tag_params': google_tag_params
   });
   
   
    if ((typeof console != 'undefined')  && (typeof console.log != 'undefined')  &&  (console.log != null))
	  {
	     console.log('OPC Tracking google_tag_manager: Fired remarketing tag for product pagetype: product, event: <?php echo $this->escapeSingle($this->params->tag_event); ?>'); 
	  }
<?php
}
?>   

</script>