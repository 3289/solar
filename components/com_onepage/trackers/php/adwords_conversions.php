<?php
/*
*
* @copyright Copyright (C) 2007 - 2012 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
* stAn note: Always use default headers for your php files, so they cannot be executed outside joomla security 
*
*/

defined( '_JEXEC' ) or die( 'Restricted access' );
$order_total = $this->order['details']['BT']->order_total;
$order_total = number_format($order_total, 2, '.', ''); 

?>


<script type="text/javascript">
/* <![CDATA[ */
var google_tag_params = {
ecomm_prodid: [], 
ecomm_pagetype: 'purchase',
ecomm_totalvalue: '<?php echo $order_total; ?>',
};

<?php foreach ($this->order['items'] as $key=>$order_item) { 

$this->params->pidformat = (int)$this->params->pidformat; 
/*
switch ($this->params->pidformat)
{
  case 0: 
   $id = $order_item->virtuemart_product_id; 
   break;
  case 2: 
    $id = $order_item->product_sku; 
	if (empty($id))
    $id = $order_item->virtuemart_product_id; 
	break; 
  case 3: 
    $id = $order_item->product_sku; 
	if (empty($id))
    $id = $order_item->virtuemart_product_id; 
    $lang = $this->order['details']['BT']->order_language; 
	if (!empty($lang) && (stripos($lang, '-')!==false))
	 {
	   $a = explode('-', $lang); 
	   $id = $id.'-'.$a[1]; 
	 }
	 else
	 {
	   $tag = JFactory::getLanguage()->getTag(); 
	   $a = explode('-', $tag); 
	   if (!empty($a))
	   $id = $id.'-'.$a[1]; 
	 }
	 break; 
	 
}
*/

$pid = $this->getPID($order_item->virtuemart_product_id, $order_item->product_sku); 

?>		
google_tag_params.ecomm_prodid.push('<?php echo $this->escapeSingle($id); ?>'); 
<?php } ?>

/* ]]> */
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id =  <?php 
$cid = preg_replace("/[^0-9]/", "", $this->params->google_conversion_id); 
if (empty($cid)) echo '0'; else echo $cid; 

?>;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/<?php echo $this->params->google_conversion_id; ?>/?value=<?php echo $order_total; ?>&guid=ON&script=0" />
</div>
</noscript>

