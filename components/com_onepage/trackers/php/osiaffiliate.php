<?php
/*
*
* @copyright Copyright (C) 2007 - 2013 RuposTel - All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* One Page checkout is free software released under GNU/GPL and uses code from VirtueMart
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* 
* stAn note: Always use default headers for your php files, so they cannot be executed outside joomla security 
*
*/

defined( '_JEXEC' ) or die( 'Restricted access' );
$order_total = $this->order['details']['BT']->order_subtotal;
$order_total_txt = number_format($order_total, 2, '.', ''); 
$order_id = $this->order['details']['BT']->virtuemart_order_id;
?>
<script>
  function hideIF() {	
  document.getElementById('IF').style.display = '';
  }
 function getSaleInfo() {
     
 document.getElementById('st_code').innerHTML='<iframe src="https://laverite.myomnistar.com/salejs.php?amount=<?php echo $order_total_txt; ?>&transaction=<?php echo $order_id; ?>" alt="" id=IF width=50 height=50 border="0" frameborder="0" onload="hideIF()">';
 }
 window.onload = getSaleInfo; 
</script>
<div id="st_code"></div>  

<script>
if ((typeof console != 'undefined')  && (typeof console.log != 'undefined')  &&  (console.log != null))
	  {
	     console.log('OPC Tracking: OsiAffiliate code was executed'); 
	  }
	  
</script>	  