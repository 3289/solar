	<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Solarcalculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Solarcalculator model.
 *
 * @since  1.6
 */
class SolarcalculatorModelStation extends JModelItem
{
	public function getSettings(){
	   
	   $db = JFactory::getDbo(); 
	   $query = $db->getQuery(true);
	   
	   $query->select('settings');
	   $query->from($db->quoteName('#__solarcalculator_settings'));
	   $query->where($db->quoteName('type') . ' LIKE '. $db->quote('settings'));
	   $db->setQuery($query);
	   $result = $db->loadObjectList();
	   $settings = json_decode($result[0]->settings);

	   return $settings;
	}
	
	public function priceConvert($unit,$priceValue,$priceUnit){
		
		$currency = self::getCurrency();
		
		if($unit == 'uah'){
			if($priceUnit == 'usd'){
			   $price = 	round($priceValue * $currency['usd']);
			}
			
			if($priceUnit == 'eur'){
			   $price = 	round($priceValue * $currency['eur']);
			}
		}
		if($unit == 'usd'){
			if($priceUnit == 'uah'){
			   $price = 	round($priceValue / $currency['usd']);
			}
			
			if($priceUnit == 'eur'){
			   $price = 	round($priceValue * ($currency['eur'] / $currency['usd']));
			}						
		}	
		if($unit == 'eur'){
			
			if($priceUnit == 'uah'){
			   $price = 	round($priceValue / $currency['eur']);
			}
			
			if($priceUnit == 'usd'){
			   $price = 	round($priceValue * ($currency['usd'] / $currency['eur']));
			}						
			
		}
		
		if(!$price){
		   $price = $priceValue;	
		}
       
	   return $price;
	   
	}
	
	public function getCurrency(){
	    $localSet     = file_get_contents (JPATH_BASE.'/modules/mod_vscurrency/files/currency.json');
        $currency = json_decode($localSet, TRUE);	
		return $currency;
		
	}
	
	public function getStation($id){
	   $db = JFactory::getDbo(); 
	   $query = $db->getQuery(true);
	   
	   $query->select('*');
	   $query->from($db->quoteName('#__solarcalculator_stations'));
	   $query->where($db->quoteName('id') . ' = '. $id);
	   $db->setQuery($query);
	   $result = $db->loadObject();

	   return $result;
	}
	
	public function getRegion($id){
	   $db = JFactory::getDbo(); 
	   $query = $db->getQuery(true);
	   
	   $query->select('*');
	   $query->from($db->quoteName('#__solarcalculator_regions'));
	   $query->where($db->quoteName('id') . ' = '. $id);
	   $db->setQuery($query);
	   $result = $db->loadObject();

	   return $result;
	}
	
	public function calculatePrivate($station_id,$region,$poweruse){
		$station  = $this->getStation($station_id);
		$settings = $this->getSettings();
		$region =   $this->getRegion($region);
		$currency = $this->getCurrency();
		$result = array();
		
		$regionInsolation = $this->getInsolation($region,$settings);
		$result['station'] = $station; 
		$result['settings'] = $settings;
		$result['region'] = $region;
		
		//Инсоляция для станции
		$result['insolation'] = $regionInsolation * $station->station_value;
		
		//Потребление за год
		$result['powerperyear'] = $poweruse * 12;

		//Остаток
		$result['residue'] = $result['insolation'] - $result['powerperyear'];
		
		//Доход за год
		$result['profit'] = $result['residue'] * $settings->setting_green_price_private;
		
		//Чистый доход за год
		$result['netprofit'] = $result['profit'] - $result['profit'] * (($settings->setting_station_incoming_tax + $settings->setting_station_war_tax) / 100);
		
		//Чистый доход за год в евро
		$result['netprofitEuro'] = round($result['netprofit'] / $currency['eur']);
		
		//Цена станции в евро
		$result['stationPriceEuro']  = $this->priceConvert('eur',$station->station_price,$station->station_price_unit);
		
		//Окупаемость станции
		$result['payback'] = round($result['stationPriceEuro'] / $result['netprofitEuro'], 1);
		
		//Лет до 2030 
		$result['years'] = 2030 - date('Y'); 
		
		//Лет после окупаемости
		$result['yearsEarnings'] = $result['years'] - $result['payback']; 
		
		//Зароботок 
		$result['earnings'] = (2030 - date('Y') - $result['payback']) * $result['netprofitEuro'];
		
		
		$result['title'] = JText::_('COM_SOLARCALCULATOR_POPUP_TITLE_PRIVATE'); 

		return $result;
	}
	
	public function calculateCommerce($station_id,$region,$poweruse,$customValue){
		$station  = $this->getStation($station_id);
		$settings = $this->getSettings();
		$region = $this->getRegion($region);
		$currency = $this->getCurrency();
		$result = array();
		
		$regionInsolation = $this->getInsolation($region,$settings);
		
		$result['station'] = $station; 
		$result['settings'] = $settings;
		$result['region'] = $region;
		
		$result['stationValue'] = $station->station_value;
		$result['stationValueKwt'] = $result['stationValue'] * 1000;
		$result['stationPrice'] = $station->station_price;
		if($customValue){
			$result['stationValue'] = $customValue;
			$result['stationValueKwt'] = $result['stationValue'] * 1000;
			$result['stationPrice'] = $customValue * 1000000 * $settings->setting_station_price_stable;
		}
		
		//Генерация за год
		$result['insolation'] = $regionInsolation * $result['stationValueKwt'];
		
		//Доход за год евро
		$result['profit'] = $result['insolation'] * $settings->setting_green_price_commerce;
			
		//Доход за год доллар
		$result['profitUsd']  = $this->priceConvert('usd',$result['profit'],'eur');
		
		//Окупаемость станции
		$result['payback'] = round($result['stationPrice'] / $result['profitUsd'], 1);
		
		$result['title'] = JText::_('COM_SOLARCALCULATOR_POPUP_TITLE_COMMERCE'); 
				
		$result['calculate']['line1'] = '<div class="table-row"><div class="table-label">' . JText::_('COM_SOLARCALCULATOR_STANTION') .' '. $stationValue .
		JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_MWT'). ' '.
		JText::_('COM_SOLARCALCULATOR_STANTION_GENERATES').'</div><div class="table-text">'.
		$insolation .' '. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT'). ' '.
		JText::_('COM_SOLARCALCULATOR_STANTION_PER_YEAR').'</div><div class="clr"></div></div>';
		
		$result['calculate']['line2'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_GREEN_TARIFF') . 
							'</div><div class="table-text">' . 
		                    $settings->setting_green_price_commerce . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR'). 
							'</div><div class="clr"></div></div>';
												
							
		$result['calculate']['line3'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_INCOME_PER_YEAR') . 
							'</div><div class="table-text">' . 
		                    $insolation . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').' * ' .
							$settings->setting_green_price_commerce . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR').  ' = ' . $profit .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR').
							'</div><div class="clr"></div></div>';			
													
		$result['calculate']['line4'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_PRICE') . 
							'</div><div class="table-text">' . 
		                    $stationPrice . ' $'.
							'</div><div class="clr"></div></div>';		
							
		$result['calculate']['line5'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_PAYBACK_WITOUT_PDV') . 
							'</div><div class="table-text">' . 
		                    $stationPrice . ' $ / '. $profitUsd .' $'. ' = '. $payback . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS').
							'</div><div class="clr"></div></div>';	
							
		
		
		switch($yearsEarnings){
		  case 1:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEAR');break;	
		  case 2:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  case 3:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  case 4:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  default:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS');break;		
		}					
		
		
        
		return $result;
	     
	}
	
	public function getInsolation($region,$settings){
		$regionSituate = $region->region_situate;
		switch($regionSituate){
		    case 'center': $insolation = $settings->setting_insolation_center;break;
			case 'north': $insolation = $settings->setting_insolation_north;break;	
			case 'south': $insolation = $settings->setting_insolation_south;break;		
		}
		
		return $insolation;
	}
}
