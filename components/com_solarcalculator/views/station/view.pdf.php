<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;



class SolarcalculatorViewStation extends JViewLegacy
{
	
	protected $result;
	protected $type;
	protected $station;
	protected $settings;
	protected $poweruse;
	protected $region;
	
	public function display($tpl = null)
	{
		$config = JFactory::getConfig();
		
	    $jinput = JFactory::getApplication()->input;
	    $this->type = $jinput->get('type');
	    $station_id = $jinput->get('id');
	    $region = $jinput->get('region');
	    $this->poweruse = $jinput->get('power');
	    $customValue = $jinput->get('custom');
		
		$model = $this->getModel('station');
		
		if($this->type == 'private'){
	       $this->result = $model->calculatePrivate($station_id,$region,$this->poweruse);   
	    }
	    
		if($this->type == 'commerce'){
	       $this->result = $model->calculateCommerce($station_id,$region,$this->poweruse,$customValue);   
	    }
		
		$this->station = $this->result['station'];
		$this->settings = $this->result['settings'];
		$this->region = $this->result['region'];
          
		$document = JFactory::getDocument();
		$document->setName('solar_strategia_raschet');
		$document->setTitle($config->get( 'sitename' ) .' - ' .$this->result['title']);
		
		parent::display($tpl);
	}

	
}
