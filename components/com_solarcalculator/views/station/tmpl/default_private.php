<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<table class="input">
      <tr>
          <td><br/><br/><?php echo JText::_('COM_SOLARCALCULATOR_STANTION_POWER') ;?><br/></td>
          <td><br/><br/><?php echo $this->station->station_value.' '.JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($this->station->station_value_units)); ?> <br/></td>
      </tr>
      <tr>
          <td><br/><br/><?php echo JText::_('COM_SOLARCALCULATOR_STANTION_REGION') ;?><br/></td>
          <td><br/><br/><?php echo $this->region->region_name; ?><br/></td>
      </tr>
      <tr>
          <td><br/><br/><?php echo JText::_('COM_SOLARCALCULATOR_STANTION_POWER_CONSUMPTION_MONTH') ;?><br/></td>
          <td><br/><br/><?php echo $this->poweruse.' '.JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($this->station->station_value_units)) ?><br/></td>
      </tr>
</table>
<p></p>
<p></p>
<p></p>

<table class="result">
    <tr>
        <td><br/>
		     <?php 
			       echo JText::_('COM_SOLARCALCULATOR_STANTION').' '.
				   $this->station->station_value .' '. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($this->station->station_value_units)) . ' '. 
		           JText::_('COM_SOLARCALCULATOR_STANTION_GENERATES');?>  <br/>  
        </td>
        <td><br/>
            <?php
                echo $this->result['insolation'] .' '. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($this->station->station_value_units)). ' '.
		        JText::_('COM_SOLARCALCULATOR_STANTION_PER_YEAR');
			
			?><br/>
        </td>
    </tr>
    <tr>
        <td> <br/><br/>
		     <?php 
			    echo JText::_('COM_SOLARCALCULATOR_STANTION_GREEN_TARIFF'); 
			 ?> <br/>
		</td>
        <td><br/><br/>
		      <?php              
			   echo $this->settings->setting_green_price_private . ' '.JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH');
			  ?><br/>				
		</td>
    </tr>                        
	<tr>
         <td><br/><br/>
		        <?php echo JText::_('COM_SOLARCALCULATOR_STANTION_POWER_CONSUMPTION'); ?> <br/>
		</td>
        <td><br/><br/>
            <?php
		          echo      $this->poweruse . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').' * 12 ' .
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_MONTH'). ' = ' . $this->result['powerperyear'] .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT');
			?>	<br/>			
		</td>
	</tr>	
    <tr>
         <td>	<br/>	<br/>			
		     <?php echo  JText::_('COM_SOLARCALCULATOR_STANTION_EXCESS_ENERGY'); ?> <br/>
		</td> 
        <td><br/><br/>
             <?php
		                   echo $this->result['insolation'] .' '. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($this->station->station_value_units)). ' - '.
							$this->result['powerperyear']. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT'). ' = ' . $this->result['residue'] .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT');
			?><br/>				
		</td>
    </tr>    							
	<tr>
         <td>	<br/>	<br/>				
            <?php echo  JText::_('COM_SOLARCALCULATOR_STANTION_INCOME_PER_YEAR'); ?><br/>
		</td>
        <td>   <br/> <br/>
		      <?php              
			             echo   $this->result['residue'] . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').' * ' .
							$this->settings->setting_green_price_private . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH').  ' = ' . $this->result['profit'] .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH');
				?>		<br/>	
		</td>
    </tr>    		
	<tr>
         <td><br/>	<br/>						
		          <?php echo JText::_('COM_SOLARCALCULATOR_STANTION_NETPROFIT_PER_YEAR') ?> <br/>
		</td>
        <td><br/><br/>
             <?php
		               echo     $this->result['profit'] .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH'). 
							' - '.$this->settings->setting_station_incoming_tax.'% - '.$this->settings->setting_station_war_tax.'% = '.
							$this->result['netprofit'] . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH').
							'<br/><div class="table-info-text">'.
							JText::sprintf('COM_SOLARCALCULATOR_SETTING_INCOMING_TAX', htmlspecialchars($this->settings->setting_station_incoming_tax.'%', ENT_COMPAT, 'UTF-8')).'<br/><br/>'.
							JText::sprintf('COM_SOLARCALCULATOR_SETTING_WAR_TAX', htmlspecialchars($this->settings->setting_station_war_tax.'%', ENT_COMPAT, 'UTF-8')).
							'</div>';
			?>	<br/>			
		</td>
     </tr>   
	<tr>
         <td>	<br/>	<br/>					
		          <?php   echo  JText::_('COM_SOLARCALCULATOR_STANTION_NETPROFIT_PER_YEAR') ?><br/>
		</td> 
        <td><br/><br/>
		          <?php   echo   $this->result['netprofitEuro'] . ' &#8364;';?><br/>
		</td>
    </tr>    	
	<tr>
         <td><br/>		<br/>					
		          <?php   echo  JText::_('COM_SOLARCALCULATOR_STANTION_PRICE') ;?><br/>
         </td>
         <td><br/><br/>
		           <?php   echo  $this->result['stationPriceEuro'] . ' &#8364;';?><br/>
		</td>
	</tr>						
	<tr>
         <td>	<br/>	<br/>					
		          <?php   echo JText::_('COM_SOLARCALCULATOR_STANTION_PAYBACK');?><br/>
         </td>         
         <td><br/><br/>
		         <?php  echo $this->result['stationPriceEuro'] . ' &#8364; / '. $this->result['netprofitEuro'] .' &#8364;'. ' = '. $this->result['payback'] . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS');?><br/>
		</td>
	</tr>						
    <tr>
         <td>	<br/>	<br/>					
		          <?php echo JText::_('COM_SOLARCALCULATOR_STANTION_YEARS_TO2030');?><br/>
          </td>
          <td><br/><br/>
		          <?php echo'2030 - '. date('Y') . ' = '. $this->result['years'] . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS'); ?><br/>
		 </td>
	</tr>						
    <tr>
         <td>	<br/>	<br/>					
		          <?php echo JText::_('COM_SOLARCALCULATOR_STANTION_YEARS_EARNINGS');?><br/>
		</td>
        <td> <br/><br/>
		         <?php echo $this->result['years'] . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS') . ' - ' . 
							$this->result['payback'] . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS') . ' = '.$this->result['yearsEarnings'] . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS'); ?><br/>
		</td>
	</tr>	
    <tr>
         <td><br/>		<br/>					
		          <?php echo JText::_('COM_SOLARCALCULATOR_STANTION_EARNINGS'); ?><br/>
         </td>
         <td>  <br/>    <br/>   
		        <?php echo $this->result['yearsEarnings'] . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS'). ' * '.
							$this->result['netprofitEuro'] .' &#8364; = '.$this->result['earnings'] .' &#8364;'; ?><br/>
		</td>
     </tr>   	
</table>
<p></p>
<p style="line-height:70px;"> 
<?php
   switch($this->result['yearsEarnings']){
		  case 1:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEAR');break;	
		  case 2:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  case 3:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  case 4:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  default:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS');break;		
		}		
    echo JText::sprintf('COM_SOLARCALCULATOR_STANTION_TEXT_RESULT1', htmlspecialchars($this->result['yearsEarnings'].' '.$textYear, ENT_COMPAT, 'UTF-8')).
		JText::sprintf('COM_SOLARCALCULATOR_STANTION_TEXT_RESULT2', $this->result['earnings']. ' &#8364;', ENT_COMPAT, 'UTF-8');
?>
</p>
<?php
  // var_dump($this->result);
?>