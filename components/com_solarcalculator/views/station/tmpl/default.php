<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<style type="text/css">
.result td{
	border-bottom:1px solid #333333;
	line-height:35px;
	}
	
.input td{
	border:1px solid #333333;
	line-height:35px;
	}	
	
.input{
	margin-bottom:40px;
	border:1px solid #333333;
	}	

</style>

<?php
   if($this->type == 'private'){
	  echo $this->loadTemplate('private');  
   }
   
   if($this->type == 'commerce'){
	  echo $this->loadTemplate('commerce');  
   }
?>