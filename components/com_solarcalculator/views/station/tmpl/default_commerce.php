<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<table class="input">
      <tr>
          <td><br/><br/><?php echo JText::_('COM_SOLARCALCULATOR_STANTION_POWER') ;?><br/></td>
          <td><br/><br/><?php echo $this->station->station_value.' '.JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($this->station->station_value_units)); ?> <br/></td>
      </tr>
      <tr>
          <td><br/><br/><?php echo JText::_('COM_SOLARCALCULATOR_STANTION_REGION') ;?><br/></td>
          <td><br/><br/><?php echo $this->region->region_name; ?><br/></td>
      </tr>
     
</table>
<p></p>
<p></p>
<p></p>

<table class="result">
    <tr>
        <td><br/>
		     <?php 
			      echo JText::_('COM_SOLARCALCULATOR_STANTION') .' '. $this->result['stationValue'] .
				  JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_MWT'). ' '.
				  JText::_('COM_SOLARCALCULATOR_STANTION_GENERATES');?>  <br/>  
        </td>
        <td><br/>
            <?php
                echo $this->result['insolation'] .' '. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($this->station->station_value_units)). ' '.
		        JText::_('COM_SOLARCALCULATOR_STANTION_PER_YEAR');
			
			?><br/>
        </td>
    </tr>
    <tr>
        <td> <br/><br/>
		     <?php 
			    echo JText::_('COM_SOLARCALCULATOR_STANTION_GREEN_TARIFF'); 
			 ?> <br/>
		</td>
        <td><br/><br/>
		      <?php              
			   echo $this->settings->setting_green_price_commerce . ' '. JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR')
			  ?><br/>				
		</td>
    </tr>                        
	<tr>
         <td><br/><br/>
		        <?php echo  JText::_('COM_SOLARCALCULATOR_STANTION_INCOME_PER_YEAR'); ?> <br/>
		</td>
        <td><br/><br/>
            <?php
		          echo      $this->result['insolation'] . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').' * ' .
							$this->settings->setting_green_price_commerce . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR').  ' = ' . $this->result['profit'] .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR');
			?>	<br/>			
		</td>
	</tr>	
    <tr>
         <td>	<br/>	<br/>			
		     <?php echo  JText::_('COM_SOLARCALCULATOR_STANTION_PRICE'); ?> <br/>
		</td> 
        <td><br/><br/>
             <?php
		                   echo $this->result['stationPrice']. ' $';
			?><br/>				
		</td>
    </tr>    							
	<tr>
         <td>	<br/>	<br/>				
            <?php echo  JText::_('COM_SOLARCALCULATOR_STANTION_PAYBACK_WITOUT_PDV'); ?><br/>
		</td>
        <td>   <br/> <br/>
		      <?php              
			             echo   $this->result['stationPrice'] . ' $ / '. $this->result['profitUsd'] .' $'. ' = '. $this->result['payback'] . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS');
				?>		<br/>	
		</td>
    </tr>    		
	
</table>

