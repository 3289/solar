<?php

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

class SolarcalculatorControllerStations extends JControllerAdmin
{
	public function getModel($name = 'Station', $prefix = 'SolarcalculatorModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

}