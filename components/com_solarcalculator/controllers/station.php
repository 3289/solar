<?php

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

class SolarcalculatorControllerStation extends JControllerLegacy {
	
	public function getAjaxCalculate(){
	  
	  $jinput = JFactory::getApplication()->input;
	  $power = $jinput->get('power');	
	  
	  $model = $this->getModel('station');
	  $settings = $model->getSettings();
	  
	  $price = $settings->setting_station_price_stable * $power * 1000000;	
	  $priceResult = $model->priceConvert( $settings->setting_price_units,$price,'usd');

	  echo number_format($priceResult,0,'',' '). ' ' . iconv('UTF-8','windows-1251',JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_'.strtoupper($settings->setting_price_units)));
	  exit;

	}
	
	public function getAjaxCalculator(){
	   $jinput = JFactory::getApplication()->input;
	   $type = $jinput->get('type');
	   $station_id = $jinput->get('station_id');
	   $region = $jinput->get('region');
	   $poweruse = $jinput->get('power_num');
	   $customValue = $jinput->get('user_value');
	   
	   
	   if($type == 'private'){
	      $result = $this->calculatePrivate($station_id,$region,$poweruse);   
	   }
	   
	   if($type == 'commerce'){
	      $result = $this->calculateCommerce($station_id,$region,$poweruse,$customValue);   
	   }
	   
	   exit;	
	}
	
	
	public function calculatePrivate($station_id,$region,$poweruse){
		$model = $this->getModel('station');
		$station  = $model->getStation($station_id);
		$settings = $model->getSettings();
		$region = $model->getRegion($region);
		$currency = $model->getCurrency();
		$result = array();
		
		$regionInsolation = $this->getInsolation($region,$settings);
		
		//Инсоляция для станции
		$insolation = $regionInsolation * $station->station_value;
		
		//Потребление за год
		$powerperyear = $poweruse * 12;
		
		//Остаток
		$residue = $insolation - $powerperyear;
		
		//Доход за год
		$profit = $residue * $settings->setting_green_price_private;
		
		//Чистый доход за год
		$netprofit = $profit - $profit * (($settings->setting_station_incoming_tax + $settings->setting_station_war_tax) / 100);
		
		//Чистый доход за год в евро
		$netprofitEuro = round($netprofit / $currency['eur']);
		
		//Цена станции в евро
		$stationPriceEuro  = $model->priceConvert('eur',$station->station_price,$station->station_price_unit);
		
		//Окупаемость станции
		$payback = round($stationPriceEuro / $netprofitEuro, 1);
		
		//Лет до 2030 
		$years = 2030 - date('Y'); 
		
		//Лет после окупаемости
		$yearsEarnings = $years - $payback; 
		
		//Зароботок 
		$earnings = (2030 - date('Y') - $payback) * $netprofitEuro;
		
		$result['inputData'] = $this->inputDataHtml($region,$poweruse,$station);
		
		$result['title'] = JText::_('COM_SOLARCALCULATOR_POPUP_TITLE_PRIVATE'); 
		
		if ($residue < 0 || $residue == 0 || $yearsEarnings == 0 || $yearsEarnings < 0 ){
			$result['calculate']['line1'] =  '<div class="calculate-error">' . JText::_('COM_SOLARCALCULATOR_STANTION_NOT_PAYBACK') . '</div>';
		}else{
		
		$result['calculate']['line1'] = '<div class="table-row"><div class="table-label">' . JText::_('COM_SOLARCALCULATOR_STANTION') .' '. $station->station_value .
		JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($station->station_value_units)) . ' '. 
		JText::_('COM_SOLARCALCULATOR_STANTION_GENERATES').'</div><div class="table-text">'.
		$insolation .' '. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($station->station_value_units)). ' '.
		JText::_('COM_SOLARCALCULATOR_STANTION_PER_YEAR').'</div><div class="clr"></div></div>';
		
		$result['calculate']['line2'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_GREEN_TARIFF') . 
							'</div><div class="table-text">' . 
		                    $settings->setting_green_price_private . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH'). 
							'</div><div class="clr"></div></div>';
							
		$result['calculate']['line3'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_POWER_CONSUMPTION') . 
							'</div><div class="table-text">' . 
		                    $poweruse . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').' * 12 ' .
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_MONTH'). ' = ' . $powerperyear .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').
							'</div><div class="clr"></div></div>';	
							
		$result['calculate']['line4'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_EXCESS_ENERGY') . 
							'</div><div class="table-text">' . 
		                    $insolation .' '. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($station->station_value_units)). ' - '.
							$powerperyear. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT'). ' = ' . $residue .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').
							'</div><div class="clr"></div></div>';								
							
		$result['calculate']['line5'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_INCOME_PER_YEAR') . 
							'</div><div class="table-text">' . 
		                    $residue . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').' * ' .
							$settings->setting_green_price_private . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH').  ' = ' . $profit .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH').
							'</div><div class="clr"></div></div>';			
							
		$result['calculate']['line6'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_NETPROFIT_PER_YEAR') . 
							'</div><div class="table-text">' . 
		                    $profit .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH'). 
							' - '.$settings->setting_station_incoming_tax.'% - '.$settings->setting_station_war_tax.'% = '.
							$netprofit . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH').
							'<div class="table-info-text">'.
							JText::sprintf('COM_SOLARCALCULATOR_SETTING_INCOMING_TAX', htmlspecialchars($settings->setting_station_incoming_tax.'%', ENT_COMPAT, 'UTF-8')).'<br/>'.
							JText::sprintf('COM_SOLARCALCULATOR_SETTING_WAR_TAX', htmlspecialchars($settings->setting_station_war_tax.'%', ENT_COMPAT, 'UTF-8')).
							'</div>'.
							'</div><div class="clr"></div></div>';	
							
		$result['calculate']['line7'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_NETPROFIT_PER_YEAR') . 
							'</div><div class="table-text">' . 
		                    $netprofitEuro . ' &#8364;'.
							'</div><div class="clr"></div></div>';		
							
		$result['calculate']['line8'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_PRICE') . 
							'</div><div class="table-text">' . 
		                    $stationPriceEuro . ' &#8364;'.
							'</div><div class="clr"></div></div>';		
							
		$result['calculate']['line9'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_PAYBACK') . 
							'</div><div class="table-text">' . 
		                    $stationPriceEuro . ' &#8364; / '. $netprofitEuro .' &#8364;'. ' = '. $payback . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS').
							'</div><div class="clr"></div></div>';	
							
		$result['calculate']['line10'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_YEARS_TO2030') . 
							'</div><div class="table-text">' . 
		                    '2030 - '. date('Y') . ' = '. $years . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS').
							'</div><div class="clr"></div></div>';	
							
		$result['calculate']['line11'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_YEARS_EARNINGS') . 
							'</div><div class="table-text">' . 
		                    $years . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS') . ' - ' . 
							$payback . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS') . ' = '. $yearsEarnings . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS').
							'</div><div class="clr"></div></div>';		
							
		$result['calculate']['line12'] = '<div class="table-row row-result"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_EARNINGS') . 
							'</div><div class="table-text">' . 
		                    $yearsEarnings . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS'). ' * '.
							$netprofitEuro .' &#8364; = '.$earnings .' &#8364;'.
							'</div><div class="clr"></div></div>';		
		
		switch($yearsEarnings){
		  case 1:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEAR');break;	
		  case 2:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  case 3:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  case 4:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  default:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS');break;		
		}					
		
							
		$result['textOutput'] = JText::sprintf('COM_SOLARCALCULATOR_STANTION_TEXT_RESULT1', htmlspecialchars($yearsEarnings.' '.$textYear, ENT_COMPAT, 'UTF-8')).
		JText::sprintf('COM_SOLARCALCULATOR_STANTION_TEXT_RESULT2', $earnings. ' &#8364;', ENT_COMPAT, 'UTF-8');
		}
        
		echo json_encode($result);
	}
	
	public function getInsolation($region,$settings){
		$regionSituate = $region->region_situate;
		switch($regionSituate){
		    case 'center': $insolation = $settings->setting_insolation_center;break;
			case 'north': $insolation = $settings->setting_insolation_north;break;	
			case 'south': $insolation = $settings->setting_insolation_south;break;		
		}
		
		return $insolation;
	}
	
	public function calculateCommerce($station_id,$region,$poweruse,$customValue){
		$model = $this->getModel('station');
		$station  = $model->getStation($station_id);
		$settings = $model->getSettings();
		$region = $model->getRegion($region);
		$currency = $model->getCurrency();
		$result = array();
		
		$regionInsolation = $this->getInsolation($region,$settings);
		
		$stationValue = $station->station_value;
		$stationValueKwt = $stationValue * 1000;
		$stationPrice = $station->station_price;
		if($customValue){
			$stationValue = $customValue;
			$stationValueKwt = $stationValue * 1000;
			$stationPrice = $customValue * 1000000 * $settings->setting_station_price_stable;
		}
		
		//Генерация за год
		$insolation = $regionInsolation * $stationValueKwt;
		
		//Доход за год евро
		$profit = $insolation * $settings->setting_green_price_commerce;
			
		//Доход за год доллар
		$profitUsd  = $model->priceConvert('usd',$profit,'eur');
		
		//Окупаемость станции
		$payback = round($stationPrice / $profitUsd, 1);

		$result['inputData'] = $this->inputDataHtml($region,$poweruse,$station,$customValue);
		
		
		$result['title'] = JText::_('COM_SOLARCALCULATOR_POPUP_TITLE_COMMERCE'); 
				
		$result['calculate']['line1'] = '<div class="table-row"><div class="table-label">' . JText::_('COM_SOLARCALCULATOR_STANTION') .' '. $stationValue .
		JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_MWT'). ' '.
		JText::_('COM_SOLARCALCULATOR_STANTION_GENERATES').'</div><div class="table-text">'.
		$insolation .' '. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT'). ' '.
		JText::_('COM_SOLARCALCULATOR_STANTION_PER_YEAR').'</div><div class="clr"></div></div>';
		
		$result['calculate']['line2'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_GREEN_TARIFF') . 
							'</div><div class="table-text">' . 
		                    $settings->setting_green_price_commerce . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR'). 
							'</div><div class="clr"></div></div>';
												
							
		$result['calculate']['line3'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_INCOME_PER_YEAR') . 
							'</div><div class="table-text">' . 
		                    $insolation . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').' * ' .
							$settings->setting_green_price_commerce . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR').  ' = ' . $profit .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR').
							'</div><div class="clr"></div></div>';			
													
		$result['calculate']['line4'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_PRICE') . 
							'</div><div class="table-text">' . 
		                    $stationPrice . ' $'.
							'</div><div class="clr"></div></div>';		
							
		$result['calculate']['line5'] = '<div class="table-row"><div class="table-label">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_PAYBACK_WITOUT_PDV') . 
							'</div><div class="table-text">' . 
		                    $stationPrice . ' $ / '. $profitUsd .' $'. ' = '. $payback . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS').
							'</div><div class="clr"></div></div>';	
							
		
		
		switch($yearsEarnings){
		  case 1:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEAR');break;	
		  case 2:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  case 3:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  case 4:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  default:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS');break;		
		}					
		
							
		$result['commerce'] = 1;
		
        
		echo json_encode($result);
	     
	}
	
	public function inputDataHtml($region,$poweruse,$station,$customValue){
		
		$power = $station->station_value;
		if($customValue){
			$power = $customValue;
		}
		
		$result = '<table style="margin-bottom:30px;" width="100%">
		              <tr>
					      <td  style="background-color:#aaaaaa;padding:10px;color:#ffffff;">'.JText::_('COM_SOLARCALCULATOR_STANTION_POWER').'</td>
						  <td  style="background-color:#ffb803;padding:10px;color:#302c2c;">'.$power.' '.JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($station->station_value_units)).'</td>
					  </tr>
					  <tr>
					      <td  style="background-color:#aaaaaa;padding:10px;color:#ffffff;">'.JText::_('COM_SOLARCALCULATOR_STANTION_REGION').'</td>
						  <td  style="background-color:#ffb803;padding:10px;color:#302c2c;">'.$region->region_name.'</td>
					  </tr>
					  <tr>
					      <td   style="background-color:#aaaaaa;padding:10px;color:#ffffff;">'.JText::_('COM_SOLARCALCULATOR_STANTION_POWER_CONSUMPTION_MONTH').'</td>
						  <td  style="background-color:#ffb803;padding:10px;color:#302c2c;">'.$poweruse.' '.JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($station->station_value_units)).'</td>
					  </tr>
	 	          </table>';
				  
		return $result;		  
	}
	
	public function currencyConvert($unit){
		
		return $insolation;
	}
	
	public function getAjaxSendemail(){
	   $jinput = JFactory::getApplication()->input;
	   $type = $jinput->get('type');
	   $station_id = $jinput->get('station_id');
	   $region = $jinput->get('region');
	   $poweruse = $jinput->get('power_num');
	   $customValue = $jinput->get('user_value');
	   $email= $jinput->get('email','', 'String');
	   
	   
	   if($type == 'private'){
	      $result = $this->sendEmailPrivate($station_id,$region,$poweruse);  
		  $title =  JText::_('COM_SOLARCALCULATOR_POPUP_TITLE_PRIVATE');
	   }
	   
	   if($type == 'commerce'){
	      $result = $this->sendEmailCommerce($station_id,$region,$poweruse,$customValue);   
		  $title =  JText::_('COM_SOLARCALCULATOR_POPUP_TITLE_COMMERCE');
	   }
	   
	   $sendResult = array();
	   
	   if(!$this->Sendemail($result,$title,$email)){
		   $sendResult['message'] = 2;
	   }else{
           $sendResult['message'] = 1;
	   }
	   
	   echo json_encode($sendResult);
	   exit;
	   
	}
	
	
	public function Sendemail($result,$title,$email){
		
	   $mailer = JFactory::getMailer();
	   $config = JFactory::getConfig();
	   $sender = array($config->get('config.mailfrom'), $config->get('config.fromname') );
	   $mailer->setSender($sender);
	   $mailer->addRecipient($email);
	   $mailer->setSubject($config->get( 'sitename' ) .' ' .$title);
	   
	   $body   = '
	        <table cellpadding="0" cellspacing="0" width="80%" style="font-family:Arial, sans-serif">
			    
				<tr>
 
			<td class="vmdoc-header-image" width="30%"><a href="<?php echo JURI::base()?>"><img src="'. JURI::root () . 'images/stories/virtuemart/vendor/logo.png" /></a></td>
			<td colspan=1 class="vmdoc-header-vendor" width="70%">
                 <div style="font-size:36px">'.vmText::sprintf ('COM_VIRTUEMART_MAIL_COMPANY_NAME').'</div>
                 <div style="font-size:16px;">'. vmText::sprintf ('COM_VIRTUEMART_MAIL_COMPANY_ADDRESS').'</div>
                 <div style="margin-top:15px; margin-bottom:15px; font-size:16px; line-height:22px;">'.vmText::sprintf ('COM_VIRTUEMART_MAIL_COMPANY_BANK').'</div>
		</td>
	</tr>
    <tr><td colspan=2 width="100%" style="height:45px; background: url('.JURI::base().'/images/mail_header.png) repeat-x;"></td></tr>
						
				
			    <tr>
				     <td colspan="2" width="100%">'.$result.'
					 </td>
				</tr>
			</table>
	             ';
				 
		$mailer->isHTML(true);
		$mailer->setBody($body);
		
		$send = $mailer->Send();
		if ($send !== true) {
			return false;
		}
		
		return true;	
		
	} 
	
	
	public function getAjaxSendorder(){
	   $jinput = JFactory::getApplication()->input;
	   $name = $jinput->get('type');
	   $phone = $jinput->get('phone');
	   $emailSender= $jinput->get('email');
	   $email = $config->get('config.mailfrom');
	   
	   
	   $body = '<table width="100%">
	                <tr>
					    <td>'.JText::_('MOD_SOLARCALCULATOR_FORM_NAME').'</td>
						<td>'.$name.'</td>
					</tr>
					<tr>
					    <td>'.JText::_('MOD_SOLARCALCULATOR_FORM_EMAIL').'</td>
						<td>'.$emailSender.'</td>
					</tr>
					<tr>
					    <td>'.JText::_('MOD_SOLARCALCULATOR_FORM_PHONE').'</td>
						<td>'.$phone.'</td>
					</tr>
	            </table>';
				
		$sendResult = array();
	   
	   $title = JText::_('MOD_SOLARCALCULATOR_CALCULATE_ORDER_TITLE');
	   
	   if(!$this->Sendemail($result,$title,$email)){
		   $sendResult['message'] = 2;
	   }else{
           $sendResult['message'] = 1;
	   }
	   
	   echo json_encode($sendResult);
	   exit;		
	}
	
	public function sendEmailPrivate($station_id,$region,$poweruse){
		$model = $this->getModel('station');
		$station  = $model->getStation($station_id);
		$settings = $model->getSettings();
		$region = $model->getRegion($region);
		$currency = $model->getCurrency();
		$result = array();
		
		$regionInsolation = $this->getInsolation($region,$settings);
		
		//Инсоляция для станции
		$insolation = $regionInsolation * $station->station_value;
		
		//Потребление за год
		$powerperyear = $poweruse * 12;
		
		//Остаток
		$residue = $insolation - $powerperyear;
		
		//Доход за год
		$profit = $residue * $settings->setting_green_price_private;
		
		//Чистый доход за год
		$netprofit = $profit - $profit * (($settings->setting_station_incoming_tax + $settings->setting_station_war_tax) / 100);
		
		//Чистый доход за год в евро
		$netprofitEuro = round($netprofit / $currency['eur']);
		
		//Цена станции в евро
		$stationPriceEuro  = $model->priceConvert('eur',$station->station_price,$station->station_price_unit);
		
		//Окупаемость станции
		$payback = round($stationPriceEuro / $netprofitEuro, 1);
		
		//Лет до 2030 
		$years = 2030 - date('Y'); 
		
		//Лет после окупаемости
		$yearsEarnings = $years - $payback; 
		
		//Зароботок 
		$earnings = (2030 - date('Y') - $payback) * $netprofitEuro;
		
		$output = '<h3>'.JText::_('COM_SOLARCALCULATOR_POPUP_TITLE_PRIVATE').'</h3>'; 
		
		$output .= $this->inputDataHtml($region,$poweruse,$station);	
		
		$output .= '<table width="100%">
		    <tr>
			    <td width="30%" style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' 
				     . JText::_('COM_SOLARCALCULATOR_STANTION') .' '. $station->station_value .
		               JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($station->station_value_units)) . ' '. 
		               JText::_('COM_SOLARCALCULATOR_STANTION_GENERATES').'
			    </td>
				<td style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">'.
		               $insolation .' '. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($station->station_value_units)). ' '.
		                JText::_('COM_SOLARCALCULATOR_STANTION_PER_YEAR').'
				</td>
			</tr>
			<tr>
			    <td width="30%" style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_GREEN_TARIFF') . 
				'</td>
				<td style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    $settings->setting_green_price_private . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH'). 
				'</td>
			 </tr>
			 <tr>
			     <td width="30%"  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_POWER_CONSUMPTION') . 
				'</td>
				<td style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    $poweruse . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').' * 12 ' .
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_MONTH'). ' = ' . $powerperyear .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').
				'</td>
			  </tr>
			  <tr>
			      <td width="30%" style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_EXCESS_ENERGY') . 
				 '</td>
				  <td style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    $insolation .' '. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($station->station_value_units)). ' - '.
							$powerperyear. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT'). ' = ' . $residue .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').
				  '</td>
			  </tr>
			  <tr>
			      <td width="30%"  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_INCOME_PER_YEAR') . 
				 '</td>
				 <td  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    $residue . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').' * ' .
							$settings->setting_green_price_private . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH').  ' = ' . $profit .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH').
				 '</td>
			  </tr>
			  <tr>
			      <td width="30%"  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_NETPROFIT_PER_YEAR') . 
				 '</td>
				  <td  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    $profit .' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH'). 
							' - '.$settings->setting_station_incoming_tax.'% - '.$settings->setting_station_war_tax.'% = '.
							$netprofit . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_UAH').
							'<div class="table-info-text">'.
							JText::sprintf('COM_SOLARCALCULATOR_SETTING_INCOMING_TAX', htmlspecialchars($settings->setting_station_incoming_tax.'%', ENT_COMPAT, 'UTF-8')).'<br/>'.
							JText::sprintf('COM_SOLARCALCULATOR_SETTING_WAR_TAX', htmlspecialchars($settings->setting_station_war_tax.'%', ENT_COMPAT, 'UTF-8')).
							'</div>'.
				 '</td>
			  </tr>
			  <tr>
			      <td width="30%"  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_NETPROFIT_PER_YEAR') . 
				 '</td>
				 <td  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    $netprofitEuro . ' &#8364;'.
				 '</td>
			  </tr>
			  <tr>
			      <td  width="30%" style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_PRICE') . 
				'</td>
				 <td  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    $stationPriceEuro . ' &#8364;'.
				'</td>
			 </tr>
			 <tr>
			     <td width="30%"  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_PAYBACK') . 
				'</td>
				<td  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    $stationPriceEuro . ' &#8364; / '. $netprofitEuro .' &#8364;'. ' = '. $payback . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS').
				'</td>
			  </tr>
			  <tr>
			      <td  width="30%"  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_YEARS_TO2030') . 
				 '</td>
				 <td  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    '2030 - '. date('Y') . ' = '. $years . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS').
				 '</td>
			  </tr>
			  <tr>
			      <td width="30%"  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_YEARS_EARNINGS') . 
				  '</td>
			       <td  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    $years . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS') . ' - ' . 
							$payback . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS') . ' = '. $yearsEarnings . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS').
					'</td>
				</tr>
				<tr>
				    <td  width="30%" style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    JText::_('COM_SOLARCALCULATOR_STANTION_EARNINGS') . 
					'</td>
					<td  style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                    $yearsEarnings . ' '.
							JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS'). ' * '.
							$netprofitEuro .' &#8364; = '.$earnings .' &#8364'.
					'</td>
				</td>	
		</table>';						
		
		switch($yearsEarnings){
		  case 1:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEAR');break;	
		  case 2:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  case 3:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  case 4:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_TEXT_YEARS');break;	
		  default:$textYear = JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS');break;		
		}					
		
							
		$output .= '<table><tr><td>'.JText::sprintf('COM_SOLARCALCULATOR_STANTION_TEXT_RESULT1', htmlspecialchars($yearsEarnings.' '.$textYear, ENT_COMPAT, 'UTF-8')).
		JText::sprintf('COM_SOLARCALCULATOR_STANTION_TEXT_RESULT2', $earnings. ' &#8364;', ENT_COMPAT, 'UTF-8').'</td></tr></table>';
		
        
		return $output;
	}
	
	public function sendEmailCommerce($station_id,$region,$poweruse,$customValue){
		$model = $this->getModel('station');
		$station  = $model->getStation($station_id);
		$settings = $model->getSettings();
		$region = $model->getRegion($region);
		$currency = $model->getCurrency();
		$result = array();
		
		$regionInsolation = $this->getInsolation($region,$settings);
		
		$stationValue = $station->station_value;
		$stationValueKwt = $stationValue * 1000;
		$stationPrice = $station->station_price;
		if($customValue){
			$stationValue = $customValue;
			$stationValueKwt = $stationValue * 1000;
			$stationPrice = $customValue * 1000000 * $settings->setting_station_price_stable;
		}
		
		//Генерация за год
		$insolation = $regionInsolation * $stationValueKwt;
		
		//Доход за год евро
		$profit = $insolation * $settings->setting_green_price_commerce;
			
		//Доход за год доллар
		$profitUsd  = $model->priceConvert('usd',$profit,'eur');
		
		//Окупаемость станции
		$payback = round($stationPrice / $profitUsd, 1);
		
		$output = '<h3>' . JText::_('COM_SOLARCALCULATOR_POPUP_TITLE_COMMERCE') . '</h3>'; 
		
		$output .= $this->inputDataHtml($region,$poweruse,$station);
		
		
				
		$output .= '<table width="100%">
		               <tr>
		                   <td  width="30%" style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . JText::_('COM_SOLARCALCULATOR_STANTION') .' '. $stationValue .
		                           JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_MWT'). ' '.
		                           JText::_('COM_SOLARCALCULATOR_STANTION_GENERATES').'
						   </td>
						   <td style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">'.
		                           $insolation .' '. JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT'). ' '.
		                           JText::_('COM_SOLARCALCULATOR_STANTION_PER_YEAR').'
						   </td>
					   </tr>
					   <tr>
					       <td  width="30%" style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                           JText::_('COM_SOLARCALCULATOR_STANTION_GREEN_TARIFF') . 
						   '</td>
						    <td style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                           $settings->setting_green_price_commerce . ' '.
							       JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR'). 
						   '</td>
						</tr>
						<tr>
						    <td width="30%" style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;"> ' . 
		                           JText::_('COM_SOLARCALCULATOR_STANTION_INCOME_PER_YEAR') . 
							'</td>
							<td style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                           $insolation . ' '.
							       JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_KWT').' * ' .
							       $settings->setting_green_price_commerce . ' '.
							       JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR').  ' = ' . $profit .' '.
							       JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_EUR').
							'</td>
					    </tr>
						<tr>
						    <td  width="30%" style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                          JText::_('COM_SOLARCALCULATOR_STANTION_PRICE') . 
							'</td>
							<td style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                          $stationPrice . ' $'.
							'</td>
						</tr>
						<tr>
						     <td  width="30%" style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                          JText::_('COM_SOLARCALCULATOR_STANTION_PAYBACK_WITOUT_PDV') . 
							'</td>
					         <td style="background-color:#e3e3e3;padding:10px;color:#000000;font-size:16px;margin-bottom:7px;">' . 
		                          $stationPrice . ' $ / '. $profitUsd .' $'. ' = '. $payback . ' '.
							      JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_YEARS').
							'</td>
						</tr>
					</table>';	
			
			
			return $output;		
										
	     
	}
	
}

?>