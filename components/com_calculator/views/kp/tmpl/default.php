<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link rel="stylesheet" href="/css/main.min.css?t=<?php echo(microtime(true)); ?>">
<link href="/css/jqvmap.css?t=<?php echo(microtime(true)); ?>" rel="stylesheet">

<!--style="display: none;"-->
<div class="AB-solar">
    <div class="container">
        <div class="head-wrap">
            <header>
                <div class="row no-gutters">
                    <div class="col-md-8 col-lg-6">
                        <div class="logo-wrap">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="logo">
                                        <img class="img-responsive"
                                             src="http://solar.3289.company/images/Calculation/logo_<?php echo JText::_('COM_CALCULATOR_KP_LANG'); ?>.png" alt="logo">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="global-text">
                                        <!--Вставка JS-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-6">
                        <div class="info-wrap">
                            <div class="row justify-content-center no-gutters">
                                <div class="col-auto">
                                    <div class="info">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto"><img
                                                        src="http://solar.3289.company/images/Calculation/phone.png"
                                                        alt="">
                                            </div>
                                            <div class="col">
                                                +38 050 635-50-50 <br>+38 068 635-50-50 <br>+38 063 635-50-50
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto align-self-center">
                                    <div class="info">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto"><img
                                                        src="http://solar.3289.company/images/Calculation/map.png"
                                                        alt=""></div>
                                            <div class="col">www.solarstrategia.com</div>
                                        </div>
                                    </div>
                                    <div class="info">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col-auto"><img
                                                        src="http://solar.3289.company/images/Calculation/email.png"
                                                        alt="">
                                            </div>
                                            <div class="col">info@solarstrategia.com</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>


        <main>
            <div class="row no-gutters">

                <!-- Left-block -->
                <div class="col-xl-6">
                    <div class="main1">
                        <div class="border-block">
                            <div class="border-block-header">
                                <h3><?php echo JText::_('COM_CALCULATOR_KP_EQUIPMENT'); ?></h3>
                            </div>
                            <!--                            <img src="http://solar.3289.company/images/Calculation/home_-->
                            <?php //echo JText::_('COM_CALCULATOR_KP_LANG'); ?><!--.png" alt="-->
                            <?php //echo JText::_('COM_CALCULATOR_KP_EQUIPMENT'); ?><!--" class="img-responsive">-->
                            <img src="http://solar.3289.company/images/Calculation/home_<?php echo JText::_('COM_CALCULATOR_KP_LANG'); ?>.png"

                                 alt="<?php echo JText::_('COM_CALCULATOR_KP_EQUIPMENT'); ?>" class="img-responsive">
                            <span class="stantion-cost"><?php echo JText::_('COM_CALCULATOR_KP_COST'); ?>: </span>
                        </div>
                        <div class="chart">
                            <h3><?php echo JText::_('COM_CALCULATOR_KP_DEADLINE'); ?></h3>
                            <div id="chart1"></div>
                        </div>
                    </div>
                    <div class="main2">
                        <div class="row justify-content-center">
                            <div class="col-sm-auto">
                                <div class="chart gray-bg">
                                    <h3><?php echo JText::_('COM_CALCULATOR_KP_SOLAR_INSOLATION'); ?></h3>
                                    <div id="chart2"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-auto">
                                <div class="chart gray-bg">
                                    <h3><?php echo JText::_('COM_CALCULATOR_KP_NET_PROFIT'); ?>, €</h3>
                                    <div id="chart3"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Right-block -->
                <div class="col-xl-6 gray-bg">
                    <div class="main3">
                        <div class="border-block">
                            <div class="border-block-header">
                                <h3><?php echo JText::_('COM_CALCULATOR_KP_PROJECT_PARAM'); ?></h3>
                            </div>
                            <table>
                                <tr>
                                    <td class="no-top-bd no-left-bd"><?php echo JText::_('COM_CALCULATOR_KP_STATION_TYPE'); ?></td>
                                    <td class="no-top-bd no-right-bd type_stantion"></td>
                                </tr>
                                <tr>
                                    <td class="no-left-bd"><?php echo JText::_('COM_CALCULATOR_KP_CONSTRUCT_TYPE'); ?></td>
                                    <td class="no-right-bd type_construct"></td>
                                </tr>
                                <tr>
                                    <td class="no-left-bd"><?php echo JText::_('COM_CALCULATOR_KP_MODULES_POWER'); ?></td>
                                    <td class="no-right-bd power"></td>
                                </tr>
                                <tr>
                                    <td class="no-left-bd"><?php echo JText::_('COM_CALCULATOR_KP_PANEL_NUMBER'); ?></td>
                                    <td class="no-right-bd number_panels"></td>
                                </tr>
                                <tr>
                                    <td class="no-left-bd no-bottom-bd"><?php echo JText::_('COM_CALCULATOR_KP_INVERTOR_POWER'); ?></td>
                                    <td class="no-right-bd no-bottom-bd invertor"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="green-block">
                            <h3><?php echo JText::_('COM_CALCULATOR_KP_CALC_PROFIT'); ?></h3>
                            <p class="content-profit">
                                <!--Вставка JS-->
                            </p>
                            <p class="small-text"><?php echo JText::_('COM_CALCULATOR_KP_PDFO'); ?>-18%
                                <br><?php echo JText::_('COM_CALCULATOR_KP_MILITARY_COLLECTION'); ?>-1.5%</p>
                        </div>
                    </div>
                    <div class="white-bg">
                        <div class="main4">
                            <h3><?php echo JText::_('COM_CALCULATOR_KP_PAYBACK_CALC'); ?></h3>
                            <strong class="content-roi">
                                <!--Вставка JS-->
                            </strong>
                            <p>
                                *<?php echo JText::_('COM_CALCULATOR_KP_NBU_CALC'); ?> <?php echo date('d.m.Y');?>
                            </p>
                            <div class="table-wrap insolation-table">
                                <table>
                                    <tr>
                                        <th class="no-top-bd no-left-bd"><?php echo JText::_('COM_CALCULATOR_KP_MONTH'); ?></th>
                                        <th class="no-top-bd"><?php echo JText::_('COM_CALCULATOR_KP_AVERAGE_ENERGY'); ?></th>
                                        <th class="no-top-bd no-right-bd green-bg"><?php echo JText::_('COM_CALCULATOR_KP_PROFIT_CLEAN'); ?>
                                            , €
                                        </th>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd  green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd">item-1</td>
                                        <td>item-2</td>
                                        <td class="no-right-bd  green-bg">item-3</td>
                                    </tr>
                                    <tr>
                                        <td class="no-left-bd no-bottom-bd">Всього</td>
                                        <td class="no-bottom-bd">item-2</td>
                                        <td class="no-right-bd no-bottom-bd green-bg">item-3</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="city"><?php echo $this->data['obl']; ?></div>
                            <div class="phrase">
                                <div>&#8220; <?php echo $this->data['phrase']->name; ?> &#8222;</div>
                                <div class="author"><?php echo $this->data['phrase']->author; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <div class="gray-bg">
            <footer>
                <?php echo JText::_('COM_CALCULATOR_KP_EARNINGS_30'); ?>:
                <span>1 590 €*13<?php echo JText::_('COM_CALCULATOR_KP_YEARS_SHORT'); ?>.</span> = 20 678 €/ 25 227$
            </footer>
        </div>
        <div class="row justify-content-center" style="display: none">
            <div class="col-auto">
                <a href="/solar.pdf?t=<?php echo(microtime(true)); ?>" id="save-btn" download="download"
                   class="button-send"><i></i><?php echo JText::_('COM_CALCULATOR_KP_SAVE'); ?></a>
            </div>
            <div class="col-auto">
                <button id="print-btn" class="button-send"><i></i><?php echo JText::_('COM_CALCULATOR_KP_PRINT'); ?>
                </button>
            </div>
            <div class="col-auto">
                <button id="order-btn" class="button-send"><i></i><?php echo JText::_('COM_CALCULATOR_KP_ORDER'); ?>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="kp" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     data-backdrop="static"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" data-error="<?php echo JText::_('COM_CALCULATOR_KP_ATTENTION'); ?>!"
                    v-if="(errr):sdfsd?sdf" id="modal-title">
                    <?php echo JText::_('COM_CALCULATOR_KP_CHANGE_CONFIG'); ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row" id="warning_1" style="display: none">
                            <div class="form-group col-md-12">
                                <label for="exampleInputEmail1"><?php echo JText::_('COM_CALCULATOR_KP_SORRY_DONBAS'); ?></label>
                            </div>
                        </div>
                        <div class="row" id="warning_2" style="display: none">
                            <div class="form-group col-md-12">
                                <label for="exampleInputEmail1"><?php echo JText::_('COM_CALCULATOR_KP_SORRY_CRIMEA'); ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 ml-auto" id="first_step" style="display: none">
                                <label for="exampleInputEmail1"><?php echo JText::_('COM_CALCULATOR_KP_CHOOSE_POWER'); ?></label>
                                <select id="select_power" class="col-md-12 ml-auto form-control">
                                    <option class="option_head" value="-1" disabled="disabled"
                                            selected><?php echo JText::_('COM_CALCULATOR_KP_CHOOSE_OPTION'); ?></option>
                                    <option value="10">10 <?php echo JText::_('COM_CALCULATOR_KP_KWT'); ?></option>
                                    <option value="20">20 <?php echo JText::_('COM_CALCULATOR_KP_KWT'); ?></option>
                                    <option value="30">30 <?php echo JText::_('COM_CALCULATOR_KP_KWT'); ?></option>
                                </select>
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group col-md-12" id="second_step" style="display: none">
                                <label for="exampleInputEmail1"><?php echo JText::_('COM_CALCULATOR_KP_CHOOSE_PLACE'); ?></label>
                                <select id="select_place" class="form-control">
                                    <option class="option_head" value="-1" disabled="disabled"
                                            selected><?php echo JText::_('COM_CALCULATOR_KP_CHOOSE_OPTION'); ?></option>
                                    <option value="1"><?php echo JText::_('COM_CALCULATOR_KP_STANDART'); ?></option>
                                    <option value="2"><?php echo JText::_('COM_CALCULATOR_KP_GROUND_CONSTRUCT'); ?></option>
                                    <option value="3"><?php echo JText::_('COM_CALCULATOR_KP_GROUND_ANGLE'); ?></option>
                                    <option value="4"><?php echo JText::_('COM_CALCULATOR_KP_TRECKER'); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12" id="third_step" style="display: none">
                                <label for="exampleInputEmail1"><?php echo JText::_('COM_CALCULATOR_KP_MONEY_PER_MONTH'); ?>
                                    ?</label>
                                <select id="select_money" class="form-control">
                                    <option value="-1" disabled="disabled"
                                            selected><?php echo JText::_('COM_CALCULATOR_KP_CHOOSE_OPTION'); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div id="send_button" class="form-group col-auto" style="display: none">
                                <button class="button-send"><i></i><?php echo JText::_('COM_CALCULATOR_KP_SEND'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!--Order-->
<div class="modal fade" id="order-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     data-backdrop="static"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"
                    id="exampleModalLongTitle"><?php echo JText::_('COM_CALCULATOR_KP_YOUR_DATA'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <div class="container-fluid order-form">
                        <form method="POST" id="form-order">
                            <div class="row">
                                <div class="form-group col-md-12 ml-auto" id="order_name">
                                    <label for="exampleInputEmail1"><span
                                                style="color: red;">* </span><?php echo JText::_('COM_CALCULATOR_KP_YOUR_NAME'); ?>
                                    </label>
                                    <input type="text" id="name" name="name" value="" required
                                           class="col-md-12 ml-auto form-control">
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-md-12" id="order-phone">
                                    <label for="exampleInputEmail1"><span
                                                style="color: red;">* </span><?php echo JText::_('COM_CALCULATOR_KP_YOUR_PHONE'); ?>
                                    </label>
                                    <input type="text" required id="phone" name="phone" value=""
                                           class="col-md-12 ml-auto form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12" id="order-email">
                                    <label for="exampleInputEmail1"><span style="color: red;">* </span>Email</label>
                                    <input type="email" required id="email" name="email" value=""
                                           class="col-md-12 ml-auto form-control">
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="form-group col-auto">
                                    <button type="submit" class="button-send">
                                        <i></i><?php echo JText::_('COM_CALCULATOR_KP_SUBMIT'); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!--modal-thankyou-->
<div class="modal fade" id="thankyou" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     data-backdrop="static"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <div class="container-fluid order-form">
                        <div class="row">
                            <div class="form-group col-md-12 ml-auto" id="thank_text" style="display: none">
                                <label for="exampleInputEmail1"><?php echo JText::_('COM_CALCULATOR_KP_MODAL_THANK'); ?></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var data = JSON.parse(<?php echo "'".json_encode($this->data)."'"; ?>);
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="./js/kp.js?t=<?php echo(microtime(true)); ?>"></script>
