<?php

// No direct access


defined('_JEXEC') or die;


jimport('joomla.application.component.controllerform');

class CalculatorControllerStation extends JControllerLegacy
{
    private $clear_profit_USD;
    private $clear_profit_EUR;
    private $stantion_cost;

    //http://solar.local/index.php?option=com_calculator&task=station.getcalc&id_place=2&id_region=2&power=30
    public function getCalc() {

        $jinput = JFactory::getApplication()->input;
        $id_place = $jinput->get('id_place');
        $id_region = $jinput->get('id_region');
        $power = $jinput->get('power');
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $construct = 1;
        if ($id_place == 2) {
            $construct = 2;
        }

        $query->from($db->quoteName('#__calculator_insolations_stations_regions', 'r'))->join('INNER', $db->quoteName('#__calculator_stations', 's') . ' ON (' . $db->quoteName('r.id_station') . ' = ' . $db->quoteName('s.id') . ')')->join('INNER', $db->quoteName('#__calculator_regions', 'reg') . ' ON (' . $db->quoteName('r.id_region') . ' = ' . $db->quoteName('reg.id') . ')')->where('r.id_region = ' . $db->quote($id_region))->where('s.power = ' . $db->quote($power))->where('r.construct = ' . $db->quote($construct))->where('s.number_panels <= reg.max')->order($db->quoteName('r.profit_eur') . ' DESC');


        $db->setQuery($query);
        $stantions = $db->loadObjectList();
        $result = [];
        $koef = $this->getKoef($id_place);
        $courses = $this->getCourses();
        foreach ($stantions as $stantion) {
            if (($id_place == 1 && $stantion->cost_dah < 1) || ($id_place == 2 && $stantion->cost_stat < 1) || ($id_place == 3 && $stantion->cost_povorot < 1) || ($id_place == 4 && $stantion->cost_track < 1)) {
                continue;
            }
            $profit_usd = $this->convertEurToUSD($courses, $stantion->profit_eur / 12 * $koef);
            $result[$stantion->id_station] = round($profit_usd);
        }

        echo json_encode($result);
        die();
    }


//http://solar.local/index.php?option=com_calculator&task=station.getall&id_place=2&id_region=2&id_station=30
    public function getAll($id_place, $id_region, $id_station) {
        $result = [];

        $region = $this->getRegionFromId($id_region);
        $stantion = $this->getStantionFromId($id_station);
        $insolation = $this->getInsolation($id_station, $id_region, $id_place);
        $koef = $this->getKoef($id_place);
        $result['global'] = $this->getGlobal($stantion, $id_place);
        $courses = $this->getCourses();
        $result['profit'] = $this->getCalcProfit($courses, $insolation, $koef);

        $result['ROI'] = $this->getROI($courses, $insolation, $stantion, $region, $koef);
        $result['cost'] = JText::_('COM_CALCULATOR_KP_COST') . ': ' . $this->stantion_cost . ' $';
        $result['h2'] = '<h2>' . JText::_('COM_CALCULATOR_KP_HEADER_PART1') . ' ' . $stantion->power . ' ' . JText::_('COM_CALCULATOR_KP_HEADER_PART2') . '</h2>';
        $result['months'] = $this->getMonths();
        $result['power'] = $stantion->power;
        $result['id_place'] = $id_place;
        $result['id_region'] = $id_region;
        $result['id_station'] = $id_station;
        $result['obl'] = JText::_('COM_CALCULATOR_REGIONS_' . strtoupper($region->sys_name)) . ' ' . JText::_('COM_CALCULATOR_KP_OBL');
        $phrase = $this->getPhrase();
        $phrase->name = str_replace("'", '&prime;', $phrase->name);
        $phrase->author = str_replace("'", '&prime;', $phrase->author);
        $result['phrase'] = $phrase;
        return $result;
        /*echo json_encode($result);
        die();*/
    }

    //http://solar.local/index.php?option=com_calculator&task=station.getpdf
    public function getPdf() {

        $html = $_POST['html'];
        if (strpos($html, '<td class="no-right-bd type_construct">Наземная с переменным углом наклона</td>') !== false) {
            $top = '298px';
            $height = '356.3mm';
        } else {
            $top = '268px';
            $height = '356.3mm';
        }
        $html = '<!doctype html><html><head><meta http-equiv="content-type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="http://solar.3289.company/css/bootstrap.min.css">
<link href="http://solar.3289.company/css/jqvmap.css" rel="stylesheet">
<link rel="stylesheet" href="http://solar.3289.company/templates/virtuastemp3/css/styles.css">
<link rel="stylesheet" href="http://solar.3289.company/css/main.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <style>  
        .info-wrap {
            height: 241px;
            width: 600px;
        }
        .logo {
            width: 502px;   
            clear: both
        }
        .logo-wrap {
            height: 281px;
            width: 601px
        }
        .global-text {
            height: 100px;
        }
        .main3 {
            position: absolute;
            z-index: 9999;
            margin-top: -180px;
            margin-left: -2px;
        }
	    .main4 {
            margin-bottom: -2px;
        }
        .white-bg{
            position: absolute;
            top:' . $top . ';
        }
	    .container .gray-bg {
            padding-top: 10px;
        }
        .AB-solar {
            margin-top: 0px;
            margin-left: -16px;
            margin-right: -15px;
        }
        .AB-solar .phrase {
            top: 20%;
        }
        .table-wrap table  th {.
            width: 172px;
            height: 103px;
        }
        .table-wrap table  th:last-child {
            background: none;
            background-image: url("http://solar.3289.company/images/Calculation/th-part.png");
            background-repeat: no-repeat;
            background-size: 167px 102px;
            background-position: left bottom;
        }
        .table-wrap table tr:last-child td:last-child {
            background: none;
            background-image: url("http://solar.3289.company/images/Calculation/td-part.png");
            background-repeat: no-repeat;
            background-size: 167px 25px;
        }
    </style>
</head><body><div class="AB-solar">' . $html;
        $html = $html . '</div></body></html>';

        file_put_contents('tml.html', $html);
        //sudo wkhtmltopdf -T 0 -R 0 -B 0 -L 0 --orientation Portrait --page-size A4 --viewport-size 1920x200 --page-width 2001px --page-height 2000px tml.html solar.pdf

        $command = "wkhtmltopdf -T 0 -R 0 -B 0 -L 0 --orientation Portrait --page-size A4 --viewport-size 1905x723 --page-width 250mm --page-height $height tml.html solar.pdf";
        system($command);

        echo '/solar.pdf';
        die();
    }

    /**
     * комплектации: стандартная крышное размещение,
     * +5% к крышной генерации -  наземная стационарная конструкция ,
     * +10% к крышной генерации - наземная        конструкция со сменным углом ,
     * +45% к крышной генерации трекер слежения за        солнцем.
     */
    private function getKoef($place) {
        $koef = 1;
        if ($place == 3) {
            $koef = 1.1;
        }
        if ($place == 4) {
            $koef = 1.45;
        }
        return $koef;
    }

    private function getGlobal($stantion, $id_place) {
        /**
         * type_stantion Тип сонячної електростанції
         * type_construct Тип конструкції
         * power Потужність фотомодулів
         * number_panels Кількість панелей
         * invertor Потужність інвертора     */
        $result['type_stantion'] = JText::_('COM_CALCULATOR_KP_STATION_TYPE_GROUND');
        if ($id_place == 1) {
            $result['type_stantion'] = JText::_('COM_CALCULATOR_KP_STATION_TYPE_ROOF');
            $result['type_construct'] = JText::_('COM_CALCULATOR_KP_TYPE_CONSTRUCT_ROOF');
            $this->stantion_cost = $stantion->cost_dah;
        }
        if ($id_place == 2) {
            $result['type_construct'] = JText::_('COM_CALCULATOR_KP_TYPE_CONSTRUCT_GROUND');
            $this->stantion_cost = $stantion->cost_stat;
        }
        if ($id_place == 3) {
            $result['type_construct'] = JText::_('COM_CALCULATOR_KP_TYPE_CONSTRUCT_GROUND_ANGLE');
            $this->stantion_cost = $stantion->cost_povorot;
        }
        if ($id_place == 4) {
            $result['type_construct'] = JText::_('COM_CALCULATOR_KP_TYPE_CONSTRUCT_TRECKER');
            $this->stantion_cost = $stantion->cost_track;
        }
        $result['number_panels'] = $stantion->number_panels . ' ' . JText::_('COM_CALCULATOR_KP_UNIT_DEFAULT');
        $result['invertor'] = $stantion->invertor . ' ' . JText::_('COM_CALCULATOR_KP_KWT');
        $result['power'] = $stantion->number_panels * $this->getSetting('panels_power') / 1000 . ' ' . JText::_('COM_CALCULATOR_KP_KWT');
        return $result;
    }

    private function convertEurToUSD($cours, $profit_eur) {
        $profit_grn = round($profit_eur * $cours['EUR']['rate'], 2);
        return round($profit_grn / $cours['USD']['rate'], 2);
    }

    private function convertUSDToEUR($cours, $profit_usd) {
        $profit_grn = round($profit_usd * $cours['USD']['rate'], 2);
        return round($profit_grn / $cours['EUR']['rate'], 2);
    }

    private function convertUAHToUSD($cours, $value) {
        return round($value / $cours['USD']['rate']);
    }

    /** Например: Если солнечная станция размещённая на крыше мощностью 10 кВт в
     * черновицкой области стоит 9055$, а количество панелей составляет 36 шт, то чистая
     * прибыль в год составляет 1590 $ (станция в год генерирует 10 973
     * кВт*0,1449євро(0,18євро(зеленый тариф)-19,5%(налог) . Чистая прибыль делится на 12
     * месяцев = 1590/12 = 132 $ в месяц.
     *
     * Зелений тариф складає - 0.18 € (5.6165 грн)
     * Прибуток в рік: 10 977 кВт*0,18 €= 1 975,8 €
     * Чистий прибуток в рік: 1 975,8- 18% - 1.5% = 1 590,5€/1 877$
     *
     * ПДФО-18%
     * Військовий збір-1.5%
     */

    private function getCalcProfit($courses, $insolation, $koef) {
        $tariffGrn = $this->getSetting('green_tarif_grn');
        $tariffGrn = floatval(str_replace(",", ".", $tariffGrn));
        $tariff = $this->getSetting('green_cost');
        $tariff = floatval(str_replace(",", ".", $tariff));
        $ins = round($insolation->insol_avg * 12 * $koef);
        $in_year = round($tariff * $ins);
        $clear_profit_EUR = round($tariff * $ins * 0.805);
        $clear_profit_USD = round($this->convertEurToUSD($courses, $clear_profit_EUR));
        $this->clear_profit_USD = $clear_profit_USD;
        $this->clear_profit_EUR = $clear_profit_EUR;


        $html = '<p>' . JText::_('COM_CALCULATOR_KP_GREEN_FARE') . ' - ' . $tariff . ' € (' . $tariffGrn . ' ' . JText::_('COM_CALCULATOR_KP_UAN') . ') <br>' . JText::_('COM_CALCULATOR_KP_PROFIT_YEAR') . ' ' . $ins . ' ' . JText::_('COM_CALCULATOR_KP_KWT') . ' * ' . $tariff . ' € = ' . $in_year . ' € <br>' . JText::_('COM_CALCULATOR_KP_PROFIT_YEAR_CLEAN') . ' ' . round($in_year) . ' € - 18% - 1.5% = ' . $clear_profit_EUR . ' €/' . $clear_profit_USD . '$</p>';


        return $html;
    }

    private function getMonths() {
        $months = [JText::_('COM_CALCULATOR_MONTH'), JText::_('COM_CALCULATOR_MONTH_JANUARY'), JText::_('COM_CALCULATOR_MONTH_FEBRUARY'), JText::_('COM_CALCULATOR_MONTH_MARCH'), JText::_('COM_CALCULATOR_MONTH_APRIL'), JText::_('COM_CALCULATOR_MONTH_MAY'), JText::_('COM_CALCULATOR_MONTH_JUNE'), JText::_('COM_CALCULATOR_MONTH_JULY'), JText::_('COM_CALCULATOR_MONTH_AUGUST'), JText::_('COM_CALCULATOR_MONTH_SEPTEMBER'), JText::_('COM_CALCULATOR_MONTH_OKTOBER'), JText::_('COM_CALCULATOR_MONTH_NOVEMBER'), JText::_('COM_CALCULATOR_MONTH_DECEMBER')];
        return $months;
    }

    private function getROI($courses, $insolation, $station, $region, $koef) {
        $counter = $this->getSetting('counter');
        $counter = $this->convertUAHToUSD($courses, $counter);
        $tariff = $this->getSetting('green_cost');
        $tariff = floatval(str_replace(",", ".", $tariff)) * 0.805;

        $cost_connect = $this->getLichCost($station->power, $region->three_phas_connect);
        $cost_connect_USD = round($cost_connect / $courses['USD']['rate']);

        $res = $this->stantion_cost + $counter + $cost_connect_USD;
        $years = round($res / $this->clear_profit_USD, 1);
        $ses_common_cost = $this->stantion_cost + $counter + $cost_connect_USD;
        $html = JText::_('COM_CALCULATOR_KP_SES_COST') . ' - ' . $this->stantion_cost . '$<br>' . JText::_('COM_CALCULATOR_KP_COUNTER_COST') . ' - ' . $counter . '$<br>' . JText::_('COM_CALCULATOR_KP_PHAS_COST') . ' - ' . $cost_connect_USD . '$<br>' . '<span style=\"padding-left:200px;\"> </span>' . JText::_('COM_CALCULATOR_KP_SES_COST_COMMON') . ' = ' . $ses_common_cost . '$<br>' . JText::_('COM_CALCULATOR_KP_PROFIT_YEAR_CLEAN') . ' - ' . $this->clear_profit_USD . '$<br>' . JText::_('COM_CALCULATOR_KP_DEADLINE') . '=' . $res . '$/' . $this->clear_profit_USD . '$=' . $years . JText::_('COM_CALCULATOR_KP_YEARS');

        $res = $res * -1;
        $graph = [];
        $year_EUR = $this->clear_profit_EUR;
        $all_EUR = round($year_EUR) * 12;
        $all_USD = $this->convertEurToUSD($courses, $all_EUR);

        for ($i = 1; $i < 13; $i++) {
            $res = $res + $this->clear_profit_USD;
            $graph[$i] = $res;
        }

        $roi_array = [[1, $insolation->insol01,], [2, $insolation->insol02,], [3, $insolation->insol03,], [4, $insolation->insol04,], [5, $insolation->insol05,], [6, $insolation->insol06,], [7, $insolation->insol07,], [8, $insolation->insol08,], [9, $insolation->insol09,], [10, $insolation->insol10,], [11, $insolation->insol11,], [12, $insolation->insol12,], [JText::_('COM_CALCULATOR_KP_TOTAL'), $insolation->insol_avg * 12]];
        //print_r($roi_array[2]); die();
        foreach ($roi_array as &$mount) {
            $mount[1] = $mount[1] * $koef;
            $mount[2] = round($mount[1] * $tariff);
            $mount[1] = round($mount[1]);
        }
        unset($mount);
        $roi = ['html' => $html, 'roi' => $roi_array, 'graph' => $graph, 'footer' => JText::_('COM_CALCULATOR_KP_EARNINGS_30') . ': <span>' . round($year_EUR) . ' €*12' . JText::_('COM_CALCULATOR_KP_YEARS_SHORT') . '.</span> = ' . round($all_EUR) . ' €/ ' . round($all_USD) . ' $'];

        return $roi;
    }

    private function getLichCost($power, $three_phas_connect) {
        $url = 'http://www.nerc.gov.ua/calculator/get_value.php';
        $ch = curl_init();

        $oblast = 10;
        $city = 1;
        $kategor = 3;
        $levelPow = 1;
        $faza = 2;
        $powerVal = 2;
        if ($power < 20) {
            $power = 17;
        }
        //echo ($three_phas_connect * $power * 1000);
        //die();
        return $three_phas_connect * $power * 1000;
    }

    private function getStantionFromId($id_station) {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_stations', 's'))->where('s.id = ' . $db->quote($id_station))->limit(1);

        $db->setQuery($query);
        $stantions = $db->loadObjectList();

        return reset($stantions);

    }

    private function getRegionFromId($id_region) {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_regions', 's'))->where('s.id = ' . $db->quote($id_region))->limit(1);

        $db->setQuery($query);
        $regions = $db->loadObjectList();

        return reset($regions);
    }

    private function getInsolation($id_station, $id_region, $id_place) {
        $db = JFactory::getDbo();

        $construct = 1;
        if ($id_place == 2) {
            $construct = 2;
        }
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_insolations_stations_regions', 'i'))->where('i.id_station = ' . $db->quote($id_station))->where('i.id_region = ' . $db->quote($id_region))->where('i.construct = ' . $db->quote($construct))->limit(1);

        $db->setQuery($query);
        $stantions = $db->loadObjectList();

        return reset($stantions);
    }

    private function getSetting($sysname) {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_settings', 's'))->where('s.sysname = ' . $db->quote($sysname))->limit(1);

        $db->setQuery($query);
        $items = $db->loadObjectList();
        $item = reset($items);

        return $item->value;
    }


    private function getCourses() {
        $url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result, true);
        $curses = [];
        foreach ($result as $item) {
            if (in_array($item['cc'], ['EUR', 'USD'])) {
                $curses[$item['cc']] = $item;
            }
        }
        return $curses;
    }

    private function getCostPanels($place, $station) {
        switch ($place) {
            case 1:
                return $station->cost_dah;
            case 2:
                return $station->cost_stat;
            case 3:
                return $station->cost_povorot;
            case 4:
                return $station->cost_track;
        }
    }

    private function getPlaceName($place) {
        switch ($place) {
            case 1:
                return JText::_('COM_CALCULATOR_KP_TYPE_CONSTRUCT_ROOF');
            case 2:
                return JText::_('COM_CALCULATOR_KP_TYPE_CONSTRUCT_GROUND');
            case 3:
                return JText::_('COM_CALCULATOR_KP_TYPE_CONSTRUCT_GROUND_ANGLE');
            case 4:
                return JText::_('COM_CALCULATOR_KP_TYPE_CONSTRUCT_TRECKER');
        }
    }

    public function getAjaxSendemail() {

        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $id_region = $_POST['region'];
        $id_place = $_POST['place'];
        $power = $_POST['power'];
        $station_id = $_POST['money'];
        $station = $this->getStantionFromId($station_id);
        $region = $this->getRegionFromId($id_region)->region_name;
        $cost = $this->getCostPanels($id_place, $station);
        $place = $this->getPlaceName($id_place);
        //$kp_html = $_POST['kp'];

        $sendResult = array();

        if (!$this->Sendemail($email, $name, $phone, $place, $power, $station, $region, $cost)) {
            $sendResult['message'] = 2;
        } else {
            $sendResult['message'] = 1;
        }

        echo json_encode($sendResult);
        exit;

    }


    public function Sendemail($email, $name, $phone, $place, $power, $station, $region, $cost) {
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array($config->get('config.mailfrom'), $config->get('config.fromname'));
        $mailer->setSender($sender);
        $mailer->addRecipient('alla@solarstrategia.com');
        $mailer->setSubject($config->get('sitename'));
        $mailer->addAttachment(JPATH_BASE . '/solar.pdf');

        $body = '
	        <table style="border: 1px solid black; border-collapse: collapse; font-size: 20px;">
    <tr>
        <td style="border: 1px solid black">ФИО</td>
        <td style="border: 1px solid black">' . $name . '</td>
    </tr>
    <tr>
        <td style="border: 1px solid black">Контактный телефон</td>
        <td style="border: 1px solid black">' . $phone . '</td>
    </tr>
    <tr>
        <td style="border: 1px solid black">Email</td>
        <td style="border: 1px solid black">' . $email . '</td>
    </tr>
    <tr>
        <td style="border: 1px solid black">Размещение</td>
        <td style="border: 1px solid black">' . $place . '</td>
    </tr>
    <tr>
        <td style="border: 1px solid black">Регион</td>
        <td style="border: 1px solid black">' . $region . '</td>
    </tr>
    <tr>
        <td style="border: 1px solid black">Мощность</td>
        <td style="border: 1px solid black">' . $power . ' ' . JText::_('COM_CALCULATOR_KP_KWT') . '</td>
    </tr>
    <tr>
        <td style="border: 1px solid black">Количество панелей</td>
        <td style="border: 1px solid black">' . $station->number_panels . '</td>
    </tr>
    <tr>
        <td style="border: 1px solid black">Цена</td>
        <td style="border: 1px solid black">' . $cost . ' $</td>
    </tr>
    <tr>
        <td style="border: 1px solid black">Мощность инвертора</td>
        <td style="border: 1px solid black">' . $station->invertor . ' ' . JText::_('COM_CALCULATOR_KP_KWT') . '</td>
    </tr>
</table>
			';

        $mailer->isHTML(true);
        $mailer->setBody($body);

        $send = $mailer->Send();
        if ($send !== true) {
            return false;
        }

        return true;
    }

    public function getPhrase() {
        $lang = JFactory::getLanguage();
        $lang = $lang->getTag();
        if ($lang == 'ru-RU') {
            $lang = 'ru';
        } else if ($lang == 'uk-UA') {
            $lang = 'ua';
        } else {
            $lang = 'en';
        }

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('name, author');
        $query->from($db->quoteName('#__calculator_phrases', 'p'))->where('p.lang = ' . $db->quote($lang))->order('RAND()')->limit(1);

        $db->setQuery($query);
        $phrases = $db->loadObjectList();

        return reset($phrases);
    }


//http://solar.local/index.php?option=com_calculator&task=station.kp&id_region=13&power=10&id_place=1&id_station=2
    public function kp() {
        $jinput = JFactory::getApplication()->input;
        $id_place = $jinput->get('place');
        $id_region = $jinput->get('region_hide');
        $id_station = $jinput->get('money');

        $data = $this->getAll($id_place, $id_region, $id_station);
        //$m = $this->getMonths();

        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $viewName = 'kp';
        $viewLayout = vRequest::getCmd('layout', 'default');

        $view = $this->getView($viewName, $viewType, '', array('layout' => $viewLayout));

        $view->data = $data;

        $view->display();
        return $this;
    }


}

?>