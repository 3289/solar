<?php

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

class CalculatorControllerStations extends JControllerAdmin
{
	public function getModel($name = 'Station', $prefix = 'CalculatorModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

}