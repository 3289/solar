<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Calculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Class CalculatorController
 *
 * @since  1.6
 */
class CalculatorController extends JControllerLegacy
{

    /**
     * Method to display a view.
     *
     * @param   boolean $cachable If true, the view output will be cached
     * @param   mixed $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
     *
     * @return  JController   This object to support chaining.
     *
     * @since    1.5
     */
    public function display($cachable = false, $urlparams = false) {
        $app = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'default');
        $app->input->set('view', $view);
        parent::display($cachable, $urlparams);


        return $this;
    }


    public function execute() {
        // Not technically needed, but a DAMN good idea.  See http://docs.joomla.org/How_to_add_CSRF_anti-spoofing_to_forms
        // JSession::checkToken();
        $task = JFactory::getApplication()->input->get('task');
        try {
            parent::execute($task);
        } catch (Exception $e) {
            echo new JResponseJson($e);
        }
    }
}
