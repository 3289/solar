<?php
/**
*
* Order items view
*
* @package	VirtueMart
* @subpackage Orders
* @author Oscar van Eijk, Valerie Isaksen
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: details_items.php 9237 2016-06-22 21:35:49Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

if($this->format == 'pdf'){
	$widthTable = '100';
	$widthTitle = '27';
} else {
	$widthTable = '100';
	$widthTitle = '49';
}

?>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr align="left" class="sectiontableheader">
        <th width="10%"></th>
		<th align="left" colspan="2" width="35%" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000; line-height:80px;"><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_NAME_TITLE') ?><br/></th>
		<th align="center" width="15%" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000;"><br/><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PRICE') ?><br/></th>
		<th align="center" width="15%" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000;"><br/><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_QTY') ?><br/></th>
		<th align="center" width="15%" style="border-top:1px solid #ffa000;"><br/><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?><br/></th>
        <th width="10%"></th>
	</tr>
<?php
	foreach($this->orderdetails['items'] as $item) {
		$qtt = $item->product_quantity ;
		$_link = JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_category_id=' . $item->virtuemart_category_id . '&virtuemart_product_id=' . $item->virtuemart_product_id, FALSE);
?>
		<tr valign="top">
            <td width="10%"></td>
			<td align="left" colspan="2" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000;">
				<?php echo $item->order_item_name; ?>
				<?php
					if(!class_exists('VirtueMartModelCustomfields'))require(VMPATH_ADMIN.DS.'models'.DS.'customfields.php');
					$product_attribute = VirtueMartModelCustomfields::CustomsFieldOrderDisplay($item,'FE');
					echo $product_attribute;
				?>
			</td>
	
			<td align="center"   class="priceCol" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000;">
				<?php
				$item->product_discountedPriceWithoutTax = (float) $item->product_discountedPriceWithoutTax;
				if (!empty($item->product_priceWithoutTax) && $item->product_discountedPriceWithoutTax != $item->product_priceWithoutTax) {
					echo '<span class="line-through">'.$this->currency->priceDisplay($item->product_item_price, $this->user_currency_id) .'</span><br />';
					echo '<span >'.$this->currency->priceDisplay($item->product_discountedPriceWithoutTax, $this->user_currency_id) .'</span><br />';
				} else {
					echo '<span >'.$this->currency->priceDisplay($item->product_item_price, $this->user_currency_id) .'</span><br />';
				}
				?>
			</td>
			<td align="center" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000;">
				<?php echo $qtt; ?>
			</td>
			
			
			<td align="center"  class="priceCol" style="border-top:1px solid #ffa000;">
				<?php
				$item->product_basePriceWithTax = (float) $item->product_basePriceWithTax;
				$class = '';
				if(!empty($item->product_basePriceWithTax) && $item->product_basePriceWithTax != $item->product_final_price ) {
					echo '<span class="line-through" >'.$this->currency->priceDisplay($item->product_basePriceWithTax,$this->user_currency_id,$qtt) .'</span><br />' ;
				}
				elseif (empty($item->product_basePriceWithTax) && $item->product_item_price != $item->product_final_price) {
					echo '<span class="line-through">' . $this->currency->priceDisplay($item->product_item_price,$this->user_currency_id,$qtt) . '</span><br />';
				}

				echo $this->currency->priceDisplay(  $item->product_subtotal_with_tax ,$this->user_currency_id); //No quantity or you must use product_final_price ?>
			</td>
            <td width="10%"></td>
		</tr>
        <tr align="left" class="sectiontableheader">
          <td width="10%"></td>
          <td align="left" colspan="2" width="35%" style="border-top:1px solid #ffa000;"></td>
          <td align="center" width="15%" style="border-top:1px solid #ffa000;"></td>
          <td align="center" width="15%" style="border-top:1px solid #ffa000;"></td>
          <td align="center" width="15%" style="border-top:1px solid #ffa000;"></td>
          <td width="10%"></td>
      </tr>

<?php
	}
?>
 

	

	
</table>
