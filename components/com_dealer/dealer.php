<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Dealer
 * @author     Andrey  Tkachenko <office@virtuas.net>
 * @copyright  2017 Andrey  Tkachenko
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Dealer', JPATH_COMPONENT);
JLoader::register('DealerController', JPATH_COMPONENT . '/controller.php');


// Execute the task.
$controller = JControllerLegacy::getInstance('Dealer');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
