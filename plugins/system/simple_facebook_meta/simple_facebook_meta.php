<?php 
/*------------------------------------------------------------------------
# install script - Simple facebook meta
# ------------------------------------------------------------------------
# author    Modulout
# copyright Copyright (C) 2012 modulout.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.modulout.com
# Version: 1.1
-------------------------------------------------------------------------*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');

class plgSystemSimple_facebook_meta extends JPlugin {

    function plgSystemFacebook_meta(&$subject, $config) {
        parent::__construct($subject, $config);
    }

    function onBeforeCompileHead() {
        $limit = $this->params->def('limitw');
        if($limit<1) 
            $limit = 247;
        else
            $limit = $this->params->def('limitw')-3;

        $option = JRequest::getVar('option', '');
        $view = JRequest::getVar('view','');
        if($view=='article' && $option=='com_content') {
            $db =  $database = JFactory::getDBO();
            $document =JFactory::getDocument();
            $id = JRequest::getInt('id');
            
            $sql = "SELECT * FROM #__content WHERE id=".$id." LIMIT 1";
            $db->setQuery($sql);		
            $item = $db->loadObject();
            
            /* Search and replace ' or " with ascii codes */
            $array_search=array("'", '"');
            $array_replace=array("&#39;","&#34;");
            $title = str_replace($array_search, $array_replace, $item->title);
            
            preg_match('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $item->introtext, $image);           
            if(empty($image))
                preg_match('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $item->fulltext, $image); 
       
            $document->addCustomTag( "<meta property='og:url' content='".JURI::current()."'>" );
            $document->addCustomTag( "<meta property='og:type' content='article'>" );
            
            if(isset($item->title))
                $document->addCustomTag( "<meta property='og:title' content='". $title ."'>" );

            if(isset($item->introtext)) {
                $description = mb_substr(strip_tags($item->introtext), 0, $limit)."...";
                $document->addCustomTag( "<meta property='og:description' content='". str_replace($array_search, $array_replace, $description) ."'>" );
            }

            if(!empty($image))
                $document->addCustomTag( "<meta property='og:image' content='". JURI::base().$image[1]."'>" );

        }
    }

}