<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array(
    /*'ADMIN_EXPORT_ONLY'=>'Admin export only',*/
    'ADVANCED' => 'Розширені',
    'DEFAULT_CURRENCY_ISO4217' => 'Код валюти - ISO 4217',
    'DEFAULT_CURRENCY_NAME' => 'Валюта',
    'DEFAULT_CURRENCY_SYMBOL' => 'Символ валюти',
    'DEFAULT_DATE_FORMAT' => 'Формат дати за замовчуванням',
    'DEFAULT_DECIMAL_SEP' => 'Десятковий розділювач',
    'DEFAULT_LANGUAGE' => 'Мова за замовчуванням',
    'DEFAULT_SYSTEM_SETTINGS' => 'Інтерфейс користувача',
    'DEFAULT_THEME' => 'Тема за замовчуванням',
    'DEFAULT_TIME_FORMAT' => 'Формат часу за замовчуванням',

    'DISPLAY_RESPONSE_TIME' => 'Відображати час відповіді сервера',



    'IMAGES' => 'Логотипи',
    'LBL_ALLOW_USER_TABS' => 'Дозволити користувачам приховувати закладки',
    'LBL_CONFIGURE_SETTINGS_TITLE' => 'Налаштування конфігурації',
    'LBL_LOGVIEW' => 'Переглянути журнал',
    'LBL_MAIL_SMTPAUTH_REQ' => 'SMTP-сервер вимагає авторизації?',
    'LBL_MAIL_SMTPPASS' => 'SMTP-пароль:',
    'LBL_MAIL_SMTPPORT' => 'SMTP-порт',
    'LBL_MAIL_SMTPSERVER' => 'Сервер вихідної пошти',
    'LBL_MAIL_SMTPUSER' => 'SMTP-логін',
    'LBL_MAIL_SMTP_SETTINGS' => 'Налаштування SMTP-сервера',
    'LBL_CHOOSE_EMAIL_PROVIDER' => 'Виберіть поштову службу:',
    'LBL_YAHOOMAIL_SMTPPASS' => 'Yahoo! - пароль:',
    'LBL_YAHOOMAIL_SMTPUSER' => 'Yahoo! - обліковий запис пошти:',
    'LBL_GMAIL_SMTPPASS' => 'Gmail - пароль:',
    'LBL_GMAIL_SMTPUSER' => 'Gmail - адреса:',
    'LBL_EXCHANGE_SMTPPASS' => 'Exchange - пароль:',
    'LBL_EXCHANGE_SMTPUSER' => 'Exchange - логін:',
    'LBL_EXCHANGE_SMTPPORT' => 'Exchange - порт серверу:',
    'LBL_EXCHANGE_SMTPSERVER' => 'Exchange - сервер:',
    'LBL_ALLOW_DEFAULT_SELECTION' => 'Дозволити користувачам використовувати цей обліковий запис для вихідних повідомлень:',
    'LBL_ALLOW_DEFAULT_SELECTION_HELP' => 'При виборі даної опції всі користувачі зможуть надсилати електронну пошту (включаючи автоматичні повідомлення про призначення записів та системні повідомлення) з використанням вказаного тут стандартного сервера вихідної пошти. В іншому випадку кожному користувачу при налаштуванні облікового запису електронної пошти необхідно вручну ввести налаштування сервера вихідної пошти.',
    'LBL_MAILMERGE' => 'Об&#039;єднання пошти',
    'LBL_MIN_AUTO_REFRESH_INTERVAL' => 'Мінімальний час автоматичного оновлення розділу',
    'LBL_MIN_AUTO_REFRESH_INTERVAL_HELP' => 'Це мінімальне значення часу автоматичного оновлення розділу. Виберіть "Ніколи", щоб повністю заборонити автоматичне оновлення розділів.',
    'LBL_MODULE_FAVICON' => 'Відображувати іконку модуля в якості іконки сайту',
    'LBL_MODULE_FAVICON_HELP' => 'Якщо ви знаходитесь в модулі, що містить іконку, то ця іконка буде використовуватися в якості іконки сайту, замість іконки сайту, що міститься в поточній темі.',
    'LBL_MODULE_NAME' => 'Налаштування конфігурації',
    'LBL_MODULE_ID' => 'Конфігуратор',
    'LBL_MODULE_TITLE' => 'Інтерфейс користувача',
    'LBL_NOTIFY_FROMADDRESS' => '"Від" Адреса:',
    'LBL_NOTIFY_SUBJECT' => 'Тема E-mail-повідомлення:',
    'LBL_PROXY_AUTH' => 'Авторизація?',
    'LBL_PROXY_HOST' => 'Проксі-сервер',
    'LBL_PROXY_ON_DESC' => 'Використовуйте проксі-сервер для налаштувань авторизації',
    'LBL_PROXY_ON' => 'Використовувати проксі-сервер?',
    'LBL_PROXY_PASSWORD' => 'Пароль',
    'LBL_PROXY_PORT' => 'Порт',
    'LBL_PROXY_TITLE' => 'Налаштування проксі-серверу',
    'LBL_PROXY_USERNAME' => 'Логін',
    'LBL_RESTORE_BUTTON_LABEL' => 'Відновити',
    'LBL_SYSTEM_SETTINGS' => 'Налаштування конфігурації',
    'LBL_SKYPEOUT_ON_DESC' => 'Дозволяти користувачам клікати на телефонних номерах з метою дозвону, використовуючи SkypeOut&reg;. Для використання цієї функції номери повинні бути правильно відформатовані: + (код країни) (телефонний номер), наприклад: +1 (555) 555-1234.',
    'LBL_SKYPEOUT_ON' => 'Enable click-to-call for phone numbers',
    'LBL_SKYPEOUT_TITLE' => 'Click-To-Call',
    'LBL_USE_REAL_NAMES' => 'Відображати повні імена',
    'LBL_USE_REAL_NAMES_DESC' => 'Відображати повні імена користувачів замість логіну в полях призначення записів.',
    'LBL_DISALBE_CONVERT_LEAD' => 'Disable convert lead action for converted leads',
    'LBL_DISALBE_CONVERT_LEAD_DESC' => 'If a lead has already been converted, enabling this option will remove the convert lead action.',
    'LBL_ENABLE_ACTION_MENU' => 'Display actions within menus',
    'LBL_ENABLE_ACTION_MENU_DESC' => 'Select to display DetailView and subpanel actions within a dropdown menu. If un-selected, the actions will display as separate buttons.',
    'LBL_ENABLE_INLINE_EDITING_LIST' => 'Enable inline editing on list view',
    'LBL_ENABLE_INLINE_EDITING_LIST_DESC' => 'Select to enable Inline Editing for fields on the list view. If unselected Inline Editing will be disabled on list view.',
    'LBL_ENABLE_INLINE_EDITING_DETAIL' => 'Enable inline editing on detail view',
    'LBL_ENABLE_INLINE_EDITING_DETAIL_DESC' => 'Select to enable Inline Editing for fields on the detail view. If unselected Inline Editing will be disabled on detail view.',
    'LBL_HIDE_SUBPANELS' => 'Collapsed subpanels',
    'LIST_ENTRIES_PER_LISTVIEW' => 'Кількість відображуваних записів на сторінці',
    'LIST_ENTRIES_PER_SUBPANEL' => 'Кількість відображуваних субпанелей на сторінці',
    'LOG_MEMORY_USAGE' => 'Вести журнал використання пам&#039;яті',
    'LOG_SLOW_QUERIES' => 'Вести журнал повільних запитів',
    'CURRENT_LOGO' => 'Поточний логотип:',
    'CURRENT_LOGO_HELP' => 'Даний логотип відображається у верхньому лівому куті додатку SuiteCRM.',
    'NEW_LOGO' => 'Вибрати логотип:',
    'NEW_LOGO_HELP' => 'The image file format can be either .png or .jpg. The maximum height is 170px, and the maximum width is 450px. Any image uploaded that is larger in any direction will be scaled to these max dimensions.',
    'NEW_LOGO_HELP_NO_SPACE' => 'The image file format can be either .png or .jpg. The maximum height is 170px, and the maximum width is 450px. Any image uploaded that is larger in any direction will be scaled to these max dimensions. Image file name must not contain a space character.',
    'SLOW_QUERY_TIME_MSEC' => 'Пороговий час виконання повільних запитів (мсек)',
    'STACK_TRACE_ERRORS' => 'Відображати трасування помилок',
    'UPLOAD_MAX_SIZE' => 'Максимальний розмір файлу, що завантажується',
    'VERIFY_CLIENT_IP' => 'Перевірка IP-адреси користувача',
    'LOCK_HOMEPAGE' => 'Заборонити користувачам налаштування головної сторінки',
    'LOCK_SUBPANELS' => 'Заборонити користувачам налаштування субпанелей',
    'MAX_DASHLETS' => 'Maximum number of SuiteCRM Dashlets on Homepage',
    'SYSTEM_NAME' => 'Системне ім\'я:',
    'SYSTEM_NAME_WIZARD' => 'Назва:',
    'SYSTEM_NAME_HELP' => 'Дана назва відображається в заголовку браузера.',
    'LBL_LDAP_TITLE' => 'Підтримка LDAP-авторизації',
    'LBL_LDAP_ENABLE' => 'Включити LDAP',
    'LBL_LDAP_SERVER_HOSTNAME' => 'Сервер:',
    'LBL_LDAP_SERVER_PORT' => 'Порт:',
    'LBL_LDAP_ADMIN_USER' => 'Ім&#039;я користувача:',
    'LBL_LDAP_ADMIN_USER_DESC' => 'Пошук користувача SuiteCRM. У разі відсутності параметра буде виконано анонімний вхід.',
    'LBL_LDAP_ADMIN_PASSWORD' => 'Пароль:',
    'LBL_LDAP_AUTHENTICATION' => 'Авторизація:',
    'LBL_LDAP_AUTHENTICATION_DESC' => 'Авторизація на сервері LDAP, використовуючи дані користувача',
    'LBL_LDAP_AUTO_CREATE_USERS' => 'Автоматичне створення користувачів:',
    'LBL_LDAP_USER_DN' => 'DN користувача:',
    'LBL_LDAP_GROUP_DN' => 'DN групи:',
    'LBL_LDAP_GROUP_DN_DESC' => 'Приклад: <em>ou=groups,dc=example,dc=com</em>',
    'LBL_LDAP_USER_FILTER' => 'Параметри:',
    'LBL_LDAP_GROUP_MEMBERSHIP' => 'Роль в групі:',
    'LBL_LDAP_GROUP_MEMBERSHIP_DESC' => 'Користувач повинен бути членом певної групи',
    'LBL_LDAP_GROUP_USER_ATTR' => 'Атрибут користувача:',
    'LBL_LDAP_GROUP_USER_ATTR_DESC' => 'Унікальний ідентифікатор користувача, що використовується для перевірки приналежності користувача до певної групи, наприклад: <em>uid</em>',
    'LBL_LDAP_GROUP_ATTR_DESC' => 'Унікальний Атрибут групи, що використовується для фільтрації за ідентифікатором користувача, наприклад: <em>memberUid</em>',
    'LBL_LDAP_GROUP_ATTR' => 'Атрибут групи:',
    'LBL_LDAP_USER_FILTER_DESC' => 'Any additional filter params to apply when authenticating users e.g.<em>is_suitecrm_user=1 or (is_suitecrm_user=1)(is_sales=1)</em>',
    'LBL_LDAP_LOGIN_ATTRIBUTE' => 'Login-атрибут:',
    'LBL_LDAP_BIND_ATTRIBUTE' => 'Bind-атрибут:',
    'LBL_LDAP_BIND_ATTRIBUTE_DESC' => 'Авторизація користувача LDAP, наприклад:[<b>AD:</b> userPrincipalName] [<b>openLDAP:</b> userPrincipalName] [<b>Mac OS X:</b> uid]',
    'LBL_LDAP_LOGIN_ATTRIBUTE_DESC' => 'Пошук користувача LDAP, наприклад:[<b>AD:</b> userPrincipalName] [<b>openLDAP:</b> dn] [<b>Mac OS X:</b> dn]',
    'LBL_LDAP_SERVER_HOSTNAME_DESC' => 'Приклад: ldap.example.com або ldaps://ldap.example.com при включенні SSL',
    'LBL_LDAP_SERVER_PORT_DESC' => 'Приклад: 389 або 636 при включенні SSL',
    'LBL_LDAP_GROUP_NAME' => 'Група:',
    'LBL_LDAP_GROUP_NAME_DESC' => 'Example <em>cn=suitecrm</em>',
    'LBL_LDAP_USER_DN_DESC' => 'Приклад: <em>ou=people,dc=example,dc=com</em>',
    'LBL_LDAP_AUTO_CREATE_USERS_DESC' => 'If an authenticated user does not exist one will be created.',
    'LBL_LDAP_ENC_KEY' => 'Ключ шифрування:',
    'DEVELOPER_MODE' => 'Режим розробки',

    'SHOW_DOWNLOADS_TAB' => 'Відображати панель завантажень',
    'LBL_LDAP_ENC_KEY_DESC' => 'Для SOAP-авторизації при використанні LDAP.',
    'LDAP_ENC_KEY_NO_FUNC_DESC' => 'Розширення php_mcrypt повинно бути включено у файлі php.ini або ж php повинен бути скомпільований із зазначенням відповідного ключа.',
    'LBL_ALL' => 'Всі',
    'LBL_MARK_POINT' => 'Встановити контрольну точку',
    'LBL_NEXT_' => 'Далі>>',
    'LBL_REFRESH_FROM_MARK' => 'Оновити з контрольної точки',
    'LBL_SEARCH' => 'Пошук:',
    'LBL_REG_EXP' => 'Реєстрація закінчується:',
    'LBL_IGNORE_SELF' => 'Ігнорувати:',
    'LBL_MARKING_WHERE_START_LOGGING' => 'Контрольна точка в журналі встановлена',
    'LBL_DISPLAYING_LOG' => 'Результат логування:',
    'LBL_YOUR_PROCESS_ID' => 'ID процесу:',
    'LBL_YOUR_IP_ADDRESS' => 'Ваш IP-адреса:',
    'LBL_IT_WILL_BE_IGNORED' => 'Буде проігноровано',
    'LBL_LOG_NOT_CHANGED' => 'Журнал не змінено',
    'LBL_ALERT_JPG_IMAGE' => 'Файл зображення повинен бути у форматі JPEG. Завантажте новий файл з розширенням .jpg.',
    'LBL_ALERT_TYPE_IMAGE' => 'Файл зображення повинен бути у форматі JPEG або PNG. Завантажте новий файл з розширенням .jpg або .png.',
    'LBL_ALERT_SIZE_RATIO' => 'Співвідношення сторін зображення повинно бути між 1:1 і 10:1. Розмір зображення буде змінено.',
    'ERR_ALERT_FILE_UPLOAD' => 'Сталася помилка під час завантаження зображення.',
    'LBL_LOGGER' => 'Параметри журналу',
    'LBL_LOGGER_FILENAME' => 'Ім&#039;я файла журналу:',
    'LBL_LOGGER_FILE_EXTENSION' => 'Розширення',
    'LBL_LOGGER_MAX_LOG_SIZE' => 'Максимальний розмір файла журналу:',
    'LBL_LOGGER_DEFAULT_DATE_FORMAT' => 'Формат дати за замовчуванням',
    'LBL_LOGGER_LOG_LEVEL' => 'Рівень деталізації:',
    'LBL_LEAD_CONV_OPTION' => 'Lead Conversion Options',
    'LEAD_CONV_OPT_HELP' => "<b>Copy</b> - Creates and relates copies of all of the Lead's activities to new records that are selected by the user during conversion. Copies are created for each of the selected records.<br><br><b>Move</b> - Moves all of the Lead's activities to a new record that is selected by the user during conversion.<br><br><b>Do Nothing</b> - Does nothing with the Lead's activities during conversion. The activities remain related to the Lead only.",
    'LBL_CONFIG_AJAX' => 'Configure AJAX User Interface',
    'LBL_CONFIG_AJAX_DESC' => 'Enable or disable the use of the AJAX UI for specific modules.',
    'LBL_LOGGER_MAX_LOGS' => 'Максимальна кількість файлів журналу:',
    'LBL_LOGGER_FILENAME_SUFFIX' => 'Додатковий суфікс імені файла журналу:',
    'LBL_VCAL_PERIOD' => 'Параметри доступності vCal:',
    'LBL_IMPORT_MAX_RECORDS' => 'Імпорт - максимальна кількість рядків:',
    'LBL_IMPORT_MAX_RECORDS_HELP' => 'Specify how many rows are allowed within import files.<br>If the number of rows in an import file exceeds this number, the user will be alerted.<br>If no number is entered, an unlimited number of rows are allowed.',
	'vCAL_HELP' => 'Використовуйте цей параметр для зазначення кількості місяців, протягом яких можлива публікація даних про зайнятість (дзвінки та зустрічі) з календаря. <br />При включеній публікації допустимі значення від 1 до 12 місяців. Для виключення можливості публікації введіть "0".',
    'LBL_PDFMODULE_NAME' => 'Налаштування PDF',
    'SUGARPDF_BASIC_SETTINGS' => 'Властивості документа',
    'SUGARPDF_ADVANCED_SETTINGS' => 'Розширені налаштування',
    'SUGARPDF_LOGO_SETTINGS' => 'Логотипи',

    'PDF_AUTHOR' => 'Автор',
    'PDF_AUTHOR_INFO' => 'Інформація про автора відображається у властивостях документа.',

    'PDF_HEADER_LOGO' => 'Для комерційних пропозицій',
    'PDF_HEADER_LOGO_INFO' => 'Цей логотип відображається в заголовку PDF-документів комерційних пропозицій за замовчуванням.',

    'PDF_NEW_HEADER_LOGO' => 'Вибрати новий логотип для комерційних пропозицій',
    'PDF_NEW_HEADER_LOGO_INFO' => 'Формат файлу може бути .jpg або .png. (Тільки .jpg для EZPDF)<br />Рекомендований розмір: 212x40 px.',

    'PDF_SMALL_HEADER_LOGO' => 'Для звітів',
    'PDF_SMALL_HEADER_LOGO_INFO' => 'This image appears in the default Header in Reports PDF Documents.<br> This image also appears in the top left-hand corner of the SuiteCRM application.',

    'PDF_NEW_SMALL_HEADER_LOGO' => 'Вибрати новий логотип для звітів',
    'PDF_NEW_SMALL_HEADER_LOGO_INFO' => 'Формат файлу може бути .jpg або .png. (Тільки .jpg для EZPDF)<br />Рекомендований розмір: 212x40 px.',

    'PDF_FILENAME' => 'Ім&#039;я файлу за замовчуванням',
    'PDF_FILENAME_INFO' => 'Назва за замовчуванням для згенерованих PDF-файлів',

    'PDF_TITLE' => 'Назва',
    'PDF_TITLE_INFO' => 'Назва відобразиться у властивостях документа.',

    'PDF_SUBJECT' => 'Тема',
    'PDF_SUBJECT_INFO' => 'Тема відобразиться у властивостях документа.',

    'PDF_KEYWORDS' => 'Ключове слово (слова)',
    'PDF_KEYWORDS_INFO' => 'Зв&#039;язати ключові слова з документом, зазвичай у вигляді "ключовеслово1 ключовеслово2..."',

    'PDF_COMPRESSION' => 'Стиснення',
    'PDF_COMPRESSION_INFO' => 'Увімкнути або вимкнути стискання сторінки.<br />Якщо включено, внутрішнє подання кожної сторінки стискається, що в підсумку призводить до коефіцієнту стиснення документу: 2.',

    'PDF_JPEG_QUALITY' => 'Якість JPEG (1-100)',
    'PDF_JPEG_QUALITY_INFO' => 'Налаштувати з якістю стиснення JPEG за замовчуванням (1-100)',

    'PDF_PDF_VERSION' => 'Версія PDF',
    'PDF_PDF_VERSION_INFO' => 'Встановити версію PDF (перевірити посилання на PDF для діючих користувачів).',

    'PDF_PROTECTION' => 'Захист документа',
    'PDF_PROTECTION_INFO' => 'Налаштувати захист документу<br / > - копіювання: копіювати текст та логотип в буфер обміну<br / > - друк: роздрукувати документ<br / > - змінити: змінити документ (окрім коментарів та форм)<br / > - коментарі та форми: додати коментарі та форми<br />Примітка: захист від змін можна налаштувати людям, які володіють повним продуктом Acrobat',

    'PDF_USER_PASSWORD' => 'Пароль користувача',
    'PDF_USER_PASSWORD_INFO' => 'Якщо пароль не налаштовано, документ відкриється, як звичайно.<br />Якщо налаштовано пароль користувача, програма перегляду PDF попросить ввести пароль перед відображенням документа.<br />Якщо головний пароль буде відрізнятися від користувацького, він буде використаний для повного доступу.',

    'PDF_OWNER_PASSWORD' => 'Пароль власника',
    'PDF_OWNER_PASSWORD_INFO' => 'Якщо налаштовано пароль користувача, програма перегляду PDF попросить ввести пароль перед відображенням документа.<br />Якщо головний пароль буде відрізнятися від користувацького, він буде використаний для повного доступу.',

    'PDF_ACL_ACCESS' => 'Контроль доступу',
    'PDF_ACL_ACCESS_INFO' => 'Контроль доступу за замовчуванням для створення PDF.',

    'K_CELL_HEIGHT_RATIO' => 'Коефіцієнт висоти комірок',
    'K_CELL_HEIGHT_RATIO_INFO' => 'Якщо висота комірок менше, ніж (Font Height x Cell Height Ratio), то (Font Height x Cell Height Ratio) використовується в якості висоти комірок. (Font Height x Cell Height Ratio) також використовується в якості висоти комірки, коли висоту не задано.',

    'K_SMALL_RATIO' => 'Фактор маленького шрифту',
    'K_SMALL_RATIO_INFO' => 'Фактор зменшення для маленького шрифту.',

    'PDF_IMAGE_SCALE_RATIO' => 'Відношення масштабу логотипу',
    'PDF_IMAGE_SCALE_RATIO_INFO' => 'Відношення, яке використовується для масштабування логотипів',

    'PDF_UNIT' => 'Одиниця виміру',
    'PDF_UNIT_INFO' => 'Одиниця виміру документа',
    'PDF_GD_WARNING' => 'У вас не встановлена бібліотека GD для PHP. Без бібліотеки GD, у файлах PDF можуть відображатися тільки логотипи у форматі JPEG.',
    'ERR_EZPDF_DISABLE' => 'Увага: клас EZPDF виключений з конфігурації таблиці і замість нього встановлено клас PDF. Збережіть цю форму, щоб встановити TCPDF в якості класу PDF та повернутися до стабільного стану.',
    'LBL_IMG_RESIZED' => "(розмір змінено для відображення)",


    'LBL_FONTMANAGER_BUTTON' => 'Менеджер шрифтів PDF',
    'LBL_FONTMANAGER_TITLE' => 'Менеджер шрифтів PDF',
    'LBL_FONT_BOLD' => 'Напівжирний',
    'LBL_FONT_ITALIC' => 'Курсив',
    'LBL_FONT_BOLDITALIC' => 'Напівжирний/Курсив',
    'LBL_FONT_REGULAR' => 'Звичайний',

    'LBL_FONT_TYPE_CID0' => 'CID-0',
    'LBL_FONT_TYPE_CORE' => 'Ядро',
    'LBL_FONT_TYPE_TRUETYPE' => 'TrueType',
    'LBL_FONT_TYPE_TYPE1' => 'Type1',
    'LBL_FONT_TYPE_TRUETYPEU' => 'TrueTypeUnicode',

    'LBL_FONT_LIST_NAME' => 'Назва',
    'LBL_FONT_LIST_FILENAME' => 'Filename',
    'LBL_FONT_LIST_TYPE' => 'Тип',
    'LBL_FONT_LIST_STYLE' => 'Стиль',
    'LBL_FONT_LIST_STYLE_INFO' => 'Стиль шрифту',
    'LBL_FONT_LIST_ENC' => 'Кодування',
    'LBL_FONT_LIST_EMBEDDED' => 'Вбудований',
    'LBL_FONT_LIST_EMBEDDED_INFO' => 'Відмітьте, щоб включити цей шрифт у файл PDF',
    'LBL_FONT_LIST_CIDINFO' => 'Інформація про CID',

    'LBL_FONT_LIST_FILESIZE' => 'Розмір шрифту',
    'LBL_ADD_FONT' => 'Додати шрифт',
    'LBL_BACK' => 'Назад',
    'LBL_REMOVE' => 'стер.',
    'LBL_JS_CONFIRM_DELETE_FONT' => 'Ви впевнені, що хочете видалити цей шрифт?',

    'LBL_ADDFONT_TITLE' => 'Додати шрифт для PDF',
    'LBL_PDF_ENCODING_TABLE' => 'Таблиця кодувань',
    'LBL_PDF_ENCODING_TABLE_INFO' => 'Назва таблиці кодувань<br />Ця опція ігнорується для TrueType Unicode, OpenType Unicode та символьних шрифтів. Кодування визначає відповідність між кодом (від 0 до 255) і символом, що містяться у шрифті. Перші 128 позицій є фіксованими і відповідають ASCII.',
    'LBL_PDF_FONT_FILE' => 'Файл шрифту',
    'LBL_PDF_FONT_FILE_INFO' => 'файл .ttf або .otf або .pfb',
    'LBL_PDF_METRIC_FILE' => 'Файл вимірювань',
    'LBL_PDF_METRIC_FILE_INFO' => 'файл .afm або .ufm',
    'LBL_ADD_FONT_BUTTON' => 'Додати',
    'JS_ALERT_PDF_WRONG_EXTENSION' => 'Розширення даного файлу не підходить.',

    'ERR_MISSING_CIDINFO' => 'Поле з інформацією про CID не може бути порожнім.',
    'LBL_ADDFONTRESULT_TITLE' => 'Результат додавання шрифту',
    'LBL_STATUS_FONT_SUCCESS' => 'SUCCESS : The font has been added to SuiteCRM.',
    'LBL_STATUS_FONT_ERROR' => 'ПОМИЛКА: Шрифт не був доданий. Перегляньте журнал помилок.',

// Font manager
    'ERR_PDF_NO_UPLOAD' => 'Помилка при завантаженні шрифту у файл вимірювань.',

// Wizard
	//Wizard Scenarios
	'LBL_WIZARD_SCENARIOS' => 'Your Scenarios',
	'LBL_WIZARD_SCENARIOS_EMPTY_LIST' => 'No scenarios have been configured',
	'LBL_WIZARD_SCENARIOS_DESC' => 'Choose which scenarios are appropriate for your installation. These options can be changed post-install.',

    'LBL_WIZARD_TITLE' => 'Майстер налаштування системи',
    'LBL_WIZARD_WELCOME_TAB' => 'Ласкаво просимо',
    'LBL_WIZARD_WELCOME_TITLE' => 'Ласкаво просимо в SuiteCRM!',
    'LBL_WIZARD_WELCOME' => 'Click <b>Next</b> to brand, localize and configure SuiteCRM now. If you wish to configure SuiteCRM later, click <b>Skip</b>.',
    'LBL_WIZARD_NEXT_BUTTON' => 'Далі >',
    'LBL_WIZARD_BACK_BUTTON' => '< Назад',
    'LBL_WIZARD_SKIP_BUTTON' => 'Пропустити',
    'LBL_WIZARD_CONTINUE_BUTTON' => 'Продовжити',
    'LBL_WIZARD_FINISH_TITLE' => 'Налаштування основних параметрів системи завершено',
    'LBL_WIZARD_SYSTEM_TITLE' => 'Налаштування логотипу',
    'LBL_WIZARD_SYSTEM_DESC' => 'Вкажіть назву вашої організації і виберіть логотип.',
    'LBL_WIZARD_LOCALE_DESC' => 'Вкажіть, яким чином повинні бути представлені дані в системі, грунтуючись на Вашому географічному положенні. Вказані тут параметри будуть параметрами за замовчуванням. Надалі користувачі зможуть змінити параметри на свій розсуд.',
    'LBL_WIZARD_SMTP_DESC' => 'Обліковий запис вихідної пошти буде використовуватися для відправки вихідної пошти, в тому числі для повідомлень про призначення записів і завдань, та листів з інформацією про новий пароль. Електронна адреса даного облікового запису буде фігурувати в листах в якості відправника.',
    'LBL_LOADING' => 'Завантаження...' /*for 508 compliance fix*/,
    'LBL_DELETE' => 'Видалити' /*for 508 compliance fix*/,
    'LBL_WELCOME' => 'Ласкаво просимо' /*for 508 compliance fix*/,
    'LBL_LOGO' => 'Logo' /*for 508 compliance fix*/,
    'LBL_ENABLE_HISTORY_CONTACTS_EMAILS' => 'Show related contacts\' emails in History subpanel for modules',

);


?>
