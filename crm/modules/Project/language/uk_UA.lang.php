<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Проект',
    'LBL_MODULE_TITLE' => 'Проекти: Головна',
    'LBL_SEARCH_FORM_TITLE' => 'Знайти проект',
    'LBL_LIST_FORM_TITLE' => 'Список проектів',
    'LBL_HISTORY_TITLE' => 'Історія',
    'LBL_ID' => 'ID',
    'LBL_DATE_ENTERED' => 'Дата створення:',
    'LBL_DATE_MODIFIED' => 'Дата зміни:',
    'LBL_ASSIGNED_USER_ID' => 'Відповідальний (а):',
    'LBL_ASSIGNED_USER_NAME' => 'Project Manager:',
    'LBL_MODIFIED_USER_ID' => 'Змінено користувачем (ID):',
    'LBL_CREATED_BY' => 'Ким створено:',
    'LBL_NAME' => 'Назва:',
    'LBL_DESCRIPTION' => 'Опис:',
    'LBL_DELETED' => 'Видалено:',
    'LBL_DATE' => 'Дата',
    'LBL_DATE_START' => 'Дата початку:',
    'LBL_DATE_END' => 'Дата закінчення:',
    'LBL_PRIORITY' => 'Пріоритет:',
    'LBL_LIST_NAME' => 'Назва',
    'LBL_LIST_TOTAL_ESTIMATED_EFFORT' => 'Попередня оцінка спільних зусиль (год.)',
    'LBL_LIST_TOTAL_ACTUAL_EFFORT' => 'Реальні спільні зусилля (год.)',
    'LBL_LIST_END_DATE' => 'Дата закінчення',
    'LBL_PROJECT_SUBPANEL_TITLE' => 'Проекти',
    'LBL_PROJECT_TASK_SUBPANEL_TITLE' => 'Проектні завдання',
    'LBL_OPPORTUNITY_SUBPANEL_TITLE' => 'Угоди',
	'LBL_PROJECT_PREDECESSOR_NONE' => 'Немає',
	'LBL_ALL_PROJECTS' => 'All Projects',
	'LBL_ALL_USERS' => 'All Users',
	'LBL_ALL_CONTACTS' => 'All Contacts',

    // quick create label
    'LBL_NEW_FORM_TITLE' => 'Новий проект',
    'LNK_NEW_PROJECT' => 'Новий проект',
    'LNK_PROJECT_LIST' => 'Переглянути список проектів',
    'LNK_NEW_PROJECT_TASK' => 'Створити завдання по проекту',
    'LNK_PROJECT_TASK_LIST' => 'Проектні завдання',
    'LBL_DEFAULT_SUBPANEL_TITLE' => 'Проекти',
    'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Дії',
    'LBL_HISTORY_SUBPANEL_TITLE' => 'Історія',
    'LBL_CONTACTS_SUBPANEL_TITLE' => 'Контакти',
    'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Контрагенти',
    'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => 'Угоди',
    'LBL_CASES_SUBPANEL_TITLE' => 'Звернення',
    'LBL_BUGS_SUBPANEL_TITLE' => 'Помилки',
    'LBL_TASK_ID' => 'ID',
    'LBL_TASK_NAME' => 'Назва задачі',
    'LBL_DURATION' => 'Тривалість',
    'LBL_ACTUAL_DURATION' => 'Фактична тривалість',
    'LBL_START' => 'Початок',
    'LBL_FINISH' => 'Готово',
    'LBL_PREDECESSORS' => 'Попередники',
    'LBL_PERCENT_COMPLETE' => '% виконання',
    'LBL_MORE' => 'Більше...',
    'LBL_OPPORTUNITIES' => 'Угоди',
    'LBL_NEXT_WEEK' => 'Наступна',
    'LBL_PROJECT_INFORMATION' => 'Огляд проекту',
    'LBL_EDITLAYOUT' => 'Правка розташування' /*for 508 compliance fix*/,
    'LBL_PROJECT_TASKS_SUBPANEL_TITLE' => 'Проектні завдання',
    'LBL_VIEW_GANTT_TITLE' => 'View Gantt',
    'LBL_VIEW_GANTT_DURATION' => 'Тривалість',
    'LBL_TASK_TITLE' => 'Edit Task',
    'LBL_DURATION_TITLE' => 'Edit Duration',
    'LBL_LAG' => 'Lag',
    'LBL_DAYS' => 'Днів',
    'LBL_HOURS' => 'Годин',
    'LBL_MONTHS' => 'Місяців',
    'LBL_SUBTASK' => 'Завдання',
    'LBL_MILESTONE_FLAG' => 'Milestone',
    'LBL_ADD_NEW_TASK' => 'Add New Task',
    'LBL_DELETE_TASK' => 'Delete Task',
    'LBL_EDIT_TASK_PROPERTIES' => 'Edit task properties.',
    'LBL_PARENT_TASK_ID' => 'Parent Task Id',
    'LBL_RESOURCE_CHART' => 'Діаграма ресурсів',
    'LBL_RELATIONSHIP_TYPE' => 'Relation Type',
    'LBL_ASSIGNED_TO' => 'Керівник проекту',
    'LBL_AM_PROJECTTEMPLATES_PROJECT_1_FROM_AM_PROJECTTEMPLATES_TITLE' => 'Project Template',
    'LBL_STATUS' => 'Статус:',
    'LBL_LIST_ASSIGNED_USER_ID' => 'Керівник проекту',
    'LBL_TOOLTIP_PROJECT_NAME' => 'Проекти',
    'LBL_TOOLTIP_TASK_NAME' => 'Назва задачі',
    'LBL_TOOLTIP_TITLE' => 'Tasks on this day',
    'LBL_TOOLTIP_TASK_DURATION' => 'Тривалість',
    'LBL_RESOURCE_TYPE_TITLE_USER' => 'Resource is a User',
    'LBL_RESOURCE_TYPE_TITLE_CONTACT' => 'Resource is a Contact',
    'LBL_RESOURCE_CHART_PREVIOUS_MONTH' => 'Попередній місяць',
    'LBL_RESOURCE_CHART_NEXT_MONTH' => 'Наступний місяць',
    'LBL_RESOURCE_CHART_WEEK' => 'Тиждень',
    'LBL_RESOURCE_CHART_DAY' => 'День',
    'LBL_RESOURCE_CHART_WARNING' => 'No resources have been assigned to a project.',
    'LBL_PROJECT_DELETE_MSG' => 'Are you sure you want to delete this Project and its related Tasks?',
    'LBL_LIST_MY_PROJECT' => 'Мої проекти',
    'LBL_LIST_ASSIGNED_USER' => 'Керівник проекту',
    'LBL_UNASSIGNED' => 'Unassigned',
    'LBL_PROJECT_USERS_1_FROM_USERS_TITLE' => 'Resources',

	'LBL_EMAIL' => 'E-mail',
	'LBL_PHONE' => 'Тел. робочий:',
	'LBL_ADD_BUTTON'=> 'Додати',
	'LBL_ADD_INVITEE' => 'Add Resource',
	'LBL_FIRST_NAME' => 'Ім\'я',
	'LBL_LAST_NAME' => 'Прізвище',
	'LBL_SEARCH_BUTTON'=> 'Знайти',
	'LBL_EMPTY_SEARCH_RESULT' => 'Sorry, no results were found. Please create an invitee below.',
	'LBL_CREATE_INVITEE' => 'Create an Resource',
	'LBL_CREATE_CONTACT' => 'Як контакт',
	'LBL_CREATE_AND_ADD' => 'Створити та додати',
	'LBL_CANCEL_CREATE_INVITEE' => 'Скасування',
	'LBL_NO_ACCESS' => 'You have no access to create $module',
	'LBL_SCHEDULING_FORM_TITLE' => 'Resource List',
	'LBL_REMOVE' => 'стер.',
	'LBL_VIEW_DETAIL' => 'Перегляд деталей',
	'LBL_OVERRIDE_BUSINESS_HOURS' => 'Consider Working days',

	'LBL_PROJECTS_SEARCH' => 'Search Projects',
	'LBL_USERS_SEARCH' => 'Search Users',
	'LBL_CONTACTS_SEARCH' => 'Select  Contacts',
	'LBL_RESOURCE_CHART_SEARCH_BUTTON' => 'Знайти',
	
	'LBL_CHART_TYPE' => 'Тип',
	'LBL_CHART_WEEKLY' => 'Щотижня',
	'LBL_CHART_MONTHLY' => 'Щомісяця',	
	'LBL_CHART_QUARTERLY' => 'Щокварталу',

	'LBL_RESOURCE_CHART_MONTH' => 'Місяць',
	'LBL_RESOURCE_CHART_QUARTER' => 'Quarter',

 	'LBL_PROJECT_CONTACTS_1_FROM_CONTACTS_TITLE' => 'Project Contacts from Contacts Title',
 	'LBL_AM_PROJECTTEMPLATES_PROJECT_1_FROM_PROJECT_TITLE' => 'Project Templates: Project from Project Title',
 	'LBL_AOS_QUOTES_PROJECT' => 'Quotes: Project',

);
?>
