<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
	'LBL_ACCEPT_THIS'=>'Прийняти?',
	'LBL_ADD_BUTTON'=> 'Додати',
	'LBL_ADD_INVITEE' => 'Додати запрошених',
	'LBL_CONTACT_NAME' => 'Контактна особа:',
	'LBL_CONTACTS_SUBPANEL_TITLE' => 'Контакти',
	'LBL_CREATED_BY'=>'Ким створено',
	'LBL_DATE_END'=>'Дата закінчення',
	'LBL_DATE_TIME' => 'Дата і час початку:',
	'LBL_DATE' => 'Дата початку:',
	'LBL_DEFAULT_SUBPANEL_TITLE' => 'Зустрічі',
	'LBL_DESCRIPTION' => 'Опис:',
    'LBL_DIRECTION' => 'Сортування:',
	'LBL_DURATION_HOURS' => 'Тривалість (годин):',
	'LBL_DURATION_MINUTES' => 'Тривалість (хвилин):',
	'LBL_DURATION' => 'Тривалість:',
	'LBL_EMAIL' => 'E-mail-адреса',
	'LBL_FIRST_NAME' => 'Ім\'я',
	'LBL_HISTORY_SUBPANEL_TITLE' => 'Нотатки',
	'LBL_HOURS_ABBREV' => 'год.',
	'LBL_HOURS_MINS' => '(годин:хвилин)',
	'LBL_INVITEE' => 'Запрошені',
	'LBL_LAST_NAME' => 'Прізвище',
	'LBL_ASSIGNED_TO_NAME'=>'Відповідальний (- а):',
	'LBL_LIST_ASSIGNED_TO_NAME' => 'Відповідальний (- а)',
	'LBL_LIST_CLOSE' => 'Закрити',
	'LBL_LIST_CONTACT' => 'Контакт',
	'LBL_LIST_DATE_MODIFIED'=>'Дата зміни',
	'LBL_LIST_DATE' => 'Дата початку',
	'LBL_LIST_DIRECTION' => 'Сортування',
	'LBL_LIST_DUE_DATE'=>'Дата завершення',
	'LBL_LIST_FORM_TITLE' => 'Список зустрічей',
	'LBL_LIST_MY_MEETINGS' => 'Мої зустрічі',
	'LBL_LIST_RELATED_TO' => 'Відноситься до',
	'LBL_LIST_STATUS'=>'Статус',
	'LBL_LIST_SUBJECT' => 'Тема',
	'LBL_LEADS_SUBPANEL_TITLE' => 'Попередні контакти',
	'LBL_LOCATION' => 'Місце проведення:',
	'LBL_MINSS_ABBREV' => 'хвил.',
	'LBL_MODIFIED_BY'=>'Остання зміна:',
	'LBL_MODULE_NAME' => 'Зустрічі',
	'LBL_MODULE_TITLE' => 'Зустрічі - Головна',
	'LBL_NAME' => 'Назва',
	'LBL_NEW_FORM_TITLE' => 'Нова зустріч',
	'LBL_OUTLOOK_ID' => 'ID в Outlook',
	'LBL_SEQUENCE' => 'Meeting update sequence',
	'LBL_PHONE' => 'Тел. робочий:',
	'LBL_REMINDER_TIME'=>'Час нагадування',
    'LBL_EMAIL_REMINDER_SENT' => 'Email reminder sent',
	'LBL_REMINDER' => 'Нагадати:',
	'LBL_REMINDER_POPUP' => 'Спливаюче',
    'LBL_REMINDER_EMAIL_ALL_INVITEES' => 'Повідомлення всім запрошеним',
    'LBL_EMAIL_REMINDER' => 'Надіслати нагадування',
    'LBL_EMAIL_REMINDER_TIME' => 'Час надсилання нагадування',
    'LBL_REMOVE' => 'стер.',
	'LBL_SCHEDULING_FORM_TITLE' => 'Планування',
	'LBL_SEARCH_BUTTON'=> 'Пошук',
	'LBL_SEARCH_FORM_TITLE' => 'Знайти зустріч',
	'LBL_SEND_BUTTON_LABEL'=>'Надсилання запрошень',
	'LBL_SEND_BUTTON_TITLE'=>'Відправка запрошень',
	'LBL_STATUS' => 'Статус',
    'LBL_TYPE' => 'Тип зустрічі',
    'LBL_PASSWORD' => 'Пароль зустрічі',
    'LBL_URL' => 'Розпочати зустріч/взяти участь',
    'LBL_HOST_URL' => 'Host URL',
    'LBL_DISPLAYED_URL' => 'Display URL',
    'LBL_CREATOR' => 'Творець зустрічі',
    'LBL_EXTERNALID' => 'ID зовнішнього додатку',
	'LBL_SUBJECT' => 'Тема:',
	'LBL_TIME' => 'Час початку:',
	'LBL_USERS_SUBPANEL_TITLE' => 'Користувачі',
    'LBL_PARENT_TYPE' => 'Тип початкового запису',
    'LBL_PARENT_ID' => 'ID початкового запису',
	'LNK_MEETING_LIST'=>'Зустрічі',
	'LNK_NEW_APPOINTMENT' => 'Нова зустріч',
	'LNK_NEW_MEETING'=>'Нова зустріч',
	'LNK_IMPORT_MEETINGS' => 'Імпорт зустрічей',

    'LBL_CREATED_USER' => 'Створено користувачем',
    'LBL_MODIFIED_USER' => 'Змінено користувачем',
    'NOTICE_DURATION_TIME' => 'Тривалість розмови повинна бути більше 0 хвилин',
    'LBL_MEETING_INFORMATION' => 'Попередній перегляд зустрічі', //Can be translated in all caps. This string will be used by SuiteP template menu actions
	'LBL_LIST_JOIN_MEETING' => 'Взяти участь у зустрічі',
    'LBL_ACCEPT_STATUS' => 'Підтвердження',
    'LBL_ACCEPT_LINK' => 'Accept Link',
    // You are not invited to the meeting messages
    'LBL_EXTNOT_MAIN' => 'Ви не можете взяти участь в цій зустрічі, оскільки Ви не запрошені.',
    'LBL_EXTNOT_RECORD_LINK' => 'Огляд зустрічі',

    //cannot start messages
    'LBL_EXTNOSTART_MAIN' => 'Ви не можете почати цю зустріч, оскільки Ви не є Адміністратором або творцем зустрічі.',

    // create invitee functionallity
    'LBL_CREATE_INVITEE' => 'Create an invitee',
    'LBL_CREATE_CONTACT' => 'Як контакт',  // Create invitee functionallity
    'LBL_CREATE_LEAD' => 'Як лід', // Create invitee functionallity
    'LBL_CREATE_AND_ADD' => 'Створити та додати', // Create invitee functionallity
    'LBL_CANCEL_CREATE_INVITEE' => 'Скасування',
    'LBL_EMPTY_SEARCH_RESULT' => 'Sorry, no results were found. Please create an invitee below.',  // create invitee functionallity
    'LBL_NO_ACCESS' => 'You have no access to create $module',  // Create invitee functionallity

    'LBL_REPEAT_TYPE' => 'Repeat Type',
    'LBL_REPEAT_INTERVAL' => 'Repeat Interval',
    'LBL_REPEAT_DOW' => 'Repeat Dow',
    'LBL_REPEAT_UNTIL' => 'Повторювати поки',
    'LBL_REPEAT_COUNT' => 'Repeat Count',
    'LBL_REPEAT_PARENT_ID' => 'Repeat Parent ID',
    'LBL_RECURRING_SOURCE' => 'Recurring Source',

    'LBL_SYNCED_RECURRING_MSG' => 'This meeting originated in another system and was synced to SuiteCRM. To make changes, go to the original meeting within the other system. Changes made in the other system can be synced to this record.',
    'LBL_RELATED_TO' => 'Related to:',

	// for reminders
	'LBL_REMINDERS' => 'Нагадування',
	'LBL_REMINDERS_ACTIONS' => 'Actions:',
	'LBL_REMINDERS_POPUP' => 'Спливаюче',
	'LBL_REMINDERS_EMAIL' => 'Email invitees',
	'LBL_REMINDERS_WHEN' => 'Коли:',
	'LBL_REMINDERS_REMOVE_REMINDER' => 'Видалити нагадування',
	'LBL_REMINDERS_ADD_ALL_INVITEES' => 'Add All Invitees',
	'LBL_REMINDERS_ADD_REMINDER' => 'Додати нагадування',
);
?>
