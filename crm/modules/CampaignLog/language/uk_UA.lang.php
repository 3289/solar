<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
	'LBL_LIST_ID'=>'Список потенційних клієнтів',
	'LBL_ID' => 'Ідентифікатор',
	'LBL_TARGET_TRACKER_KEY'=>'Ключ трекера потенційних клієнтів',
	'LBL_TARGET_ID'=>'Потенційний клієнт',
	'LBL_TARGET_TYPE'=>'Тип потенційного клієнта',
	'LBL_ACTIVITY_TYPE' =>'Тип дії',
	'LBL_ACTIVITY_DATE' => 'Дата дії',
	'LBL_RELATED_ID' => 'Пов&#039;язаний з',
	'LBL_RELATED_TYPE'=>'Тип залежності',
	'LBL_DELETED' => 'Видалено',
	'LBL_MODULE_NAME' =>'Журнал маркет. кампанії',
	'LBL_LIST_RECIPIENT_EMAIL'=>'E-mail-адреса одержувача',
	'LBL_LIST_RECIPIENT_NAME'=>'Ім&#039;я одержувача',
	'LBL_ARCHIVED'=>'Архів',
	'LBL_HITS'=>'Хіти',

	'LBL_CAMPAIGN_NAME' => 'Назва:',
  	'LBL_CAMPAIGN' => 'Маркетингова кампанія:',
  	'LBL_NAME' => 'Назва: ',
  	'LBL_INVITEE' => 'Контакти',
  	'LBL_LIST_CAMPAIGN_NAME' => 'Маркетингова кампанія',
  	'LBL_LIST_STATUS' => 'Статус',
  	'LBL_LIST_TYPE' => 'Вид',
  	'LBL_LIST_END_DATE' => 'Дата закінчення',
  	'LBL_DATE_ENTERED' => 'Дата вводу',
  	'LBL_DATE_MODIFIED' => 'Дата зміни',
  	'LBL_MODIFIED' => 'Змінено користувачем:',
  	'LBL_CREATED' => 'Творець: ',
  	'LBL_TEAM' => 'Група: ',
  	'LBL_ASSIGNED_TO' => 'Відповідальний (-а):',
  	'LBL_CAMPAIGN_START_DATE' => 'Дата початку: ',
  	'LBL_CAMPAIGN_END_DATE' => 'Дата закінчення: ',
  	'LBL_CAMPAIGN_STATUS' => 'Статус: ',
  	'LBL_CAMPAIGN_BUDGET' => 'Бюджет: ',
	'LBL_CAMPAIGN_EXPECTED_COST' => 'Очікувана вартість: ',
  	'LBL_CAMPAIGN_ACTUAL_COST' => 'Фактична вартість: ',
  	'LBL_CAMPAIGN_EXPECTED_REVENUE' => 'Розрахункові надходження: ',
  	'LBL_CAMPAIGN_TYPE' => 'Вид: ',
	'LBL_CAMPAIGN_OBJECTIVE' => 'Мета: ',
  	'LBL_CAMPAIGN_CONTENT' => 'Опис: ',
    'LBL_CREATED_LEAD' => 'Створений попередній контакт',
    'LBL_CREATED_CONTACT' => 'Створений контакт',
    'LBL_CREATED_OPPORTUNITY' => 'Створена угода',
    'LBL_TARGETED_USER' => 'Прізначений користувач',
    'LBL_SENT_EMAIL' => 'Надіслане повідомлення',
  	'LBL_LIST_FORM_TITLE'=> 'Цільові маркетингові кампанії',
  	'LBL_LIST_ACTIVITY_DATE'=>'Дата дії',
  	'LBL_RELATED'=>'Відноситься до',
  	'LBL_MORE_INFO'=>'Детальна інформація',

    'LBL_CAMPAIGNS' => 'Маркетингові кампанії',
	'LBL_LIST_MARKETING_NAME' => 'Кампанія',

	'LBL_MARKETING_ID' => 'Кампанія',
	'LBL_RELATED_NAME' => 'Related Name',
);
?>
