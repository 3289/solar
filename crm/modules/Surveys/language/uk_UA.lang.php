<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * SuiteCRM is an extension to SugarCRM Community Edition developed by Salesagility Ltd.
 * Copyright (C) 2011 - 2014 Salesagility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for  technical reasons, the Appropriate Legal Notices must
 * display the words  "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 ********************************************************************************/

$mod_strings = array(
    'LBL_ASSIGNED_TO_ID'                                     => 'Відповідальний (- а)',
    'LBL_ASSIGNED_TO_NAME'                                   => 'Відповідальний (- а)',
    'LBL_SECURITYGROUPS'                                     => 'Групи Користувачів',
    'LBL_SECURITYGROUPS_SUBPANEL_TITLE'                      => 'Групи Користувачів',
    'LBL_ID'                                                 => 'ID',
    'LBL_DATE_ENTERED'                                       => 'Дата створення',
    'LBL_DATE_MODIFIED'                                      => 'Дата зміни',
    'LBL_MODIFIED'                                           => 'Змінено',
    'LBL_MODIFIED_ID'                                        => 'Змінено користувачем',
    'LBL_MODIFIED_NAME'                                      => 'Змінено користувачем',
    'LBL_CREATED'                                            => 'Ким створено',
    'LBL_CREATED_ID'                                         => 'Створено користувачем',
    'LBL_DESCRIPTION'                                        => 'Опис',
    'LBL_DELETED'                                            => 'Видалено',
    'LBL_NAME'                                               => 'Назва',
    'LBL_CREATED_USER'                                       => 'Створено користувачем',
    'LBL_MODIFIED_USER'                                      => 'Змінено користувачем',
    'LBL_LIST_NAME'                                          => 'Назва',
    'LBL_EDIT_BUTTON'                                        => 'Змінити',
    'LBL_REMOVE'                                             => 'Видалити',
    'LBL_LIST_FORM_TITLE'                                    => 'Surveys List',
    'LBL_MODULE_NAME'                                        => 'Surveys',
    'LBL_MODULE_TITLE'                                       => 'Surveys',
    'LBL_HOMEPAGE_TITLE'                                     => 'My Surveys',
    'LNK_NEW_RECORD'                                         => 'Create Surveys',
    'LNK_LIST'                                               => 'View Surveys',
    'LNK_IMPORT_SURVEYS'                                     => 'Import Surveys',
    'LBL_SEARCH_FORM_TITLE'                                  => 'Search Surveys',
    'LBL_HISTORY_SUBPANEL_TITLE'                             => 'Історія',
    'LBL_ACTIVITIES_SUBPANEL_TITLE'                          => 'Заходи',
    'LBL_SURVEYS_SUBPANEL_TITLE'                             => 'Surveys',
    'LBL_NEW_FORM_TITLE'                                     => 'New Surveys',
    'LBL_STATUS'                                             => 'Статус',
    'LBL_SURVEY_QUESTIONS_DISPLAY'                           => 'Questions',
    'LBL_SURVEY_URL_DISPLAY'                                 => 'URL',
    'LBL_HAPPINESS_QUESTION'                                 => 'Happiness Question',
    'LBL_CANT_EDIT_RESPONDED'                                => 'Survey questions with responses cannot be edited',
    'LBL_VIEW_SURVEY_REPORTS'                                => 'View Survey Reports',
    'LBL_CHECKED'                                            => 'Checked',
    'LBL_UNCHECKED'                                          => 'Unchecked',
    'LBL_SHOW_ALL_RESPONSES'                                 => 'Show all responses',
    'LBL_RESPONSE_ANSWER'                                    => 'Answer',
    'LBL_RESPONSE_CONTACT'                                   => 'Контакт',
    'LBL_RESPONSE_TIME'                                      => 'Дата',
    'LBL_UNKNOWN_CONTACT'                                    => 'Невідомо',
    'LBL_RESPONSE_COUNT'                                     => 'Count',
    'LBL_SUBMIT_TEXT'                                        => 'Submit Text',
    'LBL_SATISFIED_TEXT'                                     => 'Satisfied Text',
    'LBL_NEITHER_TEXT'                                       => 'Neither Text',
    'LBL_DISSATISFIED_TEXT'                                  => 'Dissatisfied Text',
    'LBL_SURVEYS_SURVEYQUESTIONS_FROM_SURVEYQUESTIONS_TITLE' => 'Survey Questions',
    'LBL_SURVEYS_SURVEYRESPONSES_FROM_SURVEYRESPONSES_TITLE' => 'Survey Responses',
);