<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
	// DON'T CONVERT THESE THEY ARE MAPPINGS
	'db_name' => 'LBL_LIST_ACCOUNT_NAME',
	'db_website' => 'LBL_LIST_WEBSITE',
	'db_billing_address_city' => 'LBL_LIST_CITY',
	// END DON'T CONVERT
    'LBL_DOCUMENTS_SUBPANEL_TITLE' => 'Документи',
	// Dashlet Categories
	'LBL_CHARTS'    => 'Графіки',
	'LBL_DEFAULT' => 'Перегляди',
	// END Dashlet Categories

	'ERR_DELETE_RECORD' => 'Ви повинні вказати номер запису перед видаленням.',
	'LBL_ACCOUNT_INFORMATION' => 'Інформація по контрагенту', //Can be translated in all caps. This string will be used by SuiteP template menu actions
	'LBL_ACCOUNT_NAME' => 'Найменування контрагента:',
	'LBL_ACCOUNT' => 'Контрагент:',
	'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'Заходи',
	'LBL_ADDRESS_INFORMATION' => 'Контактна інформація',
	'LBL_ANNUAL_REVENUE' => 'Річний дохід:',
	'LBL_ANY_ADDRESS' => 'Адреса:',
	'LBL_ANY_EMAIL' => 'E-mail:',
	'LBL_ANY_PHONE' => 'Тел.:',
	'LBL_ASSIGNED_TO_NAME' => 'Призначити відповідального:',
	'LBL_ASSIGNED_TO_ID' => 'Відповідальний (а):',
	'LBL_BILLING_ADDRESS_CITY' => 'Юридична адреса - місто:',
	'LBL_BILLING_ADDRESS_COUNTRY' => 'Юридична адреса - країна:',
	'LBL_BILLING_ADDRESS_POSTALCODE' => 'Юридична адреса - індекс:',
	'LBL_BILLING_ADDRESS_STATE' => 'Юридична адреса - область:',
	'LBL_BILLING_ADDRESS_STREET_2' =>'Юридична адреса - вулиця 2',
	'LBL_BILLING_ADDRESS_STREET_3' =>'Юридична адреса - вулиця 3',
	'LBL_BILLING_ADDRESS_STREET_4' =>'Юридична адреса - вулиця 4',
	'LBL_BILLING_ADDRESS_STREET' => 'Юридична адреса - вулиця:',
	'LBL_BILLING_ADDRESS' => 'Юридична адреса',
	'LBL_BUGS_SUBPANEL_TITLE' => 'Помилки',
	'LBL_CAMPAIGN_ID' => 'Маркетингова кампанія',
	'LBL_CASES_SUBPANEL_TITLE' => 'Звернення',
	'LBL_CITY' => 'Місто:',
	'LBL_CONTACTS_SUBPANEL_TITLE' => 'Контакти',
	'LBL_COUNTRY' => 'Країна:',
	'LBL_DATE_ENTERED' => 'Дата створення:',
	'LBL_DATE_MODIFIED' => 'Дата зміни:',
	'LBL_DEFAULT_SUBPANEL_TITLE' => 'Контрагенти',
	'LBL_DESCRIPTION_INFORMATION' => 'Опис інформації',
	'LBL_DESCRIPTION' => 'Опис:',
	'LBL_DUPLICATE' => 'Можливо дублюючий контрагент',
	'LBL_EMAIL' => 'E-mail:',
	'LBL_EMAIL_OPT_OUT' => 'Не писати на E-mail',
	'LBL_EMAIL_ADDRESSES' => 'Адреса E-mail',
	'LBL_EMPLOYEES' => 'Кількість співробітників:',
	'LBL_FAX' => 'Факс:',
	'LBL_HISTORY_SUBPANEL_TITLE'=>'Історія',
	'LBL_HOMEPAGE_TITLE' => 'Мої контрагенти',
	'LBL_INDUSTRY' => 'Галузь:',
	'LBL_INVALID_EMAIL'=>'Невірний E-mail:',
	'LBL_INVITEE' => 'Контакти',
	'LBL_LEADS_SUBPANEL_TITLE' => 'Попередні контакти',
	'LBL_LIST_ACCOUNT_NAME' => 'Контрагент',
	'LBL_LIST_CITY' => 'Місто',
	'LBL_LIST_CONTACT_NAME' => 'Контактна особа',
	'LBL_LIST_EMAIL_ADDRESS' => 'Адреса E-mail',
	'LBL_LIST_FORM_TITLE' => 'Список контрагентів',
	'LBL_LIST_PHONE' => 'Тел.',
	'LBL_LIST_STATE' => 'Область',
	'LBL_MEMBER_OF' => 'Материнська компанія:',
	'LBL_MEMBER_ORG_SUBPANEL_TITLE'=>'Дочірні організації',
	'LBL_MODULE_NAME' => 'Контрагенти',
	'LBL_MODULE_TITLE' => 'Контрагенти - Головна',
	'LBL_MODULE_ID'=> 'Контрагенти',
	'LBL_NAME'=>'Назва:',
	'LBL_NEW_FORM_TITLE' => 'Новий контрагент',
	'LBL_OPPORTUNITIES_SUBPANEL_TITLE' => 'Угоди',
	'LBL_OTHER_EMAIL_ADDRESS' => 'Додаткова електронна адреса:',
	'LBL_OTHER_PHONE' => 'Додатковий тел.:',
	'LBL_OWNERSHIP' => 'Форма власності:',
	'LBL_PARENT_ACCOUNT_ID' => 'Батьківський контрагент',
	'LBL_PHONE_ALT' => 'Дод. тел.:',
	'LBL_PHONE_FAX' => 'Тел. (факс):',
	'LBL_PHONE_OFFICE' => 'Тел. (роб.):',
	'LBL_PHONE' => 'Тел.:',
	'LBL_POSTAL_CODE' => 'Індекс:',
	'LBL_PRODUCTS_TITLE'=>'Товари',
	'LBL_PROJECTS_SUBPANEL_TITLE' => 'Проекти',
	'LBL_PUSH_CONTACTS_BUTTON_LABEL' => 'Копія для контакту',
	'LBL_PUSH_CONTACTS_BUTTON_TITLE' => 'Копіювання...',
	'LBL_RATING' => 'Рейтинг:',
	'LBL_SAVE_ACCOUNT' => 'Збереження контрагента',
	'LBL_SEARCH_FORM_TITLE' => 'Пошук контрагентів',
	'LBL_SHIPPING_ADDRESS_CITY' => 'Фактична адреса - місто:',
	'LBL_SHIPPING_ADDRESS_COUNTRY' => 'Фактична адреса - країна:',
	'LBL_SHIPPING_ADDRESS_POSTALCODE' => 'Фактична адреса - індекс:',
	'LBL_SHIPPING_ADDRESS_STATE' => 'Фактична адреса - область:',
	'LBL_SHIPPING_ADDRESS_STREET_2' => 'Фактична адреса - вулиця 2',
	'LBL_SHIPPING_ADDRESS_STREET_3' => 'Фактична адреса - вулиця 3',
	'LBL_SHIPPING_ADDRESS_STREET_4' => 'Фактична адреса - вулиця 4',
	'LBL_SHIPPING_ADDRESS_STREET' => 'Фактична адреса - вулиця:',
	'LBL_SHIPPING_ADDRESS' => 'Фактична адреса:',
	'LBL_SIC_CODE' => 'Ідентифікац. код:',
	'LBL_STATE' => 'Штат чи регіон:', //For address fields
	'LBL_TICKER_SYMBOL' => 'Біржовий код:',
	'LBL_TYPE' => 'Тип:',
	'LBL_WEBSITE' => 'Сайт:',
	'LBL_CAMPAIGNS' =>'Маркет. кампанії',
	'LNK_ACCOUNT_LIST' => 'Контрагенти',
	'LNK_NEW_ACCOUNT' => 'Новий контрагент',
	'LNK_IMPORT_ACCOUNTS' => 'Імпорт контрагентів',
	'MSG_DUPLICATE' => 'Контрагент, якого Ви створюєте, можливо, дублює вже наявний запис. Схожі контрагенти показані нижче. Натисніть кнопку "Зберегти" для продовження створення нового контрагента або кнопку "Відмовитися" для повернення в модуль.',
	'MSG_SHOW_DUPLICATES' => 'Контрагент, якого Ви створюєте, можливо, дублює вже наявний запис. Схожі контрагенти показані нижче. Натисніть кнопку "Зберегти" для продовження створення нового контрагента або кнопку "Відмовитися" для повернення в модуль.',
	'LBL_ASSIGNED_USER_NAME' => 'Відповідальний (а):',
    'LBL_PROSPECT_LIST' => 'Адресати',
    'LBL_ACCOUNTS_SUBPANEL_TITLE'=>'Контрагенти',
    'LBL_PROJECT_SUBPANEL_TITLE' => 'Проекти',
    //For export labels
    'LBL_PARENT_ID' => 'ID початкового запису',
    // SNIP
	'LBL_PRODUCTS_SERVICES_PURCHASED_SUBPANEL_TITLE' => 'Придбані товари і послуги',

	'LBL_AOS_CONTRACTS' => 'Контракти',
	'LBL_AOS_INVOICES' => 'Рахунок-фактура',
	'LBL_AOS_QUOTES' => 'Комерційну пропозицію',
);
?>
