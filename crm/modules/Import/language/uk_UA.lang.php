<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
    'LBL_RECORDS_SKIPPED_DUE_TO_ERROR' => 'Записи пропущені через  виникнення помилки',
    'LBL_UPDATE_SUCCESSFULLY' => 'Записи успішно оновлені',
    'LBL_SUCCESSFULLY_IMPORTED' => 'Записи успішно створено',
    'LBL_STEP_4_TITLE' => 'Крок 4: Імпорт файлу',
    'LBL_STEP_5_TITLE' => 'Крок 5: Перегляд результатів',
    'LBL_CUSTOM_ENCLOSURE' => 'Роздільник:',
    'LBL_ERROR_UNABLE_TO_PUBLISH' => 'Не можна опублікувати схему. Вже існує інша схема імпорту з аналогічною назвою.',
    'LBL_ERROR_UNABLE_TO_UNPUBLISH' => 'Не можна скасувати публікацію схеми імпорту, що належить іншому користувачу. Необхідно щоб схема імпорту з аналогічною назвою належала Вам.',
    'LBL_ERROR_IMPORTS_NOT_SET_UP' => 'Імпорт не налаштований для даного типу модуля',
    'LBL_IMPORT_TYPE' => 'Параметри імпорту',
    'LBL_IMPORT_BUTTON' => 'Створення записів',
    'LBL_UPDATE_BUTTON' => 'Створення та оновлення записів',
    'LBL_CREATE_BUTTON_HELP' => 'Use this option to create new records. Note: Rows in the import file containing values that match the IDs of existing records will not be imported if the values are mapped to the ID field.',
    'LBL_UPDATE_BUTTON_HELP' => 'Use this option to update existing records. The data in the import file will be matched to existing records based on the record ID in the import file.',
    'LBL_ERROR_INVALID_BOOL'=>'Невірне логічне значення',
    'LBL_IMPORT_ERROR' => 'Помилка імпорту',
    'LBL_ERROR' => 'Помилка',
    'LBL_FIELD_NAME' => 'Поле',
    'LBL_VALUE' => 'Значення',
    'LBL_NONE' => 'Немає',
    'LBL_REQUIRED_VALUE' => 'Необхідне значення відсутнє',
    'LBL_ERROR_SYNC_USERS' => 'Invalid value to sync to Outlook: ',
    'LBL_ID_EXISTS_ALREADY' => 'Даний код вже присутній у цій таблиці',
    'LBL_ASSIGNED_USER' => 'Якщо користувач не існує - використовуйте поточного користувача',
    'LBL_ERROR_DELETING_RECORD' => 'Помилка видалення запису:',
    'LBL_ERROR_INVALID_ID' => 'Вказано надто довге значення коду. У полі може бути введено не більше 36 символів.',
    'LBL_ERROR_INVALID_PHONE' => 'Невірний телефонний номер',
    'LBL_ERROR_INVALID_NAME' => 'Надто довге значення',
    'LBL_ERROR_INVALID_VARCHAR' => 'Надто довге значення',
    'LBL_ERROR_INVALID_DATE' => 'Неправильне значення дати',
    'LBL_ERROR_INVALID_DATETIME' => 'Неправильне значення дати/часу',
    'LBL_ERROR_INVALID_DATETIMECOMBO' => 'Неправильне значення дати/часу',
    'LBL_ERROR_INVALID_TIME' => 'Неправильне значення часу',
    'LBL_ERROR_INVALID_INT' => 'Неправильне ціле значення',
    'LBL_ERROR_INVALID_NUM' => 'Невірний номер',
    'LBL_ERROR_INVALID_EMAIL'=>'Неправильний E-mail',
    'LBL_ERROR_INVALID_USER'=>'Невірне ім&#039;я або код користувача',
    'LBL_ERROR_INVALID_TEAM' => 'Неправильна назва або код команди',
    'LBL_ERROR_INVALID_ACCOUNT' => 'Неправильна назва або код контрагента',
    'LBL_ERROR_INVALID_RELATE' => 'Неправильні відносини',
    'LBL_ERROR_INVALID_CURRENCY' => 'Неправильне значення валюти',
    'LBL_ERROR_INVALID_FLOAT' => 'Неправильне число з плаваючою комою',
    'LBL_ERROR_NOT_IN_ENUM' => 'Значення відсутнє у списку. Допустимі значення:',
    'LBL_IMPORT_MODULE_ERROR_NO_UPLOAD' => 'Файл завантажено невдало. Можливо, значення \\&#039;upload_max_filesize\\&#039; у Вашому файлі php.ini занадто маленьке',
    'LBL_MODULE_NAME' => 'Імпорт',
    'LBL_TRY_AGAIN' => 'Спробувати ще раз',
    'LBL_IMPORT_ERROR_MAX_REC_LIMIT_REACHED' => 'The import file contains {0} rows. The optimal number of rows is {1}. More rows may slow the import process. Click OK to continue importing. Click Cancel to revise and re-upload the import file.',
    'ERR_IMPORT_SYSTEM_ADMININSTRATOR'  => 'Ви не можете імпортувати користувача, який має права системного адміністратора',
    'ERR_MULTIPLE' => 'Кілька колонок мають одні й ті ж назви.',
    'ERR_MISSING_REQUIRED_FIELDS' => 'Пропущені обов&#039;язкові поля:',
    'ERR_SELECT_FILE' => 'Вибір файлу для завантаження.',
    'LBL_SELECT_FILE' => 'Вибір файлу',
    'LBL_EXTERNAL_SOURCE' => 'an external application or service',
    'LBL_CUSTOM_DELIMITER' => 'Поля розділені за допомогою:',
    'LBL_DONT_MAP' => '-- Не імпортувати це поле --',
    'LBL_STEP_MODULE' => 'Which module do you want to import data into?',
    'LBL_STEP_1_TITLE' => 'Крок 1: Вибір джерела даних і параметрів імпорту',
    'LBL_CONFIRM_TITLE' => 'Step {0}: Confirm Import File Properties',
    'LBL_MICROSOFT_OUTLOOK' => 'Microsoft Outlook',
    'LBL_MICROSOFT_OUTLOOK_HELP' => 'The custom mappings for Microsoft Outlook rely on the import file being comma-delimited (.csv). If your import file is tab-delimited, the mappings will not be applied as expected.',
    'LBL_SALESFORCE' => 'Salesforce.com',
    'LBL_PUBLISH' => 'Опублікувати',
    'LBL_DELETE' => 'Видалити',
    'LBL_PUBLISHED_SOURCES' => 'Опубліковані схеми імпорту:',
    'LBL_UNPUBLISH' => 'Скасувати публікацію',
    'LBL_NEXT' => 'Далі >',
    'LBL_BACK' => '< Назад',
    'LBL_STEP_2_TITLE' => 'Крок 2: Завантаження файлу імпорту',
    'LBL_HAS_HEADER' => 'Має заголовок:',
    'LBL_NUM_1' => '1.',
    'LBL_NUM_2' => '2.',
    'LBL_NUM_3' => '2.',
    'LBL_NUM_4' => '4.',
    'LBL_NUM_5' => '5.',
    'LBL_NUM_6' => '6.',
    'LBL_NUM_7' => '7.',
    'LBL_NUM_8' => '8.',
    'LBL_NUM_9' => '9.',
    'LBL_NUM_10' => '10.',
    'LBL_NUM_11' => '11.',
    'LBL_NUM_12' => '12.',
    'LBL_NOTES' => 'Нотатки:',
    'LBL_STEP_3_TITLE' => 'Крок 3: Підтвердження полів та імпорт',
    'LBL_STEP_DUP_TITLE' => 'Step {0}: Check for Possible Duplicates',
    'LBL_DATABASE_FIELD' => 'Поля бази даних',
    'LBL_HEADER_ROW' => 'Рядок заголовків',
    'LBL_HEADER_ROW_OPTION_HELP' => 'Select if the top row of the import file is a Header Row containing field labels.',
    'LBL_ROW' => 'Рядок',
    'LBL_CONTACTS_NOTE_1' => 'Повинне бути вказане або <b>Прізвище</b>, або <b>Повне ім&#039;я</b>.',
    'LBL_CONTACTS_NOTE_2' => 'Якщо буде вказано <b>Повне ім&#039;я</b>, то поля <b>Ім&#039;я</b> і <b>Прізвище</b> ігноруватимуться.',
    'LBL_CONTACTS_NOTE_3' => 'Якщо буде вказано <b>Повне ім&#039;я</b>, то дані з цього поля будуть розділені між полями <b>Ім&#039;я</b> і <b>Прізвище</b> при вставці в базу даних.',
    'LBL_CONTACTS_NOTE_4' => 'Поля <b>Вулиця 2</b> і <b>Вулиця 3</b> будуть об&#039;єднані разом з основним адресним полем при вставці в базу даних.',
    'LBL_ACCOUNTS_NOTE_1' => 'Поля <b>Вулиця 2</b> і <b>Вулиця 3</b> будуть об&#039;єднані разом з основним адресним полем при вставці в базу даних.',
    'LBL_IMPORT_NOW' => 'Почати імпорт',
    'LBL_' => '',
    'LBL_CANNOT_OPEN' => 'Не вдається відкрити файл для читання',
    'LBL_NO_LINES' => 'В файлі, що імпортується, немає рядків',
    'LBL_SUCCESS' => 'Готово:',
    'LBL_LAST_IMPORT_UNDONE' => 'Ваш останній імпорт скасовано',
    'LBL_NO_IMPORT_TO_UNDO' => 'Нічого скасовувати, бо нічого не імпортовано.',
    'LBL_CREATED_TAB' => 'Created Records',
    'LBL_DUPLICATE_TAB' => 'Duplicates',
    'LBL_ERROR_TAB' => 'Errors',
    'LBL_IMPORT_MORE' => 'Імпортувати ще',
    'LBL_FINISHED' => 'Повернення до',
    'LBL_UNDO_LAST_IMPORT' => 'Скасувати результати імпорту',
    'LBL_DUPLICATES' => 'Знайдено дублікати',
    'LNK_DUPLICATE_LIST' => 'Завантажити список дублікатів',
    'LNK_ERROR_LIST' => 'Завантажити список помилок',
    'LNK_RECORDS_SKIPPED_DUE_TO_ERROR' => 'Завантажити записи, які не можуть бути імпортовані.',
    'LBL_INDEX_USED' => 'Використовувані індекси:',
    'LBL_INDEX_NOT_USED' => 'Невикористовувані індекси:',
    'LBL_IMPORT_FIELDDEF_ID' => 'Унікальний код',
    'LBL_IMPORT_FIELDDEF_RELATE' => 'Назва або код',
    'LBL_IMPORT_FIELDDEF_PHONE' => 'Номер телефону',
    'LBL_IMPORT_FIELDDEF_TEAM_LIST' => 'Назва або код команди',
    'LBL_IMPORT_FIELDDEF_NAME' => 'Текст',
    'LBL_IMPORT_FIELDDEF_VARCHAR' => 'Текст',
    'LBL_IMPORT_FIELDDEF_TEXT' => 'Текст',
    'LBL_IMPORT_FIELDDEF_TIME' => 'Час',
    'LBL_IMPORT_FIELDDEF_DATE' => 'Дата',
    'LBL_IMPORT_FIELDDEF_ASSIGNED_USER_NAME' => 'Ім&#039;я користувача або код',
    'LBL_IMPORT_FIELDDEF_BOOL' => '\'0\' або \'1\'',
    'LBL_IMPORT_FIELDDEF_ENUM' => 'Список',
    'LBL_IMPORT_FIELDDEF_EMAIL' => 'Адреса E-Mail',
    'LBL_IMPORT_FIELDDEF_INT' => 'Цифра (тільки ціле значення)',
    'LBL_IMPORT_FIELDDEF_DOUBLE' => 'Цифра (тільки ціле значення)',
    'LBL_IMPORT_FIELDDEF_NUM' => 'Цифра (тільки ціле значення)',
    'LBL_IMPORT_FIELDDEF_CURRENCY' => 'Цифра (припустиме дробове значення)',
    'LBL_IMPORT_FIELDDEF_FLOAT' => 'Цифра (припустиме дробове значення)',
    'LBL_DATE_FORMAT' => 'Формат дати',
    'LBL_TIME_FORMAT' => 'Формат часу:',
    'LBL_TIMEZONE' => 'Часовий пояс:',
    'LBL_ADD_ROW' => 'Додати поле',
    'LBL_REMOVE_ROW' => 'Видалити поле',
    'LBL_DEFAULT_VALUE' => 'Значення за замовчуванням',
    'LBL_SHOW_ADVANCED_OPTIONS' => 'Показати додаткові налаштування',
    'LBL_HIDE_ADVANCED_OPTIONS' => 'Приховати додаткові налаштування',
    'LBL_SHOW_NOTES' => 'Нотатки',
    'LBL_HIDE_NOTES' => 'Hide Notes',
    'LBL_SAVE_MAPPING_AS' => 'Зберегти схему імпорту як',
    'LBL_IMPORT_COMPLETE' => 'Завершення імпорту',
    'LBL_IMPORT_COMPLETED' => 'Import Completed',
    'LBL_IMPORT_RECORDS' => 'Імпорт записів',
    'LBL_IMPORT_RECORDS_OF' => 'з',
    'LBL_IMPORT_RECORDS_TO' => 'у',
    'LBL_CURRENCY' => 'Валюта',
	'LBL_CURRENCY_SIG_DIGITS' => 'Значущі цифри валюти',
    'LBL_NUMBER_GROUPING_SEP' => 'Символ роздільника розрядів',
    'LBL_DECIMAL_SEP' => 'Символ десяткового роздільника',
    'LBL_LOCALE_DEFAULT_NAME_FORMAT' => 'Формат відображення імені',
    'LBL_LOCALE_EXAMPLE_NAME_FORMAT' => 'Приклад',
    'LBL_LOCALE_NAME_FORMAT_DESC' => '"s" Звернення, "f" Ім&#039;я, "l" Прізвище',
    'LBL_CHARSET' => 'Кодування',
    'LBL_MY_SAVED_HELP' => 'Збережені схеми імпорту містять раніше використовувані правила імпорту даних, а також набір полів бази даних, в які потрапляють дані з файлу, що імпортується.<br>Натисніть кнопку <b>Опублікувати</b>, щоб зробити схему імпорту доступною для інших користувачів.<br>Натисніть кнопку <b>Скасувати публікацію</b>, щоб зробити схему імпорту недоступною для інших користувачів.',
    'LBL_MY_SAVED_ADMIN_HELP' => 'Use this option to apply your pre-set import settings, including import properties, mappings, and any duplicate check settings, to this import.<br><br>Click <b>Publish</b> to make the mapping available to other users.<br>Click <b>Un-Publish</b> to make the mapping unavailable to other users.<br>Click <b>Delete</b> to delete a mapping for all users.',
    'LBL_ENCLOSURE_HELP' => '<p><b>Обмежуючий символ</b> використовується для розмежування вмісту полів, включаючи будь-які символи, які використовуються в якості розділювачів.<br><br>Приклад: Якщо як роздільник використовується кома <b>(,)</b>, а як обмежуючий символ використовуються подвійні лапки <b>(")</b>, то рядок <br><b>"Монреаль, Квебек"</b> імпортується в одне поле у вигляді <b>Монреаль, Квебек</b>.<br>Якщо обмежуючі символи відсутні, або в якості таких використовуються інші символи, то рядок <br><b>&#039;Монреаль, Квебек&#039;</b> імпортується в два сусідні поля у вигляді <b>Монреаль</b> і <b>Квебек</b>.<br><br>Примітка: Файл, що імпортується, може не містити обмежуючих символів.<br> В Excel для файлів, значення яких розділені крапкою з комою або символом табуляції, обмежуючим символом за замовчуванням є подвійні лапки.</p>',
    'LBL_DATABASE_FIELD_HELP' => 'Виберіть необхідні поля з переліку всіх існуючих полів модуля.',
    'LBL_HEADER_ROW_HELP' => 'У першому рядку файлу, що імпортується, містяться наступні імена полів.',
    'LBL_DEFAULT_VALUE_HELP' => 'Значення за замовчуванням для поля у створюваного або оновлюваного запису, якщо поле у файлі імпорту не містить даних.',
    'LBL_ROW_HELP' => 'Це перший рядок після заголовка з даними файлу, що імпортується.',
    'LBL_SAVE_MAPPING_HELP' => 'Введіть назву набору полів бази даних, в які потрапляють дані з імпортованого файлу.<br>Набір полів бази даних, дані про сортування полів, а також правила імпорту вихідних даних, вибрані на першому кроці імпорту будуть збережені при спробі імпорту даних у систему.<br> Збережені схеми імпорту можуть бути обрані надалі на першому кроці процесу імпорту при необхідності імпорту аналогічних даних.',
    'LBL_IMPORT_FILE_SETTINGS_HELP' => 'Вкажіть параметри файлу, що імпортується, для того, щоб дані були імпортовані коректно. Ці параметри не перезапишуть Ваші стандартні налаштування. Тобто при створенні нових або оновленні існуючих записів знову будуть враховуватися налаштування, вказані Вами на сторінці <b>Мої налаштування</b>.',
    'LBL_IMPORT_STARTED' => 'Імпорт розпочато',
    'LBL_RECORD_CANNOT_BE_UPDATED' => 'Недостатньо прав для оновлення запису',
    'LBL_DELETE_MAP_CONFIRMATION' => 'Ви впевнені, що хочете видалити цю схему імпорту?',
    'LBL_THIRD_PARTY_CSV_SOURCES' => 'If the import file data was exported from any of the following sources, select which one.',
    'LBL_THIRD_PARTY_CSV_SOURCES_HELP' => 'Select the source to automatically apply custom mappings in order to simplify the mapping process (next step).',
    'LBL_EXAMPLE_FILE' => 'Download Import File Template',
    'LBL_CONFIRM_IMPORT' => 'You have selected to update records during the import process. Updates made to existing records cannot be undone. However, records created during the import process can be undone (deleted), if desired. Click Cancel to select to create new records only, or click OK to continue.',
    'LBL_CONFIRM_MAP_OVERRIDE' => 'Warning: You have already selected a custom mapping for this import, do you want to continue?',
    'LBL_SAMPLE_URL_HELP' => 'Download a sample import file containing a header row of the module fields. The file can be used as a template to create an import file containing the data that you would like to import.',
    'LBL_AUTO_DETECT_ERROR' => 'The field delimiter and qualifier in the import file could not be detected. Please verify the settings in the Import File Properties.',
    'LBL_MIME_TYPE_ERROR_1' => 'The selected file does not appear to contain a delimited list. Please check the file type. We recommend comma-delimited files (.csv).',
    'LBL_MIME_TYPE_ERROR_2' => 'To proceed with importing the selected file, click OK. To upload a new file, click Try Again.',
    'LBL_FIELD_DELIMETED_HELP' => 'The field delimiter specifies the character used to separate the field columns.',
    'LBL_FILE_UPLOAD_WIDGET_HELP' => 'Select a file containing data that is separated by a delimiter, such as a comma- or tab- delimited file. Files of the type .csv are recommended.',
    'LBL_ERROR_IMPORT_CACHE_NOT_WRITABLE' => 'Import cache directory is not writable.',
    'LBL_ADD_FIELD_HELP' => 'Use this option to add a value to a field in all records created and/or updated. Select the field and then enter or select a value for that field in the Default Value column.',
    'LBL_MISSING_HEADER_ROW' => 'No Header Row Found',
    'LBL_CANCEL' => 'Скасування',
    'LBL_SELECT_DS_INSTRUCTION' => 'Ready to start importing? Select the source of the data that you would like to import.',
    'LBL_SELECT_UPLOAD_INSTRUCTION' => 'Select a file on your computer that contains the data that you would like to import, or download the template to get a head start on creating the import file.',
    'LBL_SELECT_PROPERTY_INSTRUCTION' => 'Here is how the first several rows of the import file appear with the detected file properties. If a header row was detected, it is displayed in the top row of the table. View the import file properties to make changes to the detected properties and to set additional properties. Updating the settings will update the data appearing in the table.',
    'LBL_SELECT_MAPPING_INSTRUCTION' => 'The table below contains all of the fields in the module that can be mapped to the data in the import file. If the file contains a header row, the columns in the file have been mapped to matching fields. Check the mappings to make sure that they are what you expect, and make changes, as necessary. To help you check the mappings, Row 1 displays the data in the file. Be sure to map to all of the required fields (noted by an asterisk).',
    'LBL_SELECT_DUPLICATE_INSTRUCTION' => 'To avoid creating duplicate records, select which of the mapped fields you would like to use to perform a duplicate check while data is being imported. Values within existing records in the selected fields will be checked against the data in the import file. If matching data is found, the rows in the import file containing the data will be displayed along with the import results (next page). You will then be able to select which of these rows to continue importing.',
    'LBL_DUP_HELP' => 'Here are the rows in the import file that were not imported because they contain data that matches values in existing records based on the duplicate check. The data that matches is highlighted. To re-import these rows, download the list, make changes and click <b>Import Again</b>.',
    'LBL_SUMMARY' => 'Резюме',
    'LBL_OK' => 'Гаразд',
    'LBL_ERROR_HELP' => 'Here are the rows in the import file that were not imported due to errors. To re-import these rows, download the list, make changes and click <b>Import Again</b>',
    'LBL_EXTERNAL_ASSIGNED_TOOLTIP' => 'To assign the new records to a user other than yourself, use the Default Value column to select a different user.',
    'LBL_EXTERNAL_TEAM_TOOLTIP' => 'To assign the new records to teams other than your default team(s), use the Default Value column to select different teams.',
);

global $timedate;
?>
