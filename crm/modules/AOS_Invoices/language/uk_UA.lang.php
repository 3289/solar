<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
    'LBL_ASSIGNED_TO_ID' => 'Відповідальний (- а)',
    'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (- а)',
    'LBL_ID' => 'ID',
    'LBL_DATE_ENTERED' => 'Дата створення',
    'LBL_DATE_MODIFIED' => 'Дата зміни',
    'LBL_MODIFIED' => 'Змінено',
    'LBL_MODIFIED_NAME' => 'Змінено користувачем',
    'LBL_CREATED' => 'Ким створено',
    'LBL_DESCRIPTION' => 'Опис',
    'LBL_DELETED' => 'Видалено',
    'LBL_NAME' => 'Посада',
    'LBL_CREATED_USER' => 'Створено користувачем',
    'LBL_MODIFIED_USER' => 'Змінено користувачем',
    'ERR_DELETE_RECORD' => 'Перед видаленням повинен бути вказаний номер запису.',
    'LBL_ACCOUNT_NAME' => 'Посада',
    'LBL_ACCOUNT' => 'Назва компанії:',
    'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Заходи',
    'LBL_ADDRESS_INFORMATION' => 'Адресна інформація',
    'LBL_ANNUAL_REVENUE' => 'Річний дохід:',
    'LBL_ANY_ADDRESS' => 'Адреса:',
    'LBL_ANY_EMAIL' => 'E-mail:',
    'LBL_ANY_PHONE' => 'Тел.:',
    'LBL_RATING' => 'Рейтинг',
    'LBL_ASSIGNED_USER' => 'Користувач',
    'LBL_BILLING_ADDRESS_CITY' => 'Юридична адреса - місто:',
    'LBL_BILLING_ADDRESS_COUNTRY' => 'Юридична адреса - країна:',
    'LBL_BILLING_ADDRESS_POSTALCODE' => 'Юридична адреса - індекс:',
    'LBL_BILLING_ADDRESS_STATE' => 'Юридична адреса - область:',
    'LBL_BILLING_ADDRESS_STREET_2' => 'Юридична адреса - вулиця 2',
    'LBL_BILLING_ADDRESS_STREET_3' => 'Юридична адреса - вулиця 3',
    'LBL_BILLING_ADDRESS_STREET_4' => 'Юридична адреса - вулиця 4',
    'LBL_BILLING_ADDRESS_STREET' => 'Юридична адреса - вулиця:',
    'LBL_BILLING_ADDRESS' => 'Юридична адреса',
    'LBL_ACCOUNT_INFORMATION' => 'Перегляд',
    'LBL_CITY' => 'Місто',
    'LBL_CONTACTS_SUBPANEL_TITLE' => 'Контакти',
    'LBL_COUNTRY' => 'Країна:',
    'LBL_DEFAULT_SUBPANEL_TITLE' => 'Контрагенти',
    'LBL_DUPLICATE' => 'Можливо дублікат контрагента',
    'LBL_EMAIL' => 'Email:',
    'LBL_EMPLOYEES' => 'Кількість співробітників:',
    'LBL_FAX' => 'Факс:',
    'LBL_INDUSTRY' => 'Галузь:',
    'LBL_LIST_ACCOUNT_NAME' => 'Контрагент',
    'LBL_LIST_CITY' => 'Місто',
    'LBL_LIST_EMAIL_ADDRESS' => 'Адреса E-mail',
    'LBL_LIST_PHONE' => 'Тел.',
    'LBL_LIST_STATE' => 'Область',
    'LBL_MEMBER_OF' => 'Материнська компанія:',
    'LBL_MEMBER_ORG_SUBPANEL_TITLE' => 'Дочірні організації',
    'LBL_OTHER_EMAIL_ADDRESS' => 'Додаткова електронна адреса:',
    'LBL_OTHER_PHONE' => 'Додатковий тел.:',
    'LBL_OWNERSHIP' => 'Форма власності:',
    'LBL_PARENT_ACCOUNT_ID' => 'Батьківський контрагент',
    'LBL_PHONE_ALT' => 'Дод. тел.:',
    'LBL_PHONE_FAX' => 'Тел. (факс):',
    'LBL_PHONE_OFFICE' => 'Тел. робочий:',
    'LBL_PHONE' => 'Телефон:',
    'LBL_POSTAL_CODE' => 'Індекс:',
    'LBL_SAVE_ACCOUNT' => 'Збереження контрагента',
    'LBL_SHIPPING_ADDRESS_CITY' => 'Фактична адреса - місто:',
    'LBL_SHIPPING_ADDRESS_COUNTRY' => 'Фактична адреса - країна:',
    'LBL_SHIPPING_ADDRESS_POSTALCODE' => 'Фактична адреса - індекс:',
    'LBL_SHIPPING_ADDRESS_STATE' => 'Фактична адреса - область:',
    'LBL_SHIPPING_ADDRESS_STREET_2' => 'Фактична адреса - вулиця 2',
    'LBL_SHIPPING_ADDRESS_STREET_3' => 'Фактична адреса - вулиця 3',
    'LBL_SHIPPING_ADDRESS_STREET_4' => 'Фактична адреса - вулиця 4',
    'LBL_SHIPPING_ADDRESS_STREET' => 'Фактична адреса - вулиця:',
    'LBL_SHIPPING_ADDRESS' => 'Фактична адреса:',
    'LBL_STATE' => 'Штат чи регіон:', //For address fields
    'LBL_TICKER_SYMBOL' => 'Біржовий код:',
    'LBL_TYPE' => 'Тип:',
    'LBL_WEBSITE' => 'Сайт:',
    'LNK_ACCOUNT_LIST' => 'Контрагенти',
    'LNK_NEW_ACCOUNT' => 'Створити контрагента',
    'MSG_DUPLICATE' => 'Creating this account may potentially create a duplicate account. You may either select an account from the list below or you may click on Save to continue creating a new account with the previously entered data.',
    'MSG_SHOW_DUPLICATES' => 'Creating this account may potentially create a duplicate account. You may either click on Save to continue creating this new account with the previously entered data or you may click Cancel.',
    'NTC_DELETE_CONFIRMATION' => 'Ви дійсно хочете видалити цей запис?',
    'LBL_LIST_FORM_TITLE' => 'Список рахунків-фактур',
    'LBL_MODULE_NAME' => 'Рахунок-фактура',
    'LBL_MODULE_TITLE' => 'Рахунки: Головна',
    'LBL_HOMEPAGE_TITLE' => 'Мої рахунки',
    'LNK_NEW_RECORD' => 'Створити рахунок',
    'LNK_LIST' => 'Перегляд рахунків',
    'LBL_SEARCH_FORM_TITLE' => 'Пошук рахунків',
    'LBL_HISTORY_SUBPANEL_TITLE' => 'Історія',
    'LBL_NEW_FORM_TITLE' => 'Новий рахунок',
    'LBL_TERMS_C' => 'Умови',
    'LBL_APPROVAL_ISSUE' => 'Approval Issues',
    'LBL_APPROVAL_STATUS' => 'Approval Status',
    'LBL_BILLING_ACCOUNT' => 'Контрагент',
    'LBL_BILLING_CONTACT' => 'Контакт',
    'LBL_EXPIRATION' => 'Дійсний до',
    'LBL_INVOICE_NUMBER' => 'Номер рахунку',
    'LBL_OPPORTUNITY' => 'Угода:',
    'LBL_TEMPLATE_DDOWN_C' => 'Invoice Templates',
    'LBL_STAGE' => 'Статус пропозиції',
    'LBL_TERM' => 'Умови оплати',
    'LBL_SUBTOTAL_AMOUNT' => 'Subtotal',
    'LBL_DISCOUNT_AMOUNT' => 'Знижка',
    'LBL_TAX_AMOUNT' => 'Tax',
    'LBL_SHIPPING_AMOUNT' => 'Вантажні перевезення',
    'LBL_TOTAL_AMT' => 'Total',
    'VALUE' => 'Посада',
    'LBL_EMAIL_ADDRESSES' => 'Адреса E-mail',
    'LBL_LINE_ITEMS' => 'Line Items',
    'LBL_GRAND_TOTAL' => 'Grand Total',
    'LBL_QUOTE_NUMBER' => 'Quote Number',
    'LBL_QUOTE_DATE' => 'Quote Date',
    'LBL_INVOICE_DATE' => 'Invoice Date',
    'LBL_DUE_DATE' => 'Дата завершення',
    'LBL_STATUS' => 'Статус',
    'LBL_INVOICE_STATUS' => 'Invoice Status',
    'LBL_PRODUCT_QUANITY' => 'Кількість',
    'LBL_PRODUCT_NAME' => 'Продукт',
    'LBL_PART_NUMBER' => 'Артикул',
    'LBL_PRODUCT_NOTE' => 'Замітка',
    'LBL_PRODUCT_DESCRIPTION' => 'Опис',
    'LBL_LIST_PRICE' => 'Список',
    'LBL_DISCOUNT_AMT' => 'Знижка',
    'LBL_UNIT_PRICE' => 'Ціна продажу',
    'LBL_TOTAL_PRICE' => 'Всього',
    'LBL_VAT' => 'ПДВ', //VAT
    'LBL_VAT_AMT' => 'Сума ПДВ', //VAT
    'LBL_ADD_PRODUCT_LINE' => 'Додати рядок товарів',
    'LBL_SERVICE_NAME' => 'Послуги',
    'LBL_SERVICE_LIST_PRICE' => 'Список',
    'LBL_SERVICE_PRICE' => 'Ціна продажу',
    'LBL_SERVICE_DISCOUNT' => 'Знижка',
    'LBL_ADD_SERVICE_LINE' => 'Додати рядок послуг ',
    'LBL_REMOVE_PRODUCT_LINE' => 'Видалити',
    'LBL_PRINT_AS_PDF' => 'Print as PDF',
    'LBL_EMAIL_INVOICE' => 'Email Invoice',
    'LBL_LIST_NUM' => 'Num',
    'LBL_PDF_NAME' => 'Рахунок',
    'LBL_EMAIL_NAME' => 'Invoice for',
    'LBL_NO_TEMPLATE' => 'ERROR\nNo templates found. If you have not created an Invoice template, go to the PDF templates module and create one',
    'LBL_SUBTOTAL_TAX_AMOUNT' => 'Subtotal + Tax',//pre shipping
    'LBL_EMAIL_PDF' => 'Email PDF',
    'LBL_ADD_GROUP' => 'Додати групу',
    'LBL_DELETE_GROUP' => 'Видалити групу',
    'LBL_GROUP_NAME' => 'Назва групи',
    'LBL_GROUP_TOTAL' => 'Group Total',
    'LBL_SHIPPING_TAX' => 'Shipping Tax',
    'LBL_SHIPPING_TAX_AMT' => 'Shipping Tax',
    'LBL_IMPORT_LINE_ITEMS' => 'Import Line Items',
    'LBL_SUBTOTAL_AMOUNT_USDOLLAR' => 'Subtotal (Default Currency)',
    'LBL_DISCOUNT_AMOUNT_USDOLLAR' => 'Discount (Default Currency)',
    'LBL_TAX_AMOUNT_USDOLLAR' => 'Tax (Default Currency)',
    'LBL_SHIPPING_AMOUNT_USDOLLAR' => 'Shipping (Default Currency)',
    'LBL_TOTAL_AMT_USDOLLAR' => 'Total (Default Currency)',
    'LBL_SHIPPING_TAX_AMT_USDOLLAR' => 'Shipping Tax (Default Currency)',
    'LBL_GRAND_TOTAL_USDOLLAR' => 'Grand Total (Default Currency)',
    'LBL_INVOICE_TO' => 'Invoice To',
    'LBL_AOS_LINE_ITEM_GROUPS' => 'Line Item Groups',
    'LBL_AOS_PRODUCT_QUOTES' => 'Product Quotes',
    'LBL_AOS_QUOTES_AOS_INVOICES' => 'Quotes: Invoices',
);
?>
