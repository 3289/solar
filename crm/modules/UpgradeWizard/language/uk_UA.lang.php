<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
	die('Not A Valid Entry Point');
}

$mod_strings = array(
    'ERR_UW_CANNOT_DETERMINE_GROUP' => 'Неможливо визначити групу',
    'ERR_UW_CANNOT_DETERMINE_USER' => 'Неможливо визначити власника',
    'ERR_UW_CONFIG_WRITE' => 'Помилка при додаванні інформації про нову версію в файл config.php.',
    'ERR_UW_CONFIG' => 'Зробіть файл config.php доступним для запису і перезавантажте цю сторінку.',
    'ERR_UW_DIR_NOT_WRITABLE' => 'Директорія не доступна для запису',
    'ERR_UW_FILE_NOT_COPIED' => 'Файл не скопійовано',
    'ERR_UW_FILE_NOT_DELETED' => 'Не вдалося видалити пакет',
    'ERR_UW_FILE_NOT_READABLE' => 'Файл не може бути прочитаний',
    'ERR_UW_FILE_NOT_WRITABLE' => 'Файл не може бути переміщений або записаний у',
    'ERR_UW_FLAVOR_2' => 'Версія, що встановлюється:',
    'ERR_UW_FLAVOR' => 'Поточна версія SuiteCRM:',
    'ERR_UW_LOG_FILE_UNWRITABLE' => './upgradeWizard.log не може бути створений/записаний. Будь-ласка, встановіть відповідні права доступу на директорію SuiteCRM.',
    'ERR_UW_MBSTRING_FUNC_OVERLOAD' => 'Значення параметра mbstring.func_overload більше 1. Будь ласка, змініть це значення в файлі php.ini та перезавантажте веб-сервер.',
    'ERR_UW_NO_FILE_UPLOADED' => 'Будь ласка, вкажіть файл та спробуйте ще раз!',
    'ERR_UW_NO_FILES' => 'Помилка: не знайдено файлів для перевірки.',
    'ERR_UW_NO_MANIFEST' => 'В архіві відсутній файл manifest.php. Операція не може бути продовжена.',
    'ERR_UW_NO_VIEW' => 'Вказана невірна сторінка.',
    'ERR_UW_NOT_VALID_UPLOAD' => 'Невірне завантаження.',
    'ERR_UW_NO_CREATE_TMP_DIR' => 'Неможливо створити тимчасову директорію. Переконайтеся, що для цього достатньо прав.',
    'ERR_UW_ONLY_PATCHES' => ' На цій сторінці ви можете завантажувати тільки оновлення.',
    'ERR_UW_PREFLIGHT_ERRORS' => 'Виявлені помилки при попередній перевірці',
    'ERR_UW_UPLOAD_ERR' => 'Помилка завантаження файлу, Будь ласка, спробуйте ще раз!<br />\\<br />',
    'ERR_UW_VERSION' => 'Версія SuiteCRM:',
    'ERR_UW_PHP_VERSION' => 'PHP Version: ',
    'ERR_UW_SUITECRM_VERSION' => 'Версія SuiteCRM:',
    'ERR_UW_WRONG_TYPE' => 'Неправильна сторінка',
    'LBL_BUTTON_BACK' => '< Назад',
    'LBL_BUTTON_CANCEL' => 'Скасування',
    'LBL_BUTTON_DELETE' => 'Видалити пакет',
    'LBL_BUTTON_DONE' => 'Готово',
    'LBL_BUTTON_EXIT' => 'Вихід',
    'LBL_BUTTON_NEXT' => 'Далі >',
    'LBL_BUTTON_RECHECK' => 'Відмітити повторно',
    'LBL_BUTTON_RESTART' => 'Почати заново',

    'LBL_UPLOAD_UPGRADE' => 'Завантаження пакету оновлення',
    'LBL_UW_BACKUP_FILES_EXIST_TITLE' => 'Створення резервної копії',
    'LBL_UW_BACKUP_FILES_EXIST' => 'Резервні файли цього оновлення можуть бути знайдені в',
    'LBL_UW_BACKUP' => 'Створення резервної копії',
    'LBL_UW_CANCEL_DESC' => 'Майстер оновлення був зупинений. Всі тимчасові файли та архіви були видалені. Натисніть кнопку "Готово" для повторного запуску майстра.',
    'LBL_UW_CHECK_ALL' => 'Відмітити всі',
    'LBL_UW_CHECKLIST' => 'Стадії оновлення',
    'LBL_UW_COMMIT_ADD_TASK_DESC_1' => "Резервні копії перезаписуваних файлів знаходяться у директорії:",
    'LBL_UW_COMMIT_ADD_TASK_DESC_2' => "Необхідно вручну перезаписати наступні файли:",
    'LBL_UW_COMMIT_ADD_TASK_NAME' => 'Оновлення: перезаписати файли вручну',
    'LBL_UW_COMMIT_ADD_TASK_OVERVIEW' => 'Будь ласка, для перезапису файлів використовуйте найбільш зручний для вас спосіб. Поки Ви цього не зробите, система буде перебувати у нестабільному стані і оновлення не буде завершено.',
    'LBL_UW_COMPLETE' => 'Виконано',
    'LBL_UW_COMPLIANCE_ALL_OK' => 'Всі системні вимоги виконані',
    'LBL_UW_COMPLIANCE_CALLTIME' => 'Налаштування PHP: Call Time Pass By Reference',
    'LBL_UW_COMPLIANCE_CURL' => 'Модуль cURL',
    'LBL_UW_COMPLIANCE_IMAP' => 'Модуль IMAP',
    'LBL_UW_COMPLIANCE_MBSTRING' => 'Модуль MBStrings',
    'LBL_UW_COMPLIANCE_MBSTRING_FUNC_OVERLOAD' => 'Параметр MBStrings mbstring.func_overload',
    'LBL_UW_COMPLIANCE_MEMORY' => 'Налаштування PHP: Максимальний розмір використовуваної пам&#039;яті',
    'LBL_UW_COMPLIANCE_STREAM' => 'PHP Setting: Stream',
    'LBL_UW_COMPLIANCE_DB' => 'Minimum Database Version',
    'LBL_UW_COMPLIANCE_PHP_INI' => 'Розташування php.ini',
    'LBL_UW_COMPLIANCE_PHP_VERSION' => 'Мінімальна версія PHP',
    'LBL_UW_COMPLIANCE_SAFEMODE' => 'Налаштування PHP: безпечний режим',
    'LBL_UW_COMPLIANCE_TITLE2' => 'Виявлені налаштування',
    'LBL_UW_COMPLIANCE_XML' => 'XML Parsingм',
    'LBL_UW_COMPLIANCE_ZIPARCHIVE' => 'Zip Support',
    'LBL_UW_COMPLIANCE_PCRE_VERSION' => 'PCRE Version',

    'LBL_UW_COPIED_FILES_TITLE' => 'Успішно скопійовані файли',

    'LBL_UW_DB_CHOICE1' => 'Виконання SQL-запиту майстром оновлення',
    'LBL_UW_DB_CHOICE2' => 'Виконання SQL-запиту вручну',
    'LBL_UW_DB_ISSUES_PERMS' => 'Привілеї бази даних',
    'LBL_UW_DB_METHOD' => 'Метод оновлення бази даних',
    'LBL_UW_DB_NO_ADD_COLUMN' => 'ALTER TABLE [table] ADD COLUMN [column]',
    'LBL_UW_DB_NO_CHANGE_COLUMN' => 'ALTER TABLE [table] CHANGE COLUMN [column]',
    'LBL_UW_DB_NO_CREATE' => 'CREATE TABLE [table]',
    'LBL_UW_DB_NO_DELETE' => 'DELETE FROM [table]',
    'LBL_UW_DB_NO_DROP_COLUMN' => 'ALTER TABLE [table] DROP COLUMN [column]',
    'LBL_UW_DB_NO_DROP_TABLE' => 'DROP TABLE [table]',
    'LBL_UW_DB_NO_ERRORS' => 'Всі доступні привілеї ',
    'LBL_UW_DB_NO_INSERT' => 'INSERT INTO [table]',
    'LBL_UW_DB_NO_SELECT' => 'SELECT [x] FROM [table]',
    'LBL_UW_DB_NO_UPDATE' => 'UPDATE [table]',
    'LBL_UW_DB_PERMS' => 'Необхідний привілей',

    'LBL_UW_DESC_MODULES_INSTALLED' => 'Були встановлені наступні оновлення:',
    'LBL_UW_END_LOGOUT_PRE' => 'Оновлення завершено',
    'LBL_UW_END_LOGOUT_PRE2' => 'Натисніть Готово для виходу з Майстра Оновлення',
    'LBL_UW_END_LOGOUT' => 'Оновлення завершено. Будь ласка, вийдіть і увійдіть в систему заново, якщо ви плануєте в даний момент продовжити оновлення, використовуючи Майстер оновлення.',

    'LBL_UW_FILE_DELETED' => "був видалений.",
    'LBL_UW_FILE_GROUP' => 'Група',
    'LBL_UW_FILE_ISSUES_PERMS' => 'Права доступу до файлів:',
    'LBL_UW_FILE_NO_ERRORS' => 'Всі файли доступні для запису',
    'LBL_UW_FILE_OWNER' => 'Власник',
    'LBL_UW_FILE_PERMS' => 'Права доступу',
    'LBL_UW_FILE_UPLOADED' => 'було завантажено',
    'LBL_UW_FILE' => 'Ім\'я файлу',
    'LBL_UW_FILES_QUEUED' => 'Наступні оновлення готові до встановлення:',
    'LBL_UW_FILES_REMOVED' => "Наступні файли будуть видалені з системи:<br>",
    'LBL_UW_NEXT_TO_UPLOAD' => "Натисніть Далі для завантаження оновлень.",
    'LBL_UW_FROZEN' => 'Перед тим як продовжити, необхідно виконати наступні дії:',
    'LBL_UW_HIDE_DETAILS' => 'Сховати подробиці',
    'LBL_UW_IN_PROGRESS' => 'У процесі',
    'LBL_UW_INCLUDING' => 'Включаючи',
    'LBL_UW_INCOMPLETE' => 'Не закінчено',
    'LBL_UW_MANUAL_MERGE' => 'Перезапис файлу:',
    'LBL_UW_MODULE_READY' => 'Module is ready to be installed. Click Commit to proceed with installation.',
    'LBL_UW_NO_INSTALLED_UPGRADES' => 'Встановлених оновлень не виявлено.',
    'LBL_UW_NONE' => 'Немає',
    'LBL_UW_OVERWRITE_DESC' => "Всі змінені файли будуть перезаписані, включаючи файли вихідного коду, а також шаблони. Продовжити?",

    'LBL_UW_PREFLIGHT_ADD_TASK' => 'Створити завдання для перезапису вручну?',
    'LBL_UW_PREFLIGHT_EMAIL_REMINDER' => 'Відправити собі лист з нагадуванням про перезапис вручну?',
    'LBL_UW_PREFLIGHT_FILES_DESC' => 'Перераховані файли будуть змінені. Зніміть позначку з тих файлів, для яких буде виконано перезапис вручну. <i>Файли, які не потребують зміни залишилися невідміченими; встановлення позначки призведе до їх перезапису.',
    'LBL_UW_PREFLIGHT_NO_DIFFS' => 'Не потрібен перезапис файлів вручну.',
    'LBL_UW_PREFLIGHT_NOT_NEEDED' => 'Немає необхідності.',
    'LBL_UW_PREFLIGHT_PRESERVE_FILES' => 'Файли, залишені без змін:',
    'LBL_UW_PREFLIGHT_TESTS_PASSED' => 'Попередню перевірку пройдено успішно.',
    'LBL_UW_PREFLIGHT_TESTS_PASSED2' => 'Натисніть кнопку "Далі" для застосування змін.',
    'LBL_UW_PREFLIGHT_TESTS_PASSED3' => 'Будь ласка, зверніть увагу, що процес оновлення, який залишився, є обов&#039;язковим, і натиснувши на кнопку "Продовжити" Ви будете повинні завершити процес. Якщо Ви не хочете продовжувати, натисніть кнопку скасування.',
    'LBL_UW_PREFLIGHT_TOGGLE_ALL' => 'Перезаписати всі файли',

    'LBL_UW_REBUILD_TITLE' => 'Результати оновлення',
    'LBL_UW_SCHEMA_CHANGE' => 'Зміни схеми',

    'LBL_UW_SHOW_COMPLIANCE' => 'Показати виявлені налаштування',
    'LBL_UW_SHOW_DB_PERMS' => 'Показати відсутні права доступу до бази даних.',
    'LBL_UW_SHOW_DETAILS' => 'Показати подробиці',
    'LBL_UW_SHOW_DIFFS' => 'Показати файли, необхідні для перезапису вручну',
    'LBL_UW_SHOW_NW_FILES' => 'Показати файли з недостатніми правами доступу',
    'LBL_UW_SHOW_SCHEMA' => 'Показати скрипт зміни схеми',
    'LBL_UW_SHOW_SQL_ERRORS' => 'Показати некоректні запити',
    'LBL_UW_SHOW' => 'Показати ',

    'LBL_UW_SKIPPED_FILES_TITLE' => 'Пропущені файли',
    'LBL_UW_SQL_RUN' => 'Перевіряти, коли запит був виконаний вручну',
    'LBL_UW_START_DESC' => 'Ласкаво просимо до майстра оновлень. Майстер створений для того, щоб допомогти адміністратору при оновленні системи SuiteCRM. ',
    'LBL_UW_START_DESC2' => 'Наполегливо рекомендуємо перед оновленням системи зробити резервні копії як бази даних, так і системних файлів (всіх файлів у папці SuiteCRM). Також рекомендуємо спочатку провести тестове оновлення на копії вашої системи.',
    'LBL_UW_START_DESC3' => 'Натисніть кнопку "Продовжити" для перевірки системи, щоб переконатися в тому, що вона готова до оновлення. Перевірка включає в себе права доступу до файлів, привілеї баз даних та налаштування сервера.',
    'LBL_UW_START_UPGRADED_UW_DESC' => 'Майстер оновлення зараз продовжить оновлення системи.',
    'LBL_UW_START_UPGRADED_UW_TITLE' => 'Ласкаво просимо до майстра оновлення',

    'LBL_UW_TITLE_CANCEL' => 'Скасування',
    'LBL_UW_TITLE_COMMIT' => 'Виконання оновлення',
    'LBL_UW_TITLE_END' => 'Останні штрихи',
    'LBL_UW_TITLE_PREFLIGHT' => 'Попередня перевірка',
    'LBL_UW_TITLE_START' => 'Ласкаво просимо',
    'LBL_UW_TITLE_SYSTEM_CHECK' => 'Перевірка системи',
    'LBL_UW_TITLE_UPLOAD' => 'Завантаження оновлення',
    'LBL_UW_TITLE' => 'Майстер оновлення',
    'LBL_UW_UNINSTALL' => 'Видалення',
    //500 upgrade labels
    'LBL_UW_ACCEPT_THE_LICENSE' => 'Прийняти ліцензію',
    'LBL_UW_CONVERT_THE_LICENSE' => 'Конвертувати ліцензію',

    'LBL_START_UPGRADE_IN_PROGRESS' => 'Виконується початковий етап оновлення',
    'LBL_SYSTEM_CHECKS_IN_PROGRESS' => 'Виконується перевірка системи',
    'LBL_LICENSE_CHECK_IN_PROGRESS' => 'Виконується перевірка ліцензії',
    'LBL_PREFLIGHT_CHECK_IN_PROGRESS' => 'Виконується попередня перевірка',
    'LBL_PREFLIGHT_FILE_COPYING_PROGRESS' => 'Виконується копіювання файлів',
    'LBL_COMMIT_UPGRADE_IN_PROGRESS' => 'Виконується основний етап оновлення',
    'LBL_UW_COMMIT_DESC' => 'Натисніть "Продовжити", щоб запустити додаткові скрипти для оновлення.',
    'LBL_UPGRADE_SCRIPTS_IN_PROGRESS' => 'Виконується оновлення скриптів',
    'LBL_UPGRADE_SUMMARY_IN_PROGRESS' => 'Виконується заключний етап оновлення',
    'LBL_UPGRADE_IN_PROGRESS' => 'у процесі',
    'LBL_UPGRADE_TIME_ELAPSED' => 'Витрачений час',
    'LBL_UPGRADE_CANCEL_IN_PROGRESS' => 'Оновлення скасовано, виконується очищення',
    'LBL_UPGRADE_TAKES_TIME_HAVE_PATIENCE' => 'Оновлення може зайняти деякий час',
    'LBL_UPLOADE_UPGRADE_IN_PROGRESS' => 'Виконується перевірка завантаженого пакета оновлення',
    'LBL_UPLOADING_UPGRADE_PACKAGE' => 'Виконується завантаження пакету оновлень',
    'LBL_UW_DROP_SCHEMA_UPGRADE_WIZARD' => 'Майстер Оновлення видаляє стару схему 451',
    'LBL_UW_DROP_SCHEMA_MANUAL' => 'Очистити старі схеми вручну після завершення оновлення',
    'LBL_UW_DROP_SCHEMA_METHOD' => 'Метод очищення старих схем',
    'LBL_UW_SHOW_OLD_SCHEMA_TO_DROP' => 'Показувати старі схеми, які можна очистити',
    'LBL_UW_SKIPPED_QUERIES_ALREADY_EXIST' => 'Пропущені запити',
    'LBL_INCOMPATIBLE_PHP_VERSION' => 'Вимагається PHP версії 5 або вище.',
    'ERR_CHECKSYS_PHP_INVALID_VER' => 'Ваша версія PHP не підтримується в SuiteCRM. Вам необхідно буде встановити версію, яка підтримується додатком SuiteCRM. Будь ласка, зверніться до Матриці Сумісності в примітках до Версії, щоб дізнатися, які версії PHP підтримуються. Ваша поточна версія: ',
    'LBL_BACKWARD_COMPATIBILITY_ON' => 'У PHP включений режим зворотної сумісності. Для продовження вимкніть параметр zend.ze1_compatibility_mode',
    //including some strings from moduleinstall that are used in Upgrade
    'LBL_ML_ACTION' => 'Дія',
    'LBL_ML_CANCEL' => 'Скасування',
    'LBL_ML_COMMIT' => 'Підтвердити',
    'LBL_ML_DESCRIPTION' => 'Опис',
    'LBL_ML_INSTALLED' => 'Дата установки',
    'LBL_ML_NAME' => 'Назва',
    'LBL_ML_PUBLISHED' => 'Дата публікації',
    'LBL_ML_TYPE' => 'Тип',
    'LBL_ML_UNINSTALLABLE' => 'Видалення',
    'LBL_ML_VERSION' => 'Версія',
    'LBL_ML_INSTALL' => 'Встановити',
    //adding the string used in tracker. copying from homepage
    'LBL_CURRENT_PHP_VERSION' => '(Поточна версія php ',
	'LBL_RECOMMENDED_PHP_VERSION_1' => 'The recommended php version is ',
	'LBL_RECOMMENDED_PHP_VERSION_2' => ' or above.', // End of a sentence as in Recommended PHP version or above

    'LBL_MODULE_NAME' => 'Майстер оновлення',
    'LBL_UPLOAD_SUCCESS' => 'Оновлення успішно завантажено. Натисніть "Продовжити" для остаточної перевірки.',
    'LBL_UW_TITLE_LAYOUTS' => 'Затвердити макети',
    'LBL_LAYOUT_MODULE_TITLE' => 'Макети',
    'LBL_LAYOUT_MERGE_DESC' => 'Доступні нові поля, які були додані під час оновлення і можуть бути автоматично застосовані до існуючого зовнішньому вигляду ваших модулів. Більше про нові полях можна дізнатися в Release Notes версії, до якої Ви оновлюєте систему. Якщо Ви не бажаєте додавати нові поля, зніміть позначку з модуля і Ваші налаштування залишаться без змін. Поля будуть доступні в Студії після завершення оновлення.',
    'LBL_LAYOUT_MERGE_TITLE' => 'Натисніть "продовжити", щоб підтвердити зміни і закінчити оновлення.',
    'LBL_LAYOUT_MERGE_TITLE2' => 'Натисніть "продовжити", щоб закінчити оновлення.',
    'LBL_UW_CONFIRM_LAYOUTS' => 'Підтвердити макети',
    'LBL_UW_CONFIRM_LAYOUT_RESULTS' => 'Підтвердити результати макетів',
    'LBL_UW_CONFIRM_LAYOUT_RESULTS_DESC' => 'Наступні макети були успішно перезаписані:',
    'LBL_SELECT_FILE' => 'Вибір файлу',
    'ERROR_VERSION_INCOMPATIBLE' => 'Завантажуваний файл не сумісний з встановленою версією SuiteCRM:',
    'ERROR_PHP_VERSION_INCOMPATIBLE' => 'The uploaded file is not compatible with this version of PHP: ',
    'ERROR_SUITECRM_VERSION_INCOMPATIBLE' => 'Завантажуваний файл не сумісний з встановленою версією SuiteCRM Suite:',
    'ERROR_FLAVOR_INCOMPATIBLE' => 'Завантажений файл не сумісний з встановленою версією SuiteCRM Suite (Community Edition, Professional або Enterprise):',
    'LBL_LANGPACKS' => 'Language Packs' /*for 508 compliance fix*/,
    'LBL_MODULELOADER' => 'Завантажувач модулів' /*for 508 compliance fix*/,
    'LBL_PATCHUPGRADES' => 'Patch Upgrades' /*for 508 compliance fix*/,
    'LBL_THEMES' => 'Теми' /*for 508 compliance fix*/,
    'LBL_WORKFLOW' => 'Workflow' /*for 508 compliance fix*/,
    'LBL_UPGRADE' => 'Оновлення' /*for 508 compliance fix*/,
    'LBL_PROCESSING' => 'Обробляється...' /*for 508 compliance fix*/,
    'ERROR_NO_VERSION_SET' => 'Compatible version is not set in manifest file',
    'LBL_UPGRD_CSTM_CHK' => 'Upgrade process will update some files but these files also exist in custom/ folder. Please review the changes before continuing:',
    'ERR_UW_PHP_FILE_ERRORS' => array(
        1 => 'Завантажений файл більше, ніж зазначено в upload_max_filesize в php.ini.',
        2 => 'Завантажений файл більше, ніж зазначено в MAX_FILE_SIZE в HTML формі.',
        3 => 'Файл був тільки частково завантажений.',
        4 => 'Файл не був заружен.',
        5 => 'Невідома помилка.',
        6 => 'Тимчасова папка не була створена.',
        7 => 'Не вдалося записати файл на диск.',
        8 => 'Завантаження файлу зупинено розширенням.',
    ),
);
