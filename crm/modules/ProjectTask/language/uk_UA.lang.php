<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
	'LBL_MODULE_NAME' => 'Проектні завдання',
	'LBL_MODULE_TITLE' => 'Проектні завдання: Головна',

	'LBL_ID' => 'ID:',
    'LBL_PROJECT_TASK_ID' => 'ID проектного завдання:',
    'LBL_PROJECT_ID' => 'ID проекту:',
	'LBL_DATE_ENTERED' => 'Дата створення:',
	'LBL_DATE_MODIFIED' => 'Дата зміни:',
	'LBL_ASSIGNED_USER_ID' => 'Відповідальний (а):',
	'LBL_MODIFIED_USER_ID' => 'Змінено користувачем (ID):',
	'LBL_CREATED_BY' => 'Створено:',
	'LBL_NAME' => 'Назва',
	'LBL_STATUS' => 'Статус:',
	'LBL_DATE_DUE' => 'Дата закінчення:',
	'LBL_TIME_DUE' => 'Час закінчення:',
    'LBL_PREDECESSORS' => 'Попередники:',
	'LBL_DATE_START' => 'Дата початку:',
    'LBL_DATE_FINISH' => 'Дата закінчення:',    
	'LBL_TIME_START' => 'Час початку:',
    'LBL_TIME_FINISH' => 'Час закінчення:',
    'LBL_DURATION' => 'Тривалість:',
    'LBL_DURATION_UNIT' => 'Одиниця тривалості:',
    'LBL_ACTUAL_DURATION' => 'Фактична тривалість',
	'LBL_PARENT_ID' => 'Проект:',
    'LBL_PARENT_TASK_ID' => 'Id батьківського завдання:',    
    'LBL_PERCENT_COMPLETE' => '% виконання:',
	'LBL_PRIORITY' => 'Пріоритет:',
	'LBL_DESCRIPTION' => 'Замітки',
	'LBL_ORDER_NUMBER' => 'Замовлення:',
	'LBL_TASK_NUMBER' => 'Номер завдання:',
    'LBL_TASK_ID' => 'ID завдання',
	'LBL_MILESTONE_FLAG' => 'Проміжний етап розробки:',
	'LBL_ESTIMATED_EFFORT' => 'Оцінка зусиль (год.):',
	'LBL_ACTUAL_EFFORT' => 'Реальні зусилля (год.):',
	'LBL_UTILIZATION' => 'Використання (%) :',
	'LBL_DELETED' => 'Видалено:',
    'LBL_PROJECT_PREDECESSOR_NONE' => 'Немає', // PR 4366

	'LBL_LIST_NAME' => 'Назва',
	'LBL_LIST_PARENT_NAME' => 'Проект',
	'LBL_LIST_PERCENT_COMPLETE' => '% виконання',
	'LBL_LIST_STATUS' => 'Статус',
	'LBL_LIST_ASSIGNED_USER_ID' => 'Відповідальний (- а)',
	'LBL_LIST_DATE_DUE' => 'Дата закінчення',
	'LBL_LIST_PRIORITY' => 'Пріоритет',
	'LBL_LIST_CLOSE' => 'Закрити',
	'LBL_PROJECT_NAME' => 'Назва проекту',

	'LNK_NEW_PROJECT'	=> 'Новий проект',
	'LNK_PROJECT_LIST'	=> 'Список проектів',
	'LNK_NEW_PROJECT_TASK'	=> 'Створити завдання по проекту',
	'LNK_PROJECT_TASK_LIST'	=> 'Проектні завдання',

	'LBL_LIST_MY_PROJECT_TASKS' => 'Мої проектні завдання',
	'LBL_DEFAULT_SUBPANEL_TITLE' => 'Проектні завдання',
	'LBL_NEW_FORM_TITLE' => 'Створити проектне завдання',

	'LBL_HISTORY_TITLE'=>'Історія',
	'LBL_ACTIVITIES_SUBPANEL_TITLE'=>'Заходи',
	'LBL_HISTORY_SUBPANEL_TITLE'=>'Історія', 

    'LBL_ASSIGNED_USER_NAME' => 'Відповідальний (- а)',
    'LBL_PARENT_NAME' => 'Назва проекту',
	'LBL_EDITLAYOUT' => 'Правка розташування' /*for 508 compliance fix*/,
    'LBL_PANEL_TIMELINE' => 'Timeline',

	'LBL_SUBTASK' => 'Sub-Task',
	'LBL_LAG' => 'Lag',
	'LBL_DAYS' => 'Днів',
	'LBL_HOURS' => 'Годин',
	'LBL_RELATIONSHIP_TYPE' => 'Тип зв&#039;язку',
);

?>