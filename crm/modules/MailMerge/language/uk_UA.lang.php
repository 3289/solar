<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
  'LBL_STEP_1' => 'Крок 1: Вибір модуля і шаблону',
  'LBL_MAILMERGE_MODULE' => 'Вибір модуля: ',
  'LBL_MAILMERGE_SELECTED_MODULE' => 'Вибраний модуль:',
  'LBL_MAILMERGE_TEMPLATES' => 'Вибір шаблону:',
  'LBL_STEP_4' => 'Перегляд і завершення',
  'LBL_SELECTED_MODULE' => 'Вибраний модуль:',
  'LBL_SELECTED_TEMPLATE' => 'Вибраний шаблон:',
  'LBL_SELECTED_ITEMS' => 'Вибрані записи:',
  'LBL_STEP_5' => 'Об&#039;єднання пошти завершено',
  'LBL_MERGED_FILE' => 'Файл об&#039;єднання:',
  'LNK_NEW_MAILMERGE' => 'Нове об&#039;єднання',
  'LNK_UPLOAD_TEMPLATE' => 'Завантажити шаблон',
  'LBL_DOC_NAME' => 'Назва документу:',
  'LBL_FILENAME' => 'Ім\'я файлу:',
  'LBL_DOC_VERSION' => 'Версія:',
  'LBL_DOC_DESCRIPTION'=>'Опис:',
  'LBL_LIST_NAME' => 'Назва',
  'LBL_FINISH' => 'Почати об&#039;єднання',
  'LBL_NEXT' => 'Далі >',
  'LBL_BACK' => '< Назад',
  'LBL_START' => 'Клацніть мишкою, щоб продовжити',
  'LBL_CONTAINS_CONTACT_INFO' => 'Вибраний шаблон містить контактну інформацію.',
  'LBL_ADDIN_NOTICE' => 'This requires the installation of SuiteCRM Mail Merge add-in to Microsoft Word.',
  'LBL_REMOVE' => 'Remove Item(s)' /*for 508 compliance fix*/,
  'LBL_ADD' => 'Add Item(s)' /*for 508 compliance fix*/,
  'LBL_BROWSER_REQUIREMENT' => 'This requires IE 6.0 or greater for Windows.',
  'LBL_FORM_SELECT_TEMPLATE' => 'Please select a template.',
  'LBL_SELECT_ONE' => 'Please select at least one item.',
  'LBL_AVAILABLE' => 'Доступні',
  'LBL_SELECTED' => 'Обрано',
  'LBL_RELATED_INFO_SELECTION' => 'Use the selections below to identify the related information you would like to display in your merged document. Skip this step if your template document contains no related information.',

);


?>
