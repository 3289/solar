<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
	'ERR_DELETE_RECORD' => 'Вам слід вказати номер запису перед видаленням контрагента.',
	'LBL_ACCOUNT_ID' => 'Контрагент:',
	'LBL_CASE_ID' => 'Звернення:',
	'LBL_CLOSE' => 'Закрити:',
	'LBL_CONTACT_ID' => 'Контакту',
	'LBL_CONTACT_NAME' => 'Контакт:',
	'LBL_DEFAULT_SUBPANEL_TITLE' => 'Нотатки',
	'LBL_DESCRIPTION' => 'Розділ для нотаток',
	'LBL_EMAIL_ADDRESS' => 'Адреса електронної пошти:',
    'LBL_EMAIL_ATTACHMENT' => 'Вкладення',
	'LBL_FILE_MIME_TYPE' => 'Тип MIME',
	'LBL_FILE_URL' => 'Адреса файлу',
	'LBL_FILENAME' => 'Вкладення:',
	'LBL_LEAD_ID' => 'Попередній контакт:',
	'LBL_LIST_CONTACT_NAME' => 'Контакт',
	'LBL_LIST_DATE_MODIFIED' => 'Остання зміна',
	'LBL_LIST_FILENAME' => 'Вкладення',
	'LBL_LIST_FORM_TITLE' => 'Список приміток',
	'LBL_LIST_RELATED_TO' => 'Відноситься до:',
	'LBL_LIST_SUBJECT' => 'Тема',
	'LBL_LIST_STATUS' => 'Статус',
	'LBL_LIST_CONTACT' => 'Контакт',
	'LBL_MODULE_NAME' => 'Нотатки',
	'LBL_MODULE_TITLE' => 'Нотатки: Головна',
	'LBL_NEW_FORM_TITLE' => 'Нова нотатка або вкладення',
	'LBL_NOTE_STATUS' => 'Замітка',
	'LBL_NOTE_SUBJECT' => 'Тема нотатки',
	'LBL_NOTES_SUBPANEL_TITLE' => 'Вкладення',
	'LBL_NOTE' => 'Замітка:',
	'LBL_OPPORTUNITY_ID' => 'Угода:',
	'LBL_PARENT_ID' => 'Код початкового запису:',
	'LBL_PARENT_TYPE' => 'Тип початкового запису',
	'LBL_PHONE' => 'Телефон:',
	'LBL_PORTAL_FLAG' => 'Відобразити в порталі?',
	'LBL_EMBED_FLAG' => 'Вставити в Е-mail?',
	'LBL_PRODUCT_ID' => 'Продукт:',
	'LBL_QUOTE_ID' => 'Комерційна пропозиція:',
	'LBL_RELATED_TO' => 'Відноситься до:',
	'LBL_SEARCH_FORM_TITLE' => 'Знайти нотатку',
	'LBL_STATUS' => 'Статус',
	'LBL_SUBJECT' => 'Тема:',
	'LNK_IMPORT_NOTES' => 'Імпорт нотаток',
	'LNK_NEW_NOTE' => 'Нова примітка або вкладення',
	'LNK_NOTE_LIST' => 'Нотатки',
	'LBL_MEMBER_OF' => 'Входить до:',
	'LBL_LIST_ASSIGNED_TO_NAME' => 'Відповідальний (- а)',
    'LBL_REMOVING_ATTACHMENT'=>'Видалення вкладення...',
    'ERR_REMOVING_ATTACHMENT'=>'Помилка: не вдалося видалити вкладення...',
    'LBL_CREATED_BY'=>'Створено',
    'LBL_MODIFIED_BY'=>'Автор змін',
    'LBL_SEND_ANYWAYS'=> 'У цього e-mail-повідомлення немає теми. Відправити/зберегти все одно?',
	'LBL_NOTE_INFORMATION' => 'Огляд замітки', //Can be translated in all caps. This string will be used by SuiteP template menu actions
	'LBL_MY_NOTES_DASHLETNAME' => 'Мої нотатки',
	'LBL_EDITLAYOUT' => 'Правка розташування' /*for 508 compliance fix*/,
    //For export labels
	'LBL_FIRST_NAME' => 'Ім\'я',
    'LBL_LAST_NAME' => 'Прізвище',
    'LBL_DATE_ENTERED' => 'Дата створення',
    'LBL_DATE_MODIFIED' => 'Дата зміни',
    'LBL_DELETED' => 'Видалено',
);

?>
