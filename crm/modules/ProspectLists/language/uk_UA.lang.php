<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
  'LBL_MODULE_NAME' => 'Цільові списки',
  'LBL_MODULE_ID'   => 'Цільові списки',
  'LBL_MODULE_TITLE' => 'Цільові списки: Головна',
  'LBL_SEARCH_FORM_TITLE' => 'Пошук цільових списків',
  'LBL_LIST_FORM_TITLE' => 'Цільові списки',
  'LBL_PROSPECT_LIST_NAME' => 'Цільовий список:',
  'LBL_NAME' => 'Назва',
  'LBL_ENTRIES' => 'Кількість адрес',
  'LBL_LIST_PROSPECT_LIST_NAME' => 'Цільовий список',
  'LBL_LIST_ENTRIES' => 'Кількість адрес',
  'LBL_LIST_DESCRIPTION' => 'Опис',
  'LBL_LIST_TYPE_NO' => 'Тип',
  'LBL_LIST_END_DATE' => 'Дата закінчення',
  'LBL_DATE_ENTERED' => 'Дата створення',
  'LBL_MARKETING_ID' => 'Кампанія',
  'LBL_DATE_MODIFIED' => 'Дата зміни',
  'LBL_MODIFIED' => 'Змінено',
  'LBL_CREATED' => 'Створено',
  'LBL_ASSIGNED_TO' => 'Відповідальний (-а):',
  'LBL_DESCRIPTION' => 'Опис',
  'LNK_NEW_CAMPAIGN' => 'Створити маркетингову кампанію',
  'LNK_CAMPAIGN_LIST' => 'Маркетингові кампанії',
  'LNK_NEW_PROSPECT_LIST' => 'Новий цільовий список',
  'LNK_PROSPECT_LIST_LIST' => 'Цільові списки',
  'LBL_MODIFIED_BY' => 'Автор змін',
  'LBL_CREATED_BY' => 'Створено',
  'LNK_NEW_PROSPECT' => 'Створити адресата',
  'LNK_PROSPECT_LIST' => 'Адресати',

  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Контакти',
  'LBL_LEADS_SUBPANEL_TITLE' => 'Попередні контакти',
  'LBL_PROSPECTS_SUBPANEL_TITLE'=>'Адресати',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Клієнти',
  'LBL_COPY_PREFIX' =>'Копія',
  'LBL_USERS_SUBPANEL_TITLE' =>'Користувачі',
  'LBL_TYPE' => 'Тип',
  'LBL_LIST_TYPE' => 'Тип',
  'LBL_LIST_TYPE_LIST_NAME'=>'Тип',
  'LBL_NEW_FORM_TITLE'=>'Новий цільовий список',
  'LBL_MARKETING_NAME'=>'Назва кампанії',
  'LBL_DOMAIN_NAME'=>'Доменне ім&#039;я',
  'LBL_DOMAIN'=>'Немає e-mail-повідомлень для домену',
  'LBL_LIST_PROSPECTLIST_NAME'=>'Назва',

  'LBL_EMAIL_MARKETING'=>'Розсилки E-mail',

);


?>
