<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
	//module
	'LBL_MODULE_NAME' => 'Ревізії документа',

	'LNK_NEW_DOCUMENT' => 'Створити документ',
	'LNK_DOCUMENT_LIST'=> 'Документи',

	//vardef labels
	'LBL_REVISION_NAME' => 'Номер ревізії',
	'LBL_FILENAME' => 'Файл:',
	'LBL_MIME' => 'Тип MIME',
	'LBL_REVISION' => 'Ревізія',
	'LBL_DOCUMENT' => 'Відповідний документ',
	'LBL_LATEST_REVISION' => 'Остання ревізія',
	'LBL_CHANGE_LOG'=> 'Історія змін',
	'LBL_ACTIVE_DATE'=> 'Дата публікації',
	'LBL_EXPIRATION_DATE' => 'Термін дії',
	'LBL_FILE_EXTENSION'  => 'Розширення файлу',

	'LBL_DOC_NAME' => 'Назва документу:',
	'LBL_DOC_VERSION' => 'Ревізія:',

	//document revisions.
	'LBL_REV_LIST_REVISION' => 'Ревізія',
	'LBL_REV_LIST_ENTERED' => 'Дата створення',
	'LBL_REV_LIST_CREATED' => 'Створено',
	'LBL_REV_LIST_LOG'=> 'Історія змін',
	'LBL_REV_LIST_FILENAME' => 'Файл',

	'LBL_CURRENT_DOC_VERSION'=> 'Остання ревізія:',
	'LBL_SEARCH_FORM_TITLE'=> 'Знайти документ',
    'LBL_REVISIONS' => 'Версії',

	//error messages
	'ERR_FILENAME'=> 'Ім\'я файлу',
	'ERR_DOC_VERSION'=> 'Ревізія документа',
	'ERR_DELETE_CONFIRM'=> 'Ви хочете видалити цю ревізію документа?',
	'ERR_DELETE_LATEST_VERSION'=> 'Ви не маєте права видаляти останню ревізію документа.',
	'LNK_NEW_MAIL_MERGE' => 'Об&#039;єднання пошти',
	'LBL_DOC_ID' => 'ID джерела документа',
	'LBL_DOC_TYPE' => 'Джерело',
	'LBL_DOC_URL' => 'URL джерела документа',

	'LBL_CREATED_BY_NAME' => 'Created by Name',
);


?>
