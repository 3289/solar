<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array (
	'LBL_ADD_DOCUMENT'			=> 'Add a Document',
	'LBL_ADD_FILE'				=> 'Додати файл',
	'LBL_ATTACHMENTS'			=> 'Вкладення',
	'LBL_BODY'					=> 'Текст повідомлення:',
	'LBL_CLOSE'					=> 'Закрити:',
	'LBL_DESCRIPTION'			=> 'Опис:',
	'LBL_EDIT_ALT_TEXT'			=> 'Редагування текстового повідомлення',
	'LBL_EMAIL_ATTACHMENT'		=> 'Вкладення',
	'LBL_HIDE_ALT_TEXT'			=> 'Приховати Plain Text',
	'LBL_HTML_BODY'				=> 'Текст HTML',
	'LBL_INSERT_VARIABLE'		=> 'Вставити змінну:',
	'LBL_INSERT_URL_REF'		=> 'Вставити URL-посилання',
	'LBL_INSERT_TRACKER_URL'	=> 'Вставити URL трекера:',
	'LBL_INSERT'				=> 'Вставити',
	'LBL_LIST_DATE_MODIFIED'	=> 'Остання зміна',
	'LBL_LIST_DESCRIPTION'		=> 'Опис',
	'LBL_LIST_FORM_TITLE'		=> 'Список шаблонів повідомлень',
	'LBL_LIST_NAME'				=> 'Назва',
	'LBL_MODULE_NAME'			=> 'Шаблони повідомлень',
	'LBL_MODULE_TITLE'			=> 'Шаблони повідомлень: Головна',
	'LBL_NAME'					=> 'Назва:',
	'LBL_NEW_FORM_TITLE'		=> 'Створити шаблон повідомлення',
	'LBL_PUBLISH'				=> 'Опублікувати:',
	'LBL_RELATED_TO'			=> 'Відноситься до:',
	'LBL_SEARCH_FORM_TITLE'		=> 'Знайти шаблон повідомлення',
	'LBL_SHOW_ALT_TEXT'			=> 'Показати як звичайний текст',
	'LBL_SUBJECT'				=> 'Тема:',
    'LBL_SUGAR_DOCUMENT'        => 'Документ',
	'LBL_TEXT_BODY'				=> 'Текст листа',
	'LBL_USERS'					=> 'Користувачі',

	'LNK_EMAIL_TEMPLATE_LIST'	=> 'Переглянути шаблони повідомлень',
	'LNK_IMPORT_NOTES'			=> 'Імпорт нотаток',
	'LNK_NEW_EMAIL_TEMPLATE'	=> 'Створити шаблон повідомлення',
	'LNK_NEW_EMAIL'				=> 'Відправити повідомлення в архів',
	'LNK_NEW_SEND_EMAIL'		=> 'Написати листа',
	'LNK_SENT_EMAIL_LIST'		=> 'Відправлені',
	'LNK_VIEW_CALENDAR'			=> 'Сьогодні',
	// for Inbox
	'LBL_NEW'					=> 'Нове',
	'LNK_MY_DRAFTS'				=> 'Мої чернетки',
	'LNK_MY_INBOX'				=> 'Моя пошта',
    'LBL_TEXT_ONLY'             => 'Звичайний текст',
    'LBL_SEND_AS_TEXT'          => 'Відправити як звичайний текст',
    'LBL_ACCOUNT'               => 'Контрагент',
    'LBL_FROM_NAME'=>'Від (ім&#039;я)',
    'LBL_PLAIN_TEXT'=>'Нешифрований текст',
    'LBL_CREATED_BY'=>'Створено',
    'LBL_PUBLISHED'=>'Опубліковано',
    'LNK_VIEW_MY_INBOX' => 'Подивитися мою пошту',
	'LBL_ASSIGNED_TO_ID' => 'Відповідальний (- а)',
	'LBL_EDIT_LAYOUT' => 'Правка розташування' /*for 508 compliance fix*/,
	'LBL_SELECT' => 'Вибрати' /*for 508 compliance fix*/,
	'LBL_ID_FF_CLEAR' => 'Очистити' /*for 508 compliance fix*/,
    'LBL_TYPE' => 'Тип',
	'LBL_WIDTH' => 'Width Default',
    'LBL_EDIT_BUTTON_TITLE_MOZAIK' => 'RWD Edit', // RWD for Responsive Web Design. PR 3095
    'LBL_DIRECT_HTML' => 'Direct Html', // PR 2995
);
?>
