<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Головна',
    'LBL_NEW_FORM_TITLE' => 'Новий контакт',
    'LBL_FIRST_NAME' => 'Ім\'я:',
    'LBL_LAST_NAME' => 'Прізвище',
    'LBL_LIST_LAST_NAME' => 'Прізвище',
    'LBL_PHONE' => 'Тел. робочий:',
    'LBL_EMAIL_ADDRESS' => 'E-mail-адреса:',
    'LBL_MY_PIPELINE_FORM_TITLE' => 'Діаграма стадій моїх продажів',
    'LBL_PIPELINE_FORM_TITLE' => 'Діаграма стадій загальних продажів',
    'LBL_RGraph_PIPELINE_FORM_TITLE' => 'Діаграма стадій загальних продажів',
    'LNK_NEW_CONTACT' => 'Новий контакт',
    'LNK_NEW_ACCOUNT' => 'Новий контрагент',
    'LNK_NEW_OPPORTUNITY' => 'Нова угода',
    'LNK_NEW_LEAD' => 'Новий попередній контакт',
    'LNK_NEW_CASE' => 'Нове звернення',
    'LNK_NEW_NOTE' => 'Створити нотатку або вкладення',
    'LNK_NEW_CALL' => 'Журнал дзвінків',
    'LNK_NEW_EMAIL' => 'Відправити E-mail в архів',
    'LNK_NEW_MEETING' => 'Призначити зустріч',
    'LNK_NEW_TASK' => 'Нове завдання',
    'LNK_NEW_BUG' => 'Нова помилка',
    'LBL_ADD_BUSINESSCARD' => 'Додати візитну картку',
    'LNK_NEW_SEND_EMAIL' => 'Написати листа',
    'LBL_NO_ACCESS' => 'У Вас немає доступу до цієї сторінки. Зв&#039;яжіться з вашим системним адміністратором для отримання відповідних прав.',
    'LBL_NO_RESULTS_IN_MODULE' => '-- Немає результатів --',
    'LBL_NO_RESULTS' => '<h2>Нічого не знайдено. Будь ласка, повторіть пошук.</h2><br>',
    'LBL_NO_RESULTS_TIPS' => '<h3>Підказка по пошуку:</h3><ul><li>Переконайтеся, що ви відмітили необхідні категорії.</li><li>Вкажіть більш детальні умови пошуку.</li><li>Якщо ви все ще нічого не знайшли - спробуйте скористатися закладкою "Розширений пошук" відповідного модуля.</li></ul>',

    'LBL_ADD_DASHLETS' => 'Додати розділи SuiteCRM',
    'LBL_WEBSITE_TITLE' => 'Сайт',
    'LBL_RSS_TITLE' => 'Стрічки новин',
    'LBL_CLOSE_DASHLETS' => 'Закрити',
    'LBL_OPTIONS' => 'Опції',
    // dashlet search fields
    'LBL_TODAY' => 'Сьогодні',
    'LBL_YESTERDAY' => 'Вчора',
    'LBL_TOMORROW' => 'Завтра',
    'LBL_NEXT_WEEK' => 'Наступний тиждень',
    'LBL_LAST_7_DAYS' => 'Минулі 7 днів',
    'LBL_NEXT_7_DAYS' => 'Наступні 7 днів',
    'LBL_LAST_MONTH' => 'Минулий місяць',
    'LBL_NEXT_MONTH' => 'Наступний місяць',
    'LBL_LAST_YEAR' => 'Минулий рік',
    'LBL_NEXT_YEAR' => 'Наступний рік',
    'LBL_LAST_30_DAYS' => 'Останні 30 днів',
    'LBL_NEXT_30_DAYS' => 'Наступні 30 днів',
    'LBL_THIS_MONTH' => 'Цей місяць',
    'LBL_THIS_YEAR' => 'Цей рік',

    'LBL_MODULES' => 'Модулі',
    'LBL_CHARTS' => 'Графіки',
    'LBL_TOOLS' => 'Різне',
    'LBL_WEB' => 'Веб-реклама',
    'LBL_SEARCH_RESULTS' => 'Результати пошуку',

    // Dashlet Categories
    'dashlet_categories_dom' => array(
        'Module Views' => 'Перегляди модуля',
        'Portal' => 'Портал',
        'Charts' => 'Графіки',
        'Tools' => 'Інструментарій',
        'Miscellaneous' => 'Різне'),
    'LBL_ADDING_DASHLET' => 'Adding SuiteCRM Dashlet ...',
    'LBL_ADDED_DASHLET' => 'SuiteCRM Dashlet Added',
    'LBL_REMOVE_DASHLET_CONFIRM' => 'Are you sure you want to remove this SuiteCRM Dashlet?',
    'LBL_REMOVING_DASHLET' => 'Removing SuiteCRM Dashlet ...',
    'LBL_REMOVED_DASHLET' => 'SuiteCRM Dashlet removed',
    'LBL_DASHLET_CONFIGURE_GENERAL' => 'Основні налаштування',
    'LBL_DASHLET_CONFIGURE_FILTERS' => 'Фільтри',
    'LBL_DASHLET_CONFIGURE_MY_ITEMS_ONLY' => 'Тільки мої записи',
    'LBL_DASHLET_CONFIGURE_TITLE' => 'Назва',
    'LBL_DASHLET_CONFIGURE_DISPLAY_ROWS' => 'Відображати рядки',

    'LBL_DASHLET_DELETE' => 'Delete SuiteCRM Dashlet',
    'LBL_DASHLET_REFRESH' => 'Refresh SuiteCRM Dashlet',
    'LBL_DASHLET_EDIT' => 'Edit SuiteCRM Dashlet',

    // Default out-of-box names for tabs
    'LBL_HOME_PAGE_1_NAME' => 'My CRM',
    'LBL_CLOSE_SITEMAP' => 'Закрити',

    'LBL_SEARCH' => 'Пошук',
    'LBL_CLEAR' => 'Очистити',

    'LBL_BASIC_CHARTS' => 'Основні графіки',

    'LBL_DASHLET_SEARCH' => 'Find SuiteCRM Dashlet',

//ABOUT page
    'LBL_VERSION' => 'Версія',
    'LBL_BUILD' => 'Зборка',

    'LBL_SOURCE_SUGAR' => 'SugarCRM Inc - providers of CE framework',

  'LBL_DASHLET_TITLE' => 'Мої сайти',
  'LBL_DASHLET_OPT_TITLE' => 'Назва',
  'LBL_DASHLET_INCORRECT_URL' => 'Incorrect website location is specified',
  'LBL_DASHLET_OPT_URL' => 'Адреса веб-сайту',
  'LBL_DASHLET_OPT_HEIGHT' => 'Висота розділу (в пікселях)',
  'LBL_DASHLET_SUGAR_NEWS' => 'SuiteCRM News',
  'LBL_DASHLET_DISCOVER_SUGAR_PRO' => 'Discover SuiteCRM',
	'LBL_BASIC_SEARCH' => 'Quick Filter' /*for 508 compliance fix*/,
	'LBL_ADVANCED_SEARCH' => 'Advanced Filter' /*for 508 compliance fix*/,
    'LBL_TOUR_HOME' => 'Home Icon',
    'LBL_TOUR_HOME_DESCRIPTION' => 'Quickly get back to your Home Page dashboard in one click.',
    'LBL_TOUR_MODULES' => 'Модулі',
    'LBL_TOUR_MODULES_DESCRIPTION' => 'All your important modules are here.',
    'LBL_TOUR_MORE' => 'More Modules',
    'LBL_TOUR_MORE_DESCRIPTION' => 'The rest of your modules are here.',
    'LBL_TOUR_SEARCH' => 'Full Text Search',
    'LBL_TOUR_SEARCH_DESCRIPTION' => 'Search just got a whole lot better.',
    'LBL_TOUR_NOTIFICATIONS' => 'Повідомлення',
    'LBL_TOUR_NOTIFICATIONS_DESCRIPTION' => 'SuiteCRM application notifications would go here.',
    'LBL_TOUR_PROFILE' => 'Profile',
    'LBL_TOUR_PROFILE_DESCRIPTION' => 'Access profile, settings and logout.',
    'LBL_TOUR_QUICKCREATE' => 'Швидке створення',
    'LBL_TOUR_QUICKCREATE_DESCRIPTION' => 'Quickly create records without losing your place.',
    'LBL_TOUR_FOOTER' => 'Collapsible Footer',
    'LBL_TOUR_FOOTER_DESCRIPTION' => 'Easily expand and collapse the footer.',
    'LBL_TOUR_CUSTOM' => 'Custom Apps',
    'LBL_TOUR_CUSTOM_DESCRIPTION' => 'Custom integrations would go here.',
    'LBL_TOUR_BRAND' => 'Your Brand',
    'LBL_TOUR_BRAND_DESCRIPTION' => 'Your logo goes here. You can mouse over for more info.',
    'LBL_TOUR_WELCOME' => 'Welcome to SuiteCRM',
    'LBL_TOUR_WATCH' => 'Watch What\'s New in SuiteCRM',
    'LBL_TOUR_FEATURES' => '<ul style=""><li class="icon-ok">New simplifed navigation bar</li><li class="icon-ok">New collapsible footer</li><li class="icon-ok">Improved Search</li><li class="icon-ok">Updated actions menu</li></ul><p>and much more!</p>',
    'LBL_TOUR_VISIT' => 'For more information please visit our application',
    'LBL_TOUR_DONE' => 'You\'re Done!',
    'LBL_TOUR_REFERENCE_1' => 'You can always reference our',
    'LBL_TOUR_REFERENCE_2' => 'through the "Support Forum" link under the profile tab.',
    'LNK_TOUR_DOCUMENTATION' => 'documentation',
    'LBL_TOUR_CALENDAR_URL_1' => 'Do you share your SuiteCRM calendar with 3rd party applications, such as Microsoft Outlook or Exchange? If so, you have a new URL. This new, more secure URL includes a personal key which will prevent unauthorized publishing of your calendar.',
    'LBL_TOUR_CALENDAR_URL_2' => 'Retrieve your new shared calendar URL.',
    'LBL_CONTRIBUTORS' => 'Contributors',
    'LBL_ABOUT_SUITE' => 'About SuiteCRM',
    'LBL_PARTNERS' => 'Partners',
    'LBL_FEATURING' => 'AOS, AOW, AOR, AOP, AOE and Reschedule modules by SalesAgility.',

    'LBL_CONTRIBUTOR_SUITECRM' => 'SuiteCRM - Open source CRM for the world',
    'LBL_CONTRIBUTOR_SECURITY_SUITE' => 'SecuritySuite by Jason Eggers',
    'LBL_CONTRIBUTOR_JJW_GMAPS' => 'JJWDesign Google Maps by Jeffrey J. Walters',
    'LBL_CONTRIBUTOR_CONSCIOUS' => 'SuiteCRM LOGO Provided by Conscious Solutions',
    'LBL_CONTRIBUTOR_RESPONSETAP' => 'Contribution to SuiteCRM 7.3 release by ResponseTap',
    'LBL_CONTRIBUTOR_GMBH' => 'Workflow Calculated Fields contributed by diligent technology & business consulting GmbH',

    'LBL_LANGUAGE_ABOUT' => 'About SuiteCRM Translations',
    'LBL_LANGUAGE_COMMUNITY_ABOUT' => 'Collaborative translation by the SuiteCRM Community',
    'LBL_LANGUAGE_COMMUNITY_PACKS' => 'Translation created using Crowdin',

    'LBL_ABOUT_SUITE_2' => 'SuiteCRM is published under an open source licence - AGPLv3',
    'LBL_ABOUT_SUITE_4' => 'All SuiteCRM code managed and developed by the project will be released as open source - AGPLv3',
    'LBL_ABOUT_SUITE_5' => 'SuiteCRM support is available in both free and paid-for options',

    'LBL_SUITE_PARTNERS' => 'We have loyal SuiteCRM partners who are passionate about open source. To view our full partner list, see our website.',

);
