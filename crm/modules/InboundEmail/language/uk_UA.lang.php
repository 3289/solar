<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array(


    'LBL_RE' => 'RE:',

    'ERR_BAD_LOGIN_PASSWORD' => 'Невірний логін або пароль',
    'ERR_INI_ZLIB' => 'Неможливо тимчасово відключити Zlib-стиснення. Тестування установок може закінчитися невдало.',
    'ERR_NO_IMAP' => 'Не знайдені бібліотеки IMAP. Будь ласка, вирішіть цю проблему перед тим, як продовжити роботу з вхідною поштою',
    'ERR_NO_OPTS_SAVED' => 'Оптимальні налаштування не були збережені для вхідних листів. Будь ласка, перевірте відповідні налаштування',
    'ERR_TEST_MAILBOX' => 'Будь ласка, перевірте Ваші налаштування і спробуйте ще раз.',

    'LBL_ASSIGN_TO_USER' => 'Призначити користувачу',
    'LBL_AUTOREPLY' => 'Шаблон автоматичної відповіді',
    'LBL_AUTOREPLY_HELP' => 'Інформувати відправників електронних листів про те, що їх повідомлення було отримано.',
    'LBL_BASIC' => 'Інформація про обліковий запис',
    'LBL_CASE_MACRO' => 'Макрос для Звернень',
    'LBL_CASE_MACRO_DESC' => 'Вкажіть макрос, який буде проаналізовано і використано для зв&#039;язку імпортованого повідомлення із Зверненням.',
    'LBL_CASE_MACRO_DESC2' => 'Ви можете встановити будь-яке значення, але не змінюйте значення <b>"% 1"</b>.',
    'LBL_CLOSE_POPUP' => 'Закрити вікно',
    'LBL_CREATE_TEMPLATE' => 'Створити',
    'LBL_DELETE_SEEN' => 'Видалити прочитані листи після імпортування',
    'LBL_EDIT_TEMPLATE' => 'Правити',
    'LBL_EMAIL_OPTIONS' => 'Параметри обробки пошти',
    'LBL_EMAIL_BOUNCE_OPTIONS' => 'Параметри обробки E-mail-повідомлень, що повертаються',
    'LBL_FILTER_DOMAIN_DESC' => 'Вкажіть домен, на який не будуть відправлятися автоматичні відповіді.',
    'LBL_ASSIGN_TO_GROUP_FOLDER_DESC' => 'Select to automatically create email records in SuiteCRM for all incoming emails.',
    'LBL_FILTER_DOMAIN' => 'Не відправляти автоматичну відповідь на цей домен',
    'LBL_FIND_SSL_WARN' => 'Тестування SSL може зайняти тривалий час. Будь ласка, зачекайте...',
    'LBL_FROM_ADDR' => 'Адреса відправника',
    // as long as XTemplate doesn't support output escaping, transform
    // quotes to html-entities right here (bug #48913)
    'LBL_FROM_ADDR_DESC' => "The email address provided here might not appear in the &quot;From&quot; address section of the email sent due to restrictions imposed by the mail service provider. In these circumstances, the email address defined in the outgoing mail server will be used.", // as long as XTemplate doesn't support output escaping, transform quotes to html-entities right here (bug #48913)
    'LBL_FROM_NAME' => 'Від (ім&#039;я)',
    'LBL_GROUP_QUEUE' => 'Приписати до групи',
    'LBL_HOME' => 'Головна',
    'LBL_LIST_MAILBOX_TYPE' => 'Дія',
    'LBL_LIST_NAME' => 'Ім’я:',
    'LBL_LIST_GLOBAL_PERSONAL' => 'Тип',
    'LBL_LIST_SERVER_URL' => 'Поштовий сервер',
    'LBL_LIST_STATUS' => 'Статус',
    'LBL_LOGIN' => 'Логін',
    'LBL_MAILBOX_DEFAULT' => 'ВХІДНІ',
    'LBL_MAILBOX_SSL' => 'Використовувати SSL',
    'LBL_MAILBOX_TYPE' => 'Можливі дії',
    'LBL_DISTRIBUTION_METHOD' => 'Алгоритм призначення відповідального',
    'LBL_CREATE_CASE_REPLY_TEMPLATE' => 'Шаблон автоматичної відповіді на нові Звернення',
    'LBL_CREATE_CASE_REPLY_TEMPLATE_HELP' => 'Автоматично інформувати відправників електронних листів про створення нового Звернення. У темі листа буде міститися номер Звернення, формат якого буде сформовано на основі макросу Звернення. Авто-відповідь буде відправлена тільки при отриманні першого листа від відправника.',
    'LBL_MAILBOX' => 'Папки, що перевіряються',
    'LBL_TRASH_FOLDER' => 'Видалені',
    'LBL_SENT_FOLDER' => 'Відправлені',
    'LBL_SELECT' => 'Вибрати',
    'LBL_MARK_READ_NO' => 'E-mail-повідомлення позначаються як видалені після імпортування',
    'LBL_MARK_READ_YES' => 'E-mail-повідомлення залишаються на сервері при імпорті',
    'LBL_MARK_READ' => 'Залишати повідомлення на сервері',
    'LBL_MAX_AUTO_REPLIES' => 'Обмеження кількості автовідповідей',
    'LBL_MAX_AUTO_REPLIES_DESC' => 'Встановлення максимальної кількості автоматичних відповідей, що надсилаються на унікальний E-mail протягом 24 годин.',
    'LBL_PERSONAL_MODULE_NAME' => 'Персональний обліковий запис E-mail',
    'LBL_CREATE_CASE' => 'Створити Звернення з E-mail',
    'LBL_CREATE_CASE_HELP' => 'Select to automatically create case records in SuiteCRM from incoming emails.',
    'LBL_MODULE_NAME' => 'Груповий обліковий запис E-mail',
    'LBL_BOUNCE_MODULE_NAME' => 'Обліковий запис для обробки E-mail, що повертаються',
    'LBL_MODULE_TITLE' => 'Групові облікові записи',
    'LBL_NAME' => 'Назва',
    'LBL_NONE' => '--не вибрано--',
    'LBL_ONLY_SINCE_NO' => 'Ні. Перевірте відповідні листи на поштовому сервері.',
    'LBL_ONLY_SINCE_YES' => 'Так.',
    'LBL_PASSWORD' => 'Пароль',
    'LBL_POP3_SUCCESS' => 'Тестування POP3-з&#039;єднання пройшло успішно.',
    'LBL_POPUP_TITLE' => 'Тестування налаштувань...',
    'LBL_SELECT_SUBSCRIBED_FOLDERS' => 'Вибрати папку/папки',
    'LBL_SELECT_TRASH_FOLDERS' => 'Вибрати папку "Видалені"',
    'LBL_SELECT_SENT_FOLDERS' => 'Вибрати папку "Надіслані"',
    'LBL_DELETED_FOLDERS_LIST' => 'Наступні папки не існують або були видалені з серверу:% s',
    'LBL_PORT' => 'Порт',
    'LBL_REPLY_TO_NAME' => 'Ім&#039;я для відповіді',
    'LBL_REPLY_TO_ADDR' => 'Адреса для відповіді',
    'LBL_SAME_AS_ABOVE' => 'Використовувати Ім&#039;я/Адресу',
    'LBL_SERVER_OPTIONS' => 'Додаткові опції поштового сервера',
    'LBL_SERVER_TYPE' => 'Протокол поштового сервера',
    'LBL_SERVER_URL' => 'Сервер вхідної пошти',
    'LBL_SSL_DESC' => 'Якщо ваш поштовий сервер підтримує захищене з&#039;єднання, то при імпортуванні E-mail буде використано шифрування SSL.',
    'LBL_ASSIGN_TO_TEAM_DESC' => 'У вибраної команди є доступ до облікового поштового запису.',
    'LBL_SSL' => 'Використовувати шифрування (SSL)',
    'LBL_STATUS' => 'Статус',
    'LBL_SYSTEM_DEFAULT' => 'За замовчуванням',
    'LBL_TEST_BUTTON_TITLE' => 'Тест',
    'LBL_TEST_SETTINGS' => 'Перевірити налаштування',
    'LBL_TEST_SUCCESSFUL' => 'З&#039;єднання успішно встановлено.',
    'LBL_TEST_WAIT_MESSAGE' => 'Будь ласка, зачекайте...',
    'LBL_WARN_IMAP_TITLE' => 'Вихідний E-mail відключений',
    'LBL_WARN_IMAP' => 'Попередження:',
    'LBL_WARN_NO_IMAP' => 'Ця система не має IMAP-клієнту, відкомпільованого в PHP-модулі (--with-imap=/path/to/imap_c-client_library). Будь ласка, зв&#039;яжіться з Вашим адміністратором для вирішення цієї проблеми.',

    'LNK_LIST_CREATE_NEW_GROUP' => 'Новий груповий обліковий запис',
    'LNK_LIST_CREATE_NEW_BOUNCE' => 'Новий обліковий запис для обробки пошти, що повертається',
    'LNK_LIST_MAILBOXES' => 'Усі облікові записи',
    'LNK_LIST_SCHEDULER' => 'Планувальник завдань',
    'LNK_SEED_QUEUES' => 'Нова черга з груп',
    'LBL_GROUPFOLDER_ID' => 'Групова папка (Id)',

    'LBL_ALLOW_OUTBOUND_GROUP_USAGE' => 'Дозволити користувачам відправляти e-mail-повідомлення, використовуючи в якості адреси для відповіді ім&#039;я та e-mail-адресу з поля "Від"',
    'LBL_ALLOW_OUTBOUND_GROUP_USAGE_DESC' => 'При виборі цієї опції, буде можлива опція вибору інформації для полів "Від: Ім&#039;я" та "Від: E-mail-адреса", асоційованої з цим груповим обліковим поштовим записом, у разі якщо e-mail складається користувачем, у якого є доступ до групового облікового поштового запису.',
    'LBL_STATUS_ACTIVE' => 'Активно',
    'LBL_STATUS_INACTIVE' => 'Неактивно',
    'LBL_IS_PERSONAL' => 'Особистий',
    'LBL_IS_GROUP' => 'групова',
    'LBL_ENABLE_AUTO_IMPORT' => 'Автоматично імпортувати E-mail-повідомлення',
    'LBL_WARNING_CHANGING_AUTO_IMPORT' => 'Попередження: Ви змінюєте параметр автоматичного імпорту, що може призвести до втрати даних.',
    'LBL_WARNING_CHANGING_AUTO_IMPORT_WITH_CREATE_CASE' => 'Попередження: При автоматичному створенні Звернень авто імпорт також повинен бути включений.',
    'LBL_LIST_TITLE_MY_DRAFTS' => 'Drafts',
    'LBL_LIST_TITLE_MY_INBOX' => 'Inbox',
    'LBL_LIST_TITLE_MY_SENT' => 'Надіслане повідомлення',
    'LBL_LIST_TITLE_MY_ARCHIVES' => 'Архів повідомлень',
    'LNK_MY_DRAFTS' => 'Drafts',
    'LNK_MY_INBOX' => 'E-mail',
    'LNK_VIEW_MY_INBOX' => 'View Email',
    'LNK_QUICK_REPLY' => 'Відповісти',
    'LNK_SENT_EMAIL_LIST' => 'Відправлені',
	'LBL_EDIT_LAYOUT' => 'Правка розташування' /*for 508 compliance fix*/,

	'LBL_MODIFIED_BY' => 'Змінено',
	'LBL_SERVICE' => 'Service',
	'LBL_STORED_OPTIONS' => 'Stored Options',
	'LBL_GROUP_ID' => 'Group ID',
);
?>
