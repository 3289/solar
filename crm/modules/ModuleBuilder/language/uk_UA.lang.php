<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array(
    'LBL_LOADING' => 'Loading' /*for 508 compliance fix*/,
    'LBL_HIDEOPTIONS' => 'Hide Options' /*for 508 compliance fix*/,
    'LBL_DELETE' => 'Видалити' /*for 508 compliance fix*/,
    'help' => array(
        'package' => array(
            'create' => 'Вкажіть Назву інсталяційного пакета, що складається тільки з букв і цифр, що не містить пробіли (Наприклад: HR_Management)<br /><br />Ви так само можете додати інформацію в розділи Автор і Опис.<br /><br />Натисніть \\"Зберегти\\", щоб зберегти інсталяційний пакет.',
            'modify' => 'Тут відображаються властивості і можливі дії для інсталяційного пакету.<br /><br />Ви можете змінити Назву, Автора і Опис інсталяційного пакета, а так само переглянути і змінити всі модулі, що входять в інсталяційний пакет. Натисніть кнопку \\"Новий модуль\\" щоб створити новий модель для даного пакету.<br /><br />Якщо інсталяційний пакет містить хоча б один модуль, ви можете Опублікувати і Встановити пакет, а також Експортувати зміни, внесені в пакеті інсталяції.',
            'name' => 'Це Назва для даного інсталяційного пакету. Назва може складатися тільки з букв і цифр, не може містити пропусків (Наприклад: HR_Management)',
            'author' => 'Автор, який буде відображатися під час установки пакета.<br /><br />Автором може бути приватна особа або компанія.',
            'description' => 'Опис інсталяційного пакета, яке буде відображатися під час установки.',
            'publishbtn' => 'Натисніть Опублікувати, щоб зберегти всі дані і створити .zip файл який і буде настановної версією пакета.',
            'deploybtn' => 'Натисніть Встановити, щоб зберегти всі дані і встановити пакет, включаючи всі модулі, в даний екземпляр SuiteCRM.',
            'duplicatebtn' => 'Натисніть Дублювати щоб скопіювати вміст інсталяційного пакету у новий пакет і відобразити новий пакет. Назва пакету буде створено автоматично шляхом додавання до числа назвою вже існуючого пакету. Пізніше ви можете змінити цю назву.',
            'exportbtn' => 'Натисніть Експортувати щоб створити .zip файл містить всі зміни, внесені в даний пакет. Створений файл не є настановної версією пакета. Використовуйте Завантажувач модулів щоб імпортувати .zip файл і зробити пакет доступним в Конструкторі модулів.',
            'deletebtn' => 'Натисніть Видалити, щоб видалити пакет і все, що відносяться до нього файли.',
            'savebtn' => 'Натисніть Зберегти, щоб зберегти всі змінені дані, що відносяться до пакету.',
            'existing_module' => 'Натисніть на іконку Модуль редагування властивостей і внесення змін до поля, відносини і макети, що відносяться до даного модуля.',
            'new_module' => 'Натисніть Новий модуль щоб створити новий модуль для цього пакету.',
            'key' => 'Цей 5-ти значний ключ (допускаються букви і цифри) буде використаний в якості префікса до всіх каталогах, імена класів і таблиць бази даних для всіх модулів в даному пакеті. <br /><br />Ключ використовується для уніфікації табличних імен.',
            'readme' => 'Натисніть, щоб додати текст опису до установчого пакету.<br /><br />Опис буде доступно під час установки.',

        ),
        'main' => array(),
        'module' => array(
            'create' => 'Provide a <b>Name</b> for the module. The <b>Label</b> that you provide will appear in the navigation tab. <br/><br/>Choose to display a navigation tab for the module by checking the <b>Navigation Tab</b> checkbox.<br/><br/>Then choose the type of module you would like to create. <br/><br/>Select a template type. Each template contains a specific set of fields, as well as pre-defined layouts, to use as a basis for your module. <br/><br/>Click <b>Save</b> to create the module.',
            'modify' => 'You can change the module properties or customize the <b>Fields</b>, <b>Relationships</b> and <b>Layouts</b> related to the module.',
            'importable' => 'Вибравши опцію \\"Імпортований\\", Ви дозволите імпорт для цього модуля.<br /><br />Посилання на майстер імпорту з&#039;явиться в панелі ярликів модуля. За допомогою майстра імпорту можна імпортувати дані з зовнішніх джерел в модуль.',
            'team_security' => 'Вибравши опцію \\"Командна безпека\\" ви зробите її доступною в цьому модулі.<br /><br />Якщо командна безпека дозволена, у записах модуля з&#039;явиться поле вибору команди.',
            'reportable' => 'Вибір цього параметра дозволить використовувати цей модуль для підготовки звітів.',
            'assignable' => 'Вибір цього параметра дозволить закріплювати запису даного модуля за користувачами SuiteCRM.',
            'has_tab' => 'Опція \\"Навігаційна панель\\" додає таку в модуль.',
            'acl' => 'Вибір цієї опції зробить доступним для цього модуля Керування доступом, включаючи рівень безпеки полів.',
            'studio' => 'Вибір цього параметра дозволить адміністраторам змінювати цей модуль засобами Студії.',
            'audit' => 'Вибір цієї опції зробить доступним відстеження змін. Зміни полів модуля будуть записуватися та адміністратори зможуть переглядати історію змін.',
            'viewfieldsbtn' => 'Натисніть \\"Переглянути поля\\" для перегляду полів, пов&#039;язаних з модулем, а також для створення і редагування нових полів.',
            'viewrelsbtn' => 'Натисніть \\"Переглянути відносини для перегляду відносин, пов&#039;язаних з модулем, а також для створення і редагування нових відносин.',
            'viewlayoutsbtn' => 'Натисніть \\"Переглянути розмітки\\" для перегляду і зміни полів модуля.',
            'duplicatebtn' => 'Click <b>Duplicate</b> to copy the properties of the module into a new module and to display the new module. <br/><br/>For the new module, a new name will be generated automatically by appending a number to the end of the name of the module used to create the new one.',
            'deletebtn' => 'Click <b>Delete</b> to delete this module.',
            'name' => 'This is the <b>Name</b> of the current module.<br/><br/>The name must be alphanumeric and must start with a letter and contain no spaces. (Example: HR_Management)',
            'label' => 'Це Позначка, яка буде відображатися в навігаційній панелі модуля.',
            'savebtn' => 'Click <b>Save</b> to save all entered data related to the module.',
            'type_basic' => 'Шаблон Основний містить у собі основні поля, такі як Ім&#039;я, Відповідальний (- а), Команда, Дата створення та Опис.',
            'type_company' => 'Шаблон \\"Компанія\\" надає такі типи полів як: Назва компанії, Галузь і Юридична адреса.<br /><br />Використовуйте цей шаблон для створення модулів, схожих на стандартний модуль Контрагенти.',
            'type_issue' => 'Шаблон \\"Звернення\\" надає такі типи полів, пов&#039;язані зі зверненнями і помилками, як: Номер, Статус, Пріоритет і Опис.<br /><br />Використовуйте цей шаблон для створення модулів, схожих на стандартні модулі Обігу та Відстеження помилок.',
            'type_person' => 'Шаблон \\"Особисті дані\\" надає такі типи полів як: Звернення, Посада, Назва, Адреса і Телефон.<br /><br />Використовуйте цей шаблон для створення модулів, схожих на стандартні модулі Контакти і Попередні контакти.',
            'type_sale' => 'Шаблон \\"Продажу\\" надає такі типи полів, пов&#039;язані з продажами, як: Джерело попереднього контакту, Стадія продажу, Сума угоди і Ймовірність.<br /><br />Використовуйте цей шаблон для створення модулів, схожих на стандартний модуль Угоди.',
            'type_file' => 'Шаблон <b>Файли</b> надає такі типи полів, пов&#039;язані з документами, як: Ім\'я файлу, Тип документа і Дата публікації.<br><br>Використовуйте цей шаблон для створення модулів, схожих на стандартний модуль Документи.',

        ),
        'dropdowns' => array(
            'default' => 'Тут знаходяться всі випадаючі списки програми.<br /><br />Випадаючі списки можуть використовуватися в будь-якому модулі.<br /><br />Щоб внести зміни в існуючий розкривний список, натисніть на назві списку.<br /><br />Натисніть \\"Додати розкривний список\\" щоб створити новий.',
            'editdropdown' => 'editdropdown<br />Випадаючі списки можуть використовуватися для створення стандартних або додаткових полів з що випадають списками в модулі.<br /><br />Введіть Назву списку.<br /><br />Якщо у Вашій системі встановлено якісь мовні доповнення, ви можете вибрати мову, що використовується.<br /><br />У полі \\"Назва об&#039;єкта\\" введіть назву об&#039;єкта в випадаючому списку. Це ім&#039;я не буде з&#039;являтися в випадаючих списків, видимих користувачам.<br /><br />У полі \\"Відображуване значення\\" введіть значення випадаючого списку, яке буде видимим для користувачів. <br /><br />Після того, як ви ввели назва об&#039;єкта і відображуване значення натисніть \\"Додати\\", щоб додати об ’ єкт у цьому розкривному списку.<br /><br />Щоб змінити порядок елементів у списку, перетягніть об&#039;єкти мишкою на бажані позиції.<br /><br />Щоб змінити відображуване значення об&#039;єкта натисніть на іконку редагування та введіть нове значення. Щоб видалити елемент зі списку, натисніть на іконку видалення.<br /><br />Щоб скасувати зміни у відображуваному значенні, натисніть \\"Скасувати\\". Щоб повернути скасовані зміни, натисніть \\"Відновити\\".<br /><br />Натисніть \\"Зберегти\\", щоб зберегти список, що випадає.',

        ),
        'subPanelEditor' => array(
            'modify' => 'All of the fields that can be displayed in the <b>Subpanel</b> appear here.<br><br>The <b>Default</b> column contains the fields that are displayed in the Subpanel.<br/><br/>The <b>Hidden</b> column contains fields that can be added to the Default column.'
        ,
            'savebtn' => 'Click <b>Save & Deploy</b> to save changes you made and to make them active within the module.',
            'historyBtn' => 'historyBtn<br />Натисніть на Огляд історії для перегляду та відновлення раніше збереженого макета з історії.',
            'historyDefault' => 'historyDefault<br />Натисніть Відновити за замовчуванням для відновлення початкового макета.',
            'Hidden' => 'Приховані<br />Приховані поля не відображаються в субпанели.',
            'Default' => 'За умовчанням<br />Поля за замовчуванням відображаються в субпанели.',

        ),
        'listViewEditor' => array(
            'modify' => 'All of the fields that can be displayed in the <b>ListView</b> appear here.<br><br>The <b>Default</b> column contains the fields that are displayed in the ListView by default.<br/><br/>The <b>Available</b> column contains fields that a user can select in the Search to create a custom ListView. <br/><br/>The <b>Hidden</b> column contains fields that can be added to the Default or Available column.'
        ,
            'savebtn' => 'Click <b>Save & Deploy</b> to save changes you made and to make them active within the module.',
            'historyBtn' => 'Click <b>View History</b> to view and restore a previously saved layout from the history.<br><br><b>Restore</b> within <b>View History</b> restores the field placement within previously saved layouts. To change field labels, click the Edit icon next to each field.',
            'historyDefault' => 'Click <b>Restore Default</b> to restore a view to its original layout.<br><br><b>Restore Default</b> only restores the field placement within the original layout. To change field labels, click the Edit icon next to each field.',
            'Hidden' => '<b>Hidden</b> fields not currently available for users to see in ListViews.',
            'Available' => '<b>Available</b> fields are not shown by default, but can be added to ListViews by users.',
            'Default' => '<b>Default</b> fields appear in ListViews that are not customized by users.'
        ),
        'popupListViewEditor' => array(
            'modify' => 'All of the fields that can be displayed in the <b>ListView</b> appear here.<br><br>The <b>Default</b> column contains the fields that are displayed in the ListView by default.<br/><br/>The <b>Hidden</b> column contains fields that can be added to the Default or Available column.'
        ,
            'savebtn' => 'Click <b>Save & Deploy</b> to save changes you made and to make them active within the module.',
            'historyBtn' => 'Click <b>View History</b> to view and restore a previously saved layout from the history.<br><br><b>Restore</b> within <b>View History</b> restores the field placement within previously saved layouts. To change field labels, click the Edit icon next to each field.',
            'historyDefault' => 'Click <b>Restore Default</b> to restore a view to its original layout.<br><br><b>Restore Default</b> only restores the field placement within the original layout. To change field labels, click the Edit icon next to each field.',
            'Hidden' => '<b>Hidden</b> fields not currently available for users to see in ListViews.',
            'Default' => '<b>Default</b> fields appear in ListViews that are not customized by users.'
        ),
        'searchViewEditor' => array(
            'modify' => 'All of the fields that can be displayed in the <b>Filter</b> form appear here.<br><br>The <b>Default</b> column contains the fields that will be displayed in the Search form.<br/><br/>The <b>Hidden</b> column contains fields available for you as an admin to add to the Search form.'
        ,
            'savebtn' => 'Clicking <b>Save & Deploy</b> will save all changes and make them active',
            'Hidden' => '<b>Hidden</b> fields do not appear in the Search.',
            'historyBtn' => 'Click <b>View History</b> to view and restore a previously saved layout from the history.<br><br><b>Restore</b> within <b>View History</b> restores the field placement within previously saved layouts. To change field labels, click the Edit icon next to each field.',
            'historyDefault' => 'Click <b>Restore Default</b> to restore a view to its original layout.<br><br><b>Restore Default</b> only restores the field placement within the original layout. To change field labels, click the Edit icon next to each field.',
            'Default' => '<b>Default</b> fields appear in the Search.'
        ),
        'layoutEditor' => array(
            'defaultdetailview' => 'У розділі Макет містяться поля, які в даний момент відображаються в макеті \\"Додаткова інформація\\". <br /><br />Інструментарій містить Кошик, а так само поля і елементи макета, які можуть бути додані до цього макету.<br /><br />Зміни в макет можна вносити, перетягуючи поля і елементи між інструментарієм і Макетом, а так само всередині Макета.<br /><br />Щоб прибрати поле з Макета, перетягніть його в Кошик. Після цього поле стане доступним для додавання з Інструментарію.<br /><br / > * Позначає залежне підлозі яке може бути видимим в залежності від значення формули<br /><br / > * Позначає обчислюване поле, значення якого автоматично визначається згідно з формулою.',
        'defaultquickcreate' => 'У розділі Макет містяться поля, які в даний момент відображаються у формі QuickCreate.<br /><br />Форма QuickCreate з&#039;являється в субпанелях модуля після натискання кнопки Створити.<br /><br />Інструментарій містить Кошик, а так само поля і елементи макета, які можуть бути додані до цього макету.<br /><br />Зміни в макет можна вносити, перетягуючи поля і елементи між інструментарієм і Макетом, а так само всередині Макета.<br /><br />Щоб прибрати поле з Макета, перетягніть його в Кошик. Після цього поле стане доступним для додавання з Інструментарію.<br /><br / > * Позначає залежне підлозі яке може бути видимим в залежності від значення формули<br /><br / > * Позначає обчислюване поле, значення якого автоматично визначається згідно з формулою.',
            //this default will be used for edit view
            'default' => 'The <b>Layout</b> area contains the fields that are currently displayed within the <b>EditView</b>.<br/><br/>The <b>Toolbox</b> contains the <b>Recycle Bin</b> and the fields and layout elements that can be added to the layout.<br><br>Make changes to the layout by dragging and dropping elements and fields between the <b>Toolbox</b> and the <b>Layout</b> and within the layout itself.<br><br>To remove a field from the layout, drag the field to the <b>Recycle Bin</b>. The field will then be available in the Toolbox to add to the layout.',
            'saveBtn' => 'saveBtn<br />Натисніть \\"Зберегти\\" для збереження змін, внесених в макет після останнього збереження.<br /><br />Зміни не будуть відображені в модулі до того, як Ви Застосуєте збережені зміни.',
            'historyBtn' => 'Click <b>View History</b> to view and restore a previously saved layout from the history.<br><br><b>Restore</b> within <b>View History</b> restores the field placement within previously saved layouts. To change field labels, click the Edit icon next to each field.',
            'historyDefault' => 'Click <b>Restore Default</b> to restore a view to its original layout.<br><br><b>Restore Default</b> only restores the field placement within the original layout. To change field labels, click the Edit icon next to each field.',
            'publishBtn' => 'publishBtn<br />Натисніть \\"Зберегти та застосувати\\" для збереження змін, внесений в макет після останнього збереження, і для активації змін у модулі.<br /><br />Макет відразу ж буде відображений в модулі.',
            'toolbox' => 'Інструментарій містить Кошик, а так само поля і елементи макета, які можуть бути додані до цього макету.<br /><br />Зміни в макет можна вносити, перетягуючи поля і елементи між інструментарієм і Макетом, а так само всередині Макета.<br /><br />Елементи макету - це Панелі та Рядка. Додавання нової панелі або рядки в макет дає додаткові можливості для організації макета.<br /><br />Перетягуйте будь-які поля в інструментарії і макеті на позиції, які в даний момент зайняті, щоб змінювати об&#039;єкти місцями.<br /><br />Поле Фільтр створює порожній проміжок між об&#039;єктами в макеті.<br /><br / > * Позначає залежне підлозі яке може бути видимим в залежності від значення формули.<br /><br / > * Позначає обчислюване поле, значення якого автоматично визначається згідно з формулою.',
            'panels' => 'панелі<br />В області Розміток можна перевірити, як макет буде відображатися в модулі, коли зміни макета будуть застосовані.<br /><br />Ви можете перемістити поля, ряди і панелі перетягуючи їх, до потрібного розташування.<br /><br />Видалити елементи можна перетягнувши їх в Корзину в Інструментарії, або додати нові елементи і поля можна перетягнути їх з Інструментарію в необхідне розташування на макеті.',
            'delete' => 'видалити <br />Перетягніть і відпустіть будь-який елемент сюди, щоб видалити його з макета',
            'property' => 'Редагувати мітку, яка відображається для даного поля.<br /><br />Порядок перемикання визначає, в якому порядку відбувається перемикання між полями натисканні клавіші Tab.',
        ),
        'fieldsEditor' => array(
            'default' => 'The <b>Fields</b> that are available for the module are listed here by Field Name.<br><br>Custom fields created for the module appear above the fields that are available for the module by default.<br><br>To edit a field, click the <b>Field Name</b>.<br/><br/>To create a new field, click <b>Add Field</b>.',
            'mbDefault' => 'Тут знаходяться Поля, доступні в модулі. Поля відсортовані по параметру Ім&#039;я поля.<br /><br />Щоб настроїти властивості поля, натисніть на Ім&#039;я поля.<br /><br />Щоб створити нове поле, натисніть Додати поле. Після того, як поле створено, позначка сфери та інші властивості можуть бути відредаговані після натискання на Ім&#039;я поля.<br /><br />Після установки модуля нові поля, створені в Конструкторі модулів будуть сприйматися Студією як стандартні поля.',
            'addField' => '<html><body><h1>400 Bad request</h1>
Your browser sent an invalid request.
</body></html>
',
            'editField' => 'editField<br />Властивості даного поля, які можуть бути змінені.<br /><br />Натисніть Клонувати для створення нового поля з такими ж властивостями.',
            'mbeditField' => 'mbeditField<br />Позначка відображення поля шаблону може бути змінена. Інші властивості поля не можуть бути змінені.<br /><br />Натисніть Клонувати для створення нового поля з такими ж властивостями.<br /><br />Щоб полі шаблон було видалено і не відображалося в модулі, видаліть поле з відповідних Розміток.'

        ),
        'exportcustom' => array(
            'exportHelp' => 'Export customizations made in Studio by creating packages that can be uploaded into another SuiteCRM instance through the <b>Module Loader</b>.<br><br>  First, provide a <b>Package Name</b>. You can provide <b>Author</b> and <b>Description</b> information for package as well.<br><br>Select the module(s) that contain the customizations you wish to export. Only modules containing customizations will appear for you to select.<br><br>Then click <b>Export</b> to create a .zip file for the package containing the customizations.',
            'exportCustomBtn' => 'Натисніть Експорт, щоб створити *.zip-файл, що містить пакет з змінами, які Ви хотіли б експортувати.',
            'name' => 'This is the <b>Name</b> of the package. This name will be displayed during installation.',
            'author' => 'This is the <b>Author</b> that is displayed during installation as the name of the entity that created the package. The Author can be either an individual or a company.',
            'description' => 'Опис інсталяційного пакета, яке буде відображатися під час установки.',
        ),
        'studioWizard' => array(
            'mainHelp' => 'Ласкаво просимо у розділ Інструментарій розробника.<br /><br />Використовуйте інструменти даного розділу для створення і управління стандартними і користувацькими модулями і полями.',
            'studioBtn' => 'studioBtn<br />Використовуйте Студію для зміни застосованих модулів',
            'mbBtn' => 'mbBtn<br />Використовуйте Конструктор Модулів для створення нових модулів.',
            'sugarPortalBtn' => 'Use <b>SuiteCRM Portal Editor</b> to manage and customize the SuiteCRM Portal.',
            'dropDownEditorBtn' => 'dropDownEditorBtnс<br />Використовуйте Редактор випадаючих списків для редагування полів у загальних випадаючих списках',
            'appBtn' => 'У режимі Програми Ви можете змінювати різні властивості програми, наприклад яка кількість TPS-звітів буде відображатися на головній сторінці.',
            'backBtn' => 'backBtn<br />Повернутися до попереднього кроку.',
            'studioHelp' => 'studioHelp<br />Використовуйте Студію, щоб визначити, яка інформація і як відображається в модулях.',
            'moduleBtn' => 'moduleBtn<br />Натисніть для редагування цього модуля.',
            'moduleHelp' => 'Тут відображаються компоненти даного модуля, які можуть бути змінені.<br /><br />Клацніть на іконці, щоб вибрати компонент для редагування.',
            'fieldsBtn' => 'fieldsBtn<br />Створюйте та змінюйте Поля для збереження інформації в модулі.',
            'labelsBtn' => 'Редагувати Мітки, що показуватиметься у імена полів та інших назвах в модулі.',
            'relationshipsBtn' => 'relationshipsBtn<br />Додайте нові Відносини для модуля або перегляньте вже існуючі',
            'layoutsBtn' => 'layoutsBtn<br />Змініть макета модуля. Макети - різні види модуля, що містять поля.<br /><br />Ви можете вказати, які поля повинні відображатися, і як вони повинні бути організовані в кожному макеті.',
            'subpanelBtn' => 'subpanelBtn<br />Виберіть поля повинні відображатися в Субпанелях модуля.',
            'portalBtn' => 'Customize the module <b>Layouts</b> that appear in the <b>SuiteCRM Portal</b>.',
            'layoutsHelp' => 'layoutsHelp<br />Макети модуля, які можуть бути змінені, відображені тут.<br /><br />Макети відображають поля і дані по полях.<br /><br />Натисніть на іконці, щоб вибрати макет для зміни.',
            'subpanelHelp' => 'subpanelHelp<br /><br />Субпанели модуля, які можуть бути змінені, відображені тут.<br /><br />Натисніть на іконці, щоб вибрати модуль для зміни.',
            'newPackage' => 'newPackage<br /><br />Натисніть Новий пакет для створення нового пакета.',
            'exportBtn' => 'exportBtn<br />Натисніть Експорт Змін, щоб створити і завантажити пакет із змінами, зробленими в Студії з конкретними модулями.',
            'mbHelp' => 'Використовуйте Конструктор модулів для створення пакетів, що містять спеціальні модулі, засновані на стандартних або користувача об&#039;єктах.',
            'viewBtnEditView' => 'Змінити макет режиму Редагування даного модуля.<br /><br />Режим Редагування - це форма, що містить поля для введення даних користувачем.',
            'viewBtnDetailView' => 'Змінити макет режиму Детальна інформація даного модуля.<br /><br />Режим Детальна інформація відображає поля даних, введених користувачем.',
            'viewBtnDashlet' => 'Customize the module\'s <b>SuiteCRM Dashlet</b>, including the SuiteCRM Dashlet\'s ListView and Search.<br><br>The SuiteCRM Dashlet will be available to add to the pages in the Home module.',
            'viewBtnListView' => 'Змінити макет списку записів даного модуля.<br /><br />У вигляді такого списку відображаються результати пошуку.',
            'searchBtn' => 'Змінити макет Пошук даного модуля.<br /><br />Тут можна визначити, які поля будуть використані в якості фільтрів списку записів.',
            'viewBtnQuickCreate' => 'Змінити макет Швидке створення даного модуля.<br /><br />Форма Швидкого створення з&#039;являється в субпанелях модулів і модулі Email.',
            'addLayoutHelp' => "To create a custom layout for a Security Group select the appropriate Security Group and the layout from which to copy from as a starting point.",
            'searchHelp' => 'The <b>Search</b> forms that can be customized appear here.<br><br>Search forms contain fields for filtering records.<br><br>Click an icon to select the search layout to edit.',
            'dashletHelp' => 'The <b>SuiteCRM Dashlet</b> layouts that can be customized appear here.<br><br>The SuiteCRM Dashlet will be available to add to the pages in the Home module.',
            'DashletListViewBtn' => 'The <b>SuiteCRM Dashlet ListView</b> displays records based on the SuiteCRM Dashlet search filters.',
            'DashletSearchViewBtn' => 'The <b>SuiteCRM Dashlet Search</b> filters records for the SuiteCRM Dashlet listview.',
            'popupHelp' => 'Тут відображаються макети спливаючих вікон, доступні для редагування.',
            'PopupListViewBtn' => 'Спливаючий список відображає запису ґрунтуючись на переглядах через спливаючий пошук.',
            'PopupSearchViewBtn' => 'Записи переглядів контекстного пошуку для спливаючого списку.',
            'BasicSearchBtn' => 'Customize the <b>Quick Filter</b> form that appears in the Quick Filter tab in the Search area for the module.',
            'AdvancedSearchBtn' => 'Customize the <b>Advanced Filter</b> form that appears in the Advanced Search tab in the Search area for the module.',
            'portalHelp' => 'Manage and customize the <b>SuiteCRM Portal</b>.',
            'SPUploadCSS' => 'Upload a <b>Style Sheet</b> for the SuiteCRM Portal.',
            'SPSync' => '<b>Sync</b> customizations to the SuiteCRM Portal instance.',
            'Layouts' => 'Customize the <b>Layouts</b> of the SuiteCRM Portal modules.',
            'portalLayoutHelp' => 'The modules within the SuiteCRM Portal appear in this area.<br><br>Select a module to edit the <b>Layouts</b>.',
            'relationshipsHelp' => 'relationshipsHelp<br />Всі Відносини, які існують між пристроєм та іншими застосованими модулями, показані тут.<br /><br />Параметр Ім&#039;я відносини генерується системою автоматично.<br /><br />Головний модуль - це той, якому належить ставлення. Наприклад, всі властивості відносин, для яких модуль Контрагенти є головним, зберігаються в базі даних цього модуля.<br /><br />Параметр Тип - це тип відносин, які існують між головним модулем і пов&#039;язаним з ним модулем. <br /><br />Натисніть на назві колонки, щоб включити відповідний вид сортування.<br /><br />Натисніть на рядок у таблиці відносин, щоб переглянути властивості відповідного ставлення.<br /><br />Натисніть \\"Додати відношення\\" щоб створити нове ставлення.<br /><br />Відносини можуть бути створені між двома встановленими у системі модулями.',
            'relationshipHelp' => 'Відносини можуть бути встановлені між пристроєм та іншим пристроєм, встановленими у системі.<br /><br /><br /><br />Відносини візуально виражаються через субпанели і поля відносин у записах модуля.<br /><br /><br /><br />Виберіть один з наступних типів відносин:<br /><br /><br /><br />Один до Одного - Запису обох модулів будуть містити пов&#039;язані поля.<br /><br /><br /><br />Один-до-Багатьом - Запис головного модуля буде містити субпанель, запису пов&#039;язаних модулів будуть містити пов&#039;язані поля.<br /><br /><br /><br />Багато-до-Багатьом - Запису обох модулів будуть відображати субпанель.<br /><br /><br /><br />Виберіть пов&#039;язаний модуль для даних відносин.<br /><br /><br /><br />Якщо тип відносин включає в себе субпанели, виберіть вигляд субпанели для відповідних модулів.<br /><br /><br /><br />Натисніть \\"Зберегти\\" щоб створити ставлення.',
            'convertLeadHelp' => 'Тут можна додавати модулі в розділ перетворення макета і змінювати макети існуючих модулів. Ви можете змінювати порядок модулів шляхом перетягування рядків у таблиці.<br /><br />Модуль: Ім&#039; я модуля.<br /><br />Необхідні: Необхідні модулі повинні бути створені або обрані перед конвертацією попереднього контакту.<br /><br />Скопіювати дані: Якщо опція обрана, поля попереднього контакту будуть скопійовані в однойменні поля створюваних записів.<br /><br />Увімкнути виділення: Модулі з пов&#039; язаними полями в Контактах можуть бути обрані (замість створення нових) під час перетворення попереднього контакту.<br /><br />Редагувати: Змінити макет перетворення для даного модуля.<br /><br />Видалити: Видалити цей модуль з макета перетворення',


            'editDropDownBtn' => 'editDropDownBtn <br />редагувати глобальний Розкривний список',
            'addDropDownBtn' => 'addDropDownBtn<br />Додати новий глобальний Розкривний список',
        ),
        'fieldsHelp' => array(
            'default' => 'The <b>Fields</b> in the module are listed here by Field Name.<br><br>The module template includes a pre-determined set of fields.<br><br>To create a new field, click <b>Add Field</b>.<br><br>To edit a field, click the <b>Field Name</b>.<br/><br/>After the module is deployed, the new fields created in Module Builder, along with the template fields, are regarded as standard fields in Studio.',
        ),
        'relationshipsHelp' => array(
            'default' => 'The <b>Relationships</b> that have been created between the module and other modules appear here.<br><br>The relationship <b>Name</b> is the system-generated name for the relationship.<br><br>The <b>Primary Module</b> is the module that owns the relationships. The relationship properties are stored in the database tables belonging to the primary module.<br><br>The <b>Type</b> is the type of relationship exists between the Primary module and the <b>Related Module</b>.<br><br>Click a column title to sort by the column.<br><br>Click a row in the relationship table to view and edit the properties associated with the relationship.<br><br>Click <b>Add Relationship</b> to create a new relationship.',
            'addrelbtn' => 'миша над довідкою щоб додати відношення..',
            'addRelationship' => 'Відносини можуть створюватися між даними модулем та іншим користувача чи встановленим модулем.<br /><br />Візуально відносини виражаються через субпанели і пов&#039;язані поля записів модуля.<br /><br />Виберіть один з наступних типів відносин для даного модуля:<br /><br />Один до одного - запису обох модулів будуть містити пов&#039;язані поля.<br /><br />Один-до-багатьом - Запис Головного модуля буде містити субпанель, а запис Залежного модуля буде містити пов&#039;язане полі.<br /><br />Багато-до-багатьом - у записах обох модулів буде відображатися субпанель.<br /><br />Виберіть залежний модуль для даних відносин.<br /><br />Якщо тип відносин зачіпає субпанели, виберіть вигляд субпанели для відповідного модуля.<br /><br />Натисніть Зберегти щоб створити ставлення.',
        ),
        'labelsHelp' => array(
            'default' => 'The <b>Labels</b> for the fields and other titles in the module can be changed.<br><br>Edit the label by clicking within the field, entering a new label and clicking <b>Save</b>.<br><br>If any language packs are installed in the application, you can select the <b>Language</b> to use for the labels.',
            'saveBtn' => 'Click <b>Save</b> to save all changes.',
            'publishBtn' => 'Click <b>Save & Deploy</b> to save all changes and make them active.',
        ),
        'portalSync' => array(
            'default' => 'Enter the <b>SuiteCRM Portal URL</b> of the portal instance to update, and click <b>Go</b>.<br><br>Then enter a valid SuiteCRM user name and password, and then click <b>Begin Sync</b>.<br><br>The customizations made to the SuiteCRM Portal <b>Layouts</b>, along with the <b>Style Sheet</b> if one was uploaded, will be transferred to specified the portal instance.',
        ),
        'portalStyle' => array(
            'default' => 'You can customize the look of the SuiteCRM Portal by using a style sheet.<br><br>Select a <b>Style Sheet</b> to upload.<br><br>The style sheet will be implemented in the SuiteCRM Portal the next time a sync is performed.',
        ),
    ),

    'assistantHelp' => array(
        'package' => array(
            //custom begin
            'nopackages' => 'Щоб почати проект, натисніть Наступний пакет, який буде містити ваші власні модулі.<br /><br />Кожен пакет може містити в собі один або більше модулів.<br /><br />Наприклад, Ви можете створити пакет, який містить один користувальницький модуль, пов&#039;язаний зі стандартним модулем Контрагенти. Або пакет, який містить кілька нових модулів, які працюють разом і пов&#039;язаних один з одним і з іншими модулями в системі.',
            'somepackages' => 'Пакет виступає контейнером для користувальницьких модулів, які є частиною одного проекту. Пакет може містити один або більше користувальницьких модулів, які можуть бути пов&#039;язані один з одним або з іншими модулями. <br /><br />Після створення пакета для проекту, ви можете відразу ж створювати модулі для цього пакета або повернутися в Конструктор модулів пізніше, щоб закінчити проект.<br /><br />Коли проект завершений, Ви можете встановити пакет з користувацькими модулями в додаток.',
            'afterSave' => 'Your new package should contain at least one module. You can create one or more custom modules for the package.<br/><br/>Click <b>New Module</b> to create a custom module for this package.<br/><br/> After creating at least one module, you can publish or deploy the package to make it available for your instance and/or other users\' instances.<br/><br/> To deploy the package in one step within your SuiteCRM instance, click <b>Deploy</b>.<br><br>Click <b>Publish</b> to save the package as a .zip file. After the .zip file is saved to your system, use the <b>Module Loader</b> to upload and install the package within your SuiteCRM instance. <br/><br/>You can distribute the file to other users to upload and install within their own SuiteCRM instances.',
            'create' => 'A <b>package</b> acts as a container for custom modules, all of which are part of one project. The package can contain one or more custom <b>modules</b> that can be related to each other or to other modules in the application.<br/><br/>After creating a package for your project, you can create modules for the package right away, or you can return to the Module Builder at a later time to complete the project.',
        ),
        'main' => array(
            'welcome' => 'Використовуйте Інструменти розробника\\" щоб створювати і управляти стандартними і додатковими модулями і полями.<br /><br />Для керування модулями в додатку натисніть \\"Студія\\".<br /><br />Для створення додаткових модулів натисніть \\"Конструктор модулів\\".',
            'studioWelcome' => 'Всі модулі, встановлені в даний момент, включаючи стандартні і додаткові модулі, доступні для редагування засобами Студії.'
        ),
        'module' => array(
            'somemodules' => "Since the current package contains at least one module, you can <b>Deploy</b> the modules in the package within your SuiteCRM instance or <b>Publish</b> the package to be installed in the current SuiteCRM instance or another instance using the <b>Module Loader</b>.<br/><br/>To install the package directly within your SuiteCRM instance, click <b>Deploy</b>.<br><br>To create a .zip file for the package that can be loaded and installed within the current SuiteCRM instance and other instances using the <b>Module Loader</b>, click <b>Publish</b>.<br/><br/> You can build the modules for this package in stages, and publish or deploy when you are ready to do so. <br/><br/>After publishing or deploying a package, you can make changes to the package properties and customize the modules further. Then re-publish or re-deploy the package to apply the changes.",
            'editView' => 'Тут Ви можете редагувати існуючі пакети. Ви можете видаляти існуючі поля або додавати доступні поля в лівій панелі.',
            'create' => 'When choosing the type of <b>Type</b> of module that you wish to create, keep in mind the types of fields you would like to have within the module. <br/><br/>Each module template contains a set of fields pertaining to the type of module described by the title.<br/><br/><b>Basic</b> - Provides basic fields that appear in standard modules, such as the Name, Assigned to, Team, Date Created and Description fields.<br/><br/> <b>Company</b> - Provides organization-specific fields, such as Company Name, Industry and Billing Address. Use this template to create modules that are similar to the standard Accounts module.<br/><br/> <b>Person</b> - Provides individual-specific fields, such as Salutation, Title, Name, Address and Phone Number. Use this template to create modules that are similar to the standard Contacts and Leads modules.<br/><br/><b>Issue</b> - Provides case- and bug-specific fields, such as Number, Status, Priority and Description. Use this template to create modules that are similar to the standard Cases and Bugs modules.<br/><br/>Note: After you create the module, you can edit the labels of the fields provided by the template, as well as create custom fields to add to the module layouts.',
            'afterSave' => 'Customize the module to suit your needs by editing and creating fields, establishing relationships with other modules and arranging the fields within the layouts.<br/><br/>To view the template fields and manage custom fields within the module, click <b>View Fields</b>.<br/><br/>To create and manage relationships between the module and other modules, whether modules already in the application or other custom modules within the same package, click <b>View Relationships</b>.<br/><br/>To edit the module layouts, click <b>View Layouts</b>. You can change the Detail View, Edit View and List View layouts for the module just as you would for modules already in the application within Studio.<br/><br/> To create a module with the same properties as the current module, click <b>Duplicate</b>. You can further customize the new module.',
            'viewfields' => 'Поля в модулі можуть бути змінені у відповідності з Вашими потребами. <br /><br />Ви не можете видаляти стандартні поля, але можете приховувати їх відображення у відповідних макетах.<br /><br />Ви можете швидко створювати копії полів, натиснувши на Клонувати у властивостях форми. Після чого потрібно ввести нові властивості і натиснути кнопку Зберегти.<br /><br />Рекомендується поставити всі властивості стандартних і користувальницьких полів до публікації або установки пакета, яке містить користувальницький модуль.',
            'viewrelationships' => 'Ви можете створювати відносини Багато-до-багатьом між даними модулем та іншими модулями в пакеті і/або між даними модулем і модулями вже встановленими у системі.<br /><br />Щоб створити ставлення Один до-багатьом і Один до одного, створіть Пов&#039;язане поле та Поле з гнучкою зв&#039;язком для відповідних модулів.',
            'viewlayouts' => 'Ви можете визначати, які поля будуть доступні для введення даних в режимі Редагування. Ви також можете визначати, які дані будуть відображатися в режимі Детальна інформація. Ці два режими можуть не збігатися. <br /><br />Форма Швидкого створення запису відображається по натисканню клавіші Створити на субпанели модуля. За замовчуванням, макет форми Швидкого створення ідентичний макету Виправлення. Ви можете змінювати макет форми Швидкого створення прибираючи/додаючи/змінюючи поля в ньому.<br /><br />Ви можете визначати рівень безпеки модуля використовуючи Налаштування макетів і Керування ролями.',
            'existingModule' => 'Після створення і внесення змін у модуль, Ви можете створювати інші модулі або повернутися в пакет, щоб Опублікувати або Встановити його.<br /><br />Для створення додаткових модулів, натисніть Дублювати - створиться модуль з ідентичними властивостями - або поверніться в пакет і натисніть Новий модуль.<br /><br />Якщо ви готові Опублікувати або Встановити пакунок містить модулі, поверніться в пакет і виконайте ці дії. Можна публікувати або встановлювати пакунки, що містять як мінімум один модуль.',
            'labels' => 'Мітки стандартних полів і користувальницьких полів можуть бути змінені. Редагування позначок полів не вплине на дані, які містяться в цих полях.',
        ),
        'listViewEditor' => array(
            'modify' => 'There are three columns displayed to the left. The "Default" column contains the fields that are displayed in a list view by default, the "Available" column contains fields that a user can choose to use for creating a custom list view, and the "Hidden" column contains fields available for you as an admin to either add to the default or Available columns for use by users but are currently disabled.',
            'savebtn' => 'Clicking <b>Save</b> will save all changes and make them active.',
            'Hidden' => 'Hidden fields are fields that are not currently available to users for use in list views.',
            'Available' => 'Available fields are fields that are not shown by default, but can be enabled by users.',
            'Default' => 'Default fields are displayed to users who have not created custom list view settings.'
        ),

        'searchViewEditor' => array(
            'modify' => 'There are two columns displayed to the left. The "Default" column contains the fields that will be displayed in the search view, and the "Hidden" column contains fields available for you as an admin to add to the view.',
            'savebtn' => 'Clicking <b>Save & Deploy</b> will save all changes and make them active.',
            'Hidden' => 'Hidden fields are fields that will not be shown in the search view.',
            'Default' => 'Default fields will be shown in the search view.'
        ),
        'layoutEditor' => array(
            'default' => 'There are two columns displayed to the left. The right-hand column, labeled Current Layout or Layout Preview, is where you change the module layout. The left-hand column, entitled Toolbox, contains useful elements and tools for use when editing the layout. <br/><br/>If the layout area is titled Current Layout then you are working on a copy of the layout currently used by the module for display.<br/><br/>If it is titled Layout Preview then you are working on a copy created earlier by a click on the Save button, that might have already been changed from the version seen by users of this module.',
            'saveBtn' => 'Clicking this button saves the layout so that you can preserve your changes. When you return to this module you will start from this changed layout. Your layout however will not be seen by users of the module until you click the Save and Publish button.',
            'publishBtn' => 'Click this button to deploy the layout. This means that this layout will immediately be seen by users of this module.',
            'toolbox' => 'The toolbox contains a variety of useful features for editing layouts, including a trash area, a set of additional elements and a set of available fields. Any of these can be dragged and dropped onto the layout.',
            'panels' => 'This area shows how your layout will look to users of this module when it is depolyed.<br/><br/>You can reposition elements such as fields, rows and panels by dragging and dropping them; delete elements by dragging and dropping them on the trash area in the toolbox, or add new elements by dragging them from the toolbox and dropping them on to the layout in the desired position.'
        ),
        'dropdownEditor' => array(
            'default' => 'There are two columns displayed to the left. The right-hand column, labeled Current Layout or Layout Preview, is where you change the module layout. The left-hand column, entitled Toolbox, contains useful elements and tools for use when editing the layout. <br/><br/>If the layout area is titled Current Layout then you are working on a copy of the layout currently used by the module for display.<br/><br/>If it is titled Layout Preview then you are working on a copy created earlier by a click on the Save button, that might have already been changed from the version seen by users of this module.',
            'dropdownaddbtn' => 'Ця кнопка додає новий об&#039;єкт у цьому розкривному списку.',

        ),
        'exportcustom' => array(
            'exportHelp' => 'Customizations made in Studio within this instance can be packaged and deployed in another instance. <br><br>Provide a <b>Package Name</b>. You can provide <b>Author</b> and <b>Description</b> information for package.<br><br>Select the module(s) that contain the customizations to export. (Only modules containing customizations will appear for you to select.)<br><br>Click <b>Export</b> to create a .zip file for the package containing the customizations. The .zip file can be uploaded in another instance through <b>Module Loader</b>.',
            'exportCustomBtn' => 'Натисніть Експорт, щоб створити *.zip-файл, що містить пакет з змінами, які Ви хотіли б експортувати.',
            'name' => 'The <b>Name</b> of the package will be displayed in Module Loader after the package is uploaded for installation in Studio.',
            'author' => 'The <b>Author</b> is the name of the entity that created the package. The Author can be either an individual or a company.<br><br>The Author will be displayed in Module Loader after the package is uploaded for installation in Studio.',
            'description' => 'The <b>Description</b> of the package will be displayed in Module Loader after the package is uploaded for installation in Studio.',
        ),
        'studioWizard' => array(
            'mainHelp' => 'Welcome to the <b>Developer Tools</b1> area. <br/><br/>Use the tools within this area to create and manage standard and custom modules and fields.',
            'studioBtn' => 'Use <b>Studio</b> to customize installed modules by changing the field arrangement, selecting what fields are available and creating custom data fields.',
            'mbBtn' => 'mbBtn<br />Використовуйте Конструктор Модулів для створення нових модулів.',
            'appBtn' => 'Use Application mode to customize various properties of the program, such as how many TPS reports are displayed on the homepage',
            'backBtn' => 'backBtn<br />Повернутися до попереднього кроку.',
            'studioHelp' => 'Use <b>Studio</b> to customize installed modules.',
            'moduleBtn' => 'moduleBtn<br />Натисніть для редагування цього модуля.',
            'moduleHelp' => 'Select the module component that you would like to edit',
            'fieldsBtn' => 'Edit what information is stored in the module by controlling the <b>Fields</b> in the module.<br/><br/>You can edit and create custom fields here.',
            'labelsBtn' => 'Click <b>Save</b> to save your custom labels.',
            'layoutsBtn' => 'Customize the <b>Layouts</b> of the Edit, Detail, List and search views.',
            'subpanelBtn' => 'Edit what information is shown in this modules subpanels.',
            'layoutsHelp' => 'Select a <b>Layout to edit</b>.<br/><br/>To change the layout that contains data fields for entering data, click <b>Edit View</b>.<br/><br/>To change the layout that displays the data entered into the fields in the Edit View, click <b>Detail View</b>.<br/><br/>To change the columns which appear in the default list, click <b>List View</b>.<br/><br/>To change the Basic and Advanced search form layouts, click <b>Search</b>.',
            'subpanelHelp' => 'Select a <b>Subpanel</b> to edit.',
            'searchHelp' => 'Select a <b>Search</b> layout to edit.',
            'newPackage' => 'newPackage<br /><br />Натисніть Новий пакет для створення нового пакета.',
            'mbHelp' => '<b>Welcome to Module Builder.</b><br/><br/>Use <b>Module Builder</b> to create packages containing custom modules based on standard or custom objects. <br/><br/>To begin, click <b>New Package</b> to create a new package, or select a package to edit.<br/><br/> A <b>package</b> acts as a container for custom modules, all of which are part of one project. The package can contain one or more custom modules that can be related to each other or to modules in the application. <br/><br/>Examples: You might want to create a package containing one custom module that is related to the standard Accounts module. Or, you might want to create a package containing several new modules that work together as a project and that are related to each other and to modules in the application.',
            'exportBtn' => 'Click <b>Export Customizations</b> to create a package containing customizations made in Studio for specific modules.',
        ),



    ),
//HOME
    'LBL_HOME_EDIT_DROPDOWNS' => 'Редактор випадаючих списків',

//STUDIO2
    'LBL_MODULEBUILDER' => 'Конструктор модулів',
    'LBL_STUDIO' => 'Студія',
    'LBL_DROPDOWNEDITOR' => 'Редактор випадаючих списків',
    'LBL_DEVELOPER_TOOLS' => 'Інструменти розробника',
    'LBL_SUGARPORTAL' => 'SuiteCRM Portal Editor',
    'LBL_PACKAGE_LIST' => 'Список пакетів',
    'LBL_HOME' => 'Головна',
    'LBL_NONE' => 'Порожньо',
    'LBL_DEPLOYE_COMPLETE' => 'Установку завершено',
    'LBL_DEPLOY_FAILED' => 'В процесі установки відбулася помилка, пакет не може бути встановлений коректно',
    'LBL_AVAILABLE_SUBPANELS' => 'Доступні субпанели',
    'LBL_ADVANCED' => 'Розширені',
    'LBL_ADVANCED_SEARCH' => 'Advanced Filter',
    'LBL_BASIC' => 'Базові',
    'LBL_BASIC_SEARCH' => 'Quick Filter',
    'LBL_CURRENT_LAYOUT' => 'Макет',
    'LBL_CURRENCY' => 'Валюта',
    'LBL_DASHLET' => 'SuiteCRM Dashlet',
    'LBL_DASHLETLISTVIEW' => 'SuiteCRM Dashlet ListView',
    'LBL_POPUP' => 'Вид - Спливаюче вікно',
    'LBL_POPUPLISTVIEW' => 'Перегляд списку спливаючих вікон',
    'LBL_POPUPSEARCH' => 'Пошук спливаючих вікон',
    'LBL_DASHLETSEARCHVIEW' => 'SuiteCRM Dashlet Search',
    'LBL_DETAILVIEW' => 'Перегляд',
    'LBL_DROP_HERE' => '[Перемістити сюди]',
    'LBL_EDIT' => 'Правка',
    'LBL_EDIT_LAYOUT' => 'Правка розташування',
    'LBL_EDIT_FIELDS' => 'Edit Fields',
    'LBL_EDITVIEW' => 'Редагування',
    'LBL_FILLER' => '(наповнювач)',
    'LBL_FIELDS' => 'Поля',
    'LBL_FAILED_TO_SAVE' => 'Збереження неможливе',
    'LBL_FAILED_PUBLISHED' => 'Публікація неможлива',
    'LBL_HOMEPAGE_PREFIX' => 'Моя',
    'LBL_LAYOUT_PREVIEW' => 'Попередній огляд макету',
    'LBL_LAYOUTS' => 'Макети',
    'LBL_LISTVIEW' => 'Список',
    'LBL_MODULES' => 'Модулі:',
    'LBL_MODULE_TITLE' => 'Студія',
    'LBL_NEW_PACKAGE' => 'Новий пакет',
    'LBL_NEW_PANEL' => 'Нова панель',
    'LBL_NEW_ROW' => 'Новий ряд',
    'LBL_PACKAGE_DELETED' => 'Пакет видалено',
    'LBL_PUBLISHING' => 'Публікація...',
    'LBL_PUBLISHED' => 'Опубліковано',
    'LBL_SELECT_FILE' => 'Вибір файлу',
    'LBL_SUBPANELS' => 'Субпанелі',
    'LBL_SUBPANEL' => 'Субпанель',
    'LBL_SUBPANEL_TITLE' => 'Назва:',
    'LBL_SEARCH_FORMS' => 'Пошук',
    'LBL_FILTER' => 'Фільтер',
    'LBL_TOOLBOX' => 'Студія',
    'LBL_QUICKCREATE' => 'Швидке створення',
    'LBL_EDIT_DROPDOWNS' => 'Змінити загальний випадаючий список',
    'LBL_ADD_DROPDOWN' => 'Додати новий загальний випадаючий список',
    'LBL_BLANK' => '-blank-',
    'LBL_TAB_ORDER' => 'Порядок вкладок',
    'LBL_TABDEF_TYPE' => 'Display Type',
    'LBL_TABDEF_TYPE_HELP' => 'Select how this section should be displayed. This option only has effect if you have enabled tabs on this view.',
    'LBL_TABDEF_TYPE_OPTION_TAB' => 'Tab',
    'LBL_TABDEF_TYPE_OPTION_PANEL' => 'Panel',
    'LBL_TABDEF_TYPE_OPTION_HELP' => 'Select Panel to have this panel display within the view of the layout. Select Tab to have this panel displayed within a separate tab within the layout. When Tab is specified for a panel, subsequent panels set to display as Panel will be displayed within the tab. <br/>A new Tab will be started for the next panel for which Tab is selected. If Tab is selected for a panel below the first panel, the first panel will necessarily be a Tab.',
    'LBL_TABDEF_COLLAPSE' => 'Collapse',
    'LBL_TABDEF_COLLAPSE_HELP' => 'Select to make the default state of this panel collapsed.',
    'LBL_DROPDOWN_TITLE_NAME' => 'Назва',
    'LBL_DROPDOWN_LANGUAGE' => 'Мова',
    'LBL_DROPDOWN_ITEMS' => 'Елементи списку',
    'LBL_DROPDOWN_ITEM_NAME' => 'Значення в базі',
    'LBL_DROPDOWN_ITEM_LABEL' => 'Відображати ярлики',
    'LBL_SYNC_TO_DETAILVIEW' => 'Синхронізувати з "Докладною інформацією"',
    'LBL_SYNC_TO_DETAILVIEW_HELP' => 'Виберіть цю опцію, щоб синхронізувати даний макет режиму "Правка" з макетом відповідного режиму "Докладна інформація". Поля і їх розташування в макеті режиму "Правка" будуть автоматично синхронізовані з відповідними полями макету "Докладна інформація" і збережені після натиснення кнопки "Зберегти" або "Зберегти та застосувати" в режимі "Правка". Зміни макету будуть недоступні з режиму "Додаткова інформація".',
    'LBL_SYNC_TO_DETAILVIEW_NOTICE' => 'Даний макет "Докладна інформація" синхронізований з макетом відповідного режиму "Правка". Поля і їх розташування у даному макеті відображають поля і їх розташування в макеті режиму "Правка". Зміни макета режиму "Докладна інформація" не можуть бути збережені або застосовані на даній сторінці. Вносьте зміни або скасовуйте синхронізацію макетів в режимі "Правка".',
    'LBL_COPY_FROM_EDITVIEW' => 'Скопіювати з режиму "Правка".',
    'LBL_DROPDOWN_BLANK_WARNING' => 'Потрібні значення для полів Назва об&#039;єкту та Відображувана мітка. Щоб додати порожній об&#039;єкт, натисніть кнопку "Додати", не заповнюючи ці поля.',
    'LBL_DROPDOWN_KEY_EXISTS' => 'Key already exists in list',
    'LBL_NO_SAVE_ACTION' => 'Could not find the save action for this view.',
    'LBL_BADLY_FORMED_DOCUMENT' => 'Studio2:establishLocation: badly formed document',


//RELATIONSHIPS
    'LBL_MODULE' => 'Модуль',
    'LBL_LHS_MODULE' => 'Основний модуль',
    'LBL_CUSTOM_RELATIONSHIPS' => '* зв&#039;язок створено в Студії',
    'LBL_RELATIONSHIPS' => 'Зв&#039;язки',
    'LBL_RELATIONSHIP_EDIT' => 'Змінити зв&#039;язок',
    'LBL_REL_NAME' => 'Назва',
    'LBL_REL_LABEL' => 'Ярлик',
    'LBL_REL_TYPE' => 'Тип',
    'LBL_RHS_MODULE' => 'Пов&#039;язаний модуль',
    'LBL_NO_RELS' => 'Немає зв&#039;язків',
    'LBL_RELATIONSHIP_ROLE_ENTRIES' => 'Довільна умова',
    'LBL_RELATIONSHIP_ROLE_COLUMN' => 'Стовпець',
    'LBL_RELATIONSHIP_ROLE_VALUE' => 'Значення',
    'LBL_SUBPANEL_FROM' => 'Субпанель від',
    'LBL_RELATIONSHIP_ONLY' => 'Для цих відносин не будуть створені видимі елементи, оскільки між цими двома модулями вже існують видимі відносини.',
    'LBL_ONETOONE' => 'Один до одного',
    'LBL_ONETOMANY' => 'Один до багатьох',
    'LBL_MANYTOONE' => 'Багато до одного',
    'LBL_MANYTOMANY' => 'Багато до багатьох',


//STUDIO QUESTIONS
    'LBL_QUESTION_EDIT' => 'Виберіть модуль для зміни.',
    'LBL_QUESTION_LAYOUT' => 'Виберіть макет для зміни.',
    'LBL_QUESTION_SUBPANEL' => 'Виберіть субпанель для зміни.',
    'LBL_QUESTION_SEARCH' => 'Виберіть макет пошуку для зміни.',
    'LBL_QUESTION_MODULE' => 'Виберіть компонент модуля для зміни.',
    'LBL_QUESTION_PACKAGE' => 'Виберіть пакет для зміни, або створіть новий пакет',
    'LBL_QUESTION_EDITOR' => 'Виберіть інструмент.',
    'LBL_QUESTION_DASHLET' => 'Виберіть макет розділу для зміни.',
    'LBL_QUESTION_POPUP' => 'Виберіть макет спливаючого вікна для правки.',
//CUSTOM FIELDS
    'LBL_NAME' => 'Назва',
    'LBL_LABELS' => 'Ярлики',
    'LBL_MASS_UPDATE' => 'Групове оновлення',
    'LBL_DEFAULT_VALUE' => 'Значення за замовчуванням',
    'LBL_REQUIRED' => 'Обов&#039;язково',
    'LBL_DATA_TYPE' => 'Тип',
    'LBL_HCUSTOM' => 'ІНДИВІДУАЛЬНИЙ',
    'LBL_HDEFAULT' => 'ЗА ЗАМОВЧУВАННЯМ',
    'LBL_LANGUAGE' => 'Мова',
    'LBL_CUSTOM_FIELDS' => '* field created in Studio',

//SECTION
    'LBL_SECTION_EDLABELS' => 'Правка ярликів',
    'LBL_SECTION_PACKAGES' => 'Пакети',
    'LBL_SECTION_PACKAGE' => 'Пакет',
    'LBL_SECTION_MODULES' => 'Модулі',
    'LBL_SECTION_DROPDOWNS' => 'Випадаючі списки',
    'LBL_SECTION_PROPERTIES' => 'Властивості',
    'LBL_SECTION_DROPDOWNED' => 'Редагувати випадаючий список',
    'LBL_SECTION_HELP' => 'Довідка',
    'LBL_SECTION_MAIN' => 'Головна',
    'LBL_SECTION_FIELDEDITOR' => 'Редагувати поле',
    'LBL_SECTION_DEPLOY' => 'Встановити',
    'LBL_SECTION_MODULE' => 'Модуль',
//WIZARDS

//LIST VIEW EDITOR
    'LBL_DEFAULT' => 'За замовчуванням',
    'LBL_HIDDEN' => 'Приховані',
    'LBL_AVAILABLE' => 'Доступні',
    'LBL_LISTVIEW_DESCRIPTION' => 'Перед Вами три колонки. Перша колонка містить поля, що відображаються у списку за замовчуванням, друга колонка містить додаткові поля, які користувач може вибрати для створення власного макета форми, третя колонка містить поля, які поки не використовуються, вони доступні тільки адміністратору, їх він може додавати в першу і другу колонки на свій розсуд.',
    'LBL_LISTVIEW_EDIT' => 'Редактор Форми списку',

//Manager Backups History
    'LBL_MB_PREVIEW' => 'Попередній перегляд',
    'LBL_MB_RESTORE' => 'Відновити',
    'LBL_MB_DELETE' => 'Видалити',
    'LBL_MB_DEFAULT_LAYOUT' => 'Макет за замовчуванням',

//END WIZARDS

//BUTTONS
    'LBL_BTN_ADD' => 'Додати',
    'LBL_BTN_SAVE' => 'Зберегти',
    'LBL_BTN_SAVE_CHANGES' => 'Зберегти зміни',
    'LBL_BTN_DONT_SAVE' => 'Скасувати зміни',
    'LBL_BTN_CANCEL' => 'Скасування',
    'LBL_BTN_CLOSE' => 'Закрити',
    'LBL_BTN_SAVEPUBLISH' => 'Зберегти і встановити',
    'LBL_BTN_CLONE' => 'Абсолютна копія',
    'LBL_BTN_ADDROWS' => 'Додати рядки',
    'LBL_BTN_ADDFIELD' => 'Додати поле',
    'LBL_BTN_ADDDROPDOWN' => 'Додати випадаючий список',
    'LBL_BTN_SORT_ASCENDING' => 'Сортувати за зростанням',
    'LBL_BTN_SORT_DESCENDING' => 'Сортувати за спаданням',
    'LBL_BTN_EDLABELS' => 'Правка ярликів',
    'LBL_BTN_UNDO' => 'Скасування',
    'LBL_BTN_REDO' => 'Повторне виконання',
    'LBL_BTN_ADDCUSTOMFIELD' => 'Додати індивідуальне поле',
    'LBL_BTN_EXPORT' => 'Експортувати індивідуальні налаштування',
    'LBL_BTN_DUPLICATE' => 'Дублювати',
    'LBL_BTN_PUBLISH' => 'Опублікувати',
    'LBL_BTN_DEPLOY' => 'Встановити',
    'LBL_BTN_EXP' => 'Експорт',
    'LBL_BTN_DELETE' => 'Видалити',
    'LBL_BTN_VIEW_LAYOUTS' => 'Перегляд макетів',
    'LBL_BTN_VIEW_FIELDS' => 'Перегляд полів',
    'LBL_BTN_VIEW_RELATIONSHIPS' => 'Перегляд відносин',
    'LBL_BTN_ADD_RELATIONSHIP' => 'Додати відношення',
    'LBL_BTN_RENAME_MODULE' => 'Change Module Name',
//TABS


//ERRORS
    'ERROR_ALREADY_EXISTS' => 'Помилка: Поле вже існує',
    'ERROR_INVALID_KEY_VALUE' => "Помилка: Невірне значення префікса: [&#39;]",
    'ERROR_NO_HISTORY' => 'Не знайдено файлів історії',
    'ERROR_MINIMUM_FIELDS' => 'В макеті має міститися принаймні одне поле',
    'ERROR_GENERIC_TITLE' => 'Відбулася помилка',
    'ERROR_REQUIRED_FIELDS' => 'Ви впевнені, що хочете продовжити? В макеті відсутнє наступне необхідне поле:',


//PACKAGE AND MODULE BUILDER
    'LBL_PACKAGE_NAME' => 'Назва пакету:',
    'LBL_MODULE_NAME' => 'Назва модуля:',
    'LBL_AUTHOR' => 'Автор:',
    'LBL_DESCRIPTION' => 'Опис:',
    'LBL_KEY' => 'Префікс:',
    'LBL_ADD_README' => 'Readme',
    'LBL_LAST_MODIFIED' => 'Остання зміна:',
    'LBL_NEW_MODULE' => 'Новий модуль',
    'LBL_LABEL' => 'Ярлик:',
    'LBL_LABEL_TITLE' => 'Ярлик',
    'LBL_WIDTH' => 'Ширина',
    'LBL_PACKAGE' => 'Пакет:',
    'LBL_TYPE' => 'Тип:',
    'LBL_NAV_TAB' => 'Панель навігації',
    'LBL_CREATE' => 'Створити',
    'LBL_LIST' => 'Список',
    'LBL_VIEW' => 'Перегляд',
    'LBL_HISTORY' => 'Історія',
    'LBL_RESTORE_DEFAULT' => 'Повернутися до стандартних',
    'LBL_ACTIVITIES' => 'Заходи',
    'LBL_NEW' => 'Нова',
    'LBL_TYPE_BASIC' => 'basic',
    'LBL_TYPE_COMPANY' => 'company',
    'LBL_TYPE_PERSON' => 'person',
    'LBL_TYPE_ISSUE' => 'issue',
    'LBL_TYPE_SALE' => 'sale',
    'LBL_TYPE_FILE' => 'file',
    'LBL_RSUB' => 'Це субпанель, яка буде відображатися в модулі',
    'LBL_MSUB' => 'Це суб-панель, яку ваш модуль надає для відображення в пов&#039;язаних з ним модулях.',
    'LBL_MB_IMPORTABLE' => 'Імпорт',

// VISIBILITY EDITOR
    'LBL_PACKAGE_WAS_DELETED' => '[[пакет]] був видалений',

//EXPORT CUSTOMS
    'LBL_EC_TITLE' => 'Експортувати індивідуальні налаштування',
    'LBL_EC_NAME' => 'Ім&#039;я пакету:',
    'LBL_EC_AUTHOR' => 'Автор:',
    'LBL_EC_DESCRIPTION' => 'Опис:',
    'LBL_EC_CHECKERROR' => 'Будь ласка, виберіть модуль.',
    'LBL_EC_CUSTOMFIELD' => 'Індивідуально змінене (-і) поле (-я)',
    'LBL_EC_CUSTOMLAYOUT' => 'Індивідуально змінений (-і) макет (-и)',
    'LBL_EC_NOCUSTOM' => 'Немає індивідуально змінених модулів.',
    'LBL_EC_EMPTYCUSTOM' => 'має порожні індивідуальні зміни.',
    'LBL_EC_EXPORTBTN' => 'Експорт',
    'LBL_MODULE_DEPLOYED' => 'Модуль був встановлений.',
    'LBL_UNDEFINED' => 'не визначено',
    'LBL_EC_VIEWS' => 'customized view(s)',
    'LBL_EC_SUGARFEEDS' => 'customized Feed(s)',
    'LBL_EC_DASHLETS' => 'customized Dashlet(s)',
    'LBL_EC_CSS' => 'customized css(s)',
    'LBL_EC_TPLS' => 'customized tpls(s)',
    'LBL_EC_IMAGES' => 'customized image(s)',
    'LBL_EC_JS' => 'customized js(s)',
    'LBL_EC_QTIP' => 'customized qtip(s)',

//AJAX STATUS
    'LBL_AJAX_FAILED_DATA' => 'Не вдалося отримати дані',
    'LBL_AJAX_LOADING' => 'Завантаження...',
    'LBL_AJAX_DELETING' => 'Видалення...',
    'LBL_AJAX_BUILDPROGRESS' => 'Виконується створення...',
    'LBL_AJAX_DEPLOYPROGRESS' => 'Виконується установка...',

    'LBL_AJAX_RESPONSE_TITLE' => 'Result',
    'LBL_AJAX_RESPONSE_MESSAGE' => 'This operation is completed successfully',
    'LBL_AJAX_LOADING_TITLE' => 'In Progress..',
    'LBL_AJAX_LOADING_MESSAGE' => 'Please wait, loading..',

//JS
    'LBL_JS_REMOVE_PACKAGE' => 'Ви дійсно хочете видалити цей пакет? Цією дією Ви остаточно видалите всі файли, пов&#039;язані з цим пакетом.',
    'LBL_JS_REMOVE_MODULE' => 'Ви дійсно хочете видалити цей модуль? Цією дією Ви остаточно видалити всі файли, пов&#039;язані з цим модулем.',
    'LBL_JS_DEPLOY_PACKAGE' => 'Будь-які зміни, внесені Вами в Студії, будуть перезаписані при повторній установці цього модуля. Ви впевнені, що хочете продовжити?',

    'LBL_DEPLOY_IN_PROGRESS' => 'Установка пакета',
    'LBL_JS_VALIDATE_NAME' => 'Назва - повинна містити літери та цифри без пробілів, і починатися з літери.',
    'LBL_JS_VALIDATE_PACKAGE_NAME' => 'Така назва пакету вже існує.',
    'LBL_JS_VALIDATE_KEY' => 'Префікс - повинен містити літери та цифри',
    'LBL_JS_VALIDATE_LABEL' => 'Будь ласка, введіть ярлик, який буде використовуватися в якості назви при відображенні цього модуля.',
    'LBL_JS_VALIDATE_TYPE' => 'Будь ласка, зі списку виберіть тип модуля',
    'LBL_JS_VALIDATE_REL_LABEL' => 'Ярлик - додайте ярлик, який буде відображатися над суб-панеллю.',

//CONFIRM
    'LBL_CONFIRM_FIELD_DELETE' => 'Deleting this custom field will delete both the custom field and all the data related to the custom field in the database. The field will be no longer appear in any module layouts. \n\nDo you wish to continue?',

    'LBL_CONFIRM_RELATIONSHIP_DELETE' => 'Ви впевнені, що хочете видалити цей зв&#039;язок?',
    'LBL_CONFIRM_DONT_SAVE' => 'З моменту останнього збереження були внесені зміни. Хочете зберегти зміни зараз?',
    'LBL_CONFIRM_DONT_SAVE_TITLE' => 'Зберегти зміни?',
    'LBL_CONFIRM_LOWER_LENGTH' => 'Дані можуть бути обрізані і це неможливо буде скасувати в подальшому. Ви впевнені, що хочете продовжити?',

//POPUP HELP
    'LBL_POPHELP_FIELD_DATA_TYPE' => 'Виберіть відповідний тип даних, ґрунтуючись на типі даних, які будуть введені в поле.',

//Revert Module labels
    'LBL_RESET' => 'Скинути',
    'LBL_RESET_MODULE' => 'Очистити модуль',
    'LBL_REMOVE_CUSTOM' => 'Прибрати індивідуальні налаштування',
    'LBL_CLEAR_RELATIONSHIPS' => 'Очистити відносини',
    'LBL_RESET_LABELS' => 'Очистити ярлики',
    'LBL_RESET_LAYOUTS' => 'Очистити відображення зовнішнього вигляду',
    'LBL_REMOVE_FIELDS' => 'Прибрати індивідуальні поля',
    'LBL_CLEAR_EXTENSIONS' => 'Очистити розширення',
    'LBL_HISTORY_TIMESTAMP' => 'Позначка часу',
    'LBL_HISTORY_TITLE' => 'історія',

    'fieldTypes' => array(
        'varchar' => 'Текстове поле',
        'int' => 'Ціле значення',
        'float' => 'Значення з плаваючою точкою',
        'bool' => 'Прапорець',
        'enum' => 'Розкривний список',
        'dynamicenum' => 'Dynamic DropDown',
        'multienum' => 'Групове виділення',
        'date' => 'Дата',
        'phone' => 'Телефон',
        'currency' => 'Валюта',
        'html' => 'HTML-повідомлення',
        'radioenum' => 'Радіо',
        'relate' => 'Відноситься до',
        'address' => 'Адреса',
        'text' => 'Текстове поле',
        'url' => 'URL',
        'iframe' => 'Плаваючий кадр',
        'datetimecombo' => 'Дата/час',
        'encrypt'=>'Encrypt',
        'decimal' => 'Десяткове значення',
        'image' => 'Зображення',
    ),
    'labelTypes' => array(
        "frequently_used" => "Frequently used labels",
        "all" => "Всі мітки",
    ),

    'parent' => 'Гнучкі відносини',

    'LBL_CONFIRM_SAVE_DROPDOWN' => "Ви вибрали цей об&#039;єкт для видалення з випадаючого списку. Будь-які випадаючі поля, що містять список з цим об&#039;єктом в якості значення, не будуть більше відображати це значення, і значення не буде більше доступним для вибору в випадаючих полях. Ви дійсно хочете продовжити?",

    'LBL_ALL_MODULES' => 'Всі модулі',
    'LBL_RELATED_FIELD_ID_NAME_LABEL' => '{0} (related {1} ID)',
);

