<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}



$app_list_strings = array(
//e.g. auf Deutsch 'Contacts'=>'Contakten',
    'language_pack_name' => 'український (Ukrainian) - uk_UA',
    'moduleList' => array(
        'Home' => 'Головна',
        'ResourceCalendar' => 'Діаграма ресурсів',
        'Contacts' => 'Контакти',
        'Accounts' => 'Контрагенти',
        'Alerts' => 'Сповіщення',
        'Opportunities' => 'Угоди',
        'Cases' => 'Звернення',
        'Notes' => 'Нотатки',
        'Calls' => 'Дзвінки',
        'TemplateSectionLine' => 'Шаблонна секція строки',
        'Calls_Reschedule' => 'Перепланувати дзвінки',
        'Emails' => 'Електронні листи',
        'EAPM' => 'EAPM',
        'Meetings' => 'Зустрічі',
        'Tasks' => 'Завдання',
        'Calendar' => 'Календар',
        'Leads' => 'Попередні контакти',
        'Currencies' => 'Типи валюти',
        'Activities' => 'Заходи',
        'Bugs' => 'Відстеження помилок',
        'Feeds' => 'RSS',
        'iFrames' => 'Мої сайти',
        'TimePeriods' => 'Звітні періоди',
        'ContractTypes' => 'Типи контрактів',
        'Schedulers' => 'Planners',
        'Project' => 'Проекти',
        'ProjectTask' => 'Проектні завдання',
        'Campaigns' => 'Маркетингові кампанії',
        'CampaignLog' => 'Журнал маркетингових кампаній',
        'Documents' => 'Документи',
        'DocumentRevisions' => 'Ревізії документів',
        'Connectors' => 'Підключення',
        'Roles' => 'Ролі',
        'Notifications' => 'Повідомлення',
        'Sync' => 'Синхронізація',
        'Users' => 'Користувачі',
        'Employees' => 'Співробітники',
        'Administration' => 'Адміністрування',
        'ACLRoles' => 'Ролі',
        'InboundEmail' => 'E-mail',
        'Releases' => 'Версії',
        'Prospects' => 'Огляд потенційних клієнтів',
        'Queues' => 'Черги',
        'EmailMarketing' => 'Розсилки E-mail',
        'EmailTemplates' => 'Email - Templates',
        'ProspectLists' => 'Цілі - список',
        'SavedSearch' => 'Збережені пошуки',
        'UpgradeWizard' => 'Майстер оновлення',
        'Trackers' => 'Трекери',
        'TrackerSessions' => 'Сесії трекера',
        'TrackerQueries' => 'Запити трекера',
        'FAQ' => 'Найчастіші запитання',
        'Newsletters' => 'Огляд рекламних проспектів',
        'SugarFeed' => 'SuiteCRM канал',
        'SugarFavorites' => 'SuiteCRM уподобання',

        'OAuthKeys' => 'Клієнтські OAuth ключі',
        'OAuthTokens' => 'Токени OAuth',
    ),

    'moduleListSingular' => array(
        'Home' => 'Головна',
        'Dashboard' => 'Діаграми',
        'Contacts' => 'Контакт',
        'Accounts' => 'Контрагент',
        'Opportunities' => 'Угода',
        'Cases' => 'Звернення',
        'Notes' => 'Замітка',
        'Calls' => 'Дзвінок',
        'Emails' => 'E-mail',
        'EmailTemplates' => 'Шаблони E-mail',
        'Meetings' => 'Зустріч',
        'Tasks' => 'Завдання',
        'Calendar' => 'Календар',
        'Leads' => 'Попередній контакт',
        'Activities' => 'Захід',
        'Bugs' => 'Відстеження помилок',
        'KBDocuments' => 'База знань',
        'Feeds' => 'Feeds',
        'iFrames' => 'Мої сайти',
        'TimePeriods' => 'Проміжок часу',
        'Project' => 'Проекти',
        'ProjectTask' => 'Завдання проекту',
        'Prospects' => 'Адресат',
        'Campaigns' => 'Маркетингова кампанія',
        'Documents' => 'Документ',
        'Sync' => 'Синхронізація',
        'Users' => 'Користувач',
        'SugarFavorites' => 'SuiteCRM уподобання',

    ),

    'checkbox_dom' => array(
        '' => '',
        '1' => 'Так',
        '2' => 'Ні',
    ),

    //e.g. en francais 'Analyst'=>'Analyste',
    'account_type_dom' => array(
        '' => '',
        'Analyst' => 'Аналітик',
        'Competitor' => 'Конкурент',
        'Customer' => 'Клієнт',
        'Integrator' => 'Системний аналітик',
        'Investor' => 'Інвестор',
        'Partner' => 'Партнер',
        'Press' => 'Преса',
        'Prospect' => 'Потенційний клієнт',
        'Reseller' => 'Посередник',
        'Other' => 'Інший',
    ),
    //e.g. en espanol 'Apparel'=>'Ropa',
    'industry_dom' => array(
        '' => '',
        'Apparel' => 'Одяг',
        'Banking' => 'Банківська справа',
        'Biotechnology' => 'Біотехнології',
        'Chemicals' => 'Хімічна промисловість',
        'Communications' => 'Комунікації',
        'Construction' => 'Будівництво',
        'Consulting' => 'Консалтинг',
        'Education' => 'Освіта',
        'Electronics' => 'Електронне обладнання',
        'Energy' => 'Енергетика',
        'Engineering' => 'Інженерія',
        'Entertainment' => 'Розваги',
        'Environmental' => 'Екологія',
        'Finance' => 'Фінанси',
        'Government' => 'Уряд',
        'Healthcare' => 'Охорона здоров&#039;я',
        'Hospitality' => 'Готельна справа',
        'Insurance' => 'Страхування',
        'Machinery' => 'Машинобудування',
        'Manufacturing' => 'Виробництво',
        'Media' => 'Медіа',
        'Not For Profit' => 'Некомерційна діяльність',
        'Recreation' => 'Відпочинок',
        'Retail' => 'Торгівля',
        'Shipping' => 'Вантажні перевезення',
        'Technology' => 'Технології',
        'Telecommunications' => 'Телекомунікації',
        'Transportation' => 'Транспортні перевезення',
        'Utilities' => 'Комунальні системи',
        'Other' => 'Інший',
    ),
    'lead_source_default_key' => 'Self Generated',
    'lead_source_dom' => array(
        '' => '',
        'Cold Call' => 'Холодний дзвінок',
        'Existing Customer' => 'Існуючий клієнт',
        'Self Generated' => 'Пряме звернення',
        'Employee' => 'Співробітник',
        'Partner' => 'Партнер',
        'Public Relations' => 'PR-діяльність',
        'Direct Mail' => 'Пряма розсилка',
        'Conference' => 'Конференція',
        'Trade Show' => 'Спеціалізована виставка',
        'Web Site' => 'Веб-сайт',
        'Word of mouth' => 'Усна рекомендація',
        'Email' => 'E-mail',
        'Campaign' => 'Маркетингова кампанія',
        'Other' => 'Інший',
    ),
    'opportunity_type_dom' => array(
        '' => '',
        'Existing Business' => 'Існуючий бізнес',
        'New Business' => 'Новий бізнес',
    ),
    'roi_type_dom' => array(
        'Revenue' => 'Дохід',
        'Investment' => 'Інвестиції',
        'Expected_Revenue' => 'Очікуваний дохід',
        'Budget' => 'Бюджет',

    ),
    //Note:  do not translate opportunity_relationship_type_default_key
//       it is the key for the default opportunity_relationship_type_dom value
    'opportunity_relationship_type_default_key' => 'Primary Decision Maker',
    'opportunity_relationship_type_dom' => array(
        '' => '',
        'Primary Decision Maker' => 'Відповідальний за основні рішення',
        'Business Decision Maker' => 'Відповідальний за бізнес-рішення',
        'Business Evaluator' => 'Бізнес-експерт',
        'Technical Decision Maker' => 'Відповідальний за технічні рішення',
        'Technical Evaluator' => 'Технічний оцінювач',
        'Executive Sponsor' => 'Генеральний поручитель',
        'Influencer' => 'Що робить вплив',
        'Other' => 'Інший',
    ),
    //Note:  do not translate case_relationship_type_default_key
//       it is the key for the default case_relationship_type_dom value
    'case_relationship_type_default_key' => 'Primary Contact',
    'case_relationship_type_dom' => array(
        '' => '',
        'Primary Contact' => 'Основна контактна особа',
        'Alternate Contact' => 'Альтернативний контакт',
    ),
    'payment_terms' => array(
        '' => '',
        'Net 15' => 'Нетто 15',
        'Net 30' => 'Нетто 30',
    ),
    'sales_stage_default_key' => 'Prospecting',
    'sales_stage_dom' => array(
        'Prospecting' => 'Prospecting',
        'Qualification' => 'Кваліфікація',
        'Needs Analysis' => 'Аналіз потреб',
        'Value Proposition' => 'Пропозиція цінності',
        'Id. Decision Makers' => 'Identifying Decision Makers',
        'Perception Analysis' => 'Аналіз реакції',
        'Proposal/Price Quote' => 'Комерц. пропозиція/Виставлення рахунку',
        'Negotiation/Review' => 'Узгодження/Перегляд',
        'Closed Won' => 'Успішно закрито',
        'Closed Lost' => 'Закрито без успіху',
    ),
    'sales_probability_dom' => // keys must be the same as sales_stage_dom
        array(
            'Prospecting' => '10',
            'Qualification' => '20',
            'Needs Analysis' => '25',
            'Value Proposition' => '30',
            'Id. Decision Makers' => '40',
            'Perception Analysis' => '50',
            'Proposal/Price Quote' => '65',
            'Negotiation/Review' => '80',
            'Closed Won' => '100',
            'Closed Lost' => '0',
        ),
    'activity_dom' => array(
        'Call' => 'Дзвінок',
        'Meeting' => 'Зустріч',
        'Task' => 'Завдання',
        'Email' => 'E-mail',
        'Note' => 'Замітка',
    ),
    'salutation_dom' => array(
        '' => '',
        'Mr.' => 'Пан',
        'Ms.' => 'Пані',
        'Mrs.' => 'Пані',
        'Miss' => 'Miss',
        'Dr.' => 'Д-р',
        'Prof.' => 'Проф.',
    ),
    //time is in seconds; the greater the time the longer it takes;
    'reminder_max_time' => 90000,
    'reminder_time_options' => array(
        60 => '1 хвилина до',
        300 => '5 хвилин до',
        600 => '10 хвилин до',
        900 => '15 хвилин до',
        1800 => '30 хвилин до',
        3600 => '1 година до',
        7200 => '2 години до',
        10800 => '3 години до',
        18000 => '5 годин до',
        86400 => '1 день до',
    ),

    'task_priority_default' => 'Середній',
    'task_priority_dom' => array(
        'High' => 'Високий',
        'Medium' => 'Середній',
        'Low' => 'Низький',
    ),
    'task_status_default' => 'Проект',
    'task_status_dom' => array(
        'Not Started' => 'Не розпочато',
        'In Progress' => 'У процесі',
        'Completed' => 'Завершено',
        'Pending Input' => 'Очікування рішення',
        'Deferred' => 'Відкладено',
    ),
    'meeting_status_default' => 'Planned',
    'meeting_status_dom' => array(
        'Planned' => 'Запланована',
        'Held' => 'Відбулася',
        'Not Held' => 'Не відбулася',
    ),
    'extapi_meeting_password' => array(
        'WebEx' => 'WebEx',
    ),
    'meeting_type_dom' => array(
        'Other' => 'Інший',
        'Sugar' => 'SuiteCRM',
    ),
    'call_status_default' => 'Planned',
    'call_status_dom' => array(
        'Planned' => 'Запланована',
        'Held' => 'Відбулася',
        'Not Held' => 'Не відбулася',
    ),
    'call_direction_default' => 'Outbound',
    'call_direction_dom' => array(
        'Inbound' => 'Вхідний',
        'Outbound' => 'Вихідний',
    ),
    'lead_status_dom' => array(
        '' => '',
        'New' => 'Новий',
        'Assigned' => 'Призначений',
        'In Process' => 'У процесі',
        'Converted' => 'Перетворений',
        'Recycled' => 'Повторний',
        'Dead' => 'Загублений',
    ),
    'case_priority_default_key' => 'P2',
    'case_priority_dom' => array(
        'P1' => 'Високий',
        'P2' => 'Середній',
        'P3' => 'Низький',
    ),
    'user_type_dom' => array(
        'RegularUser' => 'Звичайний користувач',
        'Administrator' => 'Адміністратор',
    ),
    'user_status_dom' => array(
        'Active' => 'Активна',
        'Inactive' => 'Неактивна',
    ),
    'employee_status_dom' => array(
        'Active' => 'Активна',
        'Terminated' => 'Звільнено',
        'Leave of Absence' => 'У відпустці',
    ),
    'messenger_type_dom' => array(
        '' => '',
        'MSN' => 'MSN',
        'Yahoo!' => 'Yahoo!',
        'AOL' => 'AOL',
    ),
    'project_task_priority_options' => array(
        'High' => 'Високий',
        'Medium' => 'Середній',
        'Low' => 'Низький',
    ),
    'project_task_priority_default' => 'Середній',

    'project_task_status_options' => array(
        'Not Started' => 'Не розпочато',
        'In Progress' => 'У процесі',
        'Completed' => 'Завершено',
        'Pending Input' => 'Очікування рішення',
        'Deferred' => 'Відкладено',
    ),
    'project_task_utilization_options' => array(
        '0' => 'немає',
        '25' => '25',
        '50' => '50',
        '75' => '75',
        '100' => '100',
    ),

    'project_status_dom' => array(
        'Draft' => 'Чернетка',
        'In Review' => 'У розгляді',
        'Underway' => 'Реалізується',
        'On_Hold' => 'Ведеться',
        'Completed' => 'Завершено',
    ),
    'project_status_default' => 'Чернетка',

    'project_duration_units_dom' => array(
        'Days' => 'Днів',
        'Hours' => 'Годин',
    ),

    'activity_status_type_dom' => array(
        '' => '--Не вибрано--',
        'active' => 'Активна',
        'inactive' => 'Неактивна',
    ),

    // Note:  do not translate record_type_default_key
    //        it is the key for the default record_type_module value
    'record_type_default_key' => 'Облікові записи',
    'record_type_display' => array(
        '' => '',
        'Accounts' => 'Контрагент',
        'Opportunities' => 'Угода',
        'Cases' => 'Звернення',
        'Leads' => 'Попередній контакт',
        'Contacts' => 'Контакт', // cn (11/22/2005) added to support Emails

        'Bugs' => 'Відстеження помилок',
        'Project' => 'Проекти',

        'Prospects' => 'Адресат',
        'ProjectTask' => 'Завдання проекту',

        'Tasks' => 'Завдання',

    ),

    'record_type_display_notes' => array(
        'Accounts' => 'Контрагент',
        'Contacts' => 'Контакт',
        'Opportunities' => 'Угода',
        'Tasks' => 'Завдання',
        'Emails' => 'E-mail',

        'Bugs' => 'Відстеження помилок',
        'Project' => 'Проекти',
        'ProjectTask' => 'Завдання проекту',
        'Prospects' => 'Адресат',
        'Cases' => 'Звернення',
        'Leads' => 'Попередній контакт',

        'Meetings' => 'Зустріч',
        'Calls' => 'Дзвінок',
    ),

    'parent_type_display' => array(
        'Accounts' => 'Контрагент',
        'Contacts' => 'Контакт',
        'Tasks' => 'Завдання',
        'Opportunities' => 'Угода',

        'Bugs' => 'Відстеження помилок',
        'Cases' => 'Звернення',
        'Leads' => 'Попередній контакт',

        'Project' => 'Проекти',
        'ProjectTask' => 'Завдання проекту',

        'Prospects' => 'Адресат',

    ),

    'issue_priority_default_key' => 'Середній',
    'issue_priority_dom' => array(
        'Urgent' => 'Строковий',
        'High' => 'Високий',
        'Medium' => 'Середній',
        'Low' => 'Низький',
    ),
    'issue_resolution_default_key' => '',
    'issue_resolution_dom' => array(
        '' => '',
        'Accepted' => 'Прийнято',
        'Duplicate' => 'Дублювати',
        'Closed' => 'Закрито',
        'Out of Date' => 'Застаріло',
        'Invalid' => 'Недійсне',
    ),

    'issue_status_default_key' => 'Новий',
    'issue_status_dom' => array(
        'New' => 'Новий',
        'Assigned' => 'Призначений',
        'Closed' => 'Закрито',
        'Pending' => 'В очікуванні',
        'Rejected' => 'Відхилена',
    ),

    'bug_priority_default_key' => 'Середній',
    'bug_priority_dom' => array(
        'Urgent' => 'Строковий',
        'High' => 'Високий',
        'Medium' => 'Середній',
        'Low' => 'Низький',
    ),
    'bug_resolution_default_key' => '',
    'bug_resolution_dom' => array(
        '' => '',
        'Accepted' => 'Прийнято',
        'Duplicate' => 'Дублювати',
        'Fixed' => 'Виправлена',
        'Out of Date' => 'Застаріло',
        'Invalid' => 'Недійсне',
        'Later' => 'Відкладена',
    ),
    'bug_status_default_key' => 'Новий',
    'bug_status_dom' => array(
        'New' => 'Новий',
        'Assigned' => 'Призначений',
        'Closed' => 'Закрито',
        'Pending' => 'В очікуванні',
        'Rejected' => 'Відхилена',
    ),
    'bug_type_default_key' => 'Відстеження помилок',
    'bug_type_dom' => array(
        'Defect' => 'Помилка',
        'Feature' => 'Особливість',
    ),
    'case_type_dom' => array(
        'Administration' => 'Адміністрування',
        'Product' => 'Продукт',
        'User' => 'Користувач',
    ),

    'source_default_key' => '',
    'source_dom' => array(
        '' => '',
        'Internal' => 'Внутрішній запит',
        'Forum' => 'Форум',
        'Web' => 'Веб-реклама',
        'InboundEmail' => 'E-mail',
    ),

    'product_category_default_key' => '',
    'product_category_dom' => array(
        '' => '',
        'Accounts' => 'Контрагенти',
        'Activities' => 'Заходи',
        'Bugs' => 'Відстеження помилок',
        'Calendar' => 'Календар',
        'Calls' => 'Дзвінки',
        'Campaigns' => 'Маркетингові кампанії',
        'Cases' => 'Звернення',
        'Contacts' => 'Контакти',
        'Currencies' => 'Типи валюти',
        'Dashboard' => 'Діаграми',
        'Documents' => 'Документи',
        'Emails' => 'Електронні листи',
        'Feeds' => 'Канали',
        'Forecasts' => 'Прогноз',
        'Help' => 'Довідка',
        'Home' => 'Головна',
        'Leads' => 'Попередні контакти',
        'Meetings' => 'Зустрічі',
        'Notes' => 'Нотатки',
        'Opportunities' => 'Угоди',
        'Outlook Plugin' => 'Outlook-плагін',
        'Projects' => 'Проекти',
        'Quotes' => 'Комерційну пропозицію',
        'Releases' => 'Версії',
        'RSS' => 'RSS',
        'Studio' => 'Студія',
        'Upgrade' => 'Оновлення',
        'Users' => 'Користувачі',
    ),
    /*Added entries 'Queued' and 'Sending' for 4.0 release..*/
    'campaign_status_dom' => array(
        '' => '',
        'Planning' => 'Планується',
        'Active' => 'Активна',
        'Inactive' => 'Неактивна',
        'Complete' => 'Завершена',
        //'In Queue' => 'In Queue',
        //'Sending' => 'Sending',
    ),
    'campaign_type_dom' => array(
        '' => '',
        'Telesales' => 'Продажі по телефону',
        'Mail' => 'Поштова розсилка',
        'Email' => 'E-mail',
        'Print' => 'Друк',
        'Web' => 'Веб-реклама',
        'Radio' => 'Радіо',
        'Television' => 'Телебачення',
        'NewsLetter' => 'Рекламний проспект',
    ),

    'newsletter_frequency_dom' => array(
        '' => '',
        'Weekly' => 'Щотижня',
        'Monthly' => 'Щомісяця',
        'Quarterly' => 'Щокварталу',
        'Annually' => 'Щорічно',
    ),

    'notifymail_sendtype' => array(
        'SMTP' => 'SMTP',
    ),
    'dom_cal_month_long' => array(
        '0' => '',
        '1' => 'Січень',
        '2' => 'Лютий',
        '3' => 'Березень',
        '4' => 'Квітень',
        '5' => 'Травень',
        '6' => 'Червень',
        '7' => 'Липень',
        '8' => 'Серпень',
        '9' => 'Вересень',
        '10' => 'Жовтень',
        '11' => 'Листопад',
        '12' => 'Грудень',
    ),
    'dom_cal_month_short' => array(
        '0' => '',
        '1' => 'Січ',
        '2' => 'Лют',
        '3' => 'Бер',
        '4' => 'Кві',
        '5' => 'Тра',
        '6' => 'Чер',
        '7' => 'Лип',
        '8' => 'Сер',
        '9' => 'Вер',
        '10' => 'Жов',
        '11' => 'Лис',
        '12' => 'Гру',
    ),
    'dom_cal_day_long' => array(
        '0' => '',
        '1' => 'Неділя',
        '2' => 'Понеділок',
        '3' => 'Вівторок',
        '4' => 'Середа',
        '5' => 'Четвер',
        '6' => 'П&#039;ятниця',
        '7' => 'Субота',
    ),
    'dom_cal_day_short' => array(
        '0' => '',
        '1' => 'Нд',
        '2' => 'Пн',
        '3' => 'Вт',
        '4' => 'Ср',
        '5' => 'Чт',
        '6' => 'Пт',
        '7' => 'Сб',
    ),
    'dom_meridiem_lowercase' => array(
        'am' => 'дня',
        'pm' => 'am',
    ),
    'dom_meridiem_uppercase' => array(
        'AM' => 'Дня',
        'PM' => 'Вечора',
    ),

    'dom_email_types' => array(
        'out' => 'Відправлено',
        'archived' => 'Архів',
        'draft' => 'В чернетки',
        'inbound' => 'Вхідне',
        'campaign' => 'Маркетингова кампанія:',
    ),
    'dom_email_status' => array(
        'archived' => 'Архів',
        'closed' => 'Закрито',
        'draft' => 'В чернетки',
        'read' => 'Прочитано',
        'replied' => 'Відповідь',
        'sent' => 'Відправлено',
        'send_error' => 'Помилка відсилання',
        'unread' => 'Непрочитано',
    ),
    'dom_email_archived_status' => array(
        'archived' => 'Архів',
    ),

    'dom_email_server_type' => array(
        '' => '--Не вибрано--',
        'imap' => 'IMAP',
    ),
    'dom_mailbox_type' => array(/*''           => '--None Specified--',*/
        'pick' => '--Не вибрано--',
        'createcase' => 'Нове звернення',
        'bounce' => 'Обробка повернення',
    ),
    'dom_email_distribution' => array(
        '' => '--Не вибрано--',
        'direct' => 'Пряме призначення',
        'roundRobin' => 'У циклі',
        'leastBusy' => 'Найменш зайнятої',
    ),
    'dom_email_bool' => array(
        'bool_true' => 'Так',
        'bool_false' => 'Ні',
    ),
    'dom_int_bool' => array(
        1 => 'Так',
        0 => 'Ні',
    ),
    'dom_switch_bool' => array(
        'on' => 'Конкретна дата',
        'off' => 'Ні',
        '' => '--Не вибрано--',
    ),

    'dom_email_link_type' => array(
        'sugar' => 'Поштовий клієнт SuiteCRM',
        'mailto' => 'Зовнішній поштовий клієнт',
    ),

    'dom_editor_type' => array(
        'none' => 'Direct HTML',
        'tinymce' => 'TinyMCE',
        'mozaik' => 'Mozaik',
    ),

    'dom_email_editor_option' => array(
        '' => '--Не вибрано--',
        'html' => 'HTML-повідомлення',
        'plain' => 'Текстове повідомлення',
    ),

    'schedulers_times_dom' => array(
        'not run' => 'Час запуску пройшов, не виконуємо.',
        'ready' => 'Готово',
        'in progress' => 'У процесі',
        'failed' => 'Невдало',
        'completed' => 'Завершено',
        'no curl' => 'Не запущено: cURL не доступний',
    ),

    'scheduler_status_dom' => array(
        'Active' => 'Активна',
        'Inactive' => 'Неактивна',
    ),

    'scheduler_period_dom' => array(
        'min' => 'Хвилин',
        'hour' => 'Годин',
    ),
    'document_category_dom' => array(
        '' => '',
        'Marketing' => 'Маркетинг',
        'Knowledege Base' => 'База знань',
        'Sales' => 'Продажі',
    ),

    'email_category_dom' => array(
        '' => '',
        'Archived' => 'Архів',
        // TODO: add more categories here...
    ),

    'document_subcategory_dom' => array(
        '' => '',
        'Marketing Collateral' => 'Додаткове маркетингове забезпечення',
        'Product Brochures' => 'Брошури продуктів',
        'FAQ' => 'Найчастіші запитання',
    ),

    'document_status_dom' => array(
        'Active' => 'Активна',
        'Draft' => 'Чернетка',
        'FAQ' => 'Найчастіші запитання',
        'Expired' => 'Прострочений',
        'Under Review' => 'На розгляді',
        'Pending' => 'В очікуванні',
    ),
    'document_template_type_dom' => array(
        '' => '',
        'mailmerge' => 'Злиття пошти',
        'eula' => 'Угода користувача (EULA)',
        'nda' => 'Угоду про нерозголошення (NDA)',
        'license' => 'Ліцензійна угода',
    ),
    'dom_meeting_accept_options' => array(
        'accept' => 'Прийнято',
        'decline' => 'Відхилено',
        'tentative' => 'Під питанням',
    ),
    'dom_meeting_accept_status' => array(
        'accept' => 'Прийнято',
        'decline' => 'Відхилено',
        'tentative' => 'Під питанням',
        'none' => 'Немає',
    ),
    'duration_intervals' => array(
        '0' => '00',
        '15' => '15',
        '30' => '30',
        '45' => '45',
    ),
    'repeat_type_dom' => array(
        '' => '--Не вибрано--',
        'Daily' => 'Щодня',
        'Weekly' => 'Щотижня',
        'Monthly' => 'Щомісяця',
        'Yearly' => 'Щороку',
    ),

    'repeat_intervals' => array(
        '' => '',
        'Daily' => 'день(дні)',
        'Weekly' => 'тиждень(тиждні)',
        'Monthly' => 'місяць(місяці)',
        'Yearly' => 'рік(роки)',
    ),

    'duration_dom' => array(
        '' => '--Не вибрано--',
        '900' => '15 хвилин',
        '1800' => '30 хвилин',
        '2700' => '45 хвилин',
        '3600' => '1 година',
        '5400' => '1,5 години',
        '7200' => '2 години',
        '10800' => '3 години',
        '21600' => '6 годин',
        '86400' => '1 день',
        '172800' => '2 дні',
        '259200' => '3 дні',
        '604800' => '1 тиждень',
    ),


//prospect list type dom
    'prospect_list_type_dom' => array(
        'default' => 'За замовчуванням',
        'seed' => 'Першоджерело',
        'exempt_domain' => 'Виняток - по домену',
        'exempt_address' => 'Виняток - по email-адресі',
        'exempt' => 'Виняток - по ID',
        'test' => 'Тест',
    ),

    'email_settings_num_dom' => array(
        '10' => '10',
        '20' => '20',
        '50' => '50',
    ),
    'email_marketing_status_dom' => array(
        '' => '',
        'active' => 'Активний',
        'inactive' => 'Неактивний',
    ),

    'campainglog_activity_type_dom' => array(
        '' => '',
        'targeted' => 'Відправлені',
        'send error' => 'Не доставлені (ін.)',
        'invalid email' => 'Не доставлені (неверн. адр).',
        'link' => 'Переходи по посиланнях',
        'viewed' => 'Переглянуті',
        'removed' => 'Відписавші',
        'lead' => 'Створені попередніми конт.',
        'contact' => 'Створені контакти',
        'blocked' => 'Заблоковані',
    ),

    'campainglog_target_type_dom' => array(
        'Contacts' => 'Контакти',
        'Users' => 'Користувачі',
        'Prospects' => 'Огляд потенційних клієнтів',
        'Leads' => 'Попередні контакти',
        'Accounts' => 'Контрагенти',
    ),
    'merge_operators_dom' => array(
        'like' => 'Містить',
        'exact' => 'Точна відповідність',
        'start' => 'Починається з',
    ),

    'custom_fields_importable_dom' => array(
        'true' => 'Так',
        'false' => 'Ні',
        'required' => 'Необхідне',
    ),

    'custom_fields_merge_dup_dom' => array(
        0 => 'Відключений',
        1 => 'Включений',
    ),

    'projects_priority_options' => array(
        'high' => 'Високий',
        'medium' => 'Середній',
        'low' => 'Низький',
    ),

    'projects_status_options' => array(
        'notstarted' => 'Проект',
        'inprogress' => 'Готується до підписання',
        'completed' => 'Завершено',
    ),
    // strings to pass to Flash charts
    'chart_strings' => array(
        'expandlegend' => 'Розгорнути легенду',
        'collapselegend' => 'Згорнути легенду',
        'clickfordrilldown' => 'Натисніть для перегляду деталей',
        'detailview' => 'Докладно...',
        'piechart' => 'Кругова діаграма',
        'groupchart' => 'Група діаграм',
        'stackedchart' => 'Діаграма з накопиченням',
        'barchart' => 'Гістограма',
        'horizontalbarchart' => 'Горизонтальна гістограма',
        'linechart' => 'Лінійна діаграма',
        'noData' => 'Немає даних',
        'print' => 'Друк',
        'pieWedgeName' => 'секції',
    ),
    'release_status_dom' => array(
        'Active' => 'Активна',
        'Inactive' => 'Неактивна',
    ),
    'email_settings_for_ssl' => array(
        '0' => '',
        '1' => 'SSL',
        '2' => 'TLS',
    ),
    'import_enclosure_options' => array(
        '\'' => 'Одинарні лапки (&#39;)',
        '"' => 'Подвійні лапки (&#34;)',
        '' => '--Не вибрано--',
        'other' => 'Інший:',
    ),
    'import_delimeter_options' => array(
        ',' => ',',
        ';' => ';',
        '\t' => '\t',
        '.' => '.',
        ':' => ':',
        '|' => '|',
        'other' => 'Інший:',
    ),
    'link_target_dom' => array(
        '_blank' => 'Нова сторінка',
        '_self' => 'Поточна сторінка',
    ),
    'dashlet_auto_refresh_options' => array(
        '-1' => 'Не авто-оновлювати',
        '30' => 'Кожні 30 секунд',
        '60' => 'Кожну хвилину',
        '180' => 'Кожні 3 хвилини',
        '300' => 'Кожні 5 хвилин',
        '600' => 'Кожні 10 хвилин',
    ),
    'dashlet_auto_refresh_options_admin' => array(
        '-1' => 'Ніколи',
        '30' => 'Кожні 30 секунд',
        '60' => 'Кожну хвилину',
        '180' => 'Кожні 3 хвилини',
        '300' => 'Кожні 5 хвилин',
        '600' => 'Кожні 10 хвилин',
    ),
    'date_range_search_dom' => array(
        '=' => 'Рівно',
        'not_equal' => 'Нерівно',
        'greater_than' => 'Більше, ніж',
        'less_than' => 'Менше, ніж',
        'last_7_days' => 'Минулі 7 днів',
        'next_7_days' => 'Наступні 7 днів',
        'last_30_days' => 'Останні 30 днів',
        'next_30_days' => 'Наступні 30 днів',
        'last_month' => 'Минулий місяць',
        'this_month' => 'Поточний місяць',
        'next_month' => 'Наступний місяць',
        'last_year' => 'Минулий рік',
        'this_year' => 'Поточний рік',
        'next_year' => 'Наступний рік',
        'between' => 'Між',
    ),
    'numeric_range_search_dom' => array(
        '=' => 'Рівно',
        'not_equal' => 'Не дорівнює',
        'greater_than' => 'Більше, ніж',
        'greater_than_equals' => 'Більше або дорівнює',
        'less_than' => 'Менше ніж',
        'less_than_equals' => 'Менше або дорівнює',
        'between' => 'Між',
    ),
    'lead_conv_activity_opt' => array(
        'copy' => 'Копіювати',
        'move' => 'Перемістити',
        'donothing' => 'Нічого не робити',
    ),
);

$app_strings = array(
    'LBL_TOUR_NEXT' => 'Далі',
    'LBL_TOUR_SKIP' => 'Пропустити',
    'LBL_TOUR_BACK' => 'Назад',
    'LBL_TOUR_TAKE_TOUR' => 'Почати знайомство з системою',
    'LBL_MOREDETAIL' => 'Детальніше' /*for 508 compliance fix*/,
    'LBL_EDIT_INLINE' => 'Inline редагування' /*for 508 compliance fix*/,
    'LBL_VIEW_INLINE' => 'Перегляд' /*for 508 compliance fix*/,
    'LBL_BASIC_SEARCH' => 'Фільтр' /*for 508 compliance fix*/,
    'LBL_Blank' => ' ' /*for 508 compliance fix*/,
    'LBL_ID_FF_ADD' => 'Додати' /*for 508 compliance fix*/,
    'LBL_ID_FF_ADD_EMAIL' => 'Add Email Address' /*for 508 compliance fix*/,
    'LBL_HIDE_SHOW' => 'Показати/Приховати' /*for 508 compliance fix*/,
    'LBL_DELETE_INLINE' => 'Видалити' /*for 508 compliance fix*/,
    'LBL_ID_FF_CLEAR' => 'Очистити' /*for 508 compliance fix*/,
    'LBL_ID_FF_VCARD' => 'Візитна картка (vCard)' /*for 508 compliance fix*/,
    'LBL_ID_FF_REMOVE' => 'Видалити' /*for 508 compliance fix*/,
    'LBL_ID_FF_REMOVE_EMAIL' => 'Remove Email Address' /*for 508 compliance fix*/,
    'LBL_ID_FF_OPT_OUT' => 'Opt Out',
    'LBL_ID_FF_INVALID' => 'Make Invalid',
    'LBL_ADD' => 'Додати' /*for 508 compliance fix*/,
    'LBL_COMPANY_LOGO' => 'Логотип компанії' /*for 508 compliance fix*/,
    'LBL_CONNECTORS_POPUPS' => 'Підключення',
    'LBL_CLOSEINLINE' => 'Закрити',
    'LBL_VIEWINLINE' => 'Перегляд',
    'LBL_INFOINLINE' => 'Інформація',
    'LBL_PRINT' => 'Друк',
    'LBL_HELP' => 'Довідка',
    'LBL_ID_FF_SELECT' => 'Вибрати',
    'DEFAULT' => 'Базові', //Can be translated in all caps. This string will be used by SuiteP template menu actions
    'LBL_SORT' => 'Сортувати',
    'LBL_EMAIL_SMTP_SSL_OR_TLS' => 'Включити шифрування (SSL або TLS)',
    'LBL_NO_ACTION' => 'There is no action by that name: %s',
    'LBL_NO_SHORTCUT_MENU' => 'Немає доступних дій',
    'LBL_NO_DATA' => 'Немає даних',

    'LBL_ERROR_UNDEFINED_BEHAVIOR' => 'An unexpected error has occurred.', //PR 3669
    'LBL_ERROR_UNHANDLED_VALUE' => 'A value has not been handled correctly which is preventing a process from continuing.', //PR 3669
    'LBL_ERROR_UNUSABLE_VALUE' => 'An unusable value was found which is preventing a process from continuing.', //PR 3669
    'LBL_ERROR_INVALID_TYPE' => 'The type of a value is different than what was expected.', //PR 3669

    'LBL_ROUTING_FLAGGED' => 'вказати значення (прапор)',
    'LBL_NOTIFICATIONS' => 'Повідомлення',

    'LBL_ROUTING_TO' => 'у',
    'LBL_ROUTING_TO_ADDRESS' => 'адреси',
    'LBL_ROUTING_WITH_TEMPLATE' => 'з шаблону',

    'NTC_OVERWRITE_ADDRESS_PHONE_CONFIRM' => 'Це актуальні записи в значеннях полів офісного телефону та адреси. Перезаписати ці значення парами значень офісного телефону та адреси вибраного облікового запису, натисніть "ОК". Залишити поточні значення, натисніть "Cancel".',
    'LBL_DROP_HERE' => '[Перемістити сюди]',
    'LBL_EMAIL_ACCOUNTS_GMAIL_DEFAULTS' => 'Використовувати налаштування Gmail™',
    'LBL_EMAIL_ACCOUNTS_NAME' => 'Назва',
    'LBL_EMAIL_ACCOUNTS_OUTBOUND' => 'Параметри сервера вихідної пошти',
    'LBL_EMAIL_ACCOUNTS_SMTPPASS' => 'Пароль до SMTP',
    'LBL_EMAIL_ACCOUNTS_SMTPPORT' => 'SMTP-порт',
    'LBL_EMAIL_ACCOUNTS_SMTPSERVER' => 'Сервер вихідної пошти',
    'LBL_EMAIL_ACCOUNTS_SMTPUSER' => 'SMTP-логін',
    'LBL_EMAIL_ACCOUNTS_SMTPDEFAULT' => 'По замовчуванню',
    'LBL_EMAIL_WARNING_MISSING_USER_CREDS' => 'Увага: Відсутня ім\'я користувача та пароль для вихідного облікового запису електронної пошти.',
    'LBL_EMAIL_ACCOUNTS_SUBTITLE' => 'Налаштувати облікові записи електронної пошти, щоб переглядати вхідні повідомлення електронної пошти з Ваших облікових записів електронної пошти.',
    'LBL_EMAIL_ACCOUNTS_OUTBOUND_SUBTITLE' => 'Надати інформацію щодо поштового серверу SMTP, яка використовується для вихідної пошти в поштових облікових записах.',

    'LBL_EMAIL_ADDRESS_BOOK_ADD' => 'Готово',
    'LBL_EMAIL_ADDRESS_BOOK_CLEAR' => 'Очистити',
    'LBL_EMAIL_ADDRESS_BOOK_ADD_TO' => 'Кому:',
    'LBL_EMAIL_ADDRESS_BOOK_ADD_CC' => 'Копія:',
    'LBL_EMAIL_ADDRESS_BOOK_ADD_BCC' => 'Прихована копія:',
    'LBL_EMAIL_ADDRESS_BOOK_ADRRESS_TYPE' => 'Кому/Копія/Прихована копія',
    'LBL_EMAIL_ADDRESS_BOOK_EMAIL_ADDR' => 'Адреса E-mail',
    'LBL_EMAIL_ADDRESS_BOOK_FILTER' => 'Фільтер',
    'LBL_EMAIL_ADDRESS_BOOK_NAME' => 'Назва',
    'LBL_EMAIL_ADDRESS_BOOK_NOT_FOUND' => 'Адресу не знайдено',
    'LBL_EMAIL_ADDRESS_BOOK_SAVE_AND_ADD' => 'Зберегти та додати до адресної книги',
    'LBL_EMAIL_ADDRESS_BOOK_SELECT_TITLE' => 'Виберіть одержувачів електронної пошти',
    'LBL_EMAIL_ADDRESS_BOOK_TITLE' => 'Адресна книга',
    'LBL_EMAIL_REPORTS_TITLE' => 'Звіти',
    'LBL_EMAIL_REMOVE_SMTP_WARNING' => 'Warning! The outbound account you are trying to delete is associated to an existing inbound account. Are you sure you want to continue?',
    'LBL_EMAIL_ADDRESSES' => 'E-mail',
    'LBL_EMAIL_ADDRESS_PRIMARY' => 'Адреса E-mail',
    'LBL_EMAIL_ARCHIVE_TO_SUGAR' => 'Імпорт до SuiteCRM',
    'LBL_EMAIL_ASSIGNMENT' => 'Призначення відповідального',
    'LBL_EMAIL_ATTACH_FILE_TO_EMAIL' => 'Вкласти файл до листа',
    'LBL_EMAIL_ATTACHMENT' => 'Вкладений файл',
    'LBL_EMAIL_ATTACHMENTS' => 'З Вашого комп\'ютера',
    'LBL_EMAIL_ATTACHMENTS2' => 'З документів, що знаходяться в SuiteCRM',
    'LBL_EMAIL_ATTACHMENTS3' => 'Шаблон вкладення',
    'LBL_EMAIL_ATTACHMENTS_FILE' => 'Файл',
    'LBL_EMAIL_ATTACHMENTS_DOCUMENT' => 'Документ',
    'LBL_EMAIL_BCC' => 'Прихована копія',
    'LBL_EMAIL_CANCEL' => 'Скасування',
    'LBL_EMAIL_CC' => 'Копія',
    'LBL_EMAIL_CHARSET' => 'Кодування',
    'LBL_EMAIL_CHECK' => 'Перевірити пошту',
    'LBL_EMAIL_CHECKING_NEW' => 'Перевірка наявності нових повідомлень електронної пошти',
    'LBL_EMAIL_CHECKING_DESC' => 'Будь ласка зачекайте... <br><br>Якщо це перша перевірка облікового запису, то вона може тривати деякий час.',
    'LBL_EMAIL_CLOSE' => 'Закрити',
    'LBL_EMAIL_COFFEE_BREAK' => 'Перевірка наявності нових повідомлень електронної пошти. <br><br>Велика кількість повідомлень потребує певного часу для обробки.',

    'LBL_EMAIL_COMPOSE' => 'E-mail',
    'LBL_EMAIL_COMPOSE_ERR_NO_RECIPIENTS' => 'Будь ласка, введіть одержувача(ів) для цього повідомлення.',
    'LBL_EMAIL_COMPOSE_NO_BODY' => 'Тіло цього листа порожнє. Надіслати цей лист?',
    'LBL_EMAIL_COMPOSE_NO_SUBJECT' => 'У цього e-mail-повідомлення немає теми. Надіслати його таким?',
    'LBL_EMAIL_COMPOSE_NO_SUBJECT_LITERAL' => '(немає теми)',
    'LBL_EMAIL_COMPOSE_INVALID_ADDRESS' => 'Будь ласка, введіть дійсну адресу електронної пошти для до полів Копія і Прихована Копія',

    'LBL_EMAIL_CONFIRM_CLOSE' => 'Закрити цей лист без збереження?',
    'LBL_EMAIL_CONFIRM_DELETE_SIGNATURE' => 'Ви дійсно бажаєте видалити цей підпис?',

    'LBL_EMAIL_SENT_SUCCESS' => 'Email sent',

    'LBL_EMAIL_CREATE_NEW' => '--Створити при збереженні--',
    'LBL_EMAIL_MULT_GROUP_FOLDER_ACCOUNTS' => 'Кілька',
    'LBL_EMAIL_MULT_GROUP_FOLDER_ACCOUNTS_EMPTY' => 'Порожньо',
    'LBL_EMAIL_DATE_SENT_BY_SENDER' => 'Відправленно',
    'LBL_EMAIL_DATE_TODAY' => 'Сьогодні',
    'LBL_EMAIL_DELETE' => 'Видалити',
    'LBL_EMAIL_DELETE_CONFIRM' => 'Видалити вибрані повідомлення?',
    'LBL_EMAIL_DELETE_SUCCESS' => 'Повідомлення успішно видалені.',
    'LBL_EMAIL_DELETING_MESSAGE' => 'Видалення повідомлення',
    'LBL_EMAIL_DETAILS' => 'Подробиці',

    'LBL_EMAIL_EDIT_CONTACT_WARN' => 'Тільки основну адресу буде використано при роботі з контактами.',

    'LBL_EMAIL_EMPTYING_TRASH' => 'Очистити кошик',
    'LBL_EMAIL_DELETING_OUTBOUND' => 'Видалення сервера вихідної пошти',
    'LBL_EMAIL_CLEARING_CACHE_FILES' => 'Очищення кешу пошти',

    'LBL_EMAIL_ERROR_ADD_GROUP_FOLDER' => 'Ім\'я папки повинне бути унікальним і не пустим. Будь ласка, спробуйте ще раз.',
    'LBL_EMAIL_ERROR_DELETE_GROUP_FOLDER' => 'Не можна видалити папку. Папка або її вміст містить повідомлення  або зв\'язаний обліковий завис email',
    'LBL_EMAIL_ERROR_CANNOT_FIND_NODE' => 'Не вдалося визначити цільову папку з контексту.  Спробуй ще раз.',
    'LBL_EMAIL_ERROR_CHECK_IE_SETTINGS' => 'Будь ласка перевірте налаштування.',
    'LBL_EMAIL_ERROR_DESC' => 'Виявлені помилки: ',
    'LBL_EMAIL_DELETE_ERROR_DESC' => 'У Вас немає доступу до цієї сторінки. Зв&#039;яжіться з вашим системним адміністратором для отримання відповідних прав.',
    'LBL_EMAIL_ERROR_DUPE_FOLDER_NAME' => 'Імена папок SuiteCRM повинні бути унікальними.',
    'LBL_EMAIL_ERROR_EMPTY' => 'Будь ласка, введіть слово для пошуку.',
    'LBL_EMAIL_ERROR_GENERAL_TITLE' => 'Відбулася помилка',
    'LBL_EMAIL_ERROR_MESSAGE_DELETED' => 'Повідомлення видалено з сервера',
    'LBL_EMAIL_ERROR_IMAP_MESSAGE_DELETED' => 'Either message Removed from Server or moved to a different folder',
    'LBL_EMAIL_ERROR_MAILSERVERCONNECTION' => 'Не вдалося підключитися до поштового сервера. Будь ласка, зверніться до адміністратора',
    'LBL_EMAIL_ERROR_MOVE' => 'Moving email between servers and/or mail accounts is not supported at this time.',
    'LBL_EMAIL_ERROR_MOVE_TITLE' => 'Move Error',
    'LBL_EMAIL_ERROR_NAME' => 'A name is required.',
    'LBL_EMAIL_ERROR_FROM_ADDRESS' => 'Потрібно вказати від кого буде відправлено листа. Будь ласка, введіть діючу адресу електронної пошти.',
    'LBL_EMAIL_ERROR_NO_FILE' => 'Будь ласка, надайте файл.',
    'LBL_EMAIL_ERROR_SERVER' => 'Необхідна адреса поштового сервера.',
    'LBL_EMAIL_ERROR_SAVE_ACCOUNT' => 'Обліковий запис пошти може не бути збережений.',
    'LBL_EMAIL_ERROR_TIMEOUT' => 'An error has occurred while communicating with the mail server.',
    'LBL_EMAIL_ERROR_USER' => 'Потрібно ім\'я для входу.',
    'LBL_EMAIL_ERROR_PORT' => 'Потрібен порт поштового сервера.',
    'LBL_EMAIL_ERROR_PROTOCOL' => 'A server protocol is required.',
    'LBL_EMAIL_ERROR_MONITORED_FOLDER' => 'Monitored Folder is required.',
    'LBL_EMAIL_ERROR_TRASH_FOLDER' => 'Trash Folder is required.',
    'LBL_EMAIL_ERROR_VIEW_RAW_SOURCE' => 'Ця інформація недоступна',
    'LBL_EMAIL_ERROR_NO_OUTBOUND' => 'Не визначено сервер для вихідної пошти.',
    'LBL_EMAIL_ERROR_SENDING' => 'Error Sending Email. Please contact your administrator for assistance.',
    'LBL_EMAIL_FOLDERS' => SugarThemeRegistry::current()->getImage('icon_email_folder', 'align=absmiddle border=0',        null, null, '.gif', '') . 'Теки',
    'LBL_EMAIL_FOLDERS_SHORT' => SugarThemeRegistry::current()->getImage('icon_email_folder',        'align=absmiddle border=0', null, null, '.gif', ''),
    'LBL_EMAIL_FOLDERS_ADD' => 'Додати',
    'LBL_EMAIL_FOLDERS_ADD_DIALOG_TITLE' => 'Додати нову теку',
    'LBL_EMAIL_FOLDERS_RENAME_DIALOG_TITLE' => 'Перейменувати теку',
    'LBL_EMAIL_FOLDERS_ADD_NEW_FOLDER' => 'Зберегти',
    'LBL_EMAIL_FOLDERS_ADD_THIS_TO' => 'Додати цю теку до',
    'LBL_EMAIL_FOLDERS_CHANGE_HOME' => 'Ця тека не може бути змінена',
    'LBL_EMAIL_FOLDERS_DELETE_CONFIRM' => 'Are you sure you would like to delete this folder?\nThis process cannot be reversed.\nFolder deletions will cascade to all contained folders.',
    'LBL_EMAIL_FOLDERS_NEW_FOLDER' => 'Назва нової теки',
    'LBL_EMAIL_FOLDERS_NO_VALID_NODE' => 'Будь ласка, виберіть теку перед виконанням цієї дії.',
    'LBL_EMAIL_FOLDERS_TITLE' => 'Керування теками',

    'LBL_EMAIL_FORWARD' => 'Переслати',
    'LBL_EMAIL_DELIMITER' => '::;::',
    'LBL_EMAIL_DOWNLOAD_STATUS' => 'Downloaded [[count]] of [[total]] emails',
    'LBL_EMAIL_FROM' => 'Від',
    'LBL_EMAIL_GROUP' => 'групова',
    'LBL_EMAIL_UPPER_CASE_GROUP' => 'Група',
    'LBL_EMAIL_HOME_FOLDER' => 'Головна',
    'LBL_EMAIL_IE_DELETE' => 'Видалення облікового запису електронної пошти',
    'LBL_EMAIL_IE_DELETE_SIGNATURE' => 'Видалення підпису',
    'LBL_EMAIL_IE_DELETE_CONFIRM' => 'Ви впевнені, що ви хотіли б видалити цей обліковий запис електронної пошти?',
    'LBL_EMAIL_IE_DELETE_SUCCESSFUL' => 'Видалення успішно.',
    'LBL_EMAIL_IE_SAVE' => 'Збереження інформації щодо облікового запису пошти',
    'LBL_EMAIL_IMPORTING_EMAIL' => 'Імпортування листів електронної пошти',
    'LBL_EMAIL_IMPORT_EMAIL' => 'Import into SuiteCRM',
    'LBL_EMAIL_IMPORT_SETTINGS' => 'Імпорт налаштувань',
    'LBL_EMAIL_INVALID' => 'Недійсне',
    'LBL_EMAIL_LOADING' => 'Завантаження...',
    'LBL_EMAIL_MARK' => 'Помітити',
    'LBL_EMAIL_MARK_FLAGGED' => 'Позначити як',
    'LBL_EMAIL_MARK_READ' => 'Як прочитане',
    'LBL_EMAIL_MARK_UNFLAGGED' => 'Як не позначене',
    'LBL_EMAIL_MARK_UNREAD' => 'Як непрочитане',
    'LBL_EMAIL_ASSIGN_TO' => 'Assign To',

    'LBL_EMAIL_MENU_ADD_FOLDER' => 'Створити теку',
    'LBL_EMAIL_MENU_COMPOSE' => 'Написати листа',
    'LBL_EMAIL_MENU_DELETE_FOLDER' => 'Видалити теку',
    'LBL_EMAIL_MENU_EMPTY_TRASH' => 'Очистити кошик',
    'LBL_EMAIL_MENU_SYNCHRONIZE' => 'Синхронізувати',
    'LBL_EMAIL_MENU_CLEAR_CACHE' => 'Видалити файли кешу',
    'LBL_EMAIL_MENU_REMOVE' => 'Видалити',
    'LBL_EMAIL_MENU_RENAME_FOLDER' => 'Перейменувати теку',
    'LBL_EMAIL_MENU_RENAMING_FOLDER' => 'Перейменування теки',
    'LBL_EMAIL_MENU_MAKE_SELECTION' => 'Будь ласка, зробіть необхідний вибір перед виконанням цієї операції.',

    'LBL_EMAIL_MENU_HELP_ADD_FOLDER' => 'Створити папку (на віддаленому комп\'ютері або в SuiteCRM)',
    'LBL_EMAIL_MENU_HELP_DELETE_FOLDER' => 'Видалити теку (на віддаленому комп\'ютері або в SuiteCRM)',
    'LBL_EMAIL_MENU_HELP_EMPTY_TRASH' => 'Очищує всі кошики для вашого облікового запису',
    'LBL_EMAIL_MENU_HELP_MARK_READ' => 'Відмітити повідомлення як прочитане(-і)',
    'LBL_EMAIL_MENU_HELP_MARK_UNFLAGGED' => 'Видалити відмітку з повідомлення(ь)',
    'LBL_EMAIL_MENU_HELP_RENAME_FOLDER' => 'Перейменувати папку (на віддаленому комп\'ютері або в SuiteCRM)',

    'LBL_EMAIL_MESSAGES' => 'повідомлення',

    'LBL_EMAIL_ML_NAME' => 'Назва списку розсилки',
    'LBL_EMAIL_ML_ADDRESSES_1' => 'Вибрані адреси розсилки',
    'LBL_EMAIL_ML_ADDRESSES_2' => 'Доступні адреси розсилки',

    'LBL_EMAIL_MULTISELECT' => 'Утримуйте <b>Ctrl</b>, щоб вибрати кілька позиції< br />(<b>CMD</b> для користувачів Mac)',

    'LBL_EMAIL_NO' => 'Ні',
    'LBL_EMAIL_NOT_SENT' => 'Система не в змозі виконати ваш запит. Будь ласка, зв&#039;яжіться з системним адміністратором.',

    'LBL_EMAIL_OK' => 'Гаразд',
    'LBL_EMAIL_ONE_MOMENT' => 'Будь ласка, зачекайте...',
    'LBL_EMAIL_OPEN_ALL' => 'Відкрити декілька повідомлень',
    'LBL_EMAIL_OPTIONS' => 'Опції',
    'LBL_EMAIL_QUICK_COMPOSE' => 'Швидке створення повідомлення',
    'LBL_EMAIL_OPT_OUT' => 'Відписавші',
    'LBL_EMAIL_OPT_OUT_AND_INVALID' => 'Не писати/Не вірна адреса',
    'LBL_EMAIL_PERFORMING_TASK' => 'Виконання завдання',
    'LBL_EMAIL_PRIMARY' => 'Основний',
    'LBL_EMAIL_PRINT' => 'Друк',

    'LBL_EMAIL_QC_BUGS' => 'Відстеження помилок',
    'LBL_EMAIL_QC_CASES' => 'Звернення',
    'LBL_EMAIL_QC_LEADS' => 'Попередній контакт',
    'LBL_EMAIL_QC_CONTACTS' => 'Контакт',
    'LBL_EMAIL_QC_TASKS' => 'Завдання',
    'LBL_EMAIL_QC_OPPORTUNITIES' => 'Угода',
    'LBL_EMAIL_QUICK_CREATE' => 'Швидке створення',

    'LBL_EMAIL_REBUILDING_FOLDERS' => 'Перебудова папок',
    'LBL_EMAIL_RELATE_TO' => 'Відноситься до',
    'LBL_EMAIL_VIEW_RELATIONSHIPS' => 'Перегляд відносин',
    'LBL_EMAIL_RECORD' => 'Дані email',
    'LBL_EMAIL_REMOVE' => 'Видалити',
    'LBL_EMAIL_REPLY' => 'Відповісти',
    'LBL_EMAIL_REPLY_ALL' => 'Відповісти всім',
    'LBL_EMAIL_REPLY_TO' => 'Для авто-відповідей',
    'LBL_EMAIL_RETRIEVING_MESSAGE' => 'Отримання повідомлення',
    'LBL_EMAIL_RETRIEVING_RECORD' => 'Отримання даниз запису електронної пошти',
    'LBL_EMAIL_SELECT_ONE_RECORD' => 'Будь ласка, виберіть лише один запис електронної пошти',
    'LBL_EMAIL_RETURN_TO_VIEW' => 'Повернутися до попереднього модуля?',
    'LBL_EMAIL_REVERT' => 'Revert',
    'LBL_EMAIL_RELATE_EMAIL' => 'Relate Email',

    'LBL_EMAIL_RULES_TITLE' => 'Rule Management',

    'LBL_EMAIL_SAVE' => 'Зберегти',
    'LBL_EMAIL_SAVE_AND_REPLY' => 'Зберегти та відповісти',
    'LBL_EMAIL_SAVE_DRAFT' => 'Зберегти чернетку',
    'LBL_EMAIL_DRAFT_SAVED' => 'Draft has been saved',

    'LBL_EMAIL_SEARCH' => SugarThemeRegistry::current()->getImage('Search', 'align=absmiddle border=0', null, null,    '.gif', ''),
    'LBL_EMAIL_SEARCH_SHORT' => SugarThemeRegistry::current()->getImage('Search', 'align=absmiddle border=0', null,        null, '.gif', ''),
    'LBL_EMAIL_SEARCH_DATE_FROM' => 'Дата від',
    'LBL_EMAIL_SEARCH_DATE_UNTIL' => 'Дата поки',
    'LBL_EMAIL_SEARCH_NO_RESULTS' => 'Відсутні результати, що відповідають введеним критеріям пошуку.',
    'LBL_EMAIL_SEARCH_RESULTS_TITLE' => 'Результати пошуку',

    'LBL_EMAIL_SELECT' => 'Вибрати',

    'LBL_EMAIL_SEND' => 'Відправити',
    'LBL_EMAIL_SENDING_EMAIL' => 'Відсилання електронної пошти',

    'LBL_EMAIL_SETTINGS' => 'Налаштування',
    'LBL_EMAIL_SETTINGS_ACCOUNTS' => 'Облікові записи електронної пошти',
    'LBL_EMAIL_SETTINGS_ADD_ACCOUNT' => 'Очистити форму',
    'LBL_EMAIL_SETTINGS_CHECK_INTERVAL' => 'Перевірити наявність нової пошти',
    'LBL_EMAIL_SETTINGS_FROM_ADDR' => 'Від (адреса)',
    'LBL_EMAIL_SETTINGS_FROM_TO_EMAIL_ADDR' => 'Адреса електронної пошти для тестових сповіщень:',
    'LBL_EMAIL_SETTINGS_FROM_NAME' => 'Від (ім&#039;я)',
    'LBL_EMAIL_SETTINGS_REPLY_TO_ADDR' => 'Відповідь на адресу',
    'LBL_EMAIL_SETTINGS_FULL_SYNC' => 'Синхронізуйте всі облікові записи електронної пошти',
    'LBL_EMAIL_TEST_NOTIFICATION_SENT' => 'Лист було надіслано на вказану адресу електронної пошти за допомогою встановлених параметрів вихідної пошти. Будь ласка перевірте, чи було повідомлення електронної пошти отримано для перевірки правильності налаштувань.',
    'LBL_EMAIL_SETTINGS_FULL_SYNC_WARN' => 'Виконати повну синхронізацію? \n Синхронізація великих поштових скриньок може тривати кілька хвилин.',
    'LBL_EMAIL_SUBSCRIPTION_FOLDER_HELP' => 'Натисніть клавішу Shift або клавішу Ctrl для вибору декількох папок.',
    'LBL_EMAIL_SETTINGS_GENERAL' => 'Основні налаштування',
    'LBL_EMAIL_SETTINGS_GROUP_FOLDERS_CREATE' => 'Create Group Folders',

    'LBL_EMAIL_SETTINGS_GROUP_FOLDERS_EDIT' => 'Edit Group Folder',

    'LBL_EMAIL_SETTINGS_NAME' => 'Ім\'я облікового запису пошти',
    'LBL_EMAIL_SETTINGS_REQUIRE_REFRESH' => 'Виберіть кількість листів на сторінку в папці Вхідні. Встановлення цього параметру може вимагати оновлення сторінки для того, щоб зміни вступили в силу.',
    'LBL_EMAIL_SETTINGS_RETRIEVING_ACCOUNT' => 'Retrieving Mail Account',
    'LBL_EMAIL_SETTINGS_SAVED' => 'The settings have been saved.',
    'LBL_EMAIL_SETTINGS_SEND_EMAIL_AS' => 'Send Plain Text Emails Only',
    'LBL_EMAIL_SETTINGS_SHOW_NUM_IN_LIST' => 'Emails per Page',
    'LBL_EMAIL_SETTINGS_TITLE_LAYOUT' => 'Visual Settings',
    'LBL_EMAIL_SETTINGS_TITLE_PREFERENCES' => 'Налаштування',
    'LBL_EMAIL_SETTINGS_USER_FOLDERS' => 'Available User Folders',
    'LBL_EMAIL_ERROR_PREPEND' => 'Помилка:',
    'LBL_EMAIL_INVALID_PERSONAL_OUTBOUND' => 'The outbound mail server selected for the mail account you are using is invalid. Check the settings or select a different mail server for the mail account.',
    'LBL_EMAIL_INVALID_SYSTEM_OUTBOUND' => 'An outgoing mail server is not configured to send emails. Please configure an outgoing mail server or select an outgoing mail server for the mail account that you are using in Settings >> Mail Account.',
    'LBL_DEFAULT_EMAIL_SIGNATURES' => 'Default Signature',
    'LBL_EMAIL_SIGNATURES' => 'Підписи',
    'LBL_SMTPTYPE_GMAIL' => 'Gmail',
    'LBL_SMTPTYPE_YAHOO' => 'Yahoo! Mail',
    'LBL_SMTPTYPE_EXCHANGE' => 'Microsoft Exchange',
    'LBL_SMTPTYPE_OTHER' => 'Інший',
    'LBL_EMAIL_SPACER_MAIL_SERVER' => '[Віддалені папки]',
    'LBL_EMAIL_SPACER_LOCAL_FOLDER' => '[папки SuiteCRM ]',
    'LBL_EMAIL_SUBJECT' => 'Тема',
    'LBL_EMAIL_SUCCESS' => 'Успішно',
    'LBL_EMAIL_SUGAR_FOLDER' => 'папки SuiteCRM',
    'LBL_EMAIL_TEMPLATE_EDIT_PLAIN_TEXT' => 'Шаблон повідомлення порожній',
    'LBL_EMAIL_TEMPLATES' => 'Шаблони',
    'LBL_EMAIL_TO' => 'Кому',
    'LBL_EMAIL_VIEW' => 'Перегляд',
    'LBL_EMAIL_VIEW_HEADERS' => 'Відображати заголовки',
    'LBL_EMAIL_VIEW_RAW' => 'Відобразити Raw email',
    'LBL_EMAIL_VIEW_UNSUPPORTED' => 'Ця функція не підтримується під час використання з POP3.',
    'LBL_DEFAULT_LINK_TEXT' => 'Текст посилання за замовчуванням.',
    'LBL_EMAIL_YES' => 'Так',
    'LBL_EMAIL_TEST_OUTBOUND_SETTINGS' => 'Відправити тестове повідомлення',
    'LBL_EMAIL_TEST_OUTBOUND_SETTINGS_SENT' => 'Тестове повідомлення надіслано',
    'LBL_EMAIL_MESSAGE_NO' => 'Повідомлення №', // Counter. Message number xx
    'LBL_EMAIL_IMPORT_SUCCESS' => 'Імпорт завершенно',
    'LBL_EMAIL_IMPORT_FAIL' => 'Імпортувати не вдалося, оскільки повідомлення вже імпортовано або видалено із сервера',

    'LBL_LINK_NONE' => 'Немає',
    'LBL_LINK_ALL' => 'Всі відповідні',
    'LBL_LINK_RECORDS' => 'Записи',
    'LBL_LINK_SELECT' => 'Вибрати',
    'LBL_LINK_ACTIONS' => 'Дії', //Can be translated in all caps. This string will be used by SuiteP template menu actions
    'LBL_CLOSE_ACTIVITY_HEADER' => 'Підтвердити',
    'LBL_CLOSE_ACTIVITY_CONFIRM' => 'Закрити цей #module #?',
    'LBL_INVALID_FILE_EXTENSION' => 'Неправильне розширення файлу',

    'ERR_AJAX_LOAD' => 'Сталася помилка:',
    'ERR_AJAX_LOAD_FAILURE' => 'Сталася помилка обробки вашого запиту, будь ласка, спробуйте ще раз.',
    'ERR_AJAX_LOAD_FOOTER' => 'If this error persists, please have your administrator disable Ajax for this module',
    'ERR_DECIMAL_SEP_EQ_THOUSANDS_SEP' => 'The decimal separator cannot use the same character as the thousands separator.\n\n  Please change the values.',
    'ERR_DELETE_RECORD' => 'Ви повинні вказати номер запису перед видаленням контакту.',
    'ERR_EXPORT_DISABLED' => 'Експорт заборонений.',
    'ERR_EXPORT_TYPE' => 'Помилка експорту ',
    'ERR_INVALID_EMAIL_ADDRESS' => 'не дійсна адреса електронної пошти.',
    'ERR_INVALID_FILE_REFERENCE' => 'Invalid File Reference',
    'ERR_NO_HEADER_ID' => 'Ця функція недоступна в цій темі.',
    'ERR_NOT_ADMIN' => 'Несанкціонований доступ до адміністрування.',
    'ERR_MISSING_REQUIRED_FIELDS' => 'Відсутнє обов\'язкове поле:',
    'ERR_INVALID_REQUIRED_FIELDS' => 'Неприпустиме обов\'язкове поле:',
    'ERR_INVALID_VALUE' => 'Неприпустиме значення:',
    'ERR_NO_SUCH_FILE' => 'Файл не існує в системі',
    'ERR_NO_SINGLE_QUOTE' => 'Не можна використовувати одинарні лапки для',
    'ERR_NOTHING_SELECTED' => 'Please make a selection before proceeding.',
    'ERR_SELF_REPORTING' => 'User cannot report to him or herself.',
    'ERR_SQS_NO_MATCH_FIELD' => 'No match for field: ',
    'ERR_SQS_NO_MATCH' => 'Відповідність відсутня',
    'ERR_ADDRESS_KEY_NOT_SPECIFIED' => 'Please specify \'key\' index in displayParams attribute for the Meta-Data definition',
    'ERR_EXISTING_PORTAL_USERNAME' => 'Помилка: Назву порталу вже призначено іншому контакту.',
    'ERR_COMPATIBLE_PRECISION_VALUE' => 'Значення в полі несумісне із необхідною точністю значення',
    'ERR_EXTERNAL_API_SAVE_FAIL' => 'An error occurred when trying to save to the external account.',
    'ERR_NO_DB' => 'Не вдалося підключитися до бази даних. Будь ласка, зверніться до suitecrm.log для деталей.',
    'ERR_DB_FAIL' => 'Збій бази даних. Будь ласка, зверніться до suitecrm.log для деталей.',
    'ERR_DB_VERSION' => 'SuiteCRM {0} Files May Only Be Used With A SuiteCRM {1} Database.',

    'LBL_ACCOUNT' => 'Контрагент',
    'LBL_ACCOUNTS' => 'Контрагенти',
    'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Заходи',
    'LBL_ACCUMULATED_HISTORY_BUTTON_LABEL' => 'View Summary',
    'LBL_ACCUMULATED_HISTORY_BUTTON_TITLE' => 'View Summary',
    'LBL_ADD_BUTTON' => 'Додати',
    'LBL_ADD_DOCUMENT' => 'Додати документ',
    'LBL_ADD_TO_PROSPECT_LIST_BUTTON_KEY' => 'L',
    'LBL_ADD_TO_PROSPECT_LIST_BUTTON_LABEL' => 'Додати до цільового списку',
    'LBL_ADD_TO_PROSPECT_LIST_BUTTON_LABEL_ACCOUNTS_CONTACTS' => 'Додати контакти до цільового списку',
    'LBL_ADDITIONAL_DETAILS_CLOSE_TITLE' => 'Click to Close',
    'LBL_ADDITIONAL_DETAILS' => 'Додаткова інформація',
    'LBL_ADMIN' => 'Адміністрування',
    'LBL_ALT_HOT_KEY' => '',
    'LBL_ARCHIVE' => 'Архів',
    'LBL_ASSIGNED_TO_USER' => 'Відповідальний (а)',
    'LBL_ASSIGNED_TO' => 'Відповідальний (- а):',
    'LBL_BACK' => 'Назад',
    'LBL_BILLING_ADDRESS' => 'Адреса для доставки рахунків',
    'LBL_BROWSER_TITLE' => 'SuiteCRM - Open Source CRM',
    'LBL_BUGS' => 'Відстеження помилок',
    'LBL_BY' => 'by',
    'LBL_CALLS' => 'Дзвінки',
    'LBL_CAMPAIGNS_SEND_QUEUED' => 'Send Queued Campaign Emails',
    'LBL_SUBMIT_BUTTON_LABEL' => 'Відправити',
    'LBL_CASE' => 'Звернення',
    'LBL_CASES' => 'Звернення',
    'LBL_CHANGE_PASSWORD' => 'Змінити пароль',
    'LBL_CHARSET' => 'UTF-8',
    'LBL_CHECKALL' => 'Відмітити всі',
    'LBL_CITY' => 'Місто',
    'LBL_CLEAR_BUTTON_LABEL' => 'Очистити',
    'LBL_CLEAR_BUTTON_TITLE' => 'Очистити',
    'LBL_CLEARALL' => 'Очистити все',
    'LBL_CLOSE_BUTTON_TITLE' => 'Закрити', // As in closing a task
    'LBL_CLOSE_AND_CREATE_BUTTON_LABEL' => 'Закрити і створити новий', // As in closing a task
    'LBL_CLOSE_AND_CREATE_BUTTON_TITLE' => 'Закрити і створити новий', // As in closing a task
    'LBL_OPEN_ITEMS' => 'Open Items:',
    'LBL_COMPOSE_EMAIL_BUTTON_LABEL' => 'Написати листа',
    'LBL_COMPOSE_EMAIL_BUTTON_TITLE' => 'Написати листа',
    'LBL_SEARCH_DROPDOWN_YES' => 'Так',
    'LBL_SEARCH_DROPDOWN_NO' => 'Ні',
    'LBL_CONTACT_LIST' => 'Список контактів',
    'LBL_CONTACT' => 'Контакт',
    'LBL_CONTACTS' => 'Контакти',
    'LBL_CONTRACT' => 'Договір',
    'LBL_CONTRACTS' => 'Контракти',
    'LBL_COUNTRY' => 'Країна:',
    'LBL_CREATE_BUTTON_LABEL' => 'Створити', //Can be translated in all caps. This string will be used by SuiteP template menu actions
    'LBL_CREATED_BY_USER' => 'Створено користувачем',
    'LBL_CREATED_USER' => 'Створено користувачем',
    'LBL_CREATED' => 'Створено',
    'LBL_CURRENT_USER_FILTER' => 'My Items:',
    'LBL_CURRENCY' => 'Валюта:',
    'LBL_DOCUMENTS' => 'Документи',
    'LBL_DATE_ENTERED' => 'Дата створення:',
    'LBL_DATE_MODIFIED' => 'Дата зміни:',
    'LBL_EDIT_BUTTON' => 'Змінити',
    'LBL_DUPLICATE_BUTTON' => 'Дублювати',
    'LBL_DELETE_BUTTON' => 'Видалити',
    'LBL_DELETE' => 'Видалити',
    'LBL_DELETED' => 'Видалено',
    'LBL_DIRECT_REPORTS' => 'Підлеглі',
    'LBL_DONE_BUTTON_LABEL' => 'Готово',
    'LBL_DONE_BUTTON_TITLE' => 'Готово',
    'LBL_FAVORITES' => 'Обране',
    'LBL_VCARD' => 'Візитна картка (vCard)',
    'LBL_EMPTY_VCARD' => 'Виберіть файл візитівки vCard',
    'LBL_EMPTY_REQUIRED_VCARD' => 'vCard does not have all the required fields for this module. Please refer to suitecrm.log for details.',
    'LBL_VCARD_ERROR_FILESIZE' => 'The uploaded file exceeds the 30000 bytes size limit which was specified in the HTML form.',
    'LBL_VCARD_ERROR_DEFAULT' => 'There was an error uploading the vCard file. Please refer to suitecrm.log for details.',
    'LBL_IMPORT_VCARD' => 'Import vCard:',
    'LBL_IMPORT_VCARD_BUTTON_LABEL' => 'Імпортування vCard',
    'LBL_IMPORT_VCARD_BUTTON_TITLE' => 'Імпортування vCard',
    'LBL_VIEW_BUTTON' => 'Перегляд',
    'LBL_EMAIL_PDF_BUTTON_LABEL' => 'Email as PDF',
    'LBL_EMAIL_PDF_BUTTON_TITLE' => 'Email as PDF',
    'LBL_EMAILS' => 'Електронні листи',
    'LBL_EMPLOYEES' => 'Співробітники',
    'LBL_ENTER_DATE' => 'Enter Date',
    'LBL_EXPORT' => 'Експортувати',
    'LBL_FAVORITES_FILTER' => 'My Favorites:',
    'LBL_GO_BUTTON_LABEL' => 'Перехід',
    'LBL_HIDE' => 'Hide',
    'LBL_ID' => 'ID',
    'LBL_IMPORT' => 'Імпорт',
    'LBL_IMPORT_STARTED' => 'Import Started: ',
    'LBL_LAST_VIEWED' => 'Recently Viewed',
    'LBL_LEADS' => 'Попередні контакти',
    'LBL_LESS' => 'less',
    'LBL_CAMPAIGN' => 'Маркетингова кампанія:',
    'LBL_CAMPAIGNS' => 'Маркетингові кампанії',
    'LBL_CAMPAIGNLOG' => 'Журнал маркетингових кампаній',
    'LBL_CAMPAIGN_CONTACT' => 'Маркетингові кампанії',
    'LBL_CAMPAIGN_ID' => 'campaign_id',
    'LBL_CAMPAIGN_NONE' => 'Немає',
    'LBL_THEME' => 'Тема',
    'LBL_FOUND_IN_RELEASE' => 'Found In Release',
    'LBL_FIXED_IN_RELEASE' => 'Fixed In Release',
    'LBL_LIST_ACCOUNT_NAME' => 'Контрагент',
    'LBL_LIST_ASSIGNED_USER' => 'Користувач',
    'LBL_LIST_CONTACT_NAME' => 'Контакт',
    'LBL_LIST_CONTACT_ROLE' => 'Contact Role',
    'LBL_LIST_DATE_ENTERED' => 'Дата створення',
    'LBL_LIST_EMAIL' => 'E-mail',
    'LBL_LIST_NAME' => 'Назва',
    'LBL_LIST_OF' => 'з',
    'LBL_LIST_PHONE' => 'Тел.',
    'LBL_LIST_RELATED_TO' => 'Відноситься до',
    'LBL_LIST_USER_NAME' => 'Логін',
    'LBL_LISTVIEW_NO_SELECTED' => 'Please select at least 1 record to proceed.',
    'LBL_LISTVIEW_TWO_REQUIRED' => 'Please select at least 2 records to proceed.',
    'LBL_LISTVIEW_OPTION_SELECTED' => 'Selected Records',
    'LBL_LISTVIEW_SELECTED_OBJECTS' => 'Selected: ',

    'LBL_LOCALE_NAME_EXAMPLE_FIRST' => 'Петро',
    'LBL_LOCALE_NAME_EXAMPLE_LAST' => 'Петров',
    'LBL_LOCALE_NAME_EXAMPLE_SALUTATION' => 'Д-р',
    'LBL_LOCALE_NAME_EXAMPLE_TITLE' => 'Code Monkey Extraordinaire',
    'LBL_LOGOUT' => 'Log Out',
    'LBL_PROFILE' => 'Profile',
    'LBL_MAILMERGE' => 'Злиття пошти',
    'LBL_MASS_UPDATE' => 'Групове оновлення',
    'LBL_NO_MASS_UPDATE_FIELDS_AVAILABLE' => 'There are no fields available for the Mass Update operation',
    'LBL_OPT_OUT_FLAG_PRIMARY' => 'Opt out Primary Email',
    'LBL_MEETINGS' => 'Зустрічі',
    'LBL_MEETING_GO_BACK' => 'Go back to the meeting',
    'LBL_MEMBERS' => 'Учасники',
    'LBL_MEMBER_OF' => 'Входить до',
    'LBL_MODIFIED_BY_USER' => 'Змінено користувачем',
    'LBL_MODIFIED_USER' => 'Змінено користувачем',
    'LBL_MODIFIED' => 'Автор змін',
    'LBL_MODIFIED_NAME' => 'Змінено користувачем',
    'LBL_MORE' => 'Більше',
    'LBL_MY_ACCOUNT' => 'Мої налаштування',
    'LBL_NAME' => 'Назва',
    'LBL_NEW_BUTTON_KEY' => 'N',
    'LBL_NEW_BUTTON_LABEL' => 'Створити',
    'LBL_NEW_BUTTON_TITLE' => 'Створити',
    'LBL_NEXT_BUTTON_LABEL' => 'Далі',
    'LBL_NONE' => '--Не вибрано--',
    'LBL_NOTES' => 'Нотатки',
    'LBL_OPPORTUNITIES' => 'Угоди',
    'LBL_OPPORTUNITY_NAME' => 'Угода:',
    'LBL_OPPORTUNITY' => 'Угода',
    'LBL_OR' => 'OR',
    'LBL_PANEL_OVERVIEW' => 'Перегляд', //Can be translated in all caps. This string will be used by SuiteP template menu actions
    'LBL_PANEL_ASSIGNMENT' => 'Інший', //Can be translated in all caps. This string will be used by SuiteP template menu actions
    'LBL_PANEL_ADVANCED' => 'Детальна інформація', //Can be translated in all caps. This string will be used by SuiteP template menu actions
    'LBL_PARENT_TYPE' => 'Тип початкового запису',
    'LBL_PERCENTAGE_SYMBOL' => '%',
    'LBL_POSTAL_CODE' => 'Індекс:',
    'LBL_PRIMARY_ADDRESS_CITY' => 'Основна адреса - місто:',
    'LBL_PRIMARY_ADDRESS_COUNTRY' => 'Основна адреса - країна:',
    'LBL_PRIMARY_ADDRESS_POSTALCODE' => 'Основна адреса - індекс:',
    'LBL_PRIMARY_ADDRESS_STATE' => 'Основна адреса - область:',
    'LBL_PRIMARY_ADDRESS_STREET_2' => 'Основна адреса - вулиця 2:',
    'LBL_PRIMARY_ADDRESS_STREET_3' => 'Основна адреса - вулиця 3:',
    'LBL_PRIMARY_ADDRESS_STREET' => 'Основна адреса - вулиця:',
    'LBL_PRIMARY_ADDRESS' => 'Основна адреса:',

    'LBL_PROSPECTS' => 'Prospects',
    'LBL_PRODUCTS' => 'Товари',
    'LBL_PROJECT_TASKS' => 'Проектні завдання',
    'LBL_PROJECTS' => 'Проекти',
    'LBL_QUOTES' => 'Комерційну пропозицію',

    'LBL_RELATED' => 'Відноситься до',
    'LBL_RELATED_RECORDS' => 'Пов\'язані записи',
    'LBL_REMOVE' => 'Видалити',
    'LBL_REPORTS_TO' => 'Керівник',
    'LBL_REQUIRED_SYMBOL' => '*',
    'LBL_REQUIRED_TITLE' => 'Позначає обов\'язкове поле',
    'LBL_EMAIL_DONE_BUTTON_LABEL' => 'Готово',
    'LBL_FULL_FORM_BUTTON_KEY' => 'L',
    'LBL_FULL_FORM_BUTTON_LABEL' => 'Повна форма',
    'LBL_FULL_FORM_BUTTON_TITLE' => 'Повна форма',
    'LBL_SAVE_NEW_BUTTON_LABEL' => 'Save & Create New',
    'LBL_SAVE_NEW_BUTTON_TITLE' => 'Save & Create New',
    'LBL_SAVE_OBJECT' => 'Save {0}',
    'LBL_SEARCH_BUTTON_KEY' => 'Q',
    'LBL_SEARCH_BUTTON_LABEL' => 'Знайти',
    'LBL_SEARCH_BUTTON_TITLE' => 'Знайти',
    'LBL_FILTER' => 'Фільтер',
    'LBL_SEARCH' => 'Знайти',
    'LBL_SEARCH_ALT' => '',
    'LBL_SEARCH_MORE' => 'більше',
    'LBL_UPLOAD_IMAGE_FILE_INVALID' => 'Invalid file format, only image file can be uploaded.',
    'LBL_SELECT_BUTTON_KEY' => 'T',
    'LBL_SELECT_BUTTON_LABEL' => 'Вибрати',
    'LBL_SELECT_BUTTON_TITLE' => 'Вибрати',
    'LBL_BROWSE_DOCUMENTS_BUTTON_LABEL' => 'Перегляд документів',
    'LBL_BROWSE_DOCUMENTS_BUTTON_TITLE' => 'Перегляд документів',
    'LBL_SELECT_CONTACT_BUTTON_LABEL' => 'Виберіть контакт',
    'LBL_SELECT_CONTACT_BUTTON_TITLE' => 'Виберіть контакт',
    'LBL_SELECT_REPORTS_BUTTON_LABEL' => 'SELECT FROM Reports',
    'LBL_SELECT_REPORTS_BUTTON_TITLE' => 'Виберіть звіти',
    'LBL_SELECT_USER_BUTTON_LABEL' => 'Виберіть Користувача',
    'LBL_SELECT_USER_BUTTON_TITLE' => 'Виберіть Користувача',
    // Clear buttons take up too many keys, lets default the relate and collection ones to be empty
    'LBL_ACCESSKEY_SELECT_USERS_TITLE' => 'Виберіть Користувача',
    'LBL_ACCESSKEY_SELECT_USERS_LABEL' => 'Виберіть Користувача',
    'LBL_ACCESSKEY_CLEAR_USERS_TITLE' => 'Clear User',
    'LBL_ACCESSKEY_CLEAR_USERS_LABEL' => 'Clear User',
    'LBL_SERVER_RESPONSE_RESOURCES' => 'Resources used to construct this page (queries, files)',
    'LBL_SERVER_RESPONSE_TIME_SECONDS' => 'секунд.',
    'LBL_SERVER_RESPONSE_TIME' => 'Час відповіді сервера:',
    'LBL_SERVER_MEMORY_BYTES' => 'байт',
    'LBL_SERVER_MEMORY_USAGE' => 'Server Memory Usage: {0} ({1})',
    'LBL_SERVER_MEMORY_LOG_MESSAGE' => 'Usage: - module: {0} - action: {1}',
    'LBL_SERVER_PEAK_MEMORY_USAGE' => 'Server Peak Memory Usage: {0} ({1})',
    'LBL_SHIPPING_ADDRESS' => 'Shipping Address',
    'LBL_SHOW' => 'Показати ',
    'LBL_STATE' => 'Область:', //Used for Case State, situation, condition
    'LBL_STATUS_UPDATED' => 'Your Status for this event has been updated!',
    'LBL_STATUS' => 'Статус:',
    'LBL_STREET' => 'Вулиця',
    'LBL_SUBJECT' => 'Тема',

    'LBL_INBOUNDEMAIL_ID' => 'Inbound Email ID',

    'LBL_SCENARIO_SALES' => 'Продажі',
    'LBL_SCENARIO_MARKETING' => 'Маркетинг',
    'LBL_SCENARIO_FINANCE' => 'Фінанси',
    'LBL_SCENARIO_SERVICE' => 'Service',
    'LBL_SCENARIO_PROJECT' => 'Project Management',

    'LBL_SCENARIO_SALES_DESCRIPTION' => 'This scenario facilitates the management of sales items',
    'LBL_SCENARIO_MAKETING_DESCRIPTION' => 'This scenario facilitates the management of marketing items',
    'LBL_SCENARIO_FINANCE_DESCRIPTION' => 'This scenario facilitates the management of finance related items',
    'LBL_SCENARIO_SERVICE_DESCRIPTION' => 'This scenario facilitates the management of service related items',
    'LBL_SCENARIO_PROJECT_DESCRIPTION' => 'This scenario faciliates the management of project related items',

    'LBL_SYNC' => 'Синхронізація',
    'LBL_TABGROUP_ALL' => 'Всі відповідні',
    'LBL_TABGROUP_ACTIVITIES' => 'Заходи',
    'LBL_TABGROUP_COLLABORATION' => 'Collaboration',
    'LBL_TABGROUP_MARKETING' => 'Маркетинг',
    'LBL_TABGROUP_OTHER' => 'Інший',
    'LBL_TABGROUP_SALES' => 'Продажі',
    'LBL_TABGROUP_SUPPORT' => 'Підтримка',
    'LBL_TASKS' => 'Завдання',
    'LBL_THOUSANDS_SYMBOL' => 'K',
    'LBL_TRACK_EMAIL_BUTTON_LABEL' => 'Відправити E-mail в архів',
    'LBL_TRACK_EMAIL_BUTTON_TITLE' => 'Відправити E-mail в архів',
    'LBL_UNDELETE_BUTTON_LABEL' => 'Undelete',
    'LBL_UNDELETE_BUTTON_TITLE' => 'Undelete',
    'LBL_UNDELETE_BUTTON' => 'Undelete',
    'LBL_UNDELETE' => 'Undelete',
    'LBL_UNSYNC' => 'Unsync',
    'LBL_UPDATE' => 'Оновити',
    'LBL_USER_LIST' => 'User List',
    'LBL_USERS' => 'Користувачі',
    'LBL_VERIFY_EMAIL_ADDRESS' => 'Checking for existing email entry...',
    'LBL_VERIFY_PORTAL_NAME' => 'Checking for existing portal name...',
    'LBL_VIEW_IMAGE' => 'view',

    'LNK_ABOUT' => 'Про нас',
    'LNK_ADVANCED_FILTER' => 'Розширений фільтр',
    'LNK_BASIC_FILTER' => 'Експрес фільтр',
    'LBL_ADVANCED_SEARCH' => 'Розширений фільтр',
    'LBL_QUICK_FILTER' => 'Quick Filter',
    'LNK_SEARCH_NONFTS_VIEW_ALL' => 'Показати всі',
    'LNK_CLOSE' => 'Закрити',
    'LBL_MODIFY_CURRENT_FILTER' => 'Modify current filter',
    'LNK_SAVED_VIEWS' => 'Налаштування зовнішнього вигляду',
    'LNK_DELETE' => 'Видалити',
    'LNK_EDIT' => 'Змінити',
    'LNK_GET_LATEST' => 'Get latest',
    'LNK_GET_LATEST_TOOLTIP' => 'Замінити останню версію',
    'LNK_HELP' => 'Довідка',
    'LNK_CREATE' => 'Створити',
    'LNK_LIST_END' => 'End',
    'LNK_LIST_NEXT' => 'Далі',
    'LNK_LIST_PREVIOUS' => 'Попереднє',
    'LNK_LIST_RETURN' => 'Повернутися до списку',
    'LNK_LIST_START' => 'Початок',
    'LNK_LOAD_SIGNED' => 'Підпис',
    'LNK_LOAD_SIGNED_TOOLTIP' => 'Замінити підписаним документом',
    'LNK_PRINT' => 'Друк',
    'LNK_BACKTOTOP' => 'Вгору',
    'LNK_REMOVE' => 'Видалити',
    'LNK_RESUME' => 'Resume',
    'LNK_VIEW_CHANGE_LOG' => 'View Change Log',

    'NTC_CLICK_BACK' => 'Натисніть кнопку браузера "Назад" та ииправте помилку.',
    'NTC_DATE_FORMAT' => '(рррр-мм-дд)',
    'NTC_DELETE_CONFIRMATION_MULTIPLE' => 'Are you sure you want to delete selected record(s)?',
    'NTC_TEMPLATE_IS_USED' => 'The template is used in at least one email marketing record. Are you sure you want to delete it?',
    'NTC_TEMPLATES_IS_USED' => 'The following templates are used in email marketing records. Are you sure you want to delete them?\n',
    'NTC_DELETE_CONFIRMATION' => 'Ви дійсно хочете видалити цей запис?',
    'NTC_DELETE_CONFIRMATION_NUM' => 'Ви впевнені, що бажаєте видалити ',
    'NTC_UPDATE_CONFIRMATION_NUM' => 'Ви дійсно бажаєте оновити ',
    'NTC_DELETE_SELECTED_RECORDS' => ' вибрані записи?',
    'NTC_LOGIN_MESSAGE' => 'Будь ласка, введіть ім\'я користувача та пароль.',
    'NTC_NO_ITEMS_DISPLAY' => 'немає',
    'NTC_REMOVE_CONFIRMATION' => 'Are you sure you want to remove this relationship? Only the relationship will be removed. The record will not be deleted.',
    'NTC_REQUIRED' => 'Позначає обов\'язкове поле',
    'NTC_TIME_FORMAT' => '(24:00)',
    'NTC_WELCOME' => 'Ласкаво просимо',
    'NTC_YEAR_FORMAT' => '(РРРР)',
    'WARN_UNSAVED_CHANGES' => 'You are about to leave this record without saving any changes you may have made to the record. Are you sure you want to navigate away from this record?',
    'ERROR_NO_RECORD' => 'Error retrieving record. This record may be deleted or you may not be authorized to view it.',
    'WARN_BROWSER_VERSION_WARNING' => '<b>Warning:</b> Your browser version is no longer supported or you are using an unsupported browser.<p></p>The following browser versions are recommended:<p></p><ul><li>Internet Explorer 10 (compatibility view not supported)<li>Firefox 32.0<li>Safari 5.1<li>Chrome 37</ul>',
    'WARN_BROWSER_IE_COMPATIBILITY_MODE_WARNING' => '<b>Warning:</b> Your browser is in IE compatibility view which is not supported.',
    'ERROR_TYPE_NOT_VALID' => 'Error. This type is not valid.',
    'ERROR_NO_BEAN' => 'Failed to get bean.',
    'LBL_DUP_MERGE' => 'Find Duplicates',
    'LBL_MANAGE_SUBSCRIPTIONS' => 'Управління підписками',
    'LBL_MANAGE_SUBSCRIPTIONS_FOR' => 'Керувати підписками для ',
    // Ajax status strings
    'LBL_LOADING' => 'Завантаження...',
    'LBL_SAVING_LAYOUT' => 'Saving Layout ...',
    'LBL_SAVED_LAYOUT' => 'Layout has been saved.',
    'LBL_SAVED' => 'Збережено',
    'LBL_SAVING' => 'Збереження',
    'LBL_DISPLAY_COLUMNS' => 'Відобразити стовпці',
    'LBL_HIDE_COLUMNS' => 'Приховати стовпці',
    'LBL_PROCESSING_REQUEST' => 'Processing..',
    'LBL_REQUEST_PROCESSED' => 'Готово',
    'LBL_AJAX_FAILURE' => 'Ajax failure',
    'LBL_MERGE_DUPLICATES' => 'Об&#039;єднати',
    'LBL_SAVED_FILTER_SHORTCUT' => 'Мої фільтри',
    'LBL_SEARCH_POPULATE_ONLY' => 'Perform a search using the search form above',
    'LBL_DETAILVIEW' => 'Detail View',
    'LBL_LISTVIEW' => 'Список',
    'LBL_EDITVIEW' => 'Edit View',
    'LBL_BILLING_STREET' => 'Вулиця:',
    'LBL_SHIPPING_STREET' => 'Вулиця:',
    'LBL_SEARCHFORM' => 'Search Form',
    'LBL_SAVED_SEARCH_ERROR' => 'Please provide a name for this view.',
    'LBL_DISPLAY_LOG' => 'Display Log',
    'ERROR_JS_ALERT_SYSTEM_CLASS' => 'Система',
    'ERROR_JS_ALERT_TIMEOUT_TITLE' => 'Session Timeout',
    'ERROR_JS_ALERT_TIMEOUT_MSG_1' => 'Your session is about to timeout in 2 minutes. Please save your work.',
    'ERROR_JS_ALERT_TIMEOUT_MSG_2' => 'Your session has timed out.',
    'MSG_JS_ALERT_MTG_REMINDER_AGENDA' => "\nAgenda: ",
    'MSG_JS_ALERT_MTG_REMINDER_MEETING' => 'Зустріч',
    'MSG_JS_ALERT_MTG_REMINDER_CALL' => 'Дзвінок',
    'MSG_JS_ALERT_MTG_REMINDER_TIME' => 'Time: ',
    'MSG_JS_ALERT_MTG_REMINDER_LOC' => 'Location: ',
    'MSG_JS_ALERT_MTG_REMINDER_DESC' => 'Опис: ',
    'MSG_JS_ALERT_MTG_REMINDER_STATUS' => 'Статус: ',
    'MSG_JS_ALERT_MTG_REMINDER_RELATED_TO' => 'Related To: ',
    'MSG_JS_ALERT_MTG_REMINDER_CALL_MSG' => "\nClick OK to view this call or click Cancel to dismiss this message.",
    'MSG_JS_ALERT_MTG_REMINDER_MEETING_MSG' => "\nClick OK to view this meeting or click Cancel to dismiss this message.",
    'MSG_JS_ALERT_MTG_REMINDER_NO_EVENT_NAME' => 'Подія',
    'MSG_JS_ALERT_MTG_REMINDER_NO_DESCRIPTION' => 'Подія не настроєно.',
    'MSG_JS_ALERT_MTG_REMINDER_NO_LOCATION' => 'Розташування не настроєно.',
    'MSG_JS_ALERT_MTG_REMINDER_NO_START_DATE' => 'Дата початку не визначено.',
    'MSG_LIST_VIEW_NO_RESULTS_BASIC' => 'Результатів не знайдено.',
    'MSG_LIST_VIEW_NO_RESULTS_CHANGE_CRITERIA' => 'Результатів не знайдено... Можливо змінити критерії пошуку і повторити спробу?',
    'MSG_LIST_VIEW_NO_RESULTS' => 'No results found for <item1>',
    'MSG_LIST_VIEW_NO_RESULTS_SUBMSG' => 'Create <item1> as a new <item2>',
    'MSG_LIST_VIEW_CHANGE_SEARCH' => 'or change your search criteria',
    'MSG_EMPTY_LIST_VIEW_NO_RESULTS' => 'You currently have no records saved. <item2> or <item3> one now.',

    'LBL_CLICK_HERE' => 'Click here',
    // contextMenu strings
    'LBL_ADD_TO_FAVORITES' => 'Add to My Favorites',
    'LBL_CREATE_CONTACT' => 'Новий контакт',
    'LBL_CREATE_CASE' => 'Нове звернення',
    'LBL_CREATE_NOTE' => 'Нова примітка або вкладення',
    'LBL_CREATE_OPPORTUNITY' => 'Нова угода',
    'LBL_SCHEDULE_CALL' => 'Новий дзвінок',
    'LBL_SCHEDULE_MEETING' => 'Призначити зустріч',
    'LBL_CREATE_TASK' => 'Нове завдання',
    //web to lead
    'LBL_GENERATE_WEB_TO_LEAD_FORM' => 'Generate Form',
    'LBL_SAVE_WEB_TO_LEAD_FORM' => 'Save Web Form',
    'LBL_AVAILABLE_FIELDS' => 'Доступні поля',
    'LBL_FIRST_FORM_COLUMN' => 'First Form Column',
    'LBL_SECOND_FORM_COLUMN' => 'Second Form Column',
    'LBL_ASSIGNED_TO_REQUIRED' => 'Missing required field: Assigned to',
    'LBL_RELATED_CAMPAIGN_REQUIRED' => 'Missing required field: Related campaign',
    'LBL_TYPE_OF_PERSON_FOR_FORM' => 'Web form to create ',
    'LBL_TYPE_OF_PERSON_FOR_FORM_DESC' => 'Submitting this form will create ',

    'LBL_ADD_ALL_LEAD_FIELDS' => 'Додати всі поля',
    'LBL_RESET_ALL_LEAD_FIELDS' => 'Скинути всі поля',
    'LBL_REMOVE_ALL_LEAD_FIELDS' => 'Видалити всі поля',
    'LBL_NEXT_BTN' => 'Далі',
    'LBL_ONLY_IMAGE_ATTACHMENT' => 'Only the following supported image type attachments can be embedded: JPG, PNG.',
    'LBL_TRAINING' => 'Support Forum',
    'ERR_MSSQL_DB_CONTEXT' => 'Changed database context to',
    'ERR_MSSQL_WARNING' => 'Попередження:',

    //Meta-Data framework
    'ERR_CANNOT_CREATE_METADATA_FILE' => 'Error: File [[file]] is missing. Unable to create because no corresponding HTML file was found.',
    'ERR_CANNOT_FIND_MODULE' => 'Error: Module [module] does not exist.',
    'LBL_ALT_ADDRESS' => 'Додаткова адреса:',
    'ERR_SMARTY_UNEQUAL_RELATED_FIELD_PARAMETERS' => 'Error: There are an unequal number of arguments for the \'key\' and \'copy\' elements in the displayParams array.',

    /* MySugar Framework (for Home and Dashboard) */
    'LBL_DASHLET_CONFIGURE_GENERAL' => 'Основні налаштування',
    'LBL_DASHLET_CONFIGURE_FILTERS' => 'Фільтри',
    'LBL_DASHLET_CONFIGURE_MY_ITEMS_ONLY' => 'Тільки мої записи',
    'LBL_DASHLET_CONFIGURE_TITLE' => 'Посада',
    'LBL_DASHLET_CONFIGURE_DISPLAY_ROWS' => 'Відображати рядки',

    // MySugar status strings
    'LBL_MAX_DASHLETS_REACHED' => 'You have reached the maximum number of SuiteCRM Dashlets your adminstrator has set. Please remove a SuiteCRM Dashlet to add more.',
    'LBL_ADDING_DASHLET' => 'Adding SuiteCRM Dashlet ...',
    'LBL_ADDED_DASHLET' => 'SuiteCRM Dashlet Added',
    'LBL_REMOVE_DASHLET_CONFIRM' => 'Are you sure you want to remove this SuiteCRM Dashlet?',
    'LBL_REMOVING_DASHLET' => 'Removing SuiteCRM Dashlet ...',
    'LBL_REMOVED_DASHLET' => 'SuiteCRM Dashlet Removed',

    // MySugar Menu Options

    'LBL_LOADING_PAGE' => 'Loading page, please wait...',

    'LBL_RELOAD_PAGE' => 'Please <a href="javascript: window.location.reload()">reload the window</a> to use this SuiteCRM Dashlet.',
    'LBL_ADD_DASHLETS' => 'Add Dashlets',
    'LBL_CLOSE_DASHLETS' => 'Закрити',
    'LBL_OPTIONS' => 'Опції',
    'LBL_1_COLUMN' => '1 Column',
    'LBL_2_COLUMN' => '2 Column',
    'LBL_3_COLUMN' => '3 Column',
    'LBL_PAGE_NAME' => 'Page Name',

    'LBL_SEARCH_RESULTS' => 'Результати пошуку',
    'LBL_SEARCH_MODULES' => 'Модулі',
    'LBL_SEARCH_TOOLS' => 'Інструментарій',
    'LBL_SEARCH_HELP_TITLE' => 'Search Tips',
    /* End MySugar Framework strings */

    'LBL_NO_IMAGE' => 'No Image',

    'LBL_MODULE' => 'Модуль',

    //adding a label for address copy from left
    'LBL_COPY_ADDRESS_FROM_LEFT' => 'Copy address from left:',
    'LBL_SAVE_AND_CONTINUE' => 'Зберегти та продовжити',

    'LBL_SEARCH_HELP_TEXT' => '<p><br /><strong>Multiselect controls</strong></p><ul><li>Click on the values to select an attribute.</li><li>Ctrl-click&nbsp;to&nbsp;select multiple. Mac users use CMD-click.</li><li>To select all values between two attributes,&nbsp; click first value&nbsp;and then shift-click last value.</li></ul><p><strong>Advanced Search & Layout Options</strong><br><br>Using the <b>Saved Search & Layout</b> option, you can save a set of search parameters and/or a custom List View layout in order to quickly obtain the desired search results in the future. You can save an unlimited number of custom searches and layouts. All saved searches appear by name in the Saved Searches list, with the last loaded saved search appearing at the top of the list.<br><br>To customize the List View layout, use the Hide Columns and Display Columns boxes to select which fields to display in the search results. For example, you can view or hide details such as the record name, and assigned user, and assigned team in the search results. To add a column to List View, select the field from the Hide Columns list and use the left arrow to move it to the Display Columns list. To remove a column from List View, select it from the Display Columns list and use the right arrow to move it to the Hide Columns list.<br><br>If you save layout settings, you will be able to load them at any time to view the search results in the custom layout.<br><br>To save and update a search and/or layout:<ol><li>Enter a name for the search results in the <b>Save this search as</b> field and click <b>Save</b>.The name now displays in the Saved Searches list adjacent to the <b>Clear</b> button.</li><li>To view a saved search, select it from the Saved Searches list. The search results are displayed in the List View.</li><li>To update the properties of a saved search, select the saved search from the list, enter the new search criteria and/or layout options in the Advanced Search area, and click <b>Update</b> next to <b>Modify Current Search</b>.</li><li>To delete a saved search, select it in the Saved Searches list, click <b>Delete</b> next to <b>Modify Current Search</b>, and then click <b>OK</b> to confirm the deletion.</li></ol><p><strong>Tips</strong><br><br>By using the % as a wildcard operator you can make your search more broad. For example instead of just searching for results that equal "Apples" you could change your search to "Apples%" which would match all results that start with the word Apples but could contain other characters as well.</p>',

    //resource management
    'ERR_QUERY_LIMIT' => 'Error: Query limit of $limit reached for $module module.',
    'ERROR_NOTIFY_OVERRIDE' => 'Error: ResourceObserver->notify() needs to be overridden.',

    //tracker labels
    'ERR_MONITOR_FILE_MISSING' => 'Error: Unable to create monitor because metadata file is empty or file does not exist.',
    'ERR_MONITOR_NOT_CONFIGURED' => 'Error: There is no monitor configured for requested name',
    'ERR_UNDEFINED_METRIC' => 'Error: Unable to set value for undefined metric',
    'ERR_STORE_FILE_MISSING' => 'Error: Unable to find Store implementation file',

    'LBL_MONITOR_ID' => 'Monitor Id',
    'LBL_USER_ID' => 'User Id',
    'LBL_MODULE_NAME' => 'Module Name',
    'LBL_ITEM_ID' => 'Item Id',
    'LBL_ITEM_SUMMARY' => 'Item Summary',
    'LBL_ACTION' => 'Дія',
    'LBL_SESSION_ID' => 'Session Id',
    'LBL_BREADCRUMBSTACK_CREATED' => 'BreadCrumbStack created for user id {0}',
    'LBL_VISIBLE' => 'Record Visible',
    'LBL_DATE_LAST_ACTION' => 'Date of Last Action',

    //jc:#12287 - For javascript validation messages
    'MSG_IS_NOT_BEFORE' => 'is not before',
    'MSG_IS_MORE_THAN' => 'is more than',
    'MSG_IS_LESS_THAN' => 'is less than',
    'MSG_SHOULD_BE' => 'should be',
    'MSG_OR_GREATER' => 'or greater',

    'LBL_LIST' => 'Список',
    'LBL_CREATE_BUG' => 'Нова помилка',

    'LBL_OBJECT_IMAGE' => 'object image',
    //jchi #12300
    'LBL_MASSUPDATE_DATE' => 'Select Date',

    'LBL_VALIDATE_RANGE' => 'is not within the valid range',
    'LBL_CHOOSE_START_AND_END_DATES' => 'Please choose both a starting and ending date range',
    'LBL_CHOOSE_START_AND_END_ENTRIES' => 'Please choose both starting and ending range entries',

    //jchi #  20776
    'LBL_DROPDOWN_LIST_ALL' => 'Всі відповідні',

    //Connector
    'ERR_CONNECTOR_FILL_BEANS_SIZE_MISMATCH' => 'Error: The Array count of the bean parameter does not match the Array count of the results.',
    'ERR_MISSING_MAPPING_ENTRY_FORM_MODULE' => 'Error: Missing mapping entry for module.',
    'ERROR_UNABLE_TO_RETRIEVE_DATA' => 'Error: Unable to retrieve data for {0} Connector. The service may currently be inaccessible or the configuration settings may be invalid. Connector error message: ({1}).',

    // fastcgi checks
    'LBL_FASTCGI_LOGGING' => 'Для оптимального використання IIS / FastCGI SAPI, встановіть параметр fastcgi.logging файл php.ini рівним 0.',

    //Collection Field
    'LBL_COLLECTION_NAME' => 'Назва',
    'LBL_COLLECTION_PRIMARY' => 'Основний',
    'ERROR_MISSING_COLLECTION_SELECTION' => 'Empty required field',

    //MB -Fixed Bug #32812 -Max
    'LBL_ASSIGNED_TO_NAME' => 'Відповідальний (- а)',
    'LBL_DESCRIPTION' => 'Опис',

    'LBL_YESTERDAY' => 'yesterday',
    'LBL_TODAY' => 'сьогодні',
    'LBL_TOMORROW' => 'завтра',
    'LBL_NEXT_WEEK' => 'наступний тиждень',
    'LBL_NEXT_MONDAY' => 'next monday',
    'LBL_NEXT_FRIDAY' => 'next friday',
    'LBL_TWO_WEEKS' => 'two weeks',
    'LBL_NEXT_MONTH' => 'наступний місяць',
    'LBL_FIRST_DAY_OF_NEXT_MONTH' => 'first day of next month',
    'LBL_THREE_MONTHS' => 'three months',
    'LBL_SIXMONTHS' => 'six months',
    'LBL_NEXT_YEAR' => 'next year',

    //Datetimecombo fields
    'LBL_HOURS' => 'Годин',
    'LBL_MINUTES' => 'Хвилин',
    'LBL_MERIDIEM' => 'Meridiem',
    'LBL_DATE' => 'Дата',
    'LBL_DASHLET_CONFIGURE_AUTOREFRESH' => 'Auto-Refresh',

    'LBL_DURATION_DAY' => 'день',
    'LBL_DURATION_HOUR' => 'hour',
    'LBL_DURATION_MINUTE' => 'minute',
    'LBL_DURATION_DAYS' => 'днів',
    'LBL_DURATION_HOURS' => 'Duration Hours',
    'LBL_DURATION_MINUTES' => 'Duration Minutes',

    //Calendar widget labels
    'LBL_CHOOSE_MONTH' => 'Choose Month',
    'LBL_ENTER_YEAR' => 'Enter Year',
    'LBL_ENTER_VALID_YEAR' => 'Please enter a valid year',

    //File write error label
    'ERR_FILE_WRITE' => 'Error: Could not write file {0}. Please check system and web server permissions.',
    'ERR_FILE_NOT_FOUND' => 'Error: Could not load file {0}. Please check system and web server permissions.',

    'LBL_AND' => 'And',

    // File fields
    'LBL_SEARCH_EXTERNAL_API' => 'File on External Source',
    'LBL_EXTERNAL_SECURITY_LEVEL' => 'Security',

    //IMPORT SAMPLE TEXT
    'LBL_IMPORT_SAMPLE_FILE_TEXT' => '
"This is a sample import file which provides an example of the expected contents of a file that is ready for import."
"The file is a comma-delimited .csv file, using double-quotes as the field qualifier."

"The header row is the top-most row in the file and contains the field labels as you would see them in the application."
"These labels are used for mapping the data in the file to the fields in the application."

"Notes: The database names could also be used in the header row. This is useful when you are using phpMyAdmin or another database tool to provide an exported list of data to import."
"The column order is not critical as the import process matches the data to the appropriate fields based on the header row."


"To use this file as a template, do the following:"
"1. Remove the sample rows of data"
"2. Remove the help text that you are reading right now"
"3. Input your own data into the appropriate rows and columns"
"4. Save the file to a known location on your system"
"5. Click on the Import option from the Actions menu in the application and choose the file to upload"
   ',
    //define labels to be used for overriding local values during import/export

    'LBL_NOTIFICATIONS_NONE' => 'No Current Notifications',
    'LBL_ALT_SORT_DESC' => 'Sorted Descending',
    'LBL_ALT_SORT_ASC' => 'Sorted Ascending',
    'LBL_ALT_SORT' => 'Сортувати',
    'LBL_ALT_SHOW_OPTIONS' => 'Show Options',
    'LBL_ALT_HIDE_OPTIONS' => 'Hide Options',
    'LBL_ALT_MOVE_COLUMN_LEFT' => 'Move selected entry to the list on the left',
    'LBL_ALT_MOVE_COLUMN_RIGHT' => 'Move selected entry to the list on the right',
    'LBL_ALT_MOVE_COLUMN_UP' => 'Move selected entry up in the displayed list order',
    'LBL_ALT_MOVE_COLUMN_DOWN' => 'Move selected entry down in the displayed list order',
    'LBL_ALT_INFO' => 'Information',
    'MSG_DUPLICATE' => 'The {0} record you are about to create might be a duplicate of an {0} record that already exists. {1} records containing similar names are listed below.<br>Click Create {1} to continue creating this new {0}, or select an existing {0} listed below.',
    'MSG_SHOW_DUPLICATES' => 'The {0} record you are about to create might be a duplicate of a {0} record that already exists. {1} records containing similar names are listed below. Click Save to continue creating this new {0}, or click Cancel to return to the module without creating the {0}.',
    'LBL_EMAIL_TITLE' => 'email address',
    'LBL_EMAIL_OPT_TITLE' => 'opted out email address',
    'LBL_EMAIL_INV_TITLE' => 'invalid email address',
    'LBL_EMAIL_PRIM_TITLE' => 'Make Primary Email Address',
    'LBL_SELECT_ALL_TITLE' => 'Select all',
    'LBL_SELECT_THIS_ROW_TITLE' => 'Select this row',

    //for upload errors
    'UPLOAD_ERROR_TEXT' => 'ERROR: There was an error during upload. Error code: {0} - {1}',
    'UPLOAD_ERROR_TEXT_SIZEINFO' => 'ERROR: There was an error during upload. Error code: {0} - {1}. The upload_maxsize is {2} ',
    'UPLOAD_ERROR_HOME_TEXT' => 'ERROR: There was an error during your upload, please contact an administrator for help.',
    'UPLOAD_MAXIMUM_EXCEEDED' => 'Size of Upload ({0} bytes) Exceeded Allowed Maximum: {1} bytes',
    'UPLOAD_REQUEST_ERROR' => 'An error has occured. Please refresh your page and try again.',

    //508 used Access Keys
    'LBL_EDIT_BUTTON_KEY' => 'i',
    'LBL_EDIT_BUTTON_LABEL' => 'Змінити',
    'LBL_EDIT_BUTTON_TITLE' => 'Змінити',
    'LBL_DUPLICATE_BUTTON_KEY' => 'u',
    'LBL_DUPLICATE_BUTTON_LABEL' => 'Дублювати',
    'LBL_DUPLICATE_BUTTON_TITLE' => 'Дублювати',
    'LBL_DELETE_BUTTON_KEY' => 'd',
    'LBL_DELETE_BUTTON_LABEL' => 'Видалити',
    'LBL_DELETE_BUTTON_TITLE' => 'Видалити',
    'LBL_BULK_ACTION_BUTTON_LABEL' => 'Bulk Action', //Can be translated in all caps. This string will be used by SuiteP template menu actions
    'LBL_BULK_ACTION_BUTTON_LABEL_MOBILE' => 'Дія', //Can be translated in all caps. This string will be used by SuiteP template menu actions
    'LBL_SAVE_BUTTON_KEY' => 'a',
    'LBL_SAVE_BUTTON_LABEL' => 'Зберегти',
    'LBL_SAVE_BUTTON_TITLE' => 'Зберегти',
    'LBL_CANCEL_BUTTON_KEY' => 'l',
    'LBL_CANCEL_BUTTON_LABEL' => 'Скасування',
    'LBL_CANCEL_BUTTON_TITLE' => 'Скасування',
    'LBL_FIRST_INPUT_EDIT_VIEW_KEY' => '7',
    'LBL_ADV_SEARCH_LNK_KEY' => '8',
    'LBL_FIRST_INPUT_SEARCH_KEY' => '9',

    'ERR_CONNECTOR_NOT_ARRAY' => 'connector array in {0} been defined incorrectly or is empty and could not be used.',
    'ERR_SUHOSIN' => 'Upload stream is blocked by Suhosin, please add &quot;upload&quot; to suhosin.executor.include.whitelist (See suitecrm.log for more information)',
    'ERR_BAD_RESPONSE_FROM_SERVER' => 'Bad response from the server',
    'LBL_ACCOUNT_PRODUCT_QUOTE_LINK' => 'Quote',
    'LBL_ACCOUNT_PRODUCT_SALE_PRICE' => 'Sale Price',
    'LBL_EMAIL_CHECK_INTERVAL_DOM' => array(
        '-1' => 'Вручну',
        '5' => 'Кожні 5 хвилин',
        '15' => 'Every 15 minutes',
        '30' => 'Every 30 minutes',
        '60' => 'Every hour',
    ),

    'ERR_A_REMINDER_IS_EMPTY_OR_INCORRECT' => 'A reminder is empty or incorrect.',
    'ERR_REMINDER_IS_NOT_SET_POPUP_OR_EMAIL' => 'Reminder is not set for either a popup or email.',
    'ERR_NO_INVITEES_FOR_REMINDER' => 'No invitees for reminder.',
    'LBL_DELETE_REMINDER_CONFIRM' => 'Reminder doesn\'t include any invitees, do you want to remove the reminder?',
    'LBL_DELETE_REMINDER' => 'Delete Reminder',
    'LBL_OK' => 'Ok',

    'LBL_COLUMNS_FILTER_HEADER_TITLE' => 'Choose columns',
    'LBL_SAVE_CHANGES_BUTTON_TITLE' => 'Save changes',
    'LBL_DISPLAYED' => 'Displayed',
    'LBL_HIDDEN' => 'Приховані',
    'ERR_EMPTY_COLUMNS_LIST' => 'At least, one element required',

    'LBL_FILTER_HEADER_TITLE' => 'Фільтер',

    'LBL_CATEGORY' => 'Категорія',
    'LBL_LIST_CATEGORY' => 'Категорія',

    'LBL_CONFIRM_DISREGARD_DRAFT_TITLE' => 'Disregard draft',
    'LBL_CONFIRM_DISREGARD_DRAFT_BODY' => 'This operation will delete this email, do you want to continue?',
    'LBL_CONFIRM_APPLY_EMAIL_TEMPLATE_TITLE' => 'Apply an Email Template',
    'LBL_CONFIRM_APPLY_EMAIL_TEMPLATE_BODY' => 'This operation will override the email Body and Subject fields, do you want to continue?',
);

$app_list_strings['moduleList']['Library'] = 'Library';
$app_list_strings['moduleList']['EmailAddresses'] = 'Адреса E-mail';
$app_list_strings['project_priority_default'] = 'Середній';
$app_list_strings['project_priority_options'] = array(
    'High' => 'Високий',
    'Medium' => 'Середній',
    'Low' => 'Низький',
);

$app_list_strings['moduleList']['KBDocuments'] = 'База знань';

$app_list_strings['countries_dom'] = array(
    '' => '',
    'ABU DHABI' => 'АБУ-ДАБІ',
    'ADEN' => 'АДЕН',
    'AFGHANISTAN' => 'АФГАНІСТАН',
    'ALBANIA' => 'АЛБАНІЯ',
    'ALGERIA' => 'АЛЖИР',
    'AMERICAN SAMOA' => 'АМЕРИКАНСЬКЕ САМОА',
    'ANDORRA' => 'АНДОРРА',
    'ANGOLA' => 'АНГОЛА',
    'ANTARCTICA' => 'АНТАРКТИДА',
    'ANTIGUA' => 'АНТИГУА',
    'ARGENTINA' => 'АРГЕНТИНА',
    'ARMENIA' => 'ВІРМЕНІЯ',
    'ARUBA' => 'АРУБА',
    'AUSTRALIA' => 'АВСТРАЛІЯ',
    'AUSTRIA' => 'АВСТРІЯ',
    'AZERBAIJAN' => 'АЗЕРБАЙДЖАН',
    'BAHAMAS' => 'БАГАМСЬКІ ОСТРОВИ',
    'BAHRAIN' => 'БАХРЕЙН',
    'BANGLADESH' => 'БАНГЛАДЕШ',
    'BARBADOS' => 'БАРБАДОС',
    'BELARUS' => 'БІЛОРУСЬ',
    'BELGIUM' => 'БЕЛЬГІЯ',
    'BELIZE' => 'БЕЛІЗ',
    'BENIN' => 'БЕНІН',
    'BERMUDA' => 'БЕРМУДСЬКІ ОСТРОВИ',
    'BHUTAN' => 'БУТАН',
    'BOLIVIA' => 'БОЛІВІЯ',
    'BOSNIA' => 'БОСНІЯ',
    'BOTSWANA' => 'БОТСВАНА',
    'BOUVET ISLAND' => 'ОСТРІВ БУВе',
    'BRAZIL' => 'БРАЗИЛІЯ',
    'BRITISH ANTARCTICA TERRITORY' => 'БРИТАНСЬКА АНТАРКТИЧНА ТЕРИТОРІЯ',
    'BRITISH INDIAN OCEAN TERRITORY' => 'БРИТАНСЬКА ТЕРИТОРІЯ В ІНДІЙСЬКОМУ ОКЕАНІ',
    'BRITISH VIRGIN ISLANDS' => 'ВІРГІНСЬКІ ОСТРОВИ - ВЕЛИКОБРИТАНІЯ',
    'BRITISH WEST INDIES' => 'BRITISH WEST INDIES',
    'BRUNEI' => 'БРУНЕЙ',
    'BULGARIA' => 'БОЛГАРІЯ',
    'BURKINA FASO' => 'БУРКІНА-ФАСО',
    'BURUNDI' => 'БУРУНДІ',
    'CAMBODIA' => 'КАМБОДЖА',
    'CAMEROON' => 'КАМЕРУН',
    'CANADA' => 'КАНАДА',
    'CANAL ZONE' => 'ЗОНА ПАНАМСЬКОГО КАНАЛУ',
    'CANARY ISLAND' => 'КАНАРСЬКІ ОСТРОВИ',
    'CAPE VERDI ISLANDS' => 'РЕСПУБЛІКА КАБО-ВЕРДЕ',
    'CAYMAN ISLANDS' => 'КАЙМАНОВІ ОСТРОВИ',
    'CHAD' => 'ЧАД',
    'CHANNEL ISLAND UK' => 'НОРМАНДСЬКІ ОСТРОВИ',
    'CHILE' => 'ЧИЛІ',
    'CHINA' => 'КИТАЙ',
    'CHRISTMAS ISLAND' => 'ОСТРІВ РІЗДВА',
    'COCOS (KEELING) ISLAND' => 'КОКОСОВІ ОСТРОВИ',
    'COLOMBIA' => 'КОЛУМБІЯ',
    'COMORO ISLANDS' => 'КОМОРСЬКІ ОСТРОВИ',
    'CONGO' => 'КОНГО',
    'CONGO KINSHASA' => 'КОНГО КІНШАСА',
    'COOK ISLANDS' => 'ОСТРОВИ КУКА',
    'COSTA RICA' => 'КОСТА-РІКА',
    'CROATIA' => 'ХОРВАТІЯ',
    'CUBA' => 'КУБА',
    'CURACAO' => 'КЮРАСАО',
    'CYPRUS' => 'КІПР',
    'CZECH REPUBLIC' => 'ЧЕХІЯ',
    'DAHOMEY' => 'ДАГОМЕЯ',
    'DENMARK' => 'ДАНІЯ',
    'DJIBOUTI' => 'ФРАНК',
    'DOMINICA' => 'ДОМІНІКА',
    'DOMINICAN REPUBLIC' => 'ДОМІНІКАНСЬКА РЕСПУБЛІКА',
    'DUBAI' => 'ДУБАЙ',
    'ECUADOR' => 'ЕКВАДОР',
    'EGYPT' => 'ЄГИПЕТ',
    'EL SALVADOR' => 'САЛЬВАДОР',
    'EQUATORIAL GUINEA' => 'ЕКВАТОРІАЛЬНА ГВІНЕЯ',
    'ESTONIA' => 'ЕСТОНІЯ',
    'ETHIOPIA' => 'ЕФІОПІЯ',
    'FAEROE ISLANDS' => 'ФАРЕРСЬКІ ОСТРОВИ',
    'FALKLAND ISLANDS' => 'ФОЛКЛЕНДСЬКІ ОСТРОВИ',
    'FIJI' => 'ФІДЖІ',
    'FINLAND' => 'ФІНЛЯНДІЯ',
    'FRANCE' => 'ФРАНЦІЯ',
    'FRENCH GUIANA' => 'ФРАНЦУЗЬКА ГВІАНА',
    'FRENCH POLYNESIA' => 'ФРАНЦУЗЬКА ПОЛІНЕЗІЯ',
    'GABON' => 'ГАБОН',
    'GAMBIA' => 'ГАМБІЯ',
    'GEORGIA' => 'ГРУЗІЯ',
    'GERMANY' => 'НІМЕЧЧИНА',
    'GHANA' => 'ГАНА',
    'GIBRALTAR' => 'ГІБРАЛТАР',
    'GREECE' => 'ГРЕЦІЯ',
    'GREENLAND' => 'ГРЕНЛАНДІЯ',
    'GUADELOUPE' => 'ГВАДЕЛУПА',
    'GUAM' => 'ГУАМ',
    'GUATEMALA' => 'ГВАТЕМАЛА',
    'GUINEA' => 'ГВІНЕЯ',
    'GUYANA' => 'ГАЙАНА',
    'HAITI' => 'ГАЇТІ',
    'HONDURAS' => 'ГОНДУРАС',
    'HONG KONG' => 'ГОНКОНГ',
    'HUNGARY' => 'УГОРЩИНА',
    'ICELAND' => 'ІСЛАНДІЯ',
    'IFNI' => 'ІФНИ',
    'INDIA' => 'ІНДІЯ',
    'INDONESIA' => 'ІНДОНЕЗІЯ',
    'IRAN' => 'ІРАН',
    'IRAQ' => 'ІРАК',
    'IRELAND' => 'ІРЛАНДІЯ',
    'ISRAEL' => 'ІЗРАЇЛЬ',
    'ITALY' => 'ІТАЛІЯ',
    'IVORY COAST' => 'БЕРЕГ СЛОНОВОЇ КІСТКИ',
    'JAMAICA' => 'ЯМАЙКА',
    'JAPAN' => 'ЯПОНІЯ',
    'JORDAN' => 'ЙОРДАНІЯ',
    'KAZAKHSTAN' => 'КАЗАХСТАН',
    'KENYA' => 'КЕНІЯ',
    'KOREA' => 'КНДР',
    'KOREA, SOUTH' => 'ПІВДЕННА КОРЕЯ',
    'KUWAIT' => 'КУВЕЙТ',
    'KYRGYZSTAN' => 'КИРГИЗІЯ',
    'LAOS' => 'ЛАОС',
    'LATVIA' => 'ЛАТВІЯ',
    'LEBANON' => 'ЛІВАН',
    'LEEWARD ISLANDS' => 'ПІДВІТРЯНІ ОСТРОВИ',
    'LESOTHO' => 'ЛЕСОТО',
    'LIBYA' => 'ЛІВІЯ',
    'LIECHTENSTEIN' => 'ЛІХТЕНШТЕЙН',
    'LITHUANIA' => 'ЛИТВА',
    'LUXEMBOURG' => 'ЛЮКСЕМБУРГ',
    'MACAO' => 'МАКАО',
    'MACEDONIA' => 'МАКЕДОНІЯ',
    'MADAGASCAR' => 'МАДАГАСКАР',
    'MALAWI' => 'МАЛАВІ',
    'MALAYSIA' => 'МАЛАЙЗІЯ',
    'MALDIVES' => 'МАЛЬДІВСЬКІ ОСТРОВИ',
    'MALI' => 'МАЛІ',
    'MALTA' => 'МАЛЬТА',
    'MARTINIQUE' => 'МАРТІНІКА',
    'MAURITANIA' => 'МАВРИТАНІЯ',
    'MAURITIUS' => 'МАВРИКІЙ',
    'MELANESIA' => 'МЕЛАНЕЗІЯ',
    'MEXICO' => 'МЕКСИКА',
    'MOLDOVIA' => 'МОЛДОВА',
    'MONACO' => 'МОНАКО',
    'MONGOLIA' => 'МОНГОЛІЯ',
    'MOROCCO' => 'МАРОККО',
    'MOZAMBIQUE' => 'МОЗАМБІК',
    'MYANAMAR' => 'М&#039;ЯНМА',
    'NAMIBIA' => 'НАМІБІЯ',
    'NEPAL' => 'НЕПАЛ',
    'NETHERLANDS' => 'НІДЕРЛАНДИ',
    'NETHERLANDS ANTILLES' => 'НІДЕРЛАНДСЬКІ АНТИЛЬСЬКІ ОСТРОВИ',
    'NETHERLANDS ANTILLES NEUTRAL ZONE' => 'НІДЕРЛАНДСЬКІ АНТИЛЬСЬКІ ОСТРОВИ - НЕЙТРАЛЬНА ЗОНА',
    'NEW CALADONIA' => 'НОВА КАЛЕДОНІЯ',
    'NEW HEBRIDES' => 'НОВІ ГЕБРІДИ',
    'NEW ZEALAND' => 'НОВА ЗЕЛАНДІЯ',
    'NICARAGUA' => 'НІКАРАГУА',
    'NIGER' => 'НІГЕР',
    'NIGERIA' => 'НІГЕРІЯ',
    'NORFOLK ISLAND' => 'НОРМАНДСЬКІ ОСТРОВИ',
    'NORWAY' => 'НОРВЕГІЯ',
    'OMAN' => 'ОМАН',
    'OTHER' => 'Інше',
    'PACIFIC ISLAND' => 'МАРІАНСЬКІ ОСТРОВИ',
    'PAKISTAN' => 'ПАКИСТАН',
    'PANAMA' => 'ПАНАМА',
    'PAPUA NEW GUINEA' => 'ПАПУА-НОВА ГВІНЕЯ',
    'PARAGUAY' => 'ПАРАГВАЙ',
    'PERU' => 'ПЕРУ',
    'PHILIPPINES' => 'ФІЛІППІНИ',
    'POLAND' => 'ПОЛЬЩА',
    'PORTUGAL' => 'ПОРТУГАЛІЯ',
    'PORTUGUESE TIMOR' => 'EAST TIMOR',
    'PUERTO RICO' => 'ПУЕРТО-РІКО',
    'QATAR' => 'КАТАР',
    'REPUBLIC OF BELARUS' => 'БІЛОРУСЬ',
    'REPUBLIC OF SOUTH AFRICA' => 'ПАР',
    'REUNION' => 'РЕЮНЬЙОН',
    'ROMANIA' => 'РУМУНІЯ',
    'RUSSIA' => 'РОСІЯ',
    'RWANDA' => 'РУАНДА',
    'RYUKYU ISLANDS' => 'ОСТРОВИ РЮКЮ (НАНСЕЙ)',
    'SABAH' => 'САБАХ (МАЛАЙЗІЯ)',
    'SAN MARINO' => 'САН-МАРИНО',
    'SAUDI ARABIA' => 'САУДІВСЬКА АРАВІЯ',
    'SENEGAL' => 'СЕНЕГАЛ',
    'SERBIA' => 'СЕРБІЯ',
    'SEYCHELLES' => 'СЕЙШЕЛЬСЬКІ ОСТРОВИ',
    'SIERRA LEONE' => 'СЬЄРРА-ЛЕОНЕ',
    'SINGAPORE' => 'СІНГАПУР',
    'SLOVAKIA' => 'СЛОВАЧЧИНА',
    'SLOVENIA' => 'СЛОВЕНІЯ',
    'SOMALILIAND' => 'СОМАЛІ',
    'SOUTH AFRICA' => 'ПІВДЕННА АФРИКА',
    'SOUTH YEMEN' => 'ПІВДЕННИЙ ЄМЕН',
    'SPAIN' => 'ІСПАНІЯ',
    'SPANISH SAHARA' => 'ЗАХІДНА САХАРА',
    'SRI LANKA' => 'Шрі-Ланка',
    'ST. KITTS AND NEVIS' => 'СЕНТ-КІТС І НЕВІС',
    'ST. LUCIA' => 'СЕНТ-ЛЮСІЯ',
    'SUDAN' => 'СУДАН',
    'SURINAM' => 'СУРИНАМ',
    'SW AFRICA' => 'НАМІБІЯ',
    'SWAZILAND' => 'СВАЗІЛЕНД',
    'SWEDEN' => 'ШВЕЦІЯ',
    'SWITZERLAND' => 'ШВЕЙЦАРІЯ',
    'SYRIA' => 'СИРІЯ',
    'TAIWAN' => 'ТАЙВАНЬ',
    'TAJIKISTAN' => 'ТАДЖИКИСТАН',
    'TANZANIA' => 'ТАНЗАНІЯ',
    'THAILAND' => 'ТАЇЛАНД',
    'TONGA' => 'ТОНГА',
    'TRINIDAD' => 'ТРИНІДАД',
    'TUNISIA' => 'ТУНІС',
    'TURKEY' => 'ТУРЕЧЧИНА',
    'UGANDA' => 'УГАНДА',
    'UKRAINE' => 'УКРАЇНА',
    'UNITED ARAB EMIRATES' => 'ОАЕ',
    'UNITED KINGDOM' => 'ВЕЛИКОБРИТАНІЯ',
    'URUGUAY' => 'УРУГВАЙ',
    'US PACIFIC ISLAND' => 'МАРІАНСЬКІ ОСТРОВИ-США',
    'US VIRGIN ISLANDS' => 'ВІРГІНСЬКІ ОСТРОВИ-США',
    'USA' => 'США',
    'UZBEKISTAN' => 'УЗБЕКИСТАН',
    'VANUATU' => 'ВАНУАТУ',
    'VATICAN CITY' => 'ВАТИКАН',
    'VENEZUELA' => 'ВЕНЕСУЕЛА',
    'VIETNAM' => 'В&#039;ЄТНАМ',
    'WAKE ISLAND' => 'ОКЕАНІЯ',
    'WEST INDIES' => 'Вест-Індія',
    'WESTERN SAHARA' => 'ЗАХІДНА САХАРА',
    'YEMEN' => 'ЄМЕН',
    'ZAIRE' => 'ЗАЇР',
    'ZAMBIA' => 'ЗАМБІЯ',
    'ZIMBABWE' => 'ЗІМБАБВЕ',
);

$app_list_strings['charset_dom'] = array(
    'BIG-5' => 'BIG-5 (Тайвань і Гонконг)',
    /*'CP866'     => 'CP866', // ms-dos Cyrillic */
    /*'CP949'     => 'CP949 (Microsoft Korean)', */
    'CP1251' => 'CP1251 (MS кирилиця)',
    'CP1252' => 'CP1252 (MS Західна Європа і США)',
    'EUC-CN' => 'EUC-CN (Спрощений китайський GB2312)',
    'EUC-JP' => 'EUC-JP (Unix японська)',
    'EUC-KR' => 'EUC-KR (Корейська)',
    'EUC-TW' => 'EUC-TW (Тайванський)',
    'ISO-2022-JP' => 'ISO-2022-JP (Японська)',
    'ISO-2022-KR' => 'ISO-2022-KR (Корейська)',
    'ISO-8859-1' => 'ISO-8859-1 (Західна Європа і США)',
    'ISO-8859-2' => 'ISO-8859-2 (Центральна і Східна Європа)',
    'ISO-8859-3' => 'ISO-8859-3 (Латиниця 3)',
    'ISO-8859-4' => 'ISO-8859-4 (Латиниця 4)',
    'ISO-8859-5' => 'ISO-8859-5 (Кирилиця)',
    'ISO-8859-6' => 'ISO-8859-6 (Arabic)',
    'ISO-8859-7' => 'ISO-8859-7 (Грецький)',
    'ISO-8859-8' => 'ISO-8859-8 (Іврит)',
    'ISO-8859-9' => 'ISO-8859-9 (Латиниця 5)',
    'ISO-8859-10' => 'ISO-8859-10 (Латиниця 6)',
    'ISO-8859-13' => 'ISO-8859-13 (Латиниця 7)',
    'ISO-8859-14' => 'ISO-8859-14 (Латиниця 8)',
    'ISO-8859-15' => 'ISO-8859-15 (Латиниця 9)',
    'KOI8-R' => 'KOI8-R (Російська кирилиця)',
    'KOI8-U' => 'KOI8-U (Українська кирилиця)',
    'SJIS' => 'SJIS (MS японська)',
    'UTF-8' => 'UTF-8',
);

$app_list_strings['timezone_dom'] = array(

    'Africa/Algiers' => 'Африка/Алжир',
    'Africa/Luanda' => 'Африка/Луанда',
    'Africa/Porto-Novo' => 'Африка/Порто-Ново',
    'Africa/Gaborone' => 'Африка/Габороне',
    'Africa/Ouagadougou' => 'Африка/Уагадугу',
    'Africa/Bujumbura' => 'Африка/Бужумбура',
    'Africa/Douala' => 'Африка/Douala (Дуала)',
    'Atlantic/Cape_Verde' => 'Атлантика/Cape_Verde (Капо-Верде)',
    'Africa/Bangui' => 'Африка/Бангі',
    'Africa/Ndjamena' => 'Африка/Ndjamena (Нджамена)',
    'Indian/Comoro' => 'Індія/Comoro (Коморо)',
    'Africa/Kinshasa' => 'Африка/Кіншаса',
    'Africa/Lubumbashi' => 'Африка/Lubumbashi (Лубумбаші)',
    'Africa/Brazzaville' => 'Африка/Браззавіль',
    'Africa/Abidjan' => 'Африка/Абіджан',
    'Africa/Djibouti' => 'Африка/Франк',
    'Africa/Cairo' => 'Африка/Каїр',
    'Africa/Malabo' => 'Африка/Малабо',
    'Africa/Asmera' => 'Африка/Asmera (Асмера)',
    'Africa/Addis_Ababa' => 'Африка/Аддіс-Абеба',
    'Africa/Libreville' => 'Африка/Лібревіль',
    'Africa/Banjul' => 'Африка/Банджул',
    'Africa/Accra' => 'Африка/Аккра',
    'Africa/Conakry' => 'Африка/Конакрі',
    'Africa/Bissau' => 'Африка/Бісау',
    'Africa/Nairobi' => 'Африка/Найробі',
    'Africa/Maseru' => 'Африка/Масеру',
    'Africa/Monrovia' => 'Африка/Монровія',
    'Africa/Tripoli' => 'Африка/Тріполі',
    'Indian/Antananarivo' => 'Індія/Антананаріву',
    'Africa/Blantyre' => 'Африка/Blantyre (Блантир)',
    'Africa/Bamako' => 'Африка/Бамако',
    'Africa/Nouakchott' => 'Африка/Нуакшот',
    'Indian/Mauritius' => 'Індія/Маврикій',
    'Indian/Mayotte' => 'Індія/Mayotte',
    'Africa/Casablanca' => 'Африка/Касабланка',
    'Africa/El_Aaiun' => 'Африка/El_Aaiun (Ель-Ааяюн)',
    'Africa/Maputo' => 'Африка/Мапуто',
    'Africa/Windhoek' => 'Африка/Віндгук',
    'Africa/Niamey' => 'Африка/Ніамей',
    'Africa/Lagos' => 'Африка/Лагос',
    'Indian/Reunion' => 'Індія/Reunion (Реюніон)',
    'Africa/Kigali' => 'Африка/Kigali',
    'Atlantic/St_Helena' => 'Атлантика/St. Helena (Свята Хелена)',
    'Africa/Sao_Tome' => 'Африка/Сан-Томі',
    'Africa/Dakar' => 'Африка/Дакар',
    'Indian/Mahe' => 'Індія/Mahe (Махе)',
    'Africa/Freetown' => 'Африка/Фрітаун',
    'Africa/Mogadishu' => 'Африка/Могадішо',
    'Africa/Johannesburg' => 'Африка/Йоганнесбург',
    'Africa/Khartoum' => 'Африка/Хартум',
    'Africa/Mbabane' => 'Африка/Мбабане',
    'Africa/Dar_es_Salaam' => 'Африка/Дар-ес-Саламі',
    'Africa/Lome' => 'Африка/Ломе',
    'Africa/Tunis' => 'Африка/Туніс',
    'Africa/Kampala' => 'Африка/Кампала',
    'Africa/Lusaka' => 'Африка/Лусака',
    'Africa/Harare' => 'Африка/Хараре',
    'Antarctica/Casey' => 'Антарктика/Casey (Кейзі)',
    'Antarctica/Davis' => 'Антарктика/Davis (Девіс)',
    'Antarctica/Mawson' => 'Антарктика/Mawson (Мейсон)',
    'Indian/Kerguelen' => 'Індія/Kerguelen (Кергелен)',
    'Antarctica/DumontDUrville' => 'Антарктика/DumontDUrville (Дюмон Дюрвіль)',
    'Antarctica/Syowa' => 'Антарктика/Syowa (Сйова)',
    'Antarctica/Vostok' => 'Антарктика/Восток',
    'Antarctica/Rothera' => 'Антарктика/Rothera (Палмер)',
    'Antarctica/Palmer' => 'Антарктика/Palmer (Палмер)',
    'Antarctica/McMurdo' => 'Антарктика/McMurdo (Макмурдо)',
    'Asia/Kabul' => 'Азія/Кабул',
    'Asia/Yerevan' => 'Азія/Єреван',
    'Asia/Baku' => 'Азія/Баку',
    'Asia/Bahrain' => 'Азія/Бахрейн',
    'Asia/Dhaka' => 'Азія/Дакка',
    'Asia/Thimphu' => 'Азія/Тхімпху',
    'Indian/Chagos' => 'Індія/Chagos (Чагос)',
    'Asia/Brunei' => 'Азія/Бруней',
    'Asia/Rangoon' => 'Азія/Рангун (Ягон)',
    'Asia/Phnom_Penh' => 'Азія/Пномпень',
    'Asia/Beijing' => 'Азія/Пекін',
    'Asia/Harbin' => 'Азія/Харбін',
    'Asia/Shanghai' => 'Азія/Шанхай',
    'Asia/Chongqing' => 'Азія/Chongqing (Чонгкінг)',
    'Asia/Urumqi' => 'Азія/Urumqi (Урумкі)',
    'Asia/Kashgar' => 'Азія/Kashgar (Кашгар)',
    'Asia/Hong_Kong' => 'Азія/Гонконг',
    'Asia/Taipei' => 'Азія/Тайбей',
    'Asia/Macau' => 'Азія/Macau (Макау)',
    'Asia/Nicosia' => 'Азія/Нікосія',
    'Asia/Tbilisi' => 'Азія/Тбілісі',
    'Asia/Dili' => 'Азія/Ділі',
    'Asia/Calcutta' => 'Азія/Калькутта',
    'Asia/Jakarta' => 'Азія/Джакарта',
    'Asia/Pontianak' => 'Азія/Pontianak (Понтьянак)',
    'Asia/Makassar' => 'Азія/Makassar (Макассар)',
    'Asia/Jayapura' => 'Азія/Джаяпура (Джаяпура)',
    'Asia/Tehran' => 'Азія/Тегеран',
    'Asia/Baghdad' => 'Азія/Багдад',
    'Asia/Jerusalem' => 'Азія/Єрусалим',
    'Asia/Tokyo' => 'Азія/Токіо',
    'Asia/Amman' => 'Азія/Амман',
    'Asia/Almaty' => 'Азія/Алмати',
    'Asia/Qyzylorda' => 'Азія/Qyzylorda (Квизилорда)',
    'Asia/Aqtobe' => 'Азія/Aqtobe (Актобе)',
    'Asia/Aqtau' => 'Азія/Актау (Актау)',
    'Asia/Oral' => 'Азія/Oral (Кричав)',
    'Asia/Bishkek' => 'Азія/Бішкек',
    'Asia/Seoul' => 'Азія/Сеул',
    'Asia/Pyongyang' => 'Азія/Пхеньян',
    'Asia/Kuwait' => 'Азія/Кувейт',
    'Asia/Vientiane' => 'Азія/В&#039;єнтьян',
    'Asia/Beirut' => 'Азія/Бейрут',
    'Asia/Kuala_Lumpur' => 'Азія/Куала-Лумпур',
    'Asia/Kuching' => 'Азія/Kuching',
    'Indian/Maldives' => 'Індія/Мальдіви',
    'Asia/Hovd' => 'Азія/Hovd (Ховд)',
    'Asia/Ulaanbaatar' => 'Азія/Ulaanbaatar (Улаанбатаар)',
    'Asia/Choibalsan' => 'Азія/Choibalsan (Чоибалсан)',
    'Asia/Katmandu' => 'Азія/Катманду',
    'Asia/Muscat' => 'Азія/Маскат',
    'Asia/Karachi' => 'Азія/Карачі',
    'Asia/Gaza' => 'Азія/Газу',
    'Asia/Manila' => 'Азія/Маніла',
    'Asia/Qatar' => 'Азія/Катар',
    'Asia/Riyadh' => 'Азія/Ріяд',
    'Asia/Singapore' => 'Азія/Сінгапур',
    'Asia/Colombo' => 'Азія/Коломбо',
    'Asia/Damascus' => 'Азія/Дамаск',
    'Asia/Dushanbe' => 'Азія/Душанбе',
    'Asia/Bangkok' => 'Азія/Бангкок',
    'Asia/Ashgabat' => 'Азія/Ашгабат',
    'Asia/Dubai' => 'Азія/Дубаї',
    'Asia/Samarkand' => 'Азія/Самарканд',
    'Asia/Tashkent' => 'Азія/Ташкент',
    'Asia/Saigon' => 'Азія/Сайгон',
    'Asia/Aden' => 'Азія/Аден',
    'Australia/Darwin' => 'Австралія/Дарвін',
    'Australia/Perth' => 'Австралія/Перт',
    'Australia/Brisbane' => 'Австралія/Брісбен',
    'Australia/Lindeman' => 'Австралія/Lindeman (Линдемен)',
    'Australia/Adelaide' => 'Австралія/Аделаїда',
    'Australia/Hobart' => 'Австралія/Хобарт',
    'Australia/Currie' => 'Австралія/Currie (Каррі)',
    'Australia/Melbourne' => 'Австралія/Мельбурн',
    'Australia/Sydney' => 'Австралія/Сідней',
    'Australia/Broken_Hill' => 'Австралія/Broken_Hill (Броукен Хілл)',
    'Indian/Christmas' => 'Індія/Christmas (Різдво)',
    'Pacific/Rarotonga' => 'Тихоокеанський регіон/Раротонга',
    'Indian/Cocos' => 'Індія/Cocos (Кокос)',
    'Pacific/Fiji' => 'Тихоокеанський регіон/Фіджі',
    'Pacific/Gambier' => 'Тихоокеанський регіон/Gambier (Гамбьєр)',
    'Pacific/Marquesas' => 'Тихоокеанський регіон/Маркізькі острови',
    'Pacific/Tahiti' => 'Тихоокеанський регіон/Таїті',
    'Pacific/Guam' => 'Тихоокеанський регіон/Гуам',
    'Pacific/Tarawa' => 'Тихоокеанський регіон/Тарава',
    'Pacific/Enderbury' => 'Тихоокеанський регіон/Enderbury (Эндербери)',
    'Pacific/Kiritimati' => 'Тихоокеанський регіон/Kiritimati (Киритимати)',
    'Pacific/Saipan' => 'Тихоокеанський регіон/Сайпан',
    'Pacific/Majuro' => 'Тихоокеанський регіон/Majuro (Маюро)',
    'Pacific/Kwajalein' => 'Тихоокеанський регіон/Kwajalein (Квайялейн)',
    'Pacific/Truk' => 'Тихоокеанський регіон/Truk (Трук)',
    'Pacific/Pohnpei' => 'Pacific/Pohnpei',
    'Pacific/Kosrae' => 'Тихоокеанський регіон/Kosrae (Косрае)',
    'Pacific/Nauru' => 'Тихоокеанський регіон/Науру',
    'Pacific/Noumea' => 'Тихоокеанський регіон/Нумєа',
    'Pacific/Auckland' => 'Тихоокеанський регіон/Окленд',
    'Pacific/Chatham' => 'Тихоокеанський регіон/Чатем',
    'Pacific/Niue' => 'Тихоокеанський регіон/Ніуе',
    'Pacific/Norfolk' => 'Тихоокеанський регіон/Норфолк',
    'Pacific/Palau' => 'Тихоокеанський регіон/Палау',
    'Pacific/Port_Moresby' => 'Тихоокеанський регіон/Порт-Морсбі',
    'Pacific/Pitcairn' => 'Тихоокеанський регіон/Pitcairn (Піткерн)',
    'Pacific/Pago_Pago' => 'Тихоокеанський регіон/Паго-Паго',
    'Pacific/Apia' => 'Тихоокеанський регіон/Апіа',
    'Pacific/Guadalcanal' => 'Тихоокеанський регіон/Гуадалканал',
    'Pacific/Fakaofo' => 'Тихоокеанський регіон/Ґамб&#039;єр (Факаофо)',
    'Pacific/Tongatapu' => 'Тихоокеанський регіон/Tongatapu (Тонгатапу)',
    'Pacific/Funafuti' => 'Тихоокеанський регіон/Фунафуті',
    'Pacific/Johnston' => 'Тихоокеанський регіон/Johnston (Джонстон)',
    'Pacific/Midway' => 'Тихоокеанський регіон/Острова Мідуей',
    'Pacific/Wake' => 'Тихоокеанський регіон/Wake (Уейк)',
    'Pacific/Efate' => 'Тихоокеанський регіон/Efate (Ефате)',
    'Pacific/Wallis' => 'Тихоокеанський регіон/Wallis (Уолліс)',
    'Europe/London' => 'Європа/Лондон',
    'Europe/Dublin' => 'Європа/Дублін',
    'WET' => 'WET (Western European Time - західноєвропейський час)',
    'CET' => 'CET (Central European Time - центральноєвропейський час)',
    'MET' => 'MET (Middle European Time - середньоєвропейський час)',
    'EET' => 'EET (East European Time - східноєвропейський час)',
    'Europe/Tirane' => 'Європа/Тирана',
    'Europe/Andorra' => 'Європа/Андорра',
    'Europe/Vienna' => 'Європа/Відень',
    'Europe/Minsk' => 'Європа/Мінськ',
    'Europe/Brussels' => 'Європа/Брюссель',
    'Europe/Sofia' => 'Європа/Софія',
    'Europe/Prague' => 'Європа/Прага',
    'Europe/Copenhagen' => 'Європа/Копенгаген',
    'Atlantic/Faeroe' => 'Атлантика/Faeroe (Фарерські)',
    'America/Danmarkshavn' => 'Америка/Danmarkshavn (Данмаркшавн)',
    'America/Scoresbysund' => 'Америка/Scoresbysund (Скорсбі)',
    'America/Godthab' => 'Америка/Godthab (Годтаб)',
    'America/Thule' => 'Америка/Thule (Тул)',
    'Europe/Tallinn' => 'Європа/Таллін',
    'Europe/Helsinki' => 'Європа/Гельсінкі',
    'Europe/Paris' => 'Європа/Париж',
    'Europe/Berlin' => 'Європа/Берлін',
    'Europe/Gibraltar' => 'Європа/Гібралтар',
    'Europe/Athens' => 'Європа/Афіни',
    'Europe/Budapest' => 'Європа/Будапешт',
    'Atlantic/Reykjavik' => 'Атлантика/Рейк&#039;явік',
    'Europe/Rome' => 'Європа/Рим',
    'Europe/Riga' => 'Європа/Рига',
    'Europe/Vaduz' => 'Європа/Вадуц',
    'Europe/Vilnius' => 'Європа/Вільнюс',
    'Europe/Luxembourg' => 'Європа/Люксембург',
    'Europe/Malta' => 'Європа/Мальта',
    'Europe/Chisinau' => 'Європа/Кишинів',
    'Europe/Monaco' => 'Європа/Монако',
    'Europe/Amsterdam' => 'Європа/Амстердам',
    'Europe/Oslo' => 'Європа/Осло',
    'Europe/Warsaw' => 'Європа/Варшава',
    'Europe/Lisbon' => 'Європа/Лісабон',
    'Atlantic/Azores' => 'Атлантика/Азорські острови',
    'Atlantic/Madeira' => 'Атлантика/Madeira (Мадейра)',
    'Europe/Bucharest' => 'Європа/Бухарест',
    'Europe/Kaliningrad' => 'Європа/Калінінград',
    'Europe/Moscow' => 'Європа/Москва',
    'Europe/Samara' => 'Європа/Самара',
    'Asia/Yekaterinburg' => 'Азія/Єкатеринбург',
    'Asia/Omsk' => 'Азія/Омськ',
    'Asia/Novosibirsk' => 'Азія/Новосибірськ',
    'Asia/Krasnoyarsk' => 'Азія/Красноярськ',
    'Asia/Irkutsk' => 'Азія/Іркутськ',
    'Asia/Yakutsk' => 'Азія/Якутськ',
    'Asia/Vladivostok' => 'Азія/Владивосток',
    'Asia/Sakhalin' => 'Азія/Сахалін',
    'Asia/Magadan' => 'Азія/Магадан',
    'Asia/Kamchatka' => 'Азія/Камчатка',
    'Asia/Anadyr' => 'Азія/Анадир',
    'Europe/Belgrade' => 'Європа/Белград',
    'Europe/Madrid' => 'Європа/Мадрид',
    'Africa/Ceuta' => 'Африка/Ceuta (Кеута)',
    'Atlantic/Canary' => 'Атлинтика/Канарські острови',
    'Europe/Stockholm' => 'Європа/Стокгольм',
    'Europe/Zurich' => 'Європа/Цюріх',
    'Europe/Istanbul' => 'Європа/Стамбул',
    'Europe/Kiev' => 'Європа/Київ',
    'Europe/Uzhgorod' => 'Європа/Ужгород',
    'Europe/Zaporozhye' => 'Європа/Запоріжжя',
    'Europe/Simferopol' => 'Європа/Сімферополь',
    'America/New_York' => 'Америка/Нью-Йорк',
    'America/Chicago' => 'Америка/Чикаго',
    'America/North_Dakota/Center' => 'Америка/Північна Дакота/Center (Центр)',
    'America/Denver' => 'Америка/Денвер',
    'America/Los_Angeles' => 'Америка/Лос-Анджелес',
    'America/Juneau' => 'Америка/Джуно',
    'America/Yakutat' => 'Америка/Yakutat (Якутат)',
    'America/Anchorage' => 'Америка/Anchorage (Анкораж)',
    'America/Nome' => 'Америка/Ном',
    'America/Adak' => 'Америка/Adak (Адак)',
    'Pacific/Honolulu' => 'Тихоокеанський регіон/Гонолулу',
    'America/Phoenix' => 'Америка/Фенікс',
    'America/Boise' => 'Америка/Бойсе',
    'America/Indiana/Indianapolis' => 'Америка/Indiana/Індіанаполіс',
    'America/Indiana/Marengo' => 'Америка/Indiana/Marengo (Маренго)',
    'America/Indiana/Knox' => 'Америка/Indiana/Knox (Кнокс)',
    'America/Indiana/Vevay' => 'Америка/Indiana/Vevay (Вевай)',
    'America/Kentucky/Louisville' => 'Америка/Кентуккі/Луїсвілл',
    'America/Kentucky/Monticello' => 'Америка/Кентуккі/Monticello (Монтичелло)',
    'America/Detroit' => 'Америка/Детройт',
    'America/Menominee' => 'Америка/Menominee (Меноміні)',
    'America/St_Johns' => 'Америка/St_Johns (Сент-Джонс)',
    'America/Goose_Bay' => 'Америка/Goose_Bay (Гуз Бей)',
    'America/Halifax' => 'Америка/Галіфакс',
    'America/Glace_Bay' => 'Америка/Glace_Bay (Глейс Бей)',
    'America/Montreal' => 'Америка/Монреаль',
    'America/Toronto' => 'Америка/Торонто',
    'America/Thunder_Bay' => 'Америка/Thunder_Bay (Санде Бей)',
    'America/Nipigon' => 'Америка/Ніпіґон',
    'America/Rainy_River' => 'Америка/Rainy_River (Рейни Ріве)',
    'America/Winnipeg' => 'Америка/Вінніпег',
    'America/Regina' => 'Америка/Риджайна',
    'America/Swift_Current' => 'Америка/Swift_Current (Свіфт Карент)',
    'America/Edmonton' => 'Америка/Едмонтон',
    'America/Vancouver' => 'Америка/Ванкувер',
    'America/Dawson_Creek' => 'Америка/Доусон-Крік',
    'America/Pangnirtung' => 'Америка/Панґніртунґ',
    'America/Iqaluit' => 'Америка/Ікалуіт',
    'America/Coral_Harbour' => 'Америка/Coral_Harbour (Корал Харбор)',
    'America/Rankin_Inlet' => 'Америка/Rankin_Inlet (Ранкін Інлет)',
    'America/Cambridge_Bay' => 'Америка/Cambridge_Bay (Кембрижд Бей)',
    'America/Yellowknife' => 'Америка/Yellowknife (Йелоу Найф)',
    'America/Inuvik' => 'Америка/Inuvik (Інувік)',
    'America/Whitehorse' => 'Америка/Whitehorse (Уайтхорс)',
    'America/Dawson' => 'Америка/Dawson (Доусон)',
    'America/Cancun' => 'Америка/Cancun (Канкун)',
    'America/Merida' => 'Америка/Merida (Меріда)',
    'America/Monterrey' => 'Америка/Монтеррей',
    'America/Mexico_City' => 'Америка/Мехіко',
    'America/Chihuahua' => 'Америка/Chihuahua (Чихуахуа)',
    'America/Hermosillo' => 'Америка/Hermosillo (Ермосільо)',
    'America/Mazatlan' => 'Америка/Mazatlan (Мазатлан)',
    'America/Tijuana' => 'Америка/Tijuana (Тіхуана)',
    'America/Anguilla' => 'Америка/Anguilla (Анкилла)',
    'America/Antigua' => 'Америка/Антигуа',
    'America/Nassau' => 'Америка/Нассау',
    'America/Barbados' => 'Америка/Барбадос',
    'America/Belize' => 'Америка/Беліз',
    'Atlantic/Bermuda' => 'Атлантика/Бермудські острови',
    'America/Cayman' => 'Америка/Кайманові острови',
    'America/Costa_Rica' => 'Америка/Коста-Ріка',
    'America/Havana' => 'Америка/Гавана',
    'America/Dominica' => 'Америка/Домініка',
    'America/Santo_Domingo' => 'Америка/Санто-Домінго',
    'America/El_Salvador' => 'Америка/Сальвадор',
    'America/Grenada' => 'Америка/Гренада',
    'America/Guadeloupe' => 'Америка/Гваделупа',
    'America/Guatemala' => 'Америка/Гватемала',
    'America/Port-au-Prince' => 'Америка/Порт-о-Пренс',
    'America/Tegucigalpa' => 'Америка/Тегусігальпа',
    'America/Jamaica' => 'Америка/Ямайка',
    'America/Martinique' => 'Америка/Мартініка',
    'America/Montserrat' => 'Америка/Montserrat (Монтсеррат)',
    'America/Managua' => 'Америка/Манагуа',
    'America/Panama' => 'Америка/Панама',
    'America/Puerto_Rico' => 'Америка/Пуерто-Ріко',
    'America/St_Kitts' => 'Америка/Сент-Китс',
    'America/St_Lucia' => 'Америка/Сент-Люсія',
    'America/Miquelon' => 'Америка/Miquelon (Мікелон)',
    'America/St_Vincent' => 'Америка/Сент-Вінсент',
    'America/Grand_Turk' => 'Америка/Grand_Turk (Гранд-Турк)',
    'America/Tortola' => 'Америка/Tortola (Тортола)',
    'America/St_Thomas' => 'Америка/St_Thomas (Сент Томас)',
    'America/Argentina/Buenos_Aires' => 'Америка/Аргентина/Буенос-Айрес',
    'America/Argentina/Cordoba' => 'Америка/Аргентина/Кордова',
    'America/Argentina/Tucuman' => 'Америка/Аргентина/Tucuman (Тукуман)',
    'America/Argentina/La_Rioja' => 'Америка/Аргентина/La_Rioja (Ла Риойя)',
    'America/Argentina/San_Juan' => 'Америка/Аргентина/Сан-Хуан',
    'America/Argentina/Jujuy' => 'Америка/Аргентина/Jujuy (Джуйю)',
    'America/Argentina/Catamarca' => 'Америка/Аргентина/Catamarca (Катамарка)',
    'America/Argentina/Mendoza' => 'Америка/Аргентина/Mendoza (Мендоза)',
    'America/Argentina/Rio_Gallegos' => 'Америка/Аргентина/Rio_Gallegos (Ріо-Галлегос)',
    'America/Argentina/Ushuaia' => 'Америка/Аргентина/Ушуая (Ушуайя)',
    'America/Aruba' => 'Америка/Аруба',
    'America/La_Paz' => 'Америка/Ла-Пас',
    'America/Noronha' => 'Америка/Noronha (Норонья)',
    'America/Belem' => 'Америка/Belem',
    'America/Fortaleza' => 'Америка/Fortaleza (Форталеза)',
    'America/Recife' => 'Америка/Ресифі',
    'America/Araguaina' => 'Америка/Araguaina (Арагуйяна)',
    'America/Maceio' => 'Америка/Maceio (Масейо)',
    'America/Bahia' => 'Америка/Bahia (Бахья)',
    'America/Sao_Paulo' => 'Америка/Сан-Паулу',
    'America/Campo_Grande' => 'Америка/Campo_Grande (Кампо Гранде)',
    'America/Cuiaba' => 'Америка/Cuiaba (Куйяба)',
    'America/Porto_Velho' => 'Америка/Porto_Velho (Порто Велхо)',
    'America/Boa_Vista' => 'Америка/Boa_Vista (Боа Vista)',
    'America/Manaus' => 'Америка/Манаус',
    'America/Eirunepe' => 'Америка/Ірунепе (Ейрунепе)',
    'America/Rio_Branco' => 'Америка/Rio_Branco (Ріо-Бранко)',
    'America/Santiago' => 'Америка/Сантьяго',
    'Pacific/Easter' => 'Тихоокеанський регіон/О. Пасхи',
    'America/Bogota' => 'Америка/Богота',
    'America/Curacao' => 'Америка/Кюрасао',
    'America/Guayaquil' => 'Америка/Гуаякіль',
    'Pacific/Galapagos' => 'Тихоокеанський регіон/Черепашачі острови',
    'Atlantic/Stanley' => 'Атлантика/Stanley (Стенлі)',
    'America/Cayenne' => 'Америка/Cayenne',
    'America/Guyana' => 'Америка/Гайана',
    'America/Asuncion' => 'Америка/Асунсьйон',
    'America/Lima' => 'Америка/Ліма',
    'Atlantic/South_Georgia' => 'Атлантика/South_Georgia (Саус Джорджія)',
    'America/Paramaribo' => 'Америка/Парамарібо',
    'America/Port_of_Spain' => 'Америка/Порт-оф-Спейн',
    'America/Montevideo' => 'Америка/Монтевідео',
    'America/Caracas' => 'Америка/Каракас',
);

$app_list_strings['eapm_list'] = array(
    'Sugar' => 'SuiteCRM',
    'WebEx' => 'WebEx',
    'GoToMeeting' => 'GoToMeeting',
    'IBMSmartCloud' => 'IBM SmartCloud',
    'Google' => 'Google Docs',
    'Box' => 'Box.net',
    'Facebook' => 'Facebook',
    'Twitter' => 'Twitter',
);
$app_list_strings['eapm_list_import'] = array(
    'Google' => 'Google Contacts',
);
$app_list_strings['eapm_list_documents'] = array(
    'Google' => 'Google Drive',
);
$app_list_strings['token_status'] = array(
    1 => 'Request',
    2 => 'Доступ до модуля',
    3 => 'Недійсне',
);

$app_list_strings ['emailTemplates_type_list'] = array(
    '' => '',
    'campaign' => 'Маркетингова кампанія:',
    'email' => 'E-mail',
);

$app_list_strings ['emailTemplates_type_list_campaigns'] = array(
    '' => '',
    'campaign' => 'Маркетингова кампанія:',
);

$app_list_strings ['emailTemplates_type_list_no_workflow'] = array(
    '' => '',
    'campaign' => 'Маркетингова кампанія:',
    'email' => 'E-mail',
    'system' => 'Система',
);

// knowledge base
$app_list_strings['moduleList']['AOK_KnowledgeBase'] = 'База знань'; // Shows in the ALL menu entries
$app_list_strings['moduleList']['AOK_Knowledge_Base_Categories'] = 'KB - Categories'; // Shows in the ALL menu entries
$app_list_strings['aok_status_list']['Draft'] = 'Чернетка';
$app_list_strings['aok_status_list']['Expired'] = 'Прострочений';
$app_list_strings['aok_status_list']['In_Review'] = 'У розгляді';
//$app_list_strings['aok_status_list']['Published'] = 'Published';
$app_list_strings['aok_status_list']['published_private'] = 'Private';
$app_list_strings['aok_status_list']['published_public'] = 'Public';

$app_list_strings['moduleList']['FP_events'] = 'Events';
$app_list_strings['moduleList']['FP_Event_Locations'] = 'Locations';

//events
$app_list_strings['fp_event_invite_status_dom']['Invited'] = 'Invited';
$app_list_strings['fp_event_invite_status_dom']['Not Invited'] = 'Not Invited';
$app_list_strings['fp_event_invite_status_dom']['Attended'] = 'Attended';
$app_list_strings['fp_event_invite_status_dom']['Not Attended'] = 'Not Attended';
$app_list_strings['fp_event_status_dom']['Accepted'] = 'Прийнято';
$app_list_strings['fp_event_status_dom']['Declined'] = 'Відхилено';
$app_list_strings['fp_event_status_dom']['No Response'] = 'No Response';

$app_strings['LBL_STATUS_EVENT'] = 'Invite Status';
$app_strings['LBL_ACCEPT_STATUS'] = 'Підтвердження';
$app_strings['LBL_LISTVIEW_OPTION_CURRENT'] = 'Select This Page';
$app_strings['LBL_LISTVIEW_OPTION_ENTIRE'] = 'Select All';
$app_strings['LBL_LISTVIEW_NONE'] = 'Deselect All';

//aod
$app_list_strings['moduleList']['AOD_IndexEvent'] = 'Index Event';
$app_list_strings['moduleList']['AOD_Index'] = 'Index';

$app_list_strings['moduleList']['AOP_Case_Events'] = 'Case Events';
$app_list_strings['moduleList']['AOP_Case_Updates'] = 'Case Updates';
$app_strings['LBL_AOP_EMAIL_REPLY_DELIMITER'] = '========== Please reply above this line ==========';



//aop
$app_list_strings['case_state_default_key'] = 'Open';
$app_list_strings['case_state_dom'] =
    array(
        'Open' => 'Opened',
        'Closed' => 'Закрито',
    );
$app_list_strings['case_status_default_key'] = 'Open_New';
$app_list_strings['case_status_dom'] =
    array(
        'Open_New' => 'Новий',
        'Open_Assigned' => 'Призначений',
        'Closed_Closed' => 'Закрито',
        'Open_Pending Input' => 'Очікування рішення',
        'Closed_Rejected' => 'Відхилена',
        'Closed_Duplicate' => 'Дублювати',
    );
$app_list_strings['contact_portal_user_type_dom'] =
    array(
        'Single' => 'Single user',
        'Account' => 'Account user',
    );
$app_list_strings['dom_email_distribution_for_auto_create'] = array(
    'AOPDefault' => 'Use AOP Default',
    'singleUser' => 'Single User',
    'roundRobin' => 'У циклі',
    'leastBusy' => 'Найменш зайнятої',
    'random' => 'Random',
);

//aor
$app_list_strings['moduleList']['AOR_Reports'] = 'Звіти';
$app_list_strings['moduleList']['AOR_Conditions'] = 'Report Conditions';
$app_list_strings['moduleList']['AOR_Charts'] = 'Графіки звітів';
$app_list_strings['moduleList']['AOR_Fields'] = 'Report Fields';
$app_list_strings['moduleList']['AOR_Scheduled_Reports'] = 'Scheduled Reports';
$app_list_strings['aor_operator_list']['Equal_To'] = 'Equal To';
$app_list_strings['aor_operator_list']['Not_Equal_To'] = 'Not Equal To';
$app_list_strings['aor_operator_list']['Greater_Than'] = 'Більше, ніж';
$app_list_strings['aor_operator_list']['Less_Than'] = 'Менше ніж';
$app_list_strings['aor_operator_list']['Greater_Than_or_Equal_To'] = 'Greater Than or Equal To';
$app_list_strings['aor_operator_list']['Less_Than_or_Equal_To'] = 'Less Than or Equal To';
$app_list_strings['aor_operator_list']['Contains'] = 'Містить';
$app_list_strings['aor_operator_list']['Not_Contains'] = 'Not Contains';
$app_list_strings['aor_operator_list']['Starts_With'] = 'Починається з';
$app_list_strings['aor_operator_list']['Ends_With'] = 'Ends With';
$app_list_strings['aor_format_options'][''] = '';
$app_list_strings['aor_format_options']['Y-m-d'] = 'Y-m-d';
$app_list_strings['aor_format_options']['Ymd'] = 'Ymd';
$app_list_strings['aor_format_options']['Y-m'] = 'Y-m';
$app_list_strings['aor_format_options']['d/m/Y'] = 'd/m/Y';
$app_list_strings['aor_format_options']['Y'] = 'Y';
$app_list_strings['aor_condition_operator_list']['And'] = 'And';
$app_list_strings['aor_condition_operator_list']['OR'] = 'OR';
$app_list_strings['aor_condition_type_list']['Value'] = 'Значення';
$app_list_strings['aor_condition_type_list']['Field'] = 'Поле';
$app_list_strings['aor_condition_type_list']['Date'] = 'Дата';
$app_list_strings['aor_condition_type_list']['Multi'] = 'One of';
$app_list_strings['aor_condition_type_list']['Period'] = 'Period';
$app_list_strings['aor_condition_type_list']['CurrentUserID'] = 'Current User';
$app_list_strings['aor_date_type_list'][''] = '';
$app_list_strings['aor_date_type_list']['minute'] = 'Хвилин';
$app_list_strings['aor_date_type_list']['hour'] = 'Годин';
$app_list_strings['aor_date_type_list']['day'] = 'Днів';
$app_list_strings['aor_date_type_list']['week'] = 'Тижнів';
$app_list_strings['aor_date_type_list']['month'] = 'Місяців';
$app_list_strings['aor_date_type_list']['business_hours'] = 'Business Hours';
$app_list_strings['aor_date_options']['now'] = 'Now';
$app_list_strings['aor_date_options']['field'] = 'This Field';
$app_list_strings['aor_date_operator']['now'] = '';
$app_list_strings['aor_date_operator']['plus'] = '+';
$app_list_strings['aor_date_operator']['minus'] = '-';
$app_list_strings['aor_sort_operator'][''] = '';
$app_list_strings['aor_sort_operator']['ASC'] = 'За зростанням';
$app_list_strings['aor_sort_operator']['DESC'] = 'За спаданням';
$app_list_strings['aor_function_list'][''] = '';
$app_list_strings['aor_function_list']['COUNT'] = 'Count';
$app_list_strings['aor_function_list']['MIN'] = 'Minimum';
$app_list_strings['aor_function_list']['MAX'] = 'Maximum';
$app_list_strings['aor_function_list']['SUM'] = 'Sum';
$app_list_strings['aor_function_list']['AVG'] = 'Average';
$app_list_strings['aor_total_options'][''] = '';
$app_list_strings['aor_total_options']['COUNT'] = 'Count';
$app_list_strings['aor_total_options']['SUM'] = 'Sum';
$app_list_strings['aor_total_options']['AVG'] = 'Average';
$app_list_strings['aor_chart_types']['bar'] = 'Bar chart';
$app_list_strings['aor_chart_types']['line'] = 'Line chart';
$app_list_strings['aor_chart_types']['pie'] = 'Pie chart';
$app_list_strings['aor_chart_types']['radar'] = 'Radar chart';
$app_list_strings['aor_chart_types']['polar'] = 'Polar chart';
$app_list_strings['aor_chart_types']['stacked_bar'] = 'Stacked bar';
$app_list_strings['aor_chart_types']['grouped_bar'] = 'Grouped bar';
$app_list_strings['aor_scheduled_report_schedule_types']['monthly'] = 'Щомісяця';
$app_list_strings['aor_scheduled_report_schedule_types']['weekly'] = 'Щотижня';
$app_list_strings['aor_scheduled_report_schedule_types']['daily'] = 'Щодня';
$app_list_strings['aor_scheduled_reports_status_dom']['active'] = 'Активна';
$app_list_strings['aor_scheduled_reports_status_dom']['inactive'] = 'Неактивна';
$app_list_strings['aor_email_type_list']['Email Address'] = 'E-mail';
$app_list_strings['aor_email_type_list']['Specify User'] = 'Користувач';
$app_list_strings['aor_email_type_list']['Users'] = 'Користувачі';
$app_list_strings['aor_assign_options']['all'] = 'ALL Users';
$app_list_strings['aor_assign_options']['role'] = 'ALL Users in Role';
$app_list_strings['aor_assign_options']['security_group'] = 'ALL Users in Security Group';
$app_list_strings['date_time_period_list']['today'] = 'Сьогодні';
$app_list_strings['date_time_period_list']['yesterday'] = 'Вчора';
$app_list_strings['date_time_period_list']['this_week'] = 'This Week';
$app_list_strings['date_time_period_list']['last_week'] = 'Минулий тиждень';
$app_list_strings['date_time_period_list']['last_month'] = 'Минулий місяць';
$app_list_strings['date_time_period_list']['this_month'] = 'Поточний місяць';
$app_list_strings['date_time_period_list']['this_quarter'] = 'Цей квартал';
$app_list_strings['date_time_period_list']['last_quarter'] = 'Минулий квартал';
$app_list_strings['date_time_period_list']['this_year'] = 'This year';
$app_list_strings['date_time_period_list']['last_year'] = 'Last year';
$app_strings['LBL_CRON_ON_THE_MONTHDAY'] = 'on the';
$app_strings['LBL_CRON_ON_THE_WEEKDAY'] = 'on';
$app_strings['LBL_CRON_AT'] = 'at';
$app_strings['LBL_CRON_RAW'] = 'Розширені';
$app_strings['LBL_CRON_MIN'] = 'Min';
$app_strings['LBL_CRON_HOUR'] = 'Hour';
$app_strings['LBL_CRON_DAY'] = 'День';
$app_strings['LBL_CRON_MONTH'] = 'Місяць';
$app_strings['LBL_CRON_DOW'] = 'DOW';
$app_strings['LBL_CRON_DAILY'] = 'Щодня';
$app_strings['LBL_CRON_WEEKLY'] = 'Щотижня';
$app_strings['LBL_CRON_MONTHLY'] = 'Щомісяця';

//aos
$app_list_strings['moduleList']['AOS_Contracts'] = 'Контракти';
$app_list_strings['moduleList']['AOS_Invoices'] = 'Рахунок-фактура';
$app_list_strings['moduleList']['AOS_PDF_Templates'] = 'PDF - Templates';
$app_list_strings['moduleList']['AOS_Product_Categories'] = 'Products - Categories';
$app_list_strings['moduleList']['AOS_Products'] = 'Товари';
$app_list_strings['moduleList']['AOS_Products_Quotes'] = 'Line Items';
$app_list_strings['moduleList']['AOS_Line_Item_Groups'] = 'Line Item Groups';
$app_list_strings['moduleList']['AOS_Quotes'] = 'Комерційну пропозицію';
$app_list_strings['aos_quotes_type_dom'][''] = '';
$app_list_strings['aos_quotes_type_dom']['Analyst'] = 'Аналітик';
$app_list_strings['aos_quotes_type_dom']['Competitor'] = 'Конкурент';
$app_list_strings['aos_quotes_type_dom']['Customer'] = 'Клієнт';
$app_list_strings['aos_quotes_type_dom']['Integrator'] = 'Системний аналітик';
$app_list_strings['aos_quotes_type_dom']['Investor'] = 'Інвестор';
$app_list_strings['aos_quotes_type_dom']['Partner'] = 'Партнер';
$app_list_strings['aos_quotes_type_dom']['Press'] = 'Преса';
$app_list_strings['aos_quotes_type_dom']['Prospect'] = 'Потенційний клієнт';
$app_list_strings['aos_quotes_type_dom']['Reseller'] = 'Посередник';
$app_list_strings['aos_quotes_type_dom']['Other'] = 'Інший';
$app_list_strings['template_ddown_c_list'][''] = '';
$app_list_strings['quote_stage_dom']['Draft'] = 'Чернетка';
$app_list_strings['quote_stage_dom']['Negotiation'] = 'Узгодження';
$app_list_strings['quote_stage_dom']['Delivered'] = 'Доставлено';
$app_list_strings['quote_stage_dom']['On Hold'] = 'Ведеться';
$app_list_strings['quote_stage_dom']['Confirmed'] = 'Підтверджено';
$app_list_strings['quote_stage_dom']['Closed Accepted'] = 'Прийнято та закрито';
$app_list_strings['quote_stage_dom']['Closed Lost'] = 'Закрито без успіху';
$app_list_strings['quote_stage_dom']['Closed Dead'] = 'Закрито з припиненням';
$app_list_strings['quote_term_dom']['Net 15'] = 'Nett 15';
$app_list_strings['quote_term_dom']['Net 30'] = 'Nett 30';
$app_list_strings['quote_term_dom'][''] = '';
$app_list_strings['approval_status_dom']['Approved'] = 'Approved';
$app_list_strings['approval_status_dom']['Not Approved'] = 'Not Approved';
$app_list_strings['approval_status_dom'][''] = '';
$app_list_strings['vat_list']['0.0'] = '0%';
$app_list_strings['vat_list']['5.0'] = '5%';
$app_list_strings['vat_list']['7.5'] = '7.5%';
$app_list_strings['vat_list']['17.5'] = '17.5%';
$app_list_strings['vat_list']['20.0'] = '20%';
$app_list_strings['discount_list']['Percentage'] = 'Pct';
$app_list_strings['discount_list']['Amount'] = 'Amt';
$app_list_strings['aos_invoices_type_dom'][''] = '';
$app_list_strings['aos_invoices_type_dom']['Analyst'] = 'Аналітик';
$app_list_strings['aos_invoices_type_dom']['Competitor'] = 'Конкурент';
$app_list_strings['aos_invoices_type_dom']['Customer'] = 'Клієнт';
$app_list_strings['aos_invoices_type_dom']['Integrator'] = 'Системний аналітик';
$app_list_strings['aos_invoices_type_dom']['Investor'] = 'Інвестор';
$app_list_strings['aos_invoices_type_dom']['Partner'] = 'Партнер';
$app_list_strings['aos_invoices_type_dom']['Press'] = 'Преса';
$app_list_strings['aos_invoices_type_dom']['Prospect'] = 'Потенційний клієнт';
$app_list_strings['aos_invoices_type_dom']['Reseller'] = 'Посередник';
$app_list_strings['aos_invoices_type_dom']['Other'] = 'Інший';
$app_list_strings['invoice_status_dom']['Paid'] = 'Paid';
$app_list_strings['invoice_status_dom']['Unpaid'] = 'Unpaid';
$app_list_strings['invoice_status_dom']['Cancelled'] = 'Cancelled';
$app_list_strings['invoice_status_dom'][''] = '';
$app_list_strings['quote_invoice_status_dom']['Not Invoiced'] = 'Not Invoiced';
$app_list_strings['quote_invoice_status_dom']['Invoiced'] = 'Invoiced';
$app_list_strings['product_code_dom']['XXXX'] = 'XXXX';
$app_list_strings['product_code_dom']['YYYY'] = 'YYYY';
$app_list_strings['product_category_dom']['Laptops'] = 'Laptops';
$app_list_strings['product_category_dom']['Desktops'] = 'Desktops';
$app_list_strings['product_category_dom'][''] = '';
$app_list_strings['product_type_dom']['Good'] = 'Good';
$app_list_strings['product_type_dom']['Service'] = 'Service';
$app_list_strings['product_quote_parent_type_dom']['AOS_Quotes'] = 'Комерційну пропозицію';
$app_list_strings['product_quote_parent_type_dom']['AOS_Invoices'] = 'Рахунок-фактура';
$app_list_strings['product_quote_parent_type_dom']['AOS_Contracts'] = 'Контракти';
$app_list_strings['pdf_template_type_dom']['AOS_Quotes'] = 'Комерційну пропозицію';
$app_list_strings['pdf_template_type_dom']['AOS_Invoices'] = 'Рахунок-фактура';
$app_list_strings['pdf_template_type_dom']['AOS_Contracts'] = 'Контракти';
$app_list_strings['pdf_template_type_dom']['Accounts'] = 'Облікові записи';
$app_list_strings['pdf_template_type_dom']['Contacts'] = 'Контакти';
$app_list_strings['pdf_template_type_dom']['Leads'] = 'Попередні контакти';
$app_list_strings['pdf_template_sample_dom'][''] = '';
$app_list_strings['contract_status_list']['Not Started'] = 'Проект';
$app_list_strings['contract_status_list']['In Progress'] = 'У процесі';
$app_list_strings['contract_status_list']['Signed'] = 'Signed';
$app_list_strings['contract_type_list']['Type'] = 'Тип';
$app_strings['LBL_PRINT_AS_PDF'] = 'Print as PDF';
$app_strings['LBL_SELECT_TEMPLATE'] = 'Please Select a Template';
$app_strings['LBL_NO_TEMPLATE'] = 'ERROR\nNo templates found.\nPlease go to the PDF templates module and create one';

//aow
$app_list_strings['moduleList']['AOW_WorkFlow'] = 'WorkFlow';
$app_list_strings['moduleList']['AOW_Conditions'] = 'Умови бізнес-процесу';
$app_list_strings['moduleList']['AOW_Processed'] = 'Process Audit';
$app_list_strings['moduleList']['AOW_Actions'] = 'WorkFlow Actions';
$app_list_strings['aow_status_list']['Active'] = 'Активна';
$app_list_strings['aow_status_list']['Inactive'] = 'Неактивна';
$app_list_strings['aow_operator_list']['Equal_To'] = 'Equal To';
$app_list_strings['aow_operator_list']['Not_Equal_To'] = 'Not Equal To';
$app_list_strings['aow_operator_list']['Greater_Than'] = 'Більше, ніж';
$app_list_strings['aow_operator_list']['Less_Than'] = 'Менше ніж';
$app_list_strings['aow_operator_list']['Greater_Than_or_Equal_To'] = 'Greater Than or Equal To';
$app_list_strings['aow_operator_list']['Less_Than_or_Equal_To'] = 'Less Than or Equal To';
$app_list_strings['aow_operator_list']['Contains'] = 'Містить';
$app_list_strings['aow_operator_list']['Starts_With'] = 'Починається з';
$app_list_strings['aow_operator_list']['Ends_With'] = 'Ends With';
$app_list_strings['aow_operator_list']['is_null'] = 'Без дати';
$app_list_strings['aow_sql_operator_list']['Equal_To'] = '=';
$app_list_strings['aow_sql_operator_list']['Not_Equal_To'] = '!=';
$app_list_strings['aow_sql_operator_list']['Greater_Than'] = '>';
$app_list_strings['aow_sql_operator_list']['Less_Than'] = '<';
$app_list_strings['aow_sql_operator_list']['Greater_Than_or_Equal_To'] = '>=';
$app_list_strings['aow_sql_operator_list']['Less_Than_or_Equal_To'] = '<=';
$app_list_strings['aow_sql_operator_list']['Contains'] = 'LIKE';
$app_list_strings['aow_sql_operator_list']['Starts_With'] = 'LIKE';
$app_list_strings['aow_sql_operator_list']['Ends_With'] = 'LIKE';
$app_list_strings['aow_sql_operator_list']['is_null'] = 'IS NULL';
$app_list_strings['aow_process_status_list']['Complete'] = 'Завершена';
$app_list_strings['aow_process_status_list']['Running'] = 'Running';
$app_list_strings['aow_process_status_list']['Pending'] = 'В очікуванні';
$app_list_strings['aow_process_status_list']['Failed'] = 'Невдало';
$app_list_strings['aow_condition_operator_list']['And'] = 'And';
$app_list_strings['aow_condition_operator_list']['OR'] = 'OR';
$app_list_strings['aow_condition_type_list']['Value'] = 'Значення';
$app_list_strings['aow_condition_type_list']['Field'] = 'Поле';
$app_list_strings['aow_condition_type_list']['Any_Change'] = 'Any Change';
$app_list_strings['aow_condition_type_list']['SecurityGroup'] = 'In SecurityGroup';
$app_list_strings['aow_condition_type_list']['Date'] = 'Дата';
$app_list_strings['aow_condition_type_list']['Multi'] = 'One of';
$app_list_strings['aow_action_type_list']['Value'] = 'Значення';
$app_list_strings['aow_action_type_list']['Field'] = 'Поле';
$app_list_strings['aow_action_type_list']['Date'] = 'Дата';
$app_list_strings['aow_action_type_list']['Round_Robin'] = 'Round Robin';
$app_list_strings['aow_action_type_list']['Least_Busy'] = 'Least Busy';
$app_list_strings['aow_action_type_list']['Random'] = 'Random';
$app_list_strings['aow_rel_action_type_list']['Value'] = 'Значення';
$app_list_strings['aow_rel_action_type_list']['Field'] = 'Поле';
$app_list_strings['aow_date_type_list'][''] = '';
$app_list_strings['aow_date_type_list']['minute'] = 'Хвилин';
$app_list_strings['aow_date_type_list']['hour'] = 'Годин';
$app_list_strings['aow_date_type_list']['day'] = 'Днів';
$app_list_strings['aow_date_type_list']['week'] = 'Тижнів';
$app_list_strings['aow_date_type_list']['month'] = 'Місяців';
$app_list_strings['aow_date_type_list']['business_hours'] = 'Business Hours';
$app_list_strings['aow_date_options']['now'] = 'Now';
$app_list_strings['aow_date_options']['today'] = 'Сьогодні';
$app_list_strings['aow_date_options']['field'] = 'This Field';
$app_list_strings['aow_date_operator']['now'] = '';
$app_list_strings['aow_date_operator']['plus'] = '+';
$app_list_strings['aow_date_operator']['minus'] = '-';
$app_list_strings['aow_assign_options']['all'] = 'ALL Users';
$app_list_strings['aow_assign_options']['role'] = 'ALL Users in Role';
$app_list_strings['aow_assign_options']['security_group'] = 'ALL Users in Security Group';
$app_list_strings['aow_email_type_list']['Email Address'] = 'E-mail';
$app_list_strings['aow_email_type_list']['Record Email'] = 'Record Email';
$app_list_strings['aow_email_type_list']['Related Field'] = 'Related Field';
$app_list_strings['aow_email_type_list']['Specify User'] = 'Користувач';
$app_list_strings['aow_email_type_list']['Users'] = 'Користувачі';
$app_list_strings['aow_email_type_list']['Record Field'] = 'Поле';
$app_list_strings['aow_email_to_list']['to'] = 'Кому';
$app_list_strings['aow_email_to_list']['cc'] = 'Cc';
$app_list_strings['aow_email_to_list']['bcc'] = 'Bcc';
$app_list_strings['aow_run_on_list']['All_Records'] = 'All Records';
$app_list_strings['aow_run_on_list']['New_Records'] = 'New Records';
$app_list_strings['aow_run_on_list']['Modified_Records'] = 'Modified Records';
$app_list_strings['aow_run_when_list']['Always'] = 'Завжди';
$app_list_strings['aow_run_when_list']['On_Save'] = 'Only On Save';
$app_list_strings['aow_run_when_list']['In_Scheduler'] = 'Only In The Scheduler';

//gant
$app_list_strings['moduleList']['AM_ProjectTemplates'] = 'Projects - Templates';
$app_list_strings['moduleList']['AM_TaskTemplates'] = 'Project Task Templates';
$app_list_strings['relationship_type_list']['FS'] = 'Finish to Start';
$app_list_strings['relationship_type_list']['SS'] = 'Start to Start';
$app_list_strings['duration_unit_dom']['Days'] = 'Днів';
$app_list_strings['duration_unit_dom']['Hours'] = 'Годин';
$app_strings['LBL_GANTT_BUTTON_LABEL'] = 'View Gantt';
$app_strings['LBL_DETAIL_BUTTON_LABEL'] = 'View Detail';
$app_strings['LBL_CREATE_PROJECT'] = 'Новий проект';

//gmaps
$app_strings['LBL_MAP'] = 'Map';

$app_strings['LBL_JJWG_MAPS_LNG'] = 'Longitude';
$app_strings['LBL_JJWG_MAPS_LAT'] = 'Latitude';
$app_strings['LBL_JJWG_MAPS_GEOCODE_STATUS'] = 'Geocode Status';
$app_strings['LBL_JJWG_MAPS_ADDRESS'] = 'Адреса';

$app_list_strings['moduleList']['jjwg_Maps'] = 'Maps';
$app_list_strings['moduleList']['jjwg_Markers'] = 'Maps - Markers';
$app_list_strings['moduleList']['jjwg_Areas'] = 'Maps - Areas';
$app_list_strings['moduleList']['jjwg_Address_Cache'] = 'Maps - Address Cache';

$app_list_strings['moduleList']['jjwp_Partners'] = 'JJWP Partners';

$app_list_strings['map_unit_type_list']['mi'] = 'Miles';
$app_list_strings['map_unit_type_list']['km'] = 'Kilometers';

$app_list_strings['map_module_type_list']['Accounts'] = 'Облікові записи';
$app_list_strings['map_module_type_list']['Contacts'] = 'Контакти';
$app_list_strings['map_module_type_list']['Cases'] = 'Звернення';
$app_list_strings['map_module_type_list']['Leads'] = 'Попередні контакти';
$app_list_strings['map_module_type_list']['Meetings'] = 'Зустрічі';
$app_list_strings['map_module_type_list']['Opportunities'] = 'Угоди';
$app_list_strings['map_module_type_list']['Project'] = 'Проекти';
$app_list_strings['map_module_type_list']['Prospects'] = 'Огляд потенційних клієнтів';

$app_list_strings['map_relate_type_list']['Accounts'] = 'Контрагент';
$app_list_strings['map_relate_type_list']['Contacts'] = 'Контакт';
$app_list_strings['map_relate_type_list']['Cases'] = 'Звернення';
$app_list_strings['map_relate_type_list']['Leads'] = 'Попередній контакт';
$app_list_strings['map_relate_type_list']['Meetings'] = 'Зустріч';
$app_list_strings['map_relate_type_list']['Opportunities'] = 'Угода';
$app_list_strings['map_relate_type_list']['Project'] = 'Проекти';
$app_list_strings['map_relate_type_list']['Prospects'] = 'Адресат';

$app_list_strings['marker_image_list']['accident'] = 'Accident';
$app_list_strings['marker_image_list']['administration'] = 'Адміністрування';
$app_list_strings['marker_image_list']['agriculture'] = 'Agriculture';
$app_list_strings['marker_image_list']['aircraft_small'] = 'Aircraft Small';
$app_list_strings['marker_image_list']['airplane_tourism'] = 'Airplane Tourism';
$app_list_strings['marker_image_list']['airport'] = 'Airport';
$app_list_strings['marker_image_list']['amphitheater'] = 'Amphitheater';
$app_list_strings['marker_image_list']['apartment'] = 'Apartment';
$app_list_strings['marker_image_list']['aquarium'] = 'Aquarium';
$app_list_strings['marker_image_list']['arch'] = 'Arch';
$app_list_strings['marker_image_list']['atm'] = 'Atm';
$app_list_strings['marker_image_list']['audio'] = 'Audio';
$app_list_strings['marker_image_list']['bank'] = 'Bank';
$app_list_strings['marker_image_list']['bank_euro'] = 'Bank Euro';
$app_list_strings['marker_image_list']['bank_pound'] = 'Bank Pound';
$app_list_strings['marker_image_list']['bar'] = 'Bar';
$app_list_strings['marker_image_list']['beach'] = 'Beach';
$app_list_strings['marker_image_list']['beautiful'] = 'Beautiful';
$app_list_strings['marker_image_list']['bicycle_parking'] = 'Bicycle Parking';
$app_list_strings['marker_image_list']['big_city'] = 'Big City';
$app_list_strings['marker_image_list']['bridge'] = 'Bridge';
$app_list_strings['marker_image_list']['bridge_modern'] = 'Bridge Modern';
$app_list_strings['marker_image_list']['bus'] = 'Bus';
$app_list_strings['marker_image_list']['cable_car'] = 'Cable Car';
$app_list_strings['marker_image_list']['car'] = 'Car';
$app_list_strings['marker_image_list']['car_rental'] = 'Car Rental';
$app_list_strings['marker_image_list']['carrepair'] = 'Carrepair';
$app_list_strings['marker_image_list']['castle'] = 'Castle';
$app_list_strings['marker_image_list']['cathedral'] = 'Cathedral';
$app_list_strings['marker_image_list']['chapel'] = 'Chapel';
$app_list_strings['marker_image_list']['church'] = 'Church';
$app_list_strings['marker_image_list']['city_square'] = 'City Square';
$app_list_strings['marker_image_list']['cluster'] = 'Cluster';
$app_list_strings['marker_image_list']['cluster_2'] = 'Cluster 2';
$app_list_strings['marker_image_list']['cluster_3'] = 'Cluster 3';
$app_list_strings['marker_image_list']['cluster_4'] = 'Cluster 4';
$app_list_strings['marker_image_list']['cluster_5'] = 'Cluster 5';
$app_list_strings['marker_image_list']['coffee'] = 'Coffee';
$app_list_strings['marker_image_list']['community_centre'] = 'Community Centre';
$app_list_strings['marker_image_list']['company'] = 'Компанія';
$app_list_strings['marker_image_list']['conference'] = 'Конференція';
$app_list_strings['marker_image_list']['construction'] = 'Будівництво';
$app_list_strings['marker_image_list']['convenience'] = 'Convenience';
$app_list_strings['marker_image_list']['court'] = 'Court';
$app_list_strings['marker_image_list']['cruise'] = 'Cruise';
$app_list_strings['marker_image_list']['currency_exchange'] = 'Currency Exchange';
$app_list_strings['marker_image_list']['customs'] = 'Customs';
$app_list_strings['marker_image_list']['cycling'] = 'Cycling';
$app_list_strings['marker_image_list']['dam'] = 'Dam';
$app_list_strings['marker_image_list']['days_dim'] = 'Days Dim';
$app_list_strings['marker_image_list']['days_dom'] = 'Days Dom';
$app_list_strings['marker_image_list']['days_jeu'] = 'Days Jeu';
$app_list_strings['marker_image_list']['days_jue'] = 'Days Jue';
$app_list_strings['marker_image_list']['days_lun'] = 'Days Lun';
$app_list_strings['marker_image_list']['days_mar'] = 'Days Mar';
$app_list_strings['marker_image_list']['days_mer'] = 'Days Mer';
$app_list_strings['marker_image_list']['days_mie'] = 'Days Mie';
$app_list_strings['marker_image_list']['days_qua'] = 'Days Qua';
$app_list_strings['marker_image_list']['days_qui'] = 'Days Qui';
$app_list_strings['marker_image_list']['days_sab'] = 'Days Sab';
$app_list_strings['marker_image_list']['days_sam'] = 'Days Sam';
$app_list_strings['marker_image_list']['days_seg'] = 'Days Seg';
$app_list_strings['marker_image_list']['days_sex'] = 'Days Sex';
$app_list_strings['marker_image_list']['days_ter'] = 'Days Ter';
$app_list_strings['marker_image_list']['days_ven'] = 'Days Ven';
$app_list_strings['marker_image_list']['days_vie'] = 'Days Vie';
$app_list_strings['marker_image_list']['dentist'] = 'Dentist';
$app_list_strings['marker_image_list']['deptartment_store'] = 'Deptartment Store';
$app_list_strings['marker_image_list']['disability'] = 'Disability';
$app_list_strings['marker_image_list']['disabled_parking'] = 'Disabled Parking';
$app_list_strings['marker_image_list']['doctor'] = 'Doctor';
$app_list_strings['marker_image_list']['dog_leash'] = 'Dog Leash';
$app_list_strings['marker_image_list']['down'] = 'Down';
$app_list_strings['marker_image_list']['down_left'] = 'Down Left';
$app_list_strings['marker_image_list']['down_right'] = 'Down Right';
$app_list_strings['marker_image_list']['down_then_left'] = 'Down Then Left';
$app_list_strings['marker_image_list']['down_then_right'] = 'Down Then Right';
$app_list_strings['marker_image_list']['drugs'] = 'Drugs';
$app_list_strings['marker_image_list']['elevator'] = 'Elevator';
$app_list_strings['marker_image_list']['embassy'] = 'Embassy';
$app_list_strings['marker_image_list']['expert'] = 'Expert';
$app_list_strings['marker_image_list']['factory'] = 'Factory';
$app_list_strings['marker_image_list']['falling_rocks'] = 'Falling Rocks';
$app_list_strings['marker_image_list']['fast_food'] = 'Fast Food';
$app_list_strings['marker_image_list']['festival'] = 'Festival';
$app_list_strings['marker_image_list']['fjord'] = 'Fjord';
$app_list_strings['marker_image_list']['forest'] = 'Forest';
$app_list_strings['marker_image_list']['fountain'] = 'Fountain';
$app_list_strings['marker_image_list']['friday'] = 'П&#039;ятниця';
$app_list_strings['marker_image_list']['garden'] = 'Garden';
$app_list_strings['marker_image_list']['gas_station'] = 'Gas Station';
$app_list_strings['marker_image_list']['geyser'] = 'Geyser';
$app_list_strings['marker_image_list']['gifts'] = 'Gifts';
$app_list_strings['marker_image_list']['gourmet'] = 'Gourmet';
$app_list_strings['marker_image_list']['grocery'] = 'Grocery';
$app_list_strings['marker_image_list']['hairsalon'] = 'Hairsalon';
$app_list_strings['marker_image_list']['helicopter'] = 'Helicopter';
$app_list_strings['marker_image_list']['highway'] = 'Highway';
$app_list_strings['marker_image_list']['historical_quarter'] = 'Historical Quarter';
$app_list_strings['marker_image_list']['home'] = 'Головна';
$app_list_strings['marker_image_list']['hospital'] = 'Hospital';
$app_list_strings['marker_image_list']['hostel'] = 'Hostel';
$app_list_strings['marker_image_list']['hotel'] = 'Hotel';
$app_list_strings['marker_image_list']['hotel_1_star'] = 'Hotel 1 Star';
$app_list_strings['marker_image_list']['hotel_2_stars'] = 'Hotel 2 Stars';
$app_list_strings['marker_image_list']['hotel_3_stars'] = 'Hotel 3 Stars';
$app_list_strings['marker_image_list']['hotel_4_stars'] = 'Hotel 4 Stars';
$app_list_strings['marker_image_list']['hotel_5_stars'] = 'Hotel 5 Stars';
$app_list_strings['marker_image_list']['info'] = 'Інформація';
$app_list_strings['marker_image_list']['justice'] = 'Justice';
$app_list_strings['marker_image_list']['lake'] = 'Lake';
$app_list_strings['marker_image_list']['laundromat'] = 'Laundromat';
$app_list_strings['marker_image_list']['left'] = 'Left';
$app_list_strings['marker_image_list']['left_then_down'] = 'Left Then Down';
$app_list_strings['marker_image_list']['left_then_up'] = 'Left Then Up';
$app_list_strings['marker_image_list']['library'] = 'Library';
$app_list_strings['marker_image_list']['lighthouse'] = 'Lighthouse';
$app_list_strings['marker_image_list']['liquor'] = 'Liquor';
$app_list_strings['marker_image_list']['lock'] = 'Lock';
$app_list_strings['marker_image_list']['main_road'] = 'Main Road';
$app_list_strings['marker_image_list']['massage'] = 'Massage';
$app_list_strings['marker_image_list']['mobile_phone_tower'] = 'Mobile Phone Tower';
$app_list_strings['marker_image_list']['modern_tower'] = 'Modern Tower';
$app_list_strings['marker_image_list']['monastery'] = 'Monastery';
$app_list_strings['marker_image_list']['monday'] = 'Понеділок';
$app_list_strings['marker_image_list']['monument'] = 'Monument';
$app_list_strings['marker_image_list']['mosque'] = 'Mosque';
$app_list_strings['marker_image_list']['motorcycle'] = 'Motorcycle';
$app_list_strings['marker_image_list']['museum'] = 'Museum';
$app_list_strings['marker_image_list']['music_live'] = 'Music Live';
$app_list_strings['marker_image_list']['oil_pump_jack'] = 'Oil Pump Jack';
$app_list_strings['marker_image_list']['pagoda'] = 'Pagoda';
$app_list_strings['marker_image_list']['palace'] = 'Palace';
$app_list_strings['marker_image_list']['panoramic'] = 'Panoramic';
$app_list_strings['marker_image_list']['park'] = 'Park';
$app_list_strings['marker_image_list']['park_and_ride'] = 'Park And Ride';
$app_list_strings['marker_image_list']['parking'] = 'Parking';
$app_list_strings['marker_image_list']['photo'] = 'Фотографія';
$app_list_strings['marker_image_list']['picnic'] = 'Picnic';
$app_list_strings['marker_image_list']['places_unvisited'] = 'Places Unvisited';
$app_list_strings['marker_image_list']['places_visited'] = 'Places Visited';
$app_list_strings['marker_image_list']['playground'] = 'Playground';
$app_list_strings['marker_image_list']['police'] = 'Police';
$app_list_strings['marker_image_list']['port'] = 'Порт';
$app_list_strings['marker_image_list']['postal'] = 'Postal';
$app_list_strings['marker_image_list']['power_line_pole'] = 'Power Line Pole';
$app_list_strings['marker_image_list']['power_plant'] = 'Power Plant';
$app_list_strings['marker_image_list']['power_substation'] = 'Power Substation';
$app_list_strings['marker_image_list']['public_art'] = 'Public Art';
$app_list_strings['marker_image_list']['rain'] = 'Rain';
$app_list_strings['marker_image_list']['real_estate'] = 'Real Estate';
$app_list_strings['marker_image_list']['regroup'] = 'Regroup';
$app_list_strings['marker_image_list']['resort'] = 'Resort';
$app_list_strings['marker_image_list']['restaurant'] = 'Restaurant';
$app_list_strings['marker_image_list']['restaurant_african'] = 'Restaurant African';
$app_list_strings['marker_image_list']['restaurant_barbecue'] = 'Restaurant Barbecue';
$app_list_strings['marker_image_list']['restaurant_buffet'] = 'Restaurant Buffet';
$app_list_strings['marker_image_list']['restaurant_chinese'] = 'Restaurant Chinese';
$app_list_strings['marker_image_list']['restaurant_fish'] = 'Restaurant Fish';
$app_list_strings['marker_image_list']['restaurant_fish_chips'] = 'Restaurant Fish Chips';
$app_list_strings['marker_image_list']['restaurant_gourmet'] = 'Restaurant Gourmet';
$app_list_strings['marker_image_list']['restaurant_greek'] = 'Restaurant Greek';
$app_list_strings['marker_image_list']['restaurant_indian'] = 'Restaurant Indian';
$app_list_strings['marker_image_list']['restaurant_italian'] = 'Restaurant Italian';
$app_list_strings['marker_image_list']['restaurant_japanese'] = 'Restaurant Japanese';
$app_list_strings['marker_image_list']['restaurant_kebab'] = 'Restaurant Kebab';
$app_list_strings['marker_image_list']['restaurant_korean'] = 'Restaurant Korean';
$app_list_strings['marker_image_list']['restaurant_mediterranean'] = 'Restaurant Mediterranean';
$app_list_strings['marker_image_list']['restaurant_mexican'] = 'Restaurant Mexican';
$app_list_strings['marker_image_list']['restaurant_romantic'] = 'Restaurant Romantic';
$app_list_strings['marker_image_list']['restaurant_thai'] = 'Restaurant Thai';
$app_list_strings['marker_image_list']['restaurant_turkish'] = 'Restaurant Turkish';
$app_list_strings['marker_image_list']['right'] = 'Right';
$app_list_strings['marker_image_list']['right_then_down'] = 'Right Then Down';
$app_list_strings['marker_image_list']['right_then_up'] = 'Right Then Up';
$app_list_strings['marker_image_list']['saturday'] = 'Субота';
$app_list_strings['marker_image_list']['school'] = 'School';
$app_list_strings['marker_image_list']['shopping_mall'] = 'Shopping Mall';
$app_list_strings['marker_image_list']['shore'] = 'Shore';
$app_list_strings['marker_image_list']['sight'] = 'Sight';
$app_list_strings['marker_image_list']['small_city'] = 'Small City';
$app_list_strings['marker_image_list']['snow'] = 'Snow';
$app_list_strings['marker_image_list']['spaceport'] = 'Spaceport';
$app_list_strings['marker_image_list']['speed_100'] = 'Speed 100';
$app_list_strings['marker_image_list']['speed_110'] = 'Speed 110';
$app_list_strings['marker_image_list']['speed_120'] = 'Speed 120';
$app_list_strings['marker_image_list']['speed_130'] = 'Speed 130';
$app_list_strings['marker_image_list']['speed_20'] = 'Speed 20';
$app_list_strings['marker_image_list']['speed_30'] = 'Speed 30';
$app_list_strings['marker_image_list']['speed_40'] = 'Speed 40';
$app_list_strings['marker_image_list']['speed_50'] = 'Speed 50';
$app_list_strings['marker_image_list']['speed_60'] = 'Speed 60';
$app_list_strings['marker_image_list']['speed_70'] = 'Speed 70';
$app_list_strings['marker_image_list']['speed_80'] = 'Speed 80';
$app_list_strings['marker_image_list']['speed_90'] = 'Speed 90';
$app_list_strings['marker_image_list']['speed_hump'] = 'Speed Hump';
$app_list_strings['marker_image_list']['stadium'] = 'Stadium';
$app_list_strings['marker_image_list']['statue'] = 'Statue';
$app_list_strings['marker_image_list']['steam_train'] = 'Steam Train';
$app_list_strings['marker_image_list']['stop'] = 'Stop';
$app_list_strings['marker_image_list']['stoplight'] = 'Stoplight';
$app_list_strings['marker_image_list']['subway'] = 'Subway';
$app_list_strings['marker_image_list']['sun'] = 'Нд';
$app_list_strings['marker_image_list']['sunday'] = 'Неділя';
$app_list_strings['marker_image_list']['supermarket'] = 'Supermarket';
$app_list_strings['marker_image_list']['synagogue'] = 'Synagogue';
$app_list_strings['marker_image_list']['tapas'] = 'Tapas';
$app_list_strings['marker_image_list']['taxi'] = 'Taxi';
$app_list_strings['marker_image_list']['taxiway'] = 'Taxiway';
$app_list_strings['marker_image_list']['teahouse'] = 'Teahouse';
$app_list_strings['marker_image_list']['telephone'] = 'Telephone';
$app_list_strings['marker_image_list']['temple_hindu'] = 'Temple Hindu';
$app_list_strings['marker_image_list']['terrace'] = 'Terrace';
$app_list_strings['marker_image_list']['text'] = 'Text';
$app_list_strings['marker_image_list']['theater'] = 'Theater';
$app_list_strings['marker_image_list']['theme_park'] = 'Theme Park';
$app_list_strings['marker_image_list']['thursday'] = 'Четвер';
$app_list_strings['marker_image_list']['toilets'] = 'Toilets';
$app_list_strings['marker_image_list']['toll_station'] = 'Toll Station';
$app_list_strings['marker_image_list']['tower'] = 'Tower';
$app_list_strings['marker_image_list']['traffic_enforcement_camera'] = 'Traffic Enforcement Camera';
$app_list_strings['marker_image_list']['train'] = 'Train';
$app_list_strings['marker_image_list']['tram'] = 'Tram';
$app_list_strings['marker_image_list']['truck'] = 'Truck';
$app_list_strings['marker_image_list']['tuesday'] = 'Вівторок';
$app_list_strings['marker_image_list']['tunnel'] = 'Tunnel';
$app_list_strings['marker_image_list']['turn_left'] = 'Turn Left';
$app_list_strings['marker_image_list']['turn_right'] = 'Turn Right';
$app_list_strings['marker_image_list']['university'] = 'University';
$app_list_strings['marker_image_list']['up'] = 'Up';
$app_list_strings['marker_image_list']['up_left'] = 'Up Left';
$app_list_strings['marker_image_list']['up_right'] = 'Up Right';
$app_list_strings['marker_image_list']['up_then_left'] = 'Up Then Left';
$app_list_strings['marker_image_list']['up_then_right'] = 'Up Then Right';
$app_list_strings['marker_image_list']['vespa'] = 'Vespa';
$app_list_strings['marker_image_list']['video'] = 'Video';
$app_list_strings['marker_image_list']['villa'] = 'Villa';
$app_list_strings['marker_image_list']['water'] = 'Water';
$app_list_strings['marker_image_list']['waterfall'] = 'Waterfall';
$app_list_strings['marker_image_list']['watermill'] = 'Watermill';
$app_list_strings['marker_image_list']['waterpark'] = 'Waterpark';
$app_list_strings['marker_image_list']['watertower'] = 'Watertower';
$app_list_strings['marker_image_list']['wednesday'] = 'Середа';
$app_list_strings['marker_image_list']['wifi'] = 'Wifi';
$app_list_strings['marker_image_list']['wind_turbine'] = 'Wind Turbine';
$app_list_strings['marker_image_list']['windmill'] = 'Windmill';
$app_list_strings['marker_image_list']['winery'] = 'Winery';
$app_list_strings['marker_image_list']['work_office'] = 'Work Office';
$app_list_strings['marker_image_list']['world_heritage_site'] = 'World Heritage Site';
$app_list_strings['marker_image_list']['zoo'] = 'Zoo';

//Reschedule
$app_list_strings['call_reschedule_dom'][''] = '';
$app_list_strings['call_reschedule_dom']['Out of Office'] = 'Out of Office';
$app_list_strings['call_reschedule_dom']['In a Meeting'] = 'In a Meeting';

$app_strings['LBL_RESCHEDULE_LABEL'] = 'Перепланувати';
$app_strings['LBL_RESCHEDULE_TITLE'] = 'Please enter the reschedule information';
$app_strings['LBL_RESCHEDULE_DATE'] = 'Дата';
$app_strings['LBL_RESCHEDULE_REASON'] = 'Reason:';
$app_strings['LBL_RESCHEDULE_ERROR1'] = 'Please select a valid date';
$app_strings['LBL_RESCHEDULE_ERROR2'] = 'Please select a reason';

$app_strings['LBL_RESCHEDULE_PANEL'] = 'Перепланувати';
$app_strings['LBL_RESCHEDULE_HISTORY'] = 'Call attempt history';
$app_strings['LBL_RESCHEDULE_COUNT'] = 'Call Attempts';

//SecurityGroups
$app_list_strings['moduleList']['SecurityGroups'] = 'Управління Групами Користувачів';
$app_strings['LBL_SECURITYGROUP'] = 'Управління Групами';

$app_list_strings['moduleList']['OutboundEmailAccounts'] = 'Outbound Email Accounts';

//social
$app_strings['FACEBOOK_USER_C'] = 'Facebook';
$app_strings['TWITTER_USER_C'] = 'Twitter';
$app_strings['LBL_PANEL_SOCIAL_FEED'] = 'Social Feed Details';

$app_strings['LBL_SUBPANEL_FILTER_LABEL'] = 'Фільтер';

$app_strings['LBL_QUICK_ACCOUNT'] = 'Створити контрагента';
$app_strings['LBL_QUICK_CONTACT'] = 'Новий контакт';
$app_strings['LBL_QUICK_OPPORTUNITY'] = 'Нова угода';
$app_strings['LBL_QUICK_LEAD'] = 'Новий попередній контакт';
$app_strings['LBL_QUICK_DOCUMENT'] = 'Створити документ';
$app_strings['LBL_QUICK_CALL'] = 'Новий дзвінок';
$app_strings['LBL_QUICK_TASK'] = 'Нове завдання';
$app_strings['LBL_COLLECTION_TYPE'] = 'Тип';

$app_strings['LBL_ADD_TAB'] = 'Add Tab';
$app_strings['LBL_EDIT_TAB'] = 'Правити вкладки';
$app_strings['LBL_SUITE_DASHBOARD'] = 'SuiteCRM Dashboard'; //Can be translated in all caps. This string will be used by SuiteP template menu actions
$app_strings['LBL_ENTER_DASHBOARD_NAME'] = 'Enter Dashboard Name:';
$app_strings['LBL_NUMBER_OF_COLUMNS'] = 'Number of Columns:';
$app_strings['LBL_DELETE_DASHBOARD1'] = 'Are you sure you want to delete the';
$app_strings['LBL_DELETE_DASHBOARD2'] = 'dashboard?';
$app_strings['LBL_ADD_DASHBOARD_PAGE'] = 'Add a Dashboard Page';
$app_strings['LBL_DELETE_DASHBOARD_PAGE'] = 'Remove Current Dashboard Page';
$app_strings['LBL_RENAME_DASHBOARD_PAGE'] = 'Rename Dashboard Page';
$app_strings['LBL_SUITE_DASHBOARD_ACTIONS'] = 'Дії'; //Can be translated in all caps. This string will be used by SuiteP template menu actions

$app_list_strings['collection_temp_list'] = array(
    'Tasks' => 'Завдання',
    'Meetings' => 'Зустрічі',
    'Calls' => 'Дзвінки',
    'Notes' => 'Нотатки',
    'Emails' => 'Електронні листи'
);

$app_list_strings['moduleList']['TemplateEditor'] = 'Template Part Editor';
$app_strings['LBL_CONFIRM_CANCEL_INLINE_EDITING'] = "You have clicked away from the field you were editing without saving it. Click ok if you're happy to lose your change, or cancel if you would like to continue editing";
$app_strings['LBL_LOADING_ERROR_INLINE_EDITING'] = "There was an error loading the field. Your session may have timed out. Please log in again to fix this";

//SuiteSpots
$app_list_strings['spots_areas'] = array(
    'getSalesSpotsData' => 'Продажі',
    'getAccountsSpotsData' => 'Облікові записи',
    'getLeadsSpotsData' => 'Попередні контакти',
    'getServiceSpotsData' => 'Service',
    'getMarketingSpotsData' => 'Маркетинг',
    'getMarketingActivitySpotsData' => 'Marketing Activity',
    'getActivitiesSpotsData' => 'Заходи',
    'getQuotesSpotsData' => 'Комерційну пропозицію'
);

$app_list_strings['moduleList']['Spots'] = 'Spots';

$app_list_strings['moduleList']['AOBH_BusinessHours'] = 'Business Hours';
$app_list_strings['business_hours_list']['0'] = '12am';
$app_list_strings['business_hours_list']['1'] = '1am';
$app_list_strings['business_hours_list']['2'] = '2am';
$app_list_strings['business_hours_list']['3'] = '3am';
$app_list_strings['business_hours_list']['4'] = '4am';
$app_list_strings['business_hours_list']['5'] = '5am';
$app_list_strings['business_hours_list']['6'] = '6am';
$app_list_strings['business_hours_list']['7'] = '7am';
$app_list_strings['business_hours_list']['8'] = '8am';
$app_list_strings['business_hours_list']['9'] = '9am';
$app_list_strings['business_hours_list']['10'] = '10am';
$app_list_strings['business_hours_list']['11'] = '11am';
$app_list_strings['business_hours_list']['12'] = '12pm';
$app_list_strings['business_hours_list']['13'] = '1pm';
$app_list_strings['business_hours_list']['14'] = '2pm';
$app_list_strings['business_hours_list']['15'] = '3pm';
$app_list_strings['business_hours_list']['16'] = '4pm';
$app_list_strings['business_hours_list']['17'] = '5pm';
$app_list_strings['business_hours_list']['18'] = '6pm';
$app_list_strings['business_hours_list']['19'] = '7pm';
$app_list_strings['business_hours_list']['20'] = '8pm';
$app_list_strings['business_hours_list']['21'] = '9pm';
$app_list_strings['business_hours_list']['22'] = '10pm';
$app_list_strings['business_hours_list']['23'] = '11pm';
$app_list_strings['day_list']['Monday'] = 'Понеділок';
$app_list_strings['day_list']['Tuesday'] = 'Вівторок';
$app_list_strings['day_list']['Wednesday'] = 'Середа';
$app_list_strings['day_list']['Thursday'] = 'Четвер';
$app_list_strings['day_list']['Friday'] = 'П&#039;ятниця';
$app_list_strings['day_list']['Saturday'] = 'Субота';
$app_list_strings['day_list']['Sunday'] = 'Неділя';
$app_list_strings['pdf_page_size_dom']['A4'] = 'A4';
$app_list_strings['pdf_page_size_dom']['Letter'] = 'Letter';
$app_list_strings['pdf_page_size_dom']['Legal'] = 'Legal';
$app_list_strings['pdf_orientation_dom']['Portrait'] = 'Книжкова';
$app_list_strings['pdf_orientation_dom']['Landscape'] = 'Альбомна';
$app_list_strings['LBL_REPORTS_RESTRICTED'] = 'A report you have selected is targeting a module you do not have access to. Please select a report with a target module you have access to.';


$app_list_strings['moduleList']['SurveyResponses'] = 'Survey Responses';
$app_list_strings['moduleList']['Surveys'] = 'Surveys';
$app_list_strings['moduleList']['SurveyQuestionResponses'] = 'Survey Question Responses';
$app_list_strings['moduleList']['SurveyQuestions'] = 'Survey Questions';
$app_list_strings['moduleList']['SurveyQuestionOptions'] = 'Survey Question Options';
$app_list_strings['survey_status_list']['Draft'] = 'Чернетка';
$app_list_strings['survey_status_list']['Public'] = 'Public';
$app_list_strings['survey_status_list']['Closed'] = 'Закрито';
$app_list_strings['surveys_question_type']['Text'] = 'Text';
$app_list_strings['surveys_question_type']['Textbox'] = 'Textbox';
$app_list_strings['surveys_question_type']['Checkbox'] = 'Прапорець';
$app_list_strings['surveys_question_type']['Radio'] = 'Радіо';
$app_list_strings['surveys_question_type']['Dropdown'] = 'Dropdown';
$app_list_strings['surveys_question_type']['Multiselect'] = 'Multiselect';
$app_list_strings['surveys_question_type']['Matrix'] = 'Matrix';
$app_list_strings['surveys_question_type']['DateTime'] = 'DateTime';
$app_list_strings['surveys_question_type']['Date'] = 'Дата';
$app_list_strings['surveys_question_type']['Scale'] = 'Scale';
$app_list_strings['surveys_question_type']['Rating'] = 'Рейтинг';
$app_list_strings['surveys_matrix_options'][0] = 'Satisfied';
$app_list_strings['surveys_matrix_options'][1] = 'Neither Satisfied nor Dissatisfied';
$app_list_strings['surveys_matrix_options'][2] = 'Dissatisfied';