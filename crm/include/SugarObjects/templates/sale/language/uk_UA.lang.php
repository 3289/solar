<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array(
    'LBL_MODULE_NAME' => 'Продаж',
    'LBL_MODULE_TITLE' => 'Угоди: Головна',
    'LBL_SEARCH_FORM_TITLE' => 'Пошук угод',
    'LBL_LIST_FORM_TITLE' => 'Список угод',
    'LBL_NAME' => 'Назва угоди',
    'LBL_LIST_SALE_NAME' => 'Назва',
    'LBL_LIST_ACCOUNT_NAME' => 'Контрагент',
    'LBL_LIST_AMOUNT' => 'Сума',
    'LBL_LIST_DATE_CLOSED' => 'Закрити',
    'LBL_LIST_SALE_STAGE' => 'Стадія продажу',
    'LBL_ACCOUNT_ID' => 'Контрагент',
    //DON'T CONVERT THESE THEY ARE MAPPINGS
    'db_name' => 'LBL_NAME',
    //END DON'T CONVERT
    'LBL_ACCOUNT_NAME' => 'Контрагент:',
    'LBL_AMOUNT' => 'Сума:',
    'LBL_AMOUNT_USDOLLAR' => 'Сумма у доларах США:',
    'LBL_CURRENCY' => 'Валюта:',
    'LBL_DATE_CLOSED' => 'Передбачувана дата закриття:',
    'LBL_TYPE' => 'Тип:',
    'LBL_CAMPAIGN' => 'Маркетингова кампанія:',
    'LBL_LEADS_SUBPANEL_TITLE' => 'Попередні контакти',
    'LBL_PROJECTS_SUBPANEL_TITLE' => 'Проекти',
    'LBL_NEXT_STEP' => 'Наступний крок:',
    'LBL_LEAD_SOURCE' => 'Джерело попереднього контакту:',
    'LBL_SALES_STAGE' => 'Стадія продажу:',
    'LBL_PROBABILITY' => 'Вірогідність (%) :',
    'LBL_DESCRIPTION' => 'Опис:',
    'LBL_DUPLICATE' => 'Можливе дублювання угод',
    'MSG_DUPLICATE' => 'Угода, яку ви маєте намір створити, може бути дублікатом угоди, яка вже існує. Угоди, які містять подібні назви, наведені нижче. <br> Натисніть кнопку Зберегти, щоб продовжити створення ціїє нової угоди, або натисніть кнопку Скасувати, щоб повернутися до модуля без створення нової угоди.',
    'LBL_NEW_FORM_TITLE' => 'Створення угоди',
    'ERR_DELETE_RECORD' => 'Номер запису має бути зазначений для видалення угоди.',
    'LBL_DEFAULT_SUBPANEL_TITLE' => 'Продаж',
    'LBL_ACTIVITIES_SUBPANEL_TITLE' => 'Заходи',
    'LBL_HISTORY_SUBPANEL_TITLE' => 'Історія',

    'LBL_CONTACTS_SUBPANEL_TITLE' => 'Контакти',
    'LBL_ASSIGNED_TO_NAME' => 'Користувач:',
    'LBL_LIST_ASSIGNED_TO_NAME' => 'Відповідальний (- а)',
    'LBL_ASSIGNED_TO_ID' => 'Призначений до ідентифікатора',
    'LBL_MODIFIED_NAME' => 'Змінено',
    'LBL_SALE_INFORMATION' => 'Інформація про угоду',
    'LBL_CURRENCY_NAME' => 'Валюта',
    'LBL_CURRENCY_SYMBOL' => 'Символ валюти',
    'LBL_EDIT_BUTTON' => 'Змінити',
    'LBL_REMOVE' => 'Видалити',

);
