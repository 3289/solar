<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2017 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$mod_strings = array(
    'LBL_SALUTATION' => 'Звернення',
    'LBL_NAME' => 'Назва',
    'LBL_FIRST_NAME' => 'Ім\'я',
    'LBL_LAST_NAME' => 'Прізвище',
    'LBL_TITLE' => 'Посада',
    'LBL_DEPARTMENT' => 'Відділ',
    'LBL_DO_NOT_CALL' => 'Не телефонувати',
    'LBL_HOME_PHONE' => 'Домашній тел.:',
    'LBL_MOBILE_PHONE' => 'Моб. тел.',
    'LBL_OFFICE_PHONE' => 'Робочий тел.',
    'LBL_OTHER_PHONE' => 'Інший тел.',
    'LBL_FAX_PHONE' => 'Факс',
    'LBL_EMAIL_ADDRESS' => 'Адрес(и) електронної пошти',
    'LBL_PRIMARY_ADDRESS' => 'Основна адреса',
    'LBL_PRIMARY_ADDRESS_STREET' => 'Основна адреса',
    'LBL_PRIMARY_ADDRESS_STREET_2' => 'Основна адреса - вулиця 2:',
    'LBL_PRIMARY_ADDRESS_STREET_3' => 'Основна адреса - вулиця 3:',
    'LBL_PRIMARY_ADDRESS_CITY' => 'Основне місто',
    'LBL_PRIMARY_ADDRESS_STATE' => 'Основний штат',
    'LBL_PRIMARY_ADDRESS_POSTALCODE' => 'Основний індекс',
    'LBL_PRIMARY_ADDRESS_COUNTRY' => 'Основна адреса - країна:',
    'LBL_ALT_ADDRESS' => 'Альтернативні адреси',
    'LBL_ALT_ADDRESS_STREET' => 'Альтернативні адреси',
    'LBL_ALT_ADDRESS_STREET_2' => 'Альтернативна адреса - вулиця 2:',
    'LBL_ALT_ADDRESS_STREET_3' => 'Альтернативна адреса - вулиця 3:',
    'LBL_ALT_ADDRESS_CITY' => 'Альтернативне місто',
    'LBL_ALT_ADDRESS_STATE' => 'Альтернативний штат',
    'LBL_ALT_ADDRESS_POSTALCODE' => 'Альтернативний індекс',
    'LBL_ALT_ADDRESS_COUNTRY' => 'Альтернативна країна',
    'LBL_PRIMARY_STREET' => 'Адреса',
    'LBL_ALT_STREET' => 'Інші адреси',
    'LBL_STREET' => 'Інші адреси',
    'LBL_CITY' => 'Місто',
    'LBL_STATE' => 'Область',
    'LBL_POSTAL_CODE' => 'Індекс',
    'LBL_COUNTRY' => 'Країна',
    'LBL_CONTACT_INFORMATION' => 'Контактна інформація',
    'LBL_ADDRESS_INFORMATION' => 'Адрес(и)',
    'LBL_ASSIGNED_TO_NAME' => 'Користувач',
    'LBL_OTHER_EMAIL_ADDRESS' => 'Додаткова електронна адреса:',
    'LBL_ASSISTANT' => 'Асистент',
    'LBL_ASSISTANT_PHONE' => 'Телефон асистента',
    'LBL_WORK_PHONE' => 'Робочий тел.',
    'LNK_IMPORT_VCARD' => 'Створити з vCard',
    'LBL_ANY_EMAIL' => 'E-mail',
    'LBL_EMAIL_NON_PRIMARY' => 'Не основна електронна пошта',
    'LBL_PHOTO' => 'Фотографія',
    'LBL_EDIT_BUTTON' => 'Змінити',
    'LBL_REMOVE' => 'Видалити',
);
