<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// include the helper file
require_once(dirname(__FILE__).DS.'helper.php');
 
ModCurrencyHelper::setCurrency();

require(JModuleHelper::getLayoutPath('mod_vscurrency'));

?>