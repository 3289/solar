<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
 
 
class ModCurrencyHelper
{

    public function setCurrency()
    {
       $localSet     = file_get_contents (JPATH_BASE.'/modules/mod_vscurrency/files/currency.json');
       $localSetJson = json_decode($localSet, TRUE);
       

	   if (time() - $localSetJson['timestamp'] > 86400){	  
			   
		   $xml = simplexml_load_file("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange");
		
		   foreach($xml->currency as $currency){
				if($currency->txt == 'Долар США'){
				    $localSetJson['usd'] = (float) $currency->rate + (float) $currency->rate * 0.035;
				}	
				if($currency->txt == 'Євро'){
				    $localSetJson['eur'] = (float) $currency->rate + (float) $currency->rate * 0.035;
				}	
			}
			
			$db = JFactory::getDbo();
            $query = $db->getQuery(true);
			
			$currency_exchange = 1 / $localSetJson['usd'];
			
			$fields = array(
				$db->quoteName('currency_exchange_rate') . ' = ' . $db->quote($currency_exchange),
			);     
			$conditions = array( 
				$db->quoteName('virtuemart_currency_id') . ' = 144'
			);
			
			$query->update($db->quoteName('#__virtuemart_currencies'))
				  ->set($fields)
				  ->where($conditions);
				  
			$db->setQuery($query);
			$result = $db->execute();	  


		    $localSetJson['timestamp'] = time();
		   
		   
		   $fh = fopen(JPATH_BASE.'/modules/mod_vscurrency/files/currency.json', 'w')
				  or die("Error opening output file");
		   fwrite($fh, json_encode($localSetJson));
		   fclose($fh);
       }
    } 
	
	public function setRateUah(){
		$db		= JFactory::getDbo();
	}
 
}

?>