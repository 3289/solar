<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
JText::script('MOD_SOLARCALCULATE_ERROR_FILL_EMAIL');
JText::script('MOD_SOLARCALCULATE_ERROR_FILL_CORRECT_EMAIL');
JText::script('MOD_SOLARCALCULATE_ERROR_MESSAGE_NOT_SENT');
JText::script('MOD_SOLARCALCULATE_ERROR_FILL_NAME');
JText::script('MOD_SOLARCALCULATE_MESSAGE_SENT');
?>
<div class="popup" id="calculator-popup">
    <div class="popup-inner">
         <a href="#" class="close-icon" title="Закрыть"></a>
         <div class="popup-header"><span><?php echo JText::_('MOD_SOLARCALCULATOR_POPUP_HEADER'); ?></span></div>
         <div class="calculate-top">
             
         </div>
         <div class="calculate-title">
            
         </div>
         <div class="calculate-result">
              
         </div>
         <div class="calculate-result-text">
             
         </div>
         <div class="calculate-popup-buttons">
             <a href="#" class="send-calculate"><i></i><?php echo JText::_('MOD_SOLARCALCULATOR_BUTTON_SEND_CALCULATE'); ?></a>
             <a href="#" class="print-calculate"><i></i><?php echo JText::_('MOD_SOLARCALCULATOR_BUTTON_PRINT_CALCULATE'); ?></a>
             <a href="#" class="order-calculate"><i></i><?php echo JText::_('MOD_SOLARCALCULATOR__BUTTON_ORDER_CALCULATE'); ?></a>
             <div class="clr"></div>
         </div>
    </div>
</div>

<div class="popup" id="calculate-send-info">
    <div class="popup-inner">
         <div class="cssend-loader"><div class="loader"></div></div>
         <a href="#" class="close-icon" title="Закрыть"></a>
         <div class="popup-header"><span><?php echo JText::_('MOD_SOLARCALCULATOR_SEND_CALCULATE_HEADER'); ?></span></div>
         <div class="popup-message">
         
         </div>
         <form action="#" id="send-form">
             <input type="email" name="email" placeholder="<?php echo JText::_('MOD_SOLARCALCULATOR_FORM_EMAIL'); ?>" />
         </form>
         <a href="#" class="send-calculate-link"><i></i><?php echo JText::_('MOD_SOLARCALCULATOR_LINK_SEND_CALCULATE'); ?></a>
    </div>
</div>

<div class="popup" id="calculate-order">
    <div class="popup-inner">
         <a href="#" class="close-icon" title="Закрыть"></a>
         <div class="popup-header"><span><?php echo JText::_('MOD_SOLARCALCULATOR_ORDER_CALCULATE_HEADER'); ?></span></div>
         <div class="popup-message">
         
         </div>
         <form action="#" id="calc-order-form">
             <input type="text" name="name" class="require" placeholder="<?php echo JText::_('MOD_SOLARCALCULATOR_FORM_NAME'); ?>" />
             <input type="email" name="email" class="require" placeholder="<?php echo JText::_('MOD_SOLARCALCULATOR_FORM_EMAIL'); ?>"  />
             <input type="text" name="phone" placeholder="<?php echo JText::_('MOD_SOLARCALCULATOR_FORM_PHONE'); ?>"  />
         </form>
         <a href="#" class="order-calculate-link"><i></i><?php echo JText::_('MOD_SOLARCALCULATOR_LINK_SEND_CALCULATE'); ?></a>
    </div>
</div>
    
<div class="calculator-header">
    <span><?php echo JText::_('MOD_SOLARCALCULATOR_HEADER'); ?></span>
</div>
<div class="calculator-tabs">
    <div class="calculator-loader">
        <div class="loader"></div>
    </div>
    <ul class="tabs-links">
        <li class="active" data-id="power-privat"><?php echo JText::_('MOD_SOLARCALCULATOR_PRIVAT'); ?><span></span></li>
        <li data-id="power-commerce"><?php echo JText::_('MOD_SOLARCALCULATOR_COMMERCIAL'); ?><span></span></li>
    </ul>
    <div class="clr"></div>
    <div class="tabs">
        <div class="tab" id="privat">
             <form class="jClever" id="calculator">
             <div class="calculator-left">
                   <div class="calculator-label"><?php echo JText::_('MOD_SOLARCALCULATOR_POWER_LABEL'); ?></div>
                   <div class="calculator-label"><?php echo JText::_('MOD_SOLARCALCULATOR_PRICE_LABEL'); ?></div>
                   <div class="calculator-label"><?php echo JText::_('MOD_SOLARCALCULATOR_REGION_LABEL'); ?></div>
                   <div class="calculator-label power-label"><?php echo JText::_('MOD_SOLARCALCULATOR_POWER_CONSUMPTION_LABEL'); ?></div>
             </div>
             <div class="calculator-right">
                 <div class="power-block">
                 
                 <div class="power-privat range">
				 <?php
                     $numPrivate = count($stantionsPrivate);
					 $itemWidth  = round(600 / $numPrivate);
				?>
                <div class="power-first">&nbsp;</div>	 
				<?php
				     $privateCounter = 1;	 
					 foreach($stantionsPrivate as $station){
				 ?>
                     <div class="power-item <?php if($privateCounter == 1) echo "active first";?>" style="width:<?php echo $itemWidth; ?>px;" data-id="<?php echo $station->id;?>" data-price="<?php echo number_format($station->station_price,0,'',' ') . ' ' . JText::_('MOD_SOLARCALCULATOR_STANTION_PRICE_UNIT_'.strtoupper($currency));?>">
                         <div>
                         <?php
                            echo $station->station_value;
							echo ' '.JText::_('MOD_SOLARCALCULATOR_STANTION_UNIT_'.$station->station_value_units);
						 ?>
                         <span></span>
                         </div>
                     </div>
                     
                 <?php 
				      if($privateCounter == 1){
						  $price =  number_format($station->station_price,0,'',' '). ' ' . JText::_('MOD_SOLARCALCULATOR_STANTION_PRICE_UNIT_'.strtoupper($currency));
						  $stationId = 	$station->id;
					  }
				      $privateCounter++; }?>
                    
                 <div class="clr"></div>
                 </div>
                 
                 <div class="power-commerce range">
				 <?php
                     $numCommerce = count($stantionsCommerce);
					 $itemWidth  = round(490 / $numCommerce);
				?>
                <div class="power-first">&nbsp;</div>	 
				<?php
				     $commerceCounter = 1;	 
					 foreach($stantionsCommerce as $station){
				 ?>
                     <div class="power-item <?php if($commerceCounter == 1) echo "active first";?>" style="width:<?php echo $itemWidth; ?>px;" data-id="<?php echo $station->id;?>"  data-price="<?php echo number_format($station->station_price,0,'',' ') . ' ' . JText::_('MOD_SOLARCALCULATOR_STANTION_PRICE_UNIT_'.strtoupper($currency));?>">
                         <div>
                         <?php
                            echo $station->station_value;
							echo ' '.JText::_('MOD_SOLARCALCULATOR_STANTION_UNIT_'.$station->station_value_units);
						 ?>
                         <span></span>
                         </div>
                     </div>
                     
                 <?php 
				     
				      $commerceCounter++; }?>
                    
                    <div class="power-value">
                        <div class="power-text"><strong><?php echo ' '.JText::_('MOD_SOLARCALCULATOR_STANTION_OTHER_POWER');?></strong></div>
                        <div class="power-field">
                            <input type="text" name="custom_value" value="6" />
                            <input type="hidden" value="" name="user_value" />
                            <span><?php echo ' '.JText::_('MOD_SOLARCALCULATOR_STANTION_UNIT_MWT');?></span>
                            <div class="clr"></div>
                        </div>
                    </div>
                 <div class="clr"></div>
                 </div>
                 
                 
                 <div class="power-line"></div>
                 </div>
                 
                 <div class="station-price">
                     <?php 
					    echo $price;
					 ?>
                 </div>
                 <input type="hidden" name="station_id" value="<?php echo $stationId;?>" />
                 <div class="clr"></div>
                 <div class="power-regions p-field">
                     <select name="region">
                     <option value="0" selected><?php echo JText::_('MOD_SOLARCALCULATOR_POWER_REGION_LABEL'); ?></option>
                     <?php 
					    foreach ($regions as $region){
					 ?>
                     <option value="<?php echo $region->id; ?>"><?php echo $region->region_name?></option>
                     <?php }?>
                     </select>
                     <div class="calc-message msg1">
                         <div class="calc-msg-inner">
                             <i></i>
                             <div class="calc-msg-text"><?php echo JText::_('MOD_SOLARCALCULATOR_ERROR_FILL_REGION'); ?></div>
                         </div>
                     </div>
                 </div>
                 <div class="power-num p-field">
                      <input type="text" name="power_num" value="" /><label><?php echo JText::_('MOD_SOLARCALCULATOR_STANTION_UNIT_KWT')?></label>
                       <div class="calc-message msg2">
                         <div class="calc-msg-inner">
                             <i></i>
                             <div class="calc-msg-text"><?php echo JText::_('MOD_SOLARCALCULATOR_ERROR_FILL_POWER_NUM'); ?></div>
                         </div>
                     </div>
                      <div class="calc-message msg3">
                         <div class="calc-msg-inner">
                             <i></i>
                             <div class="calc-msg-text"><?php echo JText::_('MOD_SOLARCALCULATOR_ERROR_FILL_POWER_NUM_INTEGER'); ?></div>
                         </div>
                     </div>
                      <div class="clr"></div>
                 </div>
                 
             </div>
             <div class="clr"></div>
             
             <a href="#" class="calculate-link"><i></i><span><?php echo JText::_('MOD_SOLARCALCULATOR_STANTION_CALCULATE_BUTTON')?></span></a>
             
             </form>
        </div>
        
    </div>
</div>