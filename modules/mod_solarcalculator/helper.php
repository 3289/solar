<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
 
class ModSolarcalculatorHelper
{
	static $powerPrivate;
	static $powerCommerce;
	static $currencyUnit;
    
	function getStantions(){
		
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true)
			->select('a.*')
			->from('#__solarcalculator_stations AS a')
			->where('a.published=1')
			->order('a.id ASC');
		
		$db->setQuery($query);
		$stations = $db->loadObjectList();
		$currencyUnit = self::getCurrencyUnit();
		
		try
		{
			foreach ($stations as $power){
				if($currencyUnit != $power->station_price_unit){
				    $power->station_price = self::priceConvert($currencyUnit,$power);					
				}
				
				if($power->station_type == 1){
				   self::$powerPrivate[] = $power;
				}
				if($power->station_type == 2){
				   self::$powerCommerce[] = $power;
				}
			}
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->enqueueMessage(JText::_('JERROR_AN_ERROR_HAS_OCCURRED'), 'error');

			return array();
		}
		
	}
	
	public function getPrivate(){
		return self::$powerPrivate;
	}
	
	public function getCommerce(){
		return self::$powerCommerce;
	}
	
	public function getRegions(){
		
		$lang = JFactory::getLanguage();
		
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true)
			->select('a.*')
			->from('#__solarcalculator_regions AS a')
			->where('a.published=1')
			->where('a.lang="'.$lang->getTag().'"')
			->order('a.id ASC');
		
		$db->setQuery($query);
		try
		{
			return $db->loadObjectList();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->enqueueMessage(JText::_('JERROR_AN_ERROR_HAS_OCCURRED'), 'error');

			return array();
		}
		
	}
	
	public function getCurrency(){
	    $localSet     = file_get_contents (JPATH_BASE.'/modules/mod_vscurrency/files/currency.json');
        $currency = json_decode($localSet, TRUE);	
		return $currency;
		
	}
	
	public function getCurrencyUnit(){
       $db = JFactory::getDbo(); 
	   $query = $db->getQuery(true);
	   
	   $query->select('settings');
	   $query->from($db->quoteName('#__solarcalculator_settings'));
	   $query->where($db->quoteName('type') . ' LIKE '. $db->quote('settings'));
	   $db->setQuery($query);
	   $result = $db->loadObjectList();
	   $settings = json_decode($result[0]->settings);
	   $currency = $settings->setting_price_units;
	   
	   self::$currencyUnit = $currency;
	   
	   return $currency;
	   
	}
	
	public function priceConvert($unit,$power){
		
		$currency = self::getCurrency();
		
		if($unit == 'uah'){
			if($power->station_price_unit == 'usd'){
			   $price = 	round($power->station_price * $currency['usd']);
			}
			
			if($power->station_price_unit == 'eur'){
			   $price = 	round($power->station_price * $currency['eur']);
			}
		}
		if($unit == 'usd'){
			if($power->station_price_unit == 'uah'){
			   $price = 	round($power->station_price / $currency['usd']);
			}
			
			if($power->station_price_unit == 'eur'){
			   $price = 	round($power->station_price * ($currency['eur'] / $currency['usd']));
			}						
		}	
		if($unit == 'eur'){
			
			if($power->station_price_unit == 'uah'){
			   $price = 	round($power->station_price / $currency['eur']);
			}
			
			if($power->station_price_unit == 'usd'){
			   $price = 	round($power->station_price * ($currency['usd'] / $currency['eur']));
			}						
			
		}
       
	   return $price;
	   
	}
	
	
 
} 

?>