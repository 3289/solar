<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// include the helper file
require_once(dirname(__FILE__).'/helper.php');
 
ModSolarcalculatorHelper::getStantions();
$stantionsPrivate  = ModSolarcalculatorHelper::getPrivate();
$stantionsCommerce = ModSolarcalculatorHelper::getCommerce();
$regions = ModSolarcalculatorHelper::getRegions();
$currency = ModSolarcalculatorHelper::$currencyUnit;


require(JModuleHelper::getLayoutPath('mod_solarcalculator'));


?>