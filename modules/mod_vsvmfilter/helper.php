<?php


// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
/**
 * Example Module Helper
 *
 * @package		  Joomla!
 * @subpackage	vsregiment
 * @since 		  1.0.0
 * @class       ModVsregimentHelper
 */ 
 
class ModVsvmfilterHelper
{
    

	static public $params;
	
    
	static public function getPrices(){
		
		$categoryId = vRequest::getInt('virtuemart_category_id',0);
		
		
		$db		= JFactory::getDbo();
		
	    $query	= $db->getQuery(true)
			->select('MIN(a.product_price) as min, MAX(a.product_price) as max')
			->from('#__virtuemart_product_prices AS a')
			->join('LEFT', $db->quoteName('#__virtuemart_product_categories', 'b') . ' ON (' . $db->quoteName('a.virtuemart_product_id') . ' = ' . $db->quoteName('b.virtuemart_product_id') . ')')
			->join('LEFT', $db->quoteName('#__virtuemart_products', 'c') . ' ON (' . $db->quoteName('b.virtuemart_product_id') . ' = ' . $db->quoteName('c.virtuemart_product_id') . ')')
			->where('a.virtuemart_shoppergroup_id=0')
			->where('c.published=1');
        
		if($categoryId){
			$query->where('b.virtuemart_category_id='.$categoryId);
		} 
		
		$db->setQuery($query);
		$prices = $db->loadObjectList();
		
		return $prices; 

	} 
	
	static public function getCustomFields(){
		
		$categoryId = vRequest::getInt('virtuemart_category_id',0);
		
		$customRequest = vRequest::get('customfields');

		$db		= JFactory::getDbo();
		
		
		$q1= 'SELECT c.* FROM #__virtuemart_customs  as c ';
		if(!empty($categoryId)){
			$q1 .= 'INNER JOIN #__virtuemart_product_customfields as pc on (c.virtuemart_custom_id=pc.virtuemart_custom_id)
INNER JOIN #__virtuemart_product_categories as cat ON (pc.virtuemart_product_id=cat.virtuemart_product_id)';
		}
		$q1 .= ' WHERE';
		if(!empty($categoryId)){
			$q1 .= ' virtuemart_category_id="'.$categoryId.'" and';
		}
		$q1 .= ' searchable="1" and (field_type="S" or field_type="P") GROUP BY c.virtuemart_custom_id';

		$db->setQuery($q1);
		$selectedVal = $db->loadObjectList();
		//vmdebug('getSearchCustom '.str_replace('#__',$db->getPrefix(),$db->getQuery()),$this->selected);//,$this->categoryId,$this->selected);
		
		if($selectedVal){
			$app = JFactory::getApplication();
			foreach ($selectedVal as $selected) {
				$valueOptions = array();
				$q2= 'SELECT pc.* FROM #__virtuemart_product_customfields  as pc ';
						$q2 .= 'INNER JOIN #__virtuemart_products as p on (pc.virtuemart_product_id=p.virtuemart_product_id)';
						if(!empty($categoryId)){
							$q2 .= 'INNER JOIN #__virtuemart_product_categories as cat on (pc.virtuemart_product_id=cat.virtuemart_product_id)';
						}
						$q2 .= ' WHERE virtuemart_custom_id="'.$selected->virtuemart_custom_id.'" and p.published="1" ';
						if(!empty($categoryId)){
							$q2 .= ' and virtuemart_category_id="'.$categoryId.'" ';
						}
						$q2 .= ' GROUP BY `customfield_value`';

						$db->setQuery( $q2 );
						$Opts = $db->loadObjectList();
						//vmdebug('getSearchCustom my  q2 '.str_replace('#__',$db->getPrefix(),$db->getQuery()) );
						if($Opts){

							 $options .= '<div class="option-block">'.
									                '<div class="filter-label">'.vmText::_( $selected->custom_title ).'</div>';
							foreach( $Opts as $k => $v ) {
							  
								if(!isset($valueOptions[$v->customfield_value])) {
									    $checked = "";
									    if($customRequest[$v->virtuemart_custom_id][$k]){
											$checked = 'checked="checked"';
										}
										$options .= '<input type="checkbox" name="customfields['.$v->virtuemart_custom_id.']['.$k.']" '.$checked.' value="'.$v->customfield_value.'" />'.
											    '<label>'. $v->customfield_value .'</label><div class="clr"></div>';
								}
								
							}
							$options .= '</div>';
							
						}
			}
		}
		
		
		return $options; 

	} 
	
	
	static public function getModuleParams($id){
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->quoteName(array('id','params')));
		$query->from($db->quoteName('#__modules'));
		$query->where($db->quoteName('id') . '='. $db->quote($id));
		$db->setQuery($query);
		$results = $db->loadObjectList();
		
		self::$params = json_decode($results[0]->params,true);
		return self::$params;
		
	}
	
	
	
 
} // END ModVsregimentHelper

?>