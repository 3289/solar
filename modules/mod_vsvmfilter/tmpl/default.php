<?php

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

?>
<script type="text/javascript">
jQuery(function(){
    jQuery( ".price-selector" ).slider( {
	    range: true,
        min: <?php echo $prices[0]->min; ?>,
        max: <?php echo $prices[0]->max; ?>,
		values:[<?php echo $prices[0]->min; ?>,<?php echo $prices[0]->max; ?>],
		create: function( event, ui ) {
		   jQuery( ".price-min" ).val( <?php echo $prices[0]->min; ?>);
		   jQuery( ".price-max" ).val( <?php echo $prices[0]->max; ?>);
		},
        slide: function( event, ui ) {
           jQuery( ".price-min" ).val( ui.values[ 0 ] );
		   jQuery( ".price-max" ).val( ui.values[ 1 ] );
        },
		stop: function( event, ui ) {
			jQuery('.changeSendForm').submit();
		}
	});
	
	jQuery(".price-inputsr input").focusout(function() {
		jQuery(".filter-price").slider("values", 0, jQuery( ".place-min" ).val());
		jQuery(".filter-price").slider('values',1, jQuery( ".price-max" ).val()); 
	});
	
	jQuery('.filter-reset a').on('click',function(){
		jQuery('.changeSendForm').reset().submit();
	});
	
});	
</script>
<div class="filter-wrapper">
    <form action="#" class="jClever changeSendForm">
    <div class="filter-header"><i></i><?php echo JText::_('MOD_VSVMFILTER_HEADER'); ?></div>
    <div class="product-filter">
        <div class="filter-price">
            <div class="filter-label"><?php echo JText::_('MOD_VSVMFILTER_LABEL_PRICE_GNR'); ?></div>
            <div class="price-inputs">
                <label><?php echo JText::_('MOD_VSVMFILTER_LABEL_FROM'); ?></label>
                <input type="text" name="price-min" class="price-min" />
                <label><?php echo JText::_('MOD_VSVMFILTER_LABEL_TO'); ?></label>
                <input type="text" name="price-max" class="price-max" />
                <div class="clr"></div>
            </div>
            <div class="price-selector"></div>
        </div> 
        <?php echo $fileds; ?>
        <div class="filter-reset">
             <a href="#"><?php echo JText::_('MOD_VSVMFILTER_RESET_FILTER'); ?></a>
        </div>
    </div>
    </form>
</div>
<?php 

?>