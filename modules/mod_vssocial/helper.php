<?php
/**
 *
 * $Id: helper.php 1.0.0 2012-05-12 15:34:22 Andrey Tkachenko $
 * @package	    Joomla! 
 * @subpackage	vssocial
 * @version     1.0.0
 * @description 
 * @copyright	  Copyright © 2012 - All rights reserved.
 * @license		  GNU General Public License v2.0
 * @author		  Andrey Tkachenko
 * @author mail	office@virtuas.net
 * @website		  virtuas.net
 *
 *
 *
 * The module methods
 * -------------------------------
 * getItems()
 *
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
/**
 * Example Module Helper
 *
 * @package		  Joomla!
 * @subpackage	vssocial
 * @since 		  1.0.0
 * @class       ModVssocialHelper
 */ 
 
class ModVssocialHelper
{
	/**
	 * Do something getItems method
	 *
	 * @param 	
	 * @return
	 */
    public function getItems($params)
    {
      // Your custom code here
    } //end getItems
 
} // END ModVssocialHelper

?>