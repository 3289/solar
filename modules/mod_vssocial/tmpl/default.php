<?php
/**
 *
 * $Id: default.php 1.0.0 2012-05-12 15:34:22 Andrey Tkachenko $
 * @package	    Joomla! 
 * @subpackage	vssocial
 * @version     1.0.0
 * @description 
 * @copyright	  Copyright © 2012 - All rights reserved.
 * @license		  GNU General Public License v2.0
 * @author		  Andrey Tkachenko
 * @author mail	office@virtuas.net
 * @website		  virtuas.net
 *
 *
 *
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );


?>
    <div class="social-wrapper">
     <?php if($vk):?>
		     <a href="<?php echo $vk?>" class="vk-social social" target="_blank"></a>
	 <?php endif;?>
	 <?php if($facebook):?>
		     <a href="<?php echo $facebook?>" class="facebook-social social" target="_blank"></a>
	 <?php endif;?>
	 <?php if($twitter):?>
		     <a href="<?php echo $twitter?>" class="twitter-social social" target="_blank"></a>
	 <?php endif;?>
	 <?php if($google):?>
		     <a href="<?php echo $google?>" class="google-social social" target="_blank"></a>
	 <?php endif;?>
	 <?php if($livejournal):?>
		     <a href="<?php echo $livejournal?>" class="lj-social social" target="_blank"></a>
	 <?php endif;?>
      <?php if($ok):?>
		     <a href="<?php echo $ok?>" class="ok-social social" target="_blank"></a>
	 <?php endif;?>
	 <?php if($mailru):?>
		     <a href="<?php echo $mailru?>" class="mailru-social social" target="_blank"></a>
	 <?php endif;?>
	 <?php if($youtube ):?>
		     <a href="<?php echo $youtube ?>" class="youtube-social social" target="_blank"></a>
	 <?php endif;?>
	 <?php if($myspace):?>
		     <a href="<?php echo $myspace?>" class="myspace-social social" target="_blank"></a>
	 <?php endif;?>
	 <?php if($linkedin):?>
		     <a href="<?php echo $linkedin?>" class="linkedin-social social" target="_blank"></a>
	 <?php endif;?>
	 <?php if($yahoo):?>
		     <a href="<?php echo $yahoo?>" class="yahoo-social social" target="_blank"></a>
	 <?php endif;?>
     <div class="clr"></div>
	 </div>
<!-- END vssocial MODULE -->