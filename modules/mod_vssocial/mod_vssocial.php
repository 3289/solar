<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// include the helper file
require_once __DIR__ . '/helper.php';
 

$vk = $params->get('vkontakte');
$facebook = $params->get('facebook');
$twitter = $params->get('twitter');
$google = $params->get('google');
$livejournal = $params->get('livejournal');
$mailru = $params->get('mailru');
$youtube = $params->get('youtube');
$myspace = $params->get('myspace');
$linkedin = $params->get('linkedin');
$yahoo = $params->get('yahoo');
$ok = $params->get('ok');
 


// include the template for display
require(JModuleHelper::getLayoutPath('mod_vssocial'));

?>