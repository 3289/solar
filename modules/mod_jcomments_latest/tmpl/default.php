<?php
// no direct access
defined('_JEXEC') or die;
?>
<?php if (!empty($list)) :?>
<div id="video-popup" class="popup">
    <div class="popup-inner">
        <div class="popup-header"></div>
        <a href="#" class="close-icon"></a>
        <div class="video-content">
        cvsafsdfd
        </div>
    </div>
</div>
<div id="comment<?php echo $module->id;?>" class="comment-mod">
    <?php 
	    foreach($list as $item){
	?>
    <div class="comment-item">
        <div class="comment-image <?php if(!$item->userphoto) echo 'no-image'?>">
            <?php if($item->userphoto){?>
                <img src="<?php echo $item->userphoto; ?>" alt="<?php echo $item->username; ?>" />
            <?php }else{?>
                <i></i>
            <?php }?>
        </div>
        <div class="comment-info">
            <div class="comment-line">
				<?php echo $item->username; ?>
                <?php 
                   if($item->position){
                       echo '- '.$item->position;
                   }
                ?>
            </div>
            <div class="comment-text">
                <i></i>
                <?php echo $item->comment; ?>
            </div>
            <?php if($item->video || $item->youtube){
				if($item->video){
					$videoType = 'video';
					$videoSrc  = $item->video;
				}else{
					$videoType = 'youtube';
					preg_match('/\?v=(.*)$/', $item->youtube, $code);
					$videoSrc = 'https://www.youtube.com/embed/'.$code[1];
				}
				?>
                <a href="#" class="comment-video" data-videotype="<?php echo $videoType;?>" data-src="<?php echo $videoSrc;?>"><i></i><?php echo JText::_('MOD_JCOMMENTS_LATEST_VIDEO'); ?></a>
            <?php }?>
        </div>
    </div>
    <?php }?>
</div>
<script type="text/javascript">
jQuery(function() {
    jQuery('#comment<?php echo $module->id;?>').slick({
	  infinite: true,
	  slidesToShow: 1,
	  slidesToScroll: 1
	});
	
	jQuery('.comment-video').on('click',function(){
		jQuery('.bgblack').fadeIn();
		var html = '';
		videoType = jQuery(this).data('videotype');
		videoSrc  = jQuery(this).data('src');
		if (videoType == 'youtube'){
		    jQuery('.video-content').html('<iframe id="youtubevideo" width="580" height="320" src="'+videoSrc+'" frameborder="0" allowfullscreen></iframe>');
		}
		if (videoType == 'video'){
			html = '<div class="player" id="videoplayer"></div>';
			html += '<script type="text/javascript">';
			html += 'this.player = new Uppod({m:"video",uid:"videoplayer",file:"<?php echo JUri::base()?>media/com_jcomments/video/'+videoSrc+'"});'
			html += '<';
			html += '/script>';
			jQuery('.video-content').html(html);
		}
		
		jQuery('#video-popup').show();
		return false;
	});
	
});
</script>

<?php endif; ?>
