<?php
/**
 * JComments Latest - Shows latest comments
 *
 * @version 3.0
 * @package JComments
 * @author smart (smart@joomlatune.ru)
 * @copyright (C) 2006-2013 by smart (http://www.joomlatune.ru)
 * @license GNU General Public License version 2 or later; see license.txt
 *
 **/
 
// no direct access
defined('_JEXEC') or die;

$comments = JPATH_SITE . '/components/com_jcomments/jcomments.php';
if (file_exists($comments)) {
	require_once ($comments);
} else {
	return;
}

require_once (dirname(__FILE__).'/helper.php');
$document = JFactory::getDocument();
if ($params->get('useCSS') && !defined ('_JCOMMENTS_LATEST_CSS')) {
	define ('_JCOMMENTS_LATEST_CSS', 1);

	$app = JFactory::getApplication('site');
	$style = JFactory::getLanguage()->isRTL() ? 'style_rtl.css' : 'style.css';

}



$list = modJCommentsLatestHelper::getList($params);

if (!empty($list)) {
	$grouped = false;
	
	foreach($list as $item){
		if($item->video){
	        $document->addScript('media/' . $module->module . '/player/uppod.js');
			break;
		}
	}

	$comments_grouping = $params->get('comments_grouping', 'none');
	$item_heading = $params->get('item_heading', 4);

	if ($comments_grouping !== 'none') {
		$grouped = true;
		$list = modJCommentsLatestHelper::groupBy($list, $comments_grouping);
	}

	require (JModuleHelper::getLayoutPath('mod_jcomments_latest', $params->get('layout', 'default')));
}
