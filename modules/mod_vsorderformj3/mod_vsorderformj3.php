<?php
/**
 *
 * $Id: mod_vsregiment.php 1.0.0 2012-07-17 20:52:33 Andrey Tkachenko $
 * @package	    Joomla! 
 * @subpackage	vsregiment
 * @version     1.0.0
 * @description 
 * @copyright	  Copyright © 2012 - All rights reserved.
 * @license		  GNU General Public License v2.0
 * @author		  Andrey Tkachenko
 * @author mail	office@virtuas.net
 * @website		  virtuas.net
 *
 * CODE GENERATED BY: ALEXEY GORDEYEV IK CODE GENERATOR
 * HTTP://WWW.AGJOOMLA.COM/
 *
 *
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// include the helper file
require_once(dirname(__FILE__).'/helper.php');
 
/**
 * Example to get the items to display from the helper
 * $items = ModVsregimentHelper::getItems($params);
 */
 

$template = $params->get('layout','default');
$template = str_replace('_:','',$template);

require(JModuleHelper::getLayoutPath('mod_vsorderformj3', $template));


?>