jQuery(document).ready(function() {
	
	function showMessage(message){
		jQuery('.message-text').html('<div class="text-inner">'+message+'</div>');
	    jQuery('#message-popup').show();
		jQuery('.bgblack').fadeIn();	
	}
	
	function popupHide(){
		jQuery('#message-popup').fadeOut();
		jQuery('.bgblack').fadeOut();
	}
	
	jQuery('.vssend-button').click(function() {
		formid = jQuery(this).attr('id');
		var valid = true;
		var message = '';
		var require = 0;
		var requireText = '';
		pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		
		jQuery('.'+formid+' .require').each(function(){
			if (!jQuery(this).val()){
				require = require + 1;
				requireText  += jQuery(this).attr('data-title')+',';
			}
		});
		
		if(require){
			message = 'Пожалуйста, заполните обязательные поля: '+ '<span>' + requireText.substring(0, requireText.length - 1) + '</span>';
			valid = false;	
		}
		
        if(valid){
			if(jQuery('.'+formid+' input[type=email]').length && !pattern.test(jQuery('.'+formid+' input[type=email]').val())){
				message = 'Пожалуйста, введите корректный e-mail';
				valid = false;
			}
		}
		
		
		if (valid){
			request = {
				      'option' : 'com_ajax',
					  'module' : 'vsorderformj3',
					  'format' : 'raw'
				};
			var formData = jQuery('.'+formid).serializeArray();
			formData.push({name:'option',value:'com_ajax'},{name:'module',value:'vsorderformj3'},{name:'format',value:'raw'});
			jQuery('.popup-loader').show();
			jQuery.ajax({
			    type:'POST',
				data   : formData,
				success: function (response) {
					jQuery('.popup-loader').hide();
					showMessage(response);	
					setTimeout(popupHide, 5000);
				}	
			});
			
		}else{
			showMessage(message);	
		}
		return false;
	});
});