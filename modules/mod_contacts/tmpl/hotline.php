<?php
defined( '_JEXEC' ) or die( 'Restricted access' );


?>     
         <?php if($address):?>
		     <div class="contact-address"><i></i><?php echo $address?></div>
		 <?php endif;?>	 
         <div class="hotline-label"><?php echo JText::_('MOD_CONTACTS_HOTLINE'); ?>:</div>
	     <?php if($tel1):?>
		     <div class="contact-tel1 tel"><i></i><?php echo $tel1?></div>
		 <?php endif;?>	 
		 <?php if($tel2):?>
		     <div class="contact-tel2 tel"><i></i><?php echo $tel2?></div>
		 <?php endif;?>	 
		 <?php if($tel3):?>
		     <div class="contact-tel3 tel"><i></i><?php echo $tel3?></div>
		 <?php endif;?>	 
		 <?php if($email):?>
		     <div class="contact-email"><i></i><a href="mailto:<?php echo $email?>"><?php echo $email?></a></div>
		 <?php endif;?>	 

      
		 <div class="clr"></div>

