<?php
defined( '_JEXEC' ) or die( 'Restricted access' );


?>       <div class="contacts-left">
         <i></i>
	     <?php if($tel1):?>
		     <div class="contact-tel1"><?php echo $tel1?></div>
		 <?php endif;?>	 
		 <?php if($tel2):?>
		     <div class="contact-tel2"><?php echo $tel2?></div>
		 <?php endif;?>	 
		 <?php if($tel3):?>
		     <div class="contact-tel2"><?php echo $tel3?></div>
		 <?php endif;?>	 
		 <?php if($email):?>
		     <div class="contact-email"><a href="mailto:<?php echo $email?>"><?php echo $email?></a></div>
		 <?php endif;?>	 
         </div>
         <div class="contacts-right">
         <i></i>
         <?php if($address):?>
		     <div class="contact-address"><?php echo $address?></div>
		 <?php endif;?>	 
		 <?php if($icq):?>
		     <div class="contact-icq"><?php echo $icq?></div>
		 <?php endif;?>	 
		 <?php if($skype):?>
		     <div class="contact-skype"><a href="skype:<?php echo $skype?>"><?php echo $skype?></a></div>
		 <?php endif;?>	 
         </div>
		 <div class="clr"></div>

