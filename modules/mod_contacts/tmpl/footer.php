<?php
defined( '_JEXEC' ) or die( 'Restricted access' );


?>       <div class="contacts-left">
         <?php if($address):?>
		     <div class="contact-address"><i></i><?php echo $address?></div>
		 <?php endif;?>	 
	     <?php if($tel1):?>
		     <div class="contact-tel1"><i></i><?php echo $tel1?></div>
		 <?php endif;?>	 
		 <?php if($tel2):?>
		     <div class="contact-tel2"><i></i><?php echo $tel2?></div>
		 <?php endif;?>	 
		 <?php if($tel3):?>
		     <div class="contact-tel3"><i></i><?php echo $tel3?></div>
		 <?php endif;?>	 
		 <?php if($email):?>
		     <div class="contact-email"><i></i><a href="mailto:<?php echo $email?>"><?php echo $email?></a></div>
		 <?php endif;?>	 
         </div>
         <div class="contacts-right">
         
		 <?php if($icq):?>
		     <div class="contact-icq"><?php echo $icq?></div>
		 <?php endif;?>	 
		 <?php if($skype):?>
		     <div class="contact-skype"><a href="skype:<?php echo $skype?>"><?php echo $skype?></a></div>
		 <?php endif;?>	 
         </div>
		 <div class="clr"></div>

