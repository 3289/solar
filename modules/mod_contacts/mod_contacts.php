<?php
/**
 *
 * $Id: mod_contacts.php 1.0.0 2012-04-29 20:52:13 Andrey Tkachenko $
 * @package	    Joomla! 
 * @subpackage	contacts
 * @version     1.0.0
 * @description 
 * @copyright	  Copyright © 2012 - All rights reserved.
 * @license		  GNU General Public License v2.0
 * @author		  Andrey Tkachenko
 * @author mail	office@virtuas.net
 * @website		  virtuas.net
 *
 *
 *
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
 
// include the helper file
require_once(dirname(__FILE__).DS.'helper.php');
 


$tel1 = $params->get('telephone1');
$tel2 = $params->get('telephone2');
$tel3 = $params->get('telephone3');
$email = $params->get('email');
$skype = $params->get('skype');
$icq = $params->get('icq');
$address = $params->get('address');
$template = $params->get('template','default'); 

$template = str_replace('_:','',$template);


 
// include the template for display
require(JModuleHelper::getLayoutPath('mod_contacts',$template));

?>