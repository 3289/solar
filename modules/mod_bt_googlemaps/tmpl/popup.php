<?php
/**
 * @package 	mod_bt_googlemaps - BT Google Maps
 * @version		2.0.5
 * @created		Jun 2012

 * @author		BowThemes
 * @email		support@bowthems.com
 * @website		http://bowthemes.com
 * @support		Forum - http://bowthemes.com/forum/
 * @copyright	Copyright (C) 2012 Bowthemes. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div id="mappopup" class="popup">
<div class="popup-inner">
<div class="map-header"><span><?php echo $params->get('mapinfo');?></span></div>
<a href="#" class="close-icon"></a>
<div class="map-wrapper">
<div id="cavas_id<?php echo $module->id; ?>" class="bt-googlemaps"></div>
</div>
<?php 
   $coordinate = explode(',',$params->get('mapCenterCoordinate'));
?>
<script type="text/javascript">

function initMap() {
  
  var map = new google.maps.Map(document.getElementById('cavas_id<?php echo $module->id; ?>'),{
	   zoom: <?php echo $params->get('zoom') ?>,
       center: {lat: <?php echo $coordinate[0]?>, lng: <?php echo $coordinate[1]?>}
  });

  var marker = new google.maps.Marker({
    position:  {lat: <?php echo $coordinate[0]?>, lng: <?php echo $coordinate[1]?>},
    map: map
  });

}
jQuery(function(){
	 jQuery('.moduletable_topcontacts .contacts-right').on('click',function(){
		jQuery('#mappopup').css('top','-550px');
		jQuery('.bgblack').fadeIn();
		jQuery('#mappopup').show();
		jQuery('#mappopup').animate({'top':'100px'},1000);
		initMap();
	});
});

</script>
</div>
</div>