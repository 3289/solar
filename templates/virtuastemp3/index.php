<?php
defined('_JEXEC') or die;
$app = JFactory::getApplication();
$document = JFactory::getDocument();
$document->setGenerator('');
$menu = $app->getMenu();
$home = '';
if ($menu->getActive()->home) {
    $home = 'home';
}
$input = $app->input;
$view = $input->get('view');
?>
<!doctype html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
    <meta name="viewport" content="width=450">
    <jdoc:include type="head"/>
    <?php
    if ($menu->getActive()->home) {
        ?>
        <meta property="og:title" content="<?php echo $menu->getActive()->params->get('page_title'); ?>"/>
        <meta property="og:description"
              content="<?php echo $menu->getActive()->params->get('menu-meta_description'); ?>"/>
        <meta property="og:url" content="<?php echo JUri::base(); ?>"/>
        <meta property="og:image" content="<?php echo JUri::base() . 'images/facebook.jpg'; ?>"/>
    <?php } ?>
    <style>
        .img-menu {
            display: none;
        }

        .desk {
            display: block;
        }

        .mob {
            display: none;
        }

        .ws_cover a {
            display: none;
        }

        @media (max-width: 1200px) and (min-width: 730px) {
            .slider-wrapper {
                margin-top: 140px;
            }
        }

        @media (max-width: 730px) {
            .moduletable_mainmenu li a, .moduletable_mainmenu li span {
                color: #000 !important;
            }

            .mob {
                display: block;
            }

            .desk {
                display: none;
            }

            .main-menu {
                display: none;
            }

            .content-info {
                text-align: center;
            }

            .custom_infoline a {
                display: inline-block;
                float: none;
            }

            .slider-wrapper {
                width: 400px;
                margin: auto;
            }

            .slider-wrapper .slider-container .ws-title {
                width: 360px !important;
                left: 10px !important;
            }

            .slider-wrapper .slider-container {
                height: 460px;
            }

            .slider-wrapper .ws-title-wrapper {
                text-align: center;
            }

            .slider-wrapper .slide-btn-services {
                margin-right: 0px;
                width: 300px;
            }

            .slider-wrapper .slide-btn-order {
                margin-top: 15px;
                width: 296px;
                height: unset;
                line-height: 26px;
                padding-top: 15px;
                padding-bottom: 15px;
            }

            .slider-wrapper .slide-btn-order span {
                line-height: 20px
            }

            .slider-wrapper .ws_controls {
                display: block;
                z-index: 99;
                position: relative;
                bottom: 26px;
            }

            .slider-container .ws_images .ws_list img, .slider-container .ws_images > div > img {
                width: 900px;
                height: 500px;
            }

            .img-menu {
                display: block;
                width: 25px;
                position: absolute;
                margin-left: -110px;
                top: 15px;
            }

            .main-menu.open {
                display: block;
                position: absolute;
                width: 100%;
                top: 0;
                background: #f5f5f5;
                margin-top: 50px;
                z-index: 120;
            }

            .main-menu.open, .main-menu.open .fix-menu {
                height: unset;
                background: #f5f5f5;
                padding-bottom: 20px;
            }

            .main-menu.open .moduletable_mainmenu .nav.menu {
                display: block;
            }

            .fix-menu.fixed {
                position: relative !important;
            }

            .main-menu.open .moduletable_mainmenu li {
                float: none;
                height: unset;
                line-height: 26px;
                padding-bottom: 10px;
                text-align: left;
            }

            .main-menu.open .moduletable_mainmenu {
                width: 100%;
            }

            .main-menu.open .moduletable_mainmenu li a, .moduletable_mainmenu li span {
                color: #000;
            }

            .main-menu.open .moduletable_mainmenu li .submenu {
                width: 100%;
                position: static;
                text-align: left;
                display: block !important;
                margin-top: 4px;
                opacity: 1 !important;
            }

            .product-description {
                overflow-x: auto;
            }

            .moduletable_mainmenu .item-334 {
                margin-bottom: -20px;
            }

            .moduletable_mainmenu li:nth-child(4) .submenu li {
                width: 347px !important;
            }

            .item-346 {
                width: 347px !important;
                margin-bottom: -20px;
            }

            .main-menu.open .moduletable_mainmenu li .submenu a {
                font-size: 16px;
            }

            #vmCartModule.vm_cart_products {
                display: none
            }

            #vmCartModule.open .vm_cart_products {
                display: block
            }

            .price-selector {
                display: none;
            }

            .bgblack {
                z-index: 130;
            }

        }
    </style>
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext"
        rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/css/styles.css"
          type="text/css" media="screen"/>
    <script type="text/javascript"
            src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/js/init.js"></script>
</head>
<body>
<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq)return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq)f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2092550204307367');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1" width="1"
         src="https://www.facebook.com/tr?id=2092550204307367&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<div class="bgblack"></div>
<jdoc:include type="modules" name="popup" style="xhtml"/>
<div class="popup" id="message-popup">
    <div class="popup-inner">
        <a href="#" class="close-icon" title="Закрыть"></a>
        <div class="popup-header"><span>Сообщение</span></div>
        <div class="message-text"></div>
    </div>
</div>
<?php if(!($_GET['option'] == 'com_calculator' && $_GET['task'] == 'station.kp')): ?>
<div class="header-top">
    <div class="body-inner">
        <jdoc:include type="modules" name="topline" style="xhtml"/>
        <div class="clr"></div>
    </div>
</div>
<div class="header-line"></div>
<div class="header">
    <div class="body-inner">
        <?php if ($home) { ?>
            <span id="logo"><img
                    src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo.png"
                    alt="<?php echo JText::_('JLOGO_TITLE') ?>"/></span>
        <?php } else { ?>
            <a href="/" title="" id="logo"><img
                    src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template; ?>/images/logo.png"
                    alt="<?php echo JText::_('JLOGO_TITLE') ?>"/></a>
        <?php } ?>
        <jdoc:include type="modules" name="header" style="xhtml"/>
        <div class="moduletable_topcontacts">

            <div class="contacts-right" style="width: 271px; padding-left: 64px;">
                <i style="width: 46px; height: 50px; background-position: -238px 4px;"></i>
                <div class="contact-address">+38 099 493 58 35 (<?php echo JText::_('JHEADER_DIRECTOR') ?>)</div>
                <div class="contact-address">+38 099 440 42 24 (<?php echo JText::_('JHEADER_INGENEER') ?>)</div>
                <div class="contact-address">info@solarstrategia.com</div>
            </div>
            <div class="clr"></div>
        </div>

        <div class="moduletable_topcontacts">

            <div class="contacts-right" style="padding-left: 50px;">
                <i></i>
                <div class="contact-address"><?php echo JText::_('JHEADER_ADDRESS') ?></div>
                <div class="contact-address"></div>
            </div>
            <div class="clr"></div>
        </div>
        <div class="dialer-form moduletable_topcontacts">
            <p><?php echo JText::_('JHEADER_LOGIN_DEALER') ?></p>
            <form action="/" method="post" id="login-form">
                <div class="dialer-input">
                    <i class="dialer-login"></i>
                    <input type="text" name="username" placeholder="<?php echo JText::_('JHEADER_LOGIN') ?>" required>
                </div>
                <div class="dialer-input">
                    <i class="dialer-psw"></i>
                    <input type="text" name="password" placeholder="<?php echo JText::_('JHEADER_PASSWORD') ?>" required>
                </div>
            </form>
        </div>
        <div class="clr"></div>
    </div>
</div>
<div class="main-menu">
    <div class="fix-menu">
        <div class="body-inner">
            <jdoc:include type="modules" name="main-menu" style="xhtml"/>
            <div class="clr"></div>
        </div>
    </div>
</div>
<div class="clr"></div>
<?php endif; ?>
<?php if ($this->countModules('breadcrumbs')) : ?>
    <div class="body-inner" <?php if ($_GET['option'] == 'com_calculator' && $_GET['task'] == 'station.kp') : ?>style="display: none"<?php endif; ?>>
        <jdoc:include type="modules" name="breadcrumbs" style="xhtml"/>
        <div class="clr"></div>
    </div>
<?php endif; ?>
<?php if ($this->countModules('slider')) : ?>
    <div class="slider-wrapper">
        <jdoc:include type="modules" name="slider" style="xhtml"/>
    </div>
<?php endif; ?>
<?php if ($this->countModules('content-info')) : ?>
    <div class="content-info">
        <div class="body-inner">
            <jdoc:include type="modules" name="content-info" style="xhtml"/>
            <div class="clr"></div>
        </div>
    </div>
<?php endif; ?>
<?php if ($this->countModules('services')) : ?>
    <div class="services">
        <div class="body-inner">
            <jdoc:include type="modules" name="services" style="xtml"/>
            <div class="clr"></div>
        </div>
    </div>
<?php endif; ?>
<?php if ($this->countModules('work-process')) : ?>
    <div class="work-process">
        <div class="body-inner">
            <jdoc:include type="modules" name="work-process" style="xtml"/>
        </div>
    </div>
<?php endif; ?>
<?php if ($this->countModules('calculator')) : ?>
    <div class="calculator-wrapper">
        <div class="body-inner">
            <jdoc:include type="modules" name="calculator" style="xhtml"/>
        </div>
    </div>
<?php endif; ?>
<?php if ($this->countModules('dealer')) : ?>
    <div class="dealer-wrapper">
        <div class="body-inner">
            <jdoc:include type="modules" name="dealer" style="xhtml"/>
        </div>
    </div>
<?php endif; ?>
<?php if ($this->countModules('team')) : ?>
    <div class="team-wrapper">
        <div class="body-inner">
            <jdoc:include type="modules" name="team" style="xhtml"/>
            <div class="clr"></div>
        </div>
    </div>
<?php endif; ?>
<?php if ($this->countModules('aboutinfo')) : ?>
    <div class="aboutinfo-wrapper">
        <div class="body-inner">
            <jdoc:include type="modules" name="aboutinfo" style="xhtml"/>
        </div>
    </div>
<?php endif; ?>
<?php if ($this->countModules('counter')) : ?>
    <div class="counter-wrapper">
        <div class="body-inner">
            <jdoc:include type="modules" name="counter" style="xhtml"/>
        </div>
    </div>
<?php endif; ?>
<?php if ($this->countModules('content-top')) : ?>
    <div class="body-inner">
        <jdoc:include type="modules" name="content-top" style="xhtml"/>
    </div>
<?php endif; ?>
<?php if ($this->countModules('news')) : ?>
    <div class="news-wrapper">
        <div class="body-inner">
            <jdoc:include type="modules" name="news" style="xtml"/>
        </div>
    </div>
<?php endif; ?>

<?php if ($this->countModules('comments')) : ?>
    <div class="comments-wrapper">
        <div class="body-inner">
            <jdoc:include type="modules" name="comments" style="xhtml"/>
        </div>
    </div>
<?php endif; ?>

<?php if ($this->countModules('partners')) : ?>
    <div class="partners-wrapper">
        <div class="body-inner">
            <jdoc:include type="modules" name="partners" style="part"/>
        </div>
    </div>
<?php endif; ?>

<?php if (!$home) { ?>
    <div class="content-wrapper">
        <div class="body-inner">
            <div
                class="content-left  <?php if (!$this->countModules('content-left') || $view == 'productdetails' || $menu->getActive()->id == 238) echo 'left-hidden' ?>">
                <jdoc:include type="modules" name="content-left" style="xhtml"/>
            </div>
            <div
                class="content-right <?php if (!$this->countModules('content-left') || $view == 'productdetails' || $menu->getActive()->id == 238) echo 'right-full' ?>">
                <jdoc:include type="message"/>
                <jdoc:include type="component"/>
                <jdoc:include type="modules" name="content-bottom" style="xhtml"/>
            </div>
            <div class="clr"></div>
        </div>
    </div>
<?php } ?>

<?php if ($this->countModules('map')) : ?>
    <div class="map-wrapper">
        <jdoc:include type="modules" name="map" style="xhtml"/>
    </div>
<?php endif; ?>

<?php if ($this->countModules('question')) : ?>
    <div class="question-wrapper">
        <div class="body-inner">
            <jdoc:include type="modules" name="question" style="xtml"/>
        </div>
    </div>
<?php endif; ?>

<?php if ($this->countModules('featured')) : ?>
    <div class="featured-wrapper">
        <div class="body-inner">
            <jdoc:include type="modules" name="featured" style="xtml"/>
        </div>
    </div>
<?php endif; ?>

<?php if (!($_GET['option'] == 'com_calculator' && $_GET['task'] == 'station.kp')) : ?>
<div class="footer">
    <div class="body-inner">
        <jdoc:include type="modules" name="footer" style="footer"/>
        <?php if ($home) { ?>
            <div class="logofooter"></div>
        <?php } else { ?>
            <a href="/" class="logofooter" title=""></a>
        <?php } ?>
        <div class="clr"></div>
    </div>
</div>
<div class="copyright">
    <div class="body-inner">
        <div class="copyright-text">© 2016 - <?php echo date("Y") ?> solarstrategia.com sadfsdf</div>
        <jdoc:include type="modules" name="footer-bottom" style="xhtml"/>
        <div class="clr"></div>
    </div>
</div>
<?php endif; ?>
<script>
    var cf1 = [{breakpoint: 1200, settings: {slidesToShow: 2, slidesToScroll: 1}}, {
        breakpoint: 710,
        settings: {slidesToShow: 1, slidesToScroll: 1}
    }];
    jQuery(document).ready(function () {
        if (jQuery('#news139').length > 0) {
            jQuery('#news139').slick('slickSetOption', 'responsive', cf1, true);
            jQuery('#services-carusel120').slick('slickSetOption', 'responsive', cf1, true);
            jQuery('#services-carusel141').slick('slickSetOption', 'responsive', cf1, true);
        }
        if (jQuery('#news197').length > 0) {
            jQuery('#news197').slick('slickSetOption', 'responsive', cf1, true);
            jQuery('#services-carusel121').slick('slickSetOption', 'responsive', cf1, true);
            jQuery('#services-carusel142').slick('slickSetOption', 'responsive', cf1, true);
        }
        if (jQuery('services-carusel123').length > 0) {
            jQuery('#services-carusel123').slick('slickSetOption', 'responsive', cf1, true);
            jQuery('#services-carusel143').slick('slickSetOption', 'responsive', cf1, true);
        }

        jQuery('.header-top .social-wrapper').prepend('<img src="/templates/virtuastemp3/images/menu-icon.png" class="img-menu">');

        jQuery('.img-menu').click(function () {
            if (jQuery('.main-menu').hasClass('open')) {
                jQuery('.main-menu').removeClass('open');
            } else {
                jQuery('.main-menu').addClass('open');
            }

            return false;
        });

        jQuery('#vmCartModule').click(function () {
            if (jQuery('#vmCartModule').hasClass('open')) {
                jQuery('#vmCartModule').removeClass('open');
                jQuery('#vmCartModule .vm_cart_products').hide();
            } else {
                jQuery('#vmCartModule').addClass('open');
                jQuery('#vmCartModule .vm_cart_products').show();
            }

            return false;
        });

        jQuery('.vmcontainer a').click(function () {
            document.location.href = jQuery(this).attr('href');
        });


        jQuery('#login').remove().appendTo('body');

        jQuery('.popup .close-icon').click(function () {
            jQuery('.bgblack').fadeOut();
            jQuery('.popup').hide();
            return false;
        });


    });

    function openPopup(obj) {
        jQuery('.moduletable_ordercalc').css('top', '-550px');
        jQuery('.bgblack').fadeIn();
        jQuery('.moduletable_ordercalc').show();
        jQuery('.moduletable_ordercalc input[name=typest]').val(jQuery(obj).attr('type'));
        jQuery('.moduletable_ordercalc').animate({'top': '100px'}, 1000);

        return false;
    }
</script>


<script  async="async" type="text/javascript">
    (function (d, w, s) {
        var widgetHash = 'kljlt3ya6p3255k9khhi', gcw = d.createElement(s);
        gcw.type = 'text/javascript';
        gcw.async = true;
        gcw.src = '//widgets.binotel.com/getcall/widgets/' + widgetHash + '.js';
        var sn = d.getElementsByTagName(s)[0];
        sn.parentNode.insertBefore(gcw, sn);
    })(document, window, 'script');
</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-98447396-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>