<?php defined('_JEXEC') or die('Restricted access');
$app	= JFactory::getApplication();
// - - - - - - - - - - 
// Images
// - - - - - - - - - -
//echo '<pre>'; var_dump($this->items); echo '</pre>';
if (!empty($this->items)) {

	if ($this->tmpl['detail_window'] == 14) {
		echo '<div id="pg-msnr-container" class="pg-photoswipe pg-msnr-container" itemscope itemtype="http://schema.org/ImageGallery">';
	} else {
		echo '<div id="pg-msnr-container" class="pg-msnr-container">';
	}
	foreach($this->items as $ck => $cv) {
	
		if ($this->checkRights == 1) {
			// USER RIGHT - Access of categories (if file is included in some not accessed category) - - - - -
			// ACCESS is handled in SQL query, ACCESS USER ID is handled here (specific users)
			$rightDisplay	= 0;
			if (!isset($cv->cataccessuserid)) {
				$cv->cataccessuserid = 0;
			}
			
			if (isset($cv->catid) && isset($cv->cataccessuserid) && isset($cv->cataccess)) {
				$rightDisplay = PhocaGalleryAccess::getUserRight('accessuserid', $cv->cataccessuserid, $cv->cataccess, $this->tmpl['user']->getAuthorisedViewLevels(), $this->tmpl['user']->get('id', 0), 0);
				
			}
			// - - - - - - - - - - - - - - - - - - - - - -
		} else {
			$rightDisplay = 1;
		}

		
		// Display back button to categories list
		if ($cv->item_type == 'categorieslist'){
			$rightDisplay = 1;
		}
		
		if ($rightDisplay == 1) {
		
			// BOX Start
			echo "\n\n";
			echo '<div class="pg-cv-box item pg-grid-sizer">'."\n";
			
            echo '<div class="pg-cv-box-img pg-box1">'."\n";
			
		
			// A Start
			echo '<a class="img-gal" data-fancybox="gallery"';
			if ($cv->type == 2) {
				if ($cv->overlib == 0) {
					//echo ' title="'.$cv->odesctitletag.'"';
					echo ' title="'.htmlentities ($cv->odesctitletag, ENT_QUOTES, 'UTF-8').'"';
				}
			} 
		
			echo ' href="'. $cv->linkorig.'"';	
			// Correct size for external Image (Picasa) - subcategory
			$extImage = false;
			if (isset($cv->extid)) {
				$extImage = PhocaGalleryImage::isExtImage($cv->extid);
			}
			if ($extImage && isset($cv->extw) && isset($cv->exth)) {
				$correctImageRes = PhocaGalleryPicasa::correctSizeWithRate($cv->extw, $cv->exth, $this->tmpl['picasa_correct_width_m'], $this->tmpl['picasa_correct_height_m'], $this->tmpl['diff_thumb_height']);
			}
			// Image Box (image, category, folder)
			if ($cv->type == 2 ) {
				// Render OnClick, Rel
				echo PhocaGalleryRenderFront::renderAAttribute($this->tmpl['detail_window'], $cv->button->options, $cv->linkorig, $this->tmpl['highslideonclick'], '', $cv->linknr, $cv->catalias);
				
				// SWITCH OR OVERLIB 
				if ($this->tmpl['switch_image'] == 1) {
					echo PhocaGalleryRenderFront::renderASwitch($this->tmpl['switch_width'], $this->tmpl['switch_height'], $this->tmpl['switch_fixed_size'], $cv->extwswitch, $cv->exthswitch, $cv->extl, $cv->linkthumbnailpath);
				} else {
					echo $cv->overlib_value;					
				}
				
				if (isset($cv->datasize)) {
					echo ' '. $cv->datasize;
				}
				
				
				echo ' >';// A End
            echo '<span class="pg-img-cover"></span>';
			echo '<span class="pg-img-zoom"></span>';
					echo JHtml::_( 'image', $cv->linkthumbnailpath, $cv->oimgalt, $cv->ooverlibclass);
				
				
				// IMG End
			
			} 
				
			
			
			// A CLOSE
			echo '</a>';
			
			
			
			

			// BOX End
				
			
			
			
			

			
			
			
			
			
			
				
		
			
			echo '</div></div>'. "\n";
			
			

		}
	}
	
	
	echo '<div class="clr"></div></div>'."\n";

} else {
	// Will be not displayed
	//echo JText::_('COM_PHOCAGALLERY_THERE_IS_NO_IMAGE');
}