<?php
defined('_JEXEC') or die('Restricted access');

$this->cv = new stdClass();
echo '<div id="pg-msnr-container">';

foreach ($this->categories as $ck => $cv){


	
	echo '<div class="gallery-item">'."\n";
	
	
	echo '<a href="'.$cv->link.'">'. "\n";

		echo JHtml::_( 'image', $cv->linkthumbnailpath, $cv->title);
        echo '<span class="gallery-item-name">'.PhocaGalleryText::wordDelete($cv->title_self, $this->tmpl['char_cat_length_name'], '...').'</span>';
	    echo ' <span class="pg-csv-count">'.$cv->numlinks.'</span>'. "\n";
		echo '<span class="pg-img-cover"></span>';
	echo '</a>'. "\n";

	
	

	
	
	if ($this->tmpl['display_cat_desc_box'] == 1  && $cv->description != '') {
		echo '<div class="pg-csv-descbox">'. strip_tags($cv->description).'</div>';
	} else if ($this->tmpl['display_cat_desc_box'] == 2  && $cv->description != '') {	
		echo '<div class="pg-csv-descbox">' .(JHtml::_('content.prepare', $cv->description, 'com_phocagallery.category')).'</div>';
	}
	
	$this->cv = $cv;
	
	

	echo '</div>'. "\n";// End pg-csv-box
}
echo '</div>';
echo '<div class="ph-cb"></div>';
?>