<?php
/**
*
* Shows the products/categories of a category
*
* @package	VirtueMart
* @subpackage
* @author Max Milbers
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2014 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
 * @version $Id: default.php 6104 2012-06-13 14:15:29Z alatak $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

$categories = $viewData['categories'];
$categories_per_row = VmConfig::get ( 'categories_per_row', 3 );
$model = VmModel::getModel('category'); 
$catImgAmount = VmConfig::get('catimg_browse',1);

?>

<div class="cat-full">
    <?php foreach($categories as $cat){?>
    <div class="catfull-title">
        <?php echo $cat->category_name;?>
    </div>
    <?php 
	   $childCategories = $model->getChildCategoryList(1,$cat->virtuemart_category_id); 
	   $model->addImages($childCategories,$catImgAmount);
	?>
    <div class="catfull-row">
         <?php foreach ($childCategories as $child){ 
		     $caturl = JRoute::_ ( 'index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $child->virtuemart_category_id , FALSE);
		 ?>
             
             <a href="<?php echo $caturl ?>" title="<?php echo vmText::_($child->category_name) ?>">
                 <?php echo $child->images[0]->displayMediaThumb("",false); ?>
                 <span><?php echo $child->category_name;?></span>
             </a>
         <?php }?>
         <div class="clr"></div>
    </div>
    <?php } ?>
</div>

