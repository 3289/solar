<?php
/**
 * sublayout products
 *
 * @package	VirtueMart
 * @author Max Milbers
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL2, see LICENSE.php
 * @version $Id: cart.php 7682 2014-02-26 17:07:20Z Milbo $
 */

defined('_JEXEC') or die('Restricted access');
$products_per_row = $viewData['products_per_row'];
$currency = $viewData['currency'];
$showRating = $viewData['showRating'];
$verticalseparator = " vertical-separator";
echo shopFunctionsF::renderVmSubLayout('askrecomjs');

$user = JFactory::getUser();
$ItemidStr = '';
$Itemid = shopFunctionsF::getLastVisitedItemId();
if(!empty($Itemid)){
	$ItemidStr = '&Itemid='.$Itemid;
}

foreach ($viewData['products'] as $type => $products ) {

	$rowsHeight = shopFunctionsF::calculateProductRowsHeights($products,$currency,$products_per_row);

	if(!empty($type) and count($products)>0){
		$productTitle = vmText::_('COM_VIRTUEMART_'.strtoupper($type).'_PRODUCT'); ?>
<div class="<?php echo $type ?>-view">
  <h4><?php echo $productTitle ?></h4>
		<?php // Start the Output
    }

	// Calculating Products Per Row
	$cellwidth = ' width'.floor ( 100 / $products_per_row );

	$BrowseTotalProducts = count($products);

	$col = 1;
	$nb = 1;
	$row = 1;
	
	$lang = JFactory::getLanguage();
						//			
//echo '<pre>'; var_dump($lang->getTag()); echo '</pre>';
	foreach ( $products as $product ) {
        
		//echo '<pre>'; var_dump($product->allPrices2); echo '</pre>';
		
		foreach ($product->allPrices2 as $price2){

		   if($price2['virtuemart_shoppergroup_id'] == 6){
			   $priceOpt = $currency->convertCurrencyTo(144,$price2["product_price"]);
		       $dillerPrice['opt'] =  $currency->createPriceDiv ('','', $priceOpt);
		   }
		   
		   if($price2['virtuemart_shoppergroup_id'] == 3){
		       $dillerPrice['dillerLevel1'] =  round($price2["product_price"], 2) . '$';
		   }
		   if($price2['virtuemart_shoppergroup_id'] == 4){
		       $dillerPrice['dillerLevel2'] =  round($price2["product_price"], 2) . '$';
		   }
		   if($price2['virtuemart_shoppergroup_id'] == 5){
		       $dillerPrice['dillerLevel3'] =  round($price2["product_price"], 2) . '$';
		   }
		}
		// Show the horizontal seperator
		if ($col == 1 && $nb > $products_per_row) { ?>
	<div class="horizontal-separator"></div>
		<?php }

		// this is an indicator wether a row needs to be opened or not
		if ($col == 1) { ?>
	<div class="row">
		<?php }

		// Show the vertical seperator
		if ($nb == $products_per_row or $nb % $products_per_row == 0) {
			$show_vertical_separator = ' ';
		} else {
			$show_vertical_separator = $verticalseparator;
		}

    // Show Products ?>
	<div class="product vm-col<?php echo ' vm-col-' . $products_per_row . $show_vertical_separator ?> <?php if(!$user->guest) echo ' diller';?>">
		<div class="spacer">
                    
                    <?php if(!$user->guest && $dillerPrice['opt']){?>
                        <div class="diller-opt">
                             <div class="diller-opt-labe">
                                 <?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_DILLER_PRICE_OPT')?>
                             </div>
                             <span><?php echo $dillerPrice['opt'];?></span>
                        </div>
					<?php }?>
                    <?php if($products_per_row != 4){?>
					<a class="product-img" title="<?php echo $product->product_name ?>" href="<?php echo $product->link.$ItemidStr; ?>">
					<?php }else{?>
                        <span class="product-img">
						<?php }
						echo $product->images[0]->displayMediaThumb('class="browseProductImage"', false);
						?>
                    <?php if($products_per_row != 4){?>    
					</a>
                    <?php }else{?>
                    </span>
                    <?php }?>
                    <div class="vm-product-right">
                    
                        <div class="vm-product-name list">
                             <h2><?php echo JHtml::link ($product->link.$ItemidStr, $product->product_name); ?></h2>
                        </div>
                        <?php //echo '<pre>'; var_dump($currency); echo '</pre>';?>
                        
                        <?php //echo $rowsHeight[$row]['price'] ?>
                        <?php if($products_per_row != 4){?>
                        <div class="vm3pr-<?php echo $rowsHeight[$row]['price'] ?>"> <?php
						if (!empty($product->prices['salesPrice'])) {
                            echo shopFunctionsF::renderVmSubLayout('prices',array('product'=>$product,'currency'=>$currency));
						}
							 ?>
                        </div>
                        <?php }?>
                        
                        <div class="vm-product-info">
                        
                              <div class="product-sku">
                                   <?php if($product->product_sku){?>
                                   <div class="product-sku-inner">
                                   <span><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_SKU')?>:</span> 
                                   <?php echo $product->product_sku; ?>
                                   </div>
                                   <?php }?>
                                   
                                   
                              </div>
                              <div class="product-infoline">
                                 <?php 
								     if (empty($product->prices['salesPrice'])) {
										$product->product_in_stock = 0; 
									 }
								 ?>
                                 <?php if($product->product_in_stock > 10){?>
                                       <div class="product-stock avaible"><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_STOCK_AVAIBLE')?></div>
                                  <?php }?> 
                                  <?php if($product->product_in_stock < 10 && $product->product_in_stock > 0){?>
                                       <div class="product-stock ends"><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_STOCK_ENDS')?></div>
                                  <?php }?>  
                                   <?php if($product->product_in_stock == 0){?>
                                       <div class="product-stock notavaible"><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_STOCK_NOTAVAIBLE')?></div>
                                  <?php }?>   
                              </div>
                              <div class="vm-product-comment">
                                  <span><?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_TAB_COMMENTS')?>:</span><i><?php echo $product->nemReviews;?></i>
                                  <div class="clr"></div>
                              </div>
                              
                              <?php if($product->video1 || $product->video2){?>
                              <div class="vm-product-video">
                                  <a href="#"></a>
                              </div>
                              <?php }?>   
                              <div class="clr"></div>
                        </div>
                        
						<?php if($product->product_desc){?>
                        <div class="vm-product-desc">
                             <?php echo mb_substr(strip_tags($product->product_desc),0,200).'...'; ?>
                        </div>
                        <?php }?>
                        
                        
                        <div class="vm-product-name">
                            <?php if($products_per_row == 4){
                               echo $product->product_name;     
                            }else{?>
                            <h2><?php echo JHtml::link ($product->link.$ItemidStr, $product->product_name); ?></h2>
                            <?php }?>
                            
                        </div>
                        
                        <?php if(!$user->guest){?>
                            <div class="diller-price-wrap">
                            

                            <div class="diller-prices">
                                
                                <div class="diller-price-header">
                                    <?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_DILLER_PRICE_HEADER');?>
                                </div>
                                <div class="diller-p-inner">
                                <?php if($dillerPrice['dillerLevel1']){?>
                                    <div class="price-row">
                                        <div class="diller-p-label">
                                             <?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_DILLER_PRICE_LEVEL1');?>
                                        </div>
                                        <div class="diller-p-val">
                                            <?php echo $dillerPrice['dillerLevel1'];?>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                <?php }?>
                                <?php if($dillerPrice['dillerLevel2']){?>
                                    <div class="price-row">
                                        <div class="diller-p-label">
                                            <?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_DILLER_PRICE_LEVEL2');?>
                                        </div>
                                        <div class="diller-p-val">
                                            <?php echo $dillerPrice['dillerLevel2'];?>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                <?php }?>
                                <?php if($dillerPrice['dillerLevel3']){?>
                                    <div class="price-row">
                                        <div class="diller-p-label">
                                             <?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_DILLER_PRICE_LEVEL3');?>
                                        </div>
                                        <div class="diller-p-val">
                                            <?php echo $dillerPrice['dillerLevel3'];?>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                <?php }?>
                                </div>
                                </div>

                            </div>
						<?php }?>
                        
                        <div class="product-nbu">
                            <span>* <?php echo vmText::_('COM_VIRTUEMART_PRICE_NBU'); ?></span>
                        </div>
                        <div class="clr"></div>
                        
                        <div class="addtocart-bar"> <?php
						    if($products_per_row == 4){
						       echo '<a href="#" class="solut-order"><i></i>'. vmText::_('COM_VIRTUEMART_SOLUT_ORDER').'</a>';
							}else{
                            echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$product,'rowHeights'=>$rowsHeight[$row])); 
							}
							?>
                        </div>
                        <div class="credit">
                            <?php if ($product->credit){?>
                                 <?php
                                     $itemId = 329;
									 if($lang->getTag() == 'uk-UA'){
										$itemId = 330; 
									 }
									 if($lang->getTag() == 'en-GB'){
										$itemId = 331; 
									 }
								 ?>
                                 <a href="<?php echo JRoute::_('index.php?Itemid=' . $itemId)?>" class="credit-button"><i></i><?php echo vmText::_('COM_VIRTUEMART_CREDIT');?><span></span></a>
                            <?php }?>
                        </div>
                        <a href="" class="vm-readmore"><?php echo vmText::_('COM_VIRTUEMART_FEED_READMORE');?></a>
                        <div class="clr"></div>
                        
                    </div>
                    
                   <div class="clr"></div>
                   


			
		
			

		</div>
	</div>

	<?php
    $nb ++;
		       $dillerPrice['opt'] =  0;;
		       $dillerPrice['dillerLevel1'] =  '';
		       $dillerPrice['dillerLevel2'] =  '';
		       $dillerPrice['dillerLevel3'] =  '';
			   
      // Do we need to close the current row now?
      if ($col == $products_per_row || $nb>$BrowseTotalProducts) { ?>
    <div class="clr"></div>
  </div>
      <?php
      	$col = 1;
		$row++;
    } else {
      $col ++;
    }
  }

      if(!empty($type)and count($products)>0){
        // Do we need a final closing row tag?
        //if ($col != 1) {
      ?>
    <div class="clr"></div>
  </div>
    <?php
    // }
    }
  }
