<?php
/**
 *
 * Show the product details page
 *
 * @package    VirtueMart
 * @subpackage
 * @author Max Milbers, Valerie Isaksen
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default_reviews.php 9227 2016-05-27 10:55:25Z Milbo $
 */

// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die ('Restricted access');

// Customer Reviews
$review_editable = true;
$this->allowRating = true;
 $this->allowReview = true;
if ($this->allowRating || $this->allowReview || $this->showRating || $this->showReview) {

	$maxrating = VmConfig::get( 'vm_maximum_rating_scale', 5 );
	$ratingsShow = VmConfig::get( 'vm_num_ratings_show', 3 ); // TODO add  vm_num_ratings_show in vmConfig
	$stars = array();
	//$showall = vRequest::getBool( 'showall', FALSE );
	$ratingWidth = $maxrating*70;
	for( $num = 0; $num<=$maxrating; $num++ ) {
		$stars[] = '
				<span title="'.(vmText::_( "COM_VIRTUEMART_RATING_TITLE" ).$num.'/'.$maxrating).'" class="vmicon ratingbox" style="display:inline-block;width:'. 70*$maxrating.'px;">
					<span class="stars-orange" style="width:'.(70*$num).'px">
					</span>
				</span>
				<span class="reting-text">
				     <i>'.vmText::_( "COM_VIRTUEMART_RATING_TITLE_TEXT_VERY_BAD" ).'</i>
					 <i>'.vmText::_( "COM_VIRTUEMART_RATING_TITLE_TEXT_BAD" ).'</i>
					 <i>'.vmText::_( "COM_VIRTUEMART_RATING_TITLE_TEXT_GOOD" ).'</i>
					 <i>'.vmText::_( "COM_VIRTUEMART_RATING_TITLE_TEXT_VERY_GOOD" ).'</i>
					 <i>'.vmText::_( "COM_VIRTUEMART_RATING_TITLE_TEXT_SUPER" ).'</i>
				</span>
				<span class="clr"></span>
				';
			       
	}

	echo '<div class="customer-reviews">';?>
    
   
    <?php

	if ($this->rating_reviews) {
		foreach( $this->rating_reviews as $review ) {
			/* Check if user already commented */
			// if ($review->virtuemart_userid == $this->user->id ) {
			if ($review->created_by == $this->user->id && !$review->review_editable) {
				$review_editable = false;
			}
		}
	}
}


if ($this->allowRating or $this->allowReview) {



	if ($review_editable) {
		?>
         <div class="comment-line">
        <div class="comment-head"><?php echo vmText::_( 'COM_VIRTUEMART_WRITE_REVIEW_NUM_REWIEWS' );?></div>
        <div class="comment-nums"><?php echo count($this->rating_reviews);?></div>
        <a href="#" class="comment-add-link"><i></i><?php echo vmText::_( 'COM_VIRTUEMART_WRITE_REVIEW_ADD_LINK' );?></a>
        <div class="clr"></div>
    </div>
        <div class="comment-form">
		<form method="post"
			  action="<?php echo JRoute::_( 'index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$this->product->virtuemart_product_id.'&virtuemart_category_id='.$this->product->virtuemart_category_id, FALSE ); ?>"
			  name="reviewForm" id="reviewform">
              <div class="add-comment-title"><?php echo vmText::_( 'COM_VIRTUEMART_WRITE_REVIEW_ADD_COMMENT_TITLE' );?></div>
			<?php if($this->allowRating and $review_editable) { ?>

				
				<div class="rating">
					<label for="vote"><?php echo $stars[$maxrating]; ?></label>
					<input type="hidden" id="vote" value="<?php echo $maxrating ?>" name="vote">
				</div>

				<?php

				$reviewJavascript = "
		jQuery(function($) {
			var steps = ".$maxrating.";
			var parentPos= $('.rating .ratingbox').position();
			var boxWidth = $('.rating .ratingbox').width();// nbr of total pixels
			var starSize = (boxWidth/steps);
			var ratingboxPos= $('.rating .ratingbox').offset();

			jQuery('.rating').on('mousemove','.ratingbox', function(e){
				var steps = ".$maxrating.";
			var parentPos= $('.rating .ratingbox').position();
			var boxWidth = $('.rating .ratingbox').width();// nbr of total pixels
			var starSize = (boxWidth/steps);
			var ratingboxPos= $('.rating .ratingbox').offset();
				var span = jQuery(this).children();
				var dif = e.pageX-ratingboxPos.left; // nbr of pixels
				//jQuery('.ttt').html(dif);
				difRatio = Math.floor(dif/boxWidth* steps )+1; //step
				span.width(difRatio*starSize);
				$('#vote').val(difRatio);
				//console.log('note = ',parentPos, boxWidth, ratingboxPos);
			});
		});
		";
				vmJsApi::addJScript( 'rating_stars', $reviewJavascript );

			}

			// Writing A Review
			if ($this->allowReview and $review_editable) {
			?>
             <?php 
				
				$user = JFactory::getUser();
				?>
            <div class="comment-fields">
                <div class="ttt"></div>
                <input type="text" name="user_name" value="<?php if(!$user->guest) echo $user->name?>" placeholder="<?php echo vmText::_( 'COM_VIRTUEMART_USER_FORM_NAME')?>" />
                <input type="email" name="user_email" value="<?php if(!$user->guest) echo $user->email?>" placeholder="<?php echo vmText::_( 'COM_VIRTUEMART_USER_FORM_EMAIL')?>"/>
                <div class="clr"></div>
            </div>
			<div class="write-reviews">

				<?php // Show Review Length While Your Are Writing
				$reviewJavascript = "
function check_reviewform() {
	

var form = document.getElementById('reviewform');
var ausgewaehlt = false;
var pattern = new RegExp(/^((\"[\w-\s]+\")|([\w-]+(?:\.[\w-]+)*)|(\"[\w-\s]+\")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

	if (form.comment.value.length < ".VmConfig::get( 'reviews_minimum_comment_length', 100 ).") {
		alert('".addslashes( vmText::sprintf( 'COM_VIRTUEMART_REVIEW_ERR_COMMENT1_JS', VmConfig::get( 'reviews_minimum_comment_length', 100 ) ) )."');
		return false;
	}
	else if (form.comment.value.length > ".VmConfig::get( 'reviews_maximum_comment_length', 2000 ).") {
		alert('".addslashes( vmText::sprintf( 'COM_VIRTUEMART_REVIEW_ERR_COMMENT2_JS', VmConfig::get( 'reviews_maximum_comment_length', 2000 ) ) )."');
		return false;
	}
	else if (form.user_name.value.length == 0 ) {
		alert('".addslashes( vmText::sprintf( 'COM_VIRTUEMART_REVIEW_ERR_NAME_JS' ) )."');
		return false;
	}
	else if (form.user_email.value.length == 0 ) {
		alert('".addslashes( vmText::sprintf( 'COM_VIRTUEMART_REVIEW_ERR_EMAIL_JS' ) )."');
		return false;
	}
	else if (form.user_email.value.length > 0 && !pattern.test(form.user_email.value)) {
		alert('".addslashes( vmText::sprintf( 'COM_VIRTUEMART_REVIEW_ERR_EMAIL_CORRECT_JS' ) )."');
		return false;
	}
	else {
		return true;
	}
}

function refresh_counter() {
	var form = document.getElementById('reviewform');
	form.counter.value= form.comment.value.length;
}
";

				vmJsApi::addJScript( 'check_reviewform', $reviewJavascript ); ?>
               
				<span
					class="step"><?php echo vmText::sprintf( 'COM_VIRTUEMART_REVIEW_COMMENT', VmConfig::get( 'reviews_minimum_comment_length', 100 ), VmConfig::get( 'reviews_maximum_comment_length', 2000 ) ); ?></span>
			
				<textarea class="virtuemart" title="<?php echo vmText::_( 'COM_VIRTUEMART_WRITE_REVIEW' ) ?>"
						  class="inputbox" id="comment" onblur="refresh_counter();" onfocus="refresh_counter();"
						  onkeyup="refresh_counter();" name="comment" rows="5"
						  cols="60"><?php if(!empty($this->review->comment)) {
						echo $this->review->comment;
					} ?></textarea>
			
		<div class="review-count"><?php echo vmText::_( 'COM_VIRTUEMART_REVIEW_COUNT' ) ?>
			<input type="text" value="0" size="4" name="counter" maxlength="4" readonly/>
				</div>
				<?php
				}

				if($review_editable and $this->allowReview) {
					?>
			
					<button class="highlight-button" type="submit" onclick="return( check_reviewform());"
						   name="submit_review" 
						   value="<?php echo vmText::_( 'COM_VIRTUEMART_REVIEW_SUBMIT' ) ?>"><i></i><?php echo vmText::_( 'COM_VIRTUEMART_REVIEW_SUBMIT' ) ?></button>
				<?php } else if($review_editable and $this->allowRating) { ?>
					<input class="highlight-button" type="submit" name="submit_review"
						   title="<?php echo vmText::_( 'COM_VIRTUEMART_REVIEW_SUBMIT' ) ?>"
						   value="<?php echo vmText::_( 'COM_VIRTUEMART_REVIEW_SUBMIT' ) ?>"/>
				<?php
				}

				?>    </div>
			<input type="hidden" name="virtuemart_product_id"
				   value="<?php echo $this->product->virtuemart_product_id; ?>"/>
			<input type="hidden" name="option" value="com_virtuemart"/>
			<input type="hidden" name="virtuemart_category_id"
				   value="<?php echo vRequest::getInt( 'virtuemart_category_id' ); ?>"/>
			<input type="hidden" name="virtuemart_rating_review_id" value="0"/>
			<input type="hidden" name="task" value="review"/>
		</form>
        </div>
	<?php 
	} else if(!$review_editable) {?>
		 <div class="comment-line">
        <div class="comment-head"><?php echo vmText::_( 'COM_VIRTUEMART_WRITE_REVIEW_NUM_REWIEWS' );?></div>
        <div class="comment-nums"><?php echo count($this->rating_reviews);?></div>
        <div class="clr"></div>
  
    </div>
    
	<?php	
		echo '<div class="already-comment">'.vmText::_( 'COM_VIRTUEMART_REVIEW_ALREADYDONE' ).'</div>';
	}
	
}


if ($this->showReview) {

	?>


	<div class="list-reviews">
		<?php
		$i = 0;
		//$review_editable = TRUE;
		$reviews_published = 0;
		if ($this->rating_reviews) {
			foreach ($this->rating_reviews as $review) {
				if ($i % 2 == 0) {
					$color = 'normal';
				} else {
					$color = 'highlight';
				}


				?>

				<?php // Loop through all reviews
				if (!empty($this->rating_reviews) && $review->published) {
					$reviews_published++;
					?>
					<div class="<?php echo $color ?> comment-block">
                        <div class="author"><?php echo $review->user_name?></div>
                        <div class="divide"></div>
						<div class="date"><?php echo JHtml::date ($review->created_on, vmText::_ ('DATE_FORMAT_LC')); ?></div>
						<div class="vote"><div class="stars" style="width:<?php echo (int)$review->review_rating * 24?>px"></div></div>
                        <div class="clr"></div>
						<blockquote><?php echo $review->comment; ?></blockquote>
					</div>
					<?php
				}
				$i++;
				if ($i == $ratingsShow && !$this->showall) {
					/* Show all reviews ? */
					if ($reviews_published >= $ratingsShow) {
						$attribute = array('class'=> 'details', 'title'=> vmText::_ ('COM_VIRTUEMART_MORE_REVIEWS'));
						echo JHtml::link ($this->more_reviews, vmText::_ ('COM_VIRTUEMART_MORE_REVIEWS'), $attribute);
					}
					break;
				}
			}

		} else {
			// "There are no reviews for this product"
			?>
			<span class="step"><?php echo vmText::_ ('COM_VIRTUEMART_NO_REVIEWS') ?></span>
			<?php
		}  ?>
		<div class="clear"></div>
	</div>
<?php
}

if ($this->allowRating || $this->allowReview || $this->showRating || $this->showReview) {
	echo '</div> ';
}
?>
