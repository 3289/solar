<?php
/**
 *
 * Show the product details page
 *
 * @package	VirtueMart
 * @subpackage
 * @author Max Milbers, Eugen Stranz, Max Galt
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 9292 2016-09-19 08:07:15Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

/* Let's see if we found the product */
if (empty($this->product)) {
	echo vmText::_('COM_VIRTUEMART_PRODUCT_NOT_FOUND');
	echo '<br /><br />  ' . $this->continue_link_html;
	return;
}

echo shopFunctionsF::renderVmSubLayout('askrecomjs',array('product'=>$this->product));
$ItemidStr = '';
$Itemid = shopFunctionsF::getLastVisitedItemId();
if(!empty($Itemid)){
	$ItemidStr = '&Itemid='.$Itemid;
}


if(vRequest::getInt('print',false)){ ?>
<body onload="javascript:print();">
<?php } ?>
<?php if($this->product->video1 || $this->product->video2){?>
<div id="video-popup" class="popup">
    <div class="popup-inner">
        <div class="popup-header"></div>
        <a href="#" class="close-icon"></a>
        <div class="video-content">
 
        </div>
    </div>
</div>
<?php }?>
<?php 
    if($this->product->video1){
    preg_match('/\?v=(.*)$/', $this->product->video1, $code);
    $videoSrc1 = 'https://www.youtube.com/embed/'.$code[1];
	}
	
	if($this->product->video2){
    preg_match('/\?v=(.*)$/', $this->product->video2, $code);
    $videoSrc2 = 'https://www.youtube.com/embed/'.$code[1];
	}
?>

<div id="question-popup" class="popup">
    <div class="popup-inner">
        <div class="popup-header"><span><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_ASK_QUESTION')  ?></span></div>
        <a href="#" class="close-icon"></a>
        <div class="question-content">
            <form id="question-form">
            <div class="question-header"><?php echo $this->product->product_name ?></div>
            <div class="question-message"></div>
            <div class="question-img">
                <?php
				echo $this->loadTemplate('images');
				?>
            </div>
            <div class="question-form">
                 <input type="text" name="name" data-title="<?php echo vmText::_( 'COM_VIRTUEMART_USER_FORM_NAME')?>" class="require" value="<?php if(!$user->guest) echo $user->name?>" placeholder="<?php echo vmText::_( 'COM_VIRTUEMART_USER_FORM_NAME')?>" />
                 <input type="email" name="email" data-title="<?php echo vmText::_( 'COM_VIRTUEMART_USER_FORM_EMAIL')?>" class="require" value="<?php if(!$user->guest) echo $user->email?>" placeholder="<?php echo vmText::_( 'COM_VIRTUEMART_USER_FORM_EMAIL')?>"/>
                 <textarea data-title="<?php echo vmText::_( 'COM_VIRTUEMART_USER_FORM_QUESTION')?>" placeholder="<?php echo vmText::_( 'COM_VIRTUEMART_USER_FORM_QUESTION')?>" class="require" name="question"></textarea>
                 <input type="hidden" name="product_name" value="<?php echo $this->product->product_name ?>" />
                 <input type="hidden" name="product_link" value="<?php echo JURI::base().$this->product->link.$Itemi; ?>" />
            </div>
             <div class="clr"></div>
            <a href="#" class="quest-link"><i></i><?php echo vmText::_('COM_VIRTUEMART_ASK_SUBMIT')  ?></a>
            </form>
        </div>
    </div>
</div>

<div class="product-container productdetails-view productdetails" >

  

	<?php // Back To Category Button
	if ($this->product->virtuemart_category_id) {
		$catURL =  JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$this->product->virtuemart_category_id, FALSE);
		$categoryName = vmText::_($this->product->category_name) ;
	} else {
		$catURL =  JRoute::_('index.php?option=com_virtuemart');
		$categoryName = vmText::_('COM_VIRTUEMART_SHOP_HOME') ;
	}
	?>
	
    <?php // Product Title   ?>
    <h1 itemprop="name"><?php echo $this->product->product_name ?></h1>
    <?php // Product Title END   ?>

    <?php // afterDisplayTitle Event
    echo $this->product->event->afterDisplayTitle ?>

    <?php
    // Product Edit Link
    echo $this->edit_link;
    // Product Edit Link END
    ?>
    
    <div class="product-infowrap">
         <div class="product-info-block">
	         <div class="spacer-buy-area">
		         <?php
					echo shopFunctionsF::renderVmSubLayout('rating',array('showRating'=>$this->showRating,'product'=>$this->product));
			
					if (is_array($this->productDisplayShipments)) {
						foreach ($this->productDisplayShipments as $productDisplayShipment) {
						echo $productDisplayShipment . '<br />';
						}
					}
					if (is_array($this->productDisplayPayments)) {
						foreach ($this->productDisplayPayments as $productDisplayPayment) {
						echo $productDisplayPayment . '<br />';
						}
					}
					
					if (!empty($this->product->prices['salesPrice'])) {
					echo shopFunctionsF::renderVmSubLayout('prices',array('product'=>$this->product,'currency'=>$this->currency));
					}
					?> <div class="clr"></div>
					
                       <div class="product-nbu">
                            <span>* <?php echo vmText::_('COM_VIRTUEMART_PRICE_NBU'); ?></span>
                        </div>
                    
                    <div class="clr"></div>
                    <div class="pinfo">
                        <?php if($this->product->product_sku){?>
                            <div class="product-sku">
                                <span><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_SKU')?></span>
                                <?php echo $this->product->product_sku;?>
                            </div>
                        <?php }?>     
                        <?php 
						    if (empty($this->product->prices['salesPrice'])) {
								$this->product->product_in_stock = 0;
							}
						?>     
                        <?php if($this->product->product_in_stock > 10){?>
                             <div class="product-stock avaible"><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_STOCK_AVAIBLE')?></div>
                        <?php }?>  
                        <?php if($this->product->product_in_stock < 10 && $this->product->product_in_stock > 0){?>
                             <div class="product-stock ends"><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_STOCK_ENDS')?></div>
                        <?php }?>  
                         <?php if($this->product->product_in_stock == 0){?>
                             <div class="product-stock notavaible"><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_STOCK_NOTAVAIBLE')?></div>
                        <?php }?>  
                        <div class="clr"></div>
                    </div>
					
					<?php
					
					echo shopFunctionsF::renderVmSubLayout('addtocart',array('product'=>$this->product));				

					// Ask a question about this product
					if (VmConfig::get('ask_question', 0) == 1) {
						$askquestion_url = JRoute::_('index.php?option=com_virtuemart&view=productdetails&task=askquestion&virtuemart_product_id=' . $this->product->virtuemart_product_id . '&virtuemart_category_id=' . $this->product->virtuemart_category_id . '&tmpl=component', FALSE);
						?>
						<div class="ask-a-question">
							<a class="ask-question" href="#" rel="nofollow" ><i></i><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_ENQUIRY_LBL') ?></a>
						</div>
					<?php
					}
					?>

					<?php
                    // Manufacturer of the Product
                    if (VmConfig::get('show_manufacturers', 1) && !empty($this->product->virtuemart_manufacturer_id)) {
                        echo $this->loadTemplate('manufacturer');
                    }
                    ?>
                  <div class="clr"></div>  
                       <div class="soc-likes-content">	
                <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.9&appId=312185572538651";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-button socbuttons-likes">
                <div class="fb-share-button" data-href="<?php echo JURI::current();?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo JURI::current();?>&amp;src=sdkpreparse">Поделиться</a></div>
            </div>
            <div class="twitter-button socbuttons-likes">
                <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            </div>
            
            <div class="google-button socbuttons-likes">
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'ru'}
</script>

<div class="g-plusone" data-size="medium" data-annotation="none"></div>
            </div>
            
            
            <div class="clr"></div>  
        </div>
        
        <div class="credit-product">
                            <?php if ($this->product->credit){?>
                                 <?php
                                     $itemId = 329;
									 if($lang->getTag() == 'uk-UA'){
										$itemId = 330; 
									 }
									 if($lang->getTag() == 'en-GB'){
										$itemId = 331; 
									 }
								 ?>
                                 <a href="<?php echo JRoute::_('index.php?Itemid=' . $itemId)?>" class="credit-button"><i></i><?php echo vmText::_('COM_VIRTUEMART_CREDIT');?><span></span></a>
                            <?php }?>
                        </div>

	        </div>
         </div>
         
         <div class="product-tabs">
             <ul class="tab-links">
                 <li><a href="#" class="active tabl-main"><i></i><span><?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_TAB_MAIN') ;?></span></a></li>
                 <li><a href="#" class="tabl-features"><i></i><span><?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_TAB_FEATURES') ;?></span></a></li>
                 <li><a href="#" class="tabl-photo"><i></i><span><?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_TAB_PHOTO') ;?></span></a></li>
                 <li><a href="#" class="tabl-video"><i></i><span><?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_TAB_VIDEO') ;?></span></a></li>
                 <li><a href="#" class="tabl-comments"><i></i><span><?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_TAB_COMMENTS') ;?></span></a></li>
             </ul>
             <div class="clr"></div>
             <div class="tabs">
                 <div class="tab" id="tab-main">
                      <div class="vm-product-media-container">
						  <?php
                          echo $this->loadTemplate('images');
                          ?>
                          <div class="additional-wrap">
                           <div class="additional-images">
                           <div class="additional-images-inner">
                           <?php
							$count_images = count ($this->product->images);
							if ($count_images > 1) {
								echo $this->loadTemplate('images_additional');
							} 
							?>
                            <?php if($this->product->video1){?>
                                <div class="floatleft video">
                                    <a href="#" data-src="<?php echo $videoSrc1?>"><i></i></a>
                                </div>
                            <?php }?>
                            
                             <?php if($this->product->video2){?>
                                <div class="floatleft video">
                                    <a href="#" data-src="<?php echo $videoSrc2?>"><i></i></a>
                                </div>
                            <?php }?>
                            </div>
                            </div>
                            </div>
                      <div class="clr"></div>
                      </div>
                     
                      <div class="clr"></div>
                      	<?php
						//echo ($this->product->product_in_stock - $this->product->product_ordered);
						// Product Description
						if (!empty($this->product->product_desc)) {
							?>
							<div class="product-description" >
						<?php /** @todo Test if content plugins modify the product description */ ?>
						<?php echo $this->product->product_desc; ?>
							</div>
						<?php
						} // Product Description END
						?>
                 </div>
                 <div class="tab" id="tab-features">
                        <div class="tab-heder">
                            <span><?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_TAB_FEATURES') ;?></span> <?php echo $this->product->product_name ?>
                        </div>
                      	<?php echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'normal'));?>
                 </div>
                 <div class="tab" id="tab-photo">
                      <div class="tab-heder">
                            <span><?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_TAB_PHOTO') ;?></span> <?php echo $this->product->product_name ?>
                      </div>
                      <div class="all-photos">
                          <?php foreach ($this->product->images as $image){
							  echo '<div class="photo-item"><a href="'. $image->file_url .'" title="'. $image->file_meta .'" rel="vm-additional-images"><img src="'. $image->file_url .'"  alt="'. $image->file_meta .'" /></a></div>';
						  }
						  ?>
                      </div>
                 </div>
                 <div class="tab" id="tab-video">
                      <div class="tab-heder">
                            <span><?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_TAB_VIDEO') ;?></span> <?php echo $this->product->product_name ?>
                      </div>
                      <div class="product-video">
                           <?php if($this->product->video1){?>
                                <div class="video-block">
                     
                                     <iframe width="560" height="315" src="<?php echo $videoSrc1?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                           <?php }?>
                           <?php if($this->product->video2){?>
                                <div class="video-block">
     
                                     <iframe width="560" height="315" src="<?php echo $videoSrc2?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                           <?php }?>
                      </div>
                 </div>
                 <div class="tab" id="tab-comments">
                      <div class="tab-heder">
                            <span><?php echo vmText::_('COM_VIRTUEMART_SHOP_PRODUCT_TAB_COMMENTS') ;?></span> <?php echo $this->product->product_name ?>
                      </div>
                      <div class="product-comments-wrap">
                           <?php  echo $this->loadTemplate('reviews'); ?>
                      </div>
                 </div>
                 <div class="clr"></div>
             </div>
         </div>
    </div>
  
    <?php
    // Product Short Description
    if (!empty($this->product->product_s_desc)) {
	?>
        <div class="product-short-description">
	    <?php
	    /** @todo Test if content plugins modify the product description */
	    echo nl2br($this->product->product_s_desc);
	    ?>
        </div>
	<?php
    } // Product Short Description END

	
    ?>

  



    <?php


    // Product Packaging
    $product_packaging = '';
    if ($this->product->product_box) {
	?>
        <div class="product-box">
	    <?php
	        echo vmText::_('COM_VIRTUEMART_PRODUCT_UNITS_IN_BOX') .$this->product->product_box;
	    ?>
        </div>
    <?php } // Product Packaging END ?>

    <?php 
	echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'onbot'));

  echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'related_products','class'=> 'product-related-products','customTitle' => true ));

	echo shopFunctionsF::renderVmSubLayout('customfields',array('product'=>$this->product,'position'=>'related_categories','class'=> 'product-related-categories'));

	?>

<?php // onContentAfterDisplay event
echo $this->product->event->afterDisplayContent;



// Show child categories
if (VmConfig::get('showCategory', 1)) {
	echo $this->loadTemplate('showcategory');
}

$j = 'jQuery(document).ready(function($) {
	$("form.js-recalculate").each(function(){
		if ($(this).find(".product-fields").length && !$(this).find(".no-vm-bind").length) {
			var id= $(this).find(\'input[name="virtuemart_product_id[]"]\').val();
			Virtuemart.setproducttype($(this),id);

		}
	});
});';
//vmJsApi::addJScript('recalcReady',$j);

if(VmConfig::get ('jdynupdate', TRUE)){

	/** GALT
	 * Notice for Template Developers!
	 * Templates must set a Virtuemart.container variable as it takes part in
	 * dynamic content update.
	 * This variable points to a topmost element that holds other content.
	 */
	$j = "Virtuemart.container = jQuery('.productdetails-view');
Virtuemart.containerSelector = '.productdetails-view';
//Virtuemart.recalculate = true;	//Activate this line to recalculate your product after ajax
";

	vmJsApi::addJScript('ajaxContent',$j);

	$j = "jQuery(document).ready(function($) {
	Virtuemart.stopVmLoading();
	var msg = '';
	$('a[data-dynamic-update=\"1\"]').off('click', Virtuemart.startVmLoading).on('click', {msg:msg}, Virtuemart.startVmLoading);
	$('[data-dynamic-update=\"1\"]').off('change', Virtuemart.startVmLoading).on('change', {msg:msg}, Virtuemart.startVmLoading);
});";

	vmJsApi::addJScript('vmPreloader',$j);
}

echo vmJsApi::writeJS();

if ($this->product->prices['salesPrice'] > 0) {
  echo shopFunctionsF::renderVmSubLayout('snippets',array('product'=>$this->product, 'currency'=>$this->currency, 'showRating'=>$this->showRating));
}

?>
</div>

<script type="text/javascript">
jQuery(function(){
   jQuery('.floatleft.video a').on('click',function(){
		  jQuery('.bgblack').fadeIn();
		  jQuery('.video-content').html('<iframe id="youtubevideo" width="580" height="320" src="'+jQuery(this).data('src')+'" frameborder="0" allowfullscreen></iframe>'); 
		  jQuery('#video-popup').show();
		  return false;
	  });
	  
	  
	function showMessage(message){
		jQuery('.message-text').html('<div class="text-inner">'+message+'</div>');
	    jQuery('#message-popup').show();
		jQuery('.bgblack').fadeIn();	
	}
	
	function popupHide(){
		jQuery('#message-popup').fadeOut();
		jQuery('.bgblack').fadeOut();
	}
	  
   jQuery('.quest-link').on('click',function() {
		var valid = true;
		var message = '';
		var require = 0;
		var requireText = '';
		pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		
		jQuery('#question-form .require').each(function(){
			if (!jQuery(this).val()){
				require = require + 1;
				requireText  += ' ' + jQuery(this).attr('data-title')+',';
			}
		});
		
		if(require){
			message = '<?php echo vmText::_('COM_VIRTUEMART_REVIEW_ERR_FILL_REQUIRE_FIELD')?>: '+ ' <span>' + requireText.substring(0, requireText.length - 1) + '</span>';
			valid = false;	
		}
		
        if(valid){
			if(jQuery('#question-form input[type=email]').length && !pattern.test(jQuery('#question-form input[type=email]').val())){
				message = '<?php echo vmText::_('COM_VIRTUEMART_REVIEW_ERR_EMAIL_CORRECT_JS')?>';
				valid = false;
			}
		}
		
		
		if (valid){
			var formData = jQuery('#question-form').serializeArray();
			formData.push({name:'option',value:'com_virtuemart'},{name:'view',value:'productdetails'},{name:'task',value:'sendquestion'},{name:'format',value:'raw'});
			jQuery('.popup-loader').show();
			jQuery.ajax({
			    type:'POST',
				data   : formData,
				success: function (response) {
					console.log(response);
					jQuery('#question-popup').hide();
					showMessage(response);	
					setTimeout(popupHide, 5000);
				}	
			});
			
		}else{
			jQuery('.question-message').html('<div class="q-message">'+message+'</div>');	
		}
		return false;
	});	  
});	
</script>

