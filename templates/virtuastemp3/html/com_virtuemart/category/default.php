<?php
/**
 *
 * Show the products in a category
 *
 * @package    VirtueMart
 * @subpackage
 * @author RolandD
 * @author Max Milbers
 * @todo add pagination
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 9288 2016-09-12 15:20:56Z Milbo $
 */

defined ('_JEXEC') or die('Restricted access');

if(vRequest::getInt('dynamic')){
	if (!empty($this->products)) {
		if($this->fallback){
			$p = $this->products;
			$this->products = array();
			$this->products[0] = $p;
			vmdebug('Refallback');
		}

		echo shopFunctionsF::renderVmSubLayout($this->productsLayout,array('products'=>$this->products,'currency'=>$this->currency,'products_per_row'=>$this->perRow,'showRating'=>$this->showRating));

	}

	return ;
}
?> <div class="category-view"> <?php
$js = "
jQuery(document).ready(function () {
	jQuery('.orderlistcontainer').hover(
		function() { jQuery(this).find('.orderlist').stop().show()},
		function() { jQuery(this).find('.orderlist').stop().hide()}
	)
});
";
vmJsApi::addJScript('vm.hover',$js);



// Show child categories
if ($this->showcategory and empty($this->keyword)) {
	if (!empty($this->category->haschildren)) {
		echo ShopFunctionsF::renderVmSubLayout('categories',array('categories'=>$this->category->children));
	}
}

if($this->showproducts){
?>
<div class="browse-view">
<?php

if ($this->showsearch or !empty($this->keyword)) {
	//id taken in the view.html.php could be modified
	$category_id  = vRequest::getInt ('virtuemart_category_id', 0); ?>

	
<?php
	/*if(!empty($this->keyword)){
		?><h3><?php echo vmText::sprintf('COM_VM_SEARCH_KEYWORD_FOR', $this->keyword); ?></h3><?php
	}*/
	$j = 'jQuery(document).ready(function() {

jQuery(".changeSendForm")
	.off("change",Virtuemart.sendCurrForm)
    .on("change",Virtuemart.sendCurrForm);
})';

	vmJsApi::addJScript('sendFormChange',$j);
} ?>
<div class="popup" id="productorder">
<div class="popup-inner">
<div class="cssend-loader"><div class="loader"></div></div>
<a href="#" class="close-icon"></a>
        <div class="popup-header">
            <?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_HEADER');?>
        </div>
<div class="popup-message"></div>
<div class="contact-form" id="order-popup-893">
    <div class="callbackform-inner">

    <form class="vsorder vsform-893" method="post">
    <div class="popup-msg"></div>
       <div class="input-wrap"><span class="input-icon-1"></span><input type="text" data-title="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_NAME');?>" name="field1" placeholder="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_NAME');?>" class="require" /></div>
       <div class="input-wrap"><span class="input-icon-2"></span><input type="text" data-title="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_LASTNAME');?>" name="field2" placeholder="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_LASTNAME');?>" /></div>
       <div class="input-wrap"><span class="input-icon-3"></span><input type="text" data-title="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_PHONE');?>" name="field3" placeholder="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_PHONE');?>"  /></div>
       <div class="input-wrap"><span class="input-icon-4"></span><input type="email" data-title="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_EMAIL');?>" name="field4" placeholder="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_EMAIL');?>" class="require" /></div>
       <div class="input-wrap">
       <select name="field5" data-title="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_REGION');?>">
                     <option value="0" selected="">Выберите регион</option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN1');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN1');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN2');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN2');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN3');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN3');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN4');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN4');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN5');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN5');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN6');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN6');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN7');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN7');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN8');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN8');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN9');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN9');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN10');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN10');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN11');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN11');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN12');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN12');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN13');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN13');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN14');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN14');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN15');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN15');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN16');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN16');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN17');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN17');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN18');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN18');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN19');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN19');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN20');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN20');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN21');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN21');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN22');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN22');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN23');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN23');?></option>
                                          <option value="<?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN24');?>"><?php echo vmText::_ ('COM_VIRTUEMART_REGION_VIN24');?></option>
                                          </select>
        </div>
        <div class="input-wrap"><span class="input-icon-3"></span><input type="text" data-title="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_CITY');?>" name="field6" placeholder="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_CITY');?>"  /></div>
        <div class="input-wrap"><span class="input-icon-3"></span><textarea data-title="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_COMMENT');?>" name="field7" rows="1" cols="1" placeholder="<?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_COMMENT');?>"></textarea></div>
		<a href="#" class="vssend-button" id="vsform-893"><i></i><span><?php echo vmText::_ ('COM_VIRTUEMART_P_ORDER_BUTTON_NAME');?></span></a>
        <div class="clr"></div>
        <input type="hidden" name="field8" value="" />
        <input type="hidden" name="id" value="893">
        <input type="hidden" name="typest" value="">
        <input type="hidden" name="popup" value="1">
        <input type="hidden" name="version" value="1">
       
	</form>
    </div>
</div>
</div>
</div>

<h1><span><?php echo vmText::_($this->category->category_name); ?></span></h1>

<?php //echo '<pre>'; var_dump($this->category); echo '</pre>';?>

<div class="category_description">
	<?php echo $this->category->category_description; ?>
</div>

<?php 
$fields = VmConfig::get ('browse_orderby_fields');
if (count ($fields) > 1) {
			$orderByLink = '<select>';
			foreach ($fields as $field) {
				if ($field != $orderby) {

					$dotps = strrpos ($field, '.');
					if ($dotps !== FALSE) {
						$prefix = substr ($field, 0, $dotps + 1);
						$fieldWithoutPrefix = substr ($field, $dotps + 1);
					}
					else {
						$prefix = '';
						$fieldWithoutPrefix = $field;
					}

					$text = vmText::_ ('COM_VIRTUEMART_' . strtoupper (str_replace(array(',',' '),array('_',''),$fieldWithoutPrefix)));

					$field = explode('.',$field);
					if(isset($field[1])){
						$field = $field[1];
					} else {
						$field = $field[0];
					}
					$link = JRoute::_ ($fieldLink . $manufacturerTxt . '&orderby=' . $field,FALSE);

					$orderByLink .= '<option value="' . $link . '">' . $text . '</option>';
				}
			}
			$orderByLink .= '</select>';
		}

?>

<?php // Show child categories



if(!empty($this->orderByList)) { ?>
<form action="#" class="jClever">
<div class="orderby-displaynumber">
	<div class="product-sort">
		<?php echo $orderByLink; ?>
	</div>
	
	<div class="display-number"><?php echo $this->vmPagination->getLimitBox ($this->category->limit_list_step); ?></div>
    
    <div class="view-type">
        <a href="#" class="vm-grid active"></a>
        <a href="#" class="vm-list"></a>
    </div>

	<div class="clr"></div>
</div>
</form>
<?php } ?>



	<?php
	if (!empty($this->products)) {
		//revert of the fallback in the view.html.php, will be removed vm3.2
		if($this->fallback){
			$p = $this->products;
			$this->products = array();
			$this->products[0] = $p;
			vmdebug('Refallback');
		}
    echo '<div class="product-list">';
	echo shopFunctionsF::renderVmSubLayout($this->productsLayout,array('products'=>$this->products,'currency'=>$this->currency,'products_per_row'=>$this->perRow,'showRating'=>$this->showRating));
    echo '</div>';
	if(!empty($this->orderByList)) { ?>
		<div class="vm-pagination vm-pagination-bottom"><?php echo $this->vmPagination->getPagesLinks (); ?></div>
        <div class="clr"></div>
	<?php }
} elseif (!empty($this->keyword)) {
	echo vmText::_ ('COM_VIRTUEMART_NO_RESULT') . ($this->keyword ? ' : (' . $this->keyword . ')' : '');
}
?>
</div>

<?php } ?>
</div>

<?php
if(VmConfig::get ('jdynupdate', TRUE)){
	$j = "Virtuemart.container = jQuery('.category-view');
	Virtuemart.containerSelector = '.category-view';";

	//vmJsApi::addJScript('ajaxContent',$j);
}
?>
<!-- end browse-view -->