<?php
/**
*
* Layout for the shopping cart, look in mailshopper for more details
*
* @package	VirtueMart
* @subpackage Cart
* @author Max Milbers
*
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
*
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
/* TODO Chnage the footer place in helper or assets ???*/
// END if footer ?>
<table width="100%" style="margin-top:50px;">
    <tr>
         <td colspan="3" style="margin-bottom:15px;"><?php echo vmText::sprintf ('COM_VIRTUEMART_MAIL_SHOPPER_SIGNATURE'); ?></td>
    </tr>
     <tr>
         <td width="30%"><img src="<?php echo JURI::base()?>/images/signature.jpg" style="width:150px;" /></td>
         <td width="40%"></td>
         <td width="30%" align="right"><img src="<?php echo JURI::base()?>/images/stamp.jpg" style="width:150px;" /></td>
    </tr>
</table>