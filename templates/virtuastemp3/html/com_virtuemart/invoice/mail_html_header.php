<?php
/**
 *
 * Define here the Header for order mail success !
 *
 * @package    VirtueMart
 * @subpackage Cart
 * @author Kohl Patrick
 * @author Valérie Isaksen
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 *
 */
// Check to ensure this file is included in Joomla!
defined ('_JEXEC') or die('Restricted access');


?>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="html-email">
	<?php if ($this->vendor->vendor_letter_header>0) { ?>
	<tr>
		<?php if ($this->vendor->vendor_letter_header_image>0) { ?>
			<td class="vmdoc-header-image" width="30%"><a href="<?php echo JURI::base()?>"><img src="<?php echo JURI::root () . 'images/stories/virtuemart/vendor/logo.png' ?>" /></a></td>
			<td colspan=1 class="vmdoc-header-vendor" width="70%">
                 <div style="font-size:36px"><?php echo vmText::sprintf ('COM_VIRTUEMART_MAIL_COMPANY_NAME');?></div>
                 <div style="font-size:16px;"><?php echo vmText::sprintf ('COM_VIRTUEMART_MAIL_COMPANY_ADDRESS');?></div>
                 <div style="margin-top:15px; margin-bottom:15px; font-size:16px; line-height:22px;"><?php echo vmText::sprintf ('COM_VIRTUEMART_MAIL_COMPANY_BANK');?></div>
		<?php } else { // no image ?>
			<td colspan=2 width="100%" class="vmdoc-header-vendor">
		<?php } ?>
			<div id="vmdoc-header" class="vmdoc-header" style="font-size: <?php echo $this->vendor->vendor_letter_header_font_size; ?>pt;">
			<?php echo VirtuemartViewInvoice::replaceVendorFields ($this->vendor->vendor_letter_header_html, $this->vendor); ?>
			</div>
		</td>
	</tr>
    <tr><td colspan=2 width="100%" style="height:45px; background: url(<?php echo JURI::base()?>/images/mail_header.png) repeat-x;"></td></tr>
	
			
	<?php } // END if header ?>
	
</table>
