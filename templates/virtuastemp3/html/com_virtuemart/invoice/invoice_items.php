<?php
/**
*
* Order items view
*
* @package	VirtueMart
* @subpackage Orders
* @author Max Milbers, Valerie Isaksen
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* @version $Id: details_items.php 5432 2012-02-14 02:20:35Z Milbo $
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

$colspan=8;

if ($this->doctype != 'invoice') {
    $colspan -= 4;
} elseif ( ! VmConfig::get('show_tax')) {
    $colspan -= 1;
}

$handled = array();
$discountsBill = false;
$taxBill = false;
$vats = 0;
foreach($this->orderDetails['calc_rules'] as $rule){
	if(isset($sumRules[$rule->virtuemart_calc_id])){	// or $rule->calc_kind=='payment' or $rule->calc_kind=='shipment'){
		continue;
	}
	$handled[$rule->virtuemart_calc_id] = true;
	$r = new stdClass();
	$r->calc_result = $rule->calc_result;
	$r->calc_amount = $rule->calc_amount;
	$r->calc_rule_name = $rule->calc_rule_name;
	$r->calc_kind = $rule->calc_kind;
	$r->calc_value = $rule->calc_value;

	if($rule->calc_kind == 'DBTaxRulesBill' or $rule->calc_kind == 'DATaxRulesBill'){
		$discountsBill[$rule->virtuemart_calc_id] = $r;
	}
	if($rule->calc_kind == 'taxRulesBill' or $rule->calc_kind == 'VatTax' or $rule->calc_kind=='payment' or $rule->calc_kind=='shipment'){
		//vmdebug('method rule',$rule);
		$r->label = shopFunctionsF::getTaxNameWithValue($rule->calc_rule_name,$rule->calc_value);
		if(isset($taxBill[$rule->virtuemart_calc_id])){
			$taxBill[$rule->virtuemart_calc_id]->calc_amount += $r->calc_amount;
		} else {
			$taxBill[$rule->virtuemart_calc_id] = $r;
		}

	}

}


 ?>

<table class="html-email" width="100%" style="margin-top:30px;" cellspacing="0" cellpadding="0" border="0">
	<tr align="left" class="sectiontableheader">
        <td width="10%"></td>
		<td align="left" colspan="2" width="35%" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000;"><strong><?php echo vmText::_('COM_VIRTUEMART_PRODUCT_NAME_TITLE') ?></strong></td>
		<?php if ($this->doctype == 'invoice') { ?>
		<td align="center" width="15%" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000;"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_PRICE') ?></strong></td>
		<?php } ?>
		<td align="center" width="15%" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000;"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_QTY') ?></strong></td>
		<?php if ($this->doctype == 'invoice') { ?>
		<td align="center" width="15%" style="border-top:1px solid #ffa000;"><strong><?php echo vmText::_('COM_VIRTUEMART_ORDER_PRINT_TOTAL') ?></strong></td>
		<?php } ?>
        <td width="10%"></td>
	</tr>

<?php
	$menuItemID = shopFunctionsF::getMenuItemId($this->orderDetails['details']['BT']->order_language);
	if(!class_exists('VirtueMartModelCustomfields'))require(VMPATH_ADMIN.DS.'models'.DS.'customfields.php');
	VirtueMartModelCustomfields::$useAbsUrls = ($this->isMail or $this->isPdf);
	foreach($this->orderDetails['items'] as $item) {
		$qtt = $item->product_quantity ;
		$product_link = JURI::root().'index.php?option=com_virtuemart&view=productdetails&virtuemart_category_id=' . $item->virtuemart_category_id .
			'&virtuemart_product_id=' . $item->virtuemart_product_id . '&Itemid=' . $menuItemID;

		?>
		<tr valign="top">
            <td width="10%"></td>
			<td align="left" colspan="2" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000;">
				<div float="right" ><a href="<?php echo $product_link; ?>"><?php echo $item->order_item_name; ?></a></div>
				<?php
					$product_attribute = VirtueMartModelCustomfields::CustomsFieldOrderDisplay($item,'FE');
					echo $product_attribute;
				?>
			</td>
		<?php if ($this->doctype == 'invoice') { ?>
			<td align="center"   class="priceCol" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000;">
				<?php
				$item->product_discountedPriceWithoutTax = (float) $item->product_discountedPriceWithoutTax;
				if (!empty($item->product_priceWithoutTax) && $item->product_discountedPriceWithoutTax != $item->product_priceWithoutTax) {
					echo '<span class="line-through">'.$this->currency->priceDisplay($item->product_item_price, $this->user_currency_id) .'</span><br />';
					echo '<span >'.$this->currency->priceDisplay($item->product_discountedPriceWithoutTax, $this->user_currency_id) .'</span><br />';
				} else {
					echo '<span >'.$this->currency->priceDisplay($item->product_item_price, $this->user_currency_id) .'</span><br />';
				}
				?>
			</td>
		<?php } ?>
			<td align="center" style="border-top:1px solid #ffa000; border-right:1px solid #ffa000;">
				<?php echo $qtt; ?>
			</td>
		<?php if ($this->doctype == 'invoice') { ?>
			
			<td align="center"  class="priceCol" style="border-top:1px solid #ffa000;">
				<?php
				$item->product_basePriceWithTax = (float) $item->product_basePriceWithTax;
				$class = '';
				if(!empty($item->product_basePriceWithTax) && $item->product_basePriceWithTax != $item->product_final_price ) {
					echo '<span class="line-through" >'.$this->currency->priceDisplay($item->product_basePriceWithTax,$this->user_currency_id,$qtt) .'</span><br />' ;
				}
				elseif (empty($item->product_basePriceWithTax) && $item->product_item_price != $item->product_final_price) {
					echo '<span class="line-through">' . $this->currency->priceDisplay($item->product_item_price,$this->user_currency_id,$qtt) . '</span><br />';
				}

				echo $this->currency->priceDisplay(  $item->product_subtotal_with_tax ,$this->user_currency_id); //No quantity or you must use product_final_price ?>
			</td>
		<?php } ?>
             <td width="10%"></td>
		</tr>
        
    

<?php
	}
?>
</table>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
<tr>
        <td width="10%"></td>
		<td align="left" colspan="2" width="35%" style="border-top:1px solid #ffa000; "></td>
		<?php if ($this->doctype == 'invoice') { ?>
		<td align="center" width="15%" style="border-top:1px solid #ffa000;"></td>
		<?php } ?>
		<td align="center" width="15%" style="border-top:1px solid #ffa000; "></td>
		<?php if ($this->doctype == 'invoice') { ?>
		<td align="center" width="15%" style="border-top:1px solid #ffa000;"></td>
		<?php } ?>
        <td width="10%"></td>
	</tr>
</table>
<table class="html-email" width="100%" style="margin-top:40px ;" cellspacing="0" cellpadding="0" border="0">
 <tr>
    <td align="right"><p>
				<?php echo vmText::sprintf('COM_VIRTUEMART_MAIL_SHOPPER_TOTAL_ORDER',$this->currency->priceDisplay($this->orderDetails['details']['BT']->order_total,$this->user_currency_id) ); ?></p></td>
  </tr>
</table>
