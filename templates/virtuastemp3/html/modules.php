<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.beez3
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * beezDivision chrome.
 *
 * @since   3.0
 */
function modChrome_xtml($module, &$params, &$attribs)
{
	if ($module->content)
	{
		echo "<div class=\"moduletable" . htmlspecialchars($params->get('moduleclass_sfx')) . "\">";
		if ($module->showtitle)
		{
			echo "<h3><span>" . $module->title . "</span></h3>";
		}
		echo $module->content;
		echo "</div>";
	}
}

function modChrome_part($module, &$params, &$attribs)
{
	if ($module->content)
	{
		echo "<div class=\"moduletable" . htmlspecialchars($params->get('moduleclass_sfx')) . "\">";
		if ($module->showtitle)
		{
			echo "<h3><i>" . $module->title . "</i><span class=\"p-span\"></span></h3>";
		}
		echo $module->content;
		echo "</div>";
	}
}

function modChrome_footer($module, &$params, &$attribs)
{
	if ($module->content)
	{
		echo "<div class=\"moduletable" . htmlspecialchars($params->get('moduleclass_sfx')) . "\">";
		if ($module->showtitle)
		{
			echo "<h3><i>" . $module->title . "</i><span class=\"p-span\"></span></h3>";
		}
		echo $module->content;
		echo "</div>";
	}
}

