<?php
/**
 * @version $Id: default.php 37 2016-10-28 08:35:10Z szymon $
 * @package DJ-ImageSlider
 * @subpackage DJ-ImageSlider Component
 * @copyright Copyright (C) 2012 DJ-Extensions.com, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Szymon Woronowski - szymon.woronowski@design-joomla.eu
 *
 *
 * DJ-ImageSlider is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-ImageSlider is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-ImageSlider. If not, see <http://www.gnu.org/licenses/>.
 *
 */

// no direct access
defined('_JEXEC') or die ('Restricted access'); 

$wcag = $params->get('wcag', 1) ? ' tabindex="0"' : ''; ?>
    <?php //var_dump($height)?>
	<div id="slider-container<?php echo $mid;?>" class="slider-container">
	    <div class="ws_images">
            <ul>
                <?php
				    $counter = 0; 
				    foreach ($slides as $slide) { 
				    ?>
                    <li>
          			<?php $rel = (!empty($slide->rel) ? 'rel="'.$slide->rel.'"':''); ?>
                    <?php if($slide->image) { 
          					$action = $params->get('link_image',1);
          					if($action > 1) {
								$desc = $params->get('show_desc') ? 'title="'.(!empty($slide->title) ? htmlspecialchars($slide->title.' ') : '').(!empty($slide->description) ? htmlspecialchars('<small>'.strip_tags($slide->description,"<p><a><b><strong><em><i><u>").'</small>') : '').'"':'';
	          					if($jquery) {
	          						$attr = 'class="image-link" data-'.$desc;
	          						
	          					} else {
	          						$attr = 'rel="lightbox-slider'.$mid.'" '.$desc;
	          					}
							} else {
								$attr = $rel;
							}
          					?>
	            			<?php if (($slide->link && $action==1) || $action>1) { ?>
								<a <?php echo $attr; ?> href="<?php echo ($action>1 ? $slide->image : $slide->link); ?>" target="<?php echo $slide->target; ?>">
							<?php } ?>
								<img src="<?php echo $slide->image; ?>" alt="<?php echo $slide->alt; ?>" <?php echo (!empty($slide->img_title) ? ' title="'.$slide->img_title.'"':''); ?> id="wows1_<?php echo $counter;?>" />
							<?php if (($slide->link && $action==1) || $action>1) { ?>
								</a>
							<?php } ?>
						<?php } ?>
                        <div class="slide-desc" id="slide_desc_<?php echo $counter;?>">
                            <?php echo $slide->description; ?>
                        </div>
		          </li>    
                <?php $counter++; }?>
                
	        </ul>
        </div>
	    <div class="ws_shadow"></div>
	</div>	
	<script src="/modules/mod_djimageslider/assets/js/slider.js" type="text/javascript" defer="defer"></script>
<script type="text/javascript">
jQuery(function() {
    jQuery.noConflict();
jQuery("#slider-container<?php echo $mid;?>").wowSlider({effect:"basic,basic_linear,fade",prev:"",next:"",duration:<?php echo $params->get('duration', 3000)?>,delay:<?php echo $params->get('delay', 3000)?>,width:'auto',height:<?php echo $height;?>,autoPlay:true,autoPlayVideo:false,playPause:false,stopOnHover:false,loop:false,bullets:1,caption:true,captionEffect:"parallax",controls:true,controlsThumb:false,responsive:2,fullScreen:true,gestures:2,onBeforeStep:0,images:0});

});
</script>