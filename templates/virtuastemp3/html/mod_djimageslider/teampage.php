<?php
/**
 * @version $Id: default.php 37 2016-10-28 08:35:10Z szymon $
 * @package DJ-ImageSlider
 * @subpackage DJ-ImageSlider Component
 * @copyright Copyright (C) 2012 DJ-Extensions.com, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Szymon Woronowski - szymon.woronowski@design-joomla.eu
 *
 *
 * DJ-ImageSlider is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-ImageSlider is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-ImageSlider. If not, see <http://www.gnu.org/licenses/>.
 *
 */

// no direct access
defined('_JEXEC') or die ('Restricted access'); 

 ?>
    <?php //var_dump($height)?>
	<div  class="team-list">
    
                <?php
				    foreach ($slides as $slide) { 
				    ?>
                    <div class="team-list-block">
                       <div class="tlb-image">
                            <img src="<?php echo $slide->image; ?>" alt="<?php echo $slide->alt; ?>" <?php echo (!empty($slide->img_title) ? ' title="'.$slide->img_title.'"':''); ?>  />
                       </div>
                       <div class="tlb-info">
                           <div class="tlb-name"><?php echo $slide->title; ?></div>
                           <div class="tlb-desc"> <?php echo strip_tags($slide->description); ?></div>
                       </div>
                    </div>
                    
          			
                <?php 
				
				}?>
                
	       <div class="clr"></div>
	</div>	
