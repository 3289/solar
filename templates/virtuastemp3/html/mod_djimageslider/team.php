<?php
/**
 * @version $Id: default.php 37 2016-10-28 08:35:10Z szymon $
 * @package DJ-ImageSlider
 * @subpackage DJ-ImageSlider Component
 * @copyright Copyright (C) 2012 DJ-Extensions.com, All rights reserved.
 * @license http://www.gnu.org/licenses GNU/GPL
 * @author url: http://dj-extensions.com
 * @author email contact@dj-extensions.com
 * @developer Szymon Woronowski - szymon.woronowski@design-joomla.eu
 *
 *
 * DJ-ImageSlider is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * DJ-ImageSlider is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DJ-ImageSlider. If not, see <http://www.gnu.org/licenses/>.
 *
 */

// no direct access
defined('_JEXEC') or die ('Restricted access'); 

$wcag = $params->get('wcag', 1) ? ' tabindex="0"' : ''; ?>
    <?php //var_dump($height)?>
	<div id="services-carusel<?php echo $mid;?>" class="team-carusel">
    
                <?php
				    $counter = 0; 
					$cicount = 1; 
				    foreach ($slides as $slide) { 
				    ?>
                    <div class="ci-block ci-<?php echo $cicount;?>">
                    <?php if($slide->facebook || $slide->vkontakte || $slide->twitter || $slide->odnoklassniki || $slide->email || $slide->phone){?>
                        <div class="ci-socblock">
                             <ul>
                                  <?php if($slide->facebook){?>
                                      <li><a href="<?php echo $slide->facebook;?>" class="ci-fb" title="favebook" target="_blank"><i></i></a></li>   
                                  <?php }?>  
                                  <?php if($slide->vkontakte){?>
                                      <li><a href="<?php echo $slide->vkontakte;?>" class="ci-vk" title="вконтакте" target="_blank"><i></i></a></li>   
                                  <?php }?>
                                   <?php if($slide->twitter){?>
                                      <li><a href="<?php echo $slide->twitter?>" class="ci-twitter" title="twitter" target="_blank"><i></i></a></li>   
                                  <?php }?>
                                   <?php if($slide->odnoklassniki){?>
                                      <li><a href="<?php echo $slide->odnoklassniki;?>" class="ci-ok" title="Одноклассники" target="_blank"><i></i></a></li>   
                                  <?php }?> 
                                  <?php if($slide->email){?>
                                      <li><a href="mailto:<?php echo $slide->email;?>" class="ci-email" target="_blank"><i></i></a></li>
                                  <?php }?> 
                                  <?php if($slide->phone){?>
                                      <li><a href="tel:<?php echo $slide->phone;?>" class="ci-phone"  target="_blank"><i></i></a></li>
                                  <?php }?>
                             </ul>
                        </div>
                    <?php }?>
                    
          			<?php $rel = (!empty($slide->rel) ? 'rel="'.$slide->rel.'"':''); ?>
                    <?php if($slide->image) { 
          					$action = $params->get('link_image',1);
          					if($action > 1) {
								$desc = $params->get('show_desc') ? 'title="'.(!empty($slide->title) ? htmlspecialchars($slide->title.' ') : '').(!empty($slide->description) ? htmlspecialchars('<small>'.strip_tags($slide->description,"<p><a><b><strong><em><i><u>").'</small>') : '').'"':'';
	          					if($jquery) {
	          						$attr = 'class="image-link" data-'.$desc;
	          						
	          					} else {
	          						$attr = 'rel="lightbox-slider'.$mid.'" '.$desc;
	          					}
							} else {
								$attr = $rel;
							}
          					?>
	            			<?php if (($slide->link && $action==1) || $action>1) { ?>
								<a <?php echo $attr; ?> href="<?php echo ($action>1 ? $slide->image : $slide->link); ?>" target="<?php echo $slide->target; ?>">
							<?php } ?>
								<span class="carusel-img"><img src="<?php echo $slide->image; ?>" alt="<?php echo $slide->alt; ?>" <?php echo (!empty($slide->img_title) ? ' title="'.$slide->img_title.'"':''); ?>  /></span>
							<span class="ci-block-info">
							<?php if($params->get('show_title')){?>
							  <span class="carusel-title">
										  <?php echo $slide->title; ?>
                                      </span>
                             <?php }?>         
                                      <?php if($slide->description){?>
                                          <span class="carusel-desc">
                                              <?php echo strip_tags($slide->description); ?>
                                          </span>
                                      <?php }?>
							<?php if (($slide->link && $action==1) || $action>1) { ?>
                                 </span>   
								</a>
							<?php } ?>
						<?php } ?>
                       
                        
		          </div>    
                <?php 
				if($cicount % 5 == 0){
					$cicount = 0;
				}
				$cicount++;
				$countrt++; 
				
				}?>
                
	       
	</div>	
<script type="text/javascript">
jQuery(function() {
    jQuery('#services-carusel<?php echo $mid;?>').slick({
	  infinite: true,
	  slidesToShow: <?php echo $params->get('visible_images',5) ?>,
	  slidesToScroll: 1
	});
});
</script>