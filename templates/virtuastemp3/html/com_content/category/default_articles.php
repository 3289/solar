<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_content
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.framework');

// Create some shortcuts.
$params		= &$this->item->params;
$n			= count($this->items);
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
?>

<?php if (empty($this->items)) : ?>

	<?php if ($this->params->get('show_no_articles', 1)) : ?>
	<p><?php echo JText::_('COM_CONTENT_NO_ARTICLES'); ?></p>
	<?php endif; ?>

<?php else : ?>

<form action="<?php echo htmlspecialchars(JFactory::getURI()->toString()); ?>" method="post" name="adminForm" id="adminForm">
	<?php if ($this->params->get('show_headings') || $this->params->get('filter_field') != 'hide' || $this->params->get('show_pagination_limit')) :?>

		
	<!-- @TODO add hidden inputs -->
		<input type="hidden" name="filter_order" value="" />
		<input type="hidden" name="filter_order_Dir" value="" />
		<input type="hidden" name="limitstart" value="" />

	<?php endif; ?>
    <?php $itemLeft = round(count($this->items) / 2) ;
	    $counter = 1;
	?>
	<?php foreach ($this->items as $i => $article) : ?>
    
        <?php
                $date = explode('.',JHtml::_('date', $article->displayDate, $this->escape(
						$this->params->get('date_format', JText::_('DATE_FORMAT_LC1')))));
				switch($date[1]){
				   case '1':$month = JText::_('JDATE_JAN');break;
				   case '2':$month = JText::_('JDATE_FEB');break;
				   case '3':$month = JText::_('JDATE_MAR');break;	
				   case '4':$month = JText::_('JDATE_APR');break;	
				   case '5':$month = JText::_('JDATE_MAY');break;	
				   case '6':$month = JText::_('JDATE_JUN');break;	
				   case '7':$month = JText::_('JDATE_JUL');break;	
				   case '8':$month = JText::_('JDATE_AUG');break;	
				   case '9':$month = JText::_('JDATE_SEP');break;	
				   case '10':$month = JText::_('JDATE_OCT');break;	
				   case '11':$month = JText::_('JDATE_NOV');break;	
				   case '12':$month = JText::_('JDATE_DEC');break;			
			    }
				
				if($counter == 1){
				   echo '<div class="news-left">';	
				}
			?>
            
  
	    <div class="news-block">
			<h2><a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($article->slug, $article->catid)); ?>">
					    <?php 
						if($article->previewtitle){
						    echo $this->escape(strip_tags($article->previewtitle)); 
						}else{
							echo $this->escape(strip_tags($article->title)); 
						}?>
                            
                            
                </a></h2>
			<div class="news-info">
			    <div class="news-date">
                   <i></i>
				   <?php echo '<span>' . JText::_('JDATE_DATETEXT'). ':</span> ' . $date[0].' '.$month.' '.$date[2]; ?>
				</div>
				<div class="news-views">
                   <i></i>
				   <?php echo '<span>' .JText::_('COM_CONTENT_VIEWS') . ':</span> ' .$article->hits; ?>
				</div>
				<div class="clr"></div>
			</div>	
            
            <div class="news-block-img">
                <?php 
					preg_match_all('/<img(?:\\s[^<>]*?)?\\bsrc\\s*=\\s*(?|"([^"]*)"|\'([^\']*)\'|([^<>\'"\\s]*))[^<>]*>/i', $article->introtext, $result);
                    if($result[1][0]){
				?>
                <img src="<?php echo $result[1][0]; ?>" alt="<?php echo $this->escape($article->title); ?>" />
                <?php }?>
            </div>
            			
			<div class="news-text">
			      <?php  if (mb_strlen(strip_tags($article->introtext),"utf-8") > 250){ 
						   echo  mb_substr(strip_tags($article->introtext),0,250, "utf-8").' ...';
						}else{
						   echo strip_tags($article->introtext);
						}
						?>

			</div>
			<div class="news-readmore">
			    <a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($article->slug, $article->catid)); ?>">
							<i></i>Подробнее</a>
                <div class="clr"></div>            
			</div>
		</div>
        <?php if($counter == $itemLeft){?>
            </div><div class="news-right">
        <?php }?>
        <?php if($counter == count($this->items)){?>
            </div>
        <?php }?>
	<?php $counter ++; endforeach; ?>
<?php endif; ?>
          <div class="clr"></div>
<?php // Code to add a link to submit an article. ?>
<?php if ($this->category->getParams()->get('access-create')) : ?>
	<?php echo JHtml::_('icon.create', $this->category, $this->category->params); ?>
<?php  endif; ?>

<?php // Add pagination links ?>
<?php if (!empty($this->items)) : ?>
	<?php if (($this->params->def('show_pagination', 2) == 1  || ($this->params->get('show_pagination') == 2)) && ($this->pagination->get('pages.total') > 1)) : ?>
	<div class="pagination">

		<?php echo $this->pagination->getPagesLinks(); ?>
	</div>
	<?php endif; ?>
</form>
<?php  endif; ?>
