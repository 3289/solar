<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_content
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.framework');

// Create some shortcuts.
$params		= &$this->item->params;
$n			= count($this->items);
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
?>


<?php if (empty($this->items)) : ?>

	<?php if ($this->params->get('show_no_articles', 1)) : ?>
	<p><?php echo JText::_('COM_CONTENT_NO_ARTICLES'); ?></p>
	<?php endif; ?>

<?php else : ?>



    
      
            
	    <div class="news-block">
			<h2><a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid)); ?>">
							<?php echo $this->escape($this->item->title); ?></a></h2>
		            
            <div class="news-block-img">
               <?php echo JLayoutHelper::render('joomla.content.intro_image', $this->item); ?>
            </div>
            			
			<div class="news-text">
			      <?php  if (mb_strlen(strip_tags($this->item->introtext),"utf-8") > 500){ 
						   echo  mb_substr(strip_tags($this->item->introtext),0,500, "utf-8").' ...';
						}else{
						   echo strip_tags($this->item->introtext);
						}
						?>

			</div>
            <div class="clr"></div>
			<div class="news-readmore">
			    <a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid)); ?>">
							<i></i><p><?php echo JText::_('COM_CONTENT_READ_MORE1'); ?></p></a>
                <div class="clr"></div>            
			</div>
		</div>
        
        

<?php endif; ?>
          <div class="clr"></div>
<?php // Code to add a link to submit an article. ?>
<?php if ($this->category->getParams()->get('access-create')) : ?>
	<?php echo JHtml::_('icon.create', $this->category, $this->category->params); ?>
<?php  endif; ?>
