<?php // no direct access
defined('_JEXEC') or die('Restricted access');

//dump ($cart,'mod cart');
// Ajax is displayed in vm_cart_products
// ALL THE DISPLAY IS Done by Ajax using "hiddencontainer" ?>

<!-- Virtuemart 2 Ajax Card -->
<div class="vmCartModule <?php echo $params->get('moduleclass_sfx'); ?>" id="vmCartModule" data-link="/cart/cart">
<?php
if ($show_product_list) {
	?>
	<div class="hiddencontainer" style=" display: none; ">
		<div class="vmcontainer">
			<div class="product_row">
                <div class="product-cart-image"></div>
                <div class="product-cart-info">
                     <?php if ($show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
                              <div class="subtotal_with_tax"></div>
                           <?php } ?>
                    <div class="quant"> &nbsp;x&nbsp;<span class="quantity"></span></div>
                </div>
                <div class="clr"></div>
			    <span class="product_name"><?php echo  $product['product_name'] ?></span>
			</div>
            <div class="total-bottom">
                <div class="total">
                </div>
                <div class="show_cart">
                </div>
                <div class="clr"></div>
            </div>
            
		</div>
      
	</div>
	<div class="vm_cart_products">
        <div class="vm_cart_products-inner">
		<div class="vmcontainer">

		<?php
           
			foreach ($data->products as $product){
				 //echo '<pre>'; var_dump($product); echo '</pre>';
				?><div class="product_row">
                      <div class="product-cart-image">
                          <img src="<?php echo $product['image'] ?>" alt="<?php echo  $product['product_title'] ?>"  />
                      </div>
                      <div class="product-cart-info">
                           <?php if ($show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
                              <div class="subtotal_with_tax"><?php echo $product['subtotal_with_tax'] ?></div>
                           <?php } ?>
                           
                           <div class="quant">&nbsp;x&nbsp;<span class="quantity"><?php echo  $product['quantity'] ?></span></div>
                      </div> 
                      <div class="clr"></div>
					  <span class="product_name"><?php echo  $product['product_name'] ?></span>

			</div>
		<?php }
		?>
        <div class="total-bottom">
            <div class="total">
                <?php if ($data->totalProduct and $show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
                <?php echo $data->billTotal; ?>
                <?php } ?>
            </div>
            <div class="show_cart">
                <?php if ($data->totalProduct) echo  $data->cart_show; ?>
            </div>
            <div class="clr"></div>
        </div>
		</div>
        </div>
	</div>
<?php } ?>
    <div class="total_products"><?php echo  $data->totalProductTxt ?></div>
	<div class="total" style="display:none;">
		<?php if ($data->totalProduct and $show_price and $currencyDisplay->_priceConfig['salesPrice'][0]) { ?>
		<?php echo $data->billTotal; ?>
		<?php } ?>
	</div>



<div style="clear:both;"></div>
	<div class="payments-signin-button" ></div>
<noscript>
<?php echo vmText::_('MOD_VIRTUEMART_CART_AJAX_CART_PLZ_JAVASCRIPT') ?>
</noscript>
</div>

