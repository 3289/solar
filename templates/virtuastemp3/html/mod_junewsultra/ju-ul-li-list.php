<?php
/**
 * JUNewsUltra Pro
 *
 * @version 	6.x
 * @package 	UNewsUltra Pro
 * @author 		Denys D. Nosov (denys@joomla-ua.org)
 * @copyright 	(C) 2007-2016 by Denys D. Nosov (http://joomla-ua.org)
 * @license 	GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 *
 **/

/******************* PARAMS (update 28.04.2016) ************
*
* $params->get('moduleclass_sfx') - module class suffix
*
* $item->link           - article link for [href="..."] attribute
* $item->title          - title
* $item->title_alt      - for attribute title or alt
*
* $item->cattitle       - category title
* $item->catlink		- category link for [href="..."] attribute
*
* $item->image          - display image thumb
* $item->imagelink      - image thumb link for [src="..."] attribute
* $item->imagesource    - raw image source (original image)
*
* $item->sourcetext		- display raw intro and fulltext
*
* $item->introtext      - display introtex
* $item->fulltext       - display fulltext
*
* $item->author         - display author or created by alias
*
* $item->sqldate		- raw date [display format: 0000-00-00 00:00:00]
* $item->date           - display date & time with date format
* $item->df_d           - display day from date
* $item->df_m           - display mounth from date
* $item->df_y           - display year from date
*
* $item->hits           - display hits
*
* $item->rating         - display rating with stars
*
* $item->comments		- display comments couner
* $item->commentslink   - comment link for [href="..."] attribute
* $item->commentstext   - display comments text
* $item->commentscount  - comments couner (alias)
*
* $item->readmore       - display 'Read more...' or other text
* $item->rmtext         - display 'Read more...' or other text
*
************************************************************/

defined('_JEXEC') or die('Restricted access');
?>
<div id="news<?php echo $module->id;?>" class="junewsultra<?php echo $params->get('moduleclass_sfx'); ?>">
<?php foreach ($list as $item) :  ?>
	<div class="jn-list">
         <?php if($params->get('pik')): ?>
             <div class="jn-img">
        		<?php echo $item->image; ?><br>
             </div>
             <div class="jn-title">
                <?php if($params->get('show_title')): ?>
        		    <h4><a href="<?php echo $item->link; ?>" title="<?php echo $item->title_alt; ?>"><?php echo $item->title; ?></a></h4>
                <?php endif; ?>
             </div>
             <div class="jn-info">
                 <?php if($params->get('show_date')): ?>
                     <div class="jn-info-date">
                         <i></i>
                         <?php echo $item->df_d;?>
                        <?php
                    switch($item->df_m){
					    case 1:$month='Янв';break;
						case 2:$month='Фев';break;	
						case 3:$month='Мрт';break;	
						case 4:$month='Апр';break;	
						case 5:$month='Май';break;	
						case 6:$month='Июн';break;	
						case 7:$month='Июл';break;	
						case 8:$month='Авг';break;	
						case 9:$month='Сен';break;	
						case 10:$month='Окт';break;	
						case 11:$month='Нбр';break;	
						case 12:$month='Дек';break;		
					}
					echo $month;
				?>
                     </div>
                 <?php endif; ?>
                 <?php if($params->def('juauthor')): ?>
                     <div class="jn-info-author">
                         <i></i>
                         <?php echo $item->author; ?>
                     </div>
                 <?php endif; ?>
                 
             </div>
             <div class="jn-text">
                      <?php echo $item->introtext;?>
                 </div>
         <?php endif; ?>
	</div>
<?php endforeach; ?>
</div>
<script type="text/javascript">
jQuery(function() {
    jQuery('#news<?php echo $module->id;?>').slick({
	  infinite: true,
	  slidesToShow: 4,
	  slidesToScroll: 1
	});
});
</script>