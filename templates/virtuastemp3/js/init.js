!function(a){function d(b){var c=b||window.event,d=[].slice.call(arguments,1),e=0,g=0,h=0;return b=a.event.fix(c),b.type="mousewheel",c.wheelDelta&&(e=c.wheelDelta/120),c.detail&&(e=-c.detail/3),h=e,void 0!==c.axis&&c.axis===c.HORIZONTAL_AXIS&&(h=0,g=-1*e),void 0!==c.wheelDeltaY&&(h=c.wheelDeltaY/120),void 0!==c.wheelDeltaX&&(g=-1*c.wheelDeltaX/120),d.unshift(b,e,g,h),(a.event.dispatch||a.event.handle).apply(this,d)}var b=["DOMMouseScroll","mousewheel"];if(a.event.fixHooks)for(var c=b.length;c;)a.event.fixHooks[b[--c]]=a.event.mouseHooks;a.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var a=b.length;a;)this.addEventListener(b[--a],d,!1);else this.onmousewheel=d},teardown:function(){if(this.removeEventListener)for(var a=b.length;a;)this.removeEventListener(b[--a],d,!1);else this.onmousewheel=null}},a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})}(jQuery);
(function(b,a,c){b.fn.jScrollPane=function(e){function d(D,O){var ay,Q=this,Y,aj,v,al,T,Z,y,q,az,aE,au,i,I,h,j,aa,U,ap,X,t,A,aq,af,am,G,l,at,ax,x,av,aH,f,L,ai=true,P=true,aG=false,k=false,ao=D.clone(false,false).empty(),ac=b.fn.mwheelIntent?"mwheelIntent.jsp":"mousewheel.jsp";aH=D.css("paddingTop")+" "+D.css("paddingRight")+" "+D.css("paddingBottom")+" "+D.css("paddingLeft");f=(parseInt(D.css("paddingLeft"),10)||0)+(parseInt(D.css("paddingRight"),10)||0);function ar(aQ){var aL,aN,aM,aJ,aI,aP,aO=false,aK=false;ay=aQ;if(Y===c){aI=D.scrollTop();aP=D.scrollLeft();D.css({overflow:"hidden",padding:0});aj=D.innerWidth()+f;v=D.innerHeight();D.width(aj);Y=b('<div class="jspPane" />').css("padding",aH).append(D.children());al=b('<div class="jspContainer" />').css({width:aj+"px",height:v+"px"}).append(Y).appendTo(D)}else{D.css("width","");aO=ay.stickToBottom&&K();aK=ay.stickToRight&&B();aJ=D.innerWidth()+f!=aj||D.outerHeight()!=v;if(aJ){aj=D.innerWidth()+f;v=D.innerHeight();al.css({width:aj+"px",height:v+"px"})}if(!aJ&&L==T&&Y.outerHeight()==Z){D.width(aj);return}L=T;Y.css("width","");D.width(aj);al.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()}Y.css("overflow","auto");if(aQ.contentWidth){T=aQ.contentWidth}else{T=Y[0].scrollWidth}Z=Y[0].scrollHeight;Y.css("overflow","");y=T/aj;q=Z/v;az=q>1;aE=y>1;if(!(aE||az)){D.removeClass("jspScrollable");Y.css({top:0,width:al.width()-f});n();E();R();w()}else{D.addClass("jspScrollable");aL=ay.maintainPosition&&(I||aa);if(aL){aN=aC();aM=aA()}aF();z();F();if(aL){N(aK?(T-aj):aN,false);M(aO?(Z-v):aM,false)}J();ag();an();if(ay.enableKeyboardNavigation){S()}if(ay.clickOnTrack){p()}C();if(ay.hijackInternalLinks){m()}}if(ay.autoReinitialise&&!av){av=setInterval(function(){ar(ay)},ay.autoReinitialiseDelay)}else{if(!ay.autoReinitialise&&av){clearInterval(av)}}aI&&D.scrollTop(0)&&M(aI,false);aP&&D.scrollLeft(0)&&N(aP,false);D.trigger("jsp-initialised",[aE||az])}function aF(){if(az){al.append(b('<div class="jspVerticalBar" />').append(b('<div class="jspCap jspCapTop" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragTop" />'),b('<div class="jspDragBottom" />'))),b('<div class="jspCap jspCapBottom" />')));U=al.find(">.jspVerticalBar");ap=U.find(">.jspTrack");au=ap.find(">.jspDrag");if(ay.showArrows){aq=b('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp",aD(0,-1)).bind("click.jsp",aB);af=b('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp",aD(0,1)).bind("click.jsp",aB);if(ay.arrowScrollOnHover){aq.bind("mouseover.jsp",aD(0,-1,aq));af.bind("mouseover.jsp",aD(0,1,af))}ak(ap,ay.verticalArrowPositions,aq,af)}t=v;al.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function(){t-=b(this).outerHeight()});au.hover(function(){au.addClass("jspHover")},function(){au.removeClass("jspHover")}).bind("mousedown.jsp",function(aI){b("html").bind("dragstart.jsp selectstart.jsp",aB);au.addClass("jspActive");var s=aI.pageY-au.position().top;b("html").bind("mousemove.jsp",function(aJ){V(aJ.pageY-s,false)}).bind("mouseup.jsp mouseleave.jsp",aw);return false});o()}}function o(){ap.height(t+"px");I=0;X=ay.verticalGutter+ap.outerWidth();Y.width(aj-X-f);try{if(U.position().left===0){Y.css("margin-left",X+"px")}}catch(s){}}function z(){if(aE){al.append(b('<div class="jspHorizontalBar" />').append(b('<div class="jspCap jspCapLeft" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragLeft" />'),b('<div class="jspDragRight" />'))),b('<div class="jspCap jspCapRight" />')));am=al.find(">.jspHorizontalBar");G=am.find(">.jspTrack");h=G.find(">.jspDrag");if(ay.showArrows){ax=b('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp",aD(-1,0)).bind("click.jsp",aB);x=b('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp",aD(1,0)).bind("click.jsp",aB);
if(ay.arrowScrollOnHover){ax.bind("mouseover.jsp",aD(-1,0,ax));x.bind("mouseover.jsp",aD(1,0,x))}ak(G,ay.horizontalArrowPositions,ax,x)}h.hover(function(){h.addClass("jspHover")},function(){h.removeClass("jspHover")}).bind("mousedown.jsp",function(aI){b("html").bind("dragstart.jsp selectstart.jsp",aB);h.addClass("jspActive");var s=aI.pageX-h.position().left;b("html").bind("mousemove.jsp",function(aJ){W(aJ.pageX-s,false)}).bind("mouseup.jsp mouseleave.jsp",aw);return false});l=al.innerWidth();ah()}}function ah(){al.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function(){l-=b(this).outerWidth()});G.width(l+"px");aa=0}function F(){if(aE&&az){var aI=G.outerHeight(),s=ap.outerWidth();t-=aI;b(am).find(">.jspCap:visible,>.jspArrow").each(function(){l+=b(this).outerWidth()});l-=s;v-=s;aj-=aI;G.parent().append(b('<div class="jspCorner" />').css("width",aI+"px"));o();ah()}if(aE){Y.width((al.outerWidth()-f)+"px")}Z=Y.outerHeight();q=Z/v;if(aE){at=Math.ceil(1/y*l);if(at>ay.horizontalDragMaxWidth){at=ay.horizontalDragMaxWidth}else{if(at<ay.horizontalDragMinWidth){at=ay.horizontalDragMinWidth}}h.width(at+"px");j=l-at;ae(aa)}if(az){A=Math.ceil(1/q*t);if(A>ay.verticalDragMaxHeight){A=ay.verticalDragMaxHeight}else{if(A<ay.verticalDragMinHeight){A=ay.verticalDragMinHeight}}au.height(A+"px");i=t-A;ad(I)}}function ak(aJ,aL,aI,s){var aN="before",aK="after",aM;if(aL=="os"){aL=/Mac/.test(navigator.platform)?"after":"split"}if(aL==aN){aK=aL}else{if(aL==aK){aN=aL;aM=aI;aI=s;s=aM}}aJ[aN](aI)[aK](s)}function aD(aI,s,aJ){return function(){H(aI,s,this,aJ);this.blur();return false}}function H(aL,aK,aO,aN){aO=b(aO).addClass("jspActive");var aM,aJ,aI=true,s=function(){if(aL!==0){Q.scrollByX(aL*ay.arrowButtonSpeed)}if(aK!==0){Q.scrollByY(aK*ay.arrowButtonSpeed)}aJ=setTimeout(s,aI?ay.initialDelay:ay.arrowRepeatFreq);aI=false};s();aM=aN?"mouseout.jsp":"mouseup.jsp";aN=aN||b("html");aN.bind(aM,function(){aO.removeClass("jspActive");aJ&&clearTimeout(aJ);aJ=null;aN.unbind(aM)})}function p(){w();if(az){ap.bind("mousedown.jsp",function(aN){if(aN.originalTarget===c||aN.originalTarget==aN.currentTarget){var aL=b(this),aO=aL.offset(),aM=aN.pageY-aO.top-I,aJ,aI=true,s=function(){var aR=aL.offset(),aS=aN.pageY-aR.top-A/2,aP=v*ay.scrollPagePercent,aQ=i*aP/(Z-v);if(aM<0){if(I-aQ>aS){Q.scrollByY(-aP)}else{V(aS)}}else{if(aM>0){if(I+aQ<aS){Q.scrollByY(aP)}else{V(aS)}}else{aK();return}}aJ=setTimeout(s,aI?ay.initialDelay:ay.trackClickRepeatFreq);aI=false},aK=function(){aJ&&clearTimeout(aJ);aJ=null;b(document).unbind("mouseup.jsp",aK)};s();b(document).bind("mouseup.jsp",aK);return false}})}if(aE){G.bind("mousedown.jsp",function(aN){if(aN.originalTarget===c||aN.originalTarget==aN.currentTarget){var aL=b(this),aO=aL.offset(),aM=aN.pageX-aO.left-aa,aJ,aI=true,s=function(){var aR=aL.offset(),aS=aN.pageX-aR.left-at/2,aP=aj*ay.scrollPagePercent,aQ=j*aP/(T-aj);if(aM<0){if(aa-aQ>aS){Q.scrollByX(-aP)}else{W(aS)}}else{if(aM>0){if(aa+aQ<aS){Q.scrollByX(aP)}else{W(aS)}}else{aK();return}}aJ=setTimeout(s,aI?ay.initialDelay:ay.trackClickRepeatFreq);aI=false},aK=function(){aJ&&clearTimeout(aJ);aJ=null;b(document).unbind("mouseup.jsp",aK)};s();b(document).bind("mouseup.jsp",aK);return false}})}}function w(){if(G){G.unbind("mousedown.jsp")}if(ap){ap.unbind("mousedown.jsp")}}function aw(){b("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp");if(au){au.removeClass("jspActive")}if(h){h.removeClass("jspActive")}}function V(s,aI){if(!az){return}if(s<0){s=0}else{if(s>i){s=i}}if(aI===c){aI=ay.animateScroll}if(aI){Q.animate(au,"top",s,ad)}else{au.css("top",s);ad(s)}}function ad(aI){if(aI===c){aI=au.position().top}al.scrollTop(0);I=aI;var aL=I===0,aJ=I==i,aK=aI/i,s=-aK*(Z-v);if(ai!=aL||aG!=aJ){ai=aL;aG=aJ;D.trigger("jsp-arrow-change",[ai,aG,P,k])}u(aL,aJ);Y.css("top",s);D.trigger("jsp-scroll-y",[-s,aL,aJ]).trigger("scroll")}function W(aI,s){if(!aE){return}if(aI<0){aI=0}else{if(aI>j){aI=j}}if(s===c){s=ay.animateScroll}if(s){Q.animate(h,"left",aI,ae)
}else{h.css("left",aI);ae(aI)}}function ae(aI){if(aI===c){aI=h.position().left}al.scrollTop(0);aa=aI;var aL=aa===0,aK=aa==j,aJ=aI/j,s=-aJ*(T-aj);if(P!=aL||k!=aK){P=aL;k=aK;D.trigger("jsp-arrow-change",[ai,aG,P,k])}r(aL,aK);Y.css("left",s);D.trigger("jsp-scroll-x",[-s,aL,aK]).trigger("scroll")}function u(aI,s){if(ay.showArrows){aq[aI?"addClass":"removeClass"]("jspDisabled");af[s?"addClass":"removeClass"]("jspDisabled")}}function r(aI,s){if(ay.showArrows){ax[aI?"addClass":"removeClass"]("jspDisabled");x[s?"addClass":"removeClass"]("jspDisabled")}}function M(s,aI){var aJ=s/(Z-v);V(aJ*i,aI)}function N(aI,s){var aJ=aI/(T-aj);W(aJ*j,s)}function ab(aV,aQ,aJ){var aN,aK,aL,s=0,aU=0,aI,aP,aO,aS,aR,aT;try{aN=b(aV)}catch(aM){return}aK=aN.outerHeight();aL=aN.outerWidth();al.scrollTop(0);al.scrollLeft(0);while(!aN.is(".jspPane")){s+=aN.position().top;aU+=aN.position().left;aN=aN.offsetParent();if(/^body|html$/i.test(aN[0].nodeName)){return}}aI=aA();aO=aI+v;if(s<aI||aQ){aR=s-ay.verticalGutter}else{if(s+aK>aO){aR=s-v+aK+ay.verticalGutter}}if(aR){M(aR,aJ)}aP=aC();aS=aP+aj;if(aU<aP||aQ){aT=aU-ay.horizontalGutter}else{if(aU+aL>aS){aT=aU-aj+aL+ay.horizontalGutter}}if(aT){N(aT,aJ)}}function aC(){return -Y.position().left}function aA(){return -Y.position().top}function K(){var s=Z-v;return(s>20)&&(s-aA()<10)}function B(){var s=T-aj;return(s>20)&&(s-aC()<10)}function ag(){al.unbind(ac).bind(ac,function(aL,aM,aK,aI){var aJ=aa,s=I;Q.scrollBy(aK*ay.mouseWheelSpeed,-aI*ay.mouseWheelSpeed,false);return aJ==aa&&s==I})}function n(){al.unbind(ac)}function aB(){return false}function J(){Y.find(":input,a").unbind("focus.jsp").bind("focus.jsp",function(s){ab(s.target,false)})}function E(){Y.find(":input,a").unbind("focus.jsp")}function S(){var s,aI,aK=[];aE&&aK.push(am[0]);az&&aK.push(U[0]);Y.focus(function(){D.focus()});D.attr("tabindex",0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp",function(aN){if(aN.target!==this&&!(aK.length&&b(aN.target).closest(aK).length)){return}var aM=aa,aL=I;switch(aN.keyCode){case 40:case 38:case 34:case 32:case 33:case 39:case 37:s=aN.keyCode;aJ();break;case 35:M(Z-v);s=null;break;case 36:M(0);s=null;break}aI=aN.keyCode==s&&aM!=aa||aL!=I;return !aI}).bind("keypress.jsp",function(aL){if(aL.keyCode==s){aJ()}return !aI});if(ay.hideFocus){D.css("outline","none");if("hideFocus" in al[0]){D.attr("hideFocus",true)}}else{D.css("outline","");if("hideFocus" in al[0]){D.attr("hideFocus",false)}}function aJ(){var aM=aa,aL=I;switch(s){case 40:Q.scrollByY(ay.keyboardSpeed,false);break;case 38:Q.scrollByY(-ay.keyboardSpeed,false);break;case 34:case 32:Q.scrollByY(v*ay.scrollPagePercent,false);break;case 33:Q.scrollByY(-v*ay.scrollPagePercent,false);break;case 39:Q.scrollByX(ay.keyboardSpeed,false);break;case 37:Q.scrollByX(-ay.keyboardSpeed,false);break}aI=aM!=aa||aL!=I;return aI}}function R(){D.attr("tabindex","-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp")}function C(){if(location.hash&&location.hash.length>1){var aK,aI,aJ=escape(location.hash.substr(1));try{aK=b("#"+aJ+', a[name="'+aJ+'"]')}catch(s){return}if(aK.length&&Y.find(aJ)){if(al.scrollTop()===0){aI=setInterval(function(){if(al.scrollTop()>0){ab(aK,true);b(document).scrollTop(al.position().top);clearInterval(aI)}},50)}else{ab(aK,true);b(document).scrollTop(al.position().top)}}}}function m(){if(b(document.body).data("jspHijack")){return}b(document.body).data("jspHijack",true);b(document.body).delegate("a[href*=#]","click",function(s){var aI=this.href.substr(0,this.href.indexOf("#")),aK=location.href,aO,aP,aJ,aM,aL,aN;if(location.href.indexOf("#")!==-1){aK=location.href.substr(0,location.href.indexOf("#"))}if(aI!==aK){return}aO=escape(this.href.substr(this.href.indexOf("#")+1));aP;try{aP=b("#"+aO+', a[name="'+aO+'"]')}catch(aQ){return}if(!aP.length){return}aJ=aP.closest(".jspScrollable");aM=aJ.data("jsp");aM.scrollToElement(aP,true);if(aJ[0].scrollIntoView){aL=b(a).scrollTop();aN=aP.offset().top;if(aN<aL||aN>aL+b(a).height()){aJ[0].scrollIntoView()}}s.preventDefault()
})}function an(){var aJ,aI,aL,aK,aM,s=false;al.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp",function(aN){var aO=aN.originalEvent.touches[0];aJ=aC();aI=aA();aL=aO.pageX;aK=aO.pageY;aM=false;s=true}).bind("touchmove.jsp",function(aQ){if(!s){return}var aP=aQ.originalEvent.touches[0],aO=aa,aN=I;Q.scrollTo(aJ+aL-aP.pageX,aI+aK-aP.pageY);aM=aM||Math.abs(aL-aP.pageX)>5||Math.abs(aK-aP.pageY)>5;return aO==aa&&aN==I}).bind("touchend.jsp",function(aN){s=false}).bind("click.jsp-touchclick",function(aN){if(aM){aM=false;return false}})}function g(){var s=aA(),aI=aC();D.removeClass("jspScrollable").unbind(".jsp");D.replaceWith(ao.append(Y.children()));ao.scrollTop(s);ao.scrollLeft(aI);if(av){clearInterval(av)}}b.extend(Q,{reinitialise:function(aI){aI=b.extend({},ay,aI);ar(aI)},scrollToElement:function(aJ,aI,s){ab(aJ,aI,s)},scrollTo:function(aJ,s,aI){N(aJ,aI);M(s,aI)},scrollToX:function(aI,s){N(aI,s)},scrollToY:function(s,aI){M(s,aI)},scrollToPercentX:function(aI,s){N(aI*(T-aj),s)},scrollToPercentY:function(aI,s){M(aI*(Z-v),s)},scrollBy:function(aI,s,aJ){Q.scrollByX(aI,aJ);Q.scrollByY(s,aJ)},scrollByX:function(s,aJ){var aI=aC()+Math[s<0?"floor":"ceil"](s),aK=aI/(T-aj);W(aK*j,aJ)},scrollByY:function(s,aJ){var aI=aA()+Math[s<0?"floor":"ceil"](s),aK=aI/(Z-v);V(aK*i,aJ)},positionDragX:function(s,aI){W(s,aI)},positionDragY:function(aI,s){V(aI,s)},animate:function(aI,aL,s,aK){var aJ={};aJ[aL]=s;aI.animate(aJ,{duration:ay.animateDuration,easing:ay.animateEase,queue:false,step:aK})},getContentPositionX:function(){return aC()},getContentPositionY:function(){return aA()},getContentWidth:function(){return T},getContentHeight:function(){return Z},getPercentScrolledX:function(){return aC()/(T-aj)},getPercentScrolledY:function(){return aA()/(Z-v)},getIsScrollableH:function(){return aE},getIsScrollableV:function(){return az},getContentPane:function(){return Y},scrollToBottom:function(s){V(i,s)},hijackInternalLinks:b.noop,destroy:function(){g()}});ar(O)}e=b.extend({},b.fn.jScrollPane.defaults,e);b.each(["mouseWheelSpeed","arrowButtonSpeed","trackClickSpeed","keyboardSpeed"],function(){e[this]=e[this]||e.speed});return this.each(function(){var f=b(this),g=f.data("jsp");if(g){g.reinitialise(e)}else{g=new d(f,e);f.data("jsp",g)}})};b.fn.jScrollPane.defaults={showArrows:false,maintainPosition:true,stickToBottom:false,stickToRight:false,clickOnTrack:true,autoReinitialise:false,autoReinitialiseDelay:500,verticalDragMinHeight:0,verticalDragMaxHeight:99999,horizontalDragMinWidth:0,horizontalDragMaxWidth:99999,contentWidth:c,animateScroll:false,animateDuration:300,animateEase:"linear",hijackInternalLinks:false,verticalGutter:4,horizontalGutter:4,mouseWheelSpeed:0,arrowButtonSpeed:0,arrowRepeatFreq:50,arrowScrollOnHover:false,trackClickSpeed:0,trackClickRepeatFreq:70,verticalArrowPositions:"split",horizontalArrowPositions:"split",enableKeyboardNavigation:true,hideFocus:false,keyboardSpeed:0,initialDelay:300,speed:30,scrollPagePercent:0.8}})(jQuery,this);
!function(){var a=function(a){function b(a,b){return a<<b|a>>>32-b}function c(a,b){var c,d,e,f,g;return e=2147483648&a,f=2147483648&b,c=1073741824&a,d=1073741824&b,g=(1073741823&a)+(1073741823&b),c&d?2147483648^g^e^f:c|d?1073741824&g?3221225472^g^e^f:1073741824^g^e^f:g^e^f}function d(a,b,c){return a&b|~a&c}function e(a,b,c){return a&c|b&~c}function f(a,b,c){return a^b^c}function g(a,b,c){return b^(a|~c)}function h(a,e,f,g,h,i,j){return a=c(a,c(c(d(e,f,g),h),j)),c(b(a,i),e)}function i(a,d,f,g,h,i,j){return a=c(a,c(c(e(d,f,g),h),j)),c(b(a,i),d)}function j(a,d,e,g,h,i,j){return a=c(a,c(c(f(d,e,g),h),j)),c(b(a,i),d)}function k(a,d,e,f,h,i,j){return a=c(a,c(c(g(d,e,f),h),j)),c(b(a,i),d)}function l(a){for(var b,c=a.length,d=c+8,e=(d-d%64)/64,f=16*(e+1),g=Array(f-1),h=0,i=0;i<c;)b=(i-i%4)/4,h=i%4*8,g[b]=g[b]|a.charCodeAt(i)<<h,i++;return b=(i-i%4)/4,h=i%4*8,g[b]=g[b]|128<<h,g[f-2]=c<<3,g[f-1]=c>>>29,g}function m(a){var d,e,b="",c="";for(e=0;e<=3;e++)d=a>>>8*e&255,c="0"+d.toString(16),b+=c.substr(c.length-2,2);return b}function n(a){a=a.replace(/\r\n/g,"\n");for(var b="",c=0;c<a.length;c++){var d=a.charCodeAt(c);d<128?b+=String.fromCharCode(d):d>127&&d<2048?(b+=String.fromCharCode(d>>6|192),b+=String.fromCharCode(63&d|128)):(b+=String.fromCharCode(d>>12|224),b+=String.fromCharCode(d>>6&63|128),b+=String.fromCharCode(63&d|128))}return b}var p,q,r,s,t,u,v,w,x,o=Array(),y=7,z=12,A=17,B=22,C=5,D=9,E=14,F=20,G=4,H=11,I=16,J=23,K=6,L=10,M=15,N=21;for(a=n(a),o=l(a),u=1732584193,v=4023233417,w=2562383102,x=271733878,p=0;p<o.length;p+=16)q=u,r=v,s=w,t=x,u=h(u,v,w,x,o[p+0],y,3614090360),x=h(x,u,v,w,o[p+1],z,3905402710),w=h(w,x,u,v,o[p+2],A,606105819),v=h(v,w,x,u,o[p+3],B,3250441966),u=h(u,v,w,x,o[p+4],y,4118548399),x=h(x,u,v,w,o[p+5],z,1200080426),w=h(w,x,u,v,o[p+6],A,2821735955),v=h(v,w,x,u,o[p+7],B,4249261313),u=h(u,v,w,x,o[p+8],y,1770035416),x=h(x,u,v,w,o[p+9],z,2336552879),w=h(w,x,u,v,o[p+10],A,4294925233),v=h(v,w,x,u,o[p+11],B,2304563134),u=h(u,v,w,x,o[p+12],y,1804603682),x=h(x,u,v,w,o[p+13],z,4254626195),w=h(w,x,u,v,o[p+14],A,2792965006),v=h(v,w,x,u,o[p+15],B,1236535329),u=i(u,v,w,x,o[p+1],C,4129170786),x=i(x,u,v,w,o[p+6],D,3225465664),w=i(w,x,u,v,o[p+11],E,643717713),v=i(v,w,x,u,o[p+0],F,3921069994),u=i(u,v,w,x,o[p+5],C,3593408605),x=i(x,u,v,w,o[p+10],D,38016083),w=i(w,x,u,v,o[p+15],E,3634488961),v=i(v,w,x,u,o[p+4],F,3889429448),u=i(u,v,w,x,o[p+9],C,568446438),x=i(x,u,v,w,o[p+14],D,3275163606),w=i(w,x,u,v,o[p+3],E,4107603335),v=i(v,w,x,u,o[p+8],F,1163531501),u=i(u,v,w,x,o[p+13],C,2850285829),x=i(x,u,v,w,o[p+2],D,4243563512),w=i(w,x,u,v,o[p+7],E,1735328473),v=i(v,w,x,u,o[p+12],F,2368359562),u=j(u,v,w,x,o[p+5],G,4294588738),x=j(x,u,v,w,o[p+8],H,2272392833),w=j(w,x,u,v,o[p+11],I,1839030562),v=j(v,w,x,u,o[p+14],J,4259657740),u=j(u,v,w,x,o[p+1],G,2763975236),x=j(x,u,v,w,o[p+4],H,1272893353),w=j(w,x,u,v,o[p+7],I,4139469664),v=j(v,w,x,u,o[p+10],J,3200236656),u=j(u,v,w,x,o[p+13],G,681279174),x=j(x,u,v,w,o[p+0],H,3936430074),w=j(w,x,u,v,o[p+3],I,3572445317),v=j(v,w,x,u,o[p+6],J,76029189),u=j(u,v,w,x,o[p+9],G,3654602809),x=j(x,u,v,w,o[p+12],H,3873151461),w=j(w,x,u,v,o[p+15],I,530742520),v=j(v,w,x,u,o[p+2],J,3299628645),u=k(u,v,w,x,o[p+0],K,4096336452),x=k(x,u,v,w,o[p+7],L,1126891415),w=k(w,x,u,v,o[p+14],M,2878612391),v=k(v,w,x,u,o[p+5],N,4237533241),u=k(u,v,w,x,o[p+12],K,1700485571),x=k(x,u,v,w,o[p+3],L,2399980690),w=k(w,x,u,v,o[p+10],M,4293915773),v=k(v,w,x,u,o[p+1],N,2240044497),u=k(u,v,w,x,o[p+8],K,1873313359),x=k(x,u,v,w,o[p+15],L,4264355552),w=k(w,x,u,v,o[p+6],M,2734768916),v=k(v,w,x,u,o[p+13],N,1309151649),u=k(u,v,w,x,o[p+4],K,4149444226),x=k(x,u,v,w,o[p+11],L,3174756917),w=k(w,x,u,v,o[p+2],M,718787259),v=k(v,w,x,u,o[p+9],N,3951481745),u=c(u,q),v=c(v,r),w=c(w,s),x=c(x,t);var O=m(u)+m(v)+m(w)+m(x);return O.toLowerCase()};window.md5=a}(window);
(function(g){var k=+new Date();var c=100;var j=[];function m(p){var n=+new Date();if(n-k>c){for(var o=0;o<j.length;o++){j[o]();}k=n;}if(typeof jQuery!="undefined"){jQuery(document).trigger("onDomChange");
}}var f=function(n,o){if(o){c=o;}j.push(n);};function e(){var p=document.getElementsByTagName("*");var o=p.length;var q=setTimeout(function n(){var t=document.getElementsByTagName("*");
var r=t.length;if(r!=o){p=[];}for(var s=0;s<r;s++){if(t[s]!==p[s]){m();p=t;o=r;break;}}setTimeout(n,c);},c);}var l={};var a=document.documentElement;var d=3;
function i(){if(l.DOMNodeInserted){g.addEventListener("DOMContentLoaded",function(n){if(l.DOMSubtreeModified){a.addEventListener("DOMSubtreeModified",m,false);
}else{a.addEventListener("DOMNodeInserted",m,false);a.addEventListener("DOMNodeRemoved",m,false);}},false);}else{if(document.onpropertychange){document.onpropertychange=m;
}else{e();}}}function h(o){a.addEventListener(o,function n(){l[o]=true;a.removeEventListener(o,n,false);if(--d===0){i();}},false);}if(g.addEventListener){h("DOMSubtreeModified");
h("DOMNodeInserted");h("DOMNodeRemoved");i();}else{i();}var b=document.createElement("div");a.appendChild(b);a.removeChild(b);g.onDomChange=f;})(window);
!function(a){a.fn.jClever=function(b){var b=a.extend({applyTo:{select:!0,checkbox:!0,radio:!0,button:!0,file:!0,input:!0,textarea:!0},validate:{state:!1,items:{}},errorTemplate:'<span class="jClever-error-label"></span>',errorClassTemplate:"jClever-error-label",selfClass:"alice",fileUploadText:"Загрузить",autoTracking:!1,autoInit:!1,autoinitSelector:""},b),c={},d=[],e={},f={validation:"validation"},g={isNumeric:function(a){var b=/^\d+$/;return b.test(a)},isString:function(a){var b=/^[a-zA-ZА-Яа-я]+$/;return b.test(a)},isAnyText:function(a){var b=/^[a-zA-ZА-Яа-я0-9]+$/;return b.test(a)},isEmail:function(a){var b=/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,4}$/;return b.test(a)},isSiteURL:function(a){var b=/^((https?|ftp)\:\/\/)?([a-z0-9]{1,})([a-z0-9-.]*)\.([a-z]{2,4})$/;return b.test(a)}},h=999,i=1,j={init:function(c){var d=a(c);b.validate.state===!0&&d.submit(function(){var c=a(this).get(0),d={};a(c).find(".error").removeClass("error");for(var e in b.validate.items)if(void 0!==c[e])for(var h in b.validate.items[e])switch(h){case"custom":"function"==typeof b.validate.items[e].fn&&1!=b.validate.items[e].fn(c[e].value)&&(d[e]=b.validate.items[e][h]);break;case"required":""!=c[e].value&&c[e].value!=a(c[e]).data("placeholder")||(d[e]=b.validate.items[e][h]);break;case"numeric":1!=g.isNumeric(c[e].value)&&(d[e]=b.validate.items[e][h]);break;case"str":1!=g.isString(c[e].value)&&(d[e]=b.validate.items[e][h]);break;case"stringAndNumeric":1!=g.isAnyText(c[e].value)&&(d[e]=b.validate.items[e][h]);break;case"mail":1!=g.isEmail(c[e].value)&&(d[e]=b.validate.items[e][h]);break;case"siteURL":1!=g.isSiteURL(c[e].value)&&(d[e]=b.validate.items[e][h])}var i=0,j=[];for(var k in d)if(void 0!=c[k]){var l=d[k],m=a(c[k]),n=m.parents(".jClever-element"),o=n.find("."+b.errorClassTemplate),p=m.attr("id");a("label[for="+p+"]").addClass("error");n.addClass("error"),o.text(l),j.push({type:f.validation,element:m,text:l}),i++}return!i||(a(c).trigger("error.jClever",[j]),!1)}),d.find("input[type=text], input[type=password], textarea").each(function(){var b=a(this),c=a(this).data("placeholder");"string"==typeof c&&(""==b.val()&&b.val(c).addClass("placeholdered"),b.focusin(function(){b.val()==c&&b.val("").removeClass("placeholdered")}),b.focusout(function(){""==b.val()&&b.val(c).addClass("placeholdered")}))});var k=d.get(0).elements,l="";if("undefined"!=typeof k)for(key=0;key<k.length;key++){var m=a(k[key]),n=md5(j.elementToString(m));if(l+=n,m.data("jCleverHash",n),"undefined"!=typeof k[key].nodeName)switch(k[key].nodeName){case"SELECT":b.applyTo.select&&("undefined"==typeof m.attr("multiple")?j.selectActivate(k[key],h,i):j.multiSelectActivate(k[key],h,i),e[m.attr("name")]={type:"select",value:m.attr("value")},m.data("jclevered",!0),h--,i++);break;case"TEXTAREA":b.applyTo.textarea&&(j.textareaActivate(k[key],i),m.data("jclevered",!0),i++);break;case"INPUT":b.applyTo.checkbox&&"checkbox"==m.attr("type")&&(e[m.attr("name")]={type:"checkbox",value:m.is(":checked")?1:0},j.checkboxActivate(k[key],i),m.data("jclevered",!0),i++),b.applyTo.radio&&"radio"==m.attr("type")&&(e[m.attr("name")]={type:"radio",value:m.is(":checked")?1:0},j.radioActivate(k[key],i),m.data("jclevered",!0),i++),b.applyTo.file&&"file"==m.attr("type")&&(e[m.attr("name")]={type:"file",value:""},j.fileActivate(k[key],i),m.data("jclevered",!0),i++),!b.applyTo.button||"submit"!=m.attr("type")&&"reset"!=m.attr("type")&&"button"!=m.attr("type")||(j.submitActivate(k[key],i),m.data("jclevered",!0),i++),!b.applyTo.input||"text"!=m.attr("type")&&"password"!=m.attr("type")||(j.inputActivate(k[key],i),m.data("jclevered",!0),i++)}}d.find("input[type=password]").each(function(){var d,b=a(this),c=a(this).data("placeholder-pass");"string"==typeof c&&(a('<span class="jClever-element-input-placeholder">'+c+"</span>").insertAfter(b),d=b.next(".jClever-element-input-placeholder"),""==b.val()?(b.addClass("placeholdered"),d.show()):d.hide(),b.focusin(function(){""==b.val()&&(b.removeClass("placeholdered"),d.hide())}),b.focusout(function(){""==b.val()&&(b.addClass("placeholdered"),d.show())}),d.click(function(){b.focus()}))}),d.data("jCleverHash",md5(l)),d.find("button[type=reset]").click(function(){j.reset(d)})},refresh:function(b){var c=a(b).get(0).elements;if("undefined"==typeof c)return!1;for(key=0;key<c.length;key++){var d=a(c[key]),e=d.data("jclevered");if(d.data("jCleverHash")!=md5(j.elementToString(d))){if(elementHash=md5(j.elementToString(d)),d.data("jCleverHash",elementHash),"undefined"==typeof c[key].nodeName)continue;if("undefined"!=typeof e)switch(c[key].nodeName){case"SELECT":d.trigger("update");break;case"INPUT":d.trigger("updates.jClever")}else{var f=c[key].nodeName.toLowerCase();"input"==f&&(f=d.attr("type"));var g=j.elementAdd(d,f,a(b).data("publicApi"));a(b).data("publicApi",g)}}}},destroy:function(b){b.find("select").each(function(){var b=a(this).clone();a(this).parents(".jClever-element").empty().after(b)}),b.find("input[type=checkbox]").each(function(){var b=a(this).removeClass("hidden").clone();a(this).parents(".jClever-element").empty().after(b)}),b.find("input[type=radio]").each(function(){var b=a(this).removeClass("hidden").clone();a(this).parents(".jClever-element").empty().after(b)}),b.find(".jClever-element").remove(),b.removeClass("clevered")},reset:function(b){b.find("input[type=radio], input[type=checkbox], select, input[type=file]").each(function(){if(e[a(this).attr("name")])switch(e[a(this).attr("name")].type){case"select":j.selectSetPosition(a(this),e[a(this).attr("name")].value);break;case"checkbox":j.checkboxSetState(a(this),e[a(this).attr("name")].value);break;case"radio":j.radioSetState(a(this),e[a(this).attr("name")].value);break;case"file":j.fileSetState(a(this),e[a(this).attr("name")].value);break;default:return}})},elementToString:function(b){var c={},d="";if(c.innerContent="","undefined"==typeof b&&"undefined"==typeof b.get(0).nodeName)return!1;"SELECT"==b.get(0).nodeName&&b.find("option").each(function(){c.innerContent+=a(this).attr("value").toString()+a(this).text()}),c.className=b.attr("class"),c.name=b.attr("name"),c.checked=b.attr("checked"),c.selected=b.attr("selected"),c.multiple=b.attr("multiple"),c.readonly=b.attr("readonly"),c.disabled=b.attr("disabled"),c.id=b.attr("id"),c.alt=b.attr("alt"),c.title=b.attr("title"),c.style=b.attr("style");for(var e in c)c.hasOwnProperty(e)&&(d+=e+":"+c[e]+";");return d},selectSetPosition:function(a,b){a.find("option").removeAttr("selected"),a.find('option[value="'+b+'"]').attr("selected","selected"),a.trigger("change"),a.trigger("update")},selectAdd:function(b){a(element).find(b).each(function(){e[a(this).attr("name")]={type:"select",value:a(this).attr("value")},j.selectActivate(this,h,i),self.data("jclevered",!0),h--,i++})},checkboxSetState:function(a,b){1==b?a.attr("checked","checked").prop("checked",!0):a.removeAttr("checked").prop("checked",!1),a.trigger("change")},radioSetState:function(a,b){1==b?a.attr("checked","checked").prop("checked",!0):a.removeAttr("checked").prop("checked",!1),a.trigger("change")},updateFromHTML:function(a,b){a.html(b).trigger("update")},fileSetState:function(a,b){a.parents(".jClever-element-file").find(".jClever-element-file-name").text(b)},selectCollectionExtend:function(b,c){return b[c.attr("name")]={object:c,updateFromHTML:function(b){return a('select[name="'+this.object[0].name+'"]').html(b).trigger("update"),!1},updateFromJsonObject:function(b){var c="";for(var d in b)c+='<option value="'+d+'">'+b[d]+"</option>";return a('select[name="'+this.object[0].name+'"]').html(c).trigger("update"),!1}},b},multiSelectActivate:function(e,f,g){if(!a(e).hasClass("jc-ignore")){d[a(e).attr("name")]={},c=j.selectCollectionExtend(c,a(e));var h=a(e).width();a(e).wrap('<div class="jClever-element" style="z-index:'+f+';"><div class="jClever-element-select-wrapper multiselect" style="width:'+h+"px; z-index:"+f+';"><div class="jClever-element-select-wrapper-design"><div class="jClever-element-select-wrapper-design">').after('<span class="jClever-element-select-center">&nbsp;</span><span class="jClever-element-select-right"><span>v</span></span><div class="jClever-element-select-list-wrapper" style="z-index:'+f+';"><div class="jClever-element-select-list-wrapper-"><div class="jClever-element-select-list-wrapper--"><ul class="jClever-element-select-list"></ul></div></div></div>');var i=a(e).parents(".jClever-element").attr("tabindex",g),k=i.find(".jClever-element-select-center"),n=(i.find(".jClever-element-select-right"),i.find(".jClever-element-select-list"),i.find(".jClever-element-select-list-wrapper")),o=i.find(".jClever-element-select-list-wrapper--"),p=a(a(e).attr("id")?"label[for="+a(e).attr("id")+"]":"labels");if(a(e).attr("disabled")&&i.addClass("disabled"),i.append(b.errorTemplate),a(e).find("option").each(function(){a(this).is(":selected")?i.find(".jClever-element-select-list").append(a('<li class="active '+(a(this).is(":disabled")?" disabled":"")+'" data-value="'+a(this).val()+'"><span><i>'+a(this).text()+"</i></span></li>")):i.find(".jClever-element-select-list").append(a('<li class="'+(a(this).is(":disabled")?" disabled":"")+'" data-value="'+a(this).val()+'"><span><i>'+a(this).text()+"</i></span></li>"))}),a(e).find(":selected")){var q="";a(e).find("option:selected").each(function(){q+="<span class='multiple-item' data-value='"+a(this).val()+"'><span class='multiple-item-text'>"+a(this).text()+"</span><a href='#' class='multiple-item-remove'></a></span>"}),0==q.length?k.html("&nbsp;"):k.html(q)}else k.text("&nbsp;");i.on("click.jClever",".jClever-element-select-center, .jClever-element-select-right",function(){return!a(e).attr("disabled")&&void(n.is(":visible")?a(".jClever-element-select-list-wrapper").hide():(a(".jClever-element-select-list-wrapper").hide(),n.show(),i.trigger("focus"),d[a(e).attr("name")]=o.jScrollPane().data("jsp")))}),i.on("click.jClever",".multiple-item-remove",function(){var b=a(this),c=b.closest("span");return a(e).find('option[value="'+c.data("value")+'"]').removeAttr("selected"),a(e).trigger("change"),a(e).trigger("update.jClever"),!1}),n.on("blur.jClever",function(){a(this).hide()}),i.on("click.jClever","li",function(b){if(!a(this).hasClass("disabled")){var c=a(this).attr("data-value");return a(this).toggleClass("active"),a(this).is(".active")?a(e).find('option[value="'+c+'"]').attr("selected","selected"):a(e).find('option[value="'+c+'"]').removeAttr("selected"),a(e).trigger("change"),!1}}),a(e).on("change.jClever",function(){if(a(this).attr("disabled"))return!1;var b="";a(e).find("option:selected").each(function(){b+="<span class='multiple-item' data-value='"+a(this).val()+"'><span class='multiple-item-text'>"+a(this).text()+"</span><a href='#' class='multiple-item-remove'></a></span>"}),k.html(b)}),a(e).on("update.jClever",function(){var b=a(this).parents(".jClever-element-select-wrapper").find(".jClever-element-select-list").empty(),c=a(this);c.find("option").each(function(){var c=a(this);b.append(a('<li class="'+("selected"==c.attr("selected")?"active":"")+'" data-value="'+c.val()+'"><span><i>'+c.text()+"</i></span></li>"))}),c.find("option:selected").each(function(){q+="<span class='multiple-item' data-value='"+a(this).val()+"'><span class='multiple-item-text'>"+a(this).text()+"</span><a href='#' class='multiple-item-remove'></a></span>"}),c.trigger("change.jClever")}),i.on("focus.jClever",function(){a(this).addClass("focused")}).blur(function(){a(this).removeClass("focused")}),p.on("click.jClever",function(){i.trigger("focus"),n.show(),d[a(e).attr("name")]=o.jScrollPane().data("jsp")});i.on("keydown.jClever",function(b){var c=a(this),f=isNaN(c.data("current"))?0:c.data("current"),g=!1;switch(b.keyCode){case 40:if(a(e).attr("disabled"))return!1;f<a(e).find("option").length-1&&f++;break;case 38:if(a(e).attr("disabled"))return!1;f>0&&f--;break;case 13:if(n.is(":visible"))n.hide();else{if(a(e).attr("disabled"))return!1;n.show(),d[a(e).attr("name")]=o.jScrollPane().data("jsp")}g=!0;break;case 32:if(n.is(":visible"))n.hide();else{if(a(e).attr("disabled"))return!1;n.show(),d[a(e).attr("name")]=o.jScrollPane().data("jsp")}g=!0;break;case 9:return n.hide(),g=!0,!0}if(c.data("current",f),i.find("li.selected").removeClass("selected"),i.find("li:eq("+f+")").addClass("selected"),g){var h=a(e).find("option:eq("+f+")");"selected"==h.attr("selected")?h.removeAttr("selected"):h.attr("selected","selected"),a(e).trigger("change"),a(e).trigger("update")}return n.is(":visible")&&d[a(e).attr("name")].scrollToElement(i.find("li:eq("+f+")")),!1})}},selectActivate:function(e,f,g){if(!a(e).hasClass("jc-ignore")){d[a(e).attr("name")]={},c=j.selectCollectionExtend(c,a(e));var h=a(e).width();a(e).wrap('<div class="jClever-element" style="z-index:'+f+';"><div class="jClever-element-select-wrapper" style="width:'+h+"px; z-index:"+f+';"><div class="jClever-element-select-wrapper-design"><div class="jClever-element-select-wrapper-design">').after('<span class="jClever-element-select-center">&nbsp;</span><span class="jClever-element-select-right"><span>v</span></span><div class="jClever-element-select-list-wrapper" style="z-index:'+f+';"><div class="jClever-element-select-list-wrapper-"><div class="jClever-element-select-list-wrapper--"><ul class="jClever-element-select-list"></ul></div></div></div>');var i=a(e).parents(".jClever-element").attr("tabindex",g),k=i.find(".jClever-element-select-center"),m=(i.find(".jClever-element-select-right"),i.find(".jClever-element-select-list")),n=i.find(".jClever-element-select-list-wrapper"),o=i.find(".jClever-element-select-list-wrapper--"),p=a(a(e).attr("id")?"label[for="+a(e).attr("id")+"]":"labels");a(e).attr("disabled")&&i.addClass("disabled"),i.append(b.errorTemplate),a(e).find("option").each(function(){a(this).is(":selected")?i.find(".jClever-element-select-list").append(a('<li class="active '+(a(this).is(":disabled")?" disabled":"")+'" data-value="'+a(this).val()+'"><span><i>'+a(this).text()+"</i></span></li>")):i.find(".jClever-element-select-list").append(a('<li class=" '+(a(this).is(":disabled")?" disabled":"")+'" data-value="'+a(this).val()+'"><span><i>'+a(this).text()+"</i></span></li>"))}),a(e).find(":selected")?k.text(a(e).find("option:selected").text()):k.text(a(e).find("option:eq(0)").text()),i.on("click.jClever",".jClever-element-select-center, .jClever-element-select-right",function(){return!a(e).attr("disabled")&&void(n.is(":visible")?(a(".jClever-element-select-list-wrapper").hide(),i.removeClass("opened")):(a(".jClever-element-select-list-wrapper").hide(),a(".jClever-element").removeClass("opened"),n.show(),i.addClass("opened"),i.trigger("focus"),d[a(e).attr("name")]=o.jScrollPane().data("jsp")))}),n.on("blur.jClever",function(){a(this).hide(),i.removeClass("opened")}),i.on("click.jClever","li",function(b){var c=a(this).attr("data-value"),d=a(e);return m.find("li.active").removeClass("active"),a(this).addClass("active"),d.find("option[selected=selected]").removeAttr("selected"),d.find("option[selected=selected]").prop("selected",!1),d.find('option[value="'+c+'"]').prop("selected",!0),d.find('option[value="'+c+'"]').attr("selected","selected"),d.trigger("change"),n.hide(),i.removeClass("opened"),!1}),a(e).on("change.jClever",function(){var b=a(this),c=b.find("option[selected=selected]");b.find("option[selected=selected]").length>1&&(c=b.find("option:selected"));var d=c.text();return!b.attr("disabled")&&(k.text(d),void(b.get(0).selectedIndex=c.index()))}),a(e).on("update.jClever",function(){e=this;var b=a(this).parents(".jClever-element-select-wrapper").find(".jClever-element-select-list").empty();a(this).find("option").each(function(){b.append(a('<li data-value="'+a(this).val()+'" class="'+(a(this).prop("selected")?"active":"")+"  "+(a(this).is(":disabled")?"disabled":"")+'"><span><i>'+a(this).text()+"</i></span></li>"))}),a(this).parents(".jClever-element-select-wrapper").find(".jClever-element-select-center").text(a(e).find("option:selected")?a(e).find("option:selected").text():a(e).find("option:not(:disabled):first").text())}),i.on("focus.jClever",function(){a(this).addClass("focused")}).blur(function(){a(this).removeClass("focused"),a(this).removeClass("opened")}),p.on("click.jClever",function(){i.trigger("focus.jClever").addClass("focused"),n.show(),i.addClass("opened"),d[a(e).attr("name")]=o.jScrollPane().data("jsp")});var q="",r=null;i.on("keydown.jClever",function(b){var c=a(e)[0].selectedIndex;switch(b.keyCode){case 40:if(a(e).attr("disabled"))return!1;c<a(e).find("option").length-1&&c++;break;case 38:if(a(e).attr("disabled"))return!1;c>0&&c--;break;case 13:if(n.is(":visible"))n.hide(),i.removeClass("opened");else{if(a(e).attr("disabled"))return!1;n.show(),i.addClass("opened"),d[a(e).attr("name")]=o.jScrollPane().data("jsp")}break;case 32:if(n.is(":visible"))n.hide(),i.removeClass("opened");else{if(a(e).attr("disabled"))return!1;n.show(),i.addClass("opened"),d[a(e).attr("name")]=o.jScrollPane().data("jsp")}break;case 9:return n.hide(),!0;default:q+=String.fromCharCode(b.keyCode),clearTimeout(r),r=setTimeout(function(){for(var b=0,f=a(e)[0].options.length,g=0;g<f;g++)if("string"==typeof a(e)[0].options[g].text){var h=a(e)[0].options[g].text.toUpperCase(),j=new RegExp("^"+q,"i");if(j.test(h)){c=b,a(e)[0].selectedIndex=c,i.find("li.selected").removeClass("selected"),i.find("li:eq("+c+")").addClass("selected"),i.find("option").removeAttr("selected"),i.find("option:eq("+c+")").attr("selected","selected"),a(e).trigger("change"),n.is(":visible")&&d[a(e).attr("name")].scrollToElement(i.find("li:eq("+c+")"));break}b++}q=""},500)}return a(e)[0].selectedIndex=c,i.find("li.selected").removeClass("selected"),i.find("li:eq("+c+")").addClass("selected"),i.find("option").removeAttr("selected"),i.find("option:eq("+c+")").attr("selected","selected"),a(e).trigger("change"),n.is(":visible")&&d[a(e).attr("name")].scrollToElement(i.find("li:eq("+c+")")),!1})}},selectOpen:function(b){"string"==typeof b&&(b=a(b));var c=b.closest(".jClever-element"),e=c.find(".jClever-element-select-list-wrapper"),f=c.find(".jClever-element-select-list-wrapper--");b.addClass("focused"),e.show(),c.addClass("opened"),d[a(b).attr("name")]=f.jScrollPane().data("jsp")},selectClose:function(b){"string"==typeof b&&(b=a(b));var c=b.closest(".jClever-element"),d=c.find(".jClever-element-select-list-wrapper");c.find(".jClever-element-select-list-wrapper--");b.removeClass("focused"),d.hide(),c.removeClass("opened")},checkboxActivate:function(c,d){if(!a(c).hasClass("jc-ignore")){var e=a(c).wrap('<div class="jClever-element" tabindex="'+d+'">').addClass("hidden").after('<span class="jClever-element-checkbox-twins"><span class="jClever-element-checkbox-twins-element"></span><span class="jClever-element-checkbox-twins-color"></span></span>'),f=e.attr("id");e.parents(".jClever-element").append(b.errorTemplate),e.is(":checked")&&(e.next(".jClever-element-checkbox-twins").addClass("checked"),a("label[for="+f+"]").addClass("active")),a(c).attr("disabled")&&e.parents(".jClever-element").addClass("disabled"),e.on("change.jClever",function(){return!a(this).attr("disabled")&&void(a(this).is(":checked")?(e.next(".jClever-element-checkbox-twins").addClass("checked"),a("label[for="+f+"]").addClass("active")):(e.next(".jClever-element-checkbox-twins").removeClass("checked"),a("label[for="+f+"]").removeClass("active")))}),e.next(".jClever-element-checkbox-twins").on("click",function(){var b=a(this);b.prev("input[type=checkbox]").is(":checked")?b.prev("input[type=checkbox]").removeAttr("checked").prop("checked",!1):b.prev("input[type=checkbox]").attr("checked","checked").prop("checked",!0),b.prev("input[type=checkbox]").trigger("change")}),e.parent(".jClever-element").on("keydown.jClever",function(b){var c=a(this).find("input[type=checkbox]");switch(b.keyCode){case 32:c.is(":checked")?c.removeAttr("checked").prop("checked",!1):c.attr("checked","checked").prop("checked",!0),c.trigger("change");break;default:return}return!1}),e.parent(".jClever-element").focus(function(){a(this).addClass("focused")}).blur(function(){a(this).removeClass("focused")})}},radioActivate:function(c,d){if(!a(c).hasClass("jc-ignore")){var e=a(c).wrap('<div class="jClever-element" tabindex="'+d+'">').addClass("hidden").after('<span class="jClever-element-radio-twins"><span class="jClever-element-radio-twins-element"></span><span class="jClever-element-radio-twins-color"></span></span>'),f=e.attr("id");if(e.parents(".jClever-element").append(b.errorTemplate),e.is(":checked")&&(e.next(".jClever-element-radio-twins").addClass("checked"),a("label[for="+f+"]").addClass("active")),a(c).attr("disabled"))return void e.parents(".jClever-element").addClass("disabled");e.on("change.jClever, updates.jClever",function(b){if(a(this).attr("disabled"))return!1;var d=a(this);d.is(":checked")?(d.next(".jClever-element-radio-twins").addClass("checked"),a("label[for="+f+"]").addClass("active")):(d.next(".jClever-element-radio-twins").removeClass("checked"),a("label[for="+f+"]").removeClass("active")),"updates"!=b.type&&a('input:radio[name="'+a(c).attr("name")+'"]').not(a(this)).each(function(){var b=a(this);b.removeAttr("checked").prop("checked",!1).next(".jClever-element-radio-twins").removeClass("checked"),a("label[for="+a(this).attr("id")+"]").removeClass("active")})}),e.next(".jClever-element-radio-twins").on("click",function(){var b=a(this);b.prev("input[type=radio]").is(":checked")?b.prev("input[type=radio]").attr("checked","checked").prop("checked",!0):b.prev("input[type=radio]").attr("checked","checked").prop("checked",!0),b.prev("input[type=radio]").trigger("change")}),e.parent(".jClever-element").on("keydown.jClever",function(b){var c=a(this).find("input[type=radio]");switch(b.keyCode){case 32:c.is(":checked")?c.removeAttr("checked").prop("checked",!1):c.attr("checked","checked").prop("checked",!0),c.trigger("change");break;default:return}return!1}),e.parent(".jClever-element").focus(function(){a(this).addClass("focused")}).blur(function(){a(this).removeClass("focused")})}},submitActivate:function(b,c){if(!a(b).hasClass("jc-ignore")){var d=a(b).attr("value"),e=a(b).replaceWith('<button type="'+b.type+'" name="'+b.name+'" id="'+b.id+'"  class="styled '+b.className+'" value="'+d+'"><span><span><span>'+d+"</span></span></span>");elementHash=md5(j.elementToString(e)),e.data("jCleverHash",elementHash)}},fileActivate:function(c,d){if(!a(c).hasClass("jc-ignore")){a(c).wrap('<div class="jClever-element" tabindex="'+d+'"><div class="jClever-element-file">').addClass("hidden-file").after('<span class="jClever-element-file-name"><span><span></span></span></span><span class="jClever-element-file-button"><span><span>'+b.fileUploadText+"</span></span></span>").wrap('<div class="input-file-helper">'),a(c).parents(".jClever-element").append(b.errorTemplate);var e=a(c).parents(".jClever-element").find(".jClever-element-file-name>span>span");a(c).on("change.jClever",function(){var b=a(this).val();b=b.split("\\"),b=b[b.length-1],e.text(b)}),a(c).parents(".jClever-element").on("keydown.jClever",function(b){switch(b.keyCode){case 32:a(c).trigger("click");break;default:return}return!1}),a(c).parents(".jClever-element").focus(function(){a(this).addClass("focused")}).blur(function(){a(this).removeClass("focused")})}},inputActivate:function(c,d){a(c).hasClass("jc-ignore")||(a(c).wrap('<div class="jClever-element"><div class="jClever-element-input"><div class="jClever-element-input"><div class="jClever-element-input">'),a(c).parents(".jClever-element").append(b.errorTemplate),a(c).on("focusin.jClever",function(){a(this).parents(".jClever-element").addClass("focused")}),a(c).on("focusout.jClever",function(){a(this).parents(".jClever-element").removeClass("focused")}))},textareaActivate:function(c,d){a(c).hasClass("jc-ignore")||(a(c).wrap('<div class="jClever-element"><div class="jClever-element-textarea"><div class="jClever-element-textarea"><div class="jClever-element-textarea">'),a(c).parents(".jClever-element").append(b.errorTemplate),a(c).on("focusin.jClever",function(){a(this).parents(".jClever-element").addClass("focused")}),a(c).on("focusout.jClever",function(){a(this).parents(".jClever-element").removeClass("focused")}))},elementAdd:function(d,e,f){if("undefined"==typeof f)return!1;switch(e){case"text":case"password":if(!b.applyTo.input)break;"string"!=typeof d?d.each(function(){j.inputActivate(a(this),i),a(this).data("jclevered",!0)}):(j.inputActivate(d,i),a(d).data("jclevered",!0));break;case"file":if(!b.applyTo.file)break;"string"!=typeof d?d.each(function(){j.fileActivate(a(this),i),a(this).data("jclevered",!0)}):(j.fileActivate(d,i),a(d).data("jclevered",!0));break;case"select":if(!b.applyTo.select)break;j.selectActivate(d,f.innerCounter,i);var g=a.extend(c,f.selectCollection);f.selectCollection=g,f.innerCounter--,a(d).data("jclevered",!0);break;case"checkbox":if(!b.applyTo.checkbox)break;"string"!=typeof d?d.each(function(){j.checkboxActivate(a(this),i),a(this).data("jclevered",!0)}):(j.checkboxActivate(d,i),a(d).data("jclevered",!0));break;case"radio":if(!b.applyTo.radio)break;j.radioActivate(d,i),a(d).data("jclevered",!0);break;case"textarea":if(!b.applyTo.textarea)break;"string"!=typeof d?d.each(function(){j.textareaActivate(a(this),i),a(this).data("jclevered",!0)}):(j.textareaActivate(d,i),a(d).data("jclevered",!0));break;case"submit":case"reset":if(!b.applyTo.button)break;"string"!=typeof d?d.each(function(){j.submitActivate(this,i),a(this).data("jclevered",!0)}):(j.submitActivate(d,i),a(d).data("jclevered",!0))}return f},elementDisable:function(b){"string"==typeof b&&(b=a(b)),b.attr("disabled","disabled").closest(".jClever-element").addClass("disabled")},elementEnable:function(b){"string"==typeof b&&(b=a(b)),b.removeAttr("disabled").closest(".jClever-element").removeClass("disabled")},themeSet:function(c){a(this).removeClass(b.selfClass).addClass(c),b.selfClass=c}},k={},l=this,m=10,n=null,o=null,p=function(){if(!a(this).hasClass("clevered"))return a(this).addClass("clevered").addClass(b.selfClass),j.init(this),k={selectCollection:c,innerCounter:h,refresh:function(a){j.refresh(a)},destroy:function(a){j.destroy(a)},reset:function(a){j.reset(a)},selectOpen:function(a){j.selectOpen(a)},selectClose:function(a){j.selectClose(a)},selectSetPosition:function(b,c){a(b).length&&j.selectSetPosition(a(b),c)},selectAdd:function(a){j.selectAdd(a)},checkboxSetState:function(b,c){return!!a(b).length&&void j.checkboxSetState(a(b),c)},radioSetState:function(b,c){return!!a(b).length&&void j.radioSetState(a(b),c)},updateFromHTML:function(b,c){return!!a(b).length&&void j.updateFromHTML(a(b),c)},elementAdd:function(a,b,c){return j.elementAdd(a,b,c)},elementDisable:function(a){return j.elementDisable(a)},elementEnable:function(a){return j.elementEnable(a)},themeSet:function(b){return j.themeSet.call(a(this),b)}},c={},a.data(a(this).get(0),"publicApi",k),!0};return b.autoInit&&a(document).on("onDomChange.jClever",function(c){"undefied"!=typeof o&&null!=o&&clearTimeout(o),o=setTimeout(function(){var c="form";""!=b.autoinitSelector&&(c=b.autoinitSelector),a("body").find(c).each(function(){p.call(this),a.inArray(this,l)==-1&&l.push(this)})},m)}),b.autoTracking&&a(document).on("onDomChange.jClever",function(a){"undefied"!=typeof n&&null!=n&&clearTimeout(n),n=setTimeout(function(){l.each(function(){j.refresh(this)})},m)}),this.each(function(){p.call(this)})},a.fn.jCleverAPI=function(b){if(this.length>1)return!1;for(var c=a.data(a(this).get(0),"publicApi"),d=[],e=1;e<arguments.length;e++)d[e-1]=arguments[e];if("function"==typeof c[b]){1==arguments.length?d=new Array(a(this)):d[arguments.length-1]=c;var f=c[b].apply(a(this),d);return"object"==typeof f?a.data(a(this).get(0),"publicApi",f):f=c,f}return c[b]},jQuery.jClever=!0,a(document).mousedown(function(b){0===a(b.target).parents(".jClever-element-select-wrapper").length&&a(".jClever-element-select-list-wrapper:visible").hide()})}(jQuery);




jQuery(function(){
	
	jQuery(window).scroll(function(){
		if(jQuery(window).scrollTop() > 210){
		    jQuery('.fix-menu').addClass('fixed');	
		}else{
			jQuery('.fix-menu').removeClass('fixed');	
		}
	});
	
	jQuery('.bgblack').on('click',function(){
		jQuery(this).fadeOut();
		jQuery('.popup').hide();
		jQuery('#youtubevideo').remove();
		if (typeof(player) !== 'undefined') {player.Stop();}

	});
	
	jQuery('.close-icon').on('click',function(){
		jQuery(this).parents('.popup').hide();
		jQuery('.bgblack').fadeOut();
		jQuery('#youtubevideo').remove();
		if (typeof(player) !== 'undefined') {player.Stop();}
		return false;
	});
	
    jQuery('.logout-button').on('click',function(){
		jQuery('#login-form').submit();
		return false;
	});
	
	jQuery(window).scroll(function(){
		if(jQuery('.counter-wrapper').length){
		counter = jQuery('.counter-wrapper').offset().top;
		if(jQuery(window).scrollTop() >= counter - 500){
			
			jQuery('.count-num.cn-1 span').animate({ num: 16 - 1 }, {
			  duration: 2000,
			  step: function (num){
				  this.innerHTML = (num + 1).toFixed(0);
			}
            });
			
			jQuery('.count-num.cn-2 span,.count-num.cn-3 span').animate({ num: 100 - 2 }, {
			  duration: 2000,
			  step: function (num){
				  this.innerHTML = (num + 2).toFixed(0);
			}
            });
			
			jQuery('.count-num.cn-4 span').animate({ num: 10 - 1 }, {
			  duration: 2000,
			  step: function (num){
				  this.innerHTML = (num + 1).toFixed(0);
			}
            });
		}
		}
	});
	
	jQuery('.moduletable_mainmenu li').hover(
	    function(){
			 jQuery(this).find('.submenu').hide();  
		     jQuery(this).find('.submenu').css('top',75);
			 jQuery(this).find('.submenu').show();
			 jQuery(this).find('.submenu').stop().animate({'top':'70px'},500);
	    },
		function(){
			jQuery(this).find('.submenu').fadeOut();
		}
	);
	
	jQuery('.tab-links a').on('click',function(){
		jQuery('.tab-links a').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('.tabs .tab').hide();
		jQuery('.tab').eq(jQuery(this).parents('li').index()).show();
		return false;
	});
	
	jQuery('.comment-add-link').on('click',function(){
		if(!jQuery(this).hasClass('active')){
			jQuery(this).addClass('active')
		    jQuery('.comment-form').slideDown();	
		}else{
			jQuery(this).removeClass('active')
		    jQuery('.comment-form').slideUp();
		}
		return false;
	});
	
	jQuery('.jClever').jClever(
    {
        applyTo: {
            select: true,
            checkbox: true,
            radio: true,
            button: false,
            file: true,
            input: false,
            textarea: true
        }
    }                
    );
	
	jQuery('.vm-list').on('click',function(){
		if(!jQuery(this).hasClass('active')){
			jQuery('.view-type a').removeClass('active');
			jQuery(this).addClass('active');
			jQuery('.product-list').addClass('clist');
		}
		return false;
	});
	
	jQuery('.vm-grid').on('click',function(){
		if(!jQuery(this).hasClass('active')){
			jQuery('.view-type a').removeClass('active');
			jQuery(this).addClass('active');
			jQuery('.product-list').removeClass('clist');
		}
		return false;
	});
	
	jQuery('.ask-question').on('click',function(){
		jQuery('#question-popup').css('left','auto').css('right','-600px');
		jQuery('.bgblack').fadeIn();
		jQuery('#question-popup').show().stop().animate({'left':'50%'},1000);
		return false;
	});
	
	jQuery('.calculator-tabs .tabs-links li').on('click',function(){
		activeTab = jQuery(this).data('id');
		
		type = 'commerce';
		if(activeTab == 'power-privat'){
			type = 'private';
		}	
		
		if(type == 'commerce'){
			jQuery('.power-label').hide();
			jQuery('.power-num').hide();
		}
		
		if(type == 'private'){
			jQuery('.power-label').show();
			jQuery('.power-num').show();
		}
		
		jQuery('.calculator-tabs .tabs-links li').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('.power-block .range').hide();
		jQuery('.power-block .range').eq(jQuery(this).index()).show();
		jQuery('.station-price').html(jQuery('.'+jQuery(this).data('id') + ' .first').data('price'));
		jQuery('input[name=station_id]').val(jQuery('.'+jQuery(this).data('id') + ' .first').data('id'));
		jQuery('.'+jQuery(this).data('id')).find('.power-item').removeClass('active');
		jQuery('.'+jQuery(this).data('id')).find('.power-field').removeClass('active');
		jQuery('.'+jQuery(this).data('id') + ' .first').addClass('active');
	});
	
	jQuery('.range .power-item').on('click',function(){
		jQuery(this).parents('.range').find('.power-item').removeClass('active');
		jQuery(this).parents('.range').find('.power-field').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('.station-price').html(jQuery(this).data('price'));
		jQuery('input[name=station_id]').val(jQuery(this).data('id'))
		jQuery('input[name=user_value]').val('');
		return false;
	});
	
	function getPrice(data){
		pattern = new RegExp(/^[0-9]+$/i);
		if(data && pattern.test(data) && data > 5){
		  jQuery.ajax({
			  type: "POST",
			  url: "index.php?option=com_solarcalculator&task=station.getAjaxCalculate&power="+data,
			  cache: false,
			  dataType:'html',
			  success: function( response ){
				  jQuery('.station-price').html( response );
			  }
		  });
		}
		
	}
	
	jQuery('.power-field input').on('click',function(){
		value = jQuery(this).val();
		jQuery('input[name=user_value]').val(value);
		jQuery('.power-item').removeClass('active');
		jQuery(this).parents('.power-field').addClass('active');
		getPrice(value);
	});
	
	jQuery('.power-field input').on('keyup',function(){
		value = jQuery(this).val();
		jQuery('input[name=user_value]').val(value);
		getPrice(value);
	});
	
	var calculatorState = false;
	jQuery('.calculate-link').on('click',function(){
		region = jQuery('#calculator select[name=region]').val() * 1;
		userPower = jQuery('#calculator input[name=power_num]').val();
		pattern = new RegExp(/^[0-9]+$/i);
		error = true; 
		
		activeTab = jQuery('.tabs-links li.active').data('id');
		type = 'commerce';
		if(activeTab == 'power-privat'){
			type = 'private';
		}
				
		jQuery('.calc-message').hide();
		
		if(!region){
			jQuery('.msg1').show();
			error = false;
		}
		if(!userPower && error == true && type == 'private'){
			jQuery('.msg2').show();
			error = false;
		}
		
		if(!pattern.test(userPower) && error == true && type == 'private'){
			jQuery('.msg3').show();
			error = false;
		}
		console.log(calculatorState);
		if(error == true && calculatorState == false){
			calculatorState = true;
			jQuery('.calculate-result').html('');
			jQuery('.calculate-result-text').html('');
			jQuery('.calculate-popup-buttons').show();
			jQuery('.calculator-loader').fadeIn();
			var formData = jQuery('#calculator').serializeArray();
			formData.push({name:'option',value:'com_solarcalculator'},{name:'task',value:'station.getAjaxCalculator'},{name:'type',value:type});
			jQuery.ajax({
			   type:'POST',
				data   : formData,
				success: function (response) {
                    jQuery('.calculator-loader').fadeOut();
					jQuery('.calculate-result-text').html('');
					jQuery('.bgblack').fadeIn();
					jQuery('#calculator-popup').css('top',jQuery(window).scrollTop()+20).fadeIn();
					
					result =  JSON.parse(response);
					
					jQuery('.calculate-title').html(result['title']);
					
					var list = Object.keys(result['calculate']).length;
					var timeout = 0;
					var state = 1;
					for(var i = 1; i <= list; i++) {
						setTimeout(
							(function (i){
								return function() {
									jQuery('.calculate-result').append(result['calculate']['line'+i]).children(':last').hide().fadeIn(1500);
									if(i == list){
									calculatorState = false;	
									}
								}
							})(i)
							, 
							timeout
						);
						timeout += 1500;
						
					};
                    

					jQuery('.calculate-top').html(result['inputData']);
					
					if(result['textOutput']){
					    jQuery('.calculate-result-text').html(result['textOutput']);
					}
					if(!result['textOutput'] && !result['commerce']){
						jQuery('.calculate-popup-buttons').hide();
					}
					
				}	
		  });
		}
		return false;
	});
	
	function addResultLine(line){
		jQuery('.calculate-result').append(line);
	}
	
	jQuery('.print-calculate').on('click',function(){
		region = jQuery('#calculator select[name=region]').val() * 1;
		userPower = jQuery('#calculator input[name=power_num]').val();
		id = jQuery('#calculator input[name=station_id]').val();
		custom = jQuery('#calculator input[name=user_value]').val();
		
		activeTab = jQuery('.tabs-links li.active').data('id');
		type = 'commerce';
		if(activeTab == 'power-privat'){
			type = 'private';
		}
		
		window.open('index.php?option=com_solarcalculator&view=station&format=pdf&region='+region+'&power='+userPower+'&id='+id+'&custom='+custom+'&type='+type,'_blank');

		return false;
	});
	
	
	jQuery('.send-calculate').on('click',function(){
		jQuery('#calculator-popup').hide();
		jQuery('#calculate-send-info').fadeIn();
	});
	
	jQuery('.send-calculate-link').on('click',function(){
		jQuery('.cssend-loader').fadeIn();
		email = jQuery('#calculate-send-info input[type=email]').val();
		error = true;
	    pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		
		if(!email){
		   jQuery('#calculate-send-info').find('.popup-message').html('<div class="pm-inner">'+Joomla.JText._('MOD_SOLARCALCULATE_ERROR_FILL_EMAIL')+'</div>');	
		   error = false;
		}
		
		if(!pattern.test(email)){
		   jQuery('#calculate-send-info').find('.popup-message').html('<div class="pm-inner">'+Joomla.JText._('MOD_SOLARCALCULATE_ERROR_FILL_CORRECT_EMAIL')+'</div>');	
		   error = false;
		}
		
		activeTab = jQuery('.tabs-links li.active').data('id');
		type = 'commerce';
		if(activeTab == 'power-privat'){
			type = 'private';
		}
		
		
		if(error){
            var formData = jQuery('#calculator').serializeArray();
			formData.push({name:'option',value:'com_solarcalculator'},{name:'task',value:'station.getAjaxSendemail'},{name:'type',value:type},{name:'email',value:email});
			jQuery.ajax({
			   type:'POST',
				data   : formData,
				success: function (response) {
					result =  JSON.parse(response);
                    jQuery('.cssend-loader').fadeOut();
					if(result['message'] == 1){
					   	message = Joomla.JText._('MOD_SOLARCALCULATE_MESSAGE_SENT');
					   
					}else{
						message = Joomla.JText._('MOD_SOLARCALCULATE_ERROR_MESSAGE_NOT_SENT');
					}
					jQuery('#calculate-send-info').hide();
					showMessage(message);	
					setTimeout(popupHide, 5000);
				}
				});
		}
	});
	
	jQuery('.order-calculate-link').on('click',function(){

		error = true;
	    pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		
		if(!email){
		   jQuery('#calculate-order').find('.popup-message').html('<div class="pm-inner">'+Joomla.JText._('MOD_SOLARCALCULATE_ERROR_FILL_EMAIL')+'</div>');	
		   error = false;
		}
		
		if(!pattern.test(email)){
		   jQuery('#calculate-order').find('.popup-message').html('<div class="pm-inner">'+Joomla.JText._('MOD_SOLARCALCULATE_ERROR_FILL_CORRECT_EMAIL')+'</div>');	
		   error = false;
		}
		
		if(!jQuery('#calculate-order input[name=name]').val()){
		   jQuery('#calculate-order').find('.popup-message').html('<div class="pm-inner">'+Joomla.JText._('MOD_SOLARCALCULATE_ERROR_FILL_NAME')+'</div>');	
		   error = false;
		}
		
		if(error){
            var formData = jQuery('#calculator').serializeArray();
			formData.push({name:'option',value:'com_solarcalculator'},{name:'task',value:'station.getAjaxSendorder'},{name:'type',value:type});
			jQuery.ajax({
			   type:'POST',
				data   : formData,
				success: function (response) {
					result =  JSON.parse(response);
					if(result['message'] == 1){
					   	message = Joomla.JText._('MOD_SOLARCALCULATE_MESSAGE_SENT');
					   
					}else{
						message = Joomla.JText._('MOD_SOLARCALCULATE_ERROR_MESSAGE_NOT_SENT');
					}
					jQuery('#calculate-order').hide();
					showMessage(message);	
					setTimeout(popupHide, 5000);
				}
				});
		}
		
	});
	
	jQuery('.order-calculate').on('click',function(){
		jQuery('#calculator-popup').hide();
		jQuery('#calculate-order').show();
	});
	
	
	function showMessage(message){
		jQuery('.message-text').html('<div class="text-inner">'+message+'</div>');
	    jQuery('#message-popup').show();
		jQuery('.bgblack').fadeIn();	
	}
	
	function popupHide(){
		jQuery('#message-popup').fadeOut();
		jQuery('.bgblack').fadeOut();
	}
	
	jQuery('.solut-order').on('click',function(){
    	jQuery('.bgblack').fadeIn();
		jQuery('#productorder').fadeIn();
		jQuery('#productorder input[name=field8]').val(jQuery(this).parents('.product').find('.vm-product-name').html());
		return false;
    });
	
	
});

"undefined"==typeof Virtuemart&&(Virtuemart={}),jQuery(function(a){Virtuemart.customUpdateVirtueMartCartModule=function(a,b){var c=this;c.el=jQuery(".vmCartModule"),c.options=jQuery.extend({},Virtuemart.customUpdateVirtueMartCartModule.defaults,b),c.init=function(){jQuery.ajaxSetup({cache:!1}),jQuery.getJSON(Virtuemart.vmSiteurl+"index.php?option=com_virtuemart&nosef=1&view=cart&task=viewJS&format=json"+Virtuemart.vmLang,function(a,b){c.el.each(function(b,c){a.totalProduct>0&&(jQuery(c).find(".vm_cart_products").html(""),jQuery.each(a.products,function(b,d){jQuery(c).find(".hiddencontainer .vmcontainer .product_row").clone().appendTo(jQuery(c).find(".vm_cart_products")),jQuery.each(d,function(a,b){jQuery(c).find(".vm_cart_products ."+a).last().html(b)}),jQuery(c).find(".product-cart-image").last().html('<img src="'+a.products[b].image+'" alt="'+a.products[b].product_title+'" />')})),jQuery(c).find(".hiddencontainer .vmcontainer .total-bottom").clone().appendTo(jQuery(c).find(".vm_cart_products")),jQuery(c).find(".show_cart").html(a.cart_show),jQuery(c).find(".total_products").html(a.totalProductTxt),jQuery(c).find(".total").html(a.billTotal)})})},c.init()},Virtuemart.customUpdateVirtueMartCartModule.defaults={name1:"value1"}}),jQuery(document).ready(function(a){jQuery(document).off("updateVirtueMartCartModule","body",Virtuemart.customUpdateVirtueMartCartModule),jQuery(document).on("updateVirtueMartCartModule","body",Virtuemart.customUpdateVirtueMartCartModule)});



jQuery(document).ready(function() {
	jQuery('.slide-btn-order').on('click',function(){
		jQuery('.moduletable_ordercalc input[name=typest]').val(jQuery(this).data('type'));
		jQuery('.moduletable_ordercalc').fadeIn();
		jQuery('.bgblack').fadeIn();	
	});
	
	function showMessage(message){
		jQuery('.message-text').html('<div class="text-inner">'+message+'</div>');
	    jQuery('#message-popup').show();
		jQuery('.bgblack').fadeIn();	
	}
	
	function popupHide(){
		jQuery('#message-popup').fadeOut();
		jQuery('.bgblack').fadeOut();
	}
	
	jQuery('.vssend-button').click(function() {
		formid = jQuery(this).attr('id');
		var valid = true;
		var message = '';
		var require = 0;
		var requireText = '';

		pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		
		jQuery('.'+formid+' .require').each(function(){
			if (!jQuery(this).val()){
				require = require + 1;
				requireText  += jQuery(this).attr('data-title')+',';
			}
		});
		
		if(require){
			message = 'Пожалуйста, заполните обязательные поля: '+ '<span>' + requireText.substring(0, requireText.length - 1) + '</span>';
			valid = false;	
		}
		
        if(valid){
			if(jQuery('.'+formid+' input[type=email]').length && !pattern.test(jQuery('.'+formid+' input[type=email]').val())){
				message = 'Пожалуйста, введите корректный e-mail';
				valid = false;
			}
		}
		
		
		if (valid){

			request = {
				      'option' : 'com_ajax',
					  'module' : 'vsorderformj3',
					  'format' : 'raw'
				};
			var formData = jQuery('.'+formid).serializeArray();

			formData.push({name:'option',value:'com_ajax'},{name:'module',value:'vsorderformj3'},{name:'format',value:'raw'});
			jQuery('.popup-loader').show();
			jQuery.ajax({
			    type:'POST',
				data   : formData,
				success: function (response) {
					jQuery('.popup-loader').hide();
					jQuery('.popup').hide();
					showMessage(response);	
					setTimeout(popupHide, 5000);
				}	
			});
			
		}else{
			if(jQuery('.'+formid+' input[name=popup]').val()){
				jQuery('.'+formid+' .popup-msg').html('<div class="msg-inner">'+message+'</div>');
			}else{
			   showMessage(message);	
			}
		}
		return false;
	});
});