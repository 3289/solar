<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Solarcalculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Solarcalculator model.
 *
 * @since  1.6
 */
class SolarcalculatorModelSetting extends JModelAdmin
{
	public function getForm ($data = array(), $loadData = true)
	{
		jimport('joomla.form.form');
		JForm::addFieldPath('JPATH_ADMINISTRATOR/components/com_solarcalculator/models/fields');

		// Get the form.
		$form = $this->loadForm('com_solarcalculator.setting', 'setting', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		

		return $form;
	
	}
}
