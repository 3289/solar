<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Solarcalculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Solarcalculator records.
 *
 * @since  1.6
 */
class SolarcalculatorModelSettings extends JModelList
{
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_solarcalculator');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		$db	= $this->getDbo();
		$query	= $db->getQuery(true);

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}
	
	public function getForm ($data = array(), $loadData = true)
	{
		jimport('joomla.form.form');
		JForm::addFieldPath('JPATH_ADMINISTRATOR/components/com_solarcalculator/models/fields');
		
		$data['setting_insolation_center'] = 150;

		// Get the form.
		$form = $this->loadForm('com_solarcalculator.setting', 'setting', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		

		return $form;
	
	}
	
	public function getTable($type = 'Setting', $prefix = 'SolarcalculatorTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	public function saveSettings($data){
	   if ($data){
		   $settings = json_encode($data);
		   
		   $db = JFactory::getDbo();
           $query = $db->getQuery(true);
		   
		   $columns = array('settings');
			 
		   $values = array($db->quote($settings));
			 
			$query
				->update($db->quoteName('#__solarcalculator_settings'))
				->set($db->quoteName('settings') . '=' . $db->quote($settings))
				->where('type="settings"');
			 
			$db->setQuery($query);
 
            $result = $db->execute();
	   }
	   
	}
	
	public function getSettings(){
	   $db = JFactory::getDbo(); 
	   $query = $db->getQuery(true);
	   
	   $query->select('settings');
	   $query->from($db->quoteName('#__solarcalculator_settings'));
	   $query->where($db->quoteName('type') . ' LIKE '. $db->quote('settings'));
	   $db->setQuery($query);
	   $result = $db->loadObjectList();
	   
	   return $result;
   }	
}
