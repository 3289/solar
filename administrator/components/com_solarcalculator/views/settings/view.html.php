<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Solarcalculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Solarcalculator.
 *
 * @since  1.6
 */
class SolarcalculatorViewSettings extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;
	
	protected $form;
	

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->form  = $this->get('Form');
		
		$model = $this->getModel('settings');
		
		$result = $model->getSettings();
		$data = json_decode($result[0]->settings);
		
		$this->form->bind($data);
		

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		SolarcalculatorHelpersSolarcalculator::addSubmenu('settings');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = SolarcalculatorHelpersSolarcalculator::getActions();

		JToolBarHelper::title(JText::_('COM_SOLARCALCULATOR_TITLE_SETTINGS'), 'settings.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/setting';
		
		// If not checked out, can save the item.
		if (!$checkedOut && ($canDo->get('core.edit') || ($canDo->get('core.create'))))
		{
			JToolBarHelper::apply('settings.apply', 'JTOOLBAR_APPLY');
		}


		
		
	

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_solarcalculator');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_solarcalculator&view=settings');

		$this->extra_sidebar = '';
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
		);
	}
}
