<?php
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<style type="text/css">
 .inputshort{
	 width:80px;
	 }
	 
 .controls.short{
	 margin-left:0;
	 }	 
</style>
<?php if(!empty( $this->sidebar)): ?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
<?php else: ?>
<div id="j-main-container">
<?php endif;?>
<form action="index.php?option=com_solarcalculator&view=settings" method="post" id="adminForm" name="adminForm">
	
	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_SOLARCALCULATOR_TITLE_SETTINGS', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
                      <legend><?php echo JText::_('COM_SOLARCALCULATOR_TITLE_INSOLATION')?></legend>
                  <div class="control-group">
                      <div class="control-label"><?php echo $this->form->getLabel('setting_insolation_center'); ?></div>
                      <div class="controls"><?php echo $this->form->getInput('setting_insolation_center'); ?></div>
                  </div>
                  
                  <div class="control-group">
                      <div class="control-label"><?php echo $this->form->getLabel('setting_insolation_north'); ?></div>
                      <div class="controls"><?php echo $this->form->getInput('setting_insolation_north'); ?></div>
                  </div>
                  
                  <div class="control-group">
                      <div class="control-label"><?php echo $this->form->getLabel('setting_insolation_south'); ?></div>
                      <div class="controls"><?php echo $this->form->getInput('setting_insolation_south'); ?></div>
                  </div>

                   <legend><?php echo JText::_('COM_SOLARCALCULATOR_TITLE_CALCULATOR')?></legend>
                   <div class="control-group">
                      <div class="control-label"><?php echo $this->form->getLabel('setting_price_units'); ?></div>
                      <div class="controls"><?php echo $this->form->getInput('setting_price_units'); ?></div>
                   </div>
                   
                   <div class="control-group">
                      <div class="control-label"><?php echo $this->form->getLabel('setting_green_price_private'); ?></div>
                      <div class="controls"><?php echo $this->form->getInput('setting_green_price_private'); ?></div>
                   </div>
                   
                   <div class="control-group">
                      <div class="control-label"><?php echo $this->form->getLabel('setting_green_price_commerce'); ?></div>
                      <div class="controls"><?php echo $this->form->getInput('setting_green_price_commerce'); ?></div>
                   </div>
                   <div class="control-group">
                      <div class="control-label"><?php echo $this->form->getLabel('setting_station_price_stable'); ?></div>
                      <div class="controls"><?php echo $this->form->getInput('setting_station_price_stable'); ?></div>
                   </div> 
                   
                   <div class="control-group">
                      <div class="control-label"><?php echo $this->form->getLabel('setting_station_incoming_tax'); ?></div>
                      <div class="controls"><?php echo $this->form->getInput('setting_station_incoming_tax'); ?></div>
                   </div>
                   
                   <div class="control-group">
                      <div class="control-label"><?php echo $this->form->getLabel('setting_station_war_tax'); ?></div>
                      <div class="controls"><?php echo $this->form->getInput('setting_station_war_tax'); ?></div>
                   </div>
					 
				</fieldset>
			</div>
		</div>
     </div>   
    <input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
</form>
</div>