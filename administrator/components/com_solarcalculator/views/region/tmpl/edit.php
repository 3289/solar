<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Solarcalculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_solarcalculator/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'region.cancel') {
			Joomla.submitform(task, document.getElementById('region-form'));
		}
		else {
			
			if (task != 'region.cancel' && document.formvalidator.isValid(document.id('region-form'))) {
				
				Joomla.submitform(task, document.getElementById('region-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_solarcalculator&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="region-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_SOLARCALCULATOR_TITLE_REGION', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

					 <div class="control-group">
                          <div class="control-label"><?php echo $this->form->getLabel('region_name'); ?></div>
                          <div class="controls"><?php echo $this->form->getInput('region_name'); ?></div>
                      </div>
                      
                      <div class="control-group">
                          <div class="control-label"><?php echo $this->form->getLabel('region_situate'); ?></div>
                          <div class="controls"><?php echo $this->form->getInput('region_situate'); ?></div>
                      </div>
                      
                      <div class="control-group">
                          <div class="control-label"><?php echo $this->form->getLabel('order'); ?></div>
                          <div class="controls"><?php echo $this->form->getInput('order'); ?></div>
                      </div>
                      
                       <div class="control-group">
                          <div class="control-label"><?php echo $this->form->getLabel('lang'); ?></div>
                          <div class="controls"><?php echo $this->form->getInput('lang'); ?></div>
                      </div>
                      
                      <div class="control-group">
                          <div class="control-label"><?php echo $this->form->getLabel('published'); ?></div>
                          <div class="controls"><?php echo $this->form->getInput('published'); ?></div>
                      </div>
                      

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo $this->form->getInput('id'); ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value="region.edit"/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
