<?php
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php if(!empty( $this->sidebar)): ?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
<?php else: ?>
<div id="j-main-container">
<?php endif;?>
<form action="index.php?option=com_solarcalculator&view=stations" method="post" id="adminForm" name="adminForm">
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="2%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
            <th width="20%"><?php echo JText::_('COM_SOLARCALCULATOR_ID'); ?></th>
			<th width="30%">
				<?php echo JText::_('COM_SOLARCALCULATOR_STANTION') ;?>
			</th>
            <th width="20%">
				<?php echo JText::_('COM_SOLARCALCULATOR_TYPE'); ?>
			</th>
			<th width="10%">
				<?php echo JText::_('COM_SOLARCALCULATOR_PRICE'); ?>
			</th>
            <th width="13%">
            </th>
			
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php //echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : 
				    $link = JRoute::_('index.php?option=com_solarcalculator&task=station.edit&id=' . $row->id);
				?>
 
					<tr>
                        <td>
							<?php echo JHtml::_('grid.id', $i, $row->id); ?>
						</td>
						<td>
							<?php echo $this->pagination->getRowOffset($i); ?>
						</td>
						<td>
                            <a href="<?php echo $link; ?>" title="<?php echo $row->station_value.' '.JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($row->station_value_units)); ?>">
							<?php 
							    echo $row->station_value.' '.JText::_('COM_SOLARCALCULATOR_STANTION_UNIT_'.strtoupper($row->station_value_units)); 	
							?>
                            </a>
						</td>
                        <td align="center">
							<?php  
							   switch($row->station_type){
								   case 1: echo JText::_('COM_SOLARCALCULATOR_STANTION_TYPE_PRIVATE');break;
								   case 2: echo JText::_('COM_SOLARCALCULATOR_STANTION_TYPE_COMMERCE');break;
							   }
							?>
						</td>
                        <td>
                            <?php 
							    echo $row->station_price.' '.JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_'.strtoupper($row->station_price_unit));	
							?>
                        </td>
						<td align="center">
							<?php echo JHtml::_('jgrid.published', $row->published, $i, 'stations.', true, 'cb'); ?>
						</td>
						
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
    <input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
</form>
</div>