<?php
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php if(!empty( $this->sidebar)): ?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
<?php else: ?>
<div id="j-main-container">
<?php endif;?>
<form action="index.php?option=com_solarcalculator&view=regions" method="post" id="adminForm" name="adminForm">
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="2%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
            <th width="20%"><?php echo JText::_('COM_SOLARCALCULATOR_ID'); ?></th>
			<th width="40%">
				<?php echo JText::_('COM_SOLARCALCULATOR_REGION_NAME') ;?>
			</th>
            <th width="20%">
				<?php echo JText::_('COM_SOLARCALCULATOR_REGION_SITUATE'); ?>
			</th>
			<th width="5%">
				<?php echo JText::_('COM_SOLARCALCULATOR_REGION_LANG'); ?>
			</th>
            <th width="13%">
            </th>
			
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="6">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>

			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : 
				    $link = JRoute::_('index.php?option=com_solarcalculator&task=region.edit&id=' . $row->id);
				?>
 
					<tr>
                        <td>
							<?php echo JHtml::_('grid.id', $i, $row->id); ?>
						</td>
						<td>
							<?php echo $this->pagination->getRowOffset($i); ?>
						</td>
						<td>
                            <a href="<?php echo $link; ?>" title="<?php  echo $row->region_name;?>">
							<?php 
							    echo $row->region_name; 	
							?>
                            </a>
						</td>
                        <td align="center">
							<?php  
							   switch($row->region_situate){
								   case 'center': echo JText::_('COM_SOLARCALCULATOR_REGION_CENTER');break;
								   case 'north': echo JText::_('COM_SOLARCALCULATOR_REGION_NORTH');break;
								   case 'south': echo JText::_('COM_SOLARCALCULATOR_REGION_SOUTH');break;
							   }
							?>
						</td>
                        <td>
                            <?php if ($row->lang == ''):?>
								<?php echo JText::_('JDEFAULT'); ?>
							<?php elseif ($row->lang == '*') : ?>
								<?php echo JText::alt('JALL', 'language'); ?>
							<?php else : ?>
								<?php echo $row->language_title ? JHtml::_('image', 'mod_languages/' . $row->language_image . '.gif', $row->language_title, array('title' => $row->language_title), true) . '&nbsp;' . $this->escape($row->language_title) : JText::_('JUNDEFINED'); ?>
							<?php endif; ?>
                           
                        </td>
						<td align="center">
							<?php echo JHtml::_('jgrid.published', $row->published, $i, 'stations.', true, 'cb'); ?>
						</td>
						
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
    <input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
</form>
</div>