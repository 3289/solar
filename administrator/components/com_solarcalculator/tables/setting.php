<?php

// no direct access
defined('_JEXEC') or die('Restricted access');

class SolarcalculatorTableSetting extends JTable
{
	public function __construct(&$db) {
		parent::__construct('#__solarcalculator_settings', 'id', $db);
	}
	
	
	public function bind($array){
		$parameter = new JRegistry;
			$parameter->loadArray($array['params']);
			$array['params'] = (string)$parameter;
		return parent::bind($array, $ignore);
	}

	
}
