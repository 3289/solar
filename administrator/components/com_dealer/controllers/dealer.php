<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Dealer
 * @author     Andrey  Tkachenko <office@virtuas.net>
 * @copyright  2017 Andrey  Tkachenko
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Dealer controller class.
 *
 * @since  1.6
 */
class DealerControllerDealer extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'dealers';
		parent::__construct();
	}
	
	public function save(){
	   $jinput = JFactory::getApplication()->input;
	   $data =  $jinput->post->get('jform', '', 'filter');
	   JFactory::getApplication()->setUserState($this->option . '.edit.authorization.data', $data);
	   $this->saveDiler($data);

	}
	
	public function saveDiler($data){
		if(!$data['user_id']){
		   $this->addNewDiler($data);   	
		}else{
		   $this->updateDiler($data);
		}
	}
	
	public function addNewDiler($data){
	     $app            = JFactory::getApplication();
		 $context        = "$this->_option.edit.$this->_context";
		 $username = $data['username'];
	   
		 $model = $this->getModel();
		 
		 $checkUser = $model->checkUser($username);
		 
		 if(!$checkUser){
			 if(!$data['password']){
				 JError::raiseWarning(500, JText::_('COM_DEALER_ERROR_PASSWORD_EMPTY'));
			 }
			 
			 jimport('joomla.user.helper');
			 $udata = array(
				"name"=>$data['name'],
				"username"=>$username,
				"password"=>$data['password'],
				"password2"=>$data['password'],
				"email"=>$data['email'],
				"block"=>0,
				"accountType"=>1,
				"groups"=>array("2")
			);
			
			$user = new JUser;
			 
			//Write to database
			if(!$user->bind($udata)) {
				throw new Exception("Could not bind data. Error: " . $user->getError());
			}
			if (!$user->save()) {
				 throw new Exception("Could not save user. Error: " . $user->getError());
			}
					   
			if($user->id){
				if ($model->addDilerProfile($data,$user->id)){
					$this->setRedirect('index.php?option=com_dealer&view=dealers', JText::_('COM_DEALER_PROFILE_ADDED'));
				}else{
				   JError::raiseWarning(500, JText::_('COM_DEALER_ERROR_USER_PROFILE_NOTADDED')); 
				   $this->setRedirect('index.php?option=com_dealer&view=dealers');   
				}  
			}
			 
		 }else{
			 $app->setUserState($context.'.data', $data);
			 JError::raiseWarning(500, JText::_('COM_DEALER_ERROR_USER_AVAIBLE'));
			 $this->setRedirect('index.php?option=com_dealer&view=dealer&layout=edit');
		 }
	} 
	
	public function updateDiler($data){
		
	} 
}
