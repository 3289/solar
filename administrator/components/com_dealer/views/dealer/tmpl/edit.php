<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Dealer
 * @author     Andrey  Tkachenko <office@virtuas.net>
 * @copyright  2017 Andrey  Tkachenko
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_dealer/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'dealer.cancel') {
			Joomla.submitform(task, document.getElementById('dealer-form'));
		}
		else {
			
			if (task != 'dealer.cancel' && document.formvalidator.isValid(document.id('dealer-form'))) {
				
				Joomla.submitform(task, document.getElementById('dealer-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>
<style type="text/css">
.password{
	display:inline-block;
	margin-left:15px;
	}
	
.passvalue{
	display:inline-block;
	margin-left:15px;
	font-size:18px;
	}	
</style>
<form
	action="<?php echo JRoute::_('index.php?option=com_dealer&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="dealer-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_DEALER_TITLE_DEALER', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
                    
                    <div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
					</div>
                    
                    <div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('company_name'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('company_name'); ?></div>
					</div>
                    
                    <div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('username'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('username'); ?></div>
					</div>
                    
                     <div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('email'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('email'); ?></div>
					</div>
                    
                     <div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('password'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('password'); ?>
                        
                           <div class="password">
                                 <a href="#" class="genpass" id="genpass" data-character-set="a-z,A-Z,0-9" data-size="9"><?php echo JText::_('COM_DEALER_TITLE_DEALER_GENERATE_PASSWORD', true)?></a>
                                 <div class="passvalue"></div>
                            </div>
                        </div>
                        
					</div>
                    
                    <div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('block'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('block'); ?></div>
					</div>
                    
                     <div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('phone1'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('phone1'); ?></div>
					</div>
                    
                     <div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('phone2'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('phone2'); ?></div>
					</div>
                    
                    <div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('address'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('address'); ?></div>
					</div>
                    
                    <div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('note'); ?></div>
					</div>
                    
                    <div class="control-group">
						<div class="controls"><?php echo $this->form->getInput('user_id'); ?></div>
					</div>


					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
<script type="text/javascript">
function randString(id){
  var dataSet = jQuery(id).data('character-set').split(',');  
  var possible = '';
  if(jQuery.inArray('a-z', dataSet) >= 0){
    possible += 'abcdefghijklmnopqrstuvwxyz';
  }
  if(jQuery.inArray('A-Z', dataSet) >= 0){
    possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  }
  if(jQuery.inArray('0-9', dataSet) >= 0){
    possible += '0123456789';
  }
  if(jQuery.inArray('#', dataSet) >= 0){
    possible += '![]{}()%&*$#^<>~@|';
  }
  var text = '';
  for(var i=0; i < jQuery(id).data('size'); i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

jQuery('.genpass').on('click',function(){
	var pass = randString('#genpass');
	jQuery('.passvalue').html(pass);
	jQuery('#jform_password').val(pass);
	return false;
});

</script>