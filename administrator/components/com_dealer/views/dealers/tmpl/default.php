<?php
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php if(!empty( $this->sidebar)): ?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
<?php else: ?>
<div id="j-main-container">
<?php endif;?>
<form action="index.php?option=com_dealer&view=dealers" method="post" id="adminForm" name="adminForm">
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="2%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
            <th width="20%"><?php echo JText::_('COM_DEALER_ID'); ?></th>
			<th width="30%">
				<?php echo JText::_('COM_DEALER_NAME') ;?>
			</th>
            <th width="20%">
				<?php echo JText::_('COM_DEALER_COMPANY'); ?>
			</th>
			<th width="10%">
				<?php echo JText::_('COM_SOLARCALCULATOR_PRICE'); ?>
			</th>
            <th width="13%">
            <?php echo JText::_('COM_DEALER_STATE'); ?>
            </th>
			
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php //echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : 
				    $link = JRoute::_('index.php?option=com_dealer&task=dealer.edit&id=' . $row->user_id);
				?>
 
					<tr>
                        <td>
							<?php echo JHtml::_('grid.id', $i, $row->user_id); ?>
						</td>
						<td>
							<?php echo $this->pagination->getRowOffset($i); ?>
						</td>
						<td>
                            <a href="<?php echo $link; ?>" title="<?php echo $row->name?>">
							<?php 
							    echo $row->name; 	
							?>
                            </a>
						</td>
                        <td align="center">
							<?php  
							  echo $row->company_name;
							?>
						</td>
                        <td>
                            <?php 
							    echo $row->station_price.' '.JText::_('COM_SOLARCALCULATOR_STANTION_PRICE_UNIT_'.strtoupper($row->station_price_unit));	
							?>
                        </td>
						<td align="center">
							<?php echo JHtml::_('jgrid.published', $row->published, $i, 'stations.', true, 'cb'); ?>
						</td>
						
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
    <input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
</form>
</div>