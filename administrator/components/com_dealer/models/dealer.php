<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Dealer
 * @author     Andrey  Tkachenko <office@virtuas.net>
 * @copyright  2017 Andrey  Tkachenko
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Dealer model.
 *
 * @since  1.6
 */
class DealerModelDealer extends JModelAdmin
{
	public function getForm ($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_dealer.dealer', 'dealer', array('control' => 'jform', 'load_data' => $loadData));
	    return $form;
	}
	
	public function loadFormData(){
		$data = JFactory::getApplication()->getUserState($this->option . '.edit.authorization.data', array());
		JFactory::getApplication()->setUserState($this->option . '.edit.authorization.data', 'null');
		return $data;
	}
	
	public function getTable($type = 'Dealer', $prefix = 'DealerTable', $config = array())
	{
		$table = JTable::getInstance($type, $prefix, $config);

		return $table;
	}
	
	public function getItem($pk = null)
	{
		$jinput = JFactory::getApplication()->input;
		
		$user_id = $jinput->get('id');
		
		if($user_id){
			$db	= $this->getDbo();
			$query	= $db->getQuery(true);
			
			$query->select(array('a.username','a.email','b.*'));
			$query->from($db->quoteName('#__users','a'));
			$query->join('left', $db->quoteName('#__dealer', 'b') . ' ON (' . $db->quoteName('a.id') . ' = ' . $db->quoteName('b.user_id') . ')');
			$query->where($db->quoteName('b.user_id') . ' = ' . $user_id);
			$db->setQuery($query);
			
			$item = $db->loadObject();
	
			return $item;
		}
	}
	
	public function checkUser($username){
		if($username){
			$db	= $this->getDbo();
			$query	= $db->getQuery(true);
			
			$query->select(array('a.username','COUNT(*)'));
			$query->from($db->quoteName('#__users','a'));
			$query->where($db->quoteName('a.username') . ' = "' . $username .'"');
			$db->setQuery($query);
			
			$item = $db->loadResult();
	        
			if($item){
			  return true;
			}else{
			  return false;	
			}
		}
	}
	
	public function addDilerProfile($data,$userid){
		if($userid){
			$db	= $this->getDbo();
			$query	= $db->getQuery(true);
			
			$columns = array('user_id', 'name', 'phone1', 'phone2', 'address', 'company_name', 'note');
 
			$values = array($userid, $db->quote($data['name']), $db->quote($data['phone1']), $db->quote($data['phone2']), $db->quote($data['adress']), $db->quote($data['company_name']), $db->quote($data['note']));
			 
			$query
				->insert($db->quoteName('#__dealer'))
				->columns($db->quoteName($columns))
				->values(implode(',', $values));
			$db->setQuery($query);
			if($db->execute()){
			   return true;	
			}else{
			   return false;	
			}
		}
	}
}
