<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Dealer
 * @author     Andrey  Tkachenko <office@virtuas.net>
 * @copyright  2017 Andrey  Tkachenko
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_dealer'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Dealer', JPATH_COMPONENT_ADMINISTRATOR);

$controller = JControllerLegacy::getInstance('Dealer');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
