<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Calculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

use Joomla\Utilities\ArrayHelper;

/**
 * Settings list controller class.
 *
 * @since  1.6
 */
class CalculatorControllerSettings extends JControllerAdmin
{
	
	public function getModel($name = 'setting', $prefix = 'CalculatorModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}
	
	public function apply(){
		$jinput = JFactory::getApplication()->input;
		$post = $jinput->post->getArray();
		$model = $this->getModel('settings');
		
		$result = $model->saveSettings($post['jform']);
		
		$this->setRedirect('index.php?option=com_calculator&view=settings');
	}

	
}
