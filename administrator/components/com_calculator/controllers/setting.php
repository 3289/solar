<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Calculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Setting controller class.
 *
 * @since  1.6
 */
class CalculatorControllerSetting extends JControllerForm
{
    /**
     * Constructor
     *
     * @throws Exception
     */
    public function __construct() {
        $this->view_list = 'settings';
        parent::__construct();
    }

    /**
     * Method to save a record.
     *
     * @param   string $key The name of the primary key of the URL variable.
     * @param   string $urlVar The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
     *
     * @return  boolean  True if successful, false otherwise.
     *
     * @since   1.6
     */
    public function save($key = null, $urlVar = null) {

        $model = $this->getModel();
        $table = $model->getTable();
        $data = $this->input->post->get('jform', array(), 'array');


        parent::save();
        if ($data['id'] == 3) {
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);
            $query->select('*');
            $query->from($db->quoteName('#__calculator_stations'));

            $db->setQuery($query);

            $stantions = $db->loadObjectList();

            $val = $data['value'];

            foreach ($stantions as $stantion) {
                $power = $this->calcPower($stantion->number_panels * $val);
                $stantion->power =$power;
                $stantion->invertor = $power;
                JFactory::getDbo()->updateObject('#__calculator_stations', $stantion, 'id');
            }
            $this->refreshInsol();
        }
    }

    private function getSetting($sysname) {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_settings', 's'))->where('s.sysname = ' . $db->quote($sysname))->limit(1);

        $db->setQuery($query);
        $items = $db->loadObjectList();
        $item = reset($items);

        return $item->value;
    }

    private function calcPower($param) {
        $power = 10;
        if ($param >= 20000) {
            $power = 20;
        }
        if ($param >= 30000) {
            $power = 30;
        }
        return $power;
    }

    public function refreshInsol()
    {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_regions'));

        $db->setQuery($query);

        $regions = $db->loadObjectList();
        $regions_array = [];
        foreach ($regions as $region) {
            $regions_array[$region->id] = $region;
        }

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_stations'));

        $db->setQuery($query);

        $stations = $db->loadObjectList();
        $stations_array = [];
        foreach ($stations as $station) {
            $stations_array[$station->id] = $station;
        }

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_insolations_stations_regions'));

        $db->setQuery($query);

        $insolations = $db->loadObjectList();

        $ch = curl_init();
        foreach ($insolations as $insolation) {
            $this->getInsolation($regions_array[$insolation->id_region], $stations_array[$insolation->id_station], $insolation, $ch);

            $result = JFactory::getDbo()->updateObject('#__calculator_insolations_stations_regions', $insolation, 'id');
        }
        curl_close($ch);
        return $this;
    }

    private function getInsolation($region, $station, $insolation, $ch)
    {

        $constr = 'building';
        if($insolation->construct==2){
            $constr = 'free';
        }
        $peakPower = $station->number_panels *  (int)$this->getSetting('panels_power') / 1000;
        $url = 'http://re.jrc.ec.europa.eu/pvgis5/PVcalc.php?outputformat=basic&lat=' . $region->lat . '&lon=' . $region->lng . '&raddatabase=PVGIS-ERA5&browser=0&peakpower=' . $peakPower . '&loss=14&mountingplace=' . $constr . '&pvtechchoice=crystSi&angle=&angle=35&aspect=0&usehorizon=1&userhorizon=';

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);

        $lines = explode("\n", $result);
        $result = [];
        $avg = 0;
        foreach ($lines as $line) {
            $mounts = explode("\t", $line);
            if ((count($mounts) !== 4) || (!ctype_digit(reset($mounts)))) {
                continue;
            }
            $result[(int)$mounts[0]] = $mounts[1];
            $number = $mounts[0];
            if ($number < 10) {
                $number = '0' . $number;
            }
            $insolation->{'insol' . $number} = $mounts[1];
            $avg += $mounts[1];
        }

        $tariff = $this->getSetting('green_cost');
        $tariff = floatval(str_replace(",",".",$tariff))*0.805;
        $insolation->insol_avg = $avg / 12;
        $insolation->profit_eur = $avg * $tariff;


        return $result;
    }
}
