<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Calculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Calculator helper.
 *
 * @since  1.6
 */
class CalculatorHelpersCalculator
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string  $vName  string
	 *
	 * @return void
	 */
	public static function addSubmenu($vName = '')
	{
				JHtmlSidebar::addEntry(
			JText::_('COM_CALCULATOR_TITLE_STANTIONS'),
			'index.php?option=com_calculator&view=stations',
			$vName == 'stantions'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_CALCULATOR_TITLE_REGIONS'),
			'index.php?option=com_calculator&view=regions',
			$vName == 'regions'
		);
		JHtmlSidebar::addEntry(
			JText::_('COM_CALCULATOR_TITLE_ORDERS'),
			'index.php?option=com_calculator&view=orders',
			$vName == 'orders'
		);
        JHtmlSidebar::addEntry(
            JText::_('COM_CALCULATOR_TITLE_PHRASES'),
            'index.php?option=com_calculator&view=phrases',
            $vName == 'phrases'
        );
		JHtmlSidebar::addEntry(
			JText::_('COM_CALCULATOR_TITLE_SETTINGS'),
			'index.php?option=com_calculator&view=settings',
			$vName == 'settings'
		);
	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return    JObject
	 *
	 * @since    1.6
	 */
	public static function getActions()
	{
		$user   = JFactory::getUser();
		$result = new JObject;

		$assetName = 'com_calculator';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action)
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
}


class CalculatorHelper extends CalculatorHelpersCalculator
{

}
