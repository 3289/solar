	<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Calculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Calculator model.
 *
 * @since  1.6
 */
class CalculatorModelRegion extends JModelAdmin
{
	public function getTable($type = 'Region', $prefix = 'CalculatorTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	public function getForm ($data = array(), $loadData = true)
	{
		jimport('joomla.form.form');
		JForm::addFieldPath('JPATH_ADMINISTRATOR/components/com_calculator/models/fields');

		// Get the form.
		$form = $this->loadForm('com_calculator.region', 'region', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		

		return $form;
	
	}
	
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_calculator.edit.region.data', array());

		if (empty($data)) {
			$data = $this->getItem();
		}

		return $data;
	}
}
