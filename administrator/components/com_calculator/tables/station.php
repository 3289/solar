<?php

// no direct access
defined('_JEXEC') or die('Restricted access');

class CalculatorTableStation extends JTable
{
    public function __construct(&$db) {
        parent::__construct('#__calculator_stations', 'id', $db);
    }
}
