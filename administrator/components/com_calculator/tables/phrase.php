<?php

// no direct access
defined('_JEXEC') or die('Restricted access');

class CalculatorTablePhrase extends JTable
{
    public function __construct(&$db) {
        parent::__construct('#__calculator_phrases', 'id', $db);
    }
}
