<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Calculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Class CalculatorController
 *
 * @since  1.6
 */
class CalculatorController extends JControllerLegacy
{
    /**
     * Method to display a view.
     *
     * @param   boolean $cachable If true, the view output will be cached
     * @param   mixed $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
     *
     * @return   JController This object to support chaining.
     *
     * @since    1.5
     */
    public function display($cachable = false, $urlparams = false)
    {

        $view = JFactory::getApplication()->input->getCmd('view', 'stations');
        JFactory::getApplication()->input->set('view', $view);

        parent::display($cachable, $urlparams);

        return $this;
    }


    public function refreshInsol()
    {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_regions'));

        $db->setQuery($query);

        $regions = $db->loadObjectList();
        $regions_array = [];
        foreach ($regions as $region) {
            $regions_array[$region->id] = $region;
        }

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_stations'));

        $db->setQuery($query);

        $stations = $db->loadObjectList();
        $stations_array = [];
        foreach ($stations as $station) {
            $stations_array[$station->id] = $station;
        }

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_insolations_stations_regions'));

        $db->setQuery($query);

        $insolations = $db->loadObjectList();

        $ch = curl_init();
        foreach ($insolations as $insolation) {
            $this->getInsolation($regions_array[$insolation->id_region], $stations_array[$insolation->id_station], $insolation, $ch);

            $result = JFactory::getDbo()->updateObject('#__calculator_insolations_stations_regions', $insolation, 'id');
        }
        curl_close($ch);
        return $this;
    }

    private function getInsolation($region, $station, $insolation, $ch)
    {

        $constr = 'building';
        if($insolation->construct==2){
            $constr = 'free';
        }
        $peakPower = $station->number_panels *  (int)$this->getSetting('panels_power') / 1000;
        $url = 'http://re.jrc.ec.europa.eu/pvgis5/PVcalc.php?outputformat=basic&lat=' . $region->lat . '&lon=' . $region->lng . '&raddatabase=PVGIS-ERA5&browser=0&peakpower=' . $peakPower . '&loss=14&mountingplace=' . $constr . '&pvtechchoice=crystSi&angle=&angle=35&aspect=0&usehorizon=1&userhorizon=';

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);

        $lines = explode("\n", $result);
        $result = [];
        $avg = 0;
        foreach ($lines as $line) {
            $mounts = explode("\t", $line);
            if ((count($mounts) !== 4) || (!ctype_digit(reset($mounts)))) {
                continue;
            }
            $result[(int)$mounts[0]] = $mounts[1];
            $number = $mounts[0];
            if ($number < 10) {
                $number = '0' . $number;
            }
            $insolation->{'insol' . $number} = $mounts[1];
            $avg += $mounts[1];
        }

        $tariff = $this->getSetting('green_cost');
        $tariff = floatval(str_replace(",",".",$tariff))*0.805;
        $insolation->insol_avg = $avg / 12;
        $insolation->profit_eur = $avg * $tariff;


        return $result;
    }

    private function getSetting($sysname)
    {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__calculator_settings', 's'))->where('s.sysname = ' . $db->quote($sysname))->limit(1);

        $db->setQuery($query);
        $items = $db->loadObjectList();
        $item = reset($items);

        return $item->value;
    }
}
