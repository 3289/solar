-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: solar
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aslxn_calculator_regions`
--

DROP TABLE IF EXISTS `aslxn_calculator_regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aslxn_calculator_regions` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `region_name` varchar(155) CHARACTER SET utf16 NOT NULL,
  `published` int(2) NOT NULL,
  `sys_name` varchar(255) NOT NULL DEFAULT 'none',
  `from10` varchar(255) DEFAULT '36-64',
  `from20` varchar(255) DEFAULT '72-96',
  `from30` varchar(255) DEFAULT '108-168',
  `insol01` int(11) NOT NULL DEFAULT '0',
  `insol02` int(11) NOT NULL DEFAULT '0',
  `insol03` int(11) NOT NULL DEFAULT '0',
  `insol04` int(11) NOT NULL DEFAULT '0',
  `insol05` int(11) NOT NULL DEFAULT '0',
  `insol06` int(11) NOT NULL DEFAULT '0',
  `insol07` int(11) NOT NULL DEFAULT '0',
  `insol08` int(11) NOT NULL DEFAULT '0',
  `insol09` int(11) NOT NULL DEFAULT '0',
  `insol10` int(11) NOT NULL DEFAULT '0',
  `insol11` int(11) NOT NULL DEFAULT '0',
  `insol12` int(11) NOT NULL DEFAULT '0',
  `lat` float NOT NULL DEFAULT '0',
  `lng` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5557 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aslxn_calculator_regions`
--

/*!40000 ALTER TABLE `aslxn_calculator_regions` DISABLE KEYS */;
INSERT INTO `aslxn_calculator_regions` (`id`, `region_name`, `published`, `sys_name`, `from10`, `from20`, `from30`, `insol01`, `insol02`, `insol03`, `insol04`, `insol05`, `insol06`, `insol07`, `insol08`, `insol09`, `insol10`, `insol11`, `insol12`, `lat`, `lng`) VALUES (1,'Винницкая',1,'Vinnytsia','36-64','72-96','108-168',291,429,959,1220,1440,1420,1460,1410,1070,754,349,218,49.14,28.29),(2,'Днeпpoпетровская',1,'Dnipropetrovsk','36-64','72-96','108-168',331,588,1030,1270,1470,1470,1540,1520,1220,928,463,269,48.27,34.59),(3,'Дoнeцкaя',1,'Donetsk','36-64','72-96','108-168',362,623,1010,1260,1470,1490,1540,1550,1250,964,508,266,48,37.48),(4,'Вoлынскaя',1,'Volyn oblast','36-64','72-96','108-168',257,426,904,1180,1320,1330,1350,1340,996,698,326,214,50.44,25.2),(5,'Житoмиpcкaя',1,'Zhytomyr','36-64','72-96','108-168',253,424,876,1150,1360,1350,1380,1300,981,688,274,179,50.16,28.4),(6,'Закapпатскaя',1,'Zakarpattia','36-64','72-96','108-168',304,503,1030,1310,1390,1370,1430,1450,1100,769,460,244,48.37,22.18),(7,'Запopoжская',1,'Zaporizhzhia','36-64','72-96','108-168',257,486,902,1220,1430,1440,1520,1490,1180,875,462,249,47.5,35.1),(8,'Ивaно-Фpaнковская',1,'Ivano','36-64','72-96','108-168',352,508,834,1070,1130,1110,1190,1160,919,694,448,281,48.55,24.43),(9,'Kиeвскaя',1,'Kyiv','36-64','72-96','108-168',363,523,917,1170,1290,1250,1390,1340,1030,759,470,329,48.18,25.56),(10,'Kировoгpaдская',1,'Kirovohrad','36-64','72-96','108-168',332,542,1000,1290,1460,1470,1540,1490,1200,893,431,258,48.3,32.18),(11,'Лугaнскaя',1,'Luhansk','36-64','72-96','108-168',0,0,0,0,0,0,0,0,0,0,0,0,0,0),(12,'Львoвскaя',1,'Lviv','36-64','72-96','108-168',275,446,890,1170,1280,1240,1320,1320,1010,698,363,252,49.5,24),(13,'Никoлaeвская',1,'Mykolaiv','36-64','72-96','108-168',388,670,1100,1410,1550,1540,1660,1610,1330,972,555,368,46.58,32),(14,'Одeccкая',1,'Odesa','36-64','72-96','108-168',490,721,1220,1480,1660,1630,1690,1650,1390,1020,600,427,46.28,30.44),(15,'Полтaвcкая',1,'Poltava','36-64','72-96','108-168',312,554,975,1230,1450,1480,1490,1470,1140,811,374,226,49.35,34.34),(16,'Poвенская',1,'Rivne','36-64','72-96','108-168',249,436,878,1170,1340,1330,1360,1340,986,676,299,182,50.37,26.15),(17,'Cумcкaя',1,'Sumy','36-64','72-96','108-168',251,484,916,1190,1430,1430,1440,1390,1020,710,297,173,50.55,34.45),(18,'Tepнопoльскaя',1,'Ternopil','36-64','72-96','108-168',283,440,931,1210,1330,1300,1380,1340,1020,728,355,246,49.34,25.36),(19,'Xaрькoвскaя',1,'Kharkiv','36-64','72-96','108-168',298,527,934,1190,1420,1460,1480,1420,1080,756,346,203,50,36.15),(20,'Xeрсoнская',1,'Kherson','36-64','72-96','108-168',510,744,1200,1460,1570,1570,1670,1630,1370,1020,615,410,46.38,32.35),(21,'Xмeльницкая',1,'Khmelnytskyi','36-64','72-96','108-168',271,413,912,1200,1390,1390,1420,1390,1060,726,327,198,49.2512,27),(22,'Чepкaсская',1,'Cherkasy','36-64','72-96','108-168',298,410,968,1230,1450,1450,1460,1410,1080,787,340,200,49.264,30.0335),(23,'Чeрнигoвскaя',1,'Chernihiv','36-64','72-96','108-168',235,435,906,1190,1420,1450,1440,1340,1010,648,262,162,51.3,31.18),(24,'Чepновицкaя',1,'Chernivtsi','36-64','72-96','108-168',363,523,917,1170,1290,1250,1390,1340,1030,759,470,329,48.18,25.56),(5556,'АР Крым',1,'Crimea','36-64','72-96','108-168',0,0,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `aslxn_calculator_regions` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-26 21:08:42
