<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Calculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_calculator'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Calculator', JPATH_COMPONENT_ADMINISTRATOR);

$controller = JControllerLegacy::getInstance('Calculator');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
