<?php
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php if(!empty( $this->sidebar)): ?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
<?php else: ?>
<div id="j-main-container">
<?php endif;?>
<form action="index.php?option=com_calculator&view=regions" method="post" id="adminForm" name="adminForm">
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="2%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
            <th style="text-align: center"><?php echo JText::_('COM_CALCULATOR_ID'); ?></th>
			<th style="text-align: center">
				<?php echo JText::_('COM_CALCULATOR_REGION_NAME') ;?>
			</th>
            <th style="text-align: center">
				<?php echo JText::_('COM_CALCULATOR_REGION_PUBLISHED'); ?>
			</th>
			<th style="text-align: center">
				<?php echo JText::_('COM_CALCULATOR_REGION_SYS_NAME'); ?>
			</th>
            <th style="text-align: center">
                <?php echo JText::_('COM_CALCULATOR_REGION_LAT'); ?>
            </th>
            <th style="text-align: center">
                <?php echo JText::_('COM_CALCULATOR_REGION_LNG'); ?>
            </th>
            <th style="text-align: center">
                <?php echo JText::_('COM_CALCULATOR_REGION_MAX'); ?>
            </th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="6">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>

			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : 
				    $link = JRoute::_('index.php?option=com_calculator&task=region.edit&id=' . $row->id);
				?>
					<tr>
                        <td style="text-align: center">
							<?php echo JHtml::_('grid.id', $i, $row->id); ?>
						</td>
						<td style="text-align: center">
							<?php echo $this->pagination->getRowOffset($i); ?>
						</td>
						<td style="text-align: center">
                            <a href="<?php echo $link; ?>" title="<?php  echo $row->region_name;?>">
							<?php 
							    echo $row->region_name; 	
							?>
                            </a>
						</td>
                        <td style="text-align: center">
                            <?php
                            switch($row->published){
                                case 1: echo JText::_('JPUBLISHED');break;
                                case 0: echo JText::_('JUNPUBLISHED');break;
                            }
                            ?>
						</td>
                        <td style="text-align: center">
                            <?php echo $row->sys_name; ?>
                        </td>
                        <td style="text-align: center">
                            <?php echo $row->lat; ?>
                        </td>
                        <td style="text-align: center">
                            <?php echo $row->lng; ?>
                        </td>
                        <td style="text-align: center">
                            <?php echo $row->max; ?>
                        </td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
    <input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
</form>
</div>