<?php
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php if(!empty( $this->sidebar)): ?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
<?php else: ?>
<div id="j-main-container">
<?php endif;?>
<form action="index.php?option=com_calculator&view=stations" method="post" id="adminForm" name="adminForm">
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="2%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
            <th style="text-align: center"><?php echo JText::_('COM_CALCULATOR_ID'); ?></th>
			<th style="text-align: center">
				<?php echo JText::_('COM_CALCULATOR_STANTION_NUMBER_PANELS'); ?>
			</th>
            <th style="text-align: center">
				<?php echo JText::_('COM_CALCULATOR_STANTION_POWER'); ?>
			</th>
			<th style="text-align: center">
				<?php echo JText::_('COM_CALCULATOR_STANTION_COST_STAT'); ?>
			</th>
            <th style="text-align: center">
                <?php echo JText::_('COM_CALCULATOR_STANTION_COST_DAH'); ?>
            </th>
            <th style="text-align: center">
                <?php echo JText::_('COM_CALCULATOR_STANTION_COST_POVOROT'); ?>
            </th>
            <th style="text-align: center">
                <?php echo JText::_('COM_CALCULATOR_STANTION_COST_TRACK'); ?>
            </th>
            <th style="text-align: center">
               Инвертор
            </th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : 
				    $link = JRoute::_('index.php?option=com_calculator&task=station.edit&id=' . $row->id);
				?>
 
					<tr>
                        <td>
							<?php echo JHtml::_('grid.id', $i, $row->id); ?>
						</td>
						<td style="text-align: center">
							<?php echo $this->pagination->getRowOffset($i); ?>
						</td>
						<td style="text-align: center">
                            <a href="<?php echo $link; ?>" title="<?php echo $row->number_panels; ?>">
							<?php 
							    echo $row->number_panels;
							?>
                            </a>
						</td>
                        <td style="text-align: center">
							<?php
                                echo $row->power.' '.JText::_('COM_CALCULATOR_STANTION_UNIT_KWT');
							?>
						</td>
                        <td style="text-align: center">
                            <?php 
							    echo $row->cost_stat.' '.JText::_('COM_CALCULATOR_STANTION_PRICE_UNIT_USD');
							?>
                        </td>
                        <td style="text-align: center">
                            <?php
                                echo $row->cost_dah.' '.JText::_('COM_CALCULATOR_STANTION_PRICE_UNIT_USD');
                            ?>
                        </td>
                        <td style="text-align: center">
                            <?php
                                echo $row->cost_povorot.' '.JText::_('COM_CALCULATOR_STANTION_PRICE_UNIT_USD');
                            ?>
                        </td>
                        <td style="text-align: center">
                            <?php
                                echo $row->cost_track.' '.JText::_('COM_CALCULATOR_STANTION_PRICE_UNIT_USD');
                            ?>
                        </td>

                        <td style="text-align: center">
                            <?php
                                echo $row->invertor.' кВт'
                            ?>
                        </td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
    <input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
</form>
</div>