<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Calculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Calculator.
 *
 * @since  1.6
 */
class CalculatorViewStations extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->items = $this->get('Items');
		$this->state = $this->get('State');
		$this->pagination	= $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		CalculatorHelpersCalculator::addSubmenu('stations');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$canDo = CalculatorHelpersCalculator::getActions();

		JToolBarHelper::title(JText::_('COM_CALCULATOR_TITLE_STANTIONS'), 'stations.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/station';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('station.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_calculator');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_calculator&view=stations');

		$this->extra_sidebar = '';
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
		);
	}
}
