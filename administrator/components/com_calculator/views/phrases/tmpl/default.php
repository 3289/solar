<?php
 
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<?php if(!empty( $this->sidebar)): ?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
<?php else: ?>
<div id="j-main-container">
<?php endif;?>
<form action="index.php?option=com_calculator&view=phrases" method="post" id="adminForm" name="adminForm">
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="2%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
            <th style="text-align: center"><?php echo JText::_('COM_CALCULATOR_ID'); ?></th>
			<th style="text-align: center">
				<?php echo JText::_('COM_CALCULATOR_PHRASE_NAME'); ?>
			</th>
            <th style="text-align: center">
                <?php echo JText::_('COM_CALCULATOR_PHRASE_AUTHOR'); ?>
            </th>
            <th style="text-align: center">
                <?php echo JText::_('COM_CALCULATOR_PHRASE_LANG'); ?>
            </th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="5">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) : 
				    $link = JRoute::_('index.php?option=com_calculator&task=phrase.edit&id=' . $row->id);
				?>
 
					<tr>
                        <td>
							<?php echo JHtml::_('grid.id', $i, $row->id); ?>
						</td>
						<td style="text-align: center">
							<?php echo $this->pagination->getRowOffset($i); ?>
						</td>
						<td style="text-align: center">
                            <a href="<?php echo $link; ?>" title="<?php echo $row->name; ?>">
							<?php 
							    echo $row->name;
							?>
                            </a>
						</td>
                        <td style="text-align: center">
                            <?php echo $row->author; ?>
                        </td>
                        <td style="text-align: center">
                            <?php echo $row->lang; ?>
                        </td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
    <input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
</form>
</div>