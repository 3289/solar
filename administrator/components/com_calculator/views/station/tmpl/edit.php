<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Calculator
 * @author     virtuas <office@virtuas.net>
 * @copyright  2017 virtuas.net
 * @license    GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_calculator/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'stantion.cancel') {
			Joomla.submitform(task, document.getElementById('stantion-form'));
		}
		else {
			
			if (task != 'stantion.cancel' && document.formvalidator.isValid(document.id('stantion-form'))) {
				
				Joomla.submitform(task, document.getElementById('stantion-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_calculator&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="stantion-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_CALCULATOR_STANTION', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
                    <div class="row-fluid">
                    <div class="span3">
                        <div class="control-group">
                            <div class="control-label"><?php echo $this->form->getLabel('number_panels'); ?></div>
                            <div class="controls"><?php echo $this->form->getInput('number_panels'); ?></div>
                        </div>
                    </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span3">
                            <div class="control-group">
                                <div class="control-label"><?php echo $this->form->getLabel('power'); ?></div>
                                <div class="controls"><?php echo $this->form->getInput('power'); ?></div>
                            </div>
                        </div>
                        <div class="span7">
                            <div class="control-group">
                                <?php echo JText::_('COM_CALCULATOR_STANTION_UNIT_KWT'); ?>
                            </div>
                        </div>
                    </div>
					
                    <div class="row-fluid">
                        <div class="span3">
                            <div class="control-group">
                                <div class="control-label"><?php echo $this->form->getLabel('cost_stat'); ?></div>
                                <div class="controls"><?php echo $this->form->getInput('cost_stat'); ?></div>
                            </div>
                        </div>
                        <div class="span7">
                            <div class="control-group">
                                <?php echo JText::_('COM_CALCULATOR_STANTION_PRICE_UNIT_USD'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span3">
                            <div class="control-group">
                                <div class="control-label"><?php echo $this->form->getLabel('cost_dah'); ?></div>
                                <div class="controls"><?php echo $this->form->getInput('cost_dah'); ?></div>
                            </div>
                        </div>
                        <div class="span7">
                            <div class="control-group">
                                <?php echo JText::_('COM_CALCULATOR_STANTION_PRICE_UNIT_USD'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span3">
                            <div class="control-group">
                                <div class="control-label"><?php echo $this->form->getLabel('cost_povorot'); ?></div>
                                <div class="controls"><?php echo $this->form->getInput('cost_povorot'); ?></div>
                            </div>
                        </div>
                        <div class="span7">
                            <div class="control-group">
                                <?php echo JText::_('COM_CALCULATOR_STANTION_PRICE_UNIT_USD'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span3">
                            <div class="control-group">
                                <div class="control-label"><?php echo $this->form->getLabel('cost_track'); ?></div>
                                <div class="controls"><?php echo $this->form->getInput('cost_track'); ?></div>
                            </div>
                        </div>
                        <div class="span7">
                            <div class="control-group">
                                <?php echo JText::_('COM_CALCULATOR_STANTION_PRICE_UNIT_USD'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span3">
                            <div class="control-group">
                                <div class="control-label"><?php echo $this->form->getLabel('invertor'); ?></div>
                                <div class="controls"><?php echo $this->form->getInput('invertor'); ?></div>
                            </div>
                        </div>
                        <div class="span7">
                            <div class="control-group">
                                кВт
                            </div>
                        </div>
                    </div>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		
<?php echo $this->form->getInput('id'); ?>
		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
