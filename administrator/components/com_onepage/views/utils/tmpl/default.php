<?php
/**
 * @version		$Id: default.php 21837 2011-07-12 18:12:35Z dextercowley $
 * @package		RuposTel OnePage Utils
 * @subpackage	com_onepage
 * @copyright	Copyright (C) 2005 - 2013 RuposTel.com
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$document = JFactory::getDocument();
JHTML::script('toggle_langs.js', 'administrator/components/com_onepage/views/config/tmpl/js/', false);
$document->setTitle(JText::_('COM_ONEPAGE_UTILS')); 
$document->addStyleDeclaration('
#toolbar-box { display: none; } ');

$default_config = array('vm_lang'=>0, 'vm_menu_en_gb'=>0, 'selected_menu'=>0, 'menu_'=>0, 'tojlanguage'=>'*'); 
$session = JFactory::getSession(); 
$config = $session->get('opc_utils', $default_config); 

if (!version_compare(JVERSION,'2.5.0','ge'))
{
  $j15 = true; 
  
}


?>
<form action="index.php" method="post">
 <?php if (!empty($this->cats)) 

{
 ?>
<fieldset <?php if (!empty($j15)) echo ' disabled="disabled" '; ?> >
<legend><?php echo JText::_('COM_ONEPAGE_UTILS_VM_TO_J_LABEL'); ?></legend>
<?php if (!empty($j15)) echo '<div>'.JText::_('COM_ONEPAGE_ONLY_J25').'</div>'; ?>
<div><?php echo JText::_('COM_ONEPAGE_UTILS_DESC'); ?></div>
<div><?php echo '<div style="color: red;">'.JText::_('COM_ONEPAGE_UTILS_NOTE').' </div>'; echo JText::_('COM_ONEPAGE_UTILS_NOTE2'); ?>
</div><br />
<table  >
<tr>
<td>
<?php 

echo JText::_('COM_ONEPAGE_UTILS_SELECT_VM_CHILD'); ?><br /><?php echo JText::_('COM_ONEPAGE_UTILS_SELECT_VM_CHILD_DESC'); ?>
</td>
<td>
<select name="vm_lang" onchange="return op_unhideMenuVM(this);">
<?php
if (empty($this->cats)) $this->cats = array(); 
foreach ($this->cats as $lang=>$arr)
{
  echo '<option '; 
  if (!empty($config['vm_lang']) && ($lang==$config['vm_lang'])) 
  {
  $first_lang = $config['vm_lang']; 
  echo ' selected="selected" '; 
  }
  if (empty($config['vm_lang']))
  if (!isset($first_lang))
  $first_lang = $lang; 
  
  echo ' value="'.$lang.'">'.$lang.'</option>'; 
}
?>
</select>
<?php
if (empty($this->cats))  $this->cats = array(); 
foreach ($this->cats as $lang=>$arr)
{
 if (!empty($config['vm_menu_'.$first_lang])) $first_vm = $config['vm_menu_'.$first_lang];  
 
 if (!isset($first_vm))
 $first_vm = $lang;
?><select <?php if ($lang != $first_lang) echo ' style="display: none;" ';  ?> name="vm_menu_<?php echo $lang; ?>" id="vm_menu_<?php echo $lang; ?>"  >
<option value="0">--- <?php echo JText::_('COM_ONEPAGE_UTILS_ALL'); ?> ---</option>
<?php

foreach ($arr as $key2=>$mymenu)
{
?>

<?php
//debug_zval_dump($m); die(); 

   if (!isset($mymenu['virtuemart_category_id'])) continue; 
   echo '<option '; 
   if (!empty($config['vm_menu_'.$first_lang]) && ($mymenu['virtuemart_category_id'] == $config['vm_menu_'.$first_lang]))
   echo ' selected="selected" '; 
   echo ' value="'.$mymenu['virtuemart_category_id'].'">'.$mymenu['category_name'].'</option>'; 
   // recursion here: 
   if (!empty($mymenu['children']))
   $this->printChildren($mymenu['children'], 'virtuemart_category_id', 'category_name', '->');
 
}
?>
</select>

<?php
}
?>
</td>
</tr>
<tr>
<td>


<?php echo JText::_('COM_ONEPAGE_UTILS_TO_MOVE_JOOMLA_MENU'); ?>
</td>
<td>
<select name="selected_menu" onchange="return op_unhideMenu(this);">
<option value="0">--- <?php echo JText::_('COM_ONEPAGE_UTILS_NEW'); ?> ---</option>

<?php 
if (empty($config['selected_menu'])) $first = 0; 
else $first = $config['selected_menu']; 
foreach ($this->menus as $menu)
{
 //$first = $menu['menutype']; 
 echo '<option value="'.$menu['menutype'].'" '; 
 if ($menu['menutype'] == $config['selected_menu']) echo ' selected="selected" '; 
 echo '>'.$menu['title'].'</option>'; 
}
?>
</select>
</td>
</tr>
<tr>
<td>

<script type="text/javascript">
 var last_menu = 'menu_<?php echo $first; ?>'; 
 var last_menu_vm = 'vm_menu_<?php echo $first_lang; ?>'; 
</script>
<?php echo JText::_('COM_ONEPAGE_UTILS_WITH_PARENT_MENU_ITEM'); ?>
</td>
<td>
<select name="menu_0" id="menu_0" <?php if (!empty($first)) echo ' style="display: none;" '; ?> disabled="disabled" ><option value="">-</option></select>
<?php
foreach ($this->sortedmenu as $key2=>$m)
{
?>
<select name="menu_<?php echo $key2; ?>" id="menu_<?php echo $key2; ?>" <?php if ($key2 !== $first) echo ' style="display: none;" '; ?> >
<option value="1">--- <?php echo JText::_('COM_ONEPAGE_UTILS_TOP'); ?> ---</option>
<?php
//debug_zval_dump($m); die(); 
foreach ($m as $key=>$mymenu)
 {
   if (empty($key)) continue; 
   if ($mymenu['published']<0) continue; 
   if (!isset($mymenu['id'])) { 
   continue; 
   var_dump($key2);  var_dump($key); var_dump($menu[103]); 
   echo 'mymenu: '; 
   var_dump($mymenu);  die('hh'); }
   
   echo '<option '; 
if ((!empty($config['menu_'.$key2])) && ($config['menu_'.$key2]==$mymenu['id'])) echo ' selected="selected" ';    
   echo ' value="'.$mymenu['id'].'">'.$mymenu['item_type'].'</option>'; 
   // recursion here: 
   if (!empty($mymenu['children']))
   $this->printChildren($mymenu['children'], 'id', 'item_type', '->');
 }
?>
</select>
<?php

}
?>
</td>
</tr>
<tr>
<td>
<?php
$lang =  JFactory::getLanguage(); 
$langs = $lang->getKnownLanguages(); 
echo ''.JText::_('COM_ONEPAGE_UTILS_INS_TO_LANG'); 
?>
</td>
<td>
<?php
echo '<select name="tojlanguage">'; 
echo '<option value="*">All</option>'; 
foreach ($langs as $key=>$l)
{
  echo '<option ';
  if (!empty($config['tojlanguage']) && ($config['tojlanguage']==$key)) echo ' selected="selected" '; 
  echo ' value="'.$key.'">'.$l['name'].'</option>'; 
}
echo '</select>'; 
?>
</td>
</tr>
</table>
<input type="hidden" name="option" value="com_onepage" />
<input type="hidden" name="task" value="movemenu" />
<input type="hidden" name="view" value="utils" />
<?php if (!empty($this->cats)) { ?>
<input type="submit" name="Proceed" />
<?php } ?>
</fieldset>
<?php 
} ?>
</form> 
<form action="index.php" method="post">
<fieldset <?php if (!empty($j15)) echo ' disabled="disabled" '; ?> >
<legend><?php echo JText::_('COM_ONEPAGE_UTILS_SEARCH_FULLTEXT'); ?></legend>
<div><?php echo JText::_('COM_ONEPAGE_UTILS_SEARCH_DESC'); ?></div>

<table>
<tr>
<td>Search for text in your joomla installation: 
</td>
<td><input type="text" value="" name="searchwhat" />
</td>
<td>
<select name="ext">
 <option value="*">*.*</option>
 <option value="css">*.css</option>
 <option value="php">*.php</option>
 <option value="gif">*.gif</option>
 <option value="css">*.css</option>
 <option value="js">*.js</option>
</select>
</td>
</tr>
<tr><td>
<input type="checkbox" name="excludecache" value="1" checked="checked" id="excludecache" />
</td>
<td>
<label for="excludecache">Exclude cache</label>
</td>
</tr>
<tr><td><input type="checkbox" name="casesensitive" value="1" checked="checked" id="casesensitive" /></td><td><label for="casesensitive">Case sensitive</label></td></tr>
<tr><td><input type="checkbox" name="onlysmall" value="1" checked="checked" id="onlysmall" /></td><td><label for="onlysmall">Only smaller than 500kb</label></td></tr>
<tr>
<td><input type="submit" /></td></tr></table>
</fieldset>


<input type="hidden" name="option" value="com_onepage" />
<input type="hidden" name="task" value="searchtext" />
<input type="hidden" name="view" value="utils" />

</form>



<fieldset <?php if (!empty($j15)) echo ' disabled="disabled" '; ?> >
<legend>Products to Child Products</legend>
<div>This small utility will convert your products like: <br />
<table>
<table>
<tr><th>Name</th><th>SKU</th></tr>
<tr><td>Machanic TShirt<td><td>3PW155680X</td></tr>
<tr><td>Machanic TShirt S<td><td>3PW1556801</td></tr>
<tr><td>Machanic TShirt M<td><td>3PW1556802</td></tr>
</table>
</div>
<p>Logic: </p>
<p>1. Will take the SKU except the last letter of the SKU (3PW155680). </p>
<p>2. Will do a search in the #__virtuemart_products table to see if other products match with this SKU (3PW155680%). </p>
<p>3. Will find the parent product that has the shortest name among the results</p>
<p>4. Will assign this parent ID to it's new child products</p>
<p>5. Will create the custom fields within the parent (if it has no "generic child variant" custom attribute</p>
<table>




<tr><td>
<form action="index.php" method="post" name="childProductsForm" id="childProductsForm">

<select class="inputbox" id="virtuemart_category_id" name="virtuemart_category_id" >
					<option value=""><?php echo JText::_('COM_ONEPAGE_ANY'); ?></option>
					<?php 
					require_once(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'shopfunctions.php'); 
					
					$selected_cats = JRequest::getVar('virtuemart_category_id'); 
					if (!is_array($selected_cats))
						$selected_cats = array($selected_cats); 
					$this->category_tree = ShopFunctions::categoryListTree($selected_cats);
					echo $this->category_tree; ?>
				</select>
				
				<?php $min = JRequest::getVar('min', 2); ?>
<label for="min_p">Minimum number of products per similar SKU: <input type="number" value="<?php echo $min; ?>" id="min_p" name="min" /></label><br />
<input type="submit" value="Assign child products" /></td></tr>


<input type="hidden" name="option" value="com_onepage" />
<input type="hidden" name="task" value="createchilds" />
<input type="hidden" name="view" value="utils" />


</form>

</table>

</fieldset>





<?php $childPairing = JRequest::getVar('childpairing'); ?>
<?php if (!empty($childPairing)) {
	?>
	
	
<form action="index.php" method="post" name="childProductsForm2" id="childProductsForm2">
<input type="hidden" name="option" value="com_onepage" />
<input type="hidden" name="task" value="pair" id="pair_task" />
<input type="hidden" name="what" value="" id="what_id" />
<input type="hidden" name="view" value="utils" />
<?php $selected_cats = JRequest::getVar('virtuemart_category_id', 0);  ?>
<input type="hidden" name="virtuemart_category_id" value="<?php echo $selected_cats; ?>" />
<?php $min = JRequest::getVar('min', 2); ?>
<input type="hidden" name="min" value="<?php echo 	$min; ?>" />
<script>
function pair(task, id)
{
	var d = document.getElementById('pair_task'); 
	d.value = task; 
	var d2 = document.getElementById('what_id'); 
	d2.value = id; 
	
	d.form.submit(); 
	return false; 
}

function pair_multi()
{
	return pair('pair'); 
}


</script>
	
	<?php
	$restdata = ''; 
		$groups = $this->model->buildProducts(); 
		//debug_zval_dump($groups); die(); 
		foreach ($groups as $k=>$v)
		{
			if (empty($v)) continue; 
			ob_start(); 
			?><fieldset><legend><?php echo $k; ?></legend>
			<?php
			 $cx = ''; 
			 $sk=count($v); 
			 foreach ($v as $k2=>$v2)
			 {
				
				 if (empty($v2['best_name'])) 
				 {
					 $cx .= 'Skipped: '.$v2['product_sku'].' -> '.$v2['product_name'].'<br />'; 
					 $sk--; 
					 continue; 
				 }
				 $best_name = $v2['best_name']; 
				 $new = false; 
				 if (!empty($v2['parent_type']))
				 {
					 $new = true; 
				 }
				 ?><div><b>Action:  </b></div><br /><select name="action_'.<?php echo $k.'_'.$k2; ?>">
				   <option value="0">Do nothing</option>
				   <option <?php if (!empty($new)) echo ' selected="selected" '; ?> value="new">Create new parent</option>
				   <option <?php if (empty($new)) echo ' selected="selected" '; ?> value="1">Set this product to be the parent:</option>
				   
				   </select>
				   <br />
				   
				   <?php if ($new) { ?>
				    <div style="clear: both;">Suggested parent name: <br /><input type="text" value="<?php echo htmlentities($best_name); ?>" name="parent_name_<?php echo $k.'_'.$k2; ?>" /></div>
				   <?php } ?>
				   <br />
				   <div style="clear: both;">Set this existing product to be parent: </div><br />
				   <select name="existing_parent" >
				     <option value="">-</option>
				     <?php  
					 $names = ''; 
					 foreach ($groups[$k] as $k23=>$v23) { 
					 if (!empty($v23['product_parent_id'])) { $is_child = true; }
					 else $is_child = false; 
					 $names .= '<div style="clear: both;'; 
					 if ($is_child) { $names .= ' color: green; margin-left: 50px; '; }
					 $names .= '">Product sku: <a href="/index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$v23['virtuemart_product_id'].'">'.$v23['product_sku']." (id:".$v23['virtuemart_product_id'].") </a> name: ".$v23['product_name']."</div><br />";
					 ?>
					   <option <?php if ($v23['product_name'] == $best_name) echo ' selected="selected" '; ?> value="<?php echo htmlentities($k23); ?>"><?php echo htmlentities($k23.' '.$v23['product_name']); ?></option>
					 <?php } ?>
				   </select>
				   <br />
				   <div style="clear: both;">
				 <?php
				 
				 ?></div>
				
				 <?php
				 echo $names; 
				 break; 
			 }
			?>
			 <div style="clear: both;">
				 <?php
				 echo $cx; 
				 ?>
				 </div>
				 <br />
				 <label style="float: left;" for="id_<?php echo htmlentities($k2); ?>"><input type="checkbox" name="pair_multi_ids[]" value="<?php echo $k; ?>" class="multi_pair" id="id_<?php echo htmlentities($k2); ?>" />Add to queue</label>
				 <div style="clear: both;">
				 <input type="button" value="Pair ..." onclick="return pair('pairSingle', '<?php echo htmlentities($k); ?>');" />
				 </div>
			</fieldset>
			<?php
			$data = ob_get_clean();
			if (!empty($sk))
			{
				echo $data; 
			}
			else
			{
				$restdata .= $data; 
			}
		}
		echo $restdata; 
		//debug_zval_dump($groups); 
		//die('ok'); 
	
	
	?>
	<input type="button" value="Process Queue" onclick="return pair_multi();" />
	</form>
	<?php
 } ?>



<form action="index.php" method="post" name="innodbform" id="innodbform">
<fieldset <?php if (!empty($j15)) echo ' disabled="disabled" '; ?> >
<legend>MyISAM to InnoDB updater</legend>
<div>Please create a backup of your database before using this feature. This will will automatically alter all your tables to InnoDB or MyISAM</div>

<table>


<tr><td><input type="checkbox" value="1" name="only_if_dif" /><label>Run Alter Table only if the current definition is not equal to desired definition. Running alter table that already is assigned to the desired engine may cause optimize and recreate the table's indexes which may take some time.</label></td></tr>

<tr><td><input type="submit" value="All tables to InnoDB" onclick="submitbutton('toinnodb');"/></td></tr>

<tr><td><input type="submit" value="Virtuemart tables to InnoDB" onclick="submitbutton('vminnodb');"/></td></tr>

<tr><td><input type="submit" value="All tables to MyIsam" onclick="submitbutton('tomyisam');"/></td></tr>

<tr><td><input type="submit" value="Virtuemart tables to MyIsam" onclick="submitbutton('vmmyisam');"/></td></tr>


<tr><td><input type="submit" value="All tables to their original engine" onclick="return submitbutton('tooriginal');"/></td></tr>


</table>
<script>
function submitbutton(task)
{
  var d = document.getElementById('taskdb'); 
  if (d != null)
  d.value = task; 
  
  d2 = document.getElementById('innodbform'); 
  d2.submit(); 
  
  return false; 
}
</script>

</fieldset>


<input type="hidden" name="option" value="com_onepage" />
<input type="hidden" name="task" id="taskdb" value="display" />
<input type="hidden" name="view" value="utils" />

</form>

<fieldset><legend><?php echo JText::_('COM_ONEPAGE_GENERAL_ADVISE'); ?></legend>
<p><?php echo JText::_('COM_ONEPAGE_GENERAL_ADVISE_REDIRECT_NONWWW_TOWWW'); ?></p>
<textarea style="width: 90%; height: 100px;" readonly="readonly">
# Redirect to www
RewriteCond %{HTTP_HOST} ^[^.]+\.[^.]+$
RewriteCond %{HTTPS}s ^on(s)|
RewriteRule ^ http%1://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
</textarea><br />
<a target="_blank" href="http://stackoverflow.com/questions/12050590/redirect-non-www-to-www-in-htaccess"><?php echo JText::_('COM_ONEPAGE_GENERAL_ADVISE_REDIRECT_NONWWW_TOWWW_SOURCE'); ?></a>
</fieldset>

<?php
$error_log = @ini_get('error_log'); 
$open_base_dir = @ini_get('open_basedir');
if (empty($open_base_dir))
if (!empty($error_log))
if (file_exists($error_log))
{
?>
<fieldset><legend><?php echo JText::_('COM_ONEPAGE_PHPERRORlOG'); ?></legend>
 <a href="index.php?option=com_onepage&view=utils&task=errorlog&format=raw&tmpl=component"><?php echo JText::_('COM_ONEPAGE_VIEWPHPERRORLOG'); ?></a>
</fieldset>
<?php 
}
?>
<div><?php echo $this->results; ?></div>