<?php

if (!defined('_JEXEC'))
die('Direct Access to ' . basename(__FILE__) . ' is not allowed.');

/**
 * Calculation plugin for zip based tax rates
 *
 * @version $Id: 2.0.0
 * @package california_tax for Virtuemart 2.0.20+
 * @subpackage Plugins - Zip Based US Tax
 * @author RuposTel.com
 * @copyright Copyright (C) RuposTel.com
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *
 *
 */

if (!class_exists('VmConfig'))
{
	require(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
	VmConfig::loadConfig(); 
}
if (!class_exists('vmPlugin'))
{
if (!JFactory::getApplication()->isAdmin())
require(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'overrides'.DIRECTORY_SEPARATOR.'vmplugin.php'); 
}
if (!class_exists('vmPSplugin'))
{
	
	require(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'plugins'.DIRECTORY_SEPARATOR.'vmpsplugin.php'); 
}

if (!class_exists('vmCalculationPlugin')) require(JPATH_VM_PLUGINS.DIRECTORY_SEPARATOR.'vmcalculationplugin.php');


class plgVmCalculationCalifornia_tax extends vmCalculationPlugin {

	

	function __construct(& $subject, $config) {
		// 		if(self::$_this) return self::$_this;
		parent::__construct($subject, $config);

		$varsToPush = array();

		$this->setConfigParameterable ('calc_params', $varsToPush);

		$this->_loggable = TRUE;
		$this->tableFields = array('id', 'virtuemart_order_id', 'tax_id');
		$this->_tableId = 'id';
		$this->_tablepkey = 'id';

		
		
	}

	
	function  plgVmOnStoreInstallPluginTable($jplugin_name) {
	
	}

	public function getVmPluginCreateTableSQLData() {
		return "CREATE TABLE IF NOT EXISTS `" . $this->_tablename . "_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `virtuemart_calc_id` mediumint(1) unsigned DEFAULT NULL,
  `zip_start` mediumint(5) unsigned DEFAULT NULL,
  `zip_end` mediumint(5) unsigned DEFAULT NULL,
  `tax_rate` decimal(10,4) unsigned DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `search` (`zip_start`,`zip_end`,`virtuemart_calc_id`),
  KEY `idx_virtuemart_calc_id` (`virtuemart_calc_id`),
  KEY `zip_start` (`zip_start`),
  KEY `virtuemart_calc_id` (`virtuemart_calc_id`),
  KEY `zip_end` (`zip_end`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Table for US tax based on Zip Code' AUTO_INCREMENT=1 ;"; 
 		

	}

	/**
	 * Gets the sql for creation of the table
	 * This table is used to store order information per plugin
	 * @author RuposTel.com
	 */
	public function getVmPluginCreateTableSQL() {
		
 		return "CREATE TABLE IF NOT EXISTS `" . $this->_tablename . "` (
 			    `id` int(11) unsigned NOT NULL AUTO_INCREMENT ,
 			    `virtuemart_calc_id` mediumint(1) UNSIGNED DEFAULT NULL,
				`zip_start` mediumint(5) UNSIGNED DEFAULT NULL,
				`zip_end` mediumint(5) UNSIGNED DEFAULT NULL,
				`tax_rate` decimal(10,4) UNSIGNED DEFAULT NULL,
 			    `created_on` datetime NOT NULL default '0000-00-00 00:00:00',
 			    `created_by` int(11) NOT NULL DEFAULT 0,
 			    `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
 			    `modified_by` int(11) NOT NULL DEFAULT 0,
 			    `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
 			    `locked_by` int(11) NOT NULL DEFAULT 0,
 			     PRIMARY KEY (`id`),
 			     KEY `idx_virtuemart_calc_id` (`virtuemart_calc_id`)
 			) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Table for US tax based on Zip Code' AUTO_INCREMENT=1 ;";
			//ALTER TABLE  `rupostel_vm2onj2`.`rg6ma_virtuemart_calc_plg_california_tax_config` ADD UNIQUE  `search` (  `zip_start` ,  `zip_end` ,  `virtuemart_calc_id` )
	}


	function plgVmAddMathOp(&$entryPoints){
	    
 		$entryPoints[] = array('calc_value_mathop' => 'zip_tax', 'calc_value_mathop_name' => 'Zip Range Based Tax');
	}

	function plgVmOnDisplayEdit(&$calc,&$html){
		// check table: 
		$sql = $this->getVmPluginCreateTableSQLData(); 
		$db = JFactory::getDBO(); 
		$db->setQuery($sql); 
		$db->query(); 
		$e = $db->getErrorMsg(); 
		if (!empty($e))
		$html.= 'Error creating tables: '.$e.'<br />'; 
		
		JHTML::script('helper.js', 'plugins/vmcalculation/california_tax/', false);
		
		$q = 'select * from '.$this->_tablename.'_config where virtuemart_calc_id = '.$calc->virtuemart_calc_id.' order by zip_start asc'; 
		$db->setQuery($q); 
		$res = $db->loadAssocList(); 
		
		$html .= '<script type="text/javascript">
//<![CDATA[
     var line_iter = '.(count($res)).'; var myline = \'';
$jsline = '<tr id="trid_{num}"><td><input type="text" onchange="javascript: wasChanged({num})" id="zip_start{num}" name="zip_start{num}" value="zip_start_{num}" /></td><td><input type="text" name="zip_end{num}" onchange="javascript: wasChanged({num})" value="zip_end_{num}" /></td><td><input onchange="javascript: wasChanged({num})" type="text" name="tax_rate{num}" value="tax_rate_{num}" /></td><td><button href="#" onclick="javascript: return op_new_line(myline,\\\'tax_rates\\\');">Add more...</button></td></tr>';

	 $jsline2 = str_replace('zip_start_{num}', '', $jsline);
     $jsline2 = str_replace('zip_end_{num}', '', $jsline2);
     $jsline2 = str_replace('tax_rate_{num}', '0', $jsline2);	 
	 $html .= $jsline2.'\'; 
//]]>
</script>';
		
		$html .= '<fieldset>
		This extension was made for you by <a href="http://www.rupostel.com/">RuposTel.com</a>
	<legend>'.JText::_('VMCALCULATION_CALIFORNIA_TAX').'</legend>
	<table class="admintable" id="tax_rates">
	<tr><th>'
	.JText::_('VMCALCULATION_CALIFORNIA_TAX_ZIP_START')
	.'</th><th>'
	.JText::_('VMCALCULATION_CALIFORNIA_TAX_ZIP_END')
	.'</th><th colspan="2">'
	.JText::_('VMCALCULATION_CALIFORNIA_TAX_TAX_RATE')
	.'</th></tr>';
	
    if (!empty($res))
	{
	foreach ($res as $k=>$row)
	 {
	   $rowhtml = str_replace('zip_start_{num}', $row['zip_start'], $jsline); 
	   $rowhtml = str_replace('zip_end_{num}', $row['zip_end'], $rowhtml);
	   $rowhtml = str_replace('tax_rate_{num}', $row['tax_rate'], $rowhtml);
	   $rowhtml = str_replace("\'", "'", $rowhtml);
	   $rowhtml = str_replace('{num}', $k, $rowhtml); 
	   $rowhtml .= '<input type="hidden" name="waschanged'.$k.'" id="waschanged'.$k.'" value="NO;'.$row['id'].'" />'; 
	   
	  
	   $html .= $rowhtml; 
	 }
	 }
	 
	  {
	     
	     $rowhtml = str_replace("\'", "'", $jsline2);
		  $rowhtml = str_replace('{num}', count($res), $rowhtml); 
		 $html .= $rowhtml; 
	  }
	
		

		$html .= '</table>';
		$html .= '<input type="hidden" name="last_iter" id="last_iter" value="'.count($row).'" />';
		
		$html .= '<input type="hidden" name="my_calc_id" id="my_calc_id" value="'.$calc->virtuemart_calc_id.'" />';
		
		if ($calc->activated) {
			
		}
		$html .= '</fieldset>';
		return TRUE;
	}

	static $qcache; 
	private function getResult($q)
	{
	  if (!isset(self::$qcache)) self::$qcache = array(); 
	  $hash = md5($q); 
	  if (isset(self::$qcache[$hash])) return self::$qcache[$hash]; 
	  $db = JFactory::getDBO(); 
	  $db->setQuery($q); 
	  $res = $db->loadResult(); 
	  self::$qcache[$hash] = $res; 
	  $e = $db->getErrorMsg(); 
	  if (!empty($e)) { echo $e; die(); }
	  return $res; 
	}
	
    public function getTaxRate($zip, $id)
	{
	  $id = (int)$id; 
	  $q = 'select tax_rate from '.$this->_tablename.'_config where zip_start <= '.$zip.' and zip_end >= '.$zip.' and virtuemart_calc_id = '.$id.' limit 0,1'; 
		  //echo $q; die(); 
		  $tax_rate = $this->getResult($q); 
		  return $tax_rate;
	}
	
	public function plgVmInterpreteMathOp ($calculationHelper, $rule, $price,$revert){
		
		$rule = (object)$rule;
		$mathop = $rule->calc_value_mathop;
		$tax = 0.0;
		$id = $rule->virtuemart_calc_id; 
		
		//var_dump($rule); die(); 
		if ($mathop=='zip_tax')
		{
		  $cart = VirtueMartCart::getCart();
		  $address = (($cart->ST == 0) ? $cart->BT : $cart->ST);
		  $zip = preg_replace("/[^0-9 ]/", '', $address['zip']); 
		  $zip = (int)$zip; 
		  
		  
		  $tax_rate = $this->getTaxRate($zip, $id);
		  //var_dump($rule); die(); 
		 
		  if (empty($zip) || ($tax_rate === NULL))
		   {
		   
		     $tax_rate = $rule->calc_value; 
		   }
		  $tax_rate = (float)$tax_rate; 
		  if (!empty($tax_rate))
		   {
		     $tax = $price * ($tax_rate / 100);
		   }
		}
		if($revert){
			$tax = -$tax;
		}
		
		return $price + (float)$tax;
		

		
	}

	function plgVmConfirmedOrder ($cart, $order) {

		}
	


	/**
	 * We can only calculate it for the productdetails view
	 * @param $calculationHelper
	 * @param $rules
	 */
	public function plgVmInGatherEffectRulesProduct(&$calculationHelper,&$rules){

		//If in cart, the tax is calculated per bill, so the rule per product must be removed
		if($calculationHelper->inCart){
			foreach($rules as $k=>$rule){
				if($rule['calc_value_mathop']=='zip_tax'){
					unset($rules[$k]);
				}
			}
		}
	}



	public function plgVmStorePluginInternalDataCalc(&$data){
	
		$last_iter = (int)$data['last_iter']; 
		$myid = (int)$data['my_calc_id']; 
		if (empty($myid)) return;
		$db = JFactory::getDBO(); 
		for($i=0; $i<=$last_iter; $i++)
		 {
		 
		    $zip_start = (int)($data['zip_start'.$i]); 
			
			
			$zip_end = (int)($data['zip_end'.$i]); 
			$tax_rate = (float)$data['tax_rate'.$i]; 
			if (!isset($data['waschanged'.$i]))
			 {
			 
			 if (($zip_start == '') || ($zip_end == '')) continue; 
			 
			   // new insert
			   $q = "insert into `".$this->_tablename.'_config` (id, virtuemart_calc_id, zip_start, zip_end, tax_rate) values (NULL, "'.$myid.'", "'.$db->escape($zip_start).'", "'.$zip_end.'", "'.$tax_rate.'")';
			   
			   $db->setQuery($q); 
			   $db->query(); 
			   $e = $db->getErrorMsg(); 
			   if (!empty($e)) { echo $e; die(); }
			  
			 }
			 else
			 {
			    $a = explode(';', $data['waschanged'.$i]); 
				$id = (int)$a[1]; 
				 
				if ($a[0] == 'YES')
				{
			    if (($zip_start == '') || ($zip_end == ''))
				 {
				   $q = 'delete from '.$this->_tablename.'_config where id = '.$id.' limit 1'; 
				   $db->setQuery($q); 
				   $db->query(); 
				    if (!empty($e)) { echo $e; die(); }
					
				 }
				 else
				 {
				   $q = 'update '.$this->_tablename.'_config set zip_start = "'.$zip_start.'", zip_end="'.$zip_end.'", tax_rate = "'.$tax_rate.'" where id = '.$id.' limit 1'; 
				   $db->setQuery($q); 
				   $db->query(); 
				   
				    if (!empty($e)) { echo $e; die(); }
				   
				 }
				}
				 
			 }
			
		 }
		
		//$table = $this->getTable('calcs');
		if (!class_exists ('TableCalcs')) {
			require(JPATH_VM_ADMINISTRATOR .DIRECTORY_SEPARATOR. 'tables' .DIRECTORY_SEPARATOR. 'calcs.php');
		}
		/*

		$db = JFactory::getDBO ();
		$table = new TableCalcs($db);
		$table->setUniqueName('calc_name');
		$table->setObligatoryKeys('calc_kind');
		$table->setLoggable();
		$table->setParameterable ($this->_xParams, $this->_varsToPushParam);
		$table->bindChecknStore($data);
		*/
	}

	public function plgVmGetPluginInternalDataCalc(&$calcData){

		
		return TRUE;

	}

	public function plgVmDeleteCalculationRow($id){
		$this->removePluginInternalData($id);
	}

	public function plgVmOnUpdateOrderPayment($data,$old_order_status){


	


	}

}

// No closing tag
