<?php
/**
 * @version		$Id: opc.php$
 * @copyright	Copyright (C) 2005 - 2014 RuposTel.com
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');



if (!class_exists('VmConfig'))
{
	require(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
	VmConfig::loadConfig(); 
}
if (!class_exists('vmPlugin'))
{
if (!JFactory::getApplication()->isAdmin())
require(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'overrides'.DIRECTORY_SEPARATOR.'vmplugin.php'); 
}
if (!class_exists('vmPSplugin'))
{
	
	require(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'plugins'.DIRECTORY_SEPARATOR.'vmpsplugin.php'); 
}
class plgVmShipmentOpc_shipping_last extends vmPSPlugin {

	/**
	 * @param object $subject
	 * @param array  $config
	 */
	function __construct (& $subject, $config) {

		parent::__construct ($subject, $config);
/*
		$this->_loggable = TRUE;
		$this->_tablepkey = 'id';
		$this->_tableId = 'id';
		$this->tableFields = array(); 
		$varsToPush = array(); 
		$this->setConfigParameterable ($this->_configTableFieldName, $varsToPush);
		//vmdebug('Muh constructed plgVmShipmentWeight_countries',$varsToPush);
	*/
	}



	
	// vm3.0.6 forces automatic shipment and payment even when disabled: 
	public function plgVmOnCheckAutomaticSelectedShipment($cart, $prices, &$counter)
	{
		
		if (!class_exists('plgSystemOpc')) return; 
		if (empty($counter)) $counter = 1; 
		return "-0"; 
	}
	public function plgVmOnCheckAutomaticSelectedPayment($cart, $prices, &$counter)
	{
		if (!class_exists('plgSystemOpc')) return; 
		if (empty($counter)) $counter = 1; 
		return "-0"; 
	}
	
	function plgVmgetEmailCurrency($virtuemart_paymentmethod_id, $virtuemart_order_id, &$emailCurrencyId) {

		require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_onepage'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
		
		$default = false; 
		$config = OPCconfig::get('override_payment_currency', $default); 
		$virtuemart_order_id = (int)$virtuemart_order_id; 
		
		
		if (!empty($virtuemart_order_id))
		if (!empty($config))
		{
			$db = JFactory::getDBO(); 
			$q = 'select `user_currency_id` from #__virtuemart_orders where virtuemart_order_id = '.(int)$virtuemart_order_id; 
		    $db->setQuery($q); 
			$c = $db->loadResult(); 
			if (!empty($c))
			{
				$emailCurrencyId = $c; 
				return $c; 
			}
			
		}
		

	}
	
	
}	
	