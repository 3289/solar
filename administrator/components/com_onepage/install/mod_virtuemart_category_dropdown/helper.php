<?php
defined('_JEXEC') or die;

class modVirtuemartCategorydropdownHelper {
   
   
   public static function getCats($cat_id=0, $selected=0, &$hasResults, $level=0)
   {
		$hasResults = false; 
	   $ret2 = ''; 
	   if (empty($cat_id))
	   
	  $cat_id = (int)$cat_id; 
	  
	 
	    {
		
		   $list = self::getTopCats($cat_id); 
		   
		   

		  $ret = ''; 
		    $myid = ''; 
		    $mid = JRequest::getVar('my_item_id', ''); 
			if (!empty($mid))
		    $myid = '&my_item_id='.$mid; 
			//if (!empty($cat_id))
			{
				
			// $level = JRequest::getVar('level', 0); 
			// $level = (int)$level; 
			// $level++; 
			 $key = 'MOD_VIRTUEMART_CATEGORY_DROPDOWN_LEVEL2'; 
			 if (!empty($level))
			 {
				 $key = 'MOD_VIRTUEMART_CATEGORY_DROPDOWN_LEVEL'.$level; 
			 }
			 $ret = '<option>'.JText::_($key).'</option>'; 
			}
		   if (!empty($list))
				{
				   foreach ($list as $rid => $row)
				     {
					    $ret .= '<option '; 
						if (!empty($selected))
						{
							$selected = (int)$selected; 
							$cat_id = (int)$row['virtuemart_category_id']; 
							if ($selected === $cat_id)
							{
								$ret .= ' selected="selected" '; 
							}
							
							
						}
						$ret .= ' value="'.$row['virtuemart_category_id'].'" rel="'.JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id='.$row['virtuemart_category_id'].$myid).'">'.$row['category_name'].'</option>'; 
					 }
					 $hasResults = true; 
				}
				
				
		  
		}
		if ((empty($cat_id) && (!$hasResults)))
		{
			
			//$ret = '<option>'.JText::_('MOD_VIRTUEMART_CATEGORY_DROPDOWN_LEVEL1').'</option>'; 
		}
		
		if (empty($ret))
		{
			//$ret = '<option class="no_subcat_here">'.JText::_('MOD_VIRTUEMART_CATEGORY_DROPDOWN_NOMODELS').'</option>'; 
		}
		else
		{
			//$ret = $ret2.$ret; 
		}
		return $ret; 
   }
   
   public static function getProducts($cat_id=0, $prod_id=0)
   {
		
	   $ret2 = ''; 
	   if (empty($cat_id))
	   $cat_id = JRequest::getVar('ict', 0); 
	  $cat_id = (int)$cat_id; 
	  
	  $maxlevel = JRequest::getVar('maxlevel', 0); 
	   $level = JRequest::getVar('level', 0); 
	  
	  if (!empty($cat_id))
	    {
		
		
		   $db = JFactory::getDBO(); 
		   $q = 'select l.product_name as product_name, r.virtuemart_product_id from #__virtuemart_products_'.VMLANG.' as l, #__virtuemart_product_categories as r where l.virtuemart_product_id = r.virtuemart_product_id and r.virtuemart_category_id = '.$cat_id.' order by REPLACE(l.product_name, "&#39;", "\'") asc'; 
		   
		   $db->setQuery($q); 
		   
		   $list = $db->loadAssocList(); 
		  $ret = ''; 
		    $myid = ''; 
		    $mid = JRequest::getVar('my_item_id', ''); 
			if (!empty($mid))
		    $myid = '&my_item_id='.$mid; 
			 $ret2 = '<option>'.JText::_( 'MOD_VIRTUEMART_CATEGORY_DROPDOWN_LEVEL'.$level).'</option>'; 
		   if (!empty($list))
				{
				   foreach ($list as $rid => $row)
				     {
					    $ret .= '<option class="is_product_reply" '; 
						if ($row['virtuemart_product_id'] == $prod_id) $ret .= ' selected="selected" '; 
						$ret .= ' value="'.$row['virtuemart_product_id'].'" rel="'.JRoute::_('index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id='.$row['virtuemart_product_id'].'&virtuemart_category_id='.$cat_id.$myid).'">'.$row['product_name'].'</option>'; 
					 }
				}
		  
		}
		else
		{
			
			$ret = '<option>'.JText::_('MOD_VIRTUEMART_CATEGORY_DROPDOWN_LEVEL1').'</option>'; 
		}
		
		if (empty($ret))
		{
			$ret = '<option>'.JText::_('MOD_VIRTUEMART_CATEGORY_DROPDOWN_NOMODELS').'</option>'; 
		}
		else
		{
			$ret = $ret2.$ret; 
		}
		return $ret; 
   }
   public static function getLangCode()
 {
	 $langO = JFactory::getLanguage();
			$lang = JRequest::getVar('lang', ''); 
			$locales = $langO->getLocale();
		$tag = $langO->getTag(); 
		$app = JFactory::getApplication(); 		
		
		
		if (class_exists('JLanguageHelper') && (method_exists('JLanguageHelper', 'getLanguages')))
		{
		$sefs 		= JLanguageHelper::getLanguages('sef');
		foreach ($sefs as $k=>$v)
		{
			if ($v->lang_code == $tag)
			if (isset($v->sef)) 
			{
				$ret = $v->sef; 

				return $ret; 
			}
		}
		}
		
		
		
			 if ( version_compare( JVERSION, '3.0', '<' ) == 1) {       
			if (isset($locales[6]) && (strlen($locales[6])==2))
			{
				$action_url .= '&amp;lang='.$locales[6]; 
				$lang = $locales[6]; 
				return $lang; 
			}
			else
			if (!empty($locales[4]))
			{
				$lang = $locales[4]; 
				
				if (stripos($lang, '_')!==false)
				{
					$la = explode('_', $lang); 
					$lang = $la[1]; 
					if (stripos($lang, '.')!==false)
					{
						$la2 = explode('.', $lang); 
						$lang = strtolower($la2[0]); 
					}
				
					
				}
		     	return $lang; 
			}
			else
			{
				return $lang; 
			
			}
			 }
			return $lang; 
 }
   public static function getAjax()
    { 
	  
	  $ret = ''; 
	  self::loadVm();
	  
	  $selected = 0; 
	  
	  
	  $level = JRequest::getVar('level', 0); 
	  $maxlevel = JRequest::getVar('maxlevel', 0); 
	  
	  if ($level === $maxlevel)
	  {
		  $ret2 = self::getProducts(); 
	  }
	  else
	  {
	  $catId = JRequest::getVar('ict', 0); 
	  if (!empty($catId))
	  {
		  $hasResults = false; 
		  $level = (int)$level; 
		  $level++; 
		  $ret = self::getCats($catId, $selected, $hasResults, $level); 
		  
		  $showProducts = JRequest::getVar('showProducts', 1); 
		  
		  if (!empty($showProducts))
		  if (empty($hasResults))
		  {
			  
			  $ret2 = self::getProducts(); 
			  
			  
		  }
	  }
	  }
	  
	  
		
		@header('Content-Type: text/html; charset=utf-8');
		@header("HTTP/1.1 200 OK");
		@header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		@header("Cache-Control: no-cache");
		@header("Pragma: no-cache");
		if (!empty($ret2))
		{
		 @header("isProducts: 1"); 
		 echo $ret2; 
		}
		else
		{
		 echo $ret; 
		}
		JFactory::getApplication()->close(); 
	    die(''); 
	}
	
	public static function getTopCats($top=0)
	{
		$top = (int)$top; 
		$db = JFactory::getDbo();
		
		
		$q = "SELECT a.category_child_id, b.virtuemart_category_id, b.category_name FROM #__virtuemart_category_categories AS a, #__virtuemart_categories as c,  #__virtuemart_categories_".VMLANG." AS b WHERE a.category_child_id = b.virtuemart_category_id and a.category_parent_id = ".$top." and c.virtuemart_category_id = b.virtuemart_category_id and c.published = 1 ORDER BY b.category_name ASC";

		$db->setQuery($q);
		$result = $db->loadAssocList(); 
		return $result; 
	}
	
	public static function getD($params)
	{
		$c = 0;
		for ($i=1; $i<=5; $i++) { 
	$key = 'level'.$i.'_text'; 
	
	  
	$text = $params->get($key, ''); 
	
	if (empty($text)) break; 
	$c++;
		}
		return $c; 
	}
	
	
public static function getPath($id, $max=6)
{

  require_once(__DIR__.DIRECTORY_SEPARATOR.'com_virtuemart_helper.php'); 
  $h = categoryHelperDD::getInstance(); 
  $ret = $h->getCategoryRecurse($id, 0); 
  $ret = array_reverse($ret); 
  $z = array(); 
  foreach ($ret as $k=>$v)
  {
	  $zi = $k+1; 
	  $z['value'.$zi] = $v; 
  }
  return $z; 

	
	$id = (int)$id; 
	if (empty($id)) return array(); 
	$db = JFactory::getDBO(); 
		
		
		
		$q  = 'select '; 
		$qs = $qf = $qw = $or = array(); 
		
		$i2 = $max; 
		for ($i=1; $i<=$max; $i++)
		{
			$qs[$i] = ' level'.$i.'.`category_child_id` as value'.$i.' '; 
			$qf[$i] = ' #__virtuemart_category_categories as level'.$i; 
			
			$z = $i + 1; 
			if ($z <= $max)
			{
			$qw[$i] = ' level'.$i.'.category_parent_id = level'.$z.'.category_child_id ';  
			}
			else
			{
			  $qw[$i] = ' level'.$i.'.category_parent_id = 0 ';  
			}
		}
		$max2 = $max; 
		
		for ($i=1; $i<=$max2; $i++)
		for ($i2 = 1; $i2<=($max2 - $i)+1; $i2++)
		{
			 
			
			$z = $i + 1; 
			$mm = ($max2 - $i2) + 1; 
			if ($z <= $mm)
			{
			$or[$i2][$i] = ' level'.$i.'.category_parent_id = level'.$z.'.category_child_id ';  
			}
			else
			{
			  $or[$i2][$i] = ' level'.$i.'.category_parent_id = 0 ';  
			}
		}
	
	    $w = array(); 
		foreach ($or as $i=>$next)
		{
		$w[] = '( '.implode(' and ', $next).' ) '; 
		
		
		
		
		}
		$w2 = ' ( '.implode(' or ', $w).' ) '; 
		
		
		$q = 'select '.implode(', ',$qs).' from '.implode(', ', $qf).' where (level1.`category_child_id` = '.$id.') and '.$w2.'   limit 0,1'; 
		
		$db = JFactory::getDBO(); 
		$db->setQuery($q); 
		$res = $db->loadAssoc(); 
		return $res; 
}	

	


	public static function getDepth()
	{
		$db = JFactory::getDBO(); 
		
		
		
		$q  = 'select '; 
		$qs = $qf = $qw = array(); 
		$max = 6; 
		for ($i=1; $i<=$max; $i++)
		{
			$qs[$i] = ' level'.$i.'.`category_child_id` '; 
			$qf[$i] = ' #__virtuemart_category_categories as level'.$i; 
			
			$z = $i + 1; 
			if ($z <= $max)
			{
			$qw[$i] = ' level'.$i.'.category_parent_id = level'.$z.'.category_child_id ';  
			}
			else
			{
			  $qw[$i] = ' level'.$i.'.category_parent_id = 0 ';  
			}
		}
		
		$q = 'select '.implode(', ',$qs).' from '.implode(', ', $qf).' where '.implode(' and ', $qw).' limit 0,1'; 
		
		$db = JFactory::getDBO(); 
		$db->setQuery($q); 
		$res = $db->loadAssoc(); 
		/*
		var_dump($res); echo $db->getErrorMsg(); 
		
		
		$q = str_replace('#__', 'g52p3_', $q); 
		echo $q; 
		die(); 
		*/
		
		$q = 'select 
		level1.category_child_id, 
		level2.category_child_id, 
		level3.category_child_id,
		level4.category_child_id,
		level5.category_child_id		
		from 
		#__virtuemart_category_categories as level1, 
		#__virtuemart_category_categories as level2, 
		#__virtuemart_category_categories as level3,
		#__virtuemart_category_categories as level4, 
		#__virtuemart_category_categories as level5 
		where 
		level1.category_parent_id = level2.category_child_id
		and
		level2.category_parent_id = level3.category_child_id
		and 
		level3.category_parent_id = level4.category_child_id
		and
		level5.category_parent_id = 0
		limit 0,1
		'; 
	}
	
	public static function loadVm()
	{
	   $lang = JFactory::getLanguage();
	   $language_tag = $lang->getTag();
	   
//stAn, include VM: 
if (!class_exists('VmConfig'))
{
   if (!class_exists('VmConfig'))
		  require(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'config.php'); 
		 
		  VmConfig::loadConfig(true); 
		  if (method_exists('VmConfig', 'loadJLang'))
		  {
		  VmConfig::loadJLang('com_virtuemart',TRUE);
		  VmConfig::loadJLang('com_virtuemart_orders',TRUE);
		  }
		  
		 if (method_exists('VmConfig', 'loadJLang'))
		 VmConfig::loadJLang('com_virtuemart');
		 else
		  {
		     $lang = JFactory::getLanguage();
			 $extension = 'com_virtuemart';
			 $base_dir = JPATH_SITE;
			 $language_tag = $lang->getTag();
			 $reload = false;
			 $lang->load($extension, $base_dir, $language_tag, $reload);
			 
			 $lang = JFactory::getLanguage();
			 $extension = 'com_virtuemart';
			 $base_dir = JPATH_ADMINISTRATOR;
			 $language_tag = $lang->getTag();
			 $reload = false;
			 $lang->load($extension, $base_dir, $language_tag, $reload);
			 
		  }
		  
		  
}
JFactory::getLanguage()->load('mod_virtuemart_bohcatfilter'); 
JFactory::getLanguage()->load('mod_virtuemart_bohcatfilter', JPATH_SITE.DIRECTORY_SEPARATOR.'modules'.DIRECTORY_SEPARATOR.'mod_virtuemart_bohcatfilter'); 

	}
}