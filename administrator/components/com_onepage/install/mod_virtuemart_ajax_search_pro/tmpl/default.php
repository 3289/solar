<?php
/**
* @package mod_vm_ajax_search
*
* @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU/GPL, see LICENSE.php
* VM Live Product Search is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
*/
/**
*  Modified by RuposTel.com 25.6.2011
*  and renamed to mod_vm_ajax_search
*/
/**
 * VM Live Product Search
 *
 * Used to process Ajax searches on a Virtuemart 1.1.2 Products.
 * Based on the excellent mod_pixsearch live search module designed by Henrik Hussfelt (henrik@pixpro.net - http://pixpro.net)
 * @author		John Connolly <webmaster@GJCWebdesign.com>
 * @package		mod_vm_live_product
 * @since		1.5
 * @version     0.5
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
/* Load the virtuemart main parse code */


/*
$style = '
 #vm_ajax_search_results2'.$myid.' {margin-left:'.$params->get('offset_left_search_result').'px;margin-top:'.$params->get('offset_top_search_result').'px;}
';


$document->addStyleDeclaration($style); 		
*/
$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base().'modules/mod_virtuemart_ajax_search_pro/css/mod_vm_ajax_search.css'); 
$css = ''; 
$max_h = $params->get('results_max_height'); 
if (!empty($max_h))
{
$css .= '
 div.res_a_s {
  max-height: '.$params->get('results_max_height').';  
 }	 

'; 
}

if (!empty($min_height)) {
$css .= '
 div.vmlpsearch'.$params->get('moduleclass_sfx', '').' {
  max-height: '.$min_height.'px;  
 }	 

'; 
}

if (empty($tw)) $tw = 272; 
if (!empty($tw))
{
	$css .= '
	input#vm_ajax_search_search_str2'.$myid.' {
	   width: '.$tw.'px;
	}
	';
}

if (empty($tw)) $tw = 272; 
if (!empty($tw))
{
	$css .= '
	div#vm_ajax_search_results2'.$myid.' {
	   position: '.$params->get('css_position', 'absolute').';
	   z-index: 999;
	   width: '.$results_width.';
	}
	';
}


$document->addStyleDeclaration($css);






$min_height = $params->get('min_height');



?>
<div class="ekon_search" >
<div name="pp_search<?php echo $myid ?>" id="pp_search2_<?php echo $myid ?>" action="<?php echo JRoute::_('index.php'); ?>" method="get">
<div class="vmlpsearch<?php echo $params->get('moduleclass_sfx'); ?>" >
<div id="results_re_2<?php echo $myid ?>">
</div>
	<div class="vm_ajax_search_pretext"><?php echo $params->get('pretext'); ?></div>

	<?php
		$search = JText::_('COM_VIRTUEMART_SEARCH');
		// can set this also to: JText::_('SEARCH');
		
		$search = addslashes($search);
		$include_but = $params->get('include_but');
		$tw = $params->get('text_box_width'); 
		$opt_search = $params->get('optional_search');
		
		$no_ajax = $params->get('no_ajax', false); 
		
	?>
	 <div class="aj_label_wrapper ajax_search_pro sourcecoast" >
	 <form id="rup_search_form" action="<?php 
	 
	 
	 //echo JRoute::_('index.php?option=com_rupsearch&view=search&nosef=1'); 
	 echo JRoute::_('index.php'); 
	 ?>">
	 <input type="hidden" value="com_rupsearch" name="option" />
	 <input type="hidden" value="search" name="view" />
	 <input type="hidden" value="1" name="nosef" />
	 <input type="hidden" value="<?php echo $clang; ?>" name="lang" />
	 <input type="hidden" value="<?php echo JFactory::getLanguage()->getTag(); ?>" name="language" />
	 <input type="hidden" value="<?php echo $params->get('order_by'); ?>" name="order_by" />
	 <div class="input-prepend">
      
	  <input placeholder="<?php echo $search;  ?>" class="inputbox_vm_ajax_search_search_str2 span2 inactive_search" id="vm_ajax_search_search_str2<?php echo $myid ?>" name="keyword" type="text" value="<?php 
	  $keyword = $keyword = JRequest::getVar('keyword'); 
	  if (!empty($keyword)) echo str_replace('"', '\"', $keyword); 
	 
	 ?>" autocomplete="off" <?php if (empty($no_ajax)) { ?> onblur="javascript: return search_setText('', this, '<?php echo $myid ?>');" onfocus="javascript: aj_inputclear(this, '<?php echo $params->get('number_of_products'); ?>', '<?php echo $clang; ?>', '<?php echo $myid; ?>', '<?php echo $url ?>');" onkeyup="javascript:search_vm_ajax_live(this, '<?php echo $params->get('number_of_products'); ?>', '<?php echo $clang; ?>', '<?php echo $myid; ?>', '<?php echo $url ?>', '<?php echo $params->get('order_by'); ?>'); "   <?php } ?>/>
	 <div class="search-button"><span class="icon-search"></span></div>
	 </div>
	 <div class="aj_search_radio" >
	 <?php switch ($opt_search) {  
		case 0: ?>
		<input type="checkbox" id="optional_search" name="opt_search" value="1" title="<?php echo JText::_('COM_VIRTUEMART_SEARCH_TITLE_DESC');?>"/>
	<?php
			break;
		case 1:
			echo '<input type="hidden" id="optional_search" name="opt_search" value="0" />';
			break;
		case 2: 
			echo '<input type="hidden" id="optional_search" name="opt_search" value="1" />';
			break;
		 } ?>
	
		 
	 </div>
	    
	 
	 
		
		<input type="hidden" name="module_id" value="<?php echo (int)$myid; ?>" />
		
		<input type="hidden" name="view" value="search" />
		<input type="hidden" name="limitstart" value="0" />
		
	
	<?php if (!empty($include_but)) 
	{
	 $st = ' style="display: block; "';
	 echo '<button class="btn btn-primary button_ajax_search_old" name="Search" '.$st.'>'.$search.'</button>';
	 ?>
	 
	 <?php
	}
	else
    {	
	$st = 'style="display: none;"';
	echo '<input class="btn btn-primary button_ajax_search_old" type="submit" value="" name="Search" '.$st.'/>';
	?><?php
	}
		
		
		
		
		?>
		<input type="hidden" name="view" value="search" />
		<input type="hidden" name="option" value="com_rupsearch" />
		
		
		
		</form>
		<div id="results_position_<?php echo (int)$myid; ?>" class="results_position_x">&nbsp;</div>	
	<?php $postt = $params->get('posttext'); 
	
	if (!empty($postt))
	{
	?>
	<div class="vm_ajax_search_posttext" style="clear: both;"><?php echo $postt; ?></div>
	<?php 
	
	}
	?>
    </div>
</div>
</div>
 


</div>

<?php
if (class_exists('vmJsApi'))
	if (method_exists('vmJsApi', 'jPrice'))
		vmJsApi::jPrice();