<?php
/**
* @package mod_vm_ajax_search
*
* @license		http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU/GPL, see LICENSE.php
* VM Live Product Search is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
*/

/*
* Modified by rupostel.com team. 
* Original extensions:
*/

/**
 * VM AJAX Product Search
 *
 * Used to process Ajax searches on a Virtuemart 1.1.9 Products.
 * Based on the excellent mod_pixsearch live search module designed by Henrik Hussfelt (henrik@pixpro.net - http://pixpro.net)
 * Based on mod_vm_live_product from John Connolly <webmaster@GJCWebdesign.com
 *
 * @author		Stan Scholtz <info@rupostel.com>
 * @package		mod_vm_ajax_search
 * @since		1.5
 * @version     1.0.0
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$app = JFactory::getApplication(); 
$router = $app->getRouter();
$sef_mode = $router->getMode(); 
$router->setMode(0);

// prepare CSS includes: 
if (file_exists(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_rupsearch'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php'))
{


  //includes virtuemart
  require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_rupsearch'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php'); 
  RupHelper::getIncludes(); 
  $doc = JFactory::getDocument(); 
  /*
  $title = $doc->getTitle(); 
  $kw = $document->getMetaData('keywords');
  $desc =	$document->getDescription( );
  $robot = $document->getMetaData('robots');
  $uth = $document->getMetaData('author');
   */
  $hd = $doc->getHeadData(); 
  
  RupHelper::renderHidden(); 
  
  $hd2 = $doc->getHeadData(); 
  $hd3 = array(); 
  foreach ($hd2 as $key=>$val)
    {
	   switch ($key)
	   {
	    case 'title': 
			$hd3[$key] = $hd[$key]; 
		     break; 
	    case 'description': 
			$hd3[$key] = $hd[$key]; 
		     break; 
	    case 'link':
			$hd3[$key] = $hd[$key]; 
		     break; 
	    case 'metaTags': 
			$hd3[$key] = $hd[$key]; 
		     break; 
	    case 'links':
			$hd3[$key] = $hd[$key]; 
		     break; 
	    case 'styleSheets': 
			$hd3[$key] = $hd2[$key]; 
		     break; 
	    case 'style': 
			$hd3[$key] = $hd2[$key]; 
		     break; 
	    case 'scripts': 
			$hd3[$key] = $hd2[$key]; 
		     break; 
	    case 'script': 
			$hd3[$key] = $hd2[$key]; 
		     break; 
	    case 'custom':
			$hd3[$key] = $hd2[$key]; 
		     break; 
		default:
		  $hd3[$key] = $hd2[$key]; 
		     break; 
			 
		}

			 
	}
  $doc->setHeadData($hd3); 
  //var_dump($hd3); die(); 
  /*
  $doc->setTitle($title); 
  $doc->setMetaData('title',  $title);
  $document->setDescription( $desc );
  $document->setMetaData('keywords', $kw);
  $document->setMetaData('robots', $robot);
  $document->setMetaData('author', $uth);
  */
}


/* load language */ 
		
		



 // load virtuemart language files
$jlang =JFactory::getLanguage();
$jlang->load('com_virtuemart', JPATH_SITE, $jlang->getDefault(), true);
$jlang->load('com_virtuemart', JPATH_SITE, null, true);




$lang = JFactory::getLanguage();
$extension = 'com_search';
$base_dir = JPATH_SITE;
$language_tag = $lang->getTag(); 
$lang->load($extension, $base_dir, $language_tag, true);


 $langO = JFactory::getLanguage();
			$clang = JRequest::getVar('lang', ''); 
			$locales = $langO->getLocale();
		$tag = $langO->getTag(); 
		$app = JFactory::getApplication(); 		
		
		if (empty($clang))
		if (class_exists('JLanguageHelper') && (method_exists('JLanguageHelper', 'getLanguages')))
		{
		$sefs 		= JLanguageHelper::getLanguages('sef');
		foreach ($sefs as $k=>$v)
		{
			if ($v->lang_code == $tag)
			if (isset($v->sef)) 
			{
				$ret = $v->sef; 

				$clang = $ret; 
				
				
				
			}
		}
		}
		
		
		
			 if ( version_compare( JVERSION, '3.0', '<' ) == 1) {       
			if (isset($locales[6]) && (strlen($locales[6])==2))
			{
				
				$clang = $locales[6]; 
				
			}
			else
			if (!empty($locales[4]))
			{
				$clang = $locales[4]; 
				
				if (stripos($clang, '_')!==false)
				{
					$la = explode('_', $clang); 
					$clang = $la[1]; 
					if (stripos($clang, '.')!==false)
					{
						$la2 = explode('.', $clang); 
						$clang = strtolower($la2[0]); 
					}
				
					
				}
		     	
			}
			
			 }
			




if (empty($module->params))
{
$q = 'select params from #__modules where id = "'.$myid.'" '; 
$db = JFactory::getDBO();
$db->setQuery($q); 
$res = $db->loadResult(); 
}
else
{
$res = $module->params; 
}


if (!empty($res))
if (class_exists('JParameter'))
$params = new JParameter( $res );
else
if (class_exists('JRegistry'))
$params = new JRegistry( $res );
else
$params = $this->params; 

$prods = $params->get('number_of_products'); 

$myid = $module->id;

// we start with zero
if (empty($prods)) $prods = 4; 
//$url = JRoute::_('index.php?nosef=1&format=opchtml'); //JURI::base().'modules/mod_vm_ajax_search/ajax/index.php';
$url = JURI::base().'index.php'; 
$child_handling = $params->get('child_products', 0); 
$internal_caching = $params->get('internal_caching', 0); 



$path = JModuleHelper::getLayoutPath('mod_virtuemart_ajax_search_pro', 'default_js'); 
require($path);
$path = JModuleHelper::getLayoutPath('mod_virtuemart_ajax_search_pro'); 
require($path);


$app = JFactory::getApplication(); 
$router = $app->getRouter();
$router->setMode($sef_mode);
