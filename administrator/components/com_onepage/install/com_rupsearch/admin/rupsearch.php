<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_content
 * @copyright	Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;

$css = '
div.subhead-collapse {
display: none; 
}
header { display: none; }
img.logo { display: none; }
'; 
JFactory::getDocument()->addStyleDeclaration($css); 
$db = JFactory::getDBO(); 
$q = 'select * from #__com_rupsearch_stats order by `count` desc limit 0,500'; 
$db->setQuery($q); 
$res = $db->loadAssocList(); 
?><h1>Top 500 keywords</h1>
<?php
if (!empty($res))
{
	?>
	<table class="adminTable table">
	<?php
	foreach ($res as $row)
	{
		?><tr><td><?php echo $row['keyword']; ?></td><td><?php echo $row['count']; ?></td></tr>
		<?php
	}
	?>
	</table>
	<?php
}