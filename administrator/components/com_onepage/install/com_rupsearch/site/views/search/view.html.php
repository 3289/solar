<?php
/**
 * @package		RuposTel Ajax search pro
 * @subpackage	com_content
 * @copyright	Copyright (C) 2005 - 2014 RuposTel.com
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */


defined('_JEXEC') or die;
	 if (!class_exists('VmConfig'))	  
	 {
	  require(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
	  VmConfig::loadConfig(); 
	 }

if (!class_exists('VirtuemartViewCategory')) require(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'category'.DIRECTORY_SEPARATOR.'view.html.php');

class RupsearchViewSearch extends VirtuemartViewCategory
{
	
	function display($tpl = null)
	{
		jimport( 'joomla.registry.registry' );
	    $document = JFactory::getDocument();
		require_once(JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'helper.php'); 
		RupHelper::updateStats(); 
		RupHelper::getIncludes(); 
		
		if (class_exists('vmJsApi'))
		{
			if (method_exists('vmJsApi', 'jQuery'))
				vmJsApi::jQuery();
			if (method_exists('vmJsApi', 'jSite'))
				vmJsApi::jSite();
			if (method_exists('vmJsApi', 'cssSite'))
				vmJsApi::cssSite(); 
		}
		
		$id = JRequest::getVar('module_id', 0); 
		$params = RupHelper::getParams($id); 
		/*
		if (!empty($id))
		 {
		    $id = (int)$id; 
			$q = 'select params from `#__modules` where id = '.$id.' limit 1'; 
			
			$db = JFactory::getDBO(); 
			$db->setQuery($q); 
			$params_s = $db->loadResult(); 
			$params = new JRegistry($params_s); 
			
		 }
		 */
		
		$keyword = JRequest::getVar('keyword'); 
		if (!isset($params) || (!is_object($params)))
		{
		$prods = JRequest::getInt('prods', 5); 
		$cache = (bool)JRequest::getVar('internal_caching', true); 
		}
		else
		{
		  $prods = (int)$params->get('number_of_products', 5); 
		  $cache = (bool)$params->get('internal_caching', false); 
		}
		
		$po = (int)$prods; 
		
		$limit = JRequest::getVar('limit', $prods); 
		if (empty($limit)) $limit = $prods; 
		$limit = (int)$limit; 
		$prods = $limit; 
		
		
		$current_position = $limit; 
		
		$popup = JRequest::getInt('popup', false); 
		
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');
		$cache_dir = JPATH_SITE.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.'com_rupsearch'; 
		$orderBy = JRequest::getVar('order_by', ''); 
		if (is_writable($cache_dir))
		{
		if (!file_exists($cache_dir))
		 {
		   JFolder::create($cache_dir); 
		 }
		 
		
		$md5 = md5($keyword.'_'.$orderBy); 
		$file = JFile::makesafe($keyword); 
		$cache_file = $cache_dir.DIRECTORY_SEPARATOR.$file.'_'.$md5.'.html'; 
		}
		else
		{
			$cache = false; 
		}
		
		if ((empty($cache)) || ((!empty($cache_file)) && (!file_exists($cache_file))))
		{
        		
		$ids = RupHelper::getProducts($keyword, $prods, $popup, $orderBy); 
		
		
		$next = false; 
		
		if (count($ids) > $prods)
		{
		  $next = true; 
		}
		
		//$ids = array_pop($ids); 
		$i=0; 
		foreach ($ids as $k=>$v)
		{
		  $i++; 
		  if ($i > $prods) unset($ids[$k]); 
		}
		
		
		$show_prices  = VmConfig::get('show_prices',1);
		
		$vars = array(); 
		$vars['show_prices'] = $show_prices;
	    $vars['number_of_prods'] = $prods; 
		$vars['showproducts'] = true; 
		$vars['productsLayout'] = VmConfig::get('productsublayout','products');
		if(empty($vars['productsLayout'])) $vars['productsLayout'] = 'products';
		// add javascript for price and cart, need even for quantity buttons, so we need it almost anywhere
		vmJsApi::jPrice();

		

		

		
		$categoryModel = VmModel::getModel('category');
		$productModel = VmModel::getModel('product');


		
		//$search = VmRequest::uword('keyword', null);
		$vars['searchcustom'] = '';
		$vars['searchcustomvalues'] = '';
		if (!empty($keyword)) {
			$vars['searchcustom'] = $this->getSearchCustom();
			$search = $keyword;
		} else {
			$keyword ='';
			$search = NULL;
		}
		$vars['search'] = null;
		$vars['keyword'] = $keyword;


		$categoryId = JRequest::getInt('virtuemart_category_id', false);
		$virtuemart_manufacturer_id = JRequest::getInt('virtuemart_manufacturer_id', false );
	

		
				$category = new stdClass();
				$category->category_name = '';
				$category->category_description= '';
				$category->haschildren= false;
				$category->children = array();
				$category = $categoryModel->getCategory(0);
				
				$perRow = empty($category->products_per_row)? VmConfig::get('products_per_row',3):$category->products_per_row;
				
				$vars['perRow'] =  $perRow;
				require_once(JPATH_COMPONENT.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagination.php'); 
				
				
				
				 
				

				$ratingModel = VmModel::getModel('ratings');
				$showRating = $ratingModel->showRating();
				$productModel->withRating = $showRating;

				$vars['showRating']= $showRating;

				$products = $productModel->getProducts ($ids);
				$count = count($ids); 
				$rp = new rupPagination($count, 0, $prods , $perRow );
				$vars['vmPagination'] = $rp;
				
				
				//$products = $productModel->getProductsInCategory($categoryId);
				$productModel->addImages($products,1);

				$vars['products'] = $products;

				if ($products) {
					$currency = CurrencyDisplay::getInstance( );
					$vars['currency'] =  $currency;
					foreach($products as $product){
						$product->stock = $productModel->getStockIndicator($product);
					}
				}



				//$orderByList = $productModel->getOrderByList($categoryId);
				
				$orderByList = array(); //$productModel->getOrderByList($categoryId);
				$orderByList['orderby'] = ''; 
				$orderByList['manufacturer'] = ''; 				
				$vars['orderByList'] = $orderByList;

				

				
				$showBasePrice = false; 
				$vars['showBasePrice'] = $showBasePrice;

			

			
			


			$categoryModel->addImages($category,1);

			
			$categoryModel->addImages($category->children,1);
			if (method_exists('shopFunctionsF', 'triggerContentPlugin'))
			if (VmConfig::get('enable_content_plugin', 0)) {
				shopFunctionsF::triggerContentPlugin($category, 'category','category_description');
			}
			



			$app = JFactory::getApplication(); 
			$menus	= $app->getMenu();
			$menu = $menus->getActive();
			if(!empty($menu->query['categorylayout']) and $menu->query['virtuemart_category_id']==$categoryId){
				$category->category_layout = $menu->query['categorylayout'];
			}
			shopFunctionsF::setVmTemplate($this,$category->category_template,0,$category->category_layout);
		
		

		$vars['category'] =  $category;
		
		if ($next)
		{
			
		$prods2 = $params->get('number_of_products', $prods); 
		$p = JRequest::getVar('limit', $prods2); 
		$p = (int)$p; 
		$p = $p+$prods2; 
		$limit = $p;
		
	    $vars['next_link'] = 'index.php?keyword='.urlencode($keyword).'&opt_search=1&module_id='.$id.'&view=search&limitstart=0&limit='.$limit.'&view=search&option=com_rupsearch';
		
		$vars['current_position'] = $current_position; 
		
		
		
		}
		// Override Category name when viewing manufacturers products !IMPORTANT AFTER page title.
		if (JRequest::getInt('virtuemart_manufacturer_id' ) and !empty($products[0]) and isset($category->category_name)) $category->category_name =$products[0]->mf_name ;
		
		if (!empty($cache) && (!empty($cache_file)))
		$this->cache_file = $cache_file; 
		else $this->cache_file = ''; 
		
		$categoryView = RupHelper::getVMView($this, 'category', $vars, 'category', 'default', 'html'); 
		
		
		ob_start(); 
		
		echo $categoryView; 
		
		
	
		if (!empty($vars['next_link']))
		{
			
			$add_next = $params->get('add_next', false); 
			
		if (!empty($add_next))
		{
		
  
     ?><div class="sourcecoast" style="width: 100%; clear: both; margin-top:10px; margin-left: auto; margin-right: auto; "><span class="span_inside" style="margin-left: auto; margin-right: auto;"><a href="<?php echo JRoute::_($vars['next_link']); 
	 
	 
	 
	 if (!empty($this->current_position)) 
	 {
	    echo '#product_iter_'.$this->current_position; 
	 }
	 $next = JText::_('JNEXT').'...'; 
	 ?>" class="btn btn-primary button_for_ajaxsearch"><?php echo $next; ?></a></span></div>
	 <?php
  
		}
		}
		
		$cd = ob_get_clean(); 
		echo $cd; 
		if (!empty($cache) && (!empty($cache_file)))
		JFile::write($cache_file, $cd); 
		
		}
		else
		{
		  echo file_get_contents($cache_file); 
		}
		
		$document->setTitle($keyword); 
		$document->setMetaData( 'title', $keyword );
	}

}
