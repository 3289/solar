<?php
/**
 * @package		RuposTel Ajax search pro
 * @subpackage	com_content
 * @copyright	Copyright (C) 2005 - 2014 RuposTel.com
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */


defined('_JEXEC') or die;

class RupHelper {
	
	public static function getIncludes()
	{
	  if (!class_exists('VmConfig'))	  
		{
			require(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
		}
		VmConfig::loadConfig(); 
		if(!class_exists('shopFunctionsF'))require(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'shopfunctionsf.php');
		
		if (!class_exists('VmImage'))
			require(JPATH_VM_ADMINISTRATOR .DIRECTORY_SEPARATOR. 'helpers' .DIRECTORY_SEPARATOR. 'image.php');
			
	   
				if(!class_exists('calculationHelper')) require(JPATH_VM_ADMINISTRATOR.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'calculationh.php');
		if (!class_exists('VmConfig'))
		  require(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'config.php'); 
		 
		 
		  if (method_exists('VmConfig', 'loadJLang'))
		  {
		  VmConfig::loadJLang('com_virtuemart',TRUE);
		  VmConfig::loadJLang('com_virtuemart_orders',TRUE);
		  }
		  
		 if (method_exists('VmConfig', 'loadJLang'))
		 VmConfig::loadJLang('com_virtuemart');
		 else
		  {
		     $lang = JFactory::getLanguage();
			 $extension = 'com_virtuemart';
			 $base_dir = JPATH_SITE;
			 $language_tag = $lang->getTag();
			 $reload = false;
			 $lang->load($extension, $base_dir, $language_tag, $reload);
			 
			 $lang = JFactory::getLanguage();
			 $extension = 'com_virtuemart';
			 $base_dir = JPATH_ADMINISTRATOR;
			 $language_tag = $lang->getTag();
			 $reload = false;
			 $lang->load($extension, $base_dir, $language_tag, $reload);
			 
		  }
		  
	}

	
	public static function updateStats()
	{
		$keyword = JRequest::getVar('keyword'); 
		$qt = "CREATE TABLE IF NOT EXISTS `#__com_rupsearch_stats` (
  `keyword` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `md5` char(32) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `count` bigint(20) NOT NULL,
  `accessstamp` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `md5` (`md5`),
  KEY `count` (`count`),
  KEY `accessstamp` (`accessstamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1; "; 
	
	
	 $db = JFactory::getDBO(); 
	 
	    $prefix = $db->getPrefix();
		
		$table = '#__com_rupsearch_stats'; 
   $table = str_replace('#__', '', $table); 
   $table = str_replace($prefix, '', $table); 
 
   $q = "SHOW TABLES LIKE '".$db->getPrefix().$table."'";
	 $db->setQuery($q);
	  $r = $db->loadResult();
	   if (empty($r)) 
	   {
	     $db->setQuery($qt); 
		 $db->query(); 
	   }

	 $md5 = md5($keyword); 
	 $q = 'insert DELAYED into #__com_rupsearch_stats (`keyword`, `md5`, `count`, `accessstamp`) values '; 
	  $q .= " ('".$db->escape($keyword)."', '".$md5."', 1, ".time().") "; 
	  $q .= ' on duplicate key update count = count + 1, accessstamp = '.time(); 
	 $db->setQuery($q); 
	 $db->query();
	 $e = $db->getErrorMsg(); //if (!empty($e)) echo $e; 

	// 2 months old will get deleted
	/*
	 $old = time() - (60*60*24*60); 
	 $q = 'delete from #__com_rupsearch_stats where accessstamp < '.$old; 
	 $db->setQuery($q); 
	 $db->query();
	*/
	
	}
	public static function renderHidden()
	{
	 $db = JFactory::getDBO(); 
	 $q = 'select virtuemart_category_id from #__virtuemart_categories where published = 1 limit 0,1'; 
	 $db->setQuery($q); 
	 $cat_id = $db->loadResult(); 
	 
	  $controllerClassName = 'VirtueMartControllerCategory' ;
	  if (!class_exists($controllerClassName)) require(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.'category.php');
	  require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'category'.DIRECTORY_SEPARATOR.'view.html.php'); 
	  //$view = new VirtuemartViewCategory(); 
	   $isset = JRequest::getVar('virtuemart_category_id', null); 
	  $cat_id = JRequest::getVar('virtuemart_category_id', $cat_id); 
	  //new: JRequest::setVar('virtuemart_category_id', $cat_id); 
	  
	  $oldoption = JRequest::getVar('option'); 
	  
	  
	  //new: JRequest::setVar('option', 'com_virtuemart'); 
	  
	  
	  //JRequest::setVar('virtuemart_category_id', 1); 
	  $config = array(); 
	  $config['base_path'] = JPATH_VM_SITE;
	  $config['view_path'] = JPATH_VM_SITE.DIRECTORY_SEPARATOR.'views'; 
	  $controller = new $controllerClassName($config); 
	  $controller->set('_basePath', JPATH_VM_SITE); 
	  $controller->set('_setPath', JPATH_VM_SITE.DIRECTORY_SEPARATOR.'views'); 
	  $controller->addViewPath(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'views');
	  $tp = self::getTemplatePath('category'); 

	  //$view = $controller->getView('category', 'html', '', $config);
	  
	  require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_rupsearch'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'rupview.php'); 
		$view = new rupSearch(); 
	  
	  $view->addTemplatePath(JPATH_VM_SITE.'/views/category/tmpl');
	  if (!empty($tp))
	   {
	    $view->addTemplatePath($tp); 
		
		
	   }
	   
	   $es = ''; 
	   //var_dump($view); die(); 
	   $orderByList = array(); 
	   $orderByList['manufacturer'] = ''; 
	   $orderByList['orderby'] = ''; 
	   
	     $view->orderByList['orderby'] = ''; 
	     $view->orderByList['manufacturer'] = ''; 
	   
	   if (method_exists($view, 'assignRef'))
	   {
		   $view->assignRef('orderByList', $orderByList); 
	   }
	   
	   
	   
	   
	   
	  
	  
	  $view->viewName = 'category'; 
	  if (method_exists($view, 'setLayout'))
	  $view->setLayout('default'); 
	  else
	  if (method_exists($view, 'set'))
	  $view->set('layout', 'default');
	  else	  
	  if (method_exists($view, 'assignRef'))
	  $view->assignRef('layout', 'default'); 
	  else
	  $view->layout =  'default';

	  
	  	 
	  if (method_exists($view, 'set'))
	  $view->set('format', 'html');
	  else	  
	  if (method_exists($view, 'assignRef'))
	  $view->assignRef('format', 'html'); 
	  else
	  $view->format = 'html'; 

	   $cc = 0; 
	   
	   require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_rupsearch'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagination.php'); 
	   
	   $prodcs = array(); 
	   $rp = new rupPagination($cc, 0, 1 ,1 );
	   $view->vmPagination = $rp;
	  
	  
	
		
	  ob_start(); 
	  $view->loadYag(); 
	  $view->display(); 
	  $x = ob_get_clean(); 
	  
	  
	  //new: JRequest::setVar('option', $oldoption); 
	  
	  //new: if (empty($isset))
	  //new: JRequest::setVar('virtuemart_category_id', null); 
	}
	
	public static function getParams($id=0)
	{
		jimport( 'joomla.registry.registry' );
		
		if (empty($id))
		$id = JRequest::getVar('module_id', null); 
	
		if (!empty($id))
		 {
		    $id = (int)$id; 
			$q = 'select `params` from `#__modules` where `id` = '.$id.' and `module` = \'mod_virtuemart_ajax_search_pro\' limit 1'; 
			
			$db = JFactory::getDBO(); 
			$db->setQuery($q); 
			$params_s = $db->loadResult(); 
			
			
			
			if (!empty($params_s))
			{
			$params = new JRegistry($params_s); 
			
			return $params; 
			}
			
		 }
		 
		 {
			
			$q = 'select `params` from `#__modules` where `module` = \'mod_virtuemart_ajax_search_pro\' and published = 1 limit 1'; 
			
			$db = JFactory::getDBO(); 
			$db->setQuery($q); 
			$params_s = $db->loadResult(); 
			
			
			
			if (!empty($params_s))
			$params = new JRegistry($params_s); 
			
			return $params; 
		 }
		 
		 $r = new JRegistry(); 
		 return $r; 
		 
		 
	}
	
	public static function getProducts($keyword, $prods=5, $popup=false, $order_by)
	{
	
	 $params = self::getParams(); 
	
	 $prod_o = $prods; 
	 $prods++; 
	
	  $db = JFactory::getDBO(); 
	  $or = $or2 = $or3 = '';
	  $ko = trim($keyword);  
	  $ae = explode(' ', $ko); 
	  
	  
	  if ((!empty($ae)) && (count($ae)>1))
	   {
	      if (empty($or)) {
			  $or = ' OR ('; 
			  $or2 = ' OR ('; 
			  $or3 = ' OR ('; 
		  }
		  $i = 0;
		  $all = count($ae); 
	      foreach ($ae as $word)
		    {
			   if (empty($word)) continue; 
				
			   $i++; 
			   if (strlen($word)<=2) 
			   {
				   $word .= ' '; 
			   }
			   
			   // take just the base of the word: 
			   if (mb_strlen($word)>8)
			   {
				   $word = mb_substr($word, 0, -3); 
			   }
			   else
			   if ((mb_strlen($keyword)>6) && (mb_strlen($keyword)<9))
			   {
				      $word = mb_substr($word, 0, -2); 
			   }
			   else
			   $word = mb_substr($word, 0, -1); 
			   
			   
			   $or .= " ( l.`product_name` LIKE '%".$db->escape($word)."%' ) "; 
			   $or2 .= " ( l.`product_s_desc` LIKE '%".$db->escape($word)."%' ) "; 
			   $or3 .= " ( l.`product_desc` LIKE '%".$db->escape($word)."%' ) "; 
			   
			   if ($i!=$all) 
			   {
				   $or .= ' AND '; 
				   $or2 .= ' AND '; 
				   $or3 .= ' AND '; 
			   }
			}
			
		  $or .= ') '; 
		  $or2 .= ') '; 
		  $or3 .= ') '; 
	   }
	   
	   if (empty($i)) {
		     $or = $or2 = $or3 = '';
	   }
	   
	  $child_handling = JRequest::getVar('op_childhandling', $params->get('child_products', 0)); 
	
	  
	  $only_in_stock = $params->get('only_in_stock', false); 
	  
	  if (empty($order_by)) $order_by = $params->get('order_by', ''); 
	  
	  $stock = ''; 
	  if (!empty($$only_in_stock))
	  {
		  $stock = ' p.`product_in_stock` > 0 and '; 
	  }
	  
	  $order_sql = ''; 
	  if (!empty($order_by))
	  {
		  switch ($order_by)
		  {
			  case 'product_name': 
			  $order_sql = ', l.product_name as SORTFIELD '; 
			  $order_all = ' order by SORTFIELD '; 
			  break; 
			  default: 
			   $order_sql = ''; 
			   $order_all = ''; 
		  }
	  }
	  
	  $search = array(); 
	  
	  if (!defined('VMLANG')) define('VMLANG', 'en_gb'); 
		  // search product sku
		  $q = " select p.`virtuemart_product_id`, p.`product_parent_id`".$order_sql; 
		  if (!empty($child_handling))
		  $q .= ", (select  GROUP_CONCAT(pp.`virtuemart_product_id`) from  `#__virtuemart_products` as pp where pp.`product_parent_id` = p.`virtuemart_product_id`) as children "; 
		  $q .= " from `#__virtuemart_products` AS p, `#__virtuemart_products_".VMLANG."` as l "; 
		  $q .= " where ".$stock." p.`product_sku` LIKE '".$db->escape($keyword)."' and p.`published` = '1' and p.`virtuemart_product_id` = l.`virtuemart_product_id` LIMIT 0,".$prods." "."\n"; 
		  
		  $search['PRODUCT_SKU'] = $q; 
		  
		  if (mb_strlen($keyword)>3)
		  { 
		  //$q = " union ( "; 
		  $q .= " select p.virtuemart_product_id, p.product_parent_id ".$order_sql; 
		  if (!empty($child_handling))
		  $q .= ", (select  GROUP_CONCAT(pp.virtuemart_product_id) from  #__virtuemart_products as pp where pp.product_parent_id = p.virtuemart_product_id) as children "; 
		  $q .= " from #__virtuemart_products AS p, #__virtuemart_products_".VMLANG." as l "; 
		  $q .= " where p.product_sku LIKE '%".$db->escape($keyword)."%' and p.published = '1' and p.virtuemart_product_id = l.virtuemart_product_id LIMIT 0,".$prods." "; 
		  //$q.= " ) "."\n"; 
		  
		   $search['PRODUCT_SKU_PARTIAL'] = $q; 
		  
		  
		  }
		  
		  // search product name
		  
	      //$q .= " union ("; 
		  $q = " select p.`virtuemart_product_id`, p.product_parent_id ".$order_sql; 
		  if (!empty($child_handling))
		  $q .=" , (select  GROUP_CONCAT(pp.`virtuemart_product_id`) from  `#__virtuemart_products` as pp where pp.product_parent_id = p.`virtuemart_product_id`) as children "; 
		  $q .= " from `#__virtuemart_products` AS p, `#__virtuemart_products_".VMLANG."` as l "; 
		  $q .= " where ".$stock." (l.`product_name` LIKE '".$db->escape($keyword)."%' or l.`product_name` LIKE ' ".$db->escape($keyword)." ' ) and p.`published` = '1' and p.`virtuemart_product_id` = l.`virtuemart_product_id` LIMIT 0,".$prods." "; 
		  //$q .= " )"."\n";
		   
		  $search['PRODUCT_NAME'] = $q;
		  
		  // search product name for multi words
		  
		  if (!empty($or))
		  {
	      //$q .= " union ( "; 
		  $q = " select p.`virtuemart_product_id`, p.product_parent_id ".$order_sql; 
		 if (!empty($child_handling))
		  $q .= ", (select  GROUP_CONCAT(pp.`virtuemart_product_id`) from  `#__virtuemart_products` as pp where pp.product_parent_id = p.`virtuemart_product_id`) as children  "; 
		  $q .= " from `#__virtuemart_products` AS p, `#__virtuemart_products_".VMLANG."` as l "; 
		  $q .= " where ".$stock." (0 ".$or.") and p.`published` = '1' and p.`virtuemart_product_id` = l.`virtuemart_product_id` LIMIT 0,".$prods." "; 
		  //$q .= " )"."\n";
		  
		  $search['PRODUCT_NAME_WORDS'] = $q;
		  
		  
		  }
		  else
		  {
			  
			  // if product name includes the single word
			   //$q .= " union ("; 
			   $q = " select p.`virtuemart_product_id`, p.product_parent_id ".$order_sql; 
		  if (!empty($child_handling))
		  $q .=" , (select  GROUP_CONCAT(pp.`virtuemart_product_id`) from  `#__virtuemart_products` as pp where pp.product_parent_id = p.`virtuemart_product_id`) as children "; 
		  $q .= " from `#__virtuemart_products` AS p, `#__virtuemart_products_".VMLANG."` as l "; 
		  $q .= " where ".$stock." l.`product_name` LIKE '%".$db->escape($keyword)."%' and l.`product_name` NOT LIKE '".$db->escape($keyword)."%' and p.`published` = '1' and p.`virtuemart_product_id` = l.`virtuemart_product_id` LIMIT 0,".$prods." "; 
		  //$q .= " )"."\n";
		   $search['PRODUCT_NAME_WORDS'] = $q;
		  
		  }
		  
		  $keyword2 = ''; 
		  
		  if ((mb_strlen($keyword)>6) && (mb_strlen($keyword)<9))
		  {
			  $keyword2 = mb_substr($keyword, 0, -2); 
		  }
		 
			  if ((mb_strlen($keyword)>8))
			  {
				  $keyword2 = mb_substr($keyword, 0, -3); 
			  }
	
		  else
		  {
			   $keyword2 = mb_substr($keyword, 0, -1); 
		  }
		  
		  if (!empty($keyword2))
		  {
			  // search product name for multi words
	      //$q .= " union ( "; 
		  $q = " select p.`virtuemart_product_id`, p.product_parent_id ".$order_sql; 
		  if (!empty($child_handling))
		  $q .= ", (select  GROUP_CONCAT(pp.`virtuemart_product_id`) from  `#__virtuemart_products` as pp where pp.product_parent_id = p.`virtuemart_product_id`) as children  "; 
		  $q .= " from `#__virtuemart_products` AS p, `#__virtuemart_products_".VMLANG."` as l "; 
		  $q .= " where ".$stock." (l.`product_name` LIKE '%".$db->escape($keyword2)."%' ".$or.") and p.`published` = '1' and p.`virtuemart_product_id` = l.`virtuemart_product_id` LIMIT 0,".$prods." "; 
		  //$q .= " )"."\n";
		  
		   $search['PRODUCT_NAME_MULTI_WORDS'] = $q;
		  }
		  
		  
		 
		  // product SKU starts with... 
		  //$q .= " union ( "; 
		  $q = " select p.`virtuemart_product_id`, p.product_parent_id ".$order_sql; 
		  if (!empty($child_handling))
		  $q .= ", (select  GROUP_CONCAT(pp.`virtuemart_product_id`) from  `#__virtuemart_products` as pp where pp.product_parent_id = p.`virtuemart_product_id`) as children "; 
		  $q .= " from `#__virtuemart_products` AS p, `#__virtuemart_products_".VMLANG."` as l";
		  $q .= " where ".$stock." p.`product_sku` LIKE '".$db->escape($ko)."%' and p.`published` = '1'  and p.`virtuemart_product_id` = l.`virtuemart_product_id` LIMIT 0,".$prods." "; 
		  //$q .= " )"."\n";
		   $search['PRODUCT_SKU_STARTS_WITH'] = $q;
		  //$q .= " union (select cf.`virtuemart_product_id` from #__virtuemart_product_custom_plg_translatablestring as cf where `translatable_string_".VMLANG."` like '".$db->escape($keyword)."' or (`translatable_string_".VMLANG."` like '".$db->escape($keyword)."%') or (`translatable_string_".VMLANG."` like '%".$db->escape($keyword)."') limit 0,".$prods." )"."\n";
		  
		  
		  $x = JRequest::getInt('search_desc', 1); 
		  
		  $optional_search = $params->get('optional_search', 2); 
		  if ($optional_search === 2) $x = true; 
		  else 
		  if (($optional_search === 1) && (!empty($x))) $x = true; 
		  if (empty($optional_search)) $x = 0; 
		  
		  
		  if (!empty($x))
		  {
		  // product desc includes the phrase
		  //$q .= " union ( "; 
		  $q = "select p.`virtuemart_product_id`, p.`product_parent_id`".$order_sql; 
		  if (!empty($child_handling))
		  $q .= ", (select  GROUP_CONCAT(pp.`virtuemart_product_id`) from  `#__virtuemart_products` as pp where pp.product_parent_id = p.`virtuemart_product_id`) as children  "; 
		  $q .= " from `#__virtuemart_products` AS p, `#__virtuemart_products_".VMLANG."` as l "; 
		  $q .= " where ".$stock." (l.`product_desc` LIKE '%".$db->escape($keyword)."%' ".$or2." ) and p.`published` = '1' and p.`virtuemart_product_id` = l.`virtuemart_product_id` LIMIT 0,".$prods." "; 
		  //$q = " )"."\n";
		  
		  $search['PRODUCT_DESC'] = $q;
		  
		  // product short desc includes the phrase
		  //$q .= " union ("; 
		  $q = " select p.`virtuemart_product_id`, p.`product_parent_id` ".$order_sql; 
		  if (!empty($child_handling))
		  $q .= ", (select  GROUP_CONCAT(pp.`virtuemart_product_id`) from  `#__virtuemart_products` as pp where pp.`product_parent_id` = p.`virtuemart_product_id`) as children"; 
		  $q .= " from `#__virtuemart_products` AS p, `#__virtuemart_products_".VMLANG."` as l"; 
		  
		  $q .= " where ".$stock." (l.`product_s_desc` LIKE '%".$db->escape($keyword)."%' ".$or3.") and p.`published` = '1' and p.`virtuemart_product_id` = l.`virtuemart_product_id` LIMIT 0,".$prods." "; 
		  //$q .= " )"."\n";
		  
		   $search['PRODUCT_S_DESC'] = $q;
		  
		  }
		  
		  if (!empty($order_all)) $q .= $order_all; 
		  
		  $priorities = $params->get('search_priority', 'PRODUCT_SKU,PRODUCT_NAME,PRODUCT_NAME_WORDS,PRODUCT_NAME_MULTI_WORDS,PRODUCT_NAME_MULTI_WORDS,PRODUCT_SKU_STARTS_WITH,PRODUCT_DESC,PRODUCT_S_DESC'); 
		  
		  
		  $priorities = explode(',', $priorities); 
		  $f = true; 
		  $q = ''; 
		  //foreach ($search as $key=>$val)
		  foreach ($priorities as $k=>$v)
		  {
			  $key = strtoupper(trim($v));
			  if (empty($search[$key])) continue; 
			  $val = $search[$key]; 
			  
			  if ($f)
			  {
				  $q .= $val."\n"; 
			  }
			  else
			  {
				  $q .= ' union ('.$val.')'."\n"; 
			  }
			  $f = false; 
		  }
		  
		  
	      $q .= " LIMIT 0,".$prods; 
		$db->setQuery($q); 
		try
		{
		$ret = $db->loadAssocList(); 
		}
		catch (Exception $e)
		{
			$debug = $params->get('debug', false); 
			if ($debug)
			{
			echo $q; 
			var_dump($e); 
			die(); 
			}
		}
		
		//debug_zval_dump($q); die(); 
		/*
		$q = str_replace('#__', 'g52p3_', $q); 
		echo $q;
		var_dump($ret); die(); 
		*/
		$debug = $params->get('debug', false); 
		
		
		if ($debug)
		{
		  $e = $db->getErrorMsg(); if (!empty($e)) { echo $e; die(); }
		 
		  
		}
		
		
		if (empty($ret)) return array(); 
		
		
		
		
		if ($popup) return $ret; 
		$proddb = array(); 
		foreach ($ret as $key=>$val)
		 {
		 
		 
		 
		
		/*
		COM_ONEPAGE_XML_CHILDPRODUCTS_HANDLING_OPT1="Include both child and parent products"
		COM_ONEPAGE_XML_CHILDPRODUCTS_HANDLING_OPT2="Include only child products and products without child products (skip parent products)"
		COM_ONEPAGE_XML_CHILDPRODUCTS_HANDLING_OPT3="Include only parent products"

		*/
		
		
		
		if (!empty($child_handling))
		{
		 //if (!empty($val['children'])) { var_dump($val); die(); }
		$child_type = array(); 
		$child_type[] = 1; 
		//if (!empty($val['product_parent_id'])) $child_type[] = 2; 
		
		// has children, ie is parent: 
		if (!empty($val['children']) && (empty($val['product_parent_id']))) $child_type[] = 3; 
		
		if (!empty($val['children'])) $child_type[] = 3;
	    if (empty($val['product_parent_id'])) $child_type[] = 3; 
		
		
		// does not have children and is not a child product (it's parent product)
		if (empty($val['children']) && (empty($val['product_parent_id']))) $child_type[] = 3;
		// is parent with no children, same as above
		if ((empty($val['product_parent_id'])) && (empty($val['children']))) $child_type[] = 2; 
		// is child and does not have subchildren: 
		if ((!empty($val['product_parent_id'])) && (empty($val['children']))) $child_type[] = 2; 
		/*
		var_dump($val['children']); 
		var_dump($child_type); 
		var_dump($val['product_parent_id']); 
		var_dump($val['virtuemart_product_id']); 
		die(); 
		*/
		//var_dump($child_handling); var_dump($child_type); die(); 
		
		 if (!in_array($child_handling, $child_type))
		 {
		 
		 continue; 
		 }
		 
		}
		
		 
		 
		   $proddb[] = $val['virtuemart_product_id']; 
		 }
		 
		return $proddb; 
		
	}
	
	// original code from shopfunctionsF::renderMail
	public static function getVMView(&$ref, $viewName, $vars=array(),$controllerName = NULL, $layout='default', $format='html')
	{
		
		if (!class_exists('CurrencyDisplay'))
	require(JPATH_VM_ADMINISTRATOR .DIRECTORY_SEPARATOR. 'helpers' .DIRECTORY_SEPARATOR. 'currencydisplay.php');
	    $cache_file = $ref->cache_file; 
	    $originallayout = JRequest::getVar( 'layout' );
	  	if(!class_exists('VirtueMartControllerVirtuemart')) require(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.'virtuemart.php');
// 		$format = (VmConfig::get('order_html_email',1)) ? 'html' : 'raw';
		
		// calling this resets the layout
		//$controller = new VirtueMartControllerVirtuemart();
		//JRequest::setVar( 'layout', $layout );
		require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_rupsearch'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'rupview.php'); 
		
	   $oldoption = JRequest::getVar('option'); 
	   //new: JRequest::setVar('option', 'com_virtuemart'); 

		
		$view = new rupSearch(); 
		foreach ($ref as $k=>$v)
		{
			if (empty($view->$k))
			$view->$k = $v; 
		}
		
		$view->viewName = 'category'; 
		//Todo, do we need that? refering to http://forum.virtuemart.net/index.php?topic=96318.msg317277#msg317277
		//$controller->addViewPath(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'views');
		
		//$view = $controller->getView($viewName, $format);
	  
	  $view->showproducts = true; 
	  $show_prices  = VmConfig::get('show_prices',1);
	  $view->show_prices = $show_prices; 
	  if (isset($ref->keyword))
	  {
	  $view->keyword = $ref->keyword; 
	  $view->search = $view->keyword; 
	  }
	  if (!isset($view->currency))
	  {
	  
	    $currency = CurrencyDisplay::getInstance( );
		$view->currency =& $currency;
	  }
	  
	  if (empty($view->category))
	  {
		  
				$view->category = new stdClass();
				$view->category->category_name = '';
				$view->category->category_description= '';
				$view->category->haschildren= false;
			
	  }
	  if (!isset($view->searchcustom))
	  {
	    $view->searchcustom = '';
		$view->searchCustomValues = '';
	  }
	  if (!isset($view->categoryId))
	  {
		  $view->categoryId = 0; 
	  }
	  if (!isset($view->perRow))
	  $view->perRow = VmConfig::get('products_per_row',3);
  
	  $view->viewName = 'category'; 
	  if (method_exists($view, 'setLayout'))
	  $view->setLayout('default'); 
	  else
	  if (method_exists($view, 'set'))
	  $view->set('layout', 'default');
	  else	  
	  if (method_exists($view, 'assignRef'))
	  $view->assignRef('layout', 'default'); 
	  else
	  $view->layout =  'default';

	   $tp = self::getTemplatePath('category'); 
	   
	   if (!empty($tp))
	   {
	    $view->addTemplatePath($tp); 
		
		
	   }
	   
	   
	 
	  if (method_exists($view, 'set'))
	  $view->set('format', 'html');
	  else	  
	  if (method_exists($view, 'assignRef'))
	  $view->assignRef('format', 'html'); 
	  else
	  $view->format = 'html'; 

		
		//$view->setLayout($layout); 
		
		if (!$controllerName) $controllerName = $viewName;
		$controllerClassName = 'VirtueMartController'.ucfirst ($controllerName) ;
		if (!class_exists($controllerClassName)) require(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.$controllerName.'.php');

		//Todo, do we need that? refering to http://forum.virtuemart.net/index.php?topic=96318.msg317277#msg317277
		
		
		
		
		$path = $view->getPath('default', 'default'); 
	    
		foreach ($vars as $key => $val) {
	
		
			$view->$key = $val;
			//$view->assignRef($key, $val); 
		}
		$view->search = null; 
		$view->keyword = ''; 
		if (isset($view->category) && (is_object($view->category)))
		$view->category->haschildren = false; 
		//$count = count($ids); 
		$prods = JRequest::getInt('prods', 5); 
		$cc = count($vars['products']); 
		require_once(JPATH_SITE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_rupsearch'.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'pagination.php'); 
		$rp = new rupPagination($cc, 0, $prods , $vars['perRow'] );
		$vars['vmPagination'] = $rp;
		
		$view->vmPagination = $vars['vmPagination']; 
		$view->showRating = false; 
		$view->showBasePrice = false; 
		$view->showcategory = 0; 
		$productsLayout = VmConfig::get('productsublayout','products');
		if(empty($productsLayout)) $productsLayout = 'products';
		$view->productsLayout = $productsLayout; 
		//new: $cat = JRequest::getVar('virtuemart_category_id', null); 
		//new: JRequest::setVar('virtuemart_category_id', false); 
		
		$html3 = ''; 
		 if (empty($vars['products']))
	  {
		  
		  if (method_exists($view, 'assignRef'))
		  {
			  $f = false; 
		  $view->assignRef('showproducts', $f); 
		  $vars['showproducts'] = false; 
		  }
		  else
		  {
		  $view->showproducts = false; 
		  $vars['showproducts'] = false; 
		  }
	  
	  
	     $html3 = JText::_ ('COM_VIRTUEMART_NO_RESULT');
	  }
	  $vars['keyword'] = ''; 
	  foreach ($vars as $key => $val) {
	
		
			$view->$key = $val;
			//$view->assignRef($key, $val); 
		}
	  	 
		$orderByList = array(); 
	   $orderByList['manufacturer'] = ''; 
	   $orderByList['orderby'] = ''; 
	   
	     $view->orderByList['orderby'] = ''; 
	     $view->orderByList['manufacturer'] = ''; 
	   
	   if (method_exists($view, 'assignRef'))
	   {
		   $view->assignRef('orderByList', $orderByList); 
	   }
		
		
		ob_start(); 
		$view->loadYag(); 
		$html = $view->display();
		$html2 = ob_get_clean(); 
		//new: JRequest::setVar('virtuemart_category_id', $cat); 
		//new: JRequest::setVar( 'layout', $originallayout );
		
		 
	    //new:  JRequest::setVar('option', $oldoption); 
		
		$ret = $html.$html2.$html3; 
		return $ret; 
	}
	
	public static function getTemplatePath($viewName)
	{ 
	  if(!class_exists('shopFunctionsF'))require(JPATH_VM_SITE.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR.'shopfunctionsf.php');
	  if (method_exists('shopFunctionsF', 'loadVmTemplateStyle'))
	  {
	  //$template = shopFunctionsF::loadVmTemplateStyle(); 
	  //var_dump($emplate); echo '..'; 
	  }
	  //else
	  {
	  $vmtemplate = VmConfig::get('vmtemplate','default');
	  $vmtemplate = VmConfig::get('categorytemplate', $vmtemplate);
	 
		if($vmtemplate=='default'){
			
				$q = 'SELECT `template` FROM `#__template_styles` WHERE `client_id`="0" AND `home` <> "0"';
			
			
			$db = JFactory::getDbo();
			$db->setQuery($q);
			$template = $db->loadResult(); 
			
			
			
		} else {
			$template = $vmtemplate;
		}
	  }

		if($template){
			//$this->addTemplatePath(JPATH_ROOT.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$template.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.$viewName);
			$tp = JPATH_ROOT.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$template.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.'com_virtuemart'.DIRECTORY_SEPARATOR.$viewName;
			if (file_exists($tp)) return $tp; 
			}
	}
	
}	

