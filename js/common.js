
jQuery(window).load(function () {
    var reg;
    var power;
    var place;
    var money;
    var cur_region;
    var place_selected = false;

    jQuery('svg path').bind('click', function (e) {
        jQuery('#warning_1, #warning_2').css('display', 'none');
        jQuery('select').each(function () {
            jQuery(this).val(-1);
        });

        jQuery('#second_step, #third_step, #send_button').css('display', 'none');
        reg = jQuery(this).data('region');
        cur_region = jQuery(this);
        jQuery('#region_hide').attr('value',reg);
        place_selected = false;
        jQuery('#kp .option_head').attr('selected', 1);

        jQuery(this).attr('fill', 'yellow');
        var $modal = jQuery('#kp');

        var selector = '#first_step';

        if (jQuery.inArray(reg, [3, 11, 25]) !== -1) {
            selector = '#warning_1';
            if (jQuery.inArray(reg, [25]) !== -1) {
                selector = '#warning_2';
            }
            var $modal_title = $modal.find('#modal-title');
            var header = $modal_title.data('error');
            $modal_title.text(header);
            jQuery('#first_step').hide();
        }
        $modal.modal();

        jQuery(selector).fadeIn();
        jQuery('#kp').on('hidden.bs.modal', function () {
            jQuery('svg path ').attr('fill',"#01e4f7");
        });
    });


    jQuery('#kp').find('.close').bind('click', function (e) {
        cur_region.attr('fill', '#01e4f7');
    });

    jQuery('svg  path').hover(function (e) {
        function moveUp(thisObject) {
            thisObject.appendTo(thisObject.parents('svg'));
        }

        moveUp(jQuery(e.target));
    });
    jQuery.noConflict();

    jQuery('#select_power').bind('change', function () {
        jQuery('#second_step').fadeIn("slow");
        jQuery('#select_place').val(-1);
        power = jQuery('#select_power').val();
        if (place_selected) {
            sendAjax(reg, power, place);
        }
    });


    jQuery('#select_place').bind('change', function () {
        place = jQuery('#select_place').val();

        sendAjax(reg, power, place);
        place_selected = true;
    });


    jQuery('#select_money').bind('change', function () {
        money = jQuery('#select_money').val();
        jQuery('#send_button').fadeIn("slow");
    });

    jQuery('#form_kp').submit(function(event) {
        jQuery('#kp').find('.close').click();
        jQuery(this).unbind('submit').submit();
    });


});

function sendAjax(reg, power, place) {
    jQuery.ajax({
        url: '/index.php?option=com_calculator&task=station.getcalc',
        data: {id_region: reg, power: power, id_place: place},
        headers: '',
        type: 'GET',
        datatype: 'JSON',
        success: function (response) {
            var data = jQuery.parseJSON(response);
            jQuery('#select_money option').remove();

            jQuery('<option/>', {
                value: -1,
                selected: 1,
                disabled: 1
            }).html(select_money_text).appendTo('#select_money');

            jQuery.each(data, function (i, elem) {
                var option = jQuery('<option/>', {
                    value: i
                }).html(elem + ' $').appendTo('#select_money');
            });

            if (jQuery('#select_money option').length == 2) {
                jQuery('#select_money option:last-child').attr('selected', true);
                jQuery('#select_money').trigger('change');
            } else {
                jQuery('#third_step').fadeIn("slow");
            }
        }
    });
}