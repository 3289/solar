var data_chart1 = [
    ['Год', 'Значение'],
    ['1', 0],
    ['2', 0],
    ['3', 0],
    ['4', 0],
    ['5', 0],
    ['6', 0],
    ['7', 0],
    ['8', 0],
    ['9', 0],
    ['10', 0],
    ['11', 0],
    ['12', 0]
];
var data_chart2 = [
    ['Месяц', 'Значение', {role: 'annotation'}],
    ['Январь', 0, 0],
    ['Февраль', 0, 0],
    ['Март', 0, 0],
    ['Апрель', 0, 0],
    ['Май', 0, 0],
    ['Июнь', 0, 0],
    ['Июль', 0, 0],
    ['Август', 0, 0],
    ['Сентябрь', 0, 0],
    ['Октябрь', 0, 0],
    ['Ноябрь', 0, 0],
    ['Декабрь', 0, 0]
];
var data_chart1_min = JSON.parse(JSON.stringify(data_chart1));
var data_chart1_max = JSON.parse(JSON.stringify(data_chart1));
var data_chart2_min = JSON.parse(JSON.stringify(data_chart2));
var data_chart2_max = JSON.parse(JSON.stringify(data_chart2));
var data_chart3 = JSON.parse(JSON.stringify(data_chart2));
var data_chart3_min = JSON.parse(JSON.stringify(data_chart2));
var data_chart3_max = JSON.parse(JSON.stringify(data_chart2));


jQuery(window).load(function () {
    var reg;
    var power;
    var place;
    var money;
    var place_selected = false;

    jQuery('html, body').animate({
        scrollTop: jQuery(".head-wrap").offset().top
    }, 1500);
    jQuery('.content-profit').html(data.profit);
    jQuery.each(data.global, function (i, val) {
        jQuery('.' + i).html(val);
    });

    jQuery('.content-roi').html(data.ROI.html);
    jQuery('.stantion-cost').html(data.cost);
    jQuery('.global-text').html(data.h2);

    function getRandomArbitrary(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }

    jQuery.each(data.months, function (i, val) {
        data_chart2[i][0] = val;
        data_chart2_min[i][0] = val;
        data_chart3[i][0] = val;
        data_chart3_min[i][0] = val;
    });

    console.log(data.ROI.roi);
    jQuery.each(data.ROI.roi, function (i, val) {
        var tr = jQuery('.insolation-table tr:nth-child(' + (i + 2) + ')');
        jQuery(jQuery(tr).find('td')[0]).html(val[0]);
        jQuery(jQuery(tr).find('td')[1]).html(val[1]);
        jQuery(jQuery(tr).find('td')[2]).html(val[2]);

        var index = parseInt(i) + 1;
        if (typeof (data_chart2[index]) !== "undefined") {

            var value2 = parseInt(val[1]);
            var value3 = parseInt(val[2]);

            data_chart2[index][1] = value2;
            data_chart2[index][2] = value2.toString();
            data_chart3[index][1] = value3;
            data_chart3[index][2] = value3.toString();
        }
    });

    jQuery.each(data.ROI.graph, function (i, val) {

        if (typeof (data_chart1[i]) !== "undefined") {
            var value = parseInt(val);
            data_chart1[i][1] = value;
        }
    });
    // var max1 = 0, min1 = 0;
    // jQuery.each(data_chart1, function (i, val) {
    //     var value = parseInt(val[1]);
    //     if (max1 < value) max1 = value;
    //     if (min1 > value) min1 = value;
    // });
    // max1 = max1 * 1.2;
    // min1 = min1 * 0.7;
    // jQuery.each(data_chart1, function (i, value) {
    //     var index = parseInt(i) + 1;
    //     if (index === 1) {
    //         return;
    //     }
    //     var val = parseInt(value[1]);
    //
    //     if (typeof (data_chart1_min[index]) !== "undefined") {
    //         var min = min1, max = max1;
    //         if (val < 0) {
    //             max = 0;
    //         }
    //         if (val > 0) {
    //             min = 0;
    //         }
    //         //console.log(val, min, max, getRandomArbitrary(min, max), index);
    //         data_chart1_min[index][1] = getRandomArbitrary(min, max);
    //         data_chart1_max[index][1] = getRandomArbitrary(min, max);
    //     }
    // });
    // ////datachart2
    // var max1 = 0, min1 = 0;
    // jQuery.each(data_chart2, function (i, val) {
    //     var value = parseInt(val[1]);
    //     if (max1 < value) max1 = value;
    //     if (min1 > value) min1 = value;
    // });
    // max1 = max1 * 1.2;
    // min1 = min1 * 0.7;
    // jQuery.each(data_chart2, function (i, value) {
    //     var index = parseInt(i) + 1;
    //     if (index === 1) {
    //         return;
    //     }
    //     var val = parseInt(value[1]);
    //
    //     if (typeof (data_chart2_min[index]) !== "undefined") {
    //         var min = min1, max = max1;
    //         if (val < 0) {
    //             max = 0;
    //         }
    //         if (val > 0) {
    //             min = 0;
    //         }
    //         //console.log(val, min, max, getRandomArbitrary(min, max), index);
    //         data_chart2_min[index][1] = getRandomArbitrary(min, max);
    //         data_chart2_max[index][1] = getRandomArbitrary(min, max);
    //     }
    // });
    ////datachart3
    // var max1 = 0, min1 = 0;
    // jQuery.each(data_chart3, function (i, val) {
    //     var value = parseInt(val[1]);
    //     if (max1 < value) max1 = value;
    //     if (min1 > value) min1 = value;
    // });
    // max1 = max1 * 1.2;
    // min1 = min1 * 0.7;
    // jQuery.each(data_chart3, function (i, value) {
    //     var index = parseInt(i) + 1;
    //     if (index === 1) {
    //         return;
    //     }
    //     var val = parseInt(value[1]);
    //
    //     if (typeof (data_chart3_min[index]) !== "undefined") {
    //         var min = min1, max = max1;
    //         if (val < 0) {
    //             max = 0;
    //         }
    //         if (val > 0) {
    //             min = 0;
    //         }
    //         //console.log(val, min, max, getRandomArbitrary(min, max), index);
    //         data_chart3_min[index][1] = getRandomArbitrary(min, max);
    //         data_chart3_max[index][1] = getRandomArbitrary(min, max);
    //     }
    // });

    jQuery('.gray-bg footer').html(data.ROI.footer);
    google.charts.load('current', {'packages': ['corechart', 'bar']});

    google.charts.setOnLoadCallback(drawChart1);
    google.charts.setOnLoadCallback(drawChart2);
    google.charts.setOnLoadCallback(drawChart3);

    jQuery.noConflict();
    jQuery('#form-order').submit(function () {
        var name = jQuery('#form-order').find('input#name').val();
        var phone = jQuery('#form-order').find('input#phone').val();
        var email = jQuery('#form-order').find('input#email').val();
        var region = data.id_region;
        var place = data.id_place;
        var power = data.power;
        var money = data.id_station;

        jQuery.ajax({
            type: "POST",
            url: 'http://solar.3289.company/index.php?option=com_calculator&task=station.getajaxsendemail',
            //url: 'http://solar.local/index.php?option=com_calculator&task=station.getajaxsendemail',
            data: {name: name, phone: phone, email: email, region: region, place: place, power: power, money: money},
            //data:msg,
            //headers: '',
            datatype: 'JSON',
            success: function (response) {
                jQuery('#order-modal').find('.close').trigger('click');
            }
        }).done(function () {

            setTimeout(function () {
                // Done Functions
                jQuery('#form-order').trigger("reset");
                jQuery('#thankyou').modal();
                jQuery('#thank_text').fadeIn("slow");
            }, 1000);
        });
        return false;
    });

    jQuery('#print-btn').bind('click', function () {

        jQuery(".AB-solar").append(function (i) {
            return '<iframe src=\"/solar.pdf?t='+Math.random()+'\" name=\"kp_pdf\" style="display: none"></iframe>';
        });
        window.frames['kp_pdf'].print();

    });

    jQuery('#order-btn').bind('click', function (e) {
        jQuery('#order-modal').modal();
    });

});


function drawChart1() {
    var classicChart = new google.visualization.ColumnChart(document.getElementById('chart1'));
    var data = google.visualization.arrayToDataTable(data_chart1);
    var data_min = google.visualization.arrayToDataTable(data_chart1_min);
    var data_max = google.visualization.arrayToDataTable(data_chart1_max);
    var Vmin = 0;
    var Vmax = Vmin;
    for (var i = 1; i < data_chart1.length; ++i) {
        if (data_chart1[i][1] > Vmax) Vmax = data_chart1[i][1];
        if (data_chart1[i][1] < Vmin) Vmin = data_chart1[i][1];
    }
    Vmax = Math.ceil(Vmax / 10000) * 10000;
    Vmin = Math.floor(Vmin / 10000) * 10000;
    var delta = Math.round((Vmax - Vmin) / 5);

    var ticksValues = [];
    for (var i = 0; i < 6; i++)
        ticksValues[i] = Vmin + delta * i;

    var options = {

        annotations: {
            alwaysOutside: true,
            datum: {
                stem: {
                    length: 6,
                    color: '#ffffff'
                }
            },
            style: 'line'

        },
        enableInteractivity: false,
        legend: {position: 'none'},
        bar: {groupWidth: "38%"},
        width: 500,
        height: 270,
        chartArea: {left: 60, top: 20, width: '100%', height: '75%'},
        vAxis: {
            //minValue: -10000,
            //maxValue: Vmax+20000,
            ticks: ticksValues,
            textStyle: {color: '#ed7d1a'},
            format: '#',
            viewWindowMode: 'maximized'
        },
        hAxis: {
            textStyle: {color: '#515352'},
            title: '',
            minTextSpacing: 3
        },
        colors: ['#ed7d1a']
    };


    if (document.body.clientWidth < 1300) {
        options.width = 900;
    }
    if (document.body.clientWidth < 1200) {
        options.width = 500;
    }
    if (document.body.clientWidth < 732) {
        options.width = 320;
    }


    classicChart.draw(data_min, options);
    options.animation = {
        startup: true,
        duration: 700,
        easing: 'out'
    };
    setTimeout(function () {
        classicChart.draw(data, options);
    }, 1000);
}


function drawChart2() {
    var classicChart = new google.visualization.ColumnChart(document.getElementById('chart2'));
    var data = google.visualization.arrayToDataTable(data_chart2);
    var container = document.getElementById('chart2');
    var data_min = google.visualization.arrayToDataTable(data_chart2_min);
    var data_max = google.visualization.arrayToDataTable(data_chart2_max);

    var Vmin = 0;
    var Vmax = Vmin;
    for (var i = 1; i < data_chart2.length; ++i) {
        if (data_chart2[i][1] > Vmax) Vmax = data_chart2[i][1];
        if (data_chart2[i][1] < Vmin) Vmin = data_chart2[i][1];
    }
    Vmax = Math.ceil(Vmax / 1000) * 1000;
    Vmin = Math.floor(Vmin / 1000) * 1000;
    var delta = Math.round((Vmax - Vmin) / 5);
    var ticksValues = [];
    for (var i = 0; i < 6; i++)
        ticksValues[i] = Vmin + delta * i;

    var options = {
        annotations: {
            alwaysOutside: true,
            strokeWidth: 5,
            datum: {
                stem: {
                    length: 0,
                }
            },
            textStyle:{
                color: '#e4e5e5'
            },
        },
        backgroundColor: '#e4e5e5',
        legend: {position: 'none'},
        bar: {groupWidth: "25%"},
        width: 400,
        width: 400,
        height: 270,
        chartArea: {left: 60, top: 20, width: '100%', height: '55%'},
        vAxis: {
            ticks: ticksValues,
            textStyle: {color: '#ed7d1a'},
            format: '#',
            viewWindowMode: 'maximized'
        },
        hAxis: {
            textStyle: {color: '#515352'},
            title: '',
            slantedText: true,
            slantedTextAngle: 90
        },
        colors: ['#ed7d1a']
    };

    if (document.body.clientWidth < 1300) {
        options.width = 900;
    }
    if (document.body.clientWidth < 1200) {
        options.width = 500;
    }
    if (document.body.clientWidth < 732) {
        options.width = 320;
        options.annotations.textStyle.fontSize = 9;
    }


    var observer = new MutationObserver(function () {
        var i = 0;
        var j = 0;
        var rect_y;

        Array.prototype.forEach.call(container.getElementsByTagName('rect'), function(rect) {
            if ((rect.getAttribute('fill') === '#ed7d1a')) {
                var chartLayout = classicChart.getChartLayoutInterface();
                rect_y = rect.getAttribute('y');
                j=0;
                Array.prototype.forEach.call(container.getElementsByTagName('text'), function(annotation) {
                    //console.log('i=',i,' j=',j);
                    if ((annotation.getAttribute('text-anchor') === 'middle') &&
                        (annotation.getAttribute('fill') === '#ed7d1b')) {
                        if (j==i || (j-1)==i){
                            annotation.setAttribute('y', rect_y-5);
                        }
                        j++;
                    }
                });
                i+=2;
            }

        });
    });






    classicChart.draw(data_min, options);
    options.animation = {
        duration: 500,
        easing: 'out'
    };
    options.annotations.textStyle.color = '#ed7d1b';

    setTimeout(function () {
        classicChart.draw(data, options);
    }, 1300);

    observer.observe(container, {
        childList: true,
        subtree: true
    });
}


function drawChart3() {
    var data = google.visualization.arrayToDataTable(data_chart3);
    var container = document.getElementById('chart3');
    var classicChart = new google.visualization.ColumnChart(document.getElementById('chart3'));
    var data_min = google.visualization.arrayToDataTable(data_chart3_min);
    var data_max = google.visualization.arrayToDataTable(data_chart3_max);

    var Vmin = 0;
    var Vmax = Vmin;
    for (var i = 1; i < data_chart3.length; ++i) {
        if (data_chart3[i][1] > Vmax) Vmax = data_chart3[i][1];
        if (data_chart3[i][1] < Vmin) Vmin = data_chart3[i][1];
    }
    var n = (Vmax > 1000) ? 1000 : 100;

    Vmax = Math.ceil(Vmax / n) * n;
    Vmin = Math.floor(Vmin / n) * n;
    var delta = Math.round((Vmax - Vmin) / 5);
    var ticksValues = [];
    for (var i = 0; i < 6; i++)
        ticksValues[i] = Vmin + delta * i;
    var options = {
        annotations: {
            alwaysOutside: true,
            strokeWidth: 5,
            datum: {
                stem: {
                    length: 0,
                }
            },
            textStyle:{
              color: '#e4e5e5'
            },
        },

        backgroundColor: '#e4e5e5',
        legend: {position: 'none'},
        bar: {groupWidth: "25%"},
        width: 400,
        height: 270,
        chartArea: {left: 60, top: 20, width: '100%', height: '60%'},
        vAxis: {
            ticks: ticksValues,
            textStyle: {color: '#ed7d1a'},
            format: '#',
            viewWindowMode: 'maximized'
        },

        hAxis: {
            textStyle: {color: '#515352'},
            title: '',
            slantedText: true,
            slantedTextAngle: 90,
            minTextSpacing: 3,

        },

        colors: ['#ed7d1a']
    };

    if (document.body.clientWidth < 1300) {
        options.width = 900;
    }
    if (document.body.clientWidth < 1200) {
        options.width = 500;
    }
    if (document.body.clientWidth < 732) {
        options.width = 320;
        options.annotations.textStyle.fontSize = 9;

    }

    var flag = true;
    var observer = new MutationObserver(function () {
        var i = 0;
        var j = 0;
        var rect_y;

        Array.prototype.forEach.call(container.getElementsByTagName('rect'), function(rect) {
            if ((rect.getAttribute('fill') === '#ed7d1a')) {
                var chartLayout = classicChart.getChartLayoutInterface();
                rect_y = rect.getAttribute('y');
                j=0;
                Array.prototype.forEach.call(container.getElementsByTagName('text'), function(annotation) {
                    //console.log('i=',i,' j=',j);
                    if ((annotation.getAttribute('text-anchor') === 'middle') &&
                        (annotation.getAttribute('fill') === '#ed7d1b')) {
                        if (j==i || (j-1)==i){
                            annotation.setAttribute('y', rect_y-5);
                        }
                        j++;
                        if (flag) {
                            setTimeout(function () {
                                jQuery.ajax({
                                    url: '/index.php?option=com_calculator&task=station.getpdf',
                                    data: {html: jQuery('.AB-solar').html()},
                                    headers: '',
                                    type: 'POST',
                                    success: function (response) {
                                        jQuery('a#save-btn').href = response;
                                        jQuery('.row.justify-content-center').fadeIn();
                                    }
                                });
                            }, 300);
                            flag = false;
                        }
                    }
                });
                i+=2;
            }

        });
    });

    classicChart.draw(data_min, options);
    options.animation = {
        duration: 500,
        easing: 'out'
    };
    options.annotations.textStyle.color = '#ed7d1b';

    setTimeout(function () {
        classicChart.draw(data, options);
    }, 1300);

    observer.observe(container, {
        childList: true,
        subtree: true
    });
}
